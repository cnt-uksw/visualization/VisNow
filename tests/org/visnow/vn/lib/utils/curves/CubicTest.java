/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.curves;

import java.util.Random;
import org.jogamp.vecmath.Point3d;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test Cubic class.
 * 
 * @author Norbert Kapiński (norkap@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 
 * 
 */


public class CubicTest {
    
    public CubicTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calcCubic3D method, of class Cubic.
     */
    @Test
    public void testCalcCubic3D() {
        
        //test linear case
        
        Point3d[] points = new Point3d[]{
            new Point3d(0,0,0),
            new Point3d(1,1,1),
            new Point3d(2,2,2),
            new Point3d(3,3,3),
            new Point3d(4,4,4),
            new Point3d(5,5,5),
            new Point3d(6,6,6),
        };
        int n = points.length-1;
        float[][][] expResult = new float[][][]{{{0,1,0,0}, {1,1,0,0}, {2,1,0,0}, {3,1,0,0}, {4,1,0,0}, {5,1,0,0}},
            {{0,1,0,0}, {1,1,0,0}, {2,1,0,0}, {3,1,0,0}, {4,1,0,0}, {5,1,0,0}},
            {{0,1,0,0}, {1,1,0,0}, {2,1,0,0}, {3,1,0,0}, {4,1,0,0}, {5,1,0,0}}};
        float[][][] result = Cubic.calcCubic3D(n, points);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                assertArrayEquals(expResult[i][j], result[i][j], 0.01f);
            }   
        }
        
        //test non-linear case
        points = new Point3d[]{
            new Point3d(3.1,3.1,3.1),
            new Point3d(1.4,1.4,1.4),
            new Point3d(2.2,2.2,2.2),
            new Point3d(3.7,3.7,3.7),
            new Point3d(4.9,4.9,4.9),
            new Point3d(-0.5,-0.5,-0.5),
            new Point3d(6.56,6.56,6.56),
        };
        expResult = new float[][][]{{{3.1f,-2.3636f, 0.0f, 0.6636f}, {1.4f,-0.3726f,2.0f,-0.8183f}, {2.2f,1.1543f,-0.4640f,0.8096f}, {3.7f,2.6553f,1.9649f,-3.4203f}, {4.9f,-3.6756f,-8.296f,6.5716f}, {-0.5f,-0.5526f,11.419f,-3.806f}},
            {{3.1f,-2.3636f, 0.0f, 0.6636f}, {1.4f,-0.3726f,2.0f,-0.8183f}, {2.2f,1.1543f,-0.4640f,0.8096f}, {3.7f,2.6553f,1.9649f,-3.4203f}, {4.9f,-3.6756f,-8.296f,6.5716f}, {-0.5f,-0.5526f,11.419f,-3.806f}},
            {{3.1f,-2.3636f, 0.0f, 0.6636f}, {1.4f,-0.3726f,2.0f,-0.8183f}, {2.2f,1.1543f,-0.4640f,0.8096f}, {3.7f,2.6553f,1.9649f,-3.4203f}, {4.9f,-3.6756f,-8.296f,6.5716f}, {-0.5f,-0.5526f,11.419f,-3.806f}}};
        result = Cubic.calcCubic3D(n, points);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                assertArrayEquals(expResult[i][j], result[i][j], 0.01f);
            }   
        }
    }

    /**
     * Test of calcCubic1D method, of class Cubic.
     */
    @Test
    public void testCalcCubic1D() {
        
        //test linear case
        float[] x = new float[]{0,1,2,3,4,5,6};
        int n = x.length - 1;
        float[][] expResult = new float[][]{{0,1,0,0}, {1,1,0,0}, {2,1,0,0}, {3,1,0,0}, {4,1,0,0}, {5,1,0,0}};
        float[][] result = Cubic.calcCubic1D(n, x);
        for (int i = 0; i < n; i++) {
            assertArrayEquals(expResult[i], result[i], 0.01f);
        }
        
        //test non-linear cases (4 and 6 points)
        x = new float[]{3.1f,1.4f,2.2f,3.7f,4.9f,-0.5f,6.56f};
        expResult = new float[][]{{3.1f,-2.3636f, 0.0f, 0.6636f}, {1.4f,-0.3726f,2.0f,-0.8183f}, {2.2f,1.1543f,-0.4640f,0.8096f}, {3.7f,2.6553f,1.9649f,-3.4203f}, {4.9f,-3.6756f,-8.296f,6.5716f}, {-0.5f,-0.5526f,11.419f,-3.806f}};
        result = Cubic.calcCubic1D(n, x);
        
        for (int i = 0; i < n; i++) {
            assertArrayEquals(expResult[i], result[i], 0.01f);
        }
        
        x = new float[]{3.1f,1.4f,2.2f,3.7f};
        n = x.length - 1;
        expResult = new float[][]{{3.1f,-2.32f, 0.0f, 0.6199f}, {1.4f,-0.4594f,1.86f,-0.6f}, {2.2f,1.46f,0.0599f,-0.0199f}};
        result = Cubic.calcCubic1D(n, x);
        
        for (int i = 0; i < n; i++) {
            assertArrayEquals(expResult[i], result[i], 0.01f);
        }
    }

    /**
     * Test of eval method, of class Cubic.
     */
    @Test
    public void testEval() {
        
        float[] c;
        float u, expResult, result;
        Random generator = new Random(1);
        for (int i = 0; i < 100; i++) {
            c = new float[]{generator.nextFloat(), generator.nextFloat(), generator.nextFloat(), generator.nextFloat()};
            for (int j = 0; j < 100; j++) {
                u = j/100.0f;
                expResult = u*u*u*c[3] + u*u*c[2] + u*c[1] + c[0];
                result = Cubic.eval(c, u);
                assertEquals(expResult, result, 0.002);
            }
        }
    }
    
    
    
  
}
