/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field.subset.subvolume;

import java.util.Vector;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.ShortLargeArray;
import org.visnow.jlargearrays.StringLargeArray;
import org.visnow.jscic.dataarrays.DataArrayType;

import static org.junit.Assert.*;
import org.visnow.jscic.TimeData;

/**
 *
 * @author know
 */


public class LinearInterpolationTest
{
    
    public LinearInterpolationTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }

    /**
     * Test of interpolateToNewNodesSet method, of class LinearInterpolation.
     */
    @Test
    public void testInterpolateToNewNodesSet()
    {
        System.out.println("interpolateToNewNodesSet");
        int nNodes = 3;
        Vector<TimeData> inVals = new Vector<>();
        short[] cs = {0, 1, 2};
        int[] ci   = {0, 1, 2};
        float[] cf = {0, 1, 2, 1, 2, 3};
        float[] cref = {0, 1, 2};
        float[] cimf = {2, 1, 0};
        String[] cst = {"a", "b", "c"};
        TimeData tds = new TimeData(DataArrayType.FIELD_DATA_SHORT);
        tds.setValue(new ShortLargeArray(cs), 0);
        TimeData tdi = new TimeData(DataArrayType.FIELD_DATA_INT);
        tdi.setValue(new IntLargeArray(ci), 0);
        TimeData tdf = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
        tdf.setValue(new FloatLargeArray(cf), 0);
        TimeData tdstr = new TimeData(DataArrayType.FIELD_DATA_STRING);
        tdstr.setValue(new StringLargeArray(cst), 0);
        TimeData tdc = new TimeData(DataArrayType.FIELD_DATA_COMPLEX);
        tdc.setValue(new ComplexFloatLargeArray(cref, cimf), 0);
        inVals.add(tds);
        inVals.add(tdi);
        inVals.add(tdf);
        inVals.add(tdc);
        inVals.add(tdstr);
        int[] vLens = {1, 1, 2, 1, 1};
        NewNode[] newNodes = {new NewNode(0, 1, .4f), new NewNode(0, 2, .6f),  new NewNode(1, 2, .5f)};
        boolean singleTimeMoment = false;
        Vector<TimeData> expResult = null;
        Vector<TimeData> result = LinearInterpolation.interpolateToNewNodesSet(nNodes, inVals, vLens, newNodes, singleTimeMoment);
//        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    
    public static void main(String[] args)
    {
        new LinearInterpolationTest().testInterpolateToNewNodesSet();
    }
    
}
