/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.expressions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.math3.util.FastMath;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayArithmetics;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.utils.FloatingPointUtils;

/**
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
@RunWith(value = Parameterized.class)
public class ArrayExpressionParserTest
{

    private static final float DELTA_F = 1E-4f;
    private static final double DELTA_D = 1E-13;
    private static final int N = 10;
    private static final float FLOAT_VAL = 0.5f;
    private static final double DOUBLE_VAL = 0.5;
    private static final float[] COMPLEX_VAL = new float[]{0.5f, 0.7f};
    private static final float[] VEC_FLOAT_VAL = new float[]{0.5f, 0.6f, 0.7f};
    private static final double[] VEC_DOUBLE_VAL = new double[]{0.5, 0.6, 0.7};
    private static final float[][] VEC_COMPLEX_VAL = new float[][]{{0.5f, 0.6f}, {0.7f, 0.8f}, {0.9f, 0.1f}};
    private final DataArray[] data;

    @Parameterized.Parameters
    public static Collection<Object[]> getParameters()
    {

        //scalar constant
        DataArray scalarConstantFloat = DataArray.createConstant(DataArrayType.FIELD_DATA_FLOAT, N, FLOAT_VAL);
        DataArray scalarConstantDouble = DataArray.createConstant(DataArrayType.FIELD_DATA_DOUBLE, N, DOUBLE_VAL);
        DataArray scalarConstantComplex = DataArray.createConstant(DataArrayType.FIELD_DATA_COMPLEX, N, COMPLEX_VAL);

        //scalar
        LargeArray laFloat = new FloatLargeArray(N, false);
        LargeArray laDouble = new DoubleLargeArray(N, false);
        ComplexFloatLargeArray laComplex = new ComplexFloatLargeArray(N, false);
        for (int i = 0; i < N; i++) {
            laFloat.setFloat(i, FLOAT_VAL);
            laDouble.setDouble(i, DOUBLE_VAL);
            laComplex.setComplexFloat(i, COMPLEX_VAL);
        }
        DataArray scalarFloat = DataArray.create(laFloat, 1, "float");
        DataArray scalarDouble = DataArray.create(laDouble, 1, "double");
        DataArray scalarComplex = DataArray.create(laComplex, 1, "complex");

        //scalar constant time data
        ArrayList<Float> timeSeries = new ArrayList<>(2);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        ArrayList<LargeArray> scalarConstantDataSeriesFloat = new ArrayList<>(2);
        scalarConstantDataSeriesFloat.add(scalarConstantFloat.getRawFloatArray());
        scalarConstantDataSeriesFloat.add(scalarConstantFloat.getRawFloatArray());
        TimeData constTdFloat = new TimeData(timeSeries, scalarConstantDataSeriesFloat, timeSeries.get(0));
        DataArray scalarConstantTimeDataFloat = DataArray.create(constTdFloat, 1, "data array");
        ArrayList<LargeArray> scalarConstantDataSeriesDouble = new ArrayList<>(2);
        scalarConstantDataSeriesDouble.add(scalarConstantDouble.getRawDoubleArray());
        scalarConstantDataSeriesDouble.add(scalarConstantDouble.getRawDoubleArray());
        TimeData constTdDouble = new TimeData(timeSeries, scalarConstantDataSeriesDouble, timeSeries.get(0));
        DataArray scalarConstantTimeDataDouble = DataArray.create(constTdDouble, 1, "data array");
        ArrayList<LargeArray> scalarConstantDataSeriesComplex = new ArrayList<>(2);
        scalarConstantDataSeriesComplex.add((LargeArray) scalarConstantComplex.getRawArray().clone());
        scalarConstantDataSeriesComplex.add((LargeArray) scalarConstantComplex.getRawArray().clone());
        TimeData constTdComplex = new TimeData(timeSeries, scalarConstantDataSeriesComplex, timeSeries.get(0));
        DataArray scalarConstantTimeDataComplex = DataArray.create(constTdComplex, 1, "data array");

        //scalar time data
        ArrayList<LargeArray> scalarDataSeriesFloat = new ArrayList<>(2);
        scalarDataSeriesFloat.add(scalarFloat.getRawFloatArray());
        scalarDataSeriesFloat.add(scalarFloat.getRawFloatArray());
        TimeData tdFloat = new TimeData(timeSeries, scalarDataSeriesFloat, timeSeries.get(0));
        DataArray scalarTimeDataFloat = DataArray.create(tdFloat, 1, "data array");
        ArrayList<LargeArray> scalarDataSeriesDouble = new ArrayList<>(2);
        scalarDataSeriesDouble.add(scalarDouble.getRawDoubleArray());
        scalarDataSeriesDouble.add(scalarDouble.getRawDoubleArray());
        TimeData tdDouble = new TimeData(timeSeries, scalarDataSeriesDouble, timeSeries.get(0));
        DataArray scalarTimeDataDouble = DataArray.create(tdDouble, 1, "data array");
        ArrayList<LargeArray> scalarDataSeriesComplex = new ArrayList<>(2);
        scalarDataSeriesComplex.add((LargeArray) scalarComplex.getRawArray().clone());
        scalarDataSeriesComplex.add((LargeArray) scalarComplex.getRawArray().clone());
        TimeData tdComplex = new TimeData(timeSeries, scalarDataSeriesComplex, timeSeries.get(0));
        DataArray scalarTimeDataComplex = DataArray.create(tdComplex, 1, "data array");

        //vector
        laFloat = new FloatLargeArray(3 * N, false);
        laDouble = new DoubleLargeArray(3 * N, false);
        laComplex = new ComplexFloatLargeArray(3 * N, false);
        for (int i = 0; i < N; i++) {
            for (int v = 0; v < 3; v++) {
                laFloat.setFloat(i * 3 + v, VEC_FLOAT_VAL[v]);
                laDouble.setDouble(i * 3 + v, VEC_DOUBLE_VAL[v]);
                laComplex.setComplexFloat(i * 3 + v, VEC_COMPLEX_VAL[v]);
            }
        }
        DataArray vectorFloat = DataArray.create(laFloat, 3, "float");
        DataArray vectorDouble = DataArray.create(laDouble, 3, "double");
        DataArray vectorComplex = DataArray.create(laComplex, 3, "complex");

        //vector time data
        ArrayList<LargeArray> vectorDataSeriesFloat = new ArrayList<>(2);
        vectorDataSeriesFloat.add(vectorFloat.getRawFloatArray());
        vectorDataSeriesFloat.add(vectorFloat.getRawFloatArray());
        TimeData vecTdFloat = new TimeData(timeSeries, vectorDataSeriesFloat, timeSeries.get(0));
        DataArray vectorTimeDataFloat = DataArray.create(vecTdFloat, 3, "data array");
        ArrayList<LargeArray> vectorDataSeriesDouble = new ArrayList<>(2);
        vectorDataSeriesDouble.add(vectorDouble.getRawDoubleArray());
        vectorDataSeriesDouble.add(vectorDouble.getRawDoubleArray());
        TimeData vecTdDouble = new TimeData(timeSeries, vectorDataSeriesDouble, timeSeries.get(0));
        DataArray vectorTimeDataDouble = DataArray.create(vecTdDouble, 3, "data array");
        ArrayList<LargeArray> vectorDataSeriesComplex = new ArrayList<>(2);
        vectorDataSeriesComplex.add((LargeArray) vectorComplex.getRawArray().clone());
        vectorDataSeriesComplex.add((LargeArray) vectorComplex.getRawArray().clone());
        TimeData vecTdComplex = new TimeData(timeSeries, vectorDataSeriesComplex, timeSeries.get(0));
        DataArray vectorTimeDataComplex = DataArray.create(vecTdComplex, 3, "data array");

        final DataArray[] inputs = new DataArray[]{scalarConstantFloat, scalarConstantDouble, scalarConstantComplex, scalarFloat, scalarDouble, scalarComplex,
                                                   scalarConstantTimeDataFloat, scalarConstantTimeDataDouble, scalarConstantTimeDataComplex, scalarTimeDataFloat, scalarTimeDataDouble, scalarTimeDataComplex,
                                                   vectorFloat, vectorDouble, vectorComplex, vectorTimeDataFloat, vectorTimeDataDouble, vectorTimeDataComplex};

        final ArrayList<Object[]> parameters = new ArrayList<>();
        for (int i = 0; i < inputs.length; i++) {
            parameters.add(new Object[]{inputs, 1});
            parameters.add(new Object[]{inputs, 7});
        }
        return parameters;
    }

    public ArrayExpressionParserTest(DataArray[] inputs, int nthreads)
    {
        data = inputs;
        ConcurrencyUtils.setNumberOfThreads(nthreads);
        if (nthreads > 1) {
            ConcurrencyUtils.setConcurrentThreshold(1);
        }
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    private double processNaNs(double x)
    {
        return FloatingPointUtils.processNaNs(x, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
    }

    private float[] processNaNs(float[] x)
    {
        float[] res = new float[x.length];
        for (int i = 0; i < x.length; i++) {
            res[i] = FloatingPointUtils.processNaNs(x[i], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
        }
        return res;
    }

    private float processNaNs(float x)
    {
        return FloatingPointUtils.processNaNs(x, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
    }

    @Test
    public void testVECTOR() throws Exception
    {
        String expr = "(a,b)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, true, vars);
                DataArray da;
                da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(veclen_a + veclen_b, veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                if (da.getType() != DataArrayType.FIELD_DATA_COMPLEX) {
                    TimeData td_a = a.getTimeData().convertToDouble();
                    TimeData td_b = b.getTimeData().convertToDouble();
                    for (Float time : timeSeries) {
                        LargeArray aa = td_a.getValue(time);
                        LargeArray bb = td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                if (v < veclen_a) {
                                    assertEquals(aa.getDouble(k * veclen_a + v), cc.getDouble(k * veclen + v), DELTA_D);
                                } else {
                                    assertEquals(bb.getDouble(k * veclen_b + (v - veclen_a)), cc.getDouble(k * veclen + v), DELTA_D);
                                }
                            }
                        }
                    }
                } else {
                    TimeData td_a = a.getTimeData().convertToComplex();
                    TimeData td_b = b.getTimeData().convertToComplex();
                    for (Float time : timeSeries) {
                        ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                        ComplexFloatLargeArray bb = (ComplexFloatLargeArray) td_b.getValue(time);
                        ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                if (v < veclen_a) {
                                    assertArrayEquals(aa.getComplexFloat(k * veclen_a + v), cc.getComplexFloat(k * veclen + v), DELTA_F);
                                } else {
                                    assertArrayEquals(bb.getComplexFloat(k * veclen_b + (v - veclen_a)), cc.getComplexFloat(k * veclen + v), DELTA_F);
                                }
                            }
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, true, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(veclen_a + veclen_b, veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                if (da.getType() != DataArrayType.FIELD_DATA_COMPLEX) {
                    TimeData td_a = a.getTimeData().convertToFloat();
                    TimeData td_b = b.getTimeData().convertToFloat();
                    for (Float time : timeSeries) {
                        LargeArray aa = td_a.getValue(time);
                        LargeArray bb = td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                if (v < veclen_a) {
                                    assertEquals(aa.getFloat(k * veclen_a + v), cc.getFloat(k * veclen + v), DELTA_F);
                                } else {
                                    assertEquals(bb.getFloat(k * veclen_b + (v - veclen_a)), cc.getFloat(k * veclen + v), DELTA_F);
                                }
                            }
                        }
                    }
                } else {
                    TimeData td_a = a.getTimeData().convertToComplex();
                    TimeData td_b = b.getTimeData().convertToComplex();
                    for (Float time : timeSeries) {
                        ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                        ComplexFloatLargeArray bb = (ComplexFloatLargeArray) td_b.getValue(time);
                        ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                if (v < veclen_a) {
                                    assertArrayEquals(aa.getComplexFloat(k * veclen_a + v), cc.getComplexFloat(k * veclen + v), DELTA_F);
                                } else {
                                    assertArrayEquals(bb.getComplexFloat(k * veclen_b + (v - veclen_a)), cc.getComplexFloat(k * veclen + v), DELTA_F);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testSUM() throws Exception
    {
        String expr = "a+b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, true, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                if (da.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
                    TimeData td_a = a.getTimeData().convertToDouble();
                    TimeData td_b = b.getTimeData().convertToDouble();
                    for (Float time : timeSeries) {
                        LargeArray aa = td_a.getValue(time);
                        LargeArray bb = td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(aa.getDouble(k * veclen_a + v % veclen_a) + bb.getDouble(k * veclen_b + v % veclen_b), cc.getDouble(k * veclen + v), DELTA_D);
                            }
                        }
                    }
                } else {
                    TimeData td_a = a.getTimeData().convertToComplex();
                    TimeData td_b = b.getTimeData().convertToComplex();
                    for (Float time : timeSeries) {
                        ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                        ComplexFloatLargeArray bb = (ComplexFloatLargeArray) td_b.getValue(time);
                        ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertArrayEquals(LargeArrayArithmetics.complexAdd(aa.getComplexFloat(k * veclen_a + v % veclen_a), bb.getComplexFloat(k * veclen_b + v % veclen_b)), cc.getComplexFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, true, vars);
                try {
                    da = parser.evaluateExpr(expr);
                } catch (Exception ex) {
                    Logger.getLogger(ArrayExpressionParserTest.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                    TimeData td_a = a.getTimeData().convertToFloat();
                    TimeData td_b = b.getTimeData().convertToFloat();
                    for (Float time : timeSeries) {
                        LargeArray aa = td_a.getValue(time);
                        LargeArray bb = td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(aa.getFloat(k * veclen_a + v % veclen_a) + bb.getFloat(k * veclen_b + v % veclen_b), cc.getFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                } else {
                    TimeData td_a = a.getTimeData().convertToComplex();
                    TimeData td_b = b.getTimeData().convertToComplex();
                    for (Float time : timeSeries) {
                        ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                        ComplexFloatLargeArray bb = (ComplexFloatLargeArray) td_b.getValue(time);
                        ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertArrayEquals(LargeArrayArithmetics.complexAdd(aa.getComplexFloat(k * veclen_a + v % veclen_a), bb.getComplexFloat(k * veclen_b + v % veclen_b)), cc.getComplexFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testDIFF() throws Exception
    {
        String expr = "a-b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, true, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                if (da.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
                    TimeData td_a = a.getTimeData().convertToDouble();
                    TimeData td_b = b.getTimeData().convertToDouble();
                    for (Float time : timeSeries) {
                        LargeArray aa = td_a.getValue(time);
                        LargeArray bb = td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(aa.getDouble(k * veclen_a + v % veclen_a) - bb.getDouble(k * veclen_b + v % veclen_b), cc.getDouble(k * veclen + v), DELTA_D);
                            }
                        }
                    }
                } else {
                    TimeData td_a = a.getTimeData().convertToComplex();
                    TimeData td_b = b.getTimeData().convertToComplex();
                    for (Float time : timeSeries) {
                        ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                        ComplexFloatLargeArray bb = (ComplexFloatLargeArray) td_b.getValue(time);
                        ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertArrayEquals(LargeArrayArithmetics.complexDiff(aa.getComplexFloat(k * veclen_a + v % veclen_a), bb.getComplexFloat(k * veclen_b + v % veclen_b)), cc.getComplexFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, true, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                    TimeData td_a = a.getTimeData().convertToFloat();
                    TimeData td_b = b.getTimeData().convertToFloat();
                    for (Float time : timeSeries) {
                        LargeArray aa = td_a.getValue(time);
                        LargeArray bb = td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(aa.getFloat(k * veclen_a + v % veclen_a) - bb.getFloat(k * veclen_b + v % veclen_b), cc.getFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                } else {
                    TimeData td_a = a.getTimeData().convertToComplex();
                    TimeData td_b = b.getTimeData().convertToComplex();
                    for (Float time : timeSeries) {
                        ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                        ComplexFloatLargeArray bb = (ComplexFloatLargeArray) td_b.getValue(time);
                        ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertArrayEquals(LargeArrayArithmetics.complexDiff(aa.getComplexFloat(k * veclen_a + v % veclen_a), bb.getComplexFloat(k * veclen_b + v % veclen_b)), cc.getComplexFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testMULT() throws Exception
    {
        String expr = "a*b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, true, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(veclen_a == veclen_b ? 1 : FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                if (da.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
                    TimeData td_a = a.getTimeData().convertToDouble();
                    TimeData td_b = b.getTimeData().convertToDouble();
                    for (Float time : timeSeries) {
                        LargeArray aa = td_a.getValue(time);
                        LargeArray bb = td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        if (veclen_a == veclen_b) {
                            for (int k = 0; k < N; k++) {
                                double dot = 0;
                                for (int v = 0; v < veclen_a; v++) {
                                    dot += aa.getDouble(k * veclen_a + v) * bb.getDouble(k * veclen_b + v);
                                }
                                assertEquals(processNaNs(dot), cc.getDouble(k), DELTA_D);
                            }
                        } else {
                            for (int k = 0; k < N; k++) {
                                for (int v = 0; v < veclen; v++) {
                                    assertEquals(processNaNs(aa.getDouble(k * veclen_a + v % veclen_a) * bb.getDouble(k * veclen_b + v % veclen_b)), cc.getDouble(k * veclen + v), DELTA_D);
                                }
                            }
                        }
                    }
                } else {
                    TimeData td_a = a.getTimeData().convertToComplex();
                    TimeData td_b = b.getTimeData().convertToComplex();
                    for (Float time : timeSeries) {
                        ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                        ComplexFloatLargeArray bb = (ComplexFloatLargeArray) td_b.getValue(time);
                        ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                        if (veclen_a == veclen_b) {
                            for (int k = 0; k < N; k++) {
                                float[] dot = new float[2];
                                for (int v = 0; v < veclen_a; v++) {
                                    dot = LargeArrayArithmetics.complexAdd(dot, LargeArrayArithmetics.complexMult(aa.getComplexFloat(k * veclen_a + v), bb.getComplexFloat(k * veclen_b + v)));
                                }
                                assertArrayEquals(processNaNs(dot), cc.getComplexFloat(k), DELTA_F);
                            }
                        } else {
                            for (int k = 0; k < N; k++) {
                                for (int v = 0; v < veclen; v++) {
                                    assertArrayEquals(processNaNs(LargeArrayArithmetics.complexMult(aa.getComplexFloat(k * veclen_a + v % veclen_a), bb.getComplexFloat(k * veclen_b + v % veclen_b))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                                }
                            }
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, true, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(veclen_a == veclen_b ? 1 : FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                    TimeData td_a = a.getTimeData().convertToFloat();
                    TimeData td_b = b.getTimeData().convertToFloat();
                    for (Float time : timeSeries) {
                        LargeArray aa = td_a.getValue(time);
                        LargeArray bb = td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        if (veclen_a == veclen_b) {
                            for (int k = 0; k < N; k++) {
                                float dot = 0;
                                for (int v = 0; v < veclen_a; v++) {
                                    dot += aa.getFloat(k * veclen_a + v) * bb.getFloat(k * veclen_b + v);
                                }
                                assertEquals(processNaNs(dot), cc.getFloat(k), DELTA_F);
                            }
                        } else {
                            for (int k = 0; k < N; k++) {
                                for (int v = 0; v < veclen; v++) {
                                    assertEquals(processNaNs(aa.getFloat(k * veclen_a + v % veclen_a) * bb.getFloat(k * veclen_b + v % veclen_b)), cc.getFloat(k * veclen + v), DELTA_F);
                                }
                            }
                        }
                    }
                } else {
                    TimeData td_a = a.getTimeData().convertToComplex();
                    TimeData td_b = b.getTimeData().convertToComplex();
                    for (Float time : timeSeries) {
                        ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                        ComplexFloatLargeArray bb = (ComplexFloatLargeArray) td_b.getValue(time);
                        ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                        if (veclen_a == veclen_b) {
                            for (int k = 0; k < N; k++) {
                                float[] dot = new float[2];
                                for (int v = 0; v < veclen_a; v++) {
                                    dot = LargeArrayArithmetics.complexAdd(dot, LargeArrayArithmetics.complexMult(aa.getComplexFloat(k * veclen_a + v), bb.getComplexFloat(k * veclen_b + v)));
                                }
                                assertArrayEquals(processNaNs(dot), cc.getComplexFloat(k), DELTA_F);
                            }
                        } else {
                            for (int k = 0; k < N; k++) {
                                for (int v = 0; v < veclen; v++) {
                                    assertArrayEquals(processNaNs(LargeArrayArithmetics.complexMult(aa.getComplexFloat(k * veclen_a + v % veclen_a), bb.getComplexFloat(k * veclen_b + v % veclen_b))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testDIV() throws Exception
    {
        String expr = "a/b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, true, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                if (da.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
                    TimeData td_a = a.getTimeData().convertToDouble();
                    TimeData td_b = b.getTimeData().convertToDouble();
                    for (Float time : timeSeries) {
                        LargeArray aa = td_a.getValue(time);
                        LargeArray bb = td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(processNaNs(aa.getDouble(k * veclen_a + v % veclen_a) / bb.getDouble(k * veclen_b + v % veclen_b)), cc.getDouble(k * veclen + v), DELTA_D);
                            }
                        }
                    }
                } else {
                    TimeData td_a = a.getTimeData().convertToComplex();
                    TimeData td_b = b.getTimeData().convertToComplex();
                    for (Float time : timeSeries) {
                        ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                        ComplexFloatLargeArray bb = (ComplexFloatLargeArray) td_b.getValue(time);
                        ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertArrayEquals(processNaNs(LargeArrayArithmetics.complexDiv(aa.getComplexFloat(k * veclen_a + v % veclen_a), bb.getComplexFloat(k * veclen_b + v % veclen_b))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, true, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                    TimeData td_a = a.getTimeData().convertToFloat();
                    TimeData td_b = b.getTimeData().convertToFloat();
                    for (Float time : timeSeries) {
                        LargeArray aa = td_a.getValue(time);
                        LargeArray bb = td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(processNaNs(aa.getFloat(k * veclen_a + v % veclen_a) / bb.getFloat(k * veclen_b + v % veclen_b)), cc.getFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                } else {
                    TimeData td_a = a.getTimeData().convertToComplex();
                    TimeData td_b = b.getTimeData().convertToComplex();
                    for (Float time : timeSeries) {
                        ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                        ComplexFloatLargeArray bb = (ComplexFloatLargeArray) td_b.getValue(time);
                        ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertArrayEquals(processNaNs(LargeArrayArithmetics.complexDiv(aa.getComplexFloat(k * veclen_a + v % veclen_a), bb.getComplexFloat(k * veclen_b + v % veclen_b))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testPOW() throws Exception
    {
        String expr = "a^b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, true, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                if (da.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
                    TimeData td_a = a.getTimeData().convertToDouble();
                    TimeData td_b = b.getTimeData().convertToDouble();
                    for (Float time : timeSeries) {
                        LargeArray aa = td_a.getValue(time);
                        LargeArray bb = td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(processNaNs(FastMath.pow(aa.getDouble(k * veclen_a + v % veclen_a), bb.getDouble(k * veclen_b + v % veclen_b))), cc.getDouble(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                } else {
                    TimeData td_a = a.getTimeData().convertToComplex();
                    TimeData td_b = b.getTimeData().convertToComplex();
                    for (Float time : timeSeries) {
                        ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                        ComplexFloatLargeArray bb = (ComplexFloatLargeArray) td_b.getValue(time);
                        ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertArrayEquals(processNaNs(LargeArrayArithmetics.complexPow(aa.getComplexFloat(k * veclen_a + v % veclen_a), bb.getComplexFloat(k * veclen_b + v % veclen_b))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, true, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                    TimeData td_a = a.getTimeData().convertToFloat();
                    TimeData td_b = b.getTimeData().convertToFloat();
                    for (Float time : timeSeries) {
                        LargeArray aa = td_a.getValue(time);
                        LargeArray bb = td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(processNaNs(FastMath.pow(aa.getFloat(k * veclen_a + v % veclen_a), bb.getFloat(k * veclen_b + v % veclen_b))), cc.getFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                } else {
                    TimeData td_a = a.getTimeData().convertToComplex();
                    TimeData td_b = b.getTimeData().convertToComplex();
                    for (Float time : timeSeries) {
                        ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                        ComplexFloatLargeArray bb = (ComplexFloatLargeArray) td_b.getValue(time);
                        ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertArrayEquals(processNaNs(LargeArrayArithmetics.complexPow(aa.getComplexFloat(k * veclen_a + v % veclen_a), bb.getComplexFloat(k * veclen_b + v % veclen_b))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testNEG() throws Exception
    {
        String expr = "~a";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, true, vars);
            DataArray da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
                TimeData td_a = a.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(-aa.getDouble(k * veclen + v), cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            float[] expected = aa.getComplexFloat(k * veclen + v);
                            expected[0] = -expected[0];
                            expected[1] = -expected[1];
                            assertArrayEquals(expected, cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, true, vars);
            da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                TimeData td_a = a.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(-aa.getFloat(k * veclen + v), cc.getFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            float[] expected = aa.getComplexFloat(k * veclen + v);
                            expected[0] = -expected[0];
                            expected[1] = -expected[1];
                            assertArrayEquals(expected, cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testSQRT() throws Exception
    {
        String expr = "sqrt(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, true, vars);
            DataArray da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
                TimeData td_a = a.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.sqrt(aa.getDouble(k * veclen + v))), cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexSqrt(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, true, vars);
            da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                TimeData td_a = a.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.sqrt(aa.getFloat(k * veclen + v))), cc.getFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexSqrt(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testLOG() throws Exception
    {
        String expr = "log(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, true, vars);
            DataArray da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
                TimeData td_a = a.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.log(aa.getDouble(k * veclen + v))), cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexLog(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, true, vars);
            da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                TimeData td_a = a.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.log(aa.getFloat(k * veclen + v))), cc.getFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexLog(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testLOG10() throws Exception
    {
        String expr = "log10(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, true, vars);
            DataArray da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
                TimeData td_a = a.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.log10(aa.getDouble(k * veclen + v))), cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexLog10(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, true, vars);
            da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                TimeData td_a = a.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.log10(aa.getFloat(k * veclen + v))), cc.getFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexLog10(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testEXP() throws Exception
    {
        String expr = "exp(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, true, vars);
            DataArray da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
                TimeData td_a = a.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.exp(aa.getDouble(k * veclen + v))), cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexExp(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, true, vars);
            da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                TimeData td_a = a.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.exp(aa.getFloat(k * veclen + v))), cc.getFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexExp(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testABS() throws Exception
    {
        String expr = "abs(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, true, vars);
            DataArray da = parser.evaluateExpr(expr);
            assertEquals(1, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
            assertEquals(a.getNFrames(), da.getNFrames());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            if (a.getType() != DataArrayType.FIELD_DATA_COMPLEX) {
                TimeData td_a = a.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        double norm = 0;
                        for (int v = 0; v < veclen; v++) {
                            norm += aa.getDouble(k * veclen + v) * aa.getDouble(k * veclen + v);
                        }
                        assertEquals(processNaNs(FastMath.sqrt(norm)), cc.getDouble(k), DELTA_D);
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        double norm = 0;
                        for (int v = 0; v < veclen; v++) {
                            float[] elem_a = aa.getComplexFloat(k * veclen + v);
                            norm += elem_a[0] * elem_a[0] + elem_a[1] * elem_a[1];
                        }
                        assertEquals(processNaNs((float) FastMath.sqrt(norm)), cc.getFloat(k), DELTA_F);
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, true, vars);
            da = parser.evaluateExpr(expr);
            assertEquals(1, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT);
            assertEquals(a.getNFrames(), da.getNFrames());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            if (a.getType() != DataArrayType.FIELD_DATA_COMPLEX) {
                TimeData td_a = a.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        double norm = 0;
                        for (int v = 0; v < veclen; v++) {
                            norm += aa.getFloat(k * veclen + v) * aa.getFloat(k * veclen + v);
                        }
                        assertEquals(processNaNs((float) FastMath.sqrt(norm)), cc.getFloat(k), DELTA_F);
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        double norm = 0;
                        for (int v = 0; v < veclen; v++) {
                            float[] elem_a = aa.getComplexFloat(k * veclen + v);
                            norm += elem_a[0] * elem_a[0] + elem_a[1] * elem_a[1];
                        }
                        assertEquals(processNaNs((float) FastMath.sqrt(norm)), cc.getFloat(k), DELTA_F);
                    }
                }
            }
        }
    }

    @Test
    public void testSIN() throws Exception
    {
        String expr = "sin(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, true, vars);
            DataArray da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
                TimeData td_a = a.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.sin(aa.getDouble(k * veclen + v))), cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexSin(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, true, vars);
            da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                TimeData td_a = a.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.sin(aa.getFloat(k * veclen + v))), cc.getFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexSin(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testCOS() throws Exception
    {
        String expr = "cos(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, true, vars);
            DataArray da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
                TimeData td_a = a.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.cos(aa.getDouble(k * veclen + v))), cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexCos(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, true, vars);
            da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                TimeData td_a = a.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.cos(aa.getFloat(k * veclen + v))), cc.getFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexCos(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testTAN() throws Exception
    {
        String expr = "tan(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, true, vars);
            DataArray da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
                TimeData td_a = a.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.tan(aa.getDouble(k * veclen + v))), cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexTan(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, true, vars);
            da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                TimeData td_a = a.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.tan(aa.getFloat(k * veclen + v))), cc.getFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexTan(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testASIN() throws Exception
    {
        String expr = "asin(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, true, vars);
            DataArray da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
                TimeData td_a = a.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.asin(aa.getDouble(k * veclen + v))), cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexAsin(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, true, vars);
            da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                TimeData td_a = a.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.asin(aa.getFloat(k * veclen + v))), cc.getFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexAsin(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testACOS() throws Exception
    {
        String expr = "acos(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, true, vars);
            DataArray da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
                TimeData td_a = a.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.acos(aa.getDouble(k * veclen + v))), cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexAcos(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, true, vars);
            da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                TimeData td_a = a.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.acos(aa.getFloat(k * veclen + v))), cc.getFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexAcos(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testATAN() throws Exception
    {
        String expr = "atan(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, true, vars);
            DataArray da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
                TimeData td_a = a.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.atan(aa.getDouble(k * veclen + v))), cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexAtan(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, true, vars);
            da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                TimeData td_a = a.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(FastMath.atan(aa.getFloat(k * veclen + v))), cc.getFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            } else {
                TimeData td_a = a.getTimeData().convertToComplex();
                for (Float time : timeSeries) {
                    ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                    ComplexFloatLargeArray cc = (ComplexFloatLargeArray) td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertArrayEquals(processNaNs(LargeArrayArithmetics.complexAtan(aa.getComplexFloat(k * veclen + v))), cc.getComplexFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testSIG() throws Exception
    {
        String expr = "signum(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            if (a.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                continue;
            }
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, true, vars);
            DataArray da = parser.evaluateExpr(expr);
            int veclen = a.getVectorLength();
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
            assertEquals(a.getNFrames(), da.getNFrames());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            TimeData td_a = a.getTimeData().convertToDouble();
            for (Float time : timeSeries) {
                LargeArray aa = td_a.getValue(time);
                LargeArray cc = td.getValue(time);
                for (int k = 0; k < N; k++) {
                    for (int v = 0; v < veclen; v++) {
                        assertEquals(FastMath.signum(aa.getDouble(k * veclen_a + v)), cc.getDouble(k * veclen + v), DELTA_D);
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, true, vars);
            da = parser.evaluateExpr(expr);
            veclen = a.getVectorLength();
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT);
            assertEquals(a.getNFrames(), da.getNFrames());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            td_a = a.getTimeData().convertToFloat();
            for (Float time : timeSeries) {
                LargeArray aa = td_a.getValue(time);
                LargeArray cc = td.getValue(time);
                for (int k = 0; k < N; k++) {
                    for (int v = 0; v < veclen; v++) {
                        assertEquals(FastMath.signum(aa.getFloat(k * veclen_a + v)), cc.getFloat(k * veclen + v), DELTA_F);
                    }
                }
            }
        }
    }

    @Test
    public void testGT() throws Exception
    {
        String expr = "a>b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            if (a.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                continue;
            }
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                if (b.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                    continue;
                }
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, true, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                TimeData td_a = a.getTimeData().convertToDouble();
                TimeData td_b = b.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(aa.getDouble(k * veclen_a + v % veclen_a) > bb.getDouble(k * veclen_b + v % veclen_b) ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, true, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                td_a = a.getTimeData().convertToFloat();
                td_b = b.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(aa.getDouble(k * veclen_a + v % veclen_a) > bb.getDouble(k * veclen_b + v % veclen_b) ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testLT() throws Exception
    {
        String expr = "a<b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            if (a.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                continue;
            }
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                if (b.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                    continue;
                }
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, true, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                TimeData td_a = a.getTimeData().convertToDouble();
                TimeData td_b = b.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(aa.getDouble(k * veclen_a + v % veclen_a) < bb.getDouble(k * veclen_b + v % veclen_b) ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, true, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                td_a = a.getTimeData().convertToFloat();
                td_b = b.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(aa.getDouble(k * veclen_a + v % veclen_a) < bb.getDouble(k * veclen_b + v % veclen_b) ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testGET() throws Exception
    {
        String expr = "a>=b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            if (a.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                continue;
            }
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                if (b.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                    continue;
                }
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, true, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                TimeData td_a = a.getTimeData().convertToDouble();
                TimeData td_b = b.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(aa.getDouble(k * veclen_a + v % veclen_a) >= bb.getDouble(k * veclen_b + v % veclen_b) ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, true, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                td_a = a.getTimeData().convertToFloat();
                td_b = b.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(aa.getDouble(k * veclen_a + v % veclen_a) >= bb.getDouble(k * veclen_b + v % veclen_b) ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testLET() throws Exception
    {
        String expr = "a<=b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            if (a.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                continue;
            }
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                if (b.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                    continue;
                }
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, true, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                TimeData td_a = a.getTimeData().convertToDouble();
                TimeData td_b = b.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(aa.getDouble(k * veclen_a + v % veclen_a) <= bb.getDouble(k * veclen_b + v % veclen_b) ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, true, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                td_a = a.getTimeData().convertToFloat();
                td_b = b.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(aa.getDouble(k * veclen_a + v % veclen_a) <= bb.getDouble(k * veclen_b + v % veclen_b) ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    private static double compareComplex(float[] z1, float[] z2)
    {
        if (z1[0] == z2[0] && z1[1] == z2[1]) return 1.0;
        return 0.0;
    }

    @Test
    public void testEQ() throws Exception
    {
        String expr = "a==b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, true, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                if (a.getType() != DataArrayType.FIELD_DATA_COMPLEX && b.getType() != DataArrayType.FIELD_DATA_COMPLEX) {
                    TimeData td_a = a.getTimeData().convertToDouble();
                    TimeData td_b = b.getTimeData().convertToDouble();
                    for (Float time : timeSeries) {
                        LargeArray aa = td_a.getValue(time);
                        LargeArray bb = td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(aa.getDouble(k * veclen_a + v % veclen_a) == bb.getDouble(k * veclen_b + v % veclen_b) ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_D);
                            }
                        }
                    }
                } else {
                    TimeData td_a = a.getTimeData().convertToComplex();
                    TimeData td_b = b.getTimeData().convertToComplex();
                    for (Float time : timeSeries) {
                        ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                        ComplexFloatLargeArray bb = (ComplexFloatLargeArray) td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(compareComplex(aa.getComplexFloat(k * veclen_a + v % veclen_a), bb.getComplexFloat(k * veclen_b + v % veclen_b)), cc.getFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, true, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                if (a.getType() != DataArrayType.FIELD_DATA_COMPLEX && b.getType() != DataArrayType.FIELD_DATA_COMPLEX) {
                    TimeData td_a = a.getTimeData().convertToFloat();
                    TimeData td_b = b.getTimeData().convertToFloat();
                    for (Float time : timeSeries) {
                        LargeArray aa = td_a.getValue(time);
                        LargeArray bb = td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(aa.getFloat(k * veclen_a + v % veclen_a) == bb.getFloat(k * veclen_b + v % veclen_b) ? 1.0 : 0.0, cc.getFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                } else {
                    TimeData td_a = a.getTimeData().convertToComplex();
                    TimeData td_b = b.getTimeData().convertToComplex();
                    for (Float time : timeSeries) {
                        ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                        ComplexFloatLargeArray bb = (ComplexFloatLargeArray) td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(compareComplex(aa.getComplexFloat(k * veclen_a + v % veclen_a), bb.getComplexFloat(k * veclen_b + v % veclen_b)), cc.getFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testNEQ() throws Exception
    {
        String expr = "a!=b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, true, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                if (a.getType() != DataArrayType.FIELD_DATA_COMPLEX && b.getType() != DataArrayType.FIELD_DATA_COMPLEX) {
                    TimeData td_a = a.getTimeData().convertToDouble();
                    TimeData td_b = b.getTimeData().convertToDouble();
                    for (Float time : timeSeries) {
                        LargeArray aa = td_a.getValue(time);
                        LargeArray bb = td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(aa.getDouble(k * veclen_a + v % veclen_a) != bb.getDouble(k * veclen_b + v % veclen_b) ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_D);
                            }
                        }
                    }
                } else {
                    TimeData td_a = a.getTimeData().convertToComplex();
                    TimeData td_b = b.getTimeData().convertToComplex();
                    for (Float time : timeSeries) {
                        ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                        ComplexFloatLargeArray bb = (ComplexFloatLargeArray) td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(1.0 - compareComplex(aa.getComplexFloat(k * veclen_a + v % veclen_a), bb.getComplexFloat(k * veclen_b + v % veclen_b)), cc.getFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, true, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                if (a.getType() != DataArrayType.FIELD_DATA_COMPLEX && b.getType() != DataArrayType.FIELD_DATA_COMPLEX) {
                    TimeData td_a = a.getTimeData().convertToFloat();
                    TimeData td_b = b.getTimeData().convertToFloat();
                    for (Float time : timeSeries) {
                        LargeArray aa = td_a.getValue(time);
                        LargeArray bb = td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(aa.getFloat(k * veclen_a + v % veclen_a) != bb.getFloat(k * veclen_b + v % veclen_b) ? 1.0 : 0.0, cc.getFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                } else {
                    TimeData td_a = a.getTimeData().convertToComplex();
                    TimeData td_b = b.getTimeData().convertToComplex();
                    for (Float time : timeSeries) {
                        ComplexFloatLargeArray aa = (ComplexFloatLargeArray) td_a.getValue(time);
                        ComplexFloatLargeArray bb = (ComplexFloatLargeArray) td_b.getValue(time);
                        LargeArray cc = td.getValue(time);
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(1.0 - compareComplex(aa.getComplexFloat(k * veclen_a + v % veclen_a), bb.getComplexFloat(k * veclen_b + v % veclen_b)), cc.getFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testAVG() throws Exception
    {
        String expr = "avg(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            if (a.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                continue;
            }
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, true, vars);
            DataArray da = parser.evaluateExpr(expr);
            int veclen = a.getVectorLength();
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
            assertEquals(a.getNFrames(), da.getNFrames());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            for (Float time : timeSeries) {
                LargeArray cc = td.getValue(time);
                if (veclen == 1) {
                    for (int k = 0; k < N; k++) {
                        assertEquals(DOUBLE_VAL, cc.getDouble(k), DELTA_F);
                    }
                } else {
                    double[] elem = new double[veclen];
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            elem[v] = cc.getDouble(k * veclen + v);
                        }
                        assertArrayEquals(VEC_DOUBLE_VAL, elem, DELTA_F);
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, true, vars);
            da = parser.evaluateExpr(expr);
            veclen = a.getVectorLength();
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT);
            assertEquals(a.getNFrames(), da.getNFrames());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            for (Float time : timeSeries) {
                LargeArray cc = td.getValue(time);
                if (veclen == 1) {
                    for (int k = 0; k < N; k++) {
                        assertEquals(FLOAT_VAL, cc.getFloat(k), DELTA_F);
                    }
                } else {
                    float[] elem = new float[veclen];
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            elem[v] = cc.getFloat(k * veclen + v);
                        }
                        assertArrayEquals(VEC_FLOAT_VAL, elem, DELTA_F);
                    }
                }
            }
        }
    }

    @Test
    public void testSTDDEV() throws Exception
    {
        String expr = "stddev(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            if (a.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                continue;
            }
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, true, vars);
            DataArray da = parser.evaluateExpr(expr);
            int veclen = a.getVectorLength();
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
            assertEquals(a.getNFrames(), da.getNFrames());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            for (Float time : timeSeries) {
                LargeArray cc = td.getValue(time);
                for (int k = 0; k < N; k++) {
                    for (int v = 0; v < veclen; v++) {
                        assertEquals(0, cc.getDouble(k * veclen + v), DELTA_F);
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, true, vars);
            da = parser.evaluateExpr(expr);
            veclen = a.getVectorLength();
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT);
            assertEquals(a.getNFrames(), da.getNFrames());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            for (Float time : timeSeries) {
                LargeArray cc = td.getValue(time);
                for (int k = 0; k < N; k++) {
                    for (int v = 0; v < veclen; v++) {
                        assertEquals(0, cc.getDouble(k * veclen + v), DELTA_F);
                    }
                }
            }
        }
    }

    /**
     * Test of tokenType method, of class ArrayExpressionParser.
     */
    @Test
    public void testTokenType() {
        System.out.println("tokenType");
        String s = "";
        Map<String, DataArray> variables = null;
        ArrayExpressionParser.TOKEN expResult = null;
        ArrayExpressionParser.TOKEN result = ArrayExpressionParser.tokenType(s, variables);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addArgumentsCounts method, of class ArrayExpressionParser.
     */
    @Test
    public void testAddArgumentsCounts() {
        System.out.println("addArgumentsCounts");
        String[] inputTokens = null;
        String[] expResult = null;
        String[] result = ArrayExpressionParser.addArgumentsCounts(inputTokens);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of infixToRPN method, of class ArrayExpressionParser.
     */
    @Test
    public void testInfixToRPN() {
        System.out.println("infixToRPN");
        String[] input = null;
        Map<String, DataArray> variables = null;
        String[] expResult = null;
        String[] result = ArrayExpressionParser.infixToRPN(input, variables);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isNumeric method, of class ArrayExpressionParser.
     */
    @Test
    public void testIsNumeric() {
        System.out.println("isNumeric");
        String token = "";
        boolean expResult = false;
        boolean result = ArrayExpressionParser.isNumeric(token);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }


    /**
     * Test of isVariable method, of class ArrayExpressionParser.
     */
    @Test
    public void testIsVariable_String() {
        System.out.println("isVariable");
        String token = "";
        ArrayExpressionParser instance = null;
        boolean expResult = false;
        boolean result = instance.isVariable(token);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of RPNtoDataArray method, of class ArrayExpressionParser.
     */
    @Test
    public void testRPNtoDataArray() throws Exception {
        System.out.println("RPNtoDataArray");
        String[] tokens = null;
        ArrayExpressionParser instance = null;
        DataArray expResult = null;
        DataArray result = instance.RPNtoDataArray(tokens);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of evaluateExpr method, of class ArrayExpressionParser.
     */
    @Test
    public void testEvaluateExpr() throws Exception {
        System.out.println("evaluateExpr");
        String expr = "";
        ArrayExpressionParser instance = null;
        DataArray expResult = null;
        DataArray result = instance.evaluateExpr(expr);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of preprocessExpr method, of class ArrayExpressionParser.
     */
    @Test
    public void testPreprocessExpr() {
        System.out.println("preprocessExpr");
        String input = "";
        String expResult = "";
        String result = ArrayExpressionParser.preprocessExpr(input);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of checkParentheses method, of class ArrayExpressionParser.
     */
    @Test
    public void testCheckParentheses() {
        System.out.println("checkParentheses");
        String s = "";
        ArrayExpressionParser.checkParentheses(s);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of listVariablesInUse method, of class ArrayExpressionParser.
     */
    @Test
    public void testListVariablesInUse() {
        System.out.println("listVariablesInUse");
        String[] vars = null;
        String[] expressions = null;
        String[] expResult = null;
        String[] result = ArrayExpressionParser.listVariablesInUse(vars, expressions);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}
