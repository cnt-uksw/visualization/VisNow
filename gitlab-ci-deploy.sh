#!/bin/bash

echo "Running VisNow installer deployment script..."
echo " "

#SHA of last deployed VisNow daily build is stored in the artifact file last_daily_build.
#last_daily_build file is updated in .gitlab-ci.yml script.
LAST_DAILY_BUILD="<none>"
if [ -f "last_daily_build" ]
then 
    LAST_DAILY_BUILD=`cat last_daily_build`
fi

echo "LAST_DAILY_BUILD=$LAST_DAILY_BUILD"
echo "CI_COMMIT_SHA=$CI_COMMIT_SHA"

#Deplyment is executed only if something changed in VisNow project (not dependancy projects) since latt successful deplyment.
#SHA of the current commit and SHA of last successful deployment commit are compared. 
#Last successful deployment commit SHA is stored in the artifact file last_daily_build.
echo " "
if [ $CI_COMMIT_SHA == $LAST_DAILY_BUILD ]
then
    echo "Deployment unncessary. Daily build up to date."
    echo " "
else
    echo "Deployment ncessary. Daily build outdated."
    echo " "

    echo "Building VisNow..."
    mvn dependency:copy-dependencies -Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=./.m2/repository -DoutputDirectory=./deps 
    mvn dependency:copy-dependencies -Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=./.m2/repository -DoutputDirectory=./deps -Dclassifier=sources
    mkdir -p ../JSciC/target
    cp ./deps/JSciC* ../JSciC/target/
    ls ../JSciC/target/
    mkdir -p ../JViA/target
    cp ./deps/JViA* ../JViA/target/
    ls ../JViA/target/
    ant jar -v -Dlibs.CopyLibs.classpath=/root/org-netbeans-modules-java-j2seproject-copylibstask.jar
    echo " "
    echo "Listing dist"
    ls -la dist
    echo " "
    echo "Listing dist/lib"
    ls -la dist/lib
    echo " "
    echo "Usage of dist"
    du -k dist
    echo " "

    echo "Building installer for Linux..."
    /root/installbuilder/bin/builder build ./visnow.xml linux-x64 --verbose --license /root/installbuilder/license/license_visnow_org.xml
    echo " "

    echo "Building installer for Windows..."
    /root/installbuilder/bin/builder build ./visnow.xml windows-x64 --verbose --license /root/installbuilder/license/license_visnow_org.xml
    echo " "

    echo "Building installer for macOS..."
    /root/installbuilder/bin/builder build ./visnow.xml osx --verbose --license /root/installbuilder/license/license_visnow_org.xml
    rm -rf installers/VisNow-*-installer.app
    echo " "

    ls -la installers
    du -k installers
    echo " "

    VER_DIR=`ls installers/VisNow-*-installer.run | cut -d'-' -f2-3`
    echo "Creating directory tree for installes in version ${VER_DIR}..."

    mkdir -p installers/${VER_DIR}/linux
    mv installers/VisNow-*-installer.run installers/${VER_DIR}/linux/

    mkdir -p installers/${VER_DIR}/windows
    mv installers/VisNow-*-installer.exe installers/${VER_DIR}/windows/

    mkdir -p installers/${VER_DIR}/macos
    mv installers/VisNow-*-installer.dmg installers/${VER_DIR}/macos/
    echo " "  
    
    echo "Uploading installers to FTP..."  
     ncftpput -R -v -u ${FTP_USER} -p ${FTP_PASSWORD} ${FTP_URL} /www/download/daily installers/${VER_DIR}
    echo " "
fi








