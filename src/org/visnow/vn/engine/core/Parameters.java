/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.engine.core;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.visnow.vn.geometries.parameters.ComponentColorMap;
import org.visnow.vn.datamaps.DefaultColorMap;
import org.visnow.vn.engine.exception.VNRuntimeException;
import org.visnow.vn.geometries.parameters.ColorComponentParams;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.Pick3DListener;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class Parameters implements Iterable<Parameter>, Cloneable, ParameterProxy
{
    private static final Logger LOGGER = Logger.getLogger(Parameters.class);

//    private static final boolean debug = false;// true;
    protected HashMap<String, Parameter> parameters;

    protected Parameters()
    {
        parameters = new HashMap<String, Parameter>();
    }

    public Parameters(ParameterEgg[] parameterEggs)
    {
        parameters = new HashMap<String, Parameter>();
        if (parameterEggs != null)
            for (ParameterEgg egg : parameterEggs) {
                parameters.put(egg.getName(), egg.hatch());
            }
    }

    /**
     * This constructor does not clone parameters!
     */
    public Parameters(Parameter[] parameters)
    {
        this.parameters = new HashMap<String, Parameter>();
        for (int i = 0; i < parameters.length; i++)
            this.parameters.put(parameters[i].getName(), parameters[i]);
    }

    public HashMap<String, Parameter> getParameters()
    {
        return parameters;
    }

    public Parameter getParameter(String name)
    {
        return parameters.get(name);
    }

    public Object getValue(String name)
    {
        return parameters.get(name).getValue();
    }

    /**
     * Returns parameter value. If value is an array, than array is cloned. Single element within an array is not cloned.
     * For primitives and String (which are immutable classes) it works just fine.
     */
    @Override
    synchronized public <T> T get(ParameterName<T> signature)
    {
        return (T) Parameter.cloneValue(parameters.get(signature.getName()).getValue());
    }

    @Override
    synchronized public <T> void set(ParameterName<T> signature, T value)
    {
        set(new ParameterName[]{signature}, new Object[]{value});
    }

    @Override
    synchronized public <T1, T2> void set(ParameterName<T1> signature1, T1 value1, ParameterName<T2> signature2, T2 value2)
    {
        set(new ParameterName[]{signature1, signature2}, new Object[]{value1, value2});
    }

    @Override
    synchronized public <T1, T2, T3> void set(ParameterName<T1> signature1, T1 value1, ParameterName<T2> signature2, T2 value2, ParameterName<T3> signature3, T3 value3)
    {
        set(new ParameterName[]{signature1, signature2, signature3}, new Object[]{value1, value2, value3});
    }

    @Override
    synchronized public <T1, T2, T3, T4> void set(ParameterName<T1> signature1, T1 value1, ParameterName<T2> signature2, T2 value2, ParameterName<T3> signature3, T3 value3, ParameterName<T4> signature4, T4 value4)
    {
        set(new ParameterName[]{signature1, signature2, signature3, signature4}, new Object[]{value1, value2, value3, value4});
    }

    /**
     * Similar to set(ParameterSignature<T>, T) but for many parameters at the same time.
     * This method fires fireParameterChanged only once (with name == null).
     * <p>
     * This method checks parameter classes only for first four parameters. The rest is processed using varargs.
     */
    @Override
    synchronized public <T1, T2, T3, T4> void set(ParameterName<T1> signature1, T1 value1, ParameterName<T2> signature2, T2 value2, ParameterName<T3> signature3, T3 value3, ParameterName<T4> signature4, T4 value4, Object... signatureAndValues)
    {
        if (signatureAndValues.length % 2 != 0)
            throw new IllegalArgumentException("Number of arguments needs to be even: " + Arrays.toString(signatureAndValues));

        int parameterCount = signatureAndValues.length / 2 + 4;
        ParameterName[] names = new ParameterName[parameterCount];
        Object[] values = new Object[parameterCount];

        names[0] = signature1;
        names[1] = signature2;
        names[2] = signature3;
        names[3] = signature4;

        values[0] = value1;
        values[1] = value2;
        values[2] = value3;
        values[3] = value4;

        for (int i = 0; i < signatureAndValues.length / 2; i++) {
            names[4 + i] = (ParameterName) signatureAndValues[2 * i];
            values[4 + i] = signatureAndValues[2 * i + 1];
        }

        set(names, values);
    }

    /**
     * Main setter for parameter name/value pairs.
     * This is the one that should be used internally - all others are shortcut for public access.
     * On the other hand, this setter doesn't test parameterName/value against type compatibility. Shortcut method do!
     * <p>
     * This method fires fireParameterChanged only once (with name == null or parameterName.name if only one parameter passed).
     * <p>
     * If value is an array (also multidimensional), than array is cloned. Single element within an array is not cloned.
     * For primitives and String (which are immutable classes) it works just fine.
     */
    synchronized protected void set(ParameterName[] names, Object[] values)
    {
        if (names.length != values.length) throw new IllegalArgumentException("Inconsistent parameter data: " + names.length + " != " + values.length);
        Set<String> nameSet = new HashSet<>();
        for (ParameterName name : names)
            if (nameSet.contains(name.getName())) throw new IllegalArgumentException("Parameter: " + name.getName() + " is doubled!");
            else nameSet.add(name.getName());

        for (int i = 0; i < names.length; i++)
            parameters.get(names[i].getName()).setValue(Parameter.cloneValue(values[i]));

        fireParameterChanged(names.length == 1 ? names[0].getName() : null);
    }

    //TODO: unchecked?
    @SuppressWarnings("unchecked")
    public void setValue(String name, Object value)
    {
        try {
            parameters.get(name).setValue(value);
        } catch (NullPointerException e) {
            //TODO: przechwycić?
            throw new VNRuntimeException(
                    200906171529L,
                    "NoSuchParameter: " + name,
                    null,
                    this,
                    Thread.currentThread());
        }
        fireParameterChanged(name);
    }

    //<editor-fold defaultstate="collapsed" desc=" Write XML ">
    public String writeXML()
    {
        String ret = "";
        XStream xstream = new XStream();
        xstream.allowTypesByRegExp(new String[] { ".*" });

        xstream.omitField(ColorComponentParams.class, "listener");
//        xstream.omitField(CustomizableColorMap.class, "map");
        xstream.omitField(ComponentColorMap.class, "renderEventListeners");
        xstream.omitField(DefaultColorMap.class, "propertyChangeSupport");

//        xstream.omitField(ColorComponentParams.class, "map");
        //XXX: trying to save presentation parameters (following hack saves parameters but doesn't restore them)
//        
//        xstream.omitField(DataMappingParams.class, "set");
//        xstream.omitField(DataMappingParams.class, "field");
////        xstream.omitField(DataMappingParams.class, "colorMap0Params");
////        xstream.omitField(DataMappingParams.class, "colorMap1Params");
////        xstream.omitField(DataMappingParams.class, "redParams");
////        xstream.omitField(DataMappingParams.class, "greenParams");
////        xstream.omitField(DataMappingParams.class, "blueParams");
////        xstream.omitField(DataMappingParams.class, "satParams");
////        xstream.omitField(DataMappingParams.class, "valParams");
////        xstream.omitField(DataMappingParams.class, "uParams");
////        xstream.omitField(DataMappingParams.class, "vParams");
//        xstream.omitField(DataMappingParams.class, "transparencyParams");
////        xstream.omitField(DataMappingParams.class, "textureImage");
////        xstream.omitField(DataMappingParams.class, "texture");
////        xstream.omitField(DataMappingParams.class, "colormapLegendParameters");
////        xstream.omitField(DataMappingParams.class, "colorCompChangeListener");
        for (Parameter parameter : this) {
            ret += "<parameter name=\"" + parameter.getName() + "\">\n";
            ret += encode(xstream.toXML(parameter.getValue()));
            ret += "</parameter>\n";
        }
        return ret;
    }

    public boolean readXML(String str)
    {
        try {
            tryReadXML(str);
            return true;
        } catch (ParserConfigurationException ex) {
            LOGGER.debug("parser exception"); //TODO: handling
        } catch (SAXException ex) {
            LOGGER.debug("sax exception");
        } catch (IOException ex) {
            LOGGER.debug("io exception");
        }
        return false;
    }

    public void tryReadXML(String str) throws ParserConfigurationException, SAXException, IOException
    {
        LOGGER.debug("READING!!!");
        XStream xstream = new XStream(new DomDriver());
        xstream.allowTypesByRegExp(new String[] { ".*" });
        String xml = "<?xml version=\'1.0\' encoding=\'utf-8\'?>\n<everything>" + str + "</everything>";

        Node main = DocumentBuilderFactory.newInstance().
                newDocumentBuilder().
                parse(new InputSource(new StringReader(xml))).
                getDocumentElement();

        Vector<Node> paramNodes = new Vector<Node>();
        NodeList nl = main.getChildNodes();

        LOGGER.debug("");
        for (int i = 0; i < nl.getLength(); ++i) {
            LOGGER.debug("testing node - [" + nl.item(i).getNodeName() + "]");
            if (nl.item(i).getNodeName().equalsIgnoreCase("parameter"))
                paramNodes.add(nl.item(i));
        }

        LOGGER.debug("");
        for (Node pn : paramNodes) {
            LOGGER.debug("getting node - [" + pn.getAttributes().getNamedItem("name").getNodeValue() + "]");
            String par = pn.getAttributes().getNamedItem("name").getNodeValue();
            String val = decode(pn.getTextContent());
            try {
                Object value = xstream.fromXML(val);
                LOGGER.debug("  val=[" + val + "]");
                if (value != null) {
                    LOGGER.debug("  val=[" + value.toString() + "]");
                    if (parameters.containsKey(par)) setValue(par, value);
                    else LOGGER.warn("Parameter: " + par + " does not exists! Value: " + value);
                }
            } catch (Exception e) {
                //XXX: should be moved to XStream 1.3 and XStreamException
                LOGGER.error("Cannot decode parameter: " + par, e);
            }
        }

        LOGGER.debug("");
        LOGGER.debug("READ DONE!!!");
        LOGGER.debug("");
    }

    private static String decode(String in)
    {
        String ret = "";
        StringTokenizer tokenizer = new StringTokenizer(in, "[]|", true);
        while (tokenizer.hasMoreTokens()) {
            String next = tokenizer.nextToken();
            if (next.equals("[")) {
                ret += "<";
                continue;
            }
            if (next.equals("]")) {
                ret += ">";
                continue;
            }
            if (next.equals("|")) {
                next = tokenizer.nextToken();
                switch (next.charAt(0)) {
                    case '{':
                        ret += "[";
                        break;
                    case '}':
                        ret += "]";
                        break;
                    case '+':
                        ret += "|";
                        break;
                }
                ret += next.substring(1);
                continue;
            }
            ret += next;
        }
        return ret;
    }

    private static String encode(String in)
    {
        String ret = "";
        StringTokenizer tokenizer = new StringTokenizer(in, "<>[]|", true);
        while (tokenizer.hasMoreTokens()) {
            String next = tokenizer.nextToken();
            if (next.equals("|")) {
                ret += "|+";
                continue;
            }
            if (next.equals("<")) {
                ret += "[";
                continue;
            }
            if (next.equals(">")) {
                ret += "]";
                continue;
            }
            if (next.equals("[")) {
                ret += "|{";
                continue;
            }
            if (next.equals("]")) {
                ret += "|}";
                continue;
            }
            ret += next;
        }
        return ret;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Active ">
    /**
     * A flag indicating if state change will be forwarded to listeners.
     */
    protected boolean active = true;

    /**
     * Returns whether state change will be forwarded to listeners.
     * <p>
     * @return
     */
    public boolean isActive()
    {
        return active;
    }

    /**
     * Set value of
     * <code>active</code> and fire fireStateChanged().
     * <p/>
     * @param active
     */
    public void setActive(boolean active)
    {
        this.active = active;
        fireStateChanged();
    }

    public void setActiveValue(boolean active)
    {
        this.active = active;
    }

    /**
     * A flag indicating if parameter change will be forwarded to listeners.
     */
    protected boolean parameterActive = true;

    /**
     * Returns whether parameter change will be forwarded to listeners.
     * <p>
     * @return
     */
    public boolean isParameterActive()
    {
        return parameterActive;
    }

    /**
     * Set value of
     * <code>active</code>.
     * <p/>
     * @param active
     */
    public void setParameterActive(boolean active)
    {
        this.parameterActive = active;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Iterator ">
    @Override
    public Iterator<Parameter> iterator()
    {
        return parameters.values().iterator();
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Parameter change listeners ">
    
    /**
     * Utility field holding list of listeners of particular parameters change events
     */
    private Vector<ParameterChangeListener> listeners = new Vector<ParameterChangeListener>();
    
    /**
     * Registers parameter change listener to receive events.
     * <p/>
     * @param listener The listener to register.
     */
    public synchronized void addParameterChangelistener(ParameterChangeListener listener)
    {
        listeners.add(listener);
    }

    /**
     * Removes parameter change listener from the list.
     * <p/>
     * @param listener The listener to remove.
     */
    public synchronized void removeParameterChangeListener(ParameterChangeListener listener)
    {
        listeners.remove(listener);
    }

    /**
     * Clears ChangeListenerList.
     * <p/>
     */
    public synchronized void clearParameterChangeListeners()
    {
        listeners.clear();
    }

    public void fireParameterChanged(String parameter)
    {
        if (parameterActive)
            for (ParameterChangeListener listener : listeners)
                listener.parameterChanged(parameter);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Know Change listeners ">
    /**
     * Utility field holding list of global ChangeListeners.
     */
    protected transient ArrayList<ChangeListener> changeListenerList = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     * <p/>
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * <p/>
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Clears ChangeListenerList.
     * <p/>
     */
    public synchronized void clearChangeListeners()
    {
        changeListenerList.clear();
    }

    /**
     * Notifies all registered listeners about the event (calls
     * <code>stateChanged()</code> on each listener in
     * <code>changeListenerList</code>).
     */
    public void fireStateChanged()
    {
        if (active) {
            ChangeEvent e = new ChangeEvent(this);
            for (int i = 0; i < changeListenerList.size(); i++) {
                changeListenerList.get(i).stateChanged(e);
            }
        }
    }

    //</editor-fold>
    /**
     * To be overriden by Params in each module that want to handle pick 3D.
     * <p>
     * @return null - no Pick3DListener
     * <p>
     * @see org.visnow.vn.lib.basic.mappers.FieldSlicePlane.FieldSlicePlane#pick3DListener FieldSlicePlane.pick3DListener - how to use it
     */
    public Pick3DListener getPick3DListener()
    {
        return null;
    }

    @Override
    public Parameters clone()
    {
        Parameters p = new Parameters();

        for (Map.Entry<String, Parameter> entry : parameters.entrySet()) {
            String string = entry.getKey();
            Parameter parameter = entry.getValue();

            p.parameters.put(string, parameter.clone());
        }
        return p;
    }

    synchronized public Parameters getReadOnlyClone()
    {
        return new Parameters()
        {
            {
                for (Map.Entry<String, Parameter> entry : Parameters.this.parameters.entrySet())
                    parameters.put(entry.getKey(), entry.getValue().clone());
            }

            @Override
            protected synchronized void set(ParameterName[] names, Object[] values)
            {
                throw new UnsupportedOperationException("These parameters are read only. Check the flow!");
            }
        };
    }

    /**
     * Creates passive clone of these parameters - no listener is attached.
     * <p>
     * @return cloned parameters
     */
    synchronized public Parameters getPassiveClone()
    {
        List<Parameter> clonedParameters = new ArrayList<>(parameters.size());
        for (Parameter parameter : parameters.values())
            clonedParameters.add(new Parameter(parameter.getName(), Parameter.cloneValue(parameter.getValue())));

        return new Parameters(clonedParameters.toArray(new Parameter[clonedParameters.size()]));
    }
}
