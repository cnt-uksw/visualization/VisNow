/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.engine.core;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class Parameter<E> implements Cloneable
{

    protected String name;
    protected E value;
    protected ParameterType type;
    private final Class valueClass;

    //XXX: apparently not used in VN
//    private Vector<Widget<E>> widgets;
    public static class ParameterSerializer implements JsonSerializer<Parameter>
    {

        @Override
        public JsonElement serialize(Parameter src, Type typeOfSrc, JsonSerializationContext context)
        {
            JsonArray ja = new JsonArray();
            ja.add(new JsonPrimitive(src.getName()));
            ja.add(new JsonPrimitive(src.getValueClass().getName()));
            ja.add(context.serialize(src.getValue()));
            return ja;
        }
    }

    public static class ParameterDeserializer implements JsonDeserializer<Parameter>
    {

        @Override
        public Parameter deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
        {

            String name = context.deserialize(json.getAsJsonArray().get(0), String.class);
            Class valueClass;
            String className = null;
            try {
                className = (String) context.deserialize(json.getAsJsonArray().get(1), String.class);
                valueClass = Class.forName(className);
            } catch (ClassNotFoundException ex) {
                throw new RuntimeException("Problem with class casting [" + className + "]", ex);
            }
            Object value = context.deserialize(json.getAsJsonArray().get(2), valueClass);

            return new Parameter(valueClass, name, value, ParameterType.independent);
        }

    }

    public String getName()
    {
        return name;
    }

    public ParameterType getType()
    {
        return type;
    }

    public Parameter(String name, E value) 
    {
        this(name, value, ParameterType.independent);
    }
    
    //TODO: this constructor should be removed to avoid setting valueClass to null
    public Parameter(String name, E value, ParameterType type)
    {
        this.name = name;
        this.value = value;
        this.type = type;
        this.valueClass = null;
//        this.widgets = new Vector<Widget<E>>();
    }

    public Parameter(Class<E> valueClass, String name, E value, ParameterType type)
    {
        this.name = name;
        this.value = value;
        this.type = type;
        this.valueClass = valueClass;
        if (valueClass == null)
            throw new NullPointerException("null valueClass is not allowed here. Use old Parameter(name, value, type) instead.");
//        this.widgets = new Vector<Widget<E>>();
    }

    public Parameter(Class<E> valueClass, ParameterName<E> signature, E value)
    {
        this.name = signature.getName();
        this.value = value;
        //TODO: this should be removed
        this.type = ParameterType.independent;
        //XXX: try to find out if this is really necessary
        this.valueClass = valueClass;
        if (valueClass == null)
            throw new NullPointerException("null valueClass is not allowed here. Use old Parameter(name, value, type) instead.");
    }

    public Parameter(ParameterName<E> signature, E value)
    {
        this.name = signature.getName();
        this.value = value;
//        this.valueClass = null;
        //TODO: this should be removed
        this.type = ParameterType.independent;
        this.valueClass = null;
    }

    public E getValue()
    {
        return value;
    }

    public void setValue(E value)
    {
        this.value = value;
//
//        for (Widget<E> widget : widgets) {
//            widget.lock(true);
//            widget.setValue(value);
//            widget.lock(false);
//        }
    }

//    private void setValue(E value, Widget w)
//    {
//        this.value = value;
//
//        for (Widget<E> widget : widgets) {
//            if (widget.equals(w))
//                continue;
//            widget.lock(true);
//            widget.setValue(value);
//            widget.lock(false);
//        }
//    }
    public Class getValueClass()
    {
        if (valueClass == null)
            throw new NullPointerException("This parameter has null valueClass. Use Parameter(valueClass, name, value, type) to create parameter with non-null valueClass.");
        return valueClass;
    }

    /**
     * Clone arrays but not array elements
     */
    public static <T> T cloneValue(T val)
    {
        if (val == null) return null;
        else {
            if (val.getClass().isArray()) {
                Class c = val.getClass().getComponentType();
                if (c.isPrimitive()) {

                    if (c == Byte.TYPE) return (T) ((byte[]) val).clone();
                    if (c == Short.TYPE) return (T) ((short[]) val).clone();
                    if (c == Integer.TYPE) return (T) ((int[]) val).clone();
                    if (c == Long.TYPE) return (T) ((long[]) val).clone();
                    if (c == Float.TYPE) return (T) ((float[]) val).clone();
                    if (c == Double.TYPE) return (T) ((double[]) val).clone();
                    if (c == Boolean.TYPE) return (T) ((boolean[]) val).clone();
                    if (c == Character.TYPE) return (T) ((char[]) val).clone();
                    else throw new IllegalStateException("There should be no other primitives.");
                } else {
                    Object[] clonedArray = ((Object[]) val).clone();
                    for (int i = 0; i < clonedArray.length; i++)
                        clonedArray[i] = cloneValue(((Object[]) val)[i]);
                    return (T) clonedArray;
                }
            } else {
//            if (val.getClass().isPrimitive() || val.getClass().equals(String.class)) return val;
//            else 
                return val; //cant clone other objects .... :/
//(T) (((Object)val).clone());
            }
        }
    }

    @Override
    public Parameter<E> clone()
    {
        try {
            Parameter<E> p = (Parameter<E>) super.clone();
            //FIXME: there should be no null parameters
            if (p.value != null)
                p.value = cloneValue(p.value);
            return p;
        } catch (CloneNotSupportedException ex) {
            throw new IllegalStateException("Should never occur");
        }
    }
}
