/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.engine.core;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Progress agent which can work as a progress proxy between progress producer and progress consumer (through abstract updateProgress method).
 * This progress agent can work in two modes:
 * - setting progress using float value in range 0 ... 1
 * - setting progress by increasing current step (up to total number of steps).
 * <p>
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public abstract class ProgressAgent
{
    private final long totalSteps;
    protected AtomicLong currentStep = new AtomicLong(0);

    public ProgressAgent(long totalSteps)
    {
        this.totalSteps = totalSteps;
        if (totalSteps <= 0) throw new IllegalArgumentException("Total steps must be positive");
    }

    public ProgressAgent()
    {
        this.totalSteps = Long.MAX_VALUE;
    }

    public long getTotalSteps()
    {
        return totalSteps;
    }

    public void setProgress(double progress)
    {
        setProgressStep(Math.max(0l, Math.min(totalSteps, Math.round(progress * totalSteps))));
    }

    public void setProgressStep(long step)
    {
        currentStep.set(Math.max(0l, Math.min(totalSteps, step)));
        updateProgress();
    }

    public void increase()
    {
        increase(1);
    }

    /**
     * @param step positive number of steps to increase.
     */
    public void increase(long step)
    {
        currentStep.set(Math.min(totalSteps, currentStep.get() + step));
        updateProgress();
    }

    protected double getProgress()
    {
        return (double) currentStep.get() / totalSteps;
    }

    protected abstract void updateProgress();

    public static ProgressAgent getDummyAgent()
    {
        return new ProgressAgent()
        {
            @Override
            protected void updateProgress()
            {
            }
        };
    }

    /**
     * Creates sub-agent, which is part of this agent; Its {@link #totalSteps} &le; this.{@link #totalSteps}; Steps of both agents are mapped in 1:1 ratio.
     * One can use {@link #totalSteps} as a total number of steps to increase (no matter what is {@link #totalSteps} of parent agent}.
     * This is useful for multi threaded computations.
     * <p>
     * Note: this sub-agent doesn't support "going back" so every call to {@link #setProgress(double)} and {@link #setProgressStep(long)} must increase
     * {@link #totalSteps} value (or at least leave it same as before operation).
     * <p>
     * @param agentSteps positive number of steps to get from this agent.
     * <p>
     * @throws IllegalArgumentException if incorrect value is passed to {@link #setProgress(double)} and {@link #setProgressStep(long)}.
     */
    public ProgressAgent getSubAgent(long agentSteps)
    {
        if (agentSteps > this.totalSteps) throw new IllegalArgumentException("Incorrect step number: " + agentSteps + " > " + totalSteps);

        final ProgressAgent parentAgent = this;
        return new ProgressAgent(agentSteps)
        {
            long previousStep = 0;

            @Override
            protected void updateProgress()
            {
                long increaseStep = this.currentStep.get() - previousStep;
                if (increaseStep < 0) throw new IllegalArgumentException("CurrentStep decreased! Such operation in sub-agent is not supported");
                previousStep += increaseStep;
                parentAgent.increase(increaseStep);
            }
        };
    }
}
