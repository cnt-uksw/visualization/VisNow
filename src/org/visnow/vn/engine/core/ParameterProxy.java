/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.engine.core;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public interface ParameterProxy
{
    //trying to simplify runButton usage 
    //trying to make better abstraction for current Parameter mechanism.

    public <T> T get(ParameterName<T> signature);

    /**
     * If value is an array (also multidimensional), than array should be cloned. Single element within an array is not cloned.
     * For primitives and String (which are immutable classes) it works just fine.
     * <p>
     * Assuming that every set is one call - that's why these methods should be renamed to "send".
     * This is to avoid multiple call on one GUI action.
     */
    public <T> void set(ParameterName<T> signature, T value);

    public <T1, T2> void set(ParameterName<T1> signature1, T1 value1, ParameterName<T2> signature2, T2 value2);

    public <T1, T2, T3> void set(ParameterName<T1> signature1, T1 value1, ParameterName<T2> signature2, T2 value2, ParameterName<T3> signature3, T3 value3);

    public <T1, T2, T3, T4> void set(ParameterName<T1> signature1, T1 value1, ParameterName<T2> signature2, T2 value2, ParameterName<T3> signature3, T3 value3, ParameterName<T4> signature4, T4 value4);

    public <T1, T2, T3, T4> void set(ParameterName<T1> signature1, T1 value1, ParameterName<T2> signature2, T2 value2, ParameterName<T3> signature3, T3 value3, ParameterName<T4> signature4, T4 value4, Object... signatureAndValues);
}
