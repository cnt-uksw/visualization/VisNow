/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.engine;

import org.visnow.vn.engine.core.ParameterProxy;

/**
 * This class is a simple try to make better (yet very simple) abstraction between user interfaces and computational core of modules.
 * User interfaces considered here are: standard GUI, Viewers as interface to modules, batch mode.
 * <p>
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public interface CoreUI
{

    /**
     * This method makes (possible) connection between UI and computational core of module.
     * @param targetParameterProxy 
     */
    public void setParameterProxy(ParameterProxy targetParameterProxy);

    /**
     * XXX: This should be resolved in some other way - possibly also by using parameterProxy - to avoid direct calls from logic to UI (and vice versa)
     * 
     * This is update method for UI - needed for consistency between module state and UI state.
     * @param p (read only) parameter proxy necessary for update - main assumption here is that parameters within this parameter proxy are correct, consistent 
     * and can be (and should be) pushed directly into corresponding GUI controls
     * @param resetGUIOnlyControls if true than controls that are not directly bound to module parameters should be reset as well 
     * (such controls may be considered as additional 'GUI parameters')
     * @param setRunButtonPending if true than run button should be set into pending state (as when new input field arrives)
     */
    public void updateGUI(final ParameterProxy p, boolean resetGUIOnlyControls, boolean setRunButtonPending);

}
