/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.application.area.widgets;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.io.File;
import java.nio.file.FileSystemException;
import java.util.Vector;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.visnow.vn.application.area.Quad;
import org.visnow.vn.application.area.SelectableAreaItem;
import org.visnow.vn.engine.core.Link;
import org.visnow.vn.lib.basic.writers.FieldWriter.FieldWriter;
import org.visnow.vn.lib.basic.writers.FieldWriter.FieldWriterFileFormat;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.utils.io.VisNowFieldWriter;
import org.visnow.vn.system.framework.MainWindow;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.swing.VNSwingUtils;
import org.visnow.vn.system.swing.filechooser.VNFileChooser;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.system.utils.usermessage.UserMessage;
import org.visnow.vn.lib.basic.utilities.Clipboard.Clipboard;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class LinkPanel extends JComponent implements SelectableAreaItem
{

    //<editor-fold defaultstate="collapsed" desc=" [VAR] Link ">
    private Link link;

    public Link getLink()
    {
        return link;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" [VAR] Ports ">
    private PortPanel outputPanel;
    private PortPanel inputPanel;

    public PortPanel getInputPanel()
    {
        return inputPanel;
    }

    public PortPanel getOutputPanel()
    {
        return outputPanel;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" [VAR] Lines ">
    private Vector<LinkPanelRectangle> lines;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc=" [VAR] Selection ">
    private boolean selected;
    private int centerX, centerY;

    public String getModuleForSelecting()
    {
        return null;
    }

    @Override
    public boolean isSelected()
    {
        return selected;
    }

    @Override
    public void setSelected(boolean b)
    {
        selected = b;
        repaint();
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" [VAR] Menu ">
    private javax.swing.JMenuItem mbRemove;
    private javax.swing.JMenuItem mbShowContent;
    private javax.swing.JPopupMenu popupMenu;
    private javax.swing.JMenuItem mbSaveDataBinary;
    private javax.swing.JMenuItem mbSaveDataAscii;
    private javax.swing.JMenuItem mbCopyToClipboard;
    private javax.swing.JMenuItem mbSaveDataSerialized;

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Painting, Collision ">
    @Override
    public boolean isRectangled(Quad q)
    {
        for (LinkPanelRectangle r : lines) {
            if (r.isRectangled(q)) {
                return true;
            }
        }
        return false;
    }

    public boolean isHit(Point p, int radiusMax)
    {
        for (int radius = 0; radius < radiusMax; radius++)
            for (int x = -radius; x <= radius; x++)
                for (int y = -radius; y <= radius; y++)
                    for (LinkPanelRectangle r : lines)
                        if (r.contains(new Point(p.x + x, p.y + y)))
                            return true;

        return false;
    }

    @Override
    public void paint(Graphics g)
    {
        Graphics2D gg = (Graphics2D) g;
        int fromX = outputPanel.getTotalX() + 8;
        int fromY = outputPanel.getTotalY() + 4;

        int toX = inputPanel.getTotalX() + 8;
        int toY = inputPanel.getTotalY() + 6;

        centerX = (fromX + toX) / 2;
        centerY = (fromY + toY) / 2;

        int secX = fromX;
        int secY = fromY + 6;
        int lasX = toX;
        int lasY = (this.inputPanel != null) ? toY - 6 : toY - 15;
        int[] x;
        int[] y;

        lines = new Vector<LinkPanelRectangle>();

        if (secY < lasY) {
            int midY = (secY + lasY) / 2;

            x = new int[]{fromX, secX, secX, lasX, lasX, toX};
            y = new int[]{fromY, secY, midY, midY, lasY, toY};
            for (int i = 0; i < 5; ++i)
                lines.add(new LinkPanelRectangle(x[i], y[i], x[i + 1], y[i + 1], 2));

        } else {
            int midX = (secX + lasX) / 2;
            x = new int[]{fromX, secX, midX, midX, lasX, toX};
            y = new int[]{fromY, secY, secY, lasY, lasY, toY};
            for (int i = 0; i < 5; ++i)
                lines.add(new LinkPanelRectangle(x[i], y[i], x[i + 1], y[i + 1], 2));
        }
        gg.addRenderingHints(VNSwingUtils.getHints());
        if (selected) {
            gg.setColor(new java.awt.Color(255, 255, 204, 153));
            gg.setStroke(new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1.0f,
                                         new float[]{8, 8}, 8));
            gg.drawPolyline(x, y, 6);
        }

        gg.setColor(VNSwingUtils.typeColor(getLink().getOutput().getType().getName()));
        gg.setStroke(new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        gg.drawPolyline(x, y, 6);
        gg.setColor(VNSwingUtils.typeColor(getLink().getOutput().getType().getName()));
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" [Constructor] ">
    /**
     * Creates new instance of LinkConnectingPanel
     */
    public LinkPanel(Link link, PortPanel output, PortPanel input)
    {
        this.outputPanel = output;
        this.inputPanel = input;
        this.link = link;
        lines = new Vector<LinkPanelRectangle>();
        selected = false;
        popupMenu = new javax.swing.JPopupMenu();

        mbRemove = new javax.swing.JMenuItem();
        mbRemove.setText("Remove");
        mbRemove.addActionListener(new java.awt.event.ActionListener()
        {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                menuRemove();
            }
        });
        popupMenu.add(mbRemove);

        popupMenu.add(new JPopupMenu.Separator());

        mbShowContent = new javax.swing.JMenuItem();
        mbShowContent.setText("Show content");
        mbShowContent.addActionListener(new java.awt.event.ActionListener()
        {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                menuShowContent();
            }
        });
        popupMenu.add(mbShowContent);

        popupMenu.add(new JPopupMenu.Separator());

        mbCopyToClipboard = new javax.swing.JMenuItem();
        mbCopyToClipboard.setText("Copy to clipboard");
        mbCopyToClipboard.addActionListener(new java.awt.event.ActionListener()
        {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                Object obj = getLink().getOutput().getValue();
                if (obj == null || !(obj instanceof VNField))
                    return;
                Clipboard.add((String)JOptionPane.showInputDialog(null, "Copy to clipboard as", ((VNField)obj).getField().getName()),
                              ((VNField)obj).getField() );
            }
        });
        popupMenu.add(mbCopyToClipboard);

        popupMenu.add(new JPopupMenu.Separator());

        mbSaveDataSerialized = new javax.swing.JMenuItem();
        mbSaveDataSerialized.setEnabled(false);
        mbSaveDataSerialized.setText("Write field...");
        mbSaveDataSerialized.addActionListener(new java.awt.event.ActionListener()
        {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                menuSaveField(true, false);
            }
        });
        popupMenu.add(mbSaveDataSerialized);

        mbSaveDataBinary = new javax.swing.JMenuItem();
        mbSaveDataBinary.setEnabled(false);
        mbSaveDataBinary.setText("Write VNF (binary)...");
        mbSaveDataBinary.addActionListener(new java.awt.event.ActionListener()
        {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                menuSaveField(false, false);
            }
        });

        popupMenu.add(mbSaveDataBinary);
        mbSaveDataAscii = new javax.swing.JMenuItem();
        mbSaveDataAscii.setEnabled(false);
        mbSaveDataAscii.setText("Write VNF (ASCII)...");
        mbSaveDataAscii.addActionListener(new java.awt.event.ActionListener()
        {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                menuSaveField(false, true);
            }
        });
        popupMenu.add(mbSaveDataAscii);

        this.add(new JLabel("INIT"));
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Menu ">
    public void showMenu(Point p)
    {
        Object obj = getLink().getOutput().getValue();
        mbCopyToClipboard.setEnabled(obj != null && obj instanceof VNField);
        mbSaveDataSerialized.setEnabled(obj != null && obj instanceof VNField);
        mbSaveDataBinary.setEnabled(obj != null && obj instanceof VNField);
        mbSaveDataAscii.setEnabled(obj != null && obj instanceof VNField);
        popupMenu.show(this, (int) p.getX(), (int) p.getY());
    }

    //</editor-fold>
    private void menuRemove()
    {
        getOutputPanel()
            .getModulePanel()
            .getAreaPanel()
            .getArea()
            .getOutput()
            .deleteLink(getLink().getName());
        //.getApplication()
        //.getReceiver()
        //.receive(new LinkDeleteCommand(getLink().getName()));
    }

    public void menuShowContent()
    {
        MainWindow.getInfoFrame().showRefreshingContent(getLocationOnScreen().x, getLocationOnScreen().y, getLink().getOutput());
    }

    private void menuSaveField(boolean asSerialized, boolean asAscii)
    {
        Object obj = getLink().getOutput().getValue();
        if ((obj instanceof VNField))
           VisNowFieldWriter.writeInvokedFromPortOrConnection(((VNField) obj).getField(), asAscii, asSerialized);
    }
}
