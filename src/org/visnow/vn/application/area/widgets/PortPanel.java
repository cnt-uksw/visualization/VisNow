/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.application.area.widgets;

import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu.Separator;
import org.visnow.vn.engine.core.Input;
import org.visnow.vn.engine.core.Output;
import org.visnow.vn.engine.element.ElementSaturationListener;
import org.visnow.vn.engine.main.InputSaturation;
import org.visnow.vn.engine.main.OutputSaturation;
import org.visnow.vn.engine.main.Port;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.utils.io.VisNowFieldWriter;
import org.visnow.vn.system.framework.MainWindow;
import org.visnow.vn.system.swing.VNSwingUtils;
import org.visnow.vn.lib.basic.utilities.Clipboard.Clipboard;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class PortPanel extends javax.swing.JComponent
{

    //<editor-fold defaultstate="collapsed" desc=" PopupMenu ">
    private javax.swing.JMenuItem mbShowFullInfo;
    private javax.swing.JMenuItem mbShowContent;
    private javax.swing.JMenuItem mbCopyToClipboard;
    private javax.swing.JMenuItem mbSaveBinaryData;
    private javax.swing.JMenuItem mbSaveASCIIData;
    private javax.swing.JMenuItem mbSaveSerializedData;
    private javax.swing.JPopupMenu popupMenu;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" ModulePanel ">
    private ModulePanel modulePanel;

    public ModulePanel getModulePanel()
    {
        return modulePanel;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Coords ">
    public int getTotalX()
    {
        return getX() + getModulePanel().getX();
    }

    public int getTotalY()
    {
        return getY() + getModulePanel().getY();
    }

    public int getDX()
    {
        return 8;
    }

    public int getDY()
    {
        return (getPort().isInput()) ? 6 : 4;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Port ">
    private Port port;

    public Port getPort()
    {
        return port;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" ShowMenu ">
    public void showMenu()
    {
       if (mbCopyToClipboard != null)
            mbCopyToClipboard.setEnabled(port != null &&
                port instanceof Output &&
                ((Output) port).getData() != null &&
                ((Output) port).getValue() instanceof VNField);
        if (mbSaveSerializedData != null)
            mbSaveSerializedData.setEnabled(port != null &&
                port instanceof Output &&
                ((Output) port).getData() != null &&
                ((Output) port).getValue() instanceof VNField);
        if (mbSaveBinaryData != null)
            mbSaveBinaryData.setEnabled(port != null &&
                port instanceof Output &&
                ((Output) port).getData() != null &&
                ((Output) port).getValue() instanceof VNField);
        if (mbSaveASCIIData != null)
            mbSaveASCIIData.setEnabled(port != null &&
                port instanceof Output &&
                ((Output) port).getData() != null &&
                ((Output) port).getValue() instanceof VNField);
        popupMenu.show(this, 10, 10);
    }

    protected void showContentWindow()
    {
        if (getPort() instanceof Output)
            MainWindow.getInfoFrame().showRefreshingContent(getLocationOnScreen().x, getLocationOnScreen().y, (Output) getPort());
    }

    protected void showDetailedInfoWindow()
    {
        MainWindow.getInfoFrame().showPortDetailedInfo(getLocationOnScreen().x, getLocationOnScreen().y, getPort());
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" [CONSTRUCTOR] ">
    /**
     * Creates new form PortPanel
     */
    public PortPanel(ModulePanel module, Port port)
    {

        this.modulePanel = module;
        this.port = port;

        popupMenu = new javax.swing.JPopupMenu();
        popupMenu.add(new Separator());
        mbShowFullInfo = new javax.swing.JMenuItem();
        mbShowFullInfo.setText("Show detailed info");
        mbShowFullInfo.addActionListener(new java.awt.event.ActionListener()
        {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                showDetailedInfoWindow();
            }
        });
        popupMenu.add(mbShowFullInfo);

        if (!this.port.isInput()) {
            mbShowContent = new javax.swing.JMenuItem();
            mbShowContent.setText("Show content");
            mbShowContent.addActionListener(new java.awt.event.ActionListener()
            {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt)
                {
                    showContentWindow();
                }
            });
            popupMenu.add(mbShowContent);

            popupMenu.add(new Separator());

            mbCopyToClipboard = new javax.swing.JMenuItem();
            mbCopyToClipboard.setText("Copy to clipboard");
            mbCopyToClipboard.addActionListener(new java.awt.event.ActionListener()
            {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt)
                {
                    if (getPort() instanceof Output) {
                        Object obj = ((Output) getPort()).getValue();
                        if (!(obj instanceof VNField))
                            return;
                        Clipboard.add((String)JOptionPane.showInputDialog(null, "Copy to clipboard as", ((VNField)obj).getField().getName()),
                                     ((VNField)obj).getField() );
                    }
                }
            });
            popupMenu.add(mbCopyToClipboard);

            popupMenu.add(new Separator());

            mbSaveSerializedData = new javax.swing.JMenuItem();
            mbSaveSerializedData.setText("Write field...");
            mbSaveSerializedData.addActionListener(new java.awt.event.ActionListener()
            {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt)
                {
                    if (getPort() instanceof Output) {
                        menuSaveField(true, false);
                    }
                }
            });
            mbSaveSerializedData.setEnabled(false);
            popupMenu.add(mbSaveSerializedData);

            mbSaveBinaryData = new javax.swing.JMenuItem();
            mbSaveBinaryData.setText("Write VNF (binary)...");
            mbSaveBinaryData.addActionListener(new java.awt.event.ActionListener()
            {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt)
                {
                    if (getPort() instanceof Output) {
                        menuSaveField(false, false);
                    }
                }
            });
            mbSaveBinaryData.setEnabled(false);
            popupMenu.add(mbSaveBinaryData);

            mbSaveASCIIData = new javax.swing.JMenuItem();
            mbSaveASCIIData.setText("Write VNF (ASCII)...");
            mbSaveASCIIData.addActionListener(new java.awt.event.ActionListener()
            {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt)
                {
                    if (getPort() instanceof Output) {
                        menuSaveField(false, true);
                    }
                }
            });
            mbSaveASCIIData.setEnabled(false);
            popupMenu.add(mbSaveASCIIData);

        }

        if (!this.port.isInput()) {
            // TODO: add Attach menu;
            //popupMenu.add(module.getScenePanel().getTypeMenu().getMenu(this));
        }

        this.addMouseListener(new PortPanelMouseEvents(this));
        this.addMouseMotionListener(new PortPanelMouseEvents(this));
        port.addSaturationListener(new ElementSaturationListener()
        {
            @Override
            public void saturationChanged()
            {
                java.awt.EventQueue.invokeLater(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        repaint();
                    }
                });
            }
        });
    }

    private void menuSaveField(boolean asSerialized, boolean asAscii)
    {
        Object obj = ((Output) getPort()).getValue();
        if ((obj instanceof VNField))
           VisNowFieldWriter.writeInvokedFromPortOrConnection(((VNField) obj).getField(), asAscii, asSerialized);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Paint ">
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        Graphics2D gg = (Graphics2D) g;
        gg.addRenderingHints(VNSwingUtils.getHints());
        if (getPort().isInput()) {
            gg.setColor(VNSwingUtils.typeColor(getPort().getType().getName()));
            if (((Input) getPort()).isNecessary())
                gg.fillPolygon(new int[]{0, 8, 9, 17}, new int[]{2, 11, 11, 2}, 4);
            else
                gg.fillPolygon(new int[]{0, 8, 9, 17}, new int[]{5, 11, 11, 5}, 4);

            if (((Input) getPort()).isNecessary() && ((Input) getPort()).getInputSaturation() != InputSaturation.ok) {
                switch (((Input) getPort()).getInputSaturation()) {
                    case notLinked:
                        gg.setColor(VNSwingUtils.SATURATION_NOTLINKED);
                        break;
                    case noData:
                        gg.setColor(VNSwingUtils.SATURATION_NODATA);
                        break;
                    case wrongData:
                        gg.setColor(VNSwingUtils.SATURATION_WRONGDATA);
                        break;
                }
                gg.drawLine(1, 2, 16, 2);
            }

        } else {
            gg.setColor(VNSwingUtils.typeColor(getPort().getType().getName()));
            if (((Output) getPort()).getOutputSaturation() == OutputSaturation.noData)
                gg.fillRect(0, 0, 17, 4);
            else
                gg.fillPolygon(new int[]{0, 0, 8, 9, 17, 17}, new int[]{0, 3, 11, 11, 3, 0}, 6);

        }
    }

    private JMenu attachedMenu = null;

    public void setAttachMenu(JMenu menu)
    {

        if (attachedMenu != null)
            popupMenu.remove(attachedMenu);
        attachedMenu = menu;
        if (attachedMenu != null)
            popupMenu.add(menu, 0);

    }

    /**
     * @return the popupMenu
     */
    public javax.swing.JPopupMenu getPopupMenu()
    {
        return popupMenu;
    }
}
