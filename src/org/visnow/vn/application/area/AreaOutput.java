/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.application.area;

import java.awt.Point;
import org.visnow.vn.application.area.widgets.LinkPanel;
import org.visnow.vn.application.area.widgets.ModulePanel;
import org.visnow.vn.engine.core.CoreName;
import org.visnow.vn.engine.commands.LinkAddCommand;
import org.visnow.vn.engine.commands.LinkDeleteCommand;
import org.visnow.vn.engine.commands.ModuleDeleteCommand;
import org.visnow.vn.engine.commands.ModuleRenameCommand;
import org.visnow.vn.engine.core.LinkName;
import org.visnow.vn.engine.main.ModuleBox;
import org.visnow.vn.engine.main.Port;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class AreaOutput
{

    private Area area;

    protected Area getArea()
    {
        return area;
    }

    protected AreaOutput(Area area)
    {
        this.area = area;
    }

    public void deleteModule(final String name, final CoreName coreName, final Point location)
    {
        final Area thisArea = area;
        if (!area.getApplication().isLockupBusy())
            new Thread(new Runnable()
            {
                public void run()
                {
                    thisArea
                            .getApplication()
                            .getReceiver()
                            .receive(new ModuleDeleteCommand(
                                            name, coreName, location
                                    ));
                }

            }).start();
    }

    public void renameModule(final String name, final String jop)
    {
        final Area thisArea = area;
        if (!area.getApplication().isLockupBusy())
            new Thread(new Runnable()
            {
                public void run()
                {
                    thisArea
                            .getApplication()
                            .getReceiver()
                            .receive(new ModuleRenameCommand(name, jop));
                }

            }).start();
    }

    public void startAction(ModuleBox module)
    {
        module.startAction();
    }

    public void selectNull()
    {
        area.selectNull();
    }

    void select(ModulePanel panel, boolean controlDown)
    {
        if (controlDown) {
            area.alterSelection(panel);
        } else {
            area.select(panel);
        }
    }

    void select(LinkPanel panel, boolean controlDown)
    {
        if (controlDown) {
            area.alterSelection(panel);
        } else {
            area.select(panel);
        }
    }

    void addLink(Port a, Port b, final boolean isActive)
    {
        final Port in, out;
        if (a.isInput()) {
            in = a;
            out = b;
        } else {
            in = b;
            out = a;
        }

        if (!area.getApplication().isLockupBusy())
            new Thread(new Runnable()
            {
                public void run()
                {
                    getArea()
                            .getApplication()
                            .getReceiver()
                            .receive(
                                    new LinkAddCommand(
                                            new LinkName(out.getModuleBox().getName(),
                                                         out.getName(),
                                                         in.getModuleBox().getName(),
                                                         in.getName()
                                            ),
                                            isActive));
                }
            }).start();
    }

    public void deleteLink(final LinkName name)
    {
        if (!area.getApplication().isLockupBusy())
            new Thread(new Runnable()
            {
                public void run()
                {
                    getArea().getApplication().getReceiver().receive(new LinkDeleteCommand(name, true));
                }
            }).start();
    }
}
