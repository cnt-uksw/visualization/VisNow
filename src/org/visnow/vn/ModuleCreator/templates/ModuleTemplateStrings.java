/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.ModuleCreator.templates;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ModuleTemplateStrings
{

    public static final String header
        = "\n" +
        "package ___PACKAGE;\n" +
        "\n" +
        "import org.visnow.jscic.Field;\n" +
        "import org.visnow.jscic.IrregularField;\n" +
        "import org.visnow.jscic.RegularField;\n" +
        "import org.visnow.vn.engine.core.InputEgg;\n" +
        "import org.visnow.vn.engine.core.OutputEgg;\n" +
        "import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;\n" +
        "import org.visnow.vn.lib.types.VNField;\n" +
        "import org.visnow.vn.lib.types.VNIrregularField;\n" +
        "import org.visnow.vn.lib.utils.SwingInstancer;\n" +
        "import org.visnow.vn.system.main.VisNow;\n" +
        "\n" +
        "/**\n" +
        " * @author  \n" +
        " */\n" +
        "public class ___MODULE extends OutFieldVisualizationModule\n" +
        "   {\n";

    public static final String variables
        = "\n\n" +
        "   public static InputEgg[] inputEggs = null;\n" +
        "   public static OutputEgg[] outputEggs = null;\n" +
        "   protected Field inField;\n" +
        "   protected RegularField inRegularField;\n" +
        "   protected IrregularField inIrregularField;\n" +
        "   protected GUI computeUI = null;\n" +
        "   protected boolean fromGUI = false;\n" +
        "   protected Params params;\n" +
        "   protected boolean ignoreUI = false;";

    public static final String constructor
        = "\n\n" +
        "   public ___MODULE()\n" +
        "   {\n" +
        "      parameters = params = new Params();\n" +
        "      SwingInstancer.swingRunAndWait(new Runnable()\n" +
        "      {\n" +
        "         @Override\n" +
        "         public void run()\n" +
        "         {\n" +
        "            computeUI = new IsosurfaceGUI();\n" +
        "            computeUI.setParams(params);\n" +
        "            ui.addComputeGUI(computeUI);\n" +
        "            setPanel(ui);\n" +
        "         }\n" +
        "      });\n" +
        "      outObj.setName(\"___MODULE\");\n" +
        "      params.addChangeListener(new ChangeListener()\n" +
        "      {\n" +
        "         @Override\n" +
        "         public void stateChanged(ChangeEvent evt)\n" +
        "         {\n" +
        "            if (ignoreUI)\n" +
        "            {\n" +
        "               ignoreUI = false;\n" +
        "               return;\n" +
        "            }\n" +
        "            fromGUI = true;\n" +
        "            startAction();\n" +
        "         }\n" +
        "      });\n" +
        "   }";

    public static final String updateUI
        = "\n\n" +
        "   private void updateUI()\n" +
        "   {\n" +
        "      SwingInstancer.swingRunAndWait(new Runnable()\n" +
        "      {\n" +
        "         @Override\n" +
        "         public void run()\n" +
        "         {\n" +
        "            ignoreUI = true;\n" +
        "            computeUI.setInField(inField);\n" +
        "            ignoreUI = false;\n" +
        "         }\n" +
        "      });\n" +
        "   }";

    public static final String onActive
        = "\n\n" +
        "   @Override\n" +
        "   public void onActive()\n" +
        "   {\n" +
        "      if (!fromGUI)\n" +
        "      {\n" +
        "        VNField inVNFld = (VNField) getInputFirstValue(\"inField\");\n" +
        "        if(inVNFld == null || inVNFld.getField() == null) \n" +
        "        {\n" +
        "             inField = null;\n" +
        "             outField = null;\n" +
        "             outIrregularField = null;\n" +
        "             return;\n" +
        "        }\n" +
        "        Field newInField = inVNFld.getField();\n" +
        "        if (inField == null || inField != newInField)\n" +
        "        {\n" +
        "           params.setRecompute(true);\n" +
        "           lastTime = newInField.getCurrentTime();\n" +
        "           inField = newInField;\n" +
        "           if (inField instanceof RegularField)\n" +
        "           {\n" +
        "              inRegularField = ((RegularField) inField);\n" +
        "              int[] inDims = inRegularField.getDims();\n" +
        "              if (inDims.length != 3 || inDims[0] < 2 || inDims[1] < 2 || inDims[2] < 2)\n" +
        "              {\n" +
        "                 inField = null;\n" +
        "                 outField = null;\n" +
        "                 outIrregularField = null;\n" +
        "                 return;\n" +
        "              }\n" +
        "           } else \n" +
        "           {\n" +
        "              inIrregularField = ((IrregularField)inField);\n" +
        "              if(!(inIrregularField.hasCellsType(CellType.TETRA) || " +
        "                  inIrregularField.hasCellsType(CellType.PYRAMID) || " +
        "                  inIrregularField.hasCellsType(CellType.PRISM) ||  " +
        "                  irf.hasCellsType(CellType.HEXAHEDRON))) {\n" +
        "                  inField = null;\n" +
        "                  outField = null;\n" +
        "                  outIrregularField = null;\n" +
        "                  return;                  \n" +
        "              }\n" +
        "           }\n" +
        "           updateUI();\n" +
        "         }\n" +
        "      }\n" +
        "\n" +
        "      fromGUI = false;\n" +
        "      if (!params.isRecompute() && !params.isActive())\n" +
        "         return;\n" +
        "      update();\n" +
        "   }";

    public static String getHeader(String packageName, String moduleName)
    {
        return header.replaceAll("___PACKAGE", packageName).replaceAll("___MODULE", moduleName);
    }

    public static String getVariables()
    {
        return variables;
    }

    public static String getConstructor(String moduleName)
    {
        return constructor.replaceAll("___MODULE", moduleName);
    }

    public static String getUpdateUI()
    {
        return updateUI;
    }

    public static String getOnActive()
    {
        return onActive;
    }

    private ModuleTemplateStrings()
    {
    }
}
