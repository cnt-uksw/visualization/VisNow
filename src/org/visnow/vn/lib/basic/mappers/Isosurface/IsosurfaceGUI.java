/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.Isosurface;

import javax.swing.JPanel;
import org.apache.log4j.Logger;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.gui.widgets.RunButton;
import static org.visnow.vn.lib.basic.mappers.Isosurface.IsosurfaceShared.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * <p>
 * @modified by Szpak
 */
public class IsosurfaceGUI extends JPanel
{
    private static final Logger LOGGER = Logger.getLogger(IsosurfaceGUI.class);
    private ParameterProxy parameterProxy;

    //unfortunately these flags are necessary at the moment
    private boolean thresholdEditorActive = true;
    private boolean smoothSpinnerActive = true;

    /**
     * Creates new form IsosurfaceUI
     */
    public IsosurfaceGUI()
    {
        initComponents();
        preprocessingPanel.setVisible(preprocessingSectionHeader.isExpanded());
        thresholdPanel.setVisible(thresholdSectionHeader.isExpanded());
        thresholdEditor.setMaxRangeCount(2);
        thresholdEditor.setStartSingle(true);
    }

    public void setParameterProxy(ParameterProxy targetParameterProxy)
    {
        //putting run button between logic and gui
        parameterProxy = runButton.getPlainProxy(targetParameterProxy, RUNNING_MESSAGE);
        cropUI.setParameterProxy(parameterProxy);
        downsizeUI.setParameterProxy(parameterProxy);
    }

    void updateGUI(final ParameterProxy p, boolean resetFully, boolean setRunButtonPending)
    {
        LOGGER.debug("");
        int componentIndex = p.get(SELECTED_COMPONENT);
        double[] minMaxRange = p.get(META_MINMAX_RANGES)[componentIndex];
        selectedComponentCB.setListData(p.get(META_COMPONENT_NAMES));
        selectedComponentCB.setSelectedIndex(componentIndex);
        updateTimeSlider(p.get(META_TIME_RANGES)[componentIndex], p.get(TIME_FRAME), resetFully);
        if (resetFully)
            updateThresholdEditor(minMaxRange, p.get(THRESHOLDS));
        else //assuming ranges are correct
            updateThresholdEditor(null, p.get(THRESHOLDS));
        uncertaintyBox.setEnabled(p.get(META_IS_REGULAR));
        separateSetsBox.setSelected(p.get(CELLSET_SEPARATION));
        uncertaintyBox.setSelected(p.get(UNCERTAINTY_COMPONENT_PRESENT));
        smoothBox.setSelected(p.get(SMOOTHING_STEP_COUNT) > 0);
        if (p.get(SMOOTHING_STEP_COUNT) > 0) {
            smoothSpinnerActive = false;
            smoothSpinner.setValue(p.get(SMOOTHING_STEP_COUNT));
            smoothSpinnerActive = true;
        }
        updateSmoothSpinner();

        preprocessingRegularPanel.setVisible(p.get(META_IS_REGULAR)); //2nd level of crop visibility is cropPresentationPanel
        cropUI.updateGUI(p, resetFully);
        downsizeUI.updateGUI(p);
        runButton.updateAutoState(p.get(RUNNING_MESSAGE));
        runButton.updatePendingState(setRunButtonPending);
    }

    private void updateTimeSlider(float[] range, float value, boolean resetRange)
    {
        timeSlider.setEnabled(range[0] != range[1]);
        timeSlider.setValue(value);
        if (resetRange) {
            timeSlider.setMin(range[0]);
            timeSlider.setMax(range[1]);
        }
        //always reset global range
        timeSlider.setGlobalMin(range[0]);
        timeSlider.setGlobalMax(range[1]);
    }

    private void updateThresholdEditor(double[] minMax, float[] thresholds)
    {
        thresholdEditorActive = false;

        if (minMax != null)
            thresholdEditor.setMinMax((float) minMax[0], (float) minMax[1], (float) minMax[0], (float) minMax[1], thresholds != null ? thresholds[thresholds.length / 2] : null);
        if (thresholds != null)
            thresholdEditor.setThresholds(thresholds);
        thresholdEditorActive = true;

    }

    private void updateSmoothSpinner()
    {
        smoothStepsLabel.setEnabled(smoothBox.isSelected());
        smoothSpinner.setEnabled(smoothBox.isSelected());
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        preprocessingRegularPanel = new javax.swing.JPanel();
        preprocessingSectionHeader = new org.visnow.vn.gui.widgets.SectionHeader();
        preprocessingPanel = new javax.swing.JPanel();
        cropPresentationPanel = new javax.swing.JPanel();
        cropUI = new org.visnow.vn.lib.gui.cropUI.CropUI();
        downsizeUI = new org.visnow.vn.lib.gui.DownsizeUI();
        thresholdSectionHeader = new org.visnow.vn.gui.widgets.SectionHeader();
        thresholdPanel = new javax.swing.JPanel();
        selectedComponentCB = new org.visnow.vn.gui.widgets.WidePopupComboBox();
        timeSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        thresholdEditor = new org.visnow.vn.lib.gui.FloatArrayEditor();
        separateSetsBox = new javax.swing.JCheckBox();
        uncertaintyBox = new javax.swing.JCheckBox();
        autoSmoothPanel = new javax.swing.JPanel();
        smoothBox = new javax.swing.JCheckBox();
        smoothStepsLabel = new javax.swing.JLabel();
        smoothSpinner = new javax.swing.JSpinner();
        runButton = new org.visnow.vn.gui.widgets.RunButton();
        filler = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        setLayout(new java.awt.GridBagLayout());

        preprocessingRegularPanel.setLayout(new java.awt.GridBagLayout());

        preprocessingSectionHeader.setExpanded(false);
        preprocessingSectionHeader.setShowCheckBox(false);
        preprocessingSectionHeader.setText("Preprocessing");
        preprocessingSectionHeader.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                preprocessingSectionHeaderUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        preprocessingRegularPanel.add(preprocessingSectionHeader, gridBagConstraints);

        preprocessingPanel.setLayout(new java.awt.GridBagLayout());

        cropPresentationPanel.setLayout(new java.awt.GridBagLayout());

        cropUI.setDynamic(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        cropPresentationPanel.add(cropUI, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        preprocessingPanel.add(cropPresentationPanel, gridBagConstraints);

        downsizeUI.setDynamicButtonVisible(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        preprocessingPanel.add(downsizeUI, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 16, 0);
        preprocessingRegularPanel.add(preprocessingPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(preprocessingRegularPanel, gridBagConstraints);

        thresholdSectionHeader.setShowCheckBox(false);
        thresholdSectionHeader.setText("Isosurface thresholds");
        thresholdSectionHeader.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                thresholdSectionHeaderUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(thresholdSectionHeader, gridBagConstraints);

        thresholdPanel.setLayout(new java.awt.GridBagLayout());

        selectedComponentCB.setBorder(javax.swing.BorderFactory.createTitledBorder("Component"));
        selectedComponentCB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                selectedComponentCBUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        thresholdPanel.add(selectedComponentCB, gridBagConstraints);

        timeSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        timeSlider.setGlobalMax(1);
        timeSlider.setGlobalMin(0);
        timeSlider.setSubmitOnAdjusting(false);
        timeSlider.setValue(0.5);
        timeSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("Time:"));
        timeSlider.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                timeSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        thresholdPanel.add(timeSlider, gridBagConstraints);

        thresholdEditor.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                thresholdEditorStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        thresholdPanel.add(thresholdEditor, gridBagConstraints);

        separateSetsBox.setText("Isosurfaces as separate sets");
        separateSetsBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                separateSetsBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        thresholdPanel.add(separateSetsBox, gridBagConstraints);

        uncertaintyBox.setText("Create uncertainty component");
        uncertaintyBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                uncertaintyBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        thresholdPanel.add(uncertaintyBox, gridBagConstraints);

        autoSmoothPanel.setLayout(new java.awt.GridBagLayout());

        smoothBox.setText("Smooth mesh");
        smoothBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                smoothBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        autoSmoothPanel.add(smoothBox, gridBagConstraints);

        smoothStepsLabel.setText("Steps:");
        smoothStepsLabel.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 8);
        autoSmoothPanel.add(smoothStepsLabel, gridBagConstraints);

        smoothSpinner.setModel(new javax.swing.SpinnerNumberModel(2, 1, 6, 1));
        smoothSpinner.setEnabled(false);
        smoothSpinner.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                smoothSpinnerStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        autoSmoothPanel.add(smoothSpinner, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        thresholdPanel.add(autoSmoothPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(thresholdPanel, gridBagConstraints);

        runButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                runButtonUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(16, 0, 0, 0);
        add(runButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weighty = 1.0;
        add(filler, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void smoothSpinnerStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_smoothSpinnerStateChanged
    {//GEN-HEADEREND:event_smoothSpinnerStateChanged
        if (smoothSpinnerActive)
            parameterProxy.set(SMOOTHING_STEP_COUNT, (int) smoothSpinner.getValue());
    }//GEN-LAST:event_smoothSpinnerStateChanged

    private void smoothBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_smoothBoxActionPerformed
    {//GEN-HEADEREND:event_smoothBoxActionPerformed
        updateSmoothSpinner();
        parameterProxy.set(SMOOTHING_STEP_COUNT, smoothBox.isSelected() ? (int) smoothSpinner.getValue() : 0);
    }//GEN-LAST:event_smoothBoxActionPerformed

    private void uncertaintyBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_uncertaintyBoxActionPerformed
    {//GEN-HEADEREND:event_uncertaintyBoxActionPerformed
        parameterProxy.set(UNCERTAINTY_COMPONENT_PRESENT, uncertaintyBox.isSelected());
    }//GEN-LAST:event_uncertaintyBoxActionPerformed

    private void separateSetsBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_separateSetsBoxActionPerformed
    {//GEN-HEADEREND:event_separateSetsBoxActionPerformed
        parameterProxy.set(CELLSET_SEPARATION, separateSetsBox.isSelected());
    }//GEN-LAST:event_separateSetsBoxActionPerformed

    private void preprocessingSectionHeaderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_preprocessingSectionHeaderUserChangeAction
    {//GEN-HEADEREND:event_preprocessingSectionHeaderUserChangeAction
        preprocessingPanel.setVisible(preprocessingSectionHeader.isExpanded());
    }//GEN-LAST:event_preprocessingSectionHeaderUserChangeAction

    private void timeSliderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_timeSliderUserChangeAction
    {//GEN-HEADEREND:event_timeSliderUserChangeAction
        parameterProxy.set(TIME_FRAME, (Float) timeSlider.getValue());
    }//GEN-LAST:event_timeSliderUserChangeAction

    private void thresholdEditorStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_thresholdEditorStateChanged
    {//GEN-HEADEREND:event_thresholdEditorStateChanged
        if (thresholdEditorActive) {
            parameterProxy.set(THRESHOLDS, thresholdEditor.getThresholds());
        }
    }//GEN-LAST:event_thresholdEditorStateChanged

    /**
     * @param value time value to set or null if keep unchanged (may be auto-aligned to min/max)
     */

    private void selectedComponentCBUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_selectedComponentCBUserChangeAction
    {//GEN-HEADEREND:event_selectedComponentCBUserChangeAction
        int componentIndex = selectedComponentCB.getSelectedIndex();
        float[] timeRange = parameterProxy.get(META_TIME_RANGES)[componentIndex];
        double[] minMaxRange = parameterProxy.get(META_MINMAX_RANGES)[componentIndex];
        updateTimeSlider(timeRange, (timeRange[1] + timeRange[0]) / 2, true);

        updateThresholdEditor(minMaxRange, null);

        parameterProxy.set(SELECTED_COMPONENT, componentIndex,
                           TIME_FRAME, (Float) timeSlider.getValue(),
                           THRESHOLDS, thresholdEditor.getThresholds());
    }//GEN-LAST:event_selectedComponentCBUserChangeAction

    private void thresholdSectionHeaderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_thresholdSectionHeaderUserChangeAction
    {//GEN-HEADEREND:event_thresholdSectionHeaderUserChangeAction
        thresholdPanel.setVisible(thresholdSectionHeader.isExpanded());
    }//GEN-LAST:event_thresholdSectionHeaderUserChangeAction

    private void runButtonUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_runButtonUserChangeAction
    {//GEN-HEADEREND:event_runButtonUserChangeAction
        parameterProxy.set(RUNNING_MESSAGE, (RunButton.RunState) evt.getEventData());
    }//GEN-LAST:event_runButtonUserChangeAction

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JPanel autoSmoothPanel;
    public javax.swing.JPanel cropPresentationPanel;
    public org.visnow.vn.lib.gui.cropUI.CropUI cropUI;
    public org.visnow.vn.lib.gui.DownsizeUI downsizeUI;
    public javax.swing.Box.Filler filler;
    public javax.swing.JPanel preprocessingPanel;
    public javax.swing.JPanel preprocessingRegularPanel;
    public org.visnow.vn.gui.widgets.SectionHeader preprocessingSectionHeader;
    public org.visnow.vn.gui.widgets.RunButton runButton;
    public org.visnow.vn.gui.widgets.WidePopupComboBox selectedComponentCB;
    public javax.swing.JCheckBox separateSetsBox;
    public javax.swing.JCheckBox smoothBox;
    public javax.swing.JSpinner smoothSpinner;
    public javax.swing.JLabel smoothStepsLabel;
    public org.visnow.vn.lib.gui.FloatArrayEditor thresholdEditor;
    public javax.swing.JPanel thresholdPanel;
    public org.visnow.vn.gui.widgets.SectionHeader thresholdSectionHeader;
    public org.visnow.vn.gui.widgets.ExtendedSlider timeSlider;
    public javax.swing.JCheckBox uncertaintyBox;
    // End of variables declaration//GEN-END:variables
    
}
