/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.Isosurface;

import java.util.ArrayList;
import java.util.List;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton;
import static org.visnow.vn.gui.widgets.RunButton.RunState.RUN_DYNAMICALLY;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class IsosurfaceShared
{
    //Specification:
    //0 <= integer < META_COMPONENT_NAMES.length
    static final ParameterName<Integer> SELECTED_COMPONENT = new ParameterName("Selected component");
    //non empty array of float within [META_MINMAX_RANGES[s0][0] .. META_MINMAX_RANGES[s0][1]], s0 = SELECTED_COMPONENT    
    static final ParameterName<float[]> THRESHOLDS = new ParameterName("Threshold values");
    //integer >= 0
    static final ParameterName<Integer> SMOOTHING_STEP_COUNT = new ParameterName("Number of steps for mesh smoothing");
    static final ParameterName<Boolean> UNCERTAINTY_COMPONENT_PRESENT = new ParameterName("Uncertainty component generation");
    static final ParameterName<Boolean> CELLSET_SEPARATION = new ParameterName("Isosurfaces as separate cell sets");
    //float within [META_TIME_RANGES[s0][0] .. META_TIME_RANGES[s0][1]], s0 = SELECTED_COMPONENT
    static final ParameterName<Float> TIME_FRAME = new ParameterName("Selected time frame");

    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");

    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic)    
    //not empty array of Strings
    static final ParameterName<String[]> META_COMPONENT_NAMES = new ParameterName("META component names");
    //not empty array of double[2]
    //length == META_COMPONENT_NAMES.length
    static final ParameterName<double[][]> META_MINMAX_RANGES = new ParameterName("META component preferredPhysMin / preferredPhysMin ranges");
    //not empty array of float[2]
    //length == META_COMPONENT_NAMES.length
    static final ParameterName<float[][]> META_TIME_RANGES = new ParameterName("META component time ranges");
    static final ParameterName<Boolean> META_IS_REGULAR = new ParameterName("META is regular field");

    public static List<Parameter> createDefaultParametersAsList()
    {
        return new ArrayList<Parameter>()
        {
            {
                add(new Parameter<>(SELECTED_COMPONENT, 0));
                add(new Parameter<>(THRESHOLDS, new float[]{127}));
                add(new Parameter<>(SMOOTHING_STEP_COUNT, 0));
                add(new Parameter<>(UNCERTAINTY_COMPONENT_PRESENT, false));
                add(new Parameter<>(CELLSET_SEPARATION, false));
                add(new Parameter<>(TIME_FRAME, 0f));
                add(new Parameter<>(RUNNING_MESSAGE, RUN_DYNAMICALLY));

                add(new Parameter<>(META_COMPONENT_NAMES, new String[]{"------"}));
                add(new Parameter<>(META_MINMAX_RANGES, new double[][]{new double[]{0, 1}}));
                add(new Parameter<>(META_TIME_RANGES, new float[][]{new float[]{0, 0}}));
                add(new Parameter<>(META_IS_REGULAR, true));
            }
        };
    }
}
