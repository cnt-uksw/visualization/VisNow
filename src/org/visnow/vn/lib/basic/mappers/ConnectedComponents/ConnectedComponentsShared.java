/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.ConnectedComponents;

//import org.visnow.vn.lib.basic.filters.SurfaceComponents.*;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton;

/**
 *
 * @author pregulski
 */


public class ConnectedComponentsShared
{
    //Minimal component sizes
    static final ParameterName<Integer> MIN_COMPONENT_SIZE = new ParameterName("minComponentSize");
    //Number of connected separate components
    static final ParameterName<Integer> SEPARATE_COMPONENTS = new ParameterName("separateComponents");
    //Index of currently component
    static final ParameterName<Integer> MODIFIED_COMPONENT = new ParameterName("modifiedComponent");
    //Names of cellsets
    static final ParameterName<String[]> COMPONENT_NAMES = new ParameterName("CellSetComponentNames");
    //Selection of the modifiedSelection
    static final ParameterName<boolean[]> MODIFIED_SELECTION = new ParameterName("modifiedSelected");

    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");
    //Number of nodes in every CelllSet
    static final ParameterName<int[]> META_CELLSET_COMPONENT_ACTIVE_N_NODES = new ParameterName("CellSetActiveNNodes");
    //Name of the CellSet currently selected
    static final ParameterName<String> META_CELLSET_MODIFIED_NAME = new ParameterName("modifiedName");
    // decides if jTable should be repainted
    static final ParameterName<Boolean> META_DIFFERENT_PARAMS = new ParameterName("differentParams");
}
