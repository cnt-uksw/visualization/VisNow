/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.ConnectedComponents;

import org.jogamp.java3d.PickInfo;
import org.visnow.jscic.IrregularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.mappers.ConnectedComponents.ConnectedComponentsShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.field.ExtractCellSets;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ConnectedComponents extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private final ProcessComponents processor = new ProcessComponents();
    private GUI computeUI = null;
    private IrregularField inIrregularField = null;
    private IrregularField midIrregularField = null;

    private int lastMinComponentSize = 20, lastSeparateComponents = 20;
    private final int resetMinComponentSize = 20, resetSeparateComponents = 20;
    private int runQueue = 0;

    public ConnectedComponents()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name != null && name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startIfNotInQueue();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {

            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(MIN_COMPONENT_SIZE, resetMinComponentSize),
            new Parameter<>(SEPARATE_COMPONENTS, resetSeparateComponents),
            new Parameter<>(MODIFIED_COMPONENT, -1),
            new Parameter<>(MODIFIED_SELECTION, new boolean[]{}),
            new Parameter<>(COMPONENT_NAMES, new String[]{}),
            new Parameter<>(META_CELLSET_COMPONENT_ACTIVE_N_NODES, new int[]{}),
            new Parameter<>(META_CELLSET_MODIFIED_NAME, ""),
            new Parameter<>(META_DIFFERENT_PARAMS, false),
            new Parameter<>(RUNNING_MESSAGE, RUN_DYNAMICALLY)};
    }

    private void validateParamsAndSetSmart(boolean isFromVNA, boolean isDifferentField, boolean isDifferentParameter)
    {
        parameters.setParameterActive(false);
        if (isFromVNA) {
            parameters.set(META_DIFFERENT_PARAMS, true);
            for (int i = 0; i < midIrregularField.getNCellSets(); i++) {
                midIrregularField.getCellSet(i).setSelected(parameters.get(MODIFIED_SELECTION)[i]);
                midIrregularField.getCellSet(i).setName(parameters.get(COMPONENT_NAMES)[i]);
            }
        } else if (isDifferentField || isDifferentParameter) {
            String[] names = new String[midIrregularField.getNCellSets()];
            int[] activenNodes = new int[midIrregularField.getNCellSets()];
            boolean[] selected = new boolean[midIrregularField.getNCellSets()];
            for (int i = 0; i < names.length; i++) {
                names[i] = midIrregularField.getCellSet(i).getName();
                activenNodes[i] = midIrregularField.getCellSet(i).getNActiveNodes();
                midIrregularField.getCellSet(i).setSelected(true);
                selected[i] = midIrregularField.getCellSet(i).isSelected();
            }
            parameters.set(COMPONENT_NAMES, names,
                           META_CELLSET_COMPONENT_ACTIVE_N_NODES, activenNodes,
                           MODIFIED_SELECTION, selected,
                           META_DIFFERENT_PARAMS, true);
        } else if (parameters.get(MODIFIED_SELECTION).length > 0) {
            for (int i = 0; i < midIrregularField.getNCellSets(); i++) {
                if (parameters.get(MODIFIED_SELECTION).length > i) {
                    midIrregularField.getCellSet(i).setSelected(parameters.get(MODIFIED_SELECTION)[i]);
                }
            }
        }
        parameters.set(MODIFIED_COMPONENT, -1);
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    public void updateCellSetData(int n, String name, boolean selected, boolean updateUI)
    {
        if (n >= 0 && n < midIrregularField.getNCellSets()) {
            midIrregularField.getCellSet(n).setName(name);
            midIrregularField.getCellSet(n).setSelected(selected);
            if(irregularFieldGeometry.getField() == null)
                irregularFieldGeometry.setField(midIrregularField);
            irregularFieldGeometry.updateCellSetData(n);
            if (updateUI) 
                        presentationParams.setInField(midIrregularField);
        }
    }

    @Override
    public void processPickInfo(final String pickedItemName, PickInfo info, int screenX, int screenY)
    {
        parameters.setParameterActive(false);
        parameters.set(META_CELLSET_MODIFIED_NAME, pickedItemName, META_DIFFERENT_PARAMS, true);
        parameters.setParameterActive(true);
        final Parameters p = parameters.getReadOnlyClone();
        notifyGUIs(p, true, false);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {

            //1. get new field
            boolean isFromVNA = isFromVNA();
            IrregularField newField = ((VNIrregularField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA && (inIrregularField == null || inIrregularField.getTimestamp() != newField.getTimestamp());
            inIrregularField = newField;

            //2. validation parameters
            boolean isDifferentParameter = false;
            Parameters p;
            synchronized (parameters) {
                parameters.setParameterActive(false);
                if (isDifferentField) {
                    parameters.set(MIN_COMPONENT_SIZE, resetMinComponentSize, SEPARATE_COMPONENTS, resetSeparateComponents, META_DIFFERENT_PARAMS, false);
                } else if (isFromVNA) {
                    parameters.set(MODIFIED_COMPONENT, -1, META_DIFFERENT_PARAMS, false);
                }
                parameters.setParameterActive(true);
                p = parameters.getReadOnlyClone();
            }

            //3. update GUI
            notifyGUIs(p, isFromVNA || isDifferentField, isDifferentField);

            //4. computation
            ProgressAgent progressAgent;
            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY || isFromVNA) {
                runQueue = Math.max(runQueue - 1, 0);
                if (p.get(MODIFIED_COMPONENT) < 0) {
                    int totalSteps = p.get(SEPARATE_COMPONENTS) + 3;
                    progressAgent = getProgressAgent(totalSteps);
                    progressAgent.increase();
                    processor.setInField(inIrregularField, progressAgent);
                    progressAgent.increase();
                    midIrregularField = processor.buildOutput(p);
                    if (isFromVNA) {
                        if (p.get(MODIFIED_SELECTION).length != midIrregularField.getNCellSets()) {
                            isDifferentField = true;
                            isFromVNA = false;
                        } else {
                            for (int i = 0; i < midIrregularField.getNCellSets(); i++) {
                                if (p.get(META_CELLSET_COMPONENT_ACTIVE_N_NODES)[i] != midIrregularField.getCellSet(i).getNActiveNodes()) {
                                    isDifferentField = true;
                                    isFromVNA = false;
                                    break;
                                }
                            }
                        }
                    }
                    
                } else {
                    int totalSteps = 2;
                    progressAgent = getProgressAgent(totalSteps);
                    updateCellSetData(p.get(MODIFIED_COMPONENT), p.get(COMPONENT_NAMES)[p.get(MODIFIED_COMPONENT)], p.get(MODIFIED_SELECTION)[p.get(MODIFIED_COMPONENT)], true);
                    progressAgent.increase();
                }

                //5. second validation parameters
                synchronized (parameters) {
                    isDifferentParameter = !isFromVNA &&
                            (isDifferentField || (parameters.get(MIN_COMPONENT_SIZE) != lastMinComponentSize || parameters.get(SEPARATE_COMPONENTS) != lastSeparateComponents)) &&
                            (isDifferentField || p.get(SEPARATE_COMPONENTS) == midIrregularField.getNCellSets() || (lastSeparateComponents < p.get(SEPARATE_COMPONENTS) && (lastSeparateComponents <= midIrregularField.getNCellSets())) || parameters.get(MIN_COMPONENT_SIZE) != lastMinComponentSize);
                    lastMinComponentSize = parameters.get(MIN_COMPONENT_SIZE);
                    lastSeparateComponents = parameters.get(SEPARATE_COMPONENTS);
                    validateParamsAndSetSmart(isFromVNA, isDifferentField, isDifferentParameter);
                    p = parameters.getReadOnlyClone();
                }

                //6. second update GUI
                notifyGUIs(p, isDifferentParameter, false);

                //7. second computation
                outIrregularField = ExtractCellSets.extractCellSets(midIrregularField);

                //8. propagate
                if (outIrregularField != null)
                    setOutputValue("outField", new VNIrregularField(outIrregularField));
                else
                    setOutputValue("outField", null);
                outField = outIrregularField;
                prepareOutputGeometry();
                show();
            }
        }
    }
}
