/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.CellCenters;

import java.util.Arrays;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import static org.visnow.jscic.dataarrays.DataArrayType.*;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import static org.visnow.vn.lib.basic.mappers.CellCenters.CellCentersShared.USE_PARTIAL_DATA;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class CellCenters extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected RegularField inRegularField = null;
    protected IrregularField inField = null;
    protected GUI computeUI;

    public CellCenters()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
                outObj.setName("scale cells");
            }
        });
        setPanel(ui);
        outObj.setName("cell centers");
    }
    
    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(USE_PARTIAL_DATA, true)
        };
    }
    
    private int makeCellSetCentersData(CellSet inCS, int veclen, LargeArray in, LargeArray out, int iout, boolean round)
    {
        float[] u = new float[veclen];
        for (int j = 0; j < Cell.getNProperCellTypes(); j++)
            if (inCS.getCellArray(CellType.getType(j)) != null) {
                CellArray inCA = inCS.getCellArray(CellType.getType(j));
                int[] nodes = inCA.getNodes();
                int nv = CellType.getType(j).getNVertices();
                for (int k = 0; k < inCA.getNCells(); k++, iout++) {
                    for (int l = 0; l < veclen; l++) {
                        u[l] = 0;
                        for (int p = 0; p < nv; p++) {
                            int ip = nodes[k * nv + p];
                            u[l] += in.getFloat(veclen * ip + l);
                        }
                        u[l] /=  nv;
                        if (round)
                        u[l] = Math.round(u[l]);
                    }
                    for (int l = 0; l < veclen; l++)
                        out.setFloat(veclen * iout + l, u[l]);
                }
            }
        return iout;
    }
    
    private void createOutField()
    {
        if (inField == null)
            return;
        int nNodes = 0;
        int[] nSetNodes = new int[inField.getNCellSets()];
        Arrays.fill(nSetNodes, 0);
        for (int iCellSet = 0; iCellSet < inField.getNCellSets(); iCellSet++) {
            CellSet cs = inField.getCellSet(iCellSet);
            for (int iCellArray = 0; iCellArray < Cell.getNProperCellTypes(); iCellArray++)
                if (cs.getCellArray(CellType.getType(iCellArray)) != null) {
                    nNodes += cs.getCellArray(CellType.getType(iCellArray)).getNCells();
                    nSetNodes[iCellSet] += cs.getCellArray(CellType.getType(iCellArray)).getNCells();
                }
        }
        outIrregularField = new IrregularField(nNodes);

        float[] inCoords = inField.getCurrentCoords().getData();
        float[] coords = new float[3 * nNodes];
        float[] u = new float[3];
        
        for (int iCellSet = 0, iout = 0; iCellSet < inField.getNCellSets(); iCellSet++) {
            CellSet inCS = inField.getCellSet(iCellSet);
            CellSet outCS = new CellSet(inCS.getName());
            int[] outNodes = new int[nSetNodes[iCellSet]];
            for (int iCellArray = 0, csCell = 0; iCellArray < Cell.getNProperCellTypes(); iCellArray++)
                if (inCS.getCellArray(CellType.getType(iCellArray)) != null) {
                    CellArray inCA = inCS.getCellArray(CellType.getType(iCellArray));
                    int[] nodes = inCA.getNodes();
                    int nv = CellType.getType(iCellArray).getNVertices();
                    for (int k = 0; k < inCA.getNCells(); k++, iout++, csCell++) {
                        for (int l = 0; l < 3; l++) {
                            u[l] = 0;
                            for (int p = 0; p < nv; p++) {
                                int ip = nodes[k * nv + p];
                                u[l] += inCoords[3 * ip + l];
                            }
                            u[l] /= nv;
                        }
                        System.arraycopy(u, 0, coords, 3 * iout, 3);
                        outNodes[csCell] = iout;
                    }
                }
            CellArray outCA = new CellArray(CellType.POINT, outNodes, null, null);
            outCS.setCellArray(outCA);
            outIrregularField.addCellSet(outCS);
        }
        outIrregularField.setCurrentCoords(new FloatLargeArray(coords));
        
        for (int iCellSet = 0, kout = 0; iCellSet < inField.getNCellSets(); iCellSet++) {
            CellSet inCS = inField.getCellSet(iCellSet);
            for (int iData = 0; iData < inCS.getNComponents(); iData++) {
                int iout = kout;
                DataArray inDA = inCS.getComponent(iData);
                String name = inDA.getName();
                DataArrayType type = inDA.getType();
                int vlen = inDA.getVectorLength();
                int dataIndex = -1;
                for (int k = 0; k < outIrregularField.getNComponents(); k++) {
                    if (name.equals(outIrregularField.getComponent(k).getName())) {
                        if (type == outIrregularField.getComponent(k).getType() &&
                            vlen == outIrregularField.getComponent(k).getVectorLength()) {
                            dataIndex = k;
                            break;
                        }
                        else {
                            dataIndex = -2;
                            break;
                        }
                    }
                }
                TimeData inTimeData = inDA.getTimeData();
                TimeData outTimeData = dataIndex >= 0 ? outIrregularField.getComponent(dataIndex).getTimeData() :
                                                        new TimeData(type);
                for (int iCellArray = 0; iCellArray < Cell.getNProperCellTypes(); iCellArray++) {
                    CellArray inCA = inCS.getCellArray(CellType.getType(iCellArray));
                    if (inCA != null && inCA.getDataIndices() != null) 
                        for (int i = 0; i < inTimeData.getNSteps(); i++) {
                            LargeArray in  = inTimeData.getValue(inTimeData.getTime(i));
                            LargeArray out = outTimeData.produceValue(inTimeData.getTime(i), nNodes * vlen);
                            int[] inIndices = inCA.getDataIndices();
                            for (int k = 0; k < inIndices.length; k++, iout++) 
                                try {
                                    LargeArrayUtils.arraycopy(in, inIndices[k] * vlen, out, iout * vlen, vlen);
                                } catch (Exception e) {
                                    System.out.println("");
                                }
                        }
                    else if (inCA != null)
                        iout += inCA.getNCells();
                }
                if (dataIndex < 0)
                     outIrregularField.addComponent(DataArray.create(outTimeData, vlen, 
                                                                     dataIndex == -1 ? inDA.getName() : 
                                                                     inDA.getName() + "_" + iCellSet + "." + iData));
            }
            for (CellArray inCA : inCS.getCellArrays()) 
                if (inCA != null)
                    kout += inCA.getNCells();
        }
        for (int iData = 0; iData < inField.getNComponents(); iData++) {
            DataArray inDA = inField.getComponent(iData);
            if (inDA.isNumeric()) {
                int vlen = inDA.getVectorLength();
                String name = inDA.getName();
                boolean round = inDA.getType() == FIELD_DATA_BYTE || inDA.getType() == FIELD_DATA_SHORT ||
                                inDA.getType() == FIELD_DATA_INT  || inDA.getType() == FIELD_DATA_LONG;
                
                TimeData inTimeData = inDA.getTimeData();
                TimeData outTimeData = new TimeData(inDA.getType());
                for (int i = 0; i < inTimeData.getNSteps(); i++) {
                    LargeArray in  = inTimeData.getValue(inTimeData.getTime(i));
                    LargeArray out = outTimeData.produceValue(inTimeData.getTime(i), nNodes * vlen);
                    for (int iSet = 0, iout = 0; iSet < inField.getNCellSets(); iSet++) 
                        iout = makeCellSetCentersData(inField.getCellSet(iSet), vlen, in, out, iout, round);
                }
                outIrregularField.addComponent(DataArray.create(outTimeData, vlen, name));
            }
        
        }
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null)
            return;
        VNField input = ((VNField) getInputFirstValue("inField"));
        Field newInField = input.getField();
        if (newInField != null && inField != newInField) {
            inField = (IrregularField) newInField;
            outObj.clearAllGeometry();
            outGroup = null;
            outObj.setName(inField.getName());
            createOutField();
            outIrregularField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
            outField = outIrregularField;
            prepareOutputGeometry();
            show();
        }
        setOutputValue("outField", new VNIrregularField(outIrregularField));
    }
}
