/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.TubeGlyphs;

import java.awt.Color;
import org.jogamp.java3d.*;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.events.ColorEvent;
import org.visnow.vn.geometries.events.ColorListener;
import org.visnow.vn.geometries.geometryTemplates.Glyph;
import static org.visnow.vn.geometries.geometryTemplates.ScalarGlyphTemplates.glyph;
import org.visnow.vn.geometries.geometryTemplates.Templates;
import org.visnow.vn.geometries.objects.ColormapLegend;
import org.visnow.vn.geometries.objects.generics.*;
import org.visnow.vn.geometries.utils.ColorMapper;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import static org.visnow.vn.lib.basic.mappers.TubeGlyphs.TubeGlyphsShared.*;
import org.visnow.vn.lib.templates.visualization.modules.VisualizationModule;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class TubeGlyphs extends VisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected GUI computeUI = null;
    protected IrregularField inField = null;
    protected OpenBranchGroup outGroup = null;
    protected DataArray data, colorData;
    protected int nGlyphs, nstrip, nvert, nind, ncol;
    protected int[] glyphV = null;
    protected int[] cIndex = null;
    protected int[] pIndex = null;
    protected int[] strips = null;
    protected float[] verts = null;
    protected float[] normals = null;
    protected byte[] colors = null;
    protected Glyph gt = null;
    protected IndexedGeometryStripArray surf;
    protected OpenAppearance app;
    protected OpenShape3D surfaces;
    protected ColormapLegend colormapLegend = outObj.getColormapLegend();
    protected Color bgrColor = Color.BLACK;
    protected float[] bgrF = new float[4];
    protected int component = 0;

    public TubeGlyphs()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startIfNotInQueue();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {

            @Override
            public void run()
            {
                computeUI = new GUI();
                setPanel(computeUI);
                computeUI.setParameters(parameters, dataMappingParams, renderingParams);
            }
        });
        dataMappingParams.addRenderEventListener(new RenderEventListener()
        {
            @Override
            public void renderExtentChanged(RenderEvent e)
            {
                if (surf != null && surfaces != null) {
                    updateColors(parameters);
                    colormapLegend.getParams().setColorMapLookup(dataMappingParams.getColorMap0().getARGBColorTable());
                }
            }
        });
        backGroundColorListener = new ColorListener()
        {
            @Override
            public void colorChoosen(ColorEvent e) {
                bgrColor = e.getSelectedColor();
                bgrColor.getColorComponents(bgrF);
                if (parameters.get(BACKGROUND))
                    updateColors(parameters);
            }
        };

        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
        outObj.addGeometry2D(colormapLegend);
    }

    public void createTubeGlyphs(Parameters p)
    {
        if (outGroup != null)
            outGroup.detach();
        outGroup = new OpenBranchGroup();
        data = inField.getComponent(p.get(SELECTED_COMPONENT));
        gt = new Templates.TubeTemplate(p.get(LEVEL_OF_DETAILS));
        nGlyphs = 0;
        for (CellSet cellSet : inField.getCellSets()) {
            if (cellSet.getBoundaryCellArray(CellType.SEGMENT) != null &&
                cellSet.getBoundaryCellArray(CellType.SEGMENT).getNCells() != 0)
                nGlyphs += cellSet.getBoundaryCellArray(CellType.SEGMENT).getNCells();
            else if (cellSet.getCellArray(CellType.SEGMENT) != null &&
                     cellSet.getCellArray(CellType.SEGMENT).getNCells() != 0)
                nGlyphs += cellSet.getCellArray(CellType.SEGMENT).getNCells();
        }
        if (nGlyphs == 0)
            return;
        nstrip = nGlyphs * gt.getNstrips();
        nvert = nGlyphs * gt.getNverts();
        nind = nGlyphs * gt.getNinds();
        ncol = 2 * nGlyphs;
        glyphV = new int[2 * nGlyphs];
        strips = new int[nstrip];
        verts = new float[3 * nvert];
        normals = new float[3 * nvert];
        pIndex = new int[nind];
        cIndex = new int[nind];
        colors = new byte[3 * ncol];
        makeIndices();
        if (p.get(BACKGROUND))
            surf = new IndexedTriangleStripArray(nvert,
                                                 GeometryArray.COORDINATES ,
                                                 nind, strips);
        else {
            surf = new IndexedTriangleStripArray(nvert,
                                                 GeometryArray.COORDINATES | GeometryArray.NORMALS | GeometryArray.COLOR_4,
                                                 nind, strips);
            surf.setCapability(IndexedLineStripArray.ALLOW_COLOR_READ);
            surf.setCapability(IndexedLineStripArray.ALLOW_COLOR_WRITE);
        }
        surf.setCapability(IndexedLineStripArray.ALLOW_COUNT_READ);
        surf.setCapability(IndexedLineStripArray.ALLOW_FORMAT_READ);
        surf.setCapability(IndexedLineStripArray.ALLOW_COORDINATE_INDEX_READ);
        surf.setCapability(IndexedLineStripArray.ALLOW_COORDINATE_READ);
        surf.setCapability(IndexedLineStripArray.ALLOW_COORDINATE_WRITE);
        surf.setCoordinateIndices(0, pIndex);
        if (!p.get(BACKGROUND)) {
            surf.setColorIndices(0, cIndex);
            if (normals != null) {
                surf.setCapability(IndexedLineStripArray.ALLOW_NORMAL_READ);
                surf.setCapability(IndexedLineStripArray.ALLOW_NORMAL_WRITE);
                surf.setNormalIndices(0, pIndex);
            }
        }
        surfaces = new OpenShape3D();
        updateColors(p);
        updateCoords(p);
        surfaces.setCapability(Shape3D.ENABLE_PICK_REPORTING);
        surfaces.setCapability(Shape3D.ALLOW_GEOMETRY_READ);
        surfaces.setCapability(Shape3D.ALLOW_GEOMETRY_WRITE);
        surfaces.setCapability(Shape3D.ALLOW_APPEARANCE_READ);
        surfaces.setCapability(Shape3D.ALLOW_APPEARANCE_WRITE);
        surfaces.setCapability(Geometry.ALLOW_INTERSECT);
        surfaces.setCapability(Node.ALLOW_LOCAL_TO_VWORLD_READ);
        surfaces.addGeometry(surf);
        outGroup.addChild(surfaces);
        outObj.addNode(outGroup);
        outObj.setExtents(inField.getPreferredExtents());
    }

    private void updateCoords(Parameters pc)
    {
        float scale = pc.get(SCALE);
        float[] tVerts = gt.getVerts();
        float[] tNormals = gt.getNormals();
        float[] coords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords().getData();
        for (int k = 0, ivert = 0; k < nGlyphs; k++) {
            float[] p = new float[3];
            int k0 = glyphV[2 * k];
            int k1 = glyphV[2 * k + 1];
            float sn0, sn1;
            float pm = 0;
            int m = -1;
            float vn, wn;
            for (int i = 0; i < 3; i++) {
                p[i] = coords[3 * k1 + i] - coords[3 * k0 + i];
                if (abs(p[i]) > pm) {
                    m = i;
                    pm = abs(p[i]);
                }
            }
            float[] v = new float[3];
            float[] w = new float[3];
            if (m == -1) {
                for (int i = 0; i < p.length; i++)
                    v[i] = w[i] = 0;
                vn = wn = sn0 = sn1 = 0;
            } else {
                if (m != 0) {
                    w[0] = 0;
                    w[1] = p[2];
                    w[2] = -p[1];
                } else {
                    w[0] = -p[2];
                    w[1] = 0;
                    w[2] = p[0];
                }
                v[0] = p[1] * w[2] - p[2] * w[1];
                v[1] = p[2] * w[0] - p[0] * w[2];
                v[2] = p[0] * w[1] - p[1] * w[0];
                vn = (float) (1 / sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]));
                w[0] = p[1] * v[2] - p[2] * v[1];
                w[1] = p[2] * v[0] - p[0] * v[2];
                w[2] = p[0] * v[1] - p[1] * v[0];
                wn = (float) (1 / sqrt(w[0] * w[0] + w[1] * w[1] + w[2] * w[2]));
                if (data != null) {
                    sn0 = abs(data.getFloatElement(k0)[0] * scale);
                    sn1 = abs(data.getFloatElement(k1)[0] * scale);
                } else
                    sn0 = sn1 = scale;
            }
            for (int l = 0; l < gt.getNverts(); l += 2, ivert += 6)
                for (int i = 0; i < 3; i++) {
                    float t = vn * tVerts[3 * l]     * v[i] + wn * tVerts[3 * l + 1] * w[i];
                    verts[ivert + i]       = sn0 * t + coords[3 * k0 + i];
                    verts[ivert + 3 + i]   = sn1 * t + coords[3 * k1 + i];
                    normals[ivert + i]     =
                    normals[ivert + 3 + i] =
                            vn * tNormals[3 * l] * v[i] + wn * tNormals[3 * l + 1] * w[i];
                }
        }
        surf.setCoordinates(0, verts);
        if (!pc.get(BACKGROUND) && normals != null)
            surf.setNormals(0, normals);
    }

    public void updateColors(Parameters p)
    {
        app = new OpenAppearance();
        app.setColoringAttributes(new ColoringAttributes(bgrF[0], bgrF[1], bgrF[2], 0));
        if (colors == null || 6 * nGlyphs != colors.length)
            colors = new byte[6 * nGlyphs];
        if (p.get(BACKGROUND)) {
            app.setMaterial(null);
            app.setPolygonAttributes(new PolygonAttributes(
                    PolygonAttributes.POLYGON_FILL,
                    PolygonAttributes.CULL_FRONT, 0.f, true));
        } else {
            OpenMaterial mat = new OpenMaterial();
            mat.setDiffuseColor(renderingParams.getDiffuseColor());
            mat.setShininess(15.f);
            mat.setColorTarget(OpenMaterial.AMBIENT_AND_DIFFUSE);
            app.setMaterial(mat);
            app.setPolygonAttributes(new PolygonAttributes(
                    PolygonAttributes.POLYGON_FILL,
                    PolygonAttributes.CULL_NONE, 0.f, true));
            colors = ColorMapper.mapColorsIndexed(inField, dataMappingParams, glyphV, dataMappingParams.getDefaultColor(), colors);
            surf.setColors(0, colors);
        }
        surfaces.setAppearance(app);
    }

    protected void makeIndices()
    {
        for (int s = 0, ivert = 0; s < inField.getNCellSets(); s++) {
            CellArray seg = inField.getCellSet(s).getBoundaryCellArray(CellType.SEGMENT);
            if (seg == null || seg.getNCells() == 0) {
                seg = inField.getCellSet(s).getCellArray(CellType.SEGMENT);
                if (seg == null || seg.getNCells() == 0)
                    continue;
            }
            int[] segNodes = seg.getNodes();
            for (int k = 0; k < seg.getNCells(); k++, ivert += 2) {
                glyphV[ivert] = segNodes[2 * k];
                glyphV[ivert + 1] = segNodes[2 * k + 1];
            }
        }
        int istrip = 0, iind = 0, ivert = 0, icol = 0;
        for (int n = 0; n < nGlyphs; n++) {
            for (int i = 0; i < gt.getNstrips(); i++) {
                strips[istrip] = gt.getStrips()[i];
                istrip += 1;
            }
            for (int i = 0; i < gt.getNinds(); i++) {
                pIndex[iind] = ivert + gt.getPntsIndex()[i];
                cIndex[iind] = icol + i % 2;
                iind += 1;
            }
            ivert += gt.getNverts();
            icol += 2;
        }
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(SELECTED_COMPONENT, 0),
            new Parameter<>(BACKGROUND, false),
            new Parameter<>(SCALE, .1f),
            new Parameter<>(LEVEL_OF_DETAILS, 10),
            new Parameter<>(META_SCHEMA, null),
            new Parameter<>(META_SCALE_MAX, .1f),
            new Parameter<>(META_BGR_ENABLED, true),};
    }

    private void validateParamsAndSetSmart(boolean reset)
    {
        parameters.setParameterActive(false);
        float max = 1;
        if (inField.getComponent(parameters.get(SELECTED_COMPONENT)) != null)
            max = (float) inField.getComponent(parameters.get(SELECTED_COMPONENT)).getPreferredMaxValue();
        float[][] ext = inField.getPreferredExtents();
        double diam = 0;
        for (int i = 0; i < 3; i++)
            diam += (ext[1][i] - ext[0][i]) * (ext[1][i] - ext[0][i]);
        if (max <= 0)
            max = .001f;
        float smax = (float) (sqrt(diam / 30) / max);
        parameters.set(META_SCALE_MAX, smax);
        if (reset) {
            parameters.set(META_SCHEMA, inField.getSchema());
            if (!isFromVNA()) {
                parameters.set(SCALE, smax / 10);
            }
        }
        parameters.set(META_BGR_ENABLED, true);
        for (int i = 0; i < inField.getCellSets().size(); i++) {
            for (int j = 0; j < inField.getCellSet(i).getCellArrays().length; j++) {
                if (j == 1) continue;
                if (inField.getCellSet(i).getCellArrays()[j] != null) {
                    parameters.set(META_BGR_ENABLED, false);
                    break;
                }

            }
        }
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            IrregularField newField = ((VNIrregularField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA() && (inField == null || inField.getTimestamp() != newField.getTimestamp());
            inField = newField;
            boolean isDifferentComponent;
            Parameters p;
            synchronized (parameters) {
                validateParamsAndSetSmart(isFromVNA() || isDifferentField);
                p = parameters.getReadOnlyClone();
                int newComponent = p.get(SELECTED_COMPONENT);
                isDifferentComponent = !(component == newComponent);
                component = newComponent;
            }

            createTubeGlyphs(p);

            if (isDifferentField || isFromVNA()) {
                dataMappingParams.getColorMap0().getComponentRange().setContainer(inField);//setComponentSchema(inField.getSchema());
                dataMappingParams.getColorMap1().getComponentRange().setContainer(inField);//setComponentSchema(inField.getSchema());
            }
            notifyGUIs(p, isFromVNA() || isDifferentField, isDifferentComponent);

        }
    }
}
