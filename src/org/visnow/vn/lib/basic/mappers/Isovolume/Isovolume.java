/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.Isovolume;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import static org.visnow.vn.gui.widgets.RunButton.RunState.NO_RUN;
import org.visnow.vn.lib.gui.ComponentBasedUI.value.ComponentValue;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.field.subset.subvolume.IrregularFieldSplitter;
import org.visnow.vn.lib.utils.field.subset.subvolume.RegularFieldSplitter;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class Isovolume extends OutFieldVisualizationModule {

    /**
     *
     * inField - a 3D field to create isovolume; at least one scalar data
     * component must be present.
     * <p>
     * outField - isovolume field will be created by update method - can be
     * void, can contain no node data (geometry only)
     *
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected Field inField;

    protected IrregularField irregularInField;

    protected RegularField regularInField;
    protected GUI computeUI = null;
    protected boolean fromGUI = false;
    protected Params params;
    protected ComponentValue threshold;
    protected boolean debug = true;
    protected boolean ignoreUI = false;

    public Isovolume() {
        parameters = params = new Params();
        threshold = params.getThreshold();
        SwingInstancer.swingRunAndWait(new Runnable() {
            @Override
            public void run() {
                computeUI = new GUI();
            computeUI.setParams(params);
            ui.addComputeGUI(computeUI);
            }
        });
        outObj.setName("isovolume");
        params.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent evt) {
                if (ignoreUI || params.getRunState() == NO_RUN) {
                    ignoreUI = false;
                    return;
                }
                fromGUI = true;
                startAction();
            }
        });
        setPanel(ui);
    }
    
    @Override
    public void onActive() {
        if (getInputFirstValue("inField") != null) {
            if (!fromGUI) {
                ignoreUI = true;
                VNField inFld = (VNField) getInputFirstValue("inField");
                if (inFld != null) {
                    Field newInField = inFld.getField();
                    if (newInField == null || newInField.getNComponents() < 1) {
                        ignoreUI = false;
                        return;
                    }
                    if (inField == null || inField != newInField) {
                        inField = newInField;
                        threshold.setContainerSchema(inField.getSchema());
                        outField = outIrregularField = null;
                        if (params.getRunState() == NO_RUN) {
                            prepareOutputGeometry();
                            show();
                        }
                    }
                    ignoreUI = false;
                }
            }
            fromGUI = false;
            if (params.getRunState() == NO_RUN)
                return;
            params.setRunState(NO_RUN);
            
            DataArray isoDataArray = inField.getComponent(threshold.getComponentName());
            float thr = threshold.getValue();
            if (inField instanceof IrregularField) 
                outIrregularField = IrregularFieldSplitter.splitField((IrregularField)inField, isoDataArray.getRawFloatArray(), isoDataArray.getVectorLength(),
                                                                     thr, params.getType() > 0);
            else
                outIrregularField = RegularFieldSplitter.splitField((RegularField)inField, isoDataArray.getRawFloatArray(), isoDataArray.getVectorLength(),
                                                                     thr, params.getType() > 0);
            if (outIrregularField != null) {
                setOutputValue("outField", new VNIrregularField(outIrregularField));
            } else {
                setOutputValue("outField", null);
            }
            outField = outIrregularField;
            prepareOutputGeometry();
            show();
        }
    }
}
