/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.Skeletonizer;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.apache.log4j.Logger;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.lib.utils.events.MessagedChangeEvent;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class GUI extends javax.swing.JPanel
{
    private static final Logger LOGGER = Logger.getLogger(GUI.class);
    
    protected RegularField inField;
    protected SkeletonizerParams params = new SkeletonizerParams();
    protected DataArray segmentedData;
    protected ChangeListener activityListener = new ChangeListener()
    {
        public void stateChanged(ChangeEvent evt)
        {
            if (evt instanceof MessagedChangeEvent)
                activityLabel.setText(((MessagedChangeEvent) evt).getMessage());
        }
    };

    /**
     * Creates new form SkeletonizerUI
     */
    public GUI()
    {
        initComponents();
        dataSelector.setScalarComponentsOnly(true);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        dataSelector = new org.visnow.vn.lib.gui.DataComponentSelector();
        jPanel1 = new javax.swing.JPanel();
        aboveButton = new javax.swing.JRadioButton();
        belowButton = new javax.swing.JRadioButton();
        thrSlider = new org.visnow.vn.gui.widgets.FloatSlider();
        setPane = new javax.swing.JScrollPane();
        setList = new javax.swing.JList();
        lineLenThrSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        runButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        activityLabel = new javax.swing.JLabel();
        computedLineLengthThrSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        diameterThresholdSlder = new org.visnow.vn.gui.widgets.FloatSlider();
        minCenterDepthSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        jPanel2 = new javax.swing.JPanel();

        setBorder(javax.swing.BorderFactory.createEmptyBorder(4, 4, 4, 4));
        setMinimumSize(new java.awt.Dimension(138, 551));
        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(158, 560));
        setLayout(new java.awt.GridBagLayout());

        dataSelector.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                dataSelectorStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 0, 0);
        add(dataSelector, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        buttonGroup1.add(aboveButton);
        aboveButton.setSelected(true);
        aboveButton.setText("above");
        aboveButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                aboveButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel1.add(aboveButton, gridBagConstraints);

        buttonGroup1.add(belowButton);
        belowButton.setText("below");
        belowButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                belowButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        jPanel1.add(belowButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 0, 0);
        add(jPanel1, gridBagConstraints);

        thrSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "skeletonize area over", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        thrSlider.setMax(255.0F);
        thrSlider.setVal(128.0F);
        thrSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                thrSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 0, 0);
        add(thrSlider, gridBagConstraints);

        setPane.setViewportBorder(javax.swing.BorderFactory.createTitledBorder("skeletonize subsets"));
        setPane.setPreferredSize(new java.awt.Dimension(9, 64));
        setPane.setRequestFocusEnabled(false);

        setList.setModel(new javax.swing.AbstractListModel()
        {
            String[] strings = { " ", " ", " ", " " };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        setList.setMaximumSize(new java.awt.Dimension(200, 200));
        setList.setMinimumSize(new java.awt.Dimension(6, 40));
        setList.setPreferredSize(new java.awt.Dimension(6, 100));
        setList.addListSelectionListener(new javax.swing.event.ListSelectionListener()
        {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt)
            {
                setListValueChanged(evt);
            }
        });
        setPane.setViewportView(setList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 0, 0);
        add(setPane, gridBagConstraints);

        lineLenThrSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        lineLenThrSlider.setGlobalMin(1);
        lineLenThrSlider.setMax(200);
        lineLenThrSlider.setMin(1);
        lineLenThrSlider.setSubmitOnAdjusting(false);
        lineLenThrSlider.setValue(100);
        lineLenThrSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("Display segments longer than"));
        lineLenThrSlider.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                lineLenThrSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 0, 0);
        add(lineLenThrSlider, gridBagConstraints);

        runButton.setText("run");
        runButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                runButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 0, 0);
        add(runButton, gridBagConstraints);

        jLabel1.setText("status:");
        jLabel1.setPreferredSize(new java.awt.Dimension(20, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 0, 0);
        add(jLabel1, gridBagConstraints);

        activityLabel.setText("inactive");
        activityLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        activityLabel.setMaximumSize(new java.awt.Dimension(48, 100));
        activityLabel.setMinimumSize(new java.awt.Dimension(48, 60));
        activityLabel.setOpaque(true);
        activityLabel.setPreferredSize(new java.awt.Dimension(20, 60));
        activityLabel.setRequestFocusEnabled(false);
        activityLabel.setVerifyInputWhenFocusTarget(false);
        activityLabel.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 0, 0);
        add(activityLabel, gridBagConstraints);

        computedLineLengthThrSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        computedLineLengthThrSlider.setMax(100);
        computedLineLengthThrSlider.setMin(0);
        computedLineLengthThrSlider.setSubmitOnAdjusting(false);
        computedLineLengthThrSlider.setValue(20);
        computedLineLengthThrSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("Drop segments shorter than"));
        computedLineLengthThrSlider.setVisible(false);
        computedLineLengthThrSlider.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                computedLineLengthThrSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 0, 0);
        add(computedLineLengthThrSlider, gridBagConstraints);

        diameterThresholdSlder.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "skeletonize area of diameter below", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        diameterThresholdSlder.setMax(50.0F);
        diameterThresholdSlder.setVal(50.0F);
        diameterThresholdSlder.setVisible(false);
        diameterThresholdSlder.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                diameterThresholdSlderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 0, 0);
        add(diameterThresholdSlder, gridBagConstraints);

        minCenterDepthSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        minCenterDepthSlider.setMax(50);
        minCenterDepthSlider.setMin(0);
        minCenterDepthSlider.setSubmitOnAdjusting(false);
        minCenterDepthSlider.setValue(0);
        minCenterDepthSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "<html>Centerpoints with<p> minimum distance from boundary", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 9))); // NOI18N
        minCenterDepthSlider.setVisible(false);
        minCenterDepthSlider.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                minCenterDepthSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 0, 0);
        add(minCenterDepthSlider, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 0.6;
        add(jPanel2, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void runButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_runButtonActionPerformed
    {//GEN-HEADEREND:event_runButtonActionPerformed
        params.setRecompute(SkeletonizerParams.SKELETONIZE);
        params.setActive(true);
    }//GEN-LAST:event_runButtonActionPerformed

    private void setListValueChanged(javax.swing.event.ListSelectionEvent evt)//GEN-FIRST:event_setListValueChanged
    {//GEN-HEADEREND:event_setListValueChanged
        params.setSets(setList.getSelectedIndices());
    }//GEN-LAST:event_setListValueChanged

    private void thrSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_thrSliderStateChanged
    {//GEN-HEADEREND:event_thrSliderStateChanged
        if (!thrSlider.isAdjusting())
            params.setThreshold(thrSlider.getVal());
    }//GEN-LAST:event_thrSliderStateChanged

    private void dataSelectorStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_dataSelectorStateChanged
    {//GEN-HEADEREND:event_dataSelectorStateChanged
        thrSlider.setMinMax((float)inField.getComponent(dataSelector.getComponent()).getPreferredMinValue(),
                            (float)inField.getComponent(dataSelector.getComponent()).getPreferredMaxValue());
        params.setComponent(dataSelector.getComponent());
    }//GEN-LAST:event_dataSelectorStateChanged

    private void aboveButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_aboveButtonActionPerformed
    {//GEN-HEADEREND:event_aboveButtonActionPerformed
        params.setAbove(aboveButton.isSelected());
        thrSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(aboveButton.isSelected() ? "skeletonize area over" : "skeletonize area under"));
    }//GEN-LAST:event_aboveButtonActionPerformed

    private void belowButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_belowButtonActionPerformed
    {//GEN-HEADEREND:event_belowButtonActionPerformed
        params.setAbove(aboveButton.isSelected());
        thrSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(aboveButton.isSelected() ? "skeletonize area over" : "skeletonize area under"));
    }//GEN-LAST:event_belowButtonActionPerformed

    private void lineLenThrSliderUserAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_lineLenThrSliderUserAction
    {//GEN-HEADEREND:event_lineLenThrSliderUserAction
    }//GEN-LAST:event_lineLenThrSliderUserAction

    private void lineLenThrSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_lineLenThrSliderStateChanged
    {//GEN-HEADEREND:event_lineLenThrSliderStateChanged

    }//GEN-LAST:event_lineLenThrSliderStateChanged

    private void computedLineLengthThrSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_computedLineLengthThrSliderStateChanged
    {//GEN-HEADEREND:event_computedLineLengthThrSliderStateChanged

    }//GEN-LAST:event_computedLineLengthThrSliderStateChanged

    private void computedLineLengthThrSliderUserAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_computedLineLengthThrSliderUserAction
    {//GEN-HEADEREND:event_computedLineLengthThrSliderUserAction
        // TODO add your handling code here:
    }//GEN-LAST:event_computedLineLengthThrSliderUserAction

    private void diameterThresholdSlderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_diameterThresholdSlderStateChanged
    {//GEN-HEADEREND:event_diameterThresholdSlderStateChanged
        if (!diameterThresholdSlder.isAdjusting())
            params.setMaxRadius((Float) diameterThresholdSlder.getVal());
    }//GEN-LAST:event_diameterThresholdSlderStateChanged

    private void minCenterDepthSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_minCenterDepthSliderStateChanged
    {//GEN-HEADEREND:event_minCenterDepthSliderStateChanged

    }//GEN-LAST:event_minCenterDepthSliderStateChanged

    private void minCenterDepthSliderUserAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_minCenterDepthSliderUserAction
    {//GEN-HEADEREND:event_minCenterDepthSliderUserAction
        // TODO add your handling code here:
    }//GEN-LAST:event_minCenterDepthSliderUserAction

    private void minCenterDepthSliderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_minCenterDepthSliderUserChangeAction
    {//GEN-HEADEREND:event_minCenterDepthSliderUserChangeAction
            params.setMinCenterDepth((Float) minCenterDepthSlider.getValue());
    }//GEN-LAST:event_minCenterDepthSliderUserChangeAction

    private void computedLineLengthThrSliderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_computedLineLengthThrSliderUserChangeAction
    {//GEN-HEADEREND:event_computedLineLengthThrSliderUserChangeAction
            params.setMinComputedSegLen((Integer) computedLineLengthThrSlider.getValue());
    }//GEN-LAST:event_computedLineLengthThrSliderUserChangeAction

    private void lineLenThrSliderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_lineLenThrSliderUserChangeAction
    {//GEN-HEADEREND:event_lineLenThrSliderUserChangeAction

            params.setMinSegLen((int) lineLenThrSlider.getValue());
    }//GEN-LAST:event_lineLenThrSliderUserChangeAction

    public void setParams(SkeletonizerParams params)
    {
        this.params = params;
    }

    /**
     * Set the value of segmentedData
     *
     * @param segmentedData new value of segmentedData
     */
    public void setSegmentedData(DataArray segmentedData)
    {
        this.segmentedData = segmentedData;
        String[] names = segmentedData.getUserData();
        if (names == null || names.length < 2 || !names[0].equalsIgnoreCase("MAP")) {
            params.setSegmented(false);
            thrSlider.setEnabled(true);
            thrSlider.setVisible(true);
            setPane.setVisible(false);
            params.setSets(null);
            return;
        }
        String[] listNames = new String[names.length - 1];
        for (int i = 1; i < names.length; i++) {
            String[] item = names[i].split(":");
            if (item.length == 2)
                try {
                    listNames[Integer.parseInt(item[0])] = item[1];
                } catch (Exception e) {
                    System.out.println("bad item " + names[i] + " : " + item[0] + " cannot be parsed as int");
                }
        }
        setList.setListData(listNames);
        int[] sInd = new int[listNames.length - 2];
        for (int i = 0; i < sInd.length; i++)
            sInd[i] = i + 2;
        setList.setSelectedIndices(sInd);
        thrSlider.setVisible(false);
        setPane.setVisible(true);
        if (params != null) {
            params.setSegmented(true);
            params.setNSets(names.length);
            params.setSets(setList.getSelectedIndices());
        }
    }

    public void setInfield(RegularField inField)
    {
        this.inField = inField;
        dataSelector.setDataSchema(inField.getSchema());
        dataSelector.setSelectedIndex(0);
        thrSlider.setMinMax((float)inField.getComponent(dataSelector.getComponent()).getPreferredMinValue(),
                            (float)inField.getComponent(dataSelector.getComponent()).getPreferredMaxValue());
        params.setComponent(dataSelector.getComponent());
    }

    public ChangeListener getActivityListener()
    {
        return activityListener;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton aboveButton;
    private javax.swing.JLabel activityLabel;
    private javax.swing.JRadioButton belowButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private org.visnow.vn.gui.widgets.ExtendedSlider computedLineLengthThrSlider;
    private org.visnow.vn.lib.gui.DataComponentSelector dataSelector;
    private org.visnow.vn.gui.widgets.FloatSlider diameterThresholdSlder;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private org.visnow.vn.gui.widgets.ExtendedSlider lineLenThrSlider;
    private org.visnow.vn.gui.widgets.ExtendedSlider minCenterDepthSlider;
    private javax.swing.JButton runButton;
    private javax.swing.JList setList;
    private javax.swing.JScrollPane setPane;
    private org.visnow.vn.gui.widgets.FloatSlider thrSlider;
    // End of variables declaration//GEN-END:variables

}
