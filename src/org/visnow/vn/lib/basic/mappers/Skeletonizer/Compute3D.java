/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.Skeletonizer;

import java.util.Arrays;
import org.visnow.jscic.RegularField;
import org.visnow.jlargearrays.LargeArray;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Compute3D extends SkeletonCompute
{

    public Compute3D()
    {
    }

    public Compute3D(RegularField inField, SkeletonizerParams params)
    {
        this.params = params;
        setInField(inField);
    }

    private class ComputeSetBoundary3D implements Runnable
    {

        int iThread = 0, cSet = 0;

        public ComputeSetBoundary3D(int iThread, int cSet)
        {
            this.iThread = iThread;
            this.cSet = cSet;
        }

        @Override
        public void run()
        {
            for (int i2 = (iThread * dims[2]) / nThreads; i2 < ((iThread + 1) * dims[2]) / nThreads; i2++) {
                if (iThread == 0)
                    fireStatusChanged((i2 * .1f) / dims[2]);
                if (i2 == 0 || i2 == dims[2] - 1)
                    continue;
                for (int i1 = 1; i1 < dims[1] - 1; i1++)
                    for (int i0 = 1, i = (i2 * dims[1] + i1) * dims[0] + 1; i0 < dims[0] - 1; i0++, i++)
                        if (bData[i] == cSet)
                            boundaryDistance[i] = BIGVAL;
                        else {
                            for (int j = 0; j < nNeighb; j++)
                                if (bData[i + off[j]] == cSet) {
                                    boundaryDistance[i] = -1;
                                    nodeQueue.insert(i);
                                    break;
                                }
                        }
            }
        }
    }

    private class ComputeThresholdBoundary3D implements Runnable
    {

        int iThread = 0;

        public ComputeThresholdBoundary3D(int iThread)
        {
            this.iThread = iThread;
        }

        @Override
        public void run()
        {
            for (int i2 = (iThread * dims[2]) / nThreads; i2 < ((iThread + 1) * dims[2]) / nThreads; i2++) {
                if (iThread == 0)
                    fireStatusChanged((i2 * .1f) / dims[2]);
                if (i2 == 0 || i2 == dims[2] - 1)
                    continue;
                for (int i1 = 1; i1 < dims[1] - 1; i1++)
                    for (int i0 = 1, i = (i2 * dims[1] + i1) * dims[0] + 1; i0 < dims[0] - 1; i0++, i++) {
                        if (bData[i] != 0)
                            boundaryDistance[i] = BIGVAL;
                        else {
                            for (int j = 0; j < nNeighb; j++) {
                                if (bData[i + off[j]] != 0) {
                                    boundaryDistance[i] = -1;
                                    nodeQueue.insert(i);
                                    break;
                                }
                            }
                        }
                    }
            }
        }
    }

    @Override
    protected void findInnermostNodes()
    {
        if (params.getStartPoints() != null)
        {
            int[][] sp = params.getStartPoints();
            nCentrePoints = 0;
            centres = new int[sp.length];
            for (int[] sp1 : sp) {
                int k = (sp1[2] * dims[1] + sp1[1]) * dims[0] + sp1[0];
                if (boundaryDistance[k] > 0) {
                    centres[nCentrePoints] = k;
                    radii[nCentrePoints] = boundaryDistance[k];
                }
            }
            if (nCentrePoints > 0)
                return;
        }
        nCentrePoints = 0;
        Arrays.fill(bData, (byte) 0);
        for (int i2 = 1; i2 < dims[2] - 1; i2++)
            for (int i1 = 1; i1 < dims[1] - 1; i1++)
                for (int i0 = 1, i = (i2 * dims[1] + i1) * dims[0] + 1; i0 < dims[0] - 1; i0++, i++) {
                    short d = boundaryDistance[i];
                    if (d < initDiameter / 2)
                        continue;
                    boolean ismax = true;
                    for (int j = 0; j < nNeighb; j++)
                        if (bData[i + off[j]] == 1)
                            continue;
                        else if (boundaryDistance[i + off[j]] > d) {
                            ismax = false;
                            break;
                        } else if (boundaryDistance[i + off[j]] == d && bData[i] == 0) {
                            ismax = false;
                            bData[i] = 1;
                            break;
                        }
                    if (ismax) {
                        if (nCentrePoints >= centres.length) {
                            int[] tmpCentres = new int[2 * centres.length];
                            short[] tmpRadii = new short[2 * centres.length];
                            System.arraycopy(centres, 0, tmpCentres, 0, centres.length);
                            System.arraycopy(radii, 0, tmpRadii, 0, radii.length);
                            centres = tmpCentres;
                            radii = tmpRadii;
                        }
                        centres[nCentrePoints] = i;
                        radii[nCentrePoints] = boundaryDistance[i];
                        nCentrePoints += 1;
                    }
                }
    }

    @Override
    protected void setBoundaryData(int cSet)
    {
        bData = inData.getRawByteArray().getData();
        if (mask != null)
            for (int m = 0; m < mask.length(); m++)
                if (!mask.getBoolean(m))
                    bData[m] = 0;
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new ComputeSetBoundary3D(iThread, cSet));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            }catch (InterruptedException e) {
            }
        for (int i2 = 1; i2 < dims[2] - 1; i2++) {
            fireStatusChanged((i2 * .1f) / dims[2]);
            for (int i1 = 1; i1 < dims[1] - 1; i1++)
                for (int i0 = 1, i = (i2 * dims[1] + i1) * dims[0] + 1; i0 < dims[0] - 1; i0++, i++)
                    if (bData[i] == cSet) {
                        boundaryDistance[i] = BIGVAL;
                        for (int j = 0; j < nNeighb; j++)
                            if (bData[i + off[j]] != cSet) {
                                boundaryDistance[i] = -1;
                                nodeQueue.insert(i);
                                break;
                            }
                    }
        }
        fillOut3DMargins(boundaryDistance, dims, (byte) 0);
        // outd is now initialized by: 0 if outside on 1 voxel wide margim, -1 if on the boundary, MAXVAL if inside
        // nodeQueue is initialized as the list of indices of boundary voxels
    }

    @Override
    protected void setBoundaryData()
    {
        fireActivityChanged("finding volume boundary");
        if (above) {
            LargeArray sData = inData.getRawArray();
            for (int m = 0; m < ndata; m++)
                bData[m] = (sData.getDouble(m) > threshold) ? (byte) 0xff : 0;
        } else {
            LargeArray sData = inData.getRawArray();
            for (int m = 0; m < ndata; m++)
                bData[m] = (sData.getDouble(m) < threshold) ? (byte) 0xff : 0;
        }

        if (mask != null)
            for (int m = 0; m < mask.length(); m++)
                if (!mask.getBoolean(m))
                    bData[m] = 0;
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new ComputeThresholdBoundary3D(iThread));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            }catch (InterruptedException e) {
            }
        // bdata is now binary map of inside skeletonized area (255) and outside or masked (0)
        fillOut3DMargins(boundaryDistance, dims, (byte) 0);
        // outd is now initialized by: 0 if outside or on 1 voxel wide margin, -1 if on the boundary, MAXVAL if inside
        // nodeQueue is initialized as the list of indices of boundary voxels

    }
    
    public static void fillOut3DMargins(short[] data, int[] dims, short val)
    {
        int vlen = data.length / (dims[0] * dims[1] * dims[2]);
        if (data.length != vlen * dims[0] * dims[1] * dims[2]) {
            return;
        }
        int k = dims[0] - 1;
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i1 = 0; i1 < dims[1]; i1++) {
                data[(i2 * dims[1] + i1) * dims[0]] = data[(i2 * dims[1] + i1) * dims[0] + k] = val;
            }
        }
        k = dims[0] * (dims[1] - 1);
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[i2 * dims[1] * dims[0] + i0] = data[i2 * dims[1] * dims[0] + i0 + k] = val;
            }
        }
        k = dims[0] * dims[1] * (dims[2] - 1);
        for (int i1 = 0; i1 < dims[1]; i1++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[dims[0] * i1 + i0] = data[dims[0] * i1 + i0 + k] = val;
            }
        }
    }


    @Override
    protected void clearOutDMargins()
    {
        int k = dims[0] - 1;
        for (int i2 = 0; i2 < dims[2]; i2++)
            for (int i1 = 0; i1 < dims[1]; i1++)
                boundaryDistance[(i2 * dims[1] + i1) * dims[0]]
                    = boundaryDistance[(i2 * dims[1] + i1) * dims[0] + k] = 0;
        k = dims[0] * (dims[1] - 1);
        for (int i2 = 0; i2 < dims[2]; i2++)
            for (int i0 = 0; i0 < dims[0]; i0++)
                boundaryDistance[i2 * dims[1] * dims[0] + i0]
                    = boundaryDistance[i2 * dims[1] * dims[0] + i0 + k] = 0;
        k = dims[0] * dims[1] * (dims[2] - 1);
        for (int i1 = 0; i1 < dims[1]; i1++)
            for (int i0 = 0; i0 < dims[0]; i0++)
                boundaryDistance[dims[0] * i1 + i0]
                    = boundaryDistance[dims[0] * i1 + i0 + k] = 0;
    }

}
