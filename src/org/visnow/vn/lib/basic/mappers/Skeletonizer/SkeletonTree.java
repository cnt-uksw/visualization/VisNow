/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.Skeletonizer;

import java.util.LinkedList;
import java.util.List;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;

/**
 *
 * @author know
 */


public class SkeletonTree
{
    private SkeletonTree()
    {
    }
    
    
    public static IrregularField createTree(float[] coords, int[] segments, short[] radii)
    {
        IrregularField tree = null;
        List<int[]> treeList = new LinkedList<int[]>();
        boolean addedLast = false;
        int start = segments[0];
        int end = segments[1];
        for (int i = 2; i < segments.length; i += 2) {
            int n = segments[i];
            int n1 = segments[i + 1];
            if (n == end) {
                if (n1 == n + 1) {
                    end = segments[i + 1];
                } else {
                    for (int j = 0; j < treeList.size(); j++) {
                        int[] seg = treeList.get(j);
                        if (n1 >= seg[0] && n1 <= seg[1]) {
                            treeList.add(j + 1, new int[]{n1, seg[1], seg[2]});
                            seg[1] = n1;
                            seg[2] = -1;
                            break;
                        }
                    }
                    treeList.add(new int[]{start, end, n1});
                    i += 2;
                    if (i >= segments.length)
                    {
                        addedLast = true;
                        break;
                    }
                    start = segments[i];
                    end = segments[i + 1];
                }
            } else {
                treeList.add(new int[]{start, end, -1});
                start = n;
                end = n1;
            }
        }
        if (!addedLast)
            treeList.add(new int[]{start, end, -1});

        int nNodes = 2 * treeList.size();
        tree = new IrregularField(nNodes);
        float[] outCoords = new float[3 * nNodes];
        float[] lengths   = new float[nNodes / 2];
        float[] diams     = new float[nNodes / 2];
        int[] nodes = new int[nNodes];
        for (int i = 0; i < nodes.length; i++) 
            nodes[i] = i;
        for (int j = 0; j < treeList.size(); j++) {
            int[] seg = treeList.get(j);
            System.arraycopy(coords, 3*seg[0],outCoords, 6 * j, 3);
            int k = seg[1];
            if (seg[2] != -1)
                k = seg[2];
            System.arraycopy(coords, 3*k,outCoords, 6 * j + 3, 3);
            float d = 0, l = 0;
            for (int i = seg[0]; i < seg[1]; i++) {
                d += radii[i];
                for (int m = 0; m < 3; m++) {
                    l +=  (float)Math.sqrt((coords[3 * i + 3 + m] - coords[3 * i + m]) * 
                         (coords[3 * i + 3 + m] - coords[3 * i + m]));
                }
            }
            lengths[j] = l;
            diams[j]   = d / (seg[1] -seg[0] + 1);
        }
        tree.setCurrentCoords(new FloatLargeArray(outCoords));
        int[] cellDataIndices = new int[nNodes /2];
        for (int i = 0; i < cellDataIndices.length; i++) 
            cellDataIndices[i] = i;
        CellArray edges = new CellArray(CellType.SEGMENT, nodes, null, cellDataIndices);
        CellSet cs = new CellSet();
        cs.addCells(edges);
        cs.addComponent(DataArray.create(diams, 1, "diameters"));
        cs.addComponent(DataArray.create(lengths, 1, "lengths"));
        tree.addCellSet(cs);
        return tree;
    }
}
