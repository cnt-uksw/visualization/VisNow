/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.RegularFieldSlice;

import java.util.Arrays;
import org.apache.log4j.Logger;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.field.SliceRegularField;

import static org.visnow.vn.lib.basic.mappers.RegularFieldSlice.RegularFieldSliceShared.*;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class RegularFieldSlice extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    private static final Logger LOGGER = Logger.getLogger(RegularFieldSlice.class);

    protected RegularField inField = null;
    protected RegularFieldSliceGUI computeUI = null;

    private boolean previousRecalculateMinMax = false;

    public RegularFieldSlice()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (parameters.get(ADJUSTING)) {
                    SliceRegularField.sliceUpdate(inField, parameters.get(SELECTED_AXIS), parameters.get(SLICE_INDEX), !parameters.get(RECALCULATE_MIN_MAX), outRegularField);
                    updateMinMax();
                    if (parameters.get(RECALCULATE_MIN_MAX)) {
                        String componentName = dataMappingParams.getColorMap0().getDataComponentName();
                        if (outRegularField.getComponent(componentName) != null) {
                            dataMappingParams.getColorMap0().getComponentRange().setActive(false);
                            dataMappingParams.getColorMap0().getComponentRange().setLowUp(
                                    (float) outRegularField.getComponent(componentName).getPreferredMinValue(),
                                    (float) outRegularField.getComponent(componentName).getPreferredMaxValue());
                            dataMappingParams.getColorMap0().getComponentRange().restoreActive();
                        }
                    }
                    regularFieldGeometry.updateDataMap();
                    regularFieldGeometry.updateCoords();
                } else
                    startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new RegularFieldSliceGUI();
            }
        });
        computeUI.setParameters(parameters);
        ui.addComputeGUI(computeUI);
        setPanel(ui);
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(SELECTED_AXIS, 2),
            new Parameter<>(RECALCULATE_MIN_MAX, false),
            new Parameter<>(SLICE_INDEX, 1),
            new Parameter<>(ADJUSTING, false),
            new Parameter<>(META_FIELD_DIMENSION_LENGTHS, new int[]{2, 2, 2})
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        int[] dims = inField.getDims();
        parameters.set(META_FIELD_DIMENSION_LENGTHS, dims);
        int axis = parameters.get(SELECTED_AXIS);

        if (parameters.get(SLICE_INDEX) >= dims[axis] || resetParameters) parameters.set(SLICE_INDEX, getSmartSlice(dims[axis]));

        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully);
    }

    private void updateMinMax()
    {
        if(outRegularField == null)
            return;
        if (parameters.get(RECALCULATE_MIN_MAX)) {
            for (int i = 0; i < outRegularField.getNComponents(); i++) {
                outRegularField.getComponent(i).recomputeStatistics();
            }
        }
        if (parameters.get(RECALCULATE_MIN_MAX) != previousRecalculateMinMax) {
            for (int i = 0; i < outField.getNComponents(); i++) {
                outField.getComponent(i).setAutoResetMapRange(true);
            }
        }
        previousRecalculateMinMax = parameters.get(RECALCULATE_MIN_MAX);
    }

    /**
     * Computation GUI setter.
     * Set of a panel displayed in the "Computation" tab.
     *
     * @param computeUI alternative GUI (alternative to default one instantiated in constructor)
     */
    public void setComputeUI(RegularFieldSliceGUI computeUI) {
        this.computeUI = computeUI;
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            RegularField newInField = ((VNRegularField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA() && (inField == null || !Arrays.equals(inField.getDims(), newInField.getDims()));
            inField = newInField;

            Parameters p;
            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentField);
                p = parameters.getReadOnlyClone();
            }
            notifyGUIs(p, isFromVNA() || isDifferentField, false);

            int axis = parameters.get(SELECTED_AXIS);
            outField = outRegularField = SliceRegularField.sliceField(inField, axis, parameters.get(SLICE_INDEX), !parameters.get(RECALCULATE_MIN_MAX));
            outField.setName(inField.getName());
            setOutputValue("outField", new VNRegularField(outRegularField));
            prepareOutputGeometry();
            show();

            for (int i = 0; i < outField.getNComponents(); i++) {
                outRegularField.getComponent(i).setAutoResetMapRange(false);
            }
        }
    }
}
