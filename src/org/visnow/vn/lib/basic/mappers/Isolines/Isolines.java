/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.Isolines;

import org.visnow.vn.lib.utils.isolines.IrregularFieldIsolines;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.Arrays;
import java.util.ArrayList;
import org.jogamp.java3d.LineAttributes;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.geometry2D.GeometryObject2D;
import org.visnow.jscic.utils.RegularFieldNeighbors;
import org.visnow.vn.lib.utils.field.MergeIrregularField;
import org.visnow.vn.lib.utils.field.SliceRegularField;
import org.visnow.vn.lib.utils.geometry2D.GeometryObject2DStruct;
import org.visnow.vn.lib.utils.isolines.RegularFieldIsolines;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class Isolines extends OutFieldVisualizationModule
{

    /**
     * Creates a new instance of CreateGrid
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected VNField input = null;
    protected RegularFieldIsolines regularFieldIsolines = null;
    protected IrregularFieldIsolines irregularFieldIsolines = null;
    protected IsolinesGUI computeUI = null;
    protected Field inField = null;
    protected Isolines2D out2D;
    protected int component = -1;
    private boolean fromIn = false;
    protected IsolinesParams params;
    protected int[] size;

    public Isolines()
    {
        parameters = params = new IsolinesParams();
        params.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                if (inField != null && !fromIn)
                    startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new IsolinesGUI();
                computeUI.setParams(params);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
        outObj.setName("isolines");
    }

    @Override
    public void onDelete()
    {
        detach();
        //      ((Container) getPanel()).removeAll();
        ui = null;
        regularFieldIsolines = null;
        out2D = null;
    }

    public class Isolines2D extends GeometryObject2D implements Cloneable
    {

        private ArrayList<float[]>[] data = null;
        private float[][] coords = null;
        private Color[] colors = null;
        private float lineWidth = 1;
        private int lineStyle = LineAttributes.PATTERN_SOLID;

        public Isolines2D(String name)
        {
            super(name);
        }

        public Isolines2D(String name, ArrayList<float[]>[] data, int[] dims)
        {
            super(name);
            if (regularFieldIsolines != null) {
                setData(data, dims);
            }
        }

        public Isolines2D(String name, float[][] coords)
        {
            super(name);
            if (regularFieldIsolines == null) {
                return;
            }
            this.coords = coords;
        }

        public void setColors(Color[] colors)
        {
            this.colors = colors;
        }

        public void setLineWidth(float lineWidth)
        {
            this.lineWidth = lineWidth;
        }

        private void setLineStyle(int lineStyle)
        {
            this.lineStyle = lineStyle;
        }

        public final void setData(ArrayList<float[]>[] data, int[] dims)
        {
            this.data = data;
            this.width = dims[0];
            this.height = dims[1];
        }

        public int getDataSize()
        {
            if (data != null)
                return this.data.length;
            return 0;
        }

        @Override
        public void drawLocal2D(Graphics2D g, AffineTransform at)
        {
            if (regularFieldIsolines == null || colors == null) {
                return;
            }

            switch (lineStyle) {
                case LineAttributes.PATTERN_DASH:
                    g.setStroke(new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 2.0f, new float[]{
                        10.0f, 5.0f
                    }, 0.0f));
                    break;
                case LineAttributes.PATTERN_DASH_DOT:
                    g.setStroke(new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 2.0f, new float[]{
                        10.0f, 5.0f, lineWidth, 5.0f
                    }, 0.0f));
                    break;
                case LineAttributes.PATTERN_DOT:
                    g.setStroke(new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 2.0f, new float[]{
                        lineWidth, 5.0f
                    }, 0.0f));
                    break;
                default:
                    g.setStroke(new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 2.0f));
                    break;
            }

            g.translate(at.getTranslateX(), at.getTranslateY());
            if (data == null) {
                if (coords == null) {
                    g.translate(-at.getTranslateX(), -at.getTranslateY());
                    return;
                }
                for (int i = 0; i < coords.length; i++) {
                    g.setColor(colors[i]);
                    for (int j = 0; j < coords[i].length; j += 4) {
                        g.drawLine((int) ((coords[i][j] + 0.5) * at.getScaleX()), (int) ((height - coords[i][j + 1] - 0.5) * at.getScaleY()),
                                   (int) ((coords[i][j + 2] + 0.5) * at.getScaleX()), (int) ((height - coords[i][j + 3] - 0.5) * at.getScaleY()));
                    }
                }
                g.translate(-at.getTranslateX(), -at.getTranslateY());
                return;
            }

            for (int n = 0; n < data.length; n++) {
                g.setColor(colors[n]);
                ArrayList<float[]> vdata = data[n];
                for (int j = 0; j < vdata.size(); j++) {
                    float[] line = vdata.get(j);
                    int m = line.length / 2;
                    int[] ix = new int[m];
                    int[] iy = new int[m];
                    for (int i = 0; i < m; i++) {
                        ix[i] = (int) ((line[2 * i] + 0.5) * at.getScaleX());
                        iy[i] = (int) ((height - line[2 * i + 1] - 0.5) * at.getScaleY());
                    }
                    g.drawPolyline(ix, iy, m);
                }
            }
            g.translate(-at.getTranslateX(), -at.getTranslateY());
        }
    }

    
    private void setRange(float a, float b, float[] q, int[] l, float min, float scale, float weight)
    {
        int low, up;
        a = scale * (a - min);
        b = scale * (b - min);
        if (b < a) {
            float t = a;
            a = b;
            b = t;
        }
        low =  Math.max(0, (int)(a + .5));
        up  =  Math.min((int)(b + 1.5), q.length);
        if (low == up)
            return;
        float t = 1/(b - a);
        for (int i = low; i < up; i++) {
            q[i] += weight * t;
            l[i] += weight;
        }
    }
    
    public DataArray thrQuality()
    {
        int THR_QUALITY_PRECISION = 1024;
        float[] quality = new float[THR_QUALITY_PRECISION];
        int[] length = new int[THR_QUALITY_PRECISION];
        int[] dims = ((RegularField)inField).getDims();
        float[] vals = inField.getComponent(params.getComponent()).getVectorNorms().getData();
        float[] thresholds = params.getThresholds();
        if (thresholds == null || outIrregularField == null || outIrregularField.getNNodes() < 1)
            return null;
        float[] thrQuality = new float[(int)outIrregularField.getNNodes()];
        float low = thresholds[0], up = thresholds[0];
        for (int i = 0; i < thresholds.length; i++) {
            if (low > thresholds[i]) low = thresholds[i];
            if (up  < thresholds[i]) up  = thresholds[i];
        }
        if (low == up) {
            low = (float)inField.getComponent(params.getComponent()).getMinValue();
            up  = (float)inField.getComponent(params.getComponent()).getMaxValue();
        }
        if (low == up) 
            return null;
        Arrays.fill(quality, 0);
        Arrays.fill(length, 0);
        float d = (THR_QUALITY_PRECISION - 1) / (up - low);

        int[] nbhrs = RegularFieldNeighbors.symmetricNeighbors(dims)[0];
        float[] weights = RegularFieldNeighbors.SYMMETRIC_WEIGHTS2D[0];
        for (int i = 1; i < dims[1] - 1; i++) 
            for (int j = 1, k = i * dims[0]; j < dims[0] - 1; j++, k++)
                for (int l = 0; l < nbhrs.length; l++) 
                    setRange(vals[k], vals[k + nbhrs[l]], quality, length, low, d, weights[l]);
        
        for (int i = 0; i < quality.length; i++)
            if (length[i] > 0 && quality[i] > 0)
                quality[i] = -(float)(Math.log(quality[i] / length[i]));
             else
                quality[i] = 0;
        float[] isoVals = outIrregularField.getComponent(0).getVectorNorms().getFloatData();
        for (int i = 0; i < isoVals.length; i++) 
            thrQuality[i] = quality[(int)(.5f + d * (isoVals[i] - low))];
        return DataArray.create(thrQuality, 1, "relative_threshold_quality");
    }


    public void update()
    {
        outIrregularField = null;
        outObj.clearAllGeometry();
        outGroup = null;
        if (inField != null) {
            DataArray da = inField.getComponent(params.getComponent());
            if (da.getPreferredMaxValue() > da.getPreferredMinValue() + 1e-6f) {
                if (inField instanceof RegularField) {
                    if (((RegularField)inField).getDimNum() == 2)
                        outIrregularField = RegularFieldIsolines.create((RegularField)inField, params);
                    else if (params.drawOnBoundary()) {
                        RegularField in = (RegularField)inField;
                        int[] dims = in.getDims();
                        outIrregularField =               RegularFieldIsolines.create(SliceRegularField.sliceField(in, 0, 0, false), params);
                        outIrregularField =  
                                MergeIrregularField.merge(outIrregularField, 
                                                          RegularFieldIsolines.create(SliceRegularField.sliceField(in, 0, dims[0] - 1, false), params), 
                                                          1, false);
                        outIrregularField =  
                                MergeIrregularField.merge(outIrregularField, 
                                                          RegularFieldIsolines.create(SliceRegularField.sliceField(in, 1, 0, false), params), 
                                                          2, false);
                        outIrregularField =  
                                MergeIrregularField.merge(outIrregularField, 
                                                          RegularFieldIsolines.create(SliceRegularField.sliceField(in, 1, dims[1] - 1, false), params), 
                                                          3, false);
                        outIrregularField =  
                                MergeIrregularField.merge(outIrregularField, 
                                                          RegularFieldIsolines.create(SliceRegularField.sliceField(in, 2, 0, false), params), 
                                                          4, false);
                        outIrregularField =  
                                MergeIrregularField.merge(outIrregularField, 
                                                          RegularFieldIsolines.create(SliceRegularField.sliceField(in, 2, dims[2] - 1, false), params), 
                                                          5, false);
                    }
                    else
                        outIrregularField = null;
                }
                else {
                    outIrregularField = IrregularFieldIsolines.create((IrregularField)inField, params);
                }
            }
            if (outIrregularField != null && outIrregularField.getNNodes() > 2) {
                if (params.isEstimateQuality() && inField instanceof RegularField && ((RegularField)inField).getDimNum() == 2)
                    outIrregularField.addComponent(thrQuality());
                outIrregularField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
                setOutputValue("outField", new VNIrregularField(outIrregularField));
                outField = outIrregularField;
                outField.setName(inField.getName());
                prepareOutputGeometry();
                irregularFieldGeometry.getCellSetGeometry(0).getDataMappingParams().removeRenderEventListener(renderEventListener);
                irregularFieldGeometry.getCellSetGeometry(0).getDataMappingParams().addRenderEventListener(renderEventListener);
                irregularFieldGeometry.getCellSetGeometry(0).getParams().getRenderingParams().removeRenderEventListener(renderEventListener);
                irregularFieldGeometry.getCellSetGeometry(0).getParams().getRenderingParams().addRenderEventListener(renderEventListener);
                irregularFieldGeometry.getColormapLegend(0).setThrTable(params.getThresholds(), inField.getComponent(params.getComponent()).getName());
                GeometryObject2DStruct isolinesStruct = new GeometryObject2DStruct(out2D);
                irregularFieldGeometry.getGeometryObj2DStruct().removeAllChildren();
                irregularFieldGeometry.getGeometryObj2DStruct().addChild(isolinesStruct);
                show();
            }
            else {
                setOutputValue("outField", null);
            }
                
        }
    }

    private RenderEventListener renderEventListener = new RenderEventListener()
    {
        @Override
        public void renderExtentChanged(RenderEvent e)
        {
//            updateDisplay2DParams();
        }
    };

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null ||
                ((VNField) getInputFirstValue("inField")).getField() == null) {
            inField = null;
            outField = null;
            return;
        }
        if (((VNField) getInputFirstValue("inField")).getField() != inField) {
            params.setActive(false);
            fromIn = true;
            outObj.clearAllGeometry();
            outGroup = null;
            inField = ((VNField) getInputFirstValue("inField")).getField();
            if (!inField.hasNumericComponent()) {
                inField = null;
                outField = null;
                return;
            }
            computeUI.setInField((VNField) getInputFirstValue("inField"));
            params.setActive(true);
            fromIn = false;
        }

        update();
    }
}
