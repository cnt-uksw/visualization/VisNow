/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.LineProbe;


import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class LineProbeShared
{
    public static enum ProbeType {GEOMETRIC, INDEX};
    public static final String SHOW_ACCUMULATED_STRING = "Output accumulated";
    public static final String ADD_SLICE_STRING = "Add slice";
    public static final String DEL_SLICE_STRING = "Remove slice";
    public static final String CLEAR_SLICES_STRING = "Clear slices";
    public static final String GRAPH_POSITION_STRING = "graphs position";
    public static final String INIT_MARGIN_STRING = "init margin";
    public static final String GAP_STRING = "gap";
    public static final String POINTER_LINE_STRING = "pointer line";
    public static final String PROBE_TYPE_STRING = "probe type";
    public static final String SELECTED_GRAPHS_STRING = "selected graphs";
    public static final String GRAPH_TITLE_STRING = "graph title";

    static final ParameterName<Boolean>    SHOW_ACCUMULATED = new ParameterName(SHOW_ACCUMULATED_STRING);
    static final ParameterName<Boolean>    ADD_SLICE = new ParameterName(ADD_SLICE_STRING);
    static final ParameterName<Boolean>    DEL_SLICE = new ParameterName(DEL_SLICE_STRING);
    static final ParameterName<Boolean>    CLEAR_SLICES = new ParameterName(CLEAR_SLICES_STRING);
    static final ParameterName<Position>   GRAPHS_POSITION = new ParameterName(GRAPH_POSITION_STRING);
    static final ParameterName<Boolean>    POINTER_LINE = new ParameterName(POINTER_LINE_STRING);
    static final ParameterName<ProbeType>  PROBE_TYPE = new ParameterName(PROBE_TYPE_STRING);
    static final ParameterName<Integer>    INIT_MARGIN = new ParameterName(INIT_MARGIN_STRING);
    static final ParameterName<Integer>    GAP = new ParameterName(GAP_STRING);
    static final ParameterName<boolean[]>  SELECTED_GRAPHS = new ParameterName(SELECTED_GRAPHS_STRING);
    static final ParameterName<String>     GRAPH_TITLE = new ParameterName(GRAPH_TITLE_STRING);
}
