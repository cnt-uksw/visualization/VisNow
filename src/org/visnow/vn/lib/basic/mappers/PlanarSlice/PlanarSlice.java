/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.PlanarSlice;

import java.util.Arrays;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jogamp.java3d.J3DGraphics2D;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.geometries.events.ColorEvent;
import org.visnow.vn.geometries.events.ColorListener;
import org.visnow.vn.geometries.events.ResizeEvent;
import org.visnow.vn.geometries.events.ResizeListener;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph;
import org.visnow.vn.geometries.objects.Geometry2D;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.PickEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.PickListener;
import static org.visnow.vn.lib.basic.mappers.PlanarSlice.PlanarSliceShared.*;
import static org.visnow.vn.lib.basic.mappers.PlanarSlice.PlanarSliceShared.ProbeType.*;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;
import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position;
import static org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position.*;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams.*;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.events.MouseRestingEvent;
import org.visnow.vn.lib.utils.events.MouseRestingListener;
import org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;
import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay;
import org.visnow.vn.lib.utils.probeInterfaces.Probe;
import org.visnow.vn.lib.utils.slicing.*;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class PlanarSlice extends OutFieldVisualizationModule
{

    
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    
    protected Field inField = null;
    
    protected IrregularField probeField = null;
    
    protected GUI computeUI = null;
    
    ComponentFeature isoComponent = new ComponentFeature(true, false, false, false, false, false);

    protected Probe currentProbe = null;
    protected BasicPlanarSlice geometricProbe;
    protected InteractiveGlyph geometricProbeGlyph;
    
    protected IndexProbe2D indexProbe;
    protected OpenBranchGroup indexProbeGlyph;
    protected IndexSliceParams indexProbeParams;
    
    protected OpenBranchGroup currentProbeGlyph;
    
    protected MultiSliceDisplay slicesDisplay = new MultiSliceDisplay();
    protected SliceDisplay pickedSlice = null;
    protected int windowWidth  = 500;
    protected int windowHeight = 500;
    protected boolean fromParams = false;
    
    private float[] minima, maxima;
    private boolean outputChanged = false;
    
    Geometry2D slicesGeometry = new Geometry2D()
        {
            @Override
            public void draw2D(J3DGraphics2D vGraphics, LocalToWindow ltw, int w, int h)
            {
                slicesDisplay.draw2D(vGraphics, ltw, w, h);
            }
        };
    
    protected void refreshDisplay()
    {
        slicesDisplay.updateDataMaps(ui.getPresentationGUI().getDataMappingGUI().getDataMappingParams());
        if (outObj.getRenderingWindow() != null)
            outObj.getRenderingWindow().refresh();
    }
    
    protected MouseRestingListener toolTipListener = new MouseRestingListener() {
        @Override
        public void mouseResting(MouseRestingEvent e)
        {
            slicesDisplay.showTooltip(e.getX(), e.getY(), e.getScreenX(), e.getScreenY());
            if (outObj.getRenderingWindow() != null)
                outObj.getRenderingWindow().refresh();
        }

        @Override
        public void stateChanged(ChangeEvent e)
        {
        }
    };
    
    protected RenderEventListener dataMapListener = new RenderEventListener() {
            @Override
            public void renderExtentChanged(RenderEvent e) {
                slicesDisplay.updateDataMaps(ui.getPresentationGUI().getDataMappingGUI().getDataMappingParams());
                refreshDisplay();
            }
        };
    
    protected ResizeListener resizeListener = new ResizeListener() {
        @Override
        public void resized(ResizeEvent e)
        {
            updateAddCapability();
        }
    };
    
    protected ParameterChangeListener parameterListener = new ParameterChangeListener()
    {
        @Override
        public void parameterChanged(String name)
        {
            if (name == null)
                return;
            fromParams = true;
            boolean noOutfield = outField == null;
            switch (name) {
            case ADD_SLICE_AREA_STRING:
                probeField = currentProbe.getSliceAreaField();
                if (probeField == null)
                    return;
                if (currentProbe == geometricProbe)
                    slicesDisplay.addDisplay(probeField, dataMappingParams,
                                             currentProbe.getPlaneCenter(),            
                                             currentProbe.getPlaneX(),
                                             currentProbe.getPlaneY(),
                                             currentProbe.getRX(), currentProbe.getRY());
                else
                    slicesDisplay.addDisplay(probeField, dataMappingParams,
                                             currentProbe.getPlaneCenter(),            
                                             currentProbe.getPlaneX(),
                                             currentProbe.getPlaneY(),
                                             Float.MAX_VALUE, Float.MAX_VALUE);
                pickedSlice = (SliceDisplay)slicesDisplay.getDisplays().
                                   get(slicesDisplay.getDisplays().size() - 1);
                for (ProbeDisplay slice : slicesDisplay.getDisplays())
                    slice.setSelected(false);
                pickedSlice.setSelected(true);
                computeUI.enableRemoveGraph(true);
                computeUI.updateGraphTitle(pickedSlice.getTitle());
                dataMappingParams = ui.getPresentationGUI().getDataMappingGUI().getDataMappingParams();
                updateAddCapability();
                outputChanged = true;
                startAction();
                break;
            case DEL_SLICE_STRING:
                if (pickedSlice != null) {
                    slicesDisplay.removeDisplay(pickedSlice);
                    updateValueRanges();
                    updateAddCapability();
                    pickedSlice = null;
                    computeUI.updateGraphTitle("");
                    outputChanged = true;
                    startAction();
                }
                break;
            case CLEAR_SLICES_STRING:
                outField = null;
                slicesDisplay.clearDisplays();
                updateAddCapability();
                pickedSlice = null;
                outputChanged = true;
                startAction();
                break;   
            case ORDER_SLICES_STRING:
                if (parameters.get(ORDER_SLICES)) {
                    slicesDisplay.sort();
                    startAction();
                    parameters.set(ORDER_SLICES, false);
                }
                break;
            case REFRESH_SLICES_STRING:
                if (parameters.get(REFRESH_SLICES)) {
                    doOutput();
                    parameters.set(REFRESH_SLICES, false);
                }
                break;
            case SLICE_POSITION_STRING:
                slicesDisplay.setProbesPosition(parameters.get(SLICE_POSITION));
                if (outObj.getRenderingWindow() != null)
                    outObj.getRenderingWindow().refresh();
                break;
            case POINTER_LINE_STRING:
                slicesDisplay.setPointerLine(parameters.get(POINTER_LINE));
                refreshDisplay();
                break;
            case TYPE_STRING:
                currentProbe.getGlyphGeometry().detach();
                if (geometricProbe != null && parameters.get(TYPE) == GEOMETRIC) {
                    currentProbe = geometricProbe;
                    fieldGeometry = irregularFieldGeometry;
                }
                else 
                    currentProbe = indexProbe;
                currentProbeGlyph = currentProbe.getGlyphGeometry();
                doOutput();
                break;
            case SLICE_BANNERS_SCALE_STRING:
                slicesDisplay.setSlice2DScale(parameters.get(SLICE_BANNERS_SCALE));
                refreshDisplay();
                break;
            case SLICE_RESOLUTION_STRING:
                slicesDisplay.setSlice2DResolution(parameters.get(SLICE_RESOLUTION));
                refreshDisplay();
                break;
            case TIME_STRING:
                slicesDisplay.setTime(parameters.get(TIME));
                if (outField != null)
                    outField.setCurrentTime(parameters.get(TIME));
                fieldGeometry.updateDataMap();
                refreshDisplay();
                break;
            case SLICE_TITLE_STRING:
                if (pickedSlice != null) {
                    pickedSlice.setTitle(parameters.get(SLICE_TITLE));
                    refreshDisplay();
                }
                break;
            case TITLE_SIZE_STRING:
                slicesDisplay.setTitleFontSize(parameters.get(TITLE_SIZE));
            default:
            }
            probeField = currentProbe.getSliceAreaField();
            if (noOutfield && outField != null) {
                prepareOutputGeometry();
            }
            computeUI.enableRemoveGraph(pickedSlice != null);
        }
    };
    
    PickListener graphPickingListener = new PickListener()
    {
        @Override
        public void pickChanged(PickEvent e)
        {
            pickedSlice = slicesDisplay.pickedDisplay(e.getEvt().getX(), e.getEvt().getY());
            if (pickedSlice != null) {
                computeUI.enableRemoveGraph(true);
                refreshDisplay();
                for (ProbeDisplay slice : slicesDisplay.getDisplays())
                    slice.setSelected(false);
                pickedSlice.setSelected(true);
                computeUI.updateGraphTitle(pickedSlice.getTitle());
            }
        }
    };
    
    ChangeListener probeListener = new ChangeListener()
    {
        @Override
        public void stateChanged(ChangeEvent e)
        {
            fromParams = true;
            probeField = currentProbe.getSliceField();
            startAction();
        }
    };
    
    public PlanarSlice()
    {
        backGroundColorListener = new ColorListener()
        {
            @Override
            public void colorChoosen(ColorEvent e) 
            {
                for (ProbeDisplay display : slicesDisplay.getDisplays()) 
                    ((SliceDisplay)display).setBgr(e.getSelectedColor());
                refreshDisplay();
            }
        };
        
        parameters.addParameterChangelistener(parameterListener);
        
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.getPresentationGUI().hideTransformPanel();
                ui.addComputeGUI(computeUI, "Probe");
                computeUI.setParameters(parameters);
                computeUI.setComponentFeature(isoComponent);
                setPanel(ui);
            }
        });
        outObj.addPickListener(graphPickingListener);
    }   
    

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(TYPE, GEOMETRIC),
            new Parameter<>(ADD_SLICE_AREA, false),
            new Parameter<>(DEL_SLICE, false),
            new Parameter<>(CLEAR_SLICES, false),
            new Parameter<>(ORDER_SLICES, false),
            new Parameter<>(REFRESH_SLICES, false),
            new Parameter<>(SLICE_RESOLUTION, 300),
            new Parameter<>(SLICE_BANNERS_SCALE, .2f),
            new Parameter<>(SLICE_POSITION, RIGHT),
            new Parameter<>(POINTER_LINE, true),
            new Parameter<>(TIME, 0.f),
            new Parameter<>(TITLE_SIZE, .02f),
            new Parameter<>(SHOW_ISOLINES, false),
            new Parameter<>(ISO_COMPONENT, isoComponent),
            new Parameter<>(ISO_THRESHOLDS, new float[]{}),
            new Parameter<>(SLICE_TITLE, ""),
        };
    }
    
    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        if (resetParameters) {
            parameters.set(TYPE, GEOMETRIC);
            parameters.set(ADD_SLICE_AREA, false);
            parameters.set(DEL_SLICE, false);
            parameters.set(CLEAR_SLICES, false);
            parameters.set(ORDER_SLICES, false);
            parameters.set(REFRESH_SLICES, false);
            parameters.set(SLICE_POSITION, RIGHT);
            parameters.set(SLICE_RESOLUTION, 300);
            parameters.set(SLICE_BANNERS_SCALE, .2f);
            parameters.set(POINTER_LINE, true);
            parameters.set(TIME, 0.f);
            parameters.set(TITLE_SIZE, .02f);
            parameters.set(SHOW_ISOLINES, false);
            parameters.set(ISO_COMPONENT, isoComponent);
            parameters.set(ISO_THRESHOLDS, new float[]{});
            parameters.set(SLICE_TITLE, "");
        }
        parameters.setParameterActive(true);
    }
    
    @Override
    public MouseRestingListener getMouseRestingListener()
    {
        return toolTipListener;
    }
    
    private void updateAddCapability()
    {
        windowWidth  = outObj.getRenderingWindow().getWidth();
        windowHeight = outObj.getRenderingWindow().getHeight();
        Position probesPosition = parameters.get(SLICE_POSITION);
        int reqSize = 180 * (slicesDisplay.getDisplays().size() + 1);
        computeUI.enableAddGraph(
            (probesPosition != TOP && probesPosition != BOTTOM || reqSize < windowWidth) &&
            (probesPosition != LEFT && probesPosition != RIGHT || reqSize < windowHeight));
    }


    @Override
    protected void notifySwingGUIs(ParameterProxy clonedParameterProxy, 
                                   boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }
    
    private void updateValueRanges()
    {
        if (outField == null || outField.getNComponents() == 0) {
            minima = null;
            maxima = null;
        }
        else {
            minima = new float[outField.getNComponents()];
            maxima = new float[outField.getNComponents()];
            for (int i = 0; i < outField.getNComponents(); i++) {
                DataArray accumulatedDA = outField.getComponent(i);
                if (!accumulatedDA.isNumeric())
                    continue;
                minima[i] = (float)accumulatedDA.getMinValue();
                maxima[i] = (float)accumulatedDA.getMaxValue();
            }
        }
    }
    
   
    protected void doOutput()
    {
        outputChanged = false;
        if (probeField == null) 
            setOutputValue("currentOutField", null);
        else
            setOutputValue("currentOutField", new VNIrregularField(probeField));
        outField = slicesDisplay.getMergedField();
        if (outField == null)
            setOutputValue("accumulatedOutField", null);
        else 
            setOutputValue("accumulatedOutField", new VNIrregularField((IrregularField)outField));
        prepareOutputGeometry();
        ui.getPresentationGUI().getDataMappingGUI().getDataMappingParams().addRenderEventListener(dataMapListener);
        show();
    }
    
    @Override
    protected synchronized void show()
    {   
        outObj.clearGeometries2D();
        outObj.addGeometry2D(fieldGeometry.getColormapLegend());
        outObj.addGeometry2D(slicesGeometry);
        currentProbe.getGlyphGeometry().detach();
        slicesDisplay.getParentSlicesGroup().detach();
        outObj.addNode(currentProbe.getGlyphGeometry());
        outObj.addNode(slicesDisplay.getParentSlicesGroup());
        outObj.setExtents(inField.getPreferredExtents());
    }
    
    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null) {
            outObj.clearAllGeometry();
            outObj.clearGeometries2D();
            return;
        }
        if (fromParams) 
            fromParams = false;
        else {
            Field newField = ((VNField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = (inField == null || !Arrays.equals(inField.getComponentNames(), newField.getComponentNames()));
            boolean isNewField = newField != inField;
            inField = newField;
            dataMappingParams.setInData(inField, (DataContainer)null);
            isoComponent.setContainerSchema(inField.getSchema());
            validateParamsAndSetSmart(isDifferentField && !isFromVNA());
            notifyGUIs(parameters, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

            if (isNewField) {
                if (geometricProbe != null)
                    geometricProbe.clearChangeListeners();
                if (indexProbe != null)
                    indexProbe.clearChangeListeners();
                if (currentProbeGlyph != null)
                    currentProbeGlyph.detach();
                slicesDisplay.clearDisplays();
                indexProbe = null;
                if (inField instanceof IrregularField)
                    for (CellSet cs : ((IrregularField)inField).getCellSets()) 
                        cs.addGeometryData(inField.getCoords(0));
                currentProbe = geometricProbe = new BasicPlanarSlice(presentationParams);
                geometricProbe.setInData(inField, dataMappingParams);
                geometricProbeGlyph = geometricProbe.getGlyph();
                int visibleWidgets = U_ROT_VIS   | V_ROT_VIS   | W_ROT_VIS |
                                     U_TRANS_VIS | V_TRANS_VIS |W_TRANS_VIS |
                                     U_RANGE_VIS | V_RANGE_VIS | 
                                     SCALE_VIS   | AXES_VIS;
                geometricProbeGlyph.getParams().setVisibleWidgets(visibleWidgets);
                geometricProbeGlyph.getComputeUI().updateWidgetVisibility();
                geometricProbeGlyph.getParams().setShow(true);
                geometricProbe.addChangeListener(probeListener);
                if (inField instanceof RegularField) {
                    indexProbe = new IndexProbe2D();
                    indexProbe.setInData(inField, dataMappingParams);
                    indexProbeGlyph = indexProbe.getGlyphGeometry();
                    indexProbe.addChangeListener(probeListener);
                }
                computeUI.setProbePanels(geometricProbe != null ? geometricProbe.getGlyphGUI() : null,
                                         indexProbe != null ?     indexProbe.getGlyphGUI() :     null);
                currentProbeGlyph = currentProbe.getGlyphGeometry();
                outField = null;
                probeField = currentProbe.getSliceAreaField();
                prepareOutputGeometry();
                fieldGeometry = irregularFieldGeometry;
                dataMappingParams = ui.getPresentationGUI().getDataMappingGUI().getDataMappingParams();
                dataMappingParams.getTransparencyParams().getComponentRange().setComponentSchema((DataArraySchema)null);
                dataMappingParams.addRenderEventListener(dataMapListener);
                if (indexProbe != null)
                    indexProbe.setDataMappingParams(dataMappingParams);
                slicesDisplay.updateDataMaps(ui.getPresentationGUI().getDataMappingGUI().getDataMappingParams());
                geometricProbe.setDataMappingParams(dataMappingParams);
                outField = null;
                setOutputValue("outField", null);
                computeUI.setTimeRange(inField.getStartTime(), inField.getEndTime());
                show();
            }
        }
        if (outputChanged)
            doOutput();
    }
}
