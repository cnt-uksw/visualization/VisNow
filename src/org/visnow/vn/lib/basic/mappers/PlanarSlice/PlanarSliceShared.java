/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.PlanarSlice;

import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;
import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class PlanarSliceShared
{
    public static enum ProbeType {GEOMETRIC, INDEX};
    public static final String TYPE_STRING = "Type"; 
    public static final String ADD_SLICE_AREA_STRING = "Add slice area";
    public static final String DEL_SLICE_STRING = "Remove slice";
    public static final String CLEAR_SLICES_STRING = "Clear slices";
    public static final String SLICE_RESOLUTION_STRING = "resolution";
    public static final String SLICE_BANNERS_SCALE_STRING = "slice banners scale";
    public static final String SLICE_POSITION_STRING = "slice position";
    public static final String POINTER_LINE_STRING = "pointer line";
    public static final String ORDER_SLICES_STRING = "order slices";
    public static final String REFRESH_SLICES_STRING = "refresh slices";
    public static final String SLICE_TITLE_STRING = "slice title";
    public static final String TITLE_SIZE_STRING = "font size";
    public static final String TIME_STRING = "time";
    public static final String SHOW_ISO_STRING = "show isolines";
    public static final String ISO_COMPONENT_STRING = "isolines component";
    public static final String ISO_THRESHOLDS_STRING = "isoline thresholds";
    
    static final ParameterName<ProbeType>         TYPE = new ParameterName(TYPE_STRING); 
    static final ParameterName<Boolean>           ADD_SLICE_AREA = new ParameterName(ADD_SLICE_AREA_STRING);
    static final ParameterName<Boolean>           DEL_SLICE = new ParameterName(DEL_SLICE_STRING);
    static final ParameterName<Boolean>           CLEAR_SLICES = new ParameterName(CLEAR_SLICES_STRING);
    static final ParameterName<Integer>           SLICE_RESOLUTION = new ParameterName(SLICE_RESOLUTION_STRING);
    static final ParameterName<Boolean>           ORDER_SLICES = new ParameterName(ORDER_SLICES_STRING);
    static final ParameterName<Float>             SLICE_BANNERS_SCALE = new ParameterName(SLICE_BANNERS_SCALE_STRING);
    static final ParameterName<Position>          SLICE_POSITION = new ParameterName(SLICE_POSITION_STRING);
    static final ParameterName<Boolean>           POINTER_LINE = new ParameterName(POINTER_LINE_STRING);
    static final ParameterName<Boolean>           REFRESH_SLICES = new ParameterName(REFRESH_SLICES_STRING);
    static final ParameterName<Float>             TIME = new ParameterName(TIME_STRING);
    static final ParameterName<Float>             TITLE_SIZE = new ParameterName(TITLE_SIZE_STRING);
    static final ParameterName<Boolean>           SHOW_ISOLINES = new ParameterName(SHOW_ISO_STRING);
    static final ParameterName<ComponentFeature>  ISO_COMPONENT = new ParameterName(ISO_COMPONENT_STRING);
    static final ParameterName<float[]>           ISO_THRESHOLDS = new ParameterName(ISO_THRESHOLDS_STRING);
    static final ParameterName<String>            SLICE_TITLE = new ParameterName(SLICE_TITLE_STRING);
}
