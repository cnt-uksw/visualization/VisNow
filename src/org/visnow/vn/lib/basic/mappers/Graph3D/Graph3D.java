/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.Graph3D;

import java.util.Arrays;
import org.visnow.jscic.RegularField;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.RegularFieldSchema;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.VectorMath;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.mappers.Graph3D.Graph3DShared.*;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Graph3D extends OutFieldVisualizationModule
{

    private GUI computeUI = null;
    private RegularField inField = null;
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    public Graph3D()
    {

        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (parameters.get(ADJUSTING)) {
                    update(parameters);
                    regularFieldGeometry.updateDataMap();
                    regularFieldGeometry.updateCoords();
                } else
                    startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                computeUI = new GUI();
            }
        });
        computeUI.setParameters(parameters);
        outObj.setName("graph3D");
        ui.addComputeGUI(computeUI);
        setPanel(ui);
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(COMPONENT, 0),
            new Parameter<>(SCALE, new float[]{-1f, 0f, 1f}),
            new Parameter<>(Z_FROM_DATA, false),
            new Parameter<>(ADJUSTING, false),
            new Parameter<>(META_FIELD_IS_AFFINE_XYZ, false),
            new Parameter<>(META_FIELD_SCHEMA, null)
        };

    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        if (resetParameters) {
            parameters.setParameterActive(false);
            parameters.set(META_FIELD_IS_AFFINE_XYZ, inField.isAffineXYZ(false));
            parameters.set(META_FIELD_SCHEMA, inField.getSchema());

            float[] scale = parameters.get(SCALE);
            float[] smartValues = Graph3D.getSmartScale(inField.getSchema(), parameters.get(COMPONENT));
            //validate
            boolean validate = scale[1] < smartValues[0] || scale[1] > smartValues[2];
            //threshold used when graph scale is too close to scale range
            float smartThreshold = 0.1f;
            float position = (scale[1] - smartValues[0]) / (smartValues[2] - smartValues[0]);
            boolean smartUpdate = false;
            //smart
            if (position < smartThreshold || 1 - position < smartThreshold || scale[1] == 0) smartUpdate = true;
            if (validate || smartUpdate) parameters.set(SCALE, smartValues);
            parameters.setParameterActive(true);
        }
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully);
    }

    public void update(Parameters p)
    {
        boolean newCoords = false;
        FloatLargeArray coords;
        if (inField == null || inField.getDims() == null || inField.getDims().length != 2) {
            return;
        }
        int mapComponent = p.get(COMPONENT);
        if (mapComponent < 0 || mapComponent >= inField.getNComponents()) {
            return;
        }
        
        DataArray mappedComponent = inField.getComponent(mapComponent);
        if (mappedComponent.getVectorLength() != 1) {
            return;
        }
        long[] dims = inField.getLDims();
        if (outRegularField != null && outRegularField.getCurrentCoords() != null) {
            for (int i = 0; i < dims.length; i++) {
                if (dims[i] != outRegularField.getDims()[i]) {
                    newCoords = true;
                    break;
                }
            }
        }

        if (outRegularField == null || outRegularField.getCurrentCoords() == null || newCoords) {
            outRegularField = new RegularField(dims);
            coords = new FloatLargeArray(3 * dims[0] * dims[1]);
        } else {
            coords = outRegularField.getCurrentCoords();
        }
        for (int i = 0; i < inField.getNComponents(); i++) {
            outRegularField.setComponent(inField.getComponent(i).cloneShallow(), i);
        }

        if (inField.getCurrentCoords() == null) {
            float[] h = new float[3];
            float[][] affine = inField.getAffine();
            h[0] = affine[0][1] * affine[1][2] - affine[0][2] * affine[1][1];
            h[1] = affine[0][2] * affine[1][0] - affine[0][0] * affine[1][2];
            h[2] = affine[0][0] * affine[1][1] - affine[0][1] * affine[1][0];
            float r = (float) (sqrt(h[0] * h[0] + h[1] * h[1] + h[2] * h[2]));
            for (int i = 0; i < h.length; i++)
                h[i] *= p.get(SCALE)[1] / r;
            for (long i = 0, k = 0; i < dims[1]; i++) {
                FloatLargeArray d = mappedComponent.getCurrent1DFloatSlice(i * dims[0], dims[0], 1);
                for (long j = 0; j < dims[0]; j++)
                    for (int l = 0; l < 3; l++, k++)
                        coords.setFloat(k, affine[3][l] + i * affine[1][l] + j * affine[0][l] + h[l] * d.getFloat(j));
            }
        } else {
            FloatLargeArray c = inField.getCurrentCoords();
            FloatLargeArray z = inField.getNormals();
            for (int i = 0, k = 0; i < dims[1]; i++) {
                FloatLargeArray d = inField.getComponent(mapComponent).getCurrent1DFloatSlice(i * dims[0], dims[0], 1);
                for (int j = 0; j < dims[0]; j++)
                    for (int l = 0; l < 3; l++, k++)
                        coords.setFloat(k, c.getFloat(k) + p.get(SCALE)[1] * z.getFloat(k) * d.getFloat(j));
            }
        }
//        if (inField.hasMask() || mappedComponent.processDataOutsideRange() == DataOutsideRange.MASK_OUT) {
//             LogicLargeArray mask;
//             if (inField.hasMask())
//                 mask = inField.getCurrentMask().clone();
//             else 
//                 mask = new LogicLargeArray(inField.getNNodes(), (byte)1);
//             if (mappedComponent.processDataOutsideRange() == DataOutsideRange.MASK_OUT) {
//                 double dMin = mappedComponent.getPreferredMinValue();
//                 double dMax = mappedComponent.getPreferredMaxValue();
//                 for (int i = 0, k = 0; i < dims[1]; i++) {
//                    FloatLargeArray d = inField.getComponent(mapComponent).getCurrent1DFloatSlice(i * dims[0], dims[0], 1);
//                    for (int j = 0; j < dims[0]; j++, k++)
//                        if (d.getFloat(j) < dMin || d.getFloat(j) > dMax)
//                            mask.setBoolean(k, false);
//                 }
//             }
//             outRegularField.setCurrentMask(mask);
//        }
        if (inField.hasMask())
            outRegularField.setCurrentMask(inField.getCurrentMask());
        outRegularField.setCurrentCoords(coords);
        outRegularField.setPreferredExtents(outRegularField.getExtents());
        if (p.get(Z_FROM_DATA) && inField.isAffineXYZ(false)) {
            //find which axis is "3D"?
            float[] v0 = inField.getAffine()[0];
            float[] v1 = inField.getAffine()[1];
            float[] v2 = VectorMath.crossProduct(v0, v1);
            float ax = VectorMath.vectorAngle(v2, new float[]{1.0f, 0.0f, 0.0f}, false);
            if (ax >= Math.PI / 2.0) {
                ax = (float) (ax - Math.PI);
            }
            ax = Math.abs(ax);

            float ay = VectorMath.vectorAngle(v2, new float[]{0.0f, 1.0f, 0.0f}, false);
            if (ay >= Math.PI / 2.0) {
                ay = (float) (ay - Math.PI);
            }
            ay = Math.abs(ay);

            float az = VectorMath.vectorAngle(v2, new float[]{0.0f, 0.0f, 1.0f}, false);
            if (az >= Math.PI / 2.0) {
                az = (float) (az - Math.PI);
            }
            az = Math.abs(az);

            int axis3 = 2;
            if (ax < ay && ax < az) {
                axis3 = 0;
            } else if (ay < ax && ay < az) {
                axis3 = 1;
            }

            float[][] phExts = new float[2][3];
            float[][] exts = outRegularField.getPreferredExtents();
            for (int i = 0; i < 2; i++)
                System.arraycopy(exts[i], 0, phExts[i], 0, 3);
            phExts[0][axis3] = (float) inField.getComponent(mapComponent).getPreferredPhysMinValue();
            phExts[1][axis3] = (float) inField.getComponent(mapComponent).getPreferredPhysMaxValue();
            outRegularField.setPreferredExtents(exts, phExts);
            String[] inCoordNames = inField.getAxesNames();
            String[] outCoordNames = new String[3];

            outCoordNames[0] = (inCoordNames == null || inCoordNames.length < 1 || inCoordNames[0] == null ||
                inCoordNames[0].startsWith("__coord"))
                ? "y" : inCoordNames[0];
            outCoordNames[1] = (inCoordNames == null || inCoordNames.length < 2 || inCoordNames[1] == null ||
                inCoordNames[1].startsWith("__coord"))
                ? "y" : inCoordNames[1];
            outCoordNames[2] = (inCoordNames == null || inCoordNames.length < 3 || inCoordNames[2] == null ||
                inCoordNames[2].startsWith("__coord"))
                ? "z" : inCoordNames[2];
            outCoordNames[axis3] = inField.getComponent(mapComponent).getName();

            String u = inField.getComponent(mapComponent).getUnit();
            if (u != null && !u.isEmpty())
                outCoordNames[axis3] = inField.getComponent(mapComponent).getName() + "[" + u + "]";
            outRegularField.setAxesNames(outCoordNames);
        } else {
            outRegularField.setAxesNames(null);
        }
        outRegularField.setCurrentTime(inField.getCurrentTime());
        outField = outRegularField;
        outField.setCurrentTime(inField.getCurrentTime());
    }

    /**
     * Returns smart values for scale parameter: preferredMinValue, val, preferredMaxValue.
     *
     * @param fieldSchema
     * @param componentNumber
     *
     * @return
     */
    public static float[] getSmartScale(RegularFieldSchema fieldSchema, int componentNumber)
    {
        if (fieldSchema == null) {
            return null;
        }
        float min = (float) fieldSchema.getComponentSchema(componentNumber).getPreferredMinValue();
        float max = (float) fieldSchema.getComponentSchema(componentNumber).getPreferredMaxValue();
        if (max <= min)
            max = min + .001f;
        float ext = fieldSchema.getPreferredExtents()[1][0] - fieldSchema.getPreferredExtents()[0][0];
        for (int i = 1; i < 3; i++)
            ext = max(ext, fieldSchema.getPreferredExtents()[1][i] - fieldSchema.getPreferredExtents()[0][i]);
        int k = (int) round(log10(ext / (max - min)));
        float smax = (float) pow(10., k);
        return new float[]{-smax, 0.2f * smax, smax};
    }

    @Override
    public void onActive()
    {

        if (getInputFirstValue("inField") != null) {
            RegularField newInField = ((VNRegularField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA() && (inField == null || !Arrays.equals(inField.getDims(), newInField.getDims()));
            inField = newInField;

            Parameters p;
            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentField);
                p = parameters.getReadOnlyClone();
            }
            notifyGUIs(p, isFromVNA() || isDifferentField, false);
            update(p);
            setOutputValue("outField", new VNRegularField(outRegularField));
            prepareOutputGeometry();
            show();
        }
    }
}
