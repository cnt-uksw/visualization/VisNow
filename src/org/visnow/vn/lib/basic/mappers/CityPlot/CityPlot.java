/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.CityPlot;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.IrregularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class CityPlot extends OutFieldVisualizationModule
{

    protected RegularField inField;
    protected GUI computeUI = null;
    protected boolean ignoreUI = false;
    protected boolean fromUI = false;
    protected Params params = null;
    protected int[] inDims = null;
    protected float[] coords = null;
    protected int axis = 0, lastAxis = 0;
    protected int[] cells = null;
    protected byte[] orientations = null;
    protected int[] edges = null;
    protected boolean[] edgeOrientations = null;
    protected boolean ribbon = true;
    protected float[][] extents = new float[2][3];
    protected float[][] physExtents = new float[2][3];

    public CityPlot()
    {
        parameters = params = new Params();
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParams(params);
                ui.addComputeGUI(computeUI);
            }
        });
        outObj.setName("isosurface");
        params.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                fromUI = true;
                if (ignoreUI)
                    return;
                if (params.isAdjusting() &&
                    irregularFieldGeometry != null) {
                    updateCoords();
                    irregularFieldGeometry.updateCoords();
                } else
                    startAction();
            }
        });
        setPanel(ui);
    }

    protected FloatValueModificationListener progressListener = new FloatValueModificationListener()
    {

        public void floatValueChanged(FloatValueModificationEvent e)
        {
            setProgress(e.getVal());
        }
    };
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    void updateCells()
    {
        cells = new int[8 * inDims[1] * inDims[0]];
        for (int i = 0; i < cells.length; i++)
            cells[i] = i;
        orientations = new byte[inDims[1] * inDims[0]];
        for (int i = 0; i < orientations.length; i++)
            orientations[i] = 1;
    }

    void updateCoords()
    {
        float scale = params.getScale();
        DataArray zDA = inField.getComponent(params.getComponent());
        float[] vals = zDA.getVectorLength() == 1 ? zDA.getRawFloatArray().getData() : zDA.getVectorNorms().getData();
        float off0 = -.5f * inDims[0];
        float off1 = -.5f * inDims[1];
        float base = 0;
        if (!params.isZeroBased())
            base = scale * (float) inField.getComponent(params.getComponent()).getPreferredMinValue();
        extents[0][0] = off0;
        extents[1][0] = -off0;
        extents[0][1] = off1;
        extents[1][1] = -off1;
        if (scale > 0) {
            extents[0][2] = (float) inField.getComponent(params.getComponent()).getPreferredMinValue() * scale;
            extents[1][2] = (float) inField.getComponent(params.getComponent()).getPreferredMaxValue() * scale;
        }
        else {
            extents[0][2] = (float) inField.getComponent(params.getComponent()).getPreferredMaxValue() * scale;
            extents[1][2] = (float) inField.getComponent(params.getComponent()).getPreferredMinValue() * scale;
        }
        physExtents[0][0] = 0;
        physExtents[0][1] = 0;
        if (params.isZeroBased() && inField.getComponent(params.getComponent()).getPreferredMinValue() > 0)
            physExtents[0][2] = 0;
        else
            physExtents[0][2] = (float) inField.getComponent(params.getComponent()).getPreferredMinValue();
        physExtents[1][0] = inDims[0];
        physExtents[1][1] = inDims[1];
        physExtents[1][2] = (float) inField.getComponent(params.getComponent()).getPreferredMaxValue();
        float xLow = params.getXRange()[0];
        float xUp =  params.getXRange()[1];
        float yLow = params.getYRange()[0];
        float yUp =  params.getYRange()[1];
        float[] xVals = null;
        float xValMin = 0, dx = 0;
        float[] yVals = null;
        float yValMin = 0, dy = 0;
        DataArray xDA = inField.getComponent(params.getxComponent().getComponentName());
        if (xDA != null) {
            xVals = xDA.getVectorLength() == 1 ? xDA.getRawFloatArray().getData() : xDA.getVectorNorms().getData();
            xValMin = (float)xDA.getPreferredMinValue();
            dx = (xUp - xLow) / (float)(xDA.getPreferredMaxValue() - xValMin);
        }
        DataArray yDA = inField.getComponent(params.getyComponent().getComponentName());
        if (yDA != null) {
            yVals = yDA.getVectorLength() == 1 ? yDA.getRawFloatArray().getData() : yDA.getVectorNorms().getData();
            yValMin = (float)yDA.getPreferredMinValue();;
            dy = (yUp - yLow) / (float)(yDA.getPreferredMaxValue() - yValMin);
        }
        
        for (int i = 0, k = 0; i < inDims[1]; i++)
            for (int j = 0; j < inDims[0]; j++, k += 24) {
                float dx0 = xVals == null ? xUp - xLow : dx * (xVals[inDims[0] * i + j] - xValMin);
                float dy0 = yVals == null ? yUp - yLow : dy * (yVals[inDims[0] * i + j] - yValMin);
                setCrds( k, off0 + j + xLow, off0 + j + xLow + dx0,
                            off1 + i + yLow, off1 + i + yLow + dy0, 
                            base, scale * vals[inDims[0] * i + j]);
            }
        outIrregularField.updatePreferredExtents();
        outIrregularField.setPreferredExtents(extents, physExtents);
    }
    
    private void setCrds(int k, float xlow, float xup, float ylow, float yup, float base, float top)
    {
        coords[k]      = coords[k + 9]  = coords[k + 12] = coords[k + 21] = xlow;
        coords[k + 3]  = coords[k + 6]  = coords[k + 15] = coords[k + 18] = xup;
        coords[k + 1]  = coords[k + 4]  = coords[k + 13] = coords[k + 16] = ylow;
        coords[k + 7]  = coords[k + 10] = coords[k + 19] = coords[k + 22] = yup;
        coords[k + 2]  = coords[k + 5]  = coords[k + 8]  = coords[k + 11] = base;
        coords[k + 14] = coords[k + 17] = coords[k + 20] = coords[k + 23] = top;
    }

    @Override
    public void onActive()
    {
        boolean newField = false;
        fromUI = false;
        if (!params.isActive())
            return;
        VNRegularField inFld = (VNRegularField) getInputFirstValue("inField");
        if (inFld != null) {
            RegularField in = inFld.getField();
            if (in == null || in.getNComponents() < 1 ||
                in.getDims().length != 2 || in.getDims()[0] < 2 || in.getDims()[1] < 2)
                return;
            if (in != inField) {
                if (!in.isDataCompatibleWith(inField)) {
                    ignoreUI = true;
                    computeUI.setInField(in);
                    ignoreUI = false;
                    newField = true;
                }
                if (!in.isStructureCompatibleWith(inField))
                    newField = true;
                inField = in;
                inDims = inField.getDims();
                if (inDims.length != 2 || inDims[0] < 2 || inDims[1] < 2)
                    return;
                if (newField) {
                    params.getxComponent().setContainer(inField);
                    params.getyComponent().setContainer(inField);
                    outIrregularField = new IrregularField(8 * (int) inField.getNNodes());
                    coords = new float[3 * (int) outIrregularField.getNNodes()];
                    outIrregularField.setCurrentCoords(new FloatLargeArray(coords));
                    updateCells();
                    CellArray boxes = new CellArray(CellType.HEXAHEDRON, cells, orientations, null);
                    CellSet cs = new CellSet();
                    cs.setCellArray(boxes);
                    cs.generateExternFaces();
                    outIrregularField.addCellSet(cs);

                    for (DataArray dta : inField.getComponents()) {
                        if (dta.isNumeric() && dta.getVectorLength() == 1) {
                            LargeArray inDB = dta.getRawArray();
                            LargeArray outDB = LargeArrayUtils.create(inDB.getType(), 8 * dta.getNElements());
                            for (long i = 0; i < outDB.length(); i++)
                                outDB.set(i, inDB.get(i / 8));
                            outIrregularField.addComponent(DataArray.create(outDB, 1, dta.getName()));
                        }
                    }
                }
            }

            updateCoords();
            outField = outIrregularField;
            prepareOutputGeometry();
            irregularFieldGeometry.getFieldDisplayParams().getRenderingParams().setShadingMode(RenderingParams.FLAT_SHADED);
            show();
        }
    }
}
