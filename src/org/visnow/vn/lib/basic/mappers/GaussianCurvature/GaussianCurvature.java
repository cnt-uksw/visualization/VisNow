/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.GaussianCurvature;

import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class GaussianCurvature extends OutFieldVisualizationModule
{

    /**
     * Creates a new instance of SurfaceSmoother
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected int nNodes;
    protected double[] angles;
    protected float[] areas;

    public GaussianCurvature()
    {
        setPanel(ui);
    }
    
    private void compute()
    {
        angles = new double[nNodes];
        areas  = new float[nNodes];
        
        for (int i = 0; i < nNodes; i++) {
            angles[i] = 2 * Math.PI;
            areas[i] = 0;
        }
        float[] coords = outIrregularField.getCurrentCoords().getData();
        int[] triangleNodes = outIrregularField.getCellSet(0).getBoundaryCellArray(CellType.TRIANGLE).getNodes();
        int[] nodes = new int[3];
        for (int triangle = 0; triangle < triangleNodes.length; triangle += 3) {
            System.arraycopy(triangleNodes, triangle, nodes, 0, 3);
            double[][] edges = new double[3][3];
            double[] lengths   = new double[3];
            double[] trAngles = new double[3];
            double area;
            for (int i = 0; i < 3; i++) {
                int k = nodes[i];
                int l = nodes[(i + 1) % 3];
                for (int j = 0; j < 3; j++) 
                {
                    double u = edges[i][j] = coords[3 * l + j] - coords[3 * k + j];
                    lengths[i] += u * u;
                }
                lengths[i] = Math.sqrt(lengths[i]);
            }
            double s = 0;
            double as = -Math.PI;
            for (int i = 0; i < 3; i++) {
                double u = 0;
                int k = i - 1; 
                if (k < 0)
                    k = 2;
                int l = (i + 1) % 3;
                for (int j = 0; j < 3; j++) 
                    u -= edges[k][j] * edges[l][j];
                trAngles[i] = u = Math.acos(u / (lengths[k] * lengths[l]));
                as += u;
                if (i == 0)
                    s = .5 * lengths[k] * lengths[l] * Math.sin(u);
                angles[nodes[i]] -= u;
                areas[nodes[i]] += s;
            }
//            System.out.println(""+as);
        }
        for (int i = 0; i < nNodes; i++)
            if (Math.abs(angles[i]) > 1)
                angles[i] = 0;
            else
                angles[i] /= areas[i];
        outIrregularField.addComponent(DataArray.create(angles, 1, "gaussian curvature"));
        outIrregularField.addComponent(DataArray.create(areas, 1, "star area"));
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null || ((VNField) getInputFirstValue("inField")).getField() == null)
            return;
        outIrregularField = ((VNField) getInputFirstValue("inField")).getField().getTriangulated();
        nNodes = (int)outIrregularField.getNNodes();
        compute();
        setOutputValue("outField", new VNIrregularField(outIrregularField));
        outField = outIrregularField;
        prepareOutputGeometry();
        show();
    }
}
