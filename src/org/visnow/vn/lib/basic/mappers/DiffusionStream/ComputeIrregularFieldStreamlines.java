/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.DiffusionStream;

import org.visnow.vn.lib.utils.numeric.ODE.Deriv;
import org.visnow.vn.lib.utils.numeric.ODE.RungeKutta;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.SimplexPosition;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.cells.CellType;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ComputeIrregularFieldStreamlines extends ComputeStreamlines
{

    private IrregularField inField = null;

    public ComputeIrregularFieldStreamlines(IrregularField inField, Params params)
    {
        super(inField, params);
        this.inField = inField;
        if (inField.getGeoTree() == null) {
            System.out.println("creating cell tree");
            long start = System.currentTimeMillis();
            inField.createGeoTree();
            System.out.println("cell tree created in " + ((float) (System.currentTimeMillis() - start)) / 1000 + "seconds");
        }
    }

    public void setStartPoints(Field startPoints)
    {
        if (startPoints != null) {
            startCoords = new float[3 * (int) startPoints.getNNodes()];
            System.arraycopy(startPoints.getCurrentCoords().getData(), 0, startCoords, 0, 3 * (int) startPoints.getNNodes());
        }
    }

    @Override
    public synchronized void updateStreamlines()
    {
        nForward = params.getNForwardSteps();

        streamlineCoords = new float[nSrc][3 * nForward];
        velocityVectors = new float[nSrc][3 * nForward];
        float[][] xt = inField.getPreferredExtents();
        nThreads = params.getNThreads();
        Thread[] workThreads = new Thread[nThreads];
        threadProgress = new int[nThreads];
        for (int i = 0; i < nThreads; i++)
            threadProgress[i] = 0;
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new Streamline(iThread));
            workThreads[iThread].start();
        }
        for (int i = 0; i < workThreads.length; i++)
            try {
                workThreads[i].join();
            } catch (Exception e) {
            }
        nvert = nSrc * nForward;
        lines = new int[2 * nSrc * (nForward - 1)];
        coords = new float[3 * nvert];
        indices = new int[nvert];
        vectors = new float[3 * nvert];
        for (int i = 0; i < coords.length; i++)
            coords[i] = 0;
        for (int i = 0; i < nSrc; i++) {
            int j = i;
            for (int k = 0; k < nForward; k++, j += nSrc) {
                for (int l = 0; l < 3; l++) {
                    coords[3 * j + l] = streamlineCoords[i][3 * k + l];
                    vectors[j] = velocityVectors[i][k];
                }
                indices[j] = k;
            }
        }

        streamlineCoords = null;
        velocityVectors = null;
        for (int i = 0, j = 0; i < nSrc; i++)
            for (int k = 0, l = i * nForward; k < nForward - 1; k++, j += 2, l++) {
                lines[j] = i + k * nSrc;
                lines[j + 1] = lines[j] + nSrc;
            }
        byte[] edgeOrientations = new byte[lines.length / 2];
        for (int i = 0; i < edgeOrientations.length; i++)
            edgeOrientations[i] = 1;
        outField = new IrregularField(nvert);
        outField.setCurrentCoords(new FloatLargeArray(coords));
        DataArray da = DataArray.create(indices, 1, "steps");
        da.setPreferredRange(0, nForward);
        outField.addComponent(da);
        DataArray dn = DataArray.create(vectors, 3, "vectors");
        outField.addComponent(dn);
        CellArray streamLines = new CellArray(CellType.SEGMENT, lines, edgeOrientations, null);
        CellSet cellSet = new CellSet(inField.getName() + " " + inField.getComponent(params.getVectorComponent()).getName() + " streamlines");
        cellSet.setBoundaryCellArray(streamLines);
        cellSet.setCellArray(streamLines);
        outField.addCellSet(cellSet);
        outField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
    }

    private class Streamline implements Runnable
    {

        private int iThread;
        private SimplexPosition interp = new SimplexPosition(new int[4], new float[4], null);
//        private IrregularVectInterpolate vInt = new IrregularVectInterpolate(inField, interp);

        public Streamline(int iThread)
        {
            this.iThread = iThread;
        }

        public void run()
        {
            float[] fs = new float[nSp];
            int dk = nSrc / nThreads;
            int kstart = iThread * dk + min(iThread, nSrc % nThreads);
            int kend = (iThread + 1) * dk + min(iThread + 1, nSrc % nThreads);
            for (int n = kstart; n < kend; n++) {
                if (iThread == 0)
                    fireStatusChanged((float) n / nSrc);
                try {
                    System.arraycopy(startCoords, nSp * n, fs, 0, fs.length);
//                    toSteps[n] = RungeKutta.fourthOrderRK(vInt, fs, params.getStep(), nForward, 1, streamlineCoords[n], velocityVectors[n]);
                } catch (Exception e) {
//                    System.out.println("null at " + n + " from " + nSrc);
                    e.printStackTrace();
                }
            }
        }
    }

//    private class IrregularVectInterpolate implements Deriv
//    {
//
//        private IrregularField fld;
//        private SimplexPosition interp;
//
//        public IrregularVectInterpolate(IrregularField fld, SimplexPosition interp)
//        {
//            this.fld = fld;
//            this.interp = interp;
//        }
//
//        @Override
//        public float[] derivn(float[] y) throws Exception
//        {
//            float[] p = new float[y.length];
//            if (fld.getFieldCoords(y, interp)) {
//                float[] q = new float[]{0, 0, 0};
//                for (int i = 0; i < 4; i++) {
//                    int k = vlen * interp.getVertices()[i];
//                    for (int j = 0; j < vlen; j++)
//                        q[j] += interp.getCoords()[i] * vects[k + j];
//                }
//                return q;
//            } else
//                return null;
//        }
//    }

}
