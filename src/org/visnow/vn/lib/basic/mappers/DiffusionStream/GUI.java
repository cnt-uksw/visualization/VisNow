/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.DiffusionStream;

import java.awt.CardLayout;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class GUI extends javax.swing.JPanel
{

    private Params params = null;
    private Field inField;
    private Hashtable<Integer, JLabel> downLabels = new Hashtable<Integer, JLabel>();
    private int[] down
        = {
            1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000
        };
    private int downIndex = down.length - 1;
    private String[] downTexts
        = {
            "1", "", "", "10", "", "", "100", "", "", "1000", "", "", "1e4", "", "", "1e5"
        };
    private int[] downsize;
    private int downBy = 1;
    private Map<Integer, String> valLabels = new HashMap<Integer, String>();

    /**
     * Creates new form GUI
     */
    public GUI()
    {
        initComponents();
        for (int i = 0; i < downTexts.length; i++) {
            downLabels.put(i, new JLabel(downTexts[i]));
            downLabels.get(i).setFont(new java.awt.Font("Dialog", 0, 8));
        }
        downsizeSlider.setLabelTable(downLabels);
        vectorComponentSelector.setTitle("vector component");
        vectorComponentSelector.setVectorComponentsOnly(true);
        regularFieldDownsizeUI.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                params.setActive(false);
                params.setDown(regularFieldDownsizeUI.getDownsize());
                params.setActive(true);
            }
        });
        for (int i = 0; i < downTexts.length; i++) {
            downLabels.put(i, new JLabel(downTexts[i]));
            downLabels.get(i).setFont(new java.awt.Font("Dialog", 0, 8));
        }
        downsizeSlider.setLabelTable(downLabels);
    }

    private void startAction()
    {
        if (params == null || inField == null)
            return;
        params.setStep(stepSlider.getValue().floatValue());
        params.setVectorComponent(vectorComponentSelector.getComponent());
        params.setNThreads(VisNow.availableProcessors());
        params.setMultiplicity(multSlider.getValue().intValue());
        params.setDiffCoeff(diffusionCoeffSlider.getValue().floatValue());
        params.fireStateChanged();
    }

    public void setInField(Field inFld)
    {
        inField = inFld;
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                vectorComponentSelector.setDataSchema(inField.getSchema());
            }
        });
    }

    private void setScaleMinMax()
    {
        float max = 1;
        if (inField.getComponent(params.getVectorComponent()) != null)
            max = (float)inField.getComponent(params.getVectorComponent()).getPreferredMaxValue();
        float[][] ext = inField.getPreferredExtents();
        double diam = 0;
        for (int i = 0; i < 3; i++)
            diam += (ext[1][i] - ext[0][i]) * (ext[1][i] - ext[0][i]);
        if (max <= 0)
            max = .001f;
        float s0 = (float) (sqrt(diam / 1000000) / max);
        stepSlider.setAll(s0 / 100, s0 * 100, s0);
        params.setStep(s0);
        s0 = (float) sqrt(diam / 1000000);
        diffusionCoeffSlider.setAll(s0 / 100, s0 * 100, s0);
        params.setDiffCoeff(s0);
    }

    public void setInPts(Field pts)
    {
        if (pts instanceof RegularField) {
            downsize = params.getDown();
            SwingInstancer.swingRunAndWait(new Runnable()
            {
                public void run()
                {
                    CardLayout cl = (CardLayout) (jPanel5.getLayout());
                    downsize = params.getDown();
                    regularFieldDownsizeUI.setDownsize(downsize);
                    regularFieldDownsizeUI.setVisible(true);
                    downsizeSlider.setVisible(false);
                    cl.show(jPanel5, "regularFieldDownsizeUI");
                }
            });
        } else {
            downBy = down[down.length - 1];
            for (int i = 0; i < down.length; i++)
                if (down[i] >= params.getDownsize()) {
                    downIndex = i;
                    downBy = down[i];
                    break;
                }
            SwingInstancer.swingRunAndWait(new Runnable()
            {
                public void run()
                {
                    CardLayout cl = (CardLayout) (jPanel5.getLayout());
                    downsizeSlider.setValue(downIndex);
                    regularFieldDownsizeUI.setVisible(false);
                    downsizeSlider.setVisible(true);
                    params.setDownsize(downBy);
                    cl.show(jPanel5, "downsizeSlider");
                }
            });
        }
    }

    /**
     * Get the value of params
     *
     * @return the value of params
     */
    public Params getParams()
    {
        return params;
    }

    /**
     * Set the value of params
     *
     * @param params new value of params
     */
    public void setParams(Params params)
    {
        this.params = params;
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        vectorComponentSelector = new org.visnow.vn.lib.gui.DataComponentSelector();
        stepSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        jPanel1 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        downsizeSlider = new javax.swing.JSlider();
        regularFieldDownsizeUI = new org.visnow.vn.lib.gui.DownsizeUI();
        stepsSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        diffusionCoeffSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        multSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        runButton = new javax.swing.JButton();
        previewButton = new javax.swing.JButton();

        setMinimumSize(new java.awt.Dimension(180, 412));
        setPreferredSize(new java.awt.Dimension(220, 416));
        setLayout(new java.awt.GridBagLayout());

        vectorComponentSelector.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                vectorComponentSelectorStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(vectorComponentSelector, gridBagConstraints);

        stepSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "integration step", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        stepSlider.setMax(1);
        stepSlider.setMin(1e-4);
        stepSlider.setMinimumSize(new java.awt.Dimension(90, 62));
        stepSlider.setPreferredSize(new java.awt.Dimension(200, 65));
        stepSlider.setScaleType(org.visnow.vn.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        stepSlider.setValue(2e-3);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(stepSlider, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        add(jPanel1, gridBagConstraints);

        jPanel5.setMinimumSize(new java.awt.Dimension(10, 65));
        jPanel5.setOpaque(false);
        jPanel5.setLayout(new java.awt.CardLayout());

        downsizeSlider.setFont(new java.awt.Font("Dialog", 0, 8)); // NOI18N
        downsizeSlider.setMajorTickSpacing(1);
        downsizeSlider.setMaximum(15);
        downsizeSlider.setMinorTickSpacing(1);
        downsizeSlider.setPaintLabels(true);
        downsizeSlider.setPaintTicks(true);
        downsizeSlider.setSnapToTicks(true);
        downsizeSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "downsize init points set", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        downsizeSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                downsizeSliderStateChanged(evt);
            }
        });
        jPanel5.add(downsizeSlider, "downsizeSlider");

        regularFieldDownsizeUI.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                regularFieldDownsizeUIStateChanged(evt);
            }
        });
        jPanel5.add(regularFieldDownsizeUI, "regularFieldDownsizeUI");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(jPanel5, gridBagConstraints);

        stepsSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("Runge-Kutta steps"));
        stepsSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        stepsSlider.setMax(1000);
        stepsSlider.setMin(10);
        stepsSlider.setMinimumSize(new java.awt.Dimension(90, 56));
        stepsSlider.setName(""); // NOI18N
        stepsSlider.setPreferredSize(new java.awt.Dimension(200, 60));
        stepsSlider.setScaleType(org.visnow.vn.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(stepsSlider, gridBagConstraints);

        diffusionCoeffSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "diffusion coefficient", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        diffusionCoeffSlider.setMax(1);
        diffusionCoeffSlider.setMin(1e-5);
        diffusionCoeffSlider.setMinimumSize(new java.awt.Dimension(90, 56));
        diffusionCoeffSlider.setPreferredSize(new java.awt.Dimension(200, 60));
        diffusionCoeffSlider.setScaleType(org.visnow.vn.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        diffusionCoeffSlider.setValue(1e-3);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(diffusionCoeffSlider, gridBagConstraints);

        multSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("starting point multiplicity"));
        multSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        multSlider.setMax(100000);
        multSlider.setMin(1);
        multSlider.setMinimumSize(new java.awt.Dimension(90, 56));
        multSlider.setOpaque(false);
        multSlider.setPreferredSize(new java.awt.Dimension(200, 60));
        multSlider.setScaleType(org.visnow.vn.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        multSlider.setValue(100);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(multSlider, gridBagConstraints);

        runButton.setText("run");
        runButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                runButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(runButton, gridBagConstraints);

        previewButton.setText("preview streamlines");
        previewButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                previewButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(previewButton, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void vectorComponentSelectorStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_vectorComponentSelectorStateChanged
    {//GEN-HEADEREND:event_vectorComponentSelectorStateChanged
        setScaleMinMax();
    }//GEN-LAST:event_vectorComponentSelectorStateChanged

    private void stepSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_stepSliderStateChanged
    {//GEN-HEADEREND:event_stepSliderStateChanged
        //       if (!stepSlider.isAdjusting())
        //          startAction();
    }//GEN-LAST:event_stepSliderStateChanged

    private void downsizeSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_downsizeSliderStateChanged
    {//GEN-HEADEREND:event_downsizeSliderStateChanged
        if (!downsizeSlider.getValueIsAdjusting()) {
            params.setDownsize(down[downsizeSlider.getValue()]);
        }
    }//GEN-LAST:event_downsizeSliderStateChanged

    private void regularFieldDownsizeUIStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_regularFieldDownsizeUIStateChanged
    {//GEN-HEADEREND:event_regularFieldDownsizeUIStateChanged
        params.setDownsizeChanged(true);
    }//GEN-LAST:event_regularFieldDownsizeUIStateChanged

    private void runButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_runButtonActionPerformed
    {//GEN-HEADEREND:event_runButtonActionPerformed
        params.setComputeDiffusion(true);
        startAction();
    }//GEN-LAST:event_runButtonActionPerformed

    private void previewButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_previewButtonActionPerformed
    {//GEN-HEADEREND:event_previewButtonActionPerformed
        params.setComputeDiffusion(false);
        startAction();
    }//GEN-LAST:event_previewButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.visnow.vn.gui.widgets.ExtendedSlider diffusionCoeffSlider;
    private javax.swing.JSlider downsizeSlider;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel5;
    private org.visnow.vn.gui.widgets.ExtendedSlider multSlider;
    private javax.swing.JButton previewButton;
    private org.visnow.vn.lib.gui.DownsizeUI regularFieldDownsizeUI;
    private javax.swing.JButton runButton;
    private org.visnow.vn.gui.widgets.ExtendedSlider stepSlider;
    private org.visnow.vn.gui.widgets.ExtendedSlider stepsSlider;
    private org.visnow.vn.lib.gui.DataComponentSelector vectorComponentSelector;
    // End of variables declaration//GEN-END:variables
}
