/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.ScaleCells;

import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.parameters.RenderingParams;
import static org.visnow.vn.lib.basic.mappers.ScaleCells.ScaleCellsShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ScaleCells extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI computeUI = null;
    protected Field inField;
    protected RegularField inRegularField = null;
    protected IrregularField inIrregularField = null;
    protected float scale = .75f;

    public ScaleCells()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (inIrregularField == null)
                    return;
                if (parameters.get(ADJUSTING)) {
                    scale = parameters.get(SCALE);
                    updateOutCoords(outIrregularField.getCurrentCoords() == null ? null :
                                    outIrregularField.getCurrentCoords().getData(),
                                    outIrregularField.getNormals() == null ? null :
                                    outIrregularField.getNormals().getData());
                }
                else
                    startIfNotInQueue();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
                outObj.setName("scale cells");
            }
        });
    }

    private void updateOutCoords(float[] coords, float[] normals)
    {
        if (inIrregularField == null || coords == null)
            return;
        int n = 0;
        float[] c = new float[3];
        float[] inCoords = inIrregularField.getCurrentCoords() == null ? null : inIrregularField.getCurrentCoords().getData();

        float[] w = new float[3];

        for (int i = 0; i < inIrregularField.getNCellSets(); i++) {
            CellSet inCS = inIrregularField.getCellSet(i);
            for (int j = 0; j < Cell.getNProperCellTypes(); j++)
                if (inCS.getCellArray(CellType.getType(j)) != null) {
                    CellArray inCA = inCS.getCellArray(CellType.getType(j));
                    int nv = CellType.getType(j).getNVertices();
                    int[] nodes = inCA.getNodes();
                    for (int k = 0; k < inCA.getNCells(); k++) {
                        for (int l = 0; l < 3; l++) {
                            c[l] = 0;
                            for (int m = k * nv; m < k * nv + nv; m++)
                                c[l] += inCoords[3 * nodes[m] + l] / nv;
                        }
                        if (normals != null) {
                            float wn = (float) (sqrt(w[0] * w[0] + w[1] * w[1] + w[2] * w[2]));
                            for (int l = k * nv, nn = 3 * k * nv; l < k * nv + nv; l++)
                                for (int m = 0; m < 3; m++, nn++)
                                    normals[nn] = w[m] / wn;
                        }
                        for (int m = k * nv; m < k * nv + nv; m++)
                            for (int l = 0; l < 3; l++, n++)
                                coords[n] = scale * inCoords[3 * nodes[m] + l] + (1 - scale) * c[l];

                    }
                }
            if (fieldGeometry != null)
                fieldGeometry.updateCoords();
        }
    }

    private void createOutField()
    {
        if (inIrregularField == null)
            return;
        int nNodes = 0;
        for (int i = 0; i < inIrregularField.getNCellSets(); i++) {
            CellSet cs = inIrregularField.getCellSet(i);
            for (int j = 0; j < Cell.getNProperCellTypes(); j++)
                if (cs.getCellArray(CellType.getType(j)) != null)
                    nNodes += CellType.getType(j).getNVertices() * cs.getCellArray(CellType.getType(j)).getNCells();
        }
        outIrregularField = new IrregularField(nNodes);
        FloatLargeArray coords = new FloatLargeArray(3 * (long)nNodes, false);
        outIrregularField.setCurrentCoords(coords);
        if (inIrregularField.getCurrentMask() != null) {
            int n = 0;
            LogicLargeArray inMask = inIrregularField.getCurrentMask();
            LogicLargeArray mask = new LogicLargeArray(nNodes);
            for (int i = 0; i < inIrregularField.getNCellSets(); i++) {
                CellSet cs = inIrregularField.getCellSet(i);
                for (int j = 0; j < Cell.getNProperCellTypes(); j++)
                    if (cs.getCellArray(CellType.getType(j)) != null) {
                        int[] nodes = cs.getCellArray(CellType.getType(j)).getNodes();
                        for (int k = 0; k < nodes.length; k++, n++)
                            mask.setByte(n, inMask.getByte(nodes[k]));
                    }
            }
            outIrregularField.setCurrentMask(mask);
        }
        int n = 0;
        for (int i = 0; i < inIrregularField.getNCellSets(); i++) {
            CellSet inCS = inIrregularField.getCellSet(i);
            CellSet outCS = new CellSet(inCS.getName());
            for (int j = 0; j < Cell.getNProperCellTypes(); j++)
                if (inCS.getCellArray(CellType.getType(j)) != null) {
                    CellArray inCA = inCS.getCellArray(CellType.getType(j));
                    int[] nodes = new int[inCA.getNodes().length];
                    for (int k = 0; k < nodes.length; k++, n++)
                        nodes[k] = n;
                    CellArray outCA = new CellArray(CellType.getType(j), nodes, inCA.getOrientations(), inCA.getDataIndices());
                    outCS.setCellArray(outCA);
                }
            for (int j = 0; j < inCS.getNComponents(); j++)
                outCS.addComponent(inCS.getComponent(j).cloneShallow());
            outCS.generateDisplayData(coords);
            outIrregularField.addCellSet(outCS);
        }
        for (int d = 0; d < inIrregularField.getNComponents(); d++) {
            DataArray inDA = inIrregularField.getComponent(d);
            if (!inDA.isNumeric())
                continue;
            n = 0;
            int vlen = inDA.getVectorLength();
            LargeArray inSD = inDA.getRawArray();
            LargeArray outSD = LargeArrayUtils.create(inSD.getType(), nNodes * vlen, false);
            for (int i = 0; i < inIrregularField.getNCellSets(); i++) {
                CellSet cs = inIrregularField.getCellSet(i);
                for (int j = 0; j < Cell.getNProperCellTypes(); j++)
                    if (cs.getCellArray(CellType.getType(j)) != null) {
                        int[] nodes = cs.getCellArray(CellType.getType(j)).getNodes();
                        for (int k = 0; k < nodes.length; k++) {
                            int m = vlen * nodes[k];
                            for (int l = 0; l < vlen; l++, n++)
                                outSD.set(n, inSD.get(m + l));
                        }
                    }
            }
            DataArray outDA = DataArray.create(outSD, vlen, inDA.getName());
            if (outDA != null) {
                outDA.setPreferredRanges(inDA.getPreferredMinValue(), inDA.getPreferredMaxValue(), inDA.getPreferredPhysMinValue(), inDA.getPreferredPhysMaxValue());
                outDA.setUnit(inDA.getUnit());
                outDA.setUserData(inDA.getUserData());
                outIrregularField.addComponent(outDA);
            }
        }
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(SCALE, 0.75f),
            new Parameter<>(ADJUSTING, false)
        };
    }

    @Override
    public void onActive()
    {

        if (getInputFirstValue("inField") != null) {
            //1. get new field
            VNField input = ((VNField) getInputFirstValue("inField"));
            Field newInField = input.getField();
            boolean isDifferentField = !isFromVNA() && (inField == null ||
                    inField.getTimestamp() != newInField.getTimestamp());
            inField = newInField;

            //2. validate parameters
            Parameters p = parameters.getReadOnlyClone();

            //3. update GUI
            notifyGUIs(p, isFromVNA() || isDifferentField, false);

            //4. compute
            if (inField instanceof RegularField) {
                RegularField regularInField = (RegularField) inField;
                int[] dims = regularInField.getDims();
                int nNodes = (int) regularInField.getNNodes();
                int nIndices = 0;
                int[] indices = null;
                float[] coords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords().getData();
                float[][] aff = regularInField.getAffine();
                CellSet cs = new CellSet();
                switch (dims.length) {
                    case 3:
                        int off2 = dims[0] * dims[1];
                        int off1 = dims[0];
                        nIndices = 8 * (dims[0] - 1) * (dims[1] - 1) * (dims[2] - 1);
                        indices = new int[nIndices];
                        for (int i = 0, m = 0; i < dims[2] - 1; i++)
                            for (int j = 0; j < dims[1] - 1; j++)
                                for (int k = 0, l = (i * dims[1] + j) * dims[0]; k < dims[0] - 1; k++, l++, m += 8) {
                                    indices[m] = l;
                                    indices[m + 1] = l + 1;
                                    indices[m + 2] = l + off1 + 1;
                                    indices[m + 3] = l + off1;
                                    indices[m + 4] = l + off2;
                                    indices[m + 5] = l + off2 + 1;
                                    indices[m + 6] = l + off2 + off1 + 1;
                                    indices[m + 7] = l + off2 + off1;
                                }

                        if (coords == null) {
                            coords = new float[3 * nNodes];
                            for (int i = 0, m = 0; i < dims[2]; i++)
                                for (int j = 0; j < dims[1]; j++)
                                    for (int k = 0; k < dims[0]; k++)
                                        for (int n = 0; n < 3; n++, m++)
                                            coords[m] = aff[3][n] + i * aff[2][n] + j * aff[1][n] + k * aff[0][n];
                        }
                        byte[] orientations = new byte[(dims[0] - 1) * (dims[1] - 1) * (dims[2] - 1)];
                        for (int i = 0; i < orientations.length; i++)
                            orientations[i] = 1;
                        CellArray ca = new CellArray(CellType.HEXAHEDRON, indices, orientations, indices);
                        cs.setCellArray(ca);
                        cs.generateExternFaces();
                        break;
                    case 2:
                        off1 = dims[0];
                        nIndices = 4 * (dims[0] - 1) * (dims[1] - 1);
                        indices = new int[nIndices];
                        for (int j = 0, m = 0; j < dims[1] - 1; j++)
                            for (int k = 0, l = j * dims[0]; k < dims[0] - 1; k++, l++, m += 4) {
                                indices[m] = l;
                                indices[m + 1] = l + 1;
                                indices[m + 2] = l + off1 + 1;
                                indices[m + 3] = l + off1;
                            }

                        if (coords == null) {
                            coords = new float[3 * nNodes];
                            for (int j = 0, m = 0; j < dims[1]; j++)
                                for (int k = 0; k < dims[0]; k++)
                                    for (int n = 0; n < 3; n++, m++)
                                        coords[m] = aff[3][n] + j * aff[1][n] + k * aff[0][n];
                        }
                        orientations = new byte[(dims[0] - 1) * (dims[1] - 1)];
                        for (int i = 0; i < orientations.length; i++)
                            orientations[i] = 1;
                        ca = new CellArray(CellType.QUAD, indices, orientations, indices);
                        cs.setCellArray(ca);
                        cs.generateExternFaces();
                        break;
                    case 1:
                        nIndices = 2 * (dims[0] - 1);
                        indices = new int[nIndices];
                        for (int k = 0, m = 0; k < dims[0] - 1; k++, m += 2) {
                            indices[m] = k;
                            indices[m + 1] = k + 1;
                        }
                        if (coords == null) {
                            coords = new float[3 * nNodes];
                            for (int k = 0, m = 0; k < dims[0]; k++)
                                for (int n = 0; n < 3; n++, m++)
                                    coords[m] = aff[3][n] + aff[1][n] + k * aff[0][n];
                        }
                        orientations = new byte[dims[0] - 1];
                        for (int i = 0; i < orientations.length; i++) {
                            orientations[i] = 1;
                        }
                        ca = new CellArray(CellType.SEGMENT, indices, orientations, indices);
                        cs.setCellArray(ca);
                        cs.generateExternFaces();
                        break;
                }
                inIrregularField = new IrregularField(nNodes);
                inIrregularField.setCurrentCoords(new FloatLargeArray(coords));
                if (regularInField.getCurrentMask() != null)
                    inIrregularField.setCurrentMask(regularInField.getCurrentMask());
                inIrregularField.addCellSet(cs);
                for (DataArray da : inField.getComponents())
                    inIrregularField.addComponent(da.cloneShallow());
            }
            else if (inField instanceof IrregularField)
                inIrregularField = (IrregularField) inField;
            else {
                return;
            }
            outObj.clearAllGeometry();
            outGroup = null;
            inField = newInField;
            outObj.setName(inField.getName());
            createOutField();
            scale = p.get(SCALE);
            updateOutCoords(outIrregularField.getCurrentCoords() == null ? null : outIrregularField.getCurrentCoords().getData(), outIrregularField.getNormals() == null ? null : outIrregularField.getNormals().getData());

            //5. prepare output
            outIrregularField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
            outField = outIrregularField;
            outField.setName(inField.getName());
            prepareOutputGeometry();
            irregularFieldGeometry.getFieldDisplayParams().getRenderingParams().setShadingMode(RenderingParams.FLAT_SHADED);
            show();
            setOutputValue("outField", new VNIrregularField(outIrregularField));
        }
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully);
    }
}
