/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.mappers.Trajectories;

import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;

/**
 *
 * <p>
 * @author Bartosz Borucki, University of Warsaw, ICM
 */
public class Params extends Parameters
{

    protected static final String START_FRAME = "startframe";
    protected static final String END_FRAME = "endframe";
    protected static final String PREFERRED_SIZE = "preferredSize";

    public Params()
    {
        super(eggs);
    }

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Integer>(START_FRAME, ParameterType.dependent, 1),
        new ParameterEgg<Integer>(END_FRAME, ParameterType.dependent, 100),
        new ParameterEgg<Integer>(PREFERRED_SIZE, ParameterType.dependent, 1000),};

    public int getStartFrame()
    {
        return (Integer) getValue(START_FRAME);
    }

    public void setStartFrame(int value)
    {
        this.setValue(START_FRAME, value);
        fireStateChanged();
    }

    public int getEndFrame()
    {
        return (Integer) getValue(END_FRAME);
    }

    public void setEndFrame(int value)
    {
        this.setValue(END_FRAME, value);
        fireStateChanged();
    }

    public void setStartEndFrames(int startValue, int endValue)
    {
        this.setValue(START_FRAME, startValue);
        this.setValue(END_FRAME, endValue);
        fireStateChanged();
    }
    
    public int getPreferredSize()
    {
        return (Integer) getValue(PREFERRED_SIZE);
    }

    public void setPreferredSize(int value)
    {
        this.setValue(PREFERRED_SIZE, value);
        fireStateChanged();
    }

}
