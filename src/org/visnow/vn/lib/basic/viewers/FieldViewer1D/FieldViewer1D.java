/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
package org.visnow.vn.lib.basic.viewers.FieldViewer1D;

import java.awt.Component;
import java.awt.Frame;
import org.visnow.vn.lib.basic.viewers.FieldViewer1D.ExtendedChart.ExtendedChartPanel;
import org.visnow.vn.lib.basic.viewers.FieldViewer1D.ExtendedChart.ChartData;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jfree.data.UnknownKeyException;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.Input;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.Inputs;
import org.visnow.vn.engine.core.Link;
import org.visnow.vn.engine.core.LinkFace;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.lib.basic.viewers.FieldViewer1D.ExtendedChart.ExtendedChart;
import static org.visnow.vn.lib.basic.viewers.FieldViewer1D.FieldViewer1DShared.*;
import org.visnow.vn.gui.utils.ExtendedMenuItem;
import org.visnow.vn.gui.utils.RemoveComponentActionListener;
import org.visnow.vn.lib.gui.SimpleOneButtonGUI;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 * @author norkap
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class FieldViewer1D extends ModuleCore implements RemoveComponentActionListener
{

    protected ArrayList<ExtendedChart> charts;
    protected Viewer1DFrame frame = new Viewer1DFrame();
    private SimpleOneButtonGUI ui = null;
    private boolean dataSetChanged = false;
    private HashMap<String, String> currentConnectedComponents;

    public FieldViewer1D()
    {
        this.charts = new ArrayList<>();
        this.currentConnectedComponents = new HashMap<>();

        parameters.addParameterChangelistener(new ParameterChangeListener()
        {

            @Override
            public void parameterChanged(String name)
            {
                if (name.equals(DATA_SET.getName())) {
                    dataSetChanged = true;
                    startAction();
                } else {
                    dataSetChanged = false;
                }
            }
        });

        frame.addChartActionListener(new AddChartListener());

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                ui = new SimpleOneButtonGUI();

                ExtendedChartPanel chartPanel = new ExtendedChartPanel();
                chartPanel.addRemoveChartListener(getInstance());
                ChartData data = new ChartData();

                ExtendedChart chart = new ExtendedChart(data, chartPanel);
                charts.add(chart);

                frame.addChartToDisplay(chartPanel);
                frame.pack();
                frame.setTitle("VisNow Viewer1D");
                frame.setVisible(true);

                ui.addChangeListener(new ChangeListener()
                {
                    @Override
                    public void stateChanged(ChangeEvent evt)
                    {
                        frame.setVisible(true);
                        frame.setExtendedState(Frame.NORMAL);
                    }
                });
                setPanel(ui);
            }

        });

    }

    private FieldViewer1D getInstance()
    {
        return this;
    }

    public static InputEgg[] inputEggs = null;

    @Override
    public void onActive()
    {
        Parameters p = parameters.getReadOnlyClone();

        if (dataSetChanged) {

            XYSeriesCollection seriesCollection = p.get(DATA_SET);
            Map<String, String> legendLabelsMap = parameters.get(LEGEND_LABELS_MAP);

            for (ExtendedChart chart : charts) {
                chart.setDataSet(seriesCollection);
                chart.setLegendLabelsMap(legendLabelsMap);
                chart.update();
            }
            dataSetChanged = false;
        } else {
            Inputs moduleInputs = getInputs();
            Iterator it = moduleInputs.iterator();

            if (it.hasNext()) {

                currentConnectedComponents = new HashMap<>();
                XYSeriesCollection seriesCollection = new XYSeriesCollection();
                Map<String, String> legendLabelsMap = new HashMap<>();
                List<Link> links = ((Input) it.next()).getLinks();
                for (Link link : links) {
                    String moduleName = link.getName().getOutputModule();
                    VNRegularField field = (VNRegularField) link.getOutput().getData().getValue();

                    if (field != null) {

                        RegularField inFld;
                        DataArray da;

                        inFld = ((VNRegularField) field).getField();

                        for (int i = 0; i < inFld.getNComponents(); i++) {
                            da = inFld.getComponent(i);
                            if (da.isNumeric() && da.getVectorLength() == 1) {
                                String name = da.getName();
                                String[] userData = da.getUserData();
                                String legendName = "";
                                if (userData != null && userData.length >= 2 && userData[0].equals("description")) {
                                    legendName = userData[1];
                                }

                                try {
                                    int j = 2;
                                    String temp_name = name;
                                    while (true) {
                                        XYSeries series = seriesCollection.getSeries(name);
                                        name = temp_name + j;
                                        j++;
                                    }
                                } catch (UnknownKeyException ex) {
                                    seriesCollection.addSeries(FieldViewer1DCore.createSeries(da, inFld, name));
                                    legendLabelsMap.put(name, legendName);
                                    currentConnectedComponents.put(da.getName() + "&" + moduleName, name);
                                }
                            }
                        }
                    }
                }
                parameters.set(DATA_SET, seriesCollection);
            }
        }
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(DATA_SET, new XYSeriesCollection()),
            new Parameter<>(LEGEND_LABELS_MAP, new HashMap())
        };
    }

    private void setDataSet(XYSeriesCollection seriesCollection)
    {
        parameters.set(DATA_SET, seriesCollection);
    }

    private void setLegendLabelsMap(Map<String, String> legendLabelsMap)
    {
        parameters.set(LEGEND_LABELS_MAP, legendLabelsMap);
    }

    @Override
    public void onInputAttach(LinkFace link)
    {

        VNRegularField field = (VNRegularField) link.getOutput().getData().getValue();
        String moduleName = link.getOutput().getModuleBox().getName();

        if (field == null)
            return;

        RegularField inFld;
        DataArray da;
        boolean daAdded = false;
        XYSeriesCollection seriesCollection = parameters.get(DATA_SET);
        Map<String, String> legendLabelsMap = parameters.get(LEGEND_LABELS_MAP);

        inFld = ((VNRegularField) field).getField();

        for (int i = 0; i < inFld.getNComponents(); i++) {
            da = inFld.getComponent(i);
            if (da.isNumeric() && da.getVectorLength() == 1) {
                String name = da.getName();
                String[] userData = da.getUserData();
                String legendName = "";
                if (userData != null && userData.length >= 2 && userData[0].equals("description")) {
                    legendName = userData[1];
                }
                try {
                    int j = 2;
                    String temp_name = name;
                    while (true) {
                        XYSeries series = seriesCollection.getSeries(name);
                        name = temp_name + j;
                        j++;
                    }
                } catch (UnknownKeyException ex) {
                    seriesCollection.addSeries(FieldViewer1DCore.createSeries(da, inFld, name));
                    legendLabelsMap.put(name, legendName);
                    currentConnectedComponents.put(da.getName() + "&" + moduleName, name);
                    daAdded = true;
                }
            }
        }
        if (daAdded) {
            setDataSet(seriesCollection);
            setLegendLabelsMap(legendLabelsMap);
        }
    }

    @Override
    public void onInputDetach(LinkFace link)
    {

        String moduleName = link.getOutput().getModuleBox().getName();
        HashMap<String, String> newCurrentConnectedComponents = new HashMap<>();

        for (Map.Entry entry : currentConnectedComponents.entrySet()) {
            if (((String) entry.getKey()).split("&")[1].equals(moduleName)) {
                XYSeries seriesToBeRemoved = parameters.get(DATA_SET).getSeries((Comparable) entry.getValue());
                parameters.get(DATA_SET).removeSeries(seriesToBeRemoved);
            } else {
                newCurrentConnectedComponents.put((String) entry.getKey(), (String) entry.getValue());
            }
        }

        if (newCurrentConnectedComponents.size() != currentConnectedComponents.size()) {
            currentConnectedComponents = newCurrentConnectedComponents;
            parameters.fireParameterChanged(DATA_SET.getName());
        }
    }

    @Override
    public void removeComponent(Component comp)
    {
        int idx = frame.getComponentIndex(comp);
        charts.remove(idx);
        frame.removeChartFromDisplay((ExtendedChartPanel) comp);
        showLastComponentManagerContainer();
    }

    private void addChart()
    {
        ExtendedChartPanel chartPanel = new ExtendedChartPanel();
        chartPanel.addRemoveChartListener(this);
        ChartData data = new ChartData();
        ExtendedChart chart = new ExtendedChart(data, chartPanel);

        Parameters p = parameters.getReadOnlyClone();
        chart.setDataSet(p.get(DATA_SET));
        chart.setLegendLabelsMap(p.get(LEGEND_LABELS_MAP));
        chart.update();

        charts.add(chart);
        frame.addChartToDisplay(chartPanel);
        showLastComponentManagerContainer();
    }

    private void showLastComponentManagerContainer()
    {
        boolean show;
        for (int i = 0; i < charts.size(); i++) {
            show = (i == (charts.size() - 1));
            charts.get(i).getChartPanel().showComponentsList(show);
        }
    }

    class RemoveChartListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (e.getSource() instanceof ExtendedMenuItem) {
                ExtendedMenuItem mItem = (ExtendedMenuItem) e.getSource();
                removeComponent(SwingUtilities.getAncestorOfClass(ExtendedChartPanel.class, mItem.getAncestor()));
            }
        }

    }

    class AddChartListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            addChart();
        }

    }

    @Override
    public void onInitFinished()
    {
        updateFrameName();
    }

    @Override
    public void onNameChanged()
    {
        updateFrameName();
    }

    private void updateFrameName()
    {
        if (frame != null)
            frame.setTitle("VisNow Viewer1D - " + this.getApplication().getTitle() + " - " + this.getName());
    }

    @Override
    public void onDelete()
    {
        frame.dispose();
    }

}
