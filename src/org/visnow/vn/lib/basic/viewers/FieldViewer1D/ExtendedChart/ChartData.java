/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
package org.visnow.vn.lib.basic.viewers.FieldViewer1D.ExtendedChart;

import java.awt.Color;
import java.awt.Stroke;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.jfree.data.UnknownKeyException;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author norkap
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class ChartData
{

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ChartData.class);
    private XYSeriesCollection dataSet;
    private XYSeriesCollection dataSetToDisplay;
    private Map<String, String> legendLabelsMap;
    private final Map<String, Color> seriesColorMap;
    private final Map<String, Stroke> seriesStrokeMap;
    private float[][] stats;

    public ChartData()
    {
        this.seriesColorMap = new HashMap<>();
        this.seriesStrokeMap = new HashMap<>();
        this.dataSet = new XYSeriesCollection();
        this.dataSetToDisplay = new XYSeriesCollection();
        this.legendLabelsMap = new HashMap<>();
    }

    public void updateSeriesToDisplay()
    {
        if (dataSet.getSeriesCount() > 0) {
            if (dataSetToDisplay.getSeriesCount() > 0) {
                XYSeriesCollection newSeriesToDisplay = new XYSeriesCollection();
                for (int i = 0; i < dataSetToDisplay.getSeriesCount(); i++) {
                    try {
                        XYSeries series = dataSet.getSeries(dataSetToDisplay.getSeries(i).getKey());
                        newSeriesToDisplay.addSeries(series);
                    } catch (UnknownKeyException ex) {
                        LOGGER.info("Series removed", ex);
                    }
                }
                if (newSeriesToDisplay.getSeriesCount() > 0) {
                    dataSetToDisplay = newSeriesToDisplay;
                } else {
                    dataSetToDisplay.removeAllSeries();
                    dataSetToDisplay.addSeries(dataSet.getSeries(0));
                }
            } else {
                dataSetToDisplay.addSeries(dataSet.getSeries(0));
            }
        } else {
            dataSetToDisplay.removeAllSeries();
        }
    }

    public void initSeriesColorMap(Color c)
    {
        if (seriesColorMap.size() > 0) {
            seriesColorMap.clear();
        }
        if (dataSetToDisplay.getSeriesCount() > 0) {
            seriesColorMap.put((String) dataSetToDisplay.getSeriesKey(0), c);
        }
    }

    public void initSeriesStrokeMap(Stroke s)
    {
        if (seriesStrokeMap.size() > 0) {
            seriesStrokeMap.clear();
        }
        if (dataSetToDisplay.getSeriesCount() > 0) {
            seriesStrokeMap.put((String) dataSetToDisplay.getSeriesKey(0), s);
        }
    }

    public XYSeriesCollection getDataSet()
    {
        return dataSet;
    }

    public XYSeriesCollection getDataSetToDisplay()
    {
        return this.dataSetToDisplay;
    }
    
    public Map<String, String> getLegendLabelsMap()
    {
        return this.legendLabelsMap;
    }

    public int getDataSetNr()
    {
        return dataSet.getSeriesCount();
    }

    public int getDataSetToDisplayNr()
    {
        return this.dataSetToDisplay.getSeriesCount();
    }

    public String[] getSeriesNames()
    {
        String[] names = new String[dataSet.getSeriesCount()];

        for (int i = 0; i < names.length; i++) {
            names[i] = (String) dataSet.getSeriesKey(i);
        }
        return names;
    }

    public String[] getDisplayedSeriesNames()
    {
        String[] names = new String[dataSetToDisplay.getSeriesCount()];
        for (int i = 0; i < names.length; i++) {
            names[i] = (String) dataSetToDisplay.getSeriesKey(i);
        }
        return names;
    }

    public String[] getDisplayedSeriesXLabels()
    {
        Set<String> xLabels = new HashSet<>();
        String desc;
        for (int i = 0; i < dataSetToDisplay.getSeriesCount(); i++) {
            desc = dataSetToDisplay.getSeries(i).getDescription();
            xLabels.add(desc);
        }
        return (String[]) xLabels.toArray(new String[xLabels.size()]);
    }

    public String getDisplayedSeriesName(int idx)
    {
        return (String) dataSetToDisplay.getSeriesKey(idx);
    }

    public Map<String, Color> getSeriesColorMap()
    {
        return seriesColorMap;
    }
    
    public Map<String, Stroke> getSeriesStrokeMap()
    {
        return seriesStrokeMap;
    }

    public float[][] getStats()
    {
        return stats;
    }

    public void setStats(float[][] stats)
    {
        this.stats = new float[stats.length][4];

        for (int i = 0; i < stats.length; i++) {
            this.stats[i] = Arrays.copyOf(stats[i], stats[i].length);
        }
    }

    public void setDataSet(XYSeriesCollection dataSet)
    {
        this.dataSet = dataSet;
    }
    
    public void setLegendLebelsMap(Map<String, String> legendLabelsMap)
    {
        this.legendLabelsMap = legendLabelsMap;
    }

    public void addColorToMap(String key, Color c)
    {
        seriesColorMap.put(key, c);
    }

    public void addStrokeToMap(String key, Stroke s)
    {
        seriesStrokeMap.put(key, s);
    }
    
    public boolean addSeriesToDisplay(String key)
    {
        if (dataSetToDisplay.getSeriesIndex(key) == -1) {
            dataSetToDisplay.addSeries(dataSet.getSeries(key));
            return true;
        }
        return false;
    }

    public void removeSeriesToDisplay(String[] names)
    {
        for (String name : names) {
            dataSetToDisplay.removeSeries(dataSetToDisplay.getSeries(name));
            seriesColorMap.remove(name);
            seriesStrokeMap.remove(name);
        }
    }
}
