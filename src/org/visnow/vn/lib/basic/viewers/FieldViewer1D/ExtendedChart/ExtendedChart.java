/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
package org.visnow.vn.lib.basic.viewers.FieldViewer1D.ExtendedChart;

import org.visnow.vn.gui.utils.KeyListenerAdapter;
import java.awt.Color;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Map;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import org.jfree.chart.ChartPanel;
import org.jfree.data.xy.XYSeriesCollection;
import org.visnow.vn.lib.basic.viewers.FieldViewer1D.FieldViewer1DCore;
import org.visnow.vn.lib.basic.viewers.FieldViewer1D.utils.ColorEditor;
import org.visnow.vn.lib.basic.viewers.FieldViewer1D.utils.StrokeEditor;
import org.visnow.vn.gui.utils.DataSeriesChangedListener;

/**
 *
 * @author norkap
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class ExtendedChart implements DataSeriesChangedListener
{

    private final ChartData data;
    private final ExtendedChartPanel chartGUI;

    public ExtendedChart(ChartData data, ExtendedChartPanel chartGUI)
    {
        this.data = data;
        this.chartGUI = chartGUI;

        chartGUI.addSeriesListener(this);
        chartGUI.addRemoveSeriesListener(new RemoveSeriesListener());
        chartGUI.addSeriesColorChangedListener(new SeriesColorChangedListener());
        chartGUI.addSeriesStrokeChangedListener(new SeriesStrokeChangedListener());
        chartGUI.addKeyPressedOnSeriesTableListener(new KeyListenerAdapter()
        {

            @Override
            public void keyPressed(KeyEvent e)
            {
                if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                    removeSeries();
                }
            }
        });
    }

    public ExtendedChartPanel getChartPanel()
    {
        return chartGUI;
    }

    public void setDataSet(XYSeriesCollection chartData)
    {
        data.setDataSet(chartData);
    }
    
    public void setLegendLabelsMap(Map<String, String> legendLabelsMap)
    {
        data.setLegendLebelsMap(legendLabelsMap);
    }

    public void update()
    {
        data.updateSeriesToDisplay();
        chartGUI.setChartPanel(new ChartPanel(FieldViewer1DCore.createPannableXYChart(data.getDataSetToDisplay())));
        data.initSeriesColorMap(FieldViewer1DCore.PLOT_COLORS[0]);
        data.initSeriesStrokeMap(FieldViewer1DCore.PLOT_STROKES[0]);
        data.setStats(FieldViewer1DCore.computeStats(data.getDataSet()));
        chartGUI.initGUIComponents(data.getSeriesNames(), data.getStats(), data.getDisplayedSeriesXLabels());
        chartGUI.update(data.getDisplayedSeriesNames(), data.getStats(), data.getDisplayedSeriesXLabels(), data.getLegendLabelsMap());
        chartGUI.showChart();
    }

    private void addSeries(String[] names)
    {
        boolean seriesAdded = false;
        if (data.getDataSetNr() > 0) {
            for (String name : names) {
                if (data.addSeriesToDisplay(name)) {
                    int idx = data.getDataSetToDisplayNr();
                    Color c = chartGUI.addNewColorToPlot(--idx);
                    Stroke s = chartGUI.addNewStrokeToPlot(idx);
                    String key = data.getDisplayedSeriesName(idx);
                    data.addColorToMap(key, c);
                    data.addStrokeToMap(key, s);
                    seriesAdded = true;
                }
            }
            if (seriesAdded) {
                chartGUI.update(data.getDisplayedSeriesNames(), data.getStats(), data.getDisplayedSeriesXLabels(), data.getLegendLabelsMap());
            }
        }
    }

    private void addSeries()
    {
        if (data.getDataSetNr() > 0 && data.addSeriesToDisplay(chartGUI.getSelectedComponentName())) {
            int idx = data.getDataSetToDisplayNr();
            Color c = chartGUI.addNewColorToPlot(--idx);
            Stroke s = chartGUI.addNewStrokeToPlot(idx);
            String key = data.getDisplayedSeriesName(idx);
            data.addColorToMap(key, c);
            data.addStrokeToMap(key, s);
            chartGUI.update(data.getDisplayedSeriesNames(), data.getStats(), data.getDisplayedSeriesXLabels(), data.getLegendLabelsMap());
        }
    }

    private void removeSeries()
    {
        String[] names = chartGUI.getRemovedSeriesNames();
        if (names != null) {
            data.removeSeriesToDisplay(names);
            chartGUI.paintPlot(data.getSeriesColorMap(), data.getSeriesStrokeMap());
            chartGUI.update(data.getDisplayedSeriesNames(), data.getStats(), data.getDisplayedSeriesXLabels(), data.getLegendLabelsMap());
        }
    }

    private void updateSeriesColor(ColorEditor cEditor)
    {
        Color c = chartGUI.getSeriesColor(cEditor);
        chartGUI.setSeriesColor(chartGUI.getSelectedRow(), c);
        data.addColorToMap(chartGUI.getSelectedSeriesName(), c);
        chartGUI.update(data.getDisplayedSeriesNames(), data.getStats(), data.getDisplayedSeriesXLabels(), data.getLegendLabelsMap());
    }

    private void updateSeriesStroke(StrokeEditor sEditor)
    {
        Stroke s = chartGUI.getSeriesStroke(sEditor);
        chartGUI.setSeriesStroke(chartGUI.getSelectedRow(), s);
        data.addStrokeToMap(chartGUI.getSelectedSeriesName(), s);
        chartGUI.update(data.getDisplayedSeriesNames(), data.getStats(), data.getDisplayedSeriesXLabels(), data.getLegendLabelsMap());
    }

    @Override
    public void addSeriesToDisplay(String[] names)
    {
        addSeries(names);
    }

    class AddSeriesActionListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            addSeries();
        }

    }

    class RemoveSeriesListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            removeSeries();
        }

    }

    class SeriesColorChangedListener implements CellEditorListener
    {

        @Override
        public void editingStopped(ChangeEvent e)
        {
            if (e.getSource() instanceof ColorEditor)
                updateSeriesColor((ColorEditor) e.getSource());
        }

        @Override
        public void editingCanceled(ChangeEvent e)
        {

        }

    }

    class SeriesStrokeChangedListener implements CellEditorListener
    {

        @Override
        public void editingStopped(ChangeEvent e)
        {
            if (e.getSource() instanceof StrokeEditor)
                updateSeriesStroke((StrokeEditor) e.getSource());
        }

        @Override
        public void editingCanceled(ChangeEvent e)
        {

        }

    }
}
