//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
//</editor-fold>
package org.visnow.vn.lib.basic.viewers.FieldViewer1D.utils;

import java.awt.Stroke;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import org.jfree.chart.ui.StrokeChooserPanel;
import org.jfree.chart.ui.StrokeSample;
import org.visnow.vn.lib.basic.viewers.FieldViewer1D.FieldViewer1DCore;

/**
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class StrokeDialog
{

    private final JComponent[] components;
    private final String title;
    private final int messageType;
    private final JRootPane rootPane;
    private final String[] options;
    private final int optionIndex;
    private final StrokeChooserPanel panel;
    private final StrokeSample[] availableStrokeSamples;

    public StrokeDialog()
    {

        this.title = "Pick a Stroke";
        this.messageType = JOptionPane.PLAIN_MESSAGE;
        this.rootPane = null;
        this.options = new String[]{"OK", "Cancel"};
        this.optionIndex = 0;
        availableStrokeSamples = new StrokeSample[FieldViewer1DCore.PLOT_STROKES.length];
        for (int i = 0; i < availableStrokeSamples.length; i++) {
            availableStrokeSamples[i] = new StrokeSample(FieldViewer1DCore.PLOT_STROKES[i]);
        }
        panel = new StrokeChooserPanel(new StrokeSample(availableStrokeSamples[0].getStroke()), this.availableStrokeSamples);
        components = new JComponent[]{panel};
    }

    public int show()
    {
        int optionType = JOptionPane.OK_CANCEL_OPTION;
        Object optionSelection = null;

        if (options.length != 0) {
            optionSelection = options[optionIndex];
        }

        int selection = JOptionPane.showOptionDialog(rootPane,
                                                     components, title, optionType, messageType, null,
                                                     options, optionSelection);

        return selection;
    }

    public Stroke getSelectedStroke()
    {
        return panel.getSelectedStroke();
    }

}
