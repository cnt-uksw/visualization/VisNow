/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.FieldViewer3D.DataProvider;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.RegularField;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.ViewPanels.OrthosliceNumberChangedEvent;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.GlobalParams;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class DataProviderUI extends javax.swing.JPanel implements DataProviderParamsListener
{

    private RegularField field = null;
    private DataProvider dataProvider = null;
    private DataProviderParams dataProviderParams = null;
    private boolean silent = false;
    private int mode = GlobalParams.MODE_INDEXEDSLICES;

    /**
     * Creates new form DataProviderUI
     */
    public DataProviderUI()
    {
        initComponents();
        overlayComponentSelector.setScalarComponentsOnly(true);
        overlayComponentSelector.setTitle("overlay component");
        overlayComponentSelector.addChangeListener(new ChangeListener()
        {

            @Override
            public void stateChanged(ChangeEvent evt)
            {
                if (!silent) {
                    if (field == null) {
                        return;
                    }
                    silent = true;
                    int c = overlayComponentSelector.getComponent();
                    float up = (float)field.getComponent(c).getPreferredMaxValue();
                    float low = (float)(field.getComponent(c).getPreferredMaxValue()+field.getComponent(c).getPreferredMinValue())/2;
                    overlayRange.setDataSchema(field.getComponent(c).getSchema());
                    overlayRange.setLowUp(low, up);
                    silent = false;                    
                    dataProviderParams.setSimpleOverlayLowUp(low, up);
                    dataProviderParams.setSimpleOverlayComponent(c);
                }
            }
        });

    }

    public void updateGUI()
    {
        SwingInstancer.swingRunAndWait(new Runnable()
        {

            @Override
            public void run()
            {
                silent = true;

                if (dataProviderParams == null || field == null) {
                    componentColormappingPanel1.setEnabled(false);
                    overlaySlider.setEnabled(false);
                    overlayRange.setEmpty(true);
                    overlayRange.setEnabled(false);
                    overlayComponentSelector.setEnabled(false);
                    invertOverlayCB.setEnabled(false);
                    maskOverlayCB.setEnabled(false);
                    dataOverlayCB.setEnabled(false);
                    overlayColorEditor.setEnabled(false);
                    zSlider.setEnabled(false);
                    zField.setEnabled(false);
                    ySlider.setEnabled(false);
                    yField.setEnabled(false);
                    xSlider.setEnabled(false);
                    xField.setEnabled(false);
                    silent = false;
                    return;
                }

                componentColormappingPanel1.setEnabled(true);
                overlaySlider.setEnabled(true);
                if (field.getDims().length == 3) {
                    zSlider.setEnabled(mode==GlobalParams.MODE_INDEXEDSLICES || mode==GlobalParams.MODE_CUSTOMSLICE);
                    zField.setEnabled(mode==GlobalParams.MODE_INDEXEDSLICES || mode==GlobalParams.MODE_CUSTOMSLICE);
                    ySlider.setEnabled(mode==GlobalParams.MODE_INDEXEDSLICES || mode==GlobalParams.MODE_CUSTOMSLICE);
                    yField.setEnabled(mode==GlobalParams.MODE_INDEXEDSLICES || mode==GlobalParams.MODE_CUSTOMSLICE);
                    xSlider.setEnabled(mode==GlobalParams.MODE_INDEXEDSLICES || mode==GlobalParams.MODE_CUSTOMSLICE);
                    xField.setEnabled(mode==GlobalParams.MODE_INDEXEDSLICES || mode==GlobalParams.MODE_CUSTOMSLICE);
                }

                overlayComponentSelector.setEnabled(dataProviderParams.isSimpleOverlay());
                overlayComponentSelector.setComponent(dataProviderParams.getSimpleOverlayComponent());
                overlayRange.setEnabled(dataProviderParams.isSimpleOverlay());
                dataOverlayCB.setEnabled(true);
                dataOverlayCB.setSelected(dataProviderParams.isSimpleOverlay());
                invertOverlayCB.setEnabled(dataProviderParams.isSimpleOverlay());
                invertOverlayCB.setSelected(dataProviderParams.isSimpleOverlayInvert());
                maskOverlayCB.setEnabled(dataProviderParams.isSimpleOverlay());
                maskOverlayCB.setSelected(dataProviderParams.isSimpleOverlayMask());
                overlayColorEditor.setEnabled(dataProviderParams.isSimpleOverlay());
                overlayColorEditor.setColor(dataProviderParams.getSimpleOverlayColor());

                if (field.getDims().length == 3) {
                    xSlider.setValue(dataProviderParams.getOrthosliceNumber(0));
                    ySlider.setValue(dataProviderParams.getOrthosliceNumber(1));
                    zSlider.setValue(dataProviderParams.getOrthosliceNumber(2));
                }

                overlayRange.setDataSchema(field.getComponent(dataProviderParams.getSimpleOverlayComponent()).getSchema());
                overlayRange.setLowUp(dataProviderParams.getSimpleOverlayLow(), dataProviderParams.getSimpleOverlayUp());

                overlaySlider.setValue((int) (overlaySlider.getMaximum() * dataProviderParams.getOverlayOpacity()));

                componentColormappingPanel1.updateGUI();

                silent = false;

            }
        });
    }

    public void setInfield(final RegularField inField)
    {
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                silent = true;
                DataProviderUI.this.field = inField;
                dataProviderParams.getDataMappingParams().setInData(inField, (DataContainerSchema)null);

                if (inField == null) {
                    updateGUI();
                    return;
                }

                int[] dims = inField.getDims();
                if (dims == null || (dims.length != 3 && dims.length != 2)) {
                    DataProviderUI.this.field = null;
                    updateGUI();
                    return;
                }

                overlayComponentSelector.setDataSchema(inField.getSchema());

                if (dims.length == 3) {
                    int xMajorTick;
                    if (dims[0] < 20) {
                        xMajorTick = 5;
                    } else if (dims[0] < 50) {
                        xMajorTick = 10;
                    } else if (dims[0] < 100) {
                        xMajorTick = 20;
                    } else {
                        xMajorTick = 50;
                    }
                    xSlider.setMaximum(dims[0] - 1);
                    xSlider.setLabelTable(xSlider.createStandardLabels(xMajorTick));
                    xSlider.setValue(dims[0] / 2);

                    int yMajorTick;
                    if (dims[1] < 20) {
                        yMajorTick = 5;
                    } else if (dims[1] < 50) {
                        yMajorTick = 10;
                    } else if (dims[1] < 100) {
                        yMajorTick = 20;
                    } else {
                        yMajorTick = 50;
                    }
                    ySlider.setMaximum(dims[1] - 1);
                    ySlider.setLabelTable(ySlider.createStandardLabels(yMajorTick));
                    ySlider.setValue(dims[1] / 2);
                    ySlider.repaint();

                    int zMajorTick;
                    if (dims[2] < 20) {
                        zMajorTick = 5;
                    } else if (dims[2] < 50) {
                        zMajorTick = 10;
                    } else if (dims[2] < 100) {
                        zMajorTick = 20;
                    } else {
                        zMajorTick = 50;
                    }
                    zSlider.setMaximum(dims[2] - 1);
                    zSlider.setLabelTable(zSlider.createStandardLabels(zMajorTick));
                    zSlider.setValue(dims[2] / 2);
                    zSlider.repaint();
                }

                //lowUpMappingPanel.reset();
                overlayRange.reset();

                updateGUI();
                silent = false;
            }
        });
    }

    public void setDataProvider(DataProvider dp)
    {
        dataProvider = dp;
        dataProviderParams = dataProvider.getParams();
        dataProviderParams.getDataMappingParams().setActive(true);
        componentColormappingPanel1.setMap(dataProviderParams.getDataMappingParams().getColorMap0());
        dataProviderParams.addDataProviderParamsListener(this);
        updateGUI();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        topPanel = new javax.swing.JPanel();
        componentColormappingPanel1 = new org.visnow.vn.geometries.gui.ComponentColormappingPanel();
        overlayPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        overlaySlider = new javax.swing.JSlider();
        dataOverlayCB = new javax.swing.JCheckBox();
        overlayComponentSelector = new org.visnow.vn.lib.gui.DataComponentSelector();
        overlayRange = new org.visnow.vn.lib.gui.LowUpMappingUI();
        invertOverlayCB = new javax.swing.JCheckBox();
        maskOverlayCB = new javax.swing.JCheckBox();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        overlayColorEditor = new org.visnow.vn.gui.widgets.ColorEditor();
        slicesPanel = new javax.swing.JPanel();
        xPanel = new javax.swing.JPanel();
        xSlider = new javax.swing.JSlider();
        xField = new javax.swing.JTextField();
        yPanel = new javax.swing.JPanel();
        ySlider = new javax.swing.JSlider();
        yField = new javax.swing.JTextField();
        zPanel = new javax.swing.JPanel();
        zSlider = new javax.swing.JSlider();
        zField = new javax.swing.JTextField();

        setMinimumSize(new java.awt.Dimension(200, 700));
        setPreferredSize(new java.awt.Dimension(200, 800));
        setLayout(new java.awt.GridBagLayout());

        topPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("slices colormap"));
        topPanel.setLayout(new java.awt.BorderLayout());

        componentColormappingPanel1.setEnabled(false);
        topPanel.add(componentColormappingPanel1, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(topPanel, gridBagConstraints);

        overlayPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("overlay"));
        overlayPanel.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("overlay opacity");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        overlayPanel.add(jLabel1, gridBagConstraints);

        overlaySlider.setMaximumSize(new java.awt.Dimension(32767, 20));
        overlaySlider.setMinimumSize(new java.awt.Dimension(100, 20));
        overlaySlider.setPreferredSize(new java.awt.Dimension(200, 20));
        overlaySlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                overlaySliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        overlayPanel.add(overlaySlider, gridBagConstraints);

        dataOverlayCB.setText("thresholded overlay");
        dataOverlayCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dataOverlayCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        overlayPanel.add(dataOverlayCB, gridBagConstraints);

        overlayComponentSelector.setEnabled(false);
        overlayComponentSelector.setMinimumSize(new java.awt.Dimension(150, 45));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        overlayPanel.add(overlayComponentSelector, gridBagConstraints);

        overlayRange.setEnabled(false);
        overlayRange.setTitle("threshold range");
        overlayRange.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                overlayRangeStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        overlayPanel.add(overlayRange, gridBagConstraints);

        invertOverlayCB.setText("invert");
        invertOverlayCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                invertOverlayCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        overlayPanel.add(invertOverlayCB, gridBagConstraints);

        maskOverlayCB.setText("mask data");
        maskOverlayCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maskOverlayCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        overlayPanel.add(maskOverlayCB, gridBagConstraints);

        jPanel3.setLayout(new java.awt.GridBagLayout());

        jLabel2.setText("Overlay color:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
        jPanel3.add(jLabel2, gridBagConstraints);

        overlayColorEditor.setEnabled(false);
        overlayColorEditor.setTitle("");
        overlayColorEditor.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                overlayColorEditorStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 2, 5);
        jPanel3.add(overlayColorEditor, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        overlayPanel.add(jPanel3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        add(overlayPanel, gridBagConstraints);

        slicesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("indexed slices"));
        slicesPanel.setLayout(new java.awt.GridBagLayout());

        xPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "i slice", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 8))); // NOI18N
        xPanel.setLayout(new java.awt.GridBagLayout());

        xSlider.setFont(new java.awt.Font("Dialog", 0, 8)); // NOI18N
        xSlider.setMajorTickSpacing(20);
        xSlider.setMinorTickSpacing(1);
        xSlider.setPaintLabels(true);
        xSlider.setPaintTicks(true);
        xSlider.setMinimumSize(new java.awt.Dimension(120, 40));
        xSlider.setPreferredSize(new java.awt.Dimension(200, 42));
        xSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                xSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 4);
        xPanel.add(xSlider, gridBagConstraints);

        xField.setText("0");
        xField.setMinimumSize(new java.awt.Dimension(54, 19));
        xField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        xPanel.add(xField, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        slicesPanel.add(xPanel, gridBagConstraints);

        yPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "j slice", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 8))); // NOI18N
        yPanel.setLayout(new java.awt.GridBagLayout());

        ySlider.setFont(new java.awt.Font("Dialog", 0, 8)); // NOI18N
        ySlider.setMajorTickSpacing(20);
        ySlider.setMinorTickSpacing(1);
        ySlider.setPaintLabels(true);
        ySlider.setPaintTicks(true);
        ySlider.setMinimumSize(new java.awt.Dimension(120, 40));
        ySlider.setPreferredSize(new java.awt.Dimension(200, 42));
        ySlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                ySliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 4);
        yPanel.add(ySlider, gridBagConstraints);

        yField.setText("0");
        yField.setMinimumSize(new java.awt.Dimension(54, 19));
        yField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                yFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        yPanel.add(yField, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        slicesPanel.add(yPanel, gridBagConstraints);

        zPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "k slice", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 8))); // NOI18N
        zPanel.setLayout(new java.awt.GridBagLayout());

        zSlider.setFont(new java.awt.Font("Dialog", 0, 8)); // NOI18N
        zSlider.setMajorTickSpacing(20);
        zSlider.setMinorTickSpacing(1);
        zSlider.setPaintLabels(true);
        zSlider.setPaintTicks(true);
        zSlider.setMinimumSize(new java.awt.Dimension(120, 40));
        zSlider.setPreferredSize(new java.awt.Dimension(200, 42));
        zSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                zSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 4);
        zPanel.add(zSlider, gridBagConstraints);

        zField.setText("0");
        zField.setMinimumSize(new java.awt.Dimension(54, 19));
        zField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        zPanel.add(zField, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        slicesPanel.add(zPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(slicesPanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void zSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_zSliderStateChanged
        zField.setText("" + zSlider.getValue());
        if (!silent) {
            dataProviderParams.setOrthosliceNumber(2, zSlider.getValue());
        }

    }//GEN-LAST:event_zSliderStateChanged

    private void zFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zFieldActionPerformed
        try {
            zSlider.setValue(Integer.parseInt(zField.getText()));
        } catch (Exception e) {
            zField.setText("" + zSlider.getValue());
        }
    }//GEN-LAST:event_zFieldActionPerformed

    private void ySliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_ySliderStateChanged
        yField.setText("" + ySlider.getValue());
        if (!silent) {
            dataProviderParams.setOrthosliceNumber(1, ySlider.getValue());
        }

    }//GEN-LAST:event_ySliderStateChanged

    private void yFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_yFieldActionPerformed
        try {
            ySlider.setValue(Integer.parseInt(yField.getText()));
        } catch (Exception e) {
            yField.setText("" + ySlider.getValue());
        }
    }//GEN-LAST:event_yFieldActionPerformed

    private void xSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_xSliderStateChanged
        xField.setText("" + xSlider.getValue());
        if (!silent) {
            dataProviderParams.setOrthosliceNumber(0, xSlider.getValue());
        }

    }//GEN-LAST:event_xSliderStateChanged

    private void xFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xFieldActionPerformed
        try {
            xSlider.setValue(Integer.parseInt(xField.getText()));
        } catch (Exception e) {
            xField.setText("" + xSlider.getValue());
        }
    }//GEN-LAST:event_xFieldActionPerformed

    private void overlaySliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_overlaySliderStateChanged
    {//GEN-HEADEREND:event_overlaySliderStateChanged
        if (dataProviderParams == null) {
            return;
        }

        dataProviderParams.setOverlayOpacity((float) overlaySlider.getValue() / (float) overlaySlider.getMaximum());
    }//GEN-LAST:event_overlaySliderStateChanged

    private void dataOverlayCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dataOverlayCBActionPerformed
        dataProviderParams.setSimpleOverlay(dataOverlayCB.isSelected());
    }//GEN-LAST:event_dataOverlayCBActionPerformed

    private void overlayRangeStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_overlayRangeStateChanged
        if (!silent) {
            dataProviderParams.setSimpleOverlayLowUp(overlayRange.getLow(), overlayRange.getUp());
        }
    }//GEN-LAST:event_overlayRangeStateChanged

    private void invertOverlayCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_invertOverlayCBActionPerformed
        dataProviderParams.setSimpleOverlayInvert(invertOverlayCB.isSelected());
    }//GEN-LAST:event_invertOverlayCBActionPerformed

    private void maskOverlayCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maskOverlayCBActionPerformed
        dataProviderParams.setSimpleOverlayMask(maskOverlayCB.isSelected());
        if (maskOverlayCB.isSelected()) {
            overlaySlider.setValue(overlaySlider.getMaximum());
        }
    }//GEN-LAST:event_maskOverlayCBActionPerformed

    private void overlayColorEditorStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_overlayColorEditorStateChanged
        if (!silent) {
            dataProviderParams.setSimpleOverlayColor(overlayColorEditor.getColor());
        }
    }//GEN-LAST:event_overlayColorEditorStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.visnow.vn.geometries.gui.ComponentColormappingPanel componentColormappingPanel1;
    private javax.swing.JCheckBox dataOverlayCB;
    private javax.swing.JCheckBox invertOverlayCB;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JCheckBox maskOverlayCB;
    private org.visnow.vn.gui.widgets.ColorEditor overlayColorEditor;
    private org.visnow.vn.lib.gui.DataComponentSelector overlayComponentSelector;
    private javax.swing.JPanel overlayPanel;
    private org.visnow.vn.lib.gui.LowUpMappingUI overlayRange;
    private javax.swing.JSlider overlaySlider;
    private javax.swing.JPanel slicesPanel;
    private javax.swing.JPanel topPanel;
    private javax.swing.JTextField xField;
    private javax.swing.JPanel xPanel;
    private javax.swing.JSlider xSlider;
    private javax.swing.JTextField yField;
    private javax.swing.JPanel yPanel;
    private javax.swing.JSlider ySlider;
    private javax.swing.JTextField zField;
    private javax.swing.JPanel zPanel;
    private javax.swing.JSlider zSlider;
    // End of variables declaration//GEN-END:variables
    @Override
    public void onOrthosliceNumberChanged(OrthosliceNumberChangedEvent evt)
    {
        updateGUI();
    }

    @Override
    public void onRgbComponentChanged(RgbComponentChangedEvent evt)
    {
        updateGUI();
    }

    @Override
    public void onColormapChanged(ColormapChangedEvent evt)
    {
        updateGUI();
    }

    @Override
    public void onRgbComponentWeightChanged(RgbComponentWeightChangedEvent evt)
    {
    }

    @Override
    public void onCustomPlaneChanged(CustomPlaneChangedEvent evt)
    {
    }

    @Override
    public void onIsolineThresholdChanged(IsolineThresholdChangedEvent evt)
    {
    }

    @Override
    public void onOverlayOpacityChanged(DataProviderParamsEvent evt)
    {
    }

    @Override
    public void onOverlayChanged(DataProviderParamsEvent evt)
    {
        updateGUI();
    }

    @Override
    public void onCustomOrthoPlaneChanged(CustomOrthoPlaneChangedEvent evt)
    {
    }

    /**
     * @param mode the mode to set
     */
    public void setMode(int mode) {
        this.mode = mode;
        updateGUI();
    }

}
