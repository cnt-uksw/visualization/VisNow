/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry;

import java.util.ArrayList;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataObjectInterface;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.ComplexDataArray;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.dataarrays.DataObjectInterface;
import org.visnow.jscic.dataarrays.LogicDataArray;
import org.visnow.jscic.dataarrays.ObjectDataArray;
import org.visnow.jscic.dataarrays.StringDataArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.ObjectLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling 19 July
 * 2013
 */
public class GeometryFieldConverter
{

    public static IrregularField pac2field(ArrayList<PointDescriptor> pointsDescriptors,
                                           ArrayList<ConnectionDescriptor> connectionDescriptors,
                                           boolean addClassData,
                                           boolean addFieldDataComponents,
                                           RegularField inField)
    {
        int nPoints = pointsDescriptors.size();
        if (nPoints < 1) {
            return null;
        }

        IrregularField field = new IrregularField(nPoints);

        int[] cells = new int[nPoints];
        FloatLargeArray coords = new FloatLargeArray((long)nPoints * 3, false);
        int[] data = new int[nPoints];
        String[] names = new String[nPoints];
        int[] classes = new int[nPoints];
        float[] p;
        for (int i = 0; i < data.length; i++) {
            p = pointsDescriptors.get(i).getWorldCoords();
            data[i] = i + 1;
            coords.setFloat(3 * i, p[0]);
            coords.setFloat(3 * i + 1, p[1]);
            coords.setFloat(3 * i + 2, p[2]);
            cells[i] = i;
            names[i] = pointsDescriptors.get(i).getName();
            classes[i] = pointsDescriptors.get(i).getMembership();
        }

        field.setCurrentCoords(coords);
        field.addComponent(DataArray.create(data, 1, "points"));
        field.addComponent(DataArray.create(names, 1, "names"));
        if (addClassData) {
            field.addComponent(DataArray.create(classes, 1, "class"));
        }
        CellArray ca = new CellArray(CellType.POINT, cells, null, null);
        CellSet cs = new CellSet("points");
        cs.setCellArray(ca);
        cs.generateDisplayData(coords);
        field.addCellSet(cs);

        if (!connectionDescriptors.isEmpty()) {
            int nLines = connectionDescriptors.size();
            int[] ccells = new int[nLines * 2];
            String[] cnames = new String[nLines];
            for (int i = 0; i < nLines; i++) {
                ConnectionDescriptor cd = connectionDescriptors.get(i);
                ccells[2 * i] = pointsDescriptors.indexOf(cd.getP1());
                ccells[2 * i + 1] = pointsDescriptors.indexOf(cd.getP2());
                cnames[i] = cd.getName();
            }

            CellArray cca = new CellArray(CellType.SEGMENT, ccells, null, null);
            CellSet ccs = new CellSet("connections");
            ccs.setCellArray(cca);
            ccs.addComponent(DataArray.create(cnames, 1, "names"));
            ccs.generateDisplayData(coords);
            field.addCellSet(ccs);
        }

        if (addFieldDataComponents && inField != null) {
            int[] fDims = inField.getDims();
            int dimX = fDims[0];
            int dimY = fDims[1];

            int[] ind;
            int vlen;
            for (int i = 0; i < inField.getNComponents(); i++) {
                DataArray tmpDa = inField.getComponent(i);
                vlen = tmpDa.getVectorLength();
                switch (tmpDa.getType()) {
                    case FIELD_DATA_LOGIC:
                        LogicLargeArray lData = new LogicLargeArray(nPoints * vlen);
                        LogicLargeArray inLData = ((LogicDataArray) tmpDa).getRawArray();
                        for (int j = 0; j < nPoints; j++) {
                            ind = pointsDescriptors.get(j).getIndices();
                            for (int l = 0; l < vlen; l++) {
                                lData.setBoolean(j * vlen + l, inLData.getBoolean(vlen * (ind[2] * dimX * dimY + ind[1] * dimX + ind[0]) + l));
                            }
                        }
                        field.addComponent(DataArray.create(lData, vlen, tmpDa.getName()));
                        break;
                    case FIELD_DATA_BYTE:
                    case FIELD_DATA_SHORT:
                    case FIELD_DATA_INT:
                    case FIELD_DATA_FLOAT:
                    case FIELD_DATA_DOUBLE:
                    case FIELD_DATA_STRING:
                    case FIELD_DATA_OBJECT:
                        LargeArray inDData = tmpDa.getRawArray();
                        LargeArray dData = LargeArrayUtils.create(inDData.getType(), nPoints * vlen, false);
                        for (int j = 0; j < nPoints; j++) {
                            ind = pointsDescriptors.get(j).getIndices();
                            for (int l = 0; l < vlen; l++) {
                                dData.set(j * vlen + l, inDData.get(vlen * (ind[2] * dimX * dimY + ind[1] * dimX + ind[0]) + l));
                            }
                        }
                        field.addComponent(DataArray.create(dData, vlen, tmpDa.getName()));
                        break;
                    case FIELD_DATA_COMPLEX:
                        float[] fDataRe = new float[nPoints * vlen];
                        float[] fDataIm = new float[nPoints * vlen];
                        float[] inFDataRe = ((ComplexDataArray) tmpDa).getFloatRealArray().getFloatData();
                        float[] inFDataIm = ((ComplexDataArray) tmpDa).getFloatImaginaryArray().getFloatData();
                        int off;
                        for (int j = 0; j < nPoints; j++) {
                            ind = pointsDescriptors.get(j).getIndices();
                            for (int l = 0; l < vlen; l++) {
                                off = vlen * (ind[2] * dimX * dimY + ind[1] * dimX + ind[0]) + l;
                                fDataRe[j * vlen + l] = inFDataRe[off];
                                fDataIm[j * vlen + l] = inFDataIm[off];
                            }
                        }
                        field.addComponent(DataArray.create(new ComplexFloatLargeArray(new FloatLargeArray(fDataRe), new FloatLargeArray(fDataIm)), vlen, tmpDa.getName()));
                        break;
                    default:
                        throw new IllegalArgumentException("Unsupported array type.");
                }
            }
        }
        return field;
    }

    public static void field2pac(IrregularField ptsField,
                                 RegularField inField,
                                 ArrayList<PointDescriptor> pointsDescriptors,
                                 ArrayList<ConnectionDescriptor> connectionDescriptors)
    {

        int nPoints = (int) ptsField.getNNodes();

        String[] names = null;
        if (ptsField.getComponent("names") != null && ptsField.getComponent("names").getType() == DataArrayType.FIELD_DATA_STRING) {
            names = ((StringDataArray) ptsField.getComponent("names")).getRawArray().getData();
        } else if (ptsField.getComponent("name") != null && ptsField.getComponent("name").getType() == DataArrayType.FIELD_DATA_STRING) {
            names = ((StringDataArray) ptsField.getComponent("name")).getRawArray().getData();
        }

        int[] classes = null;
        if (ptsField.getComponent("class") != null && ptsField.getComponent("class").getType() == DataArrayType.FIELD_DATA_INT) {
            classes = (int[])ptsField.getComponent("class").getRawArray().getData();
        } else if (ptsField.getComponent("classes") != null && ptsField.getComponent("classes").getType() == DataArrayType.FIELD_DATA_INT) {
            classes = (int[])ptsField.getComponent("classes").getRawArray().getData();
        }

        int cId;
        float[] coords = ptsField.getCurrentCoords() == null ? null : ptsField.getCurrentCoords().getData();
        //int[] data = ptsField.getData(0).getIData().getData();        
        int c = 0;
        for (int i = 0; i < nPoints; i++) {
            String name;
            if (names != null) {
                name = names[i];
            } else {
                name = "ip" + c++;
            }

            if (classes != null) {
                cId = classes[i];
            } else {
                cId = -1;
            }

            if (inField == null) {
                float[] cp = new float[3];
                for (int j = 0; j < 3; j++) {
                    cp[j] = coords[3 * i + j];
                }
                pointsDescriptors.add(new PointDescriptor(name, null, cp, cId));
            } else {
                int[] p = inField.getIndices(coords[3 * i], coords[3 * i + 1], coords[3 * i + 2]);
                float[] cp = inField.getGridCoords((float) (p[0]), (float) (p[1]), (float) (p[2]));
                pointsDescriptors.add(new PointDescriptor(name, p, cp, cId));
            }
        }

        for (int i = 0; i < ptsField.getNCellSets(); i++) {
            CellSet cs = ptsField.getCellSet(i);
            if (cs.getCellArray(CellType.SEGMENT) != null &&
                cs.getCellArray(CellType.SEGMENT).getNCells() > 0) {
                CellArray ca = cs.getCellArray(CellType.SEGMENT);
                int[] nodes = ca.getNodes();
                int nConns = ca.getNCells();
                String[] connNames = null;
                if (cs.getNComponents() > 0 && cs.getComponent("names") != null && cs.getComponent("names").getType() == DataArrayType.FIELD_DATA_STRING) {
                    connNames = ((StringDataArray) cs.getComponent("names")).getRawArray().getData();
                } else if (cs.getNComponents() > 0 && cs.getComponent("name") != null && cs.getComponent("name").getType() == DataArrayType.FIELD_DATA_STRING) {
                    connNames = ((StringDataArray) cs.getComponent("name")).getRawArray().getData();
                }

                int[] conn = new int[2];
                for (int j = 0; j < nConns; j++) {
                    conn[0] = nodes[2 * j];
                    conn[1] = nodes[2 * j + 1];
                    String connName;
                    if (connNames != null) {
                        connName = connNames[i];
                    } else {
                        connName = "" + names[conn[0]] + "->" + names[conn[1]];
                    }
                    connectionDescriptors.add(new ConnectionDescriptor(connName, pointsDescriptors.get(conn[0]), pointsDescriptors.get(conn[1])));
                }
            }
        }
    }
}
