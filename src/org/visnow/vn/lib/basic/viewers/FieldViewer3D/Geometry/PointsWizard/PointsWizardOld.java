/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.PointsWizard;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Vector;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class PointsWizardOld
{

    private String[] pointDescriptions = null;
    private int currentIndex = -1;

    public PointsWizardOld(String filePath)
    {
        Vector<String> lines = new Vector<String>();

        try {
            File f = new File(filePath);
            BufferedReader input = new BufferedReader(new FileReader(f));

            String line = input.readLine();
            while (line != null) {
                lines.add(new String(line));
                line = input.readLine();
            }
            input.close();

            pointDescriptions = new String[lines.size()];
            for (int i = 0; i < lines.size(); i++) {
                pointDescriptions[i] = lines.get(i);
            }
        } catch (Exception ex) {
            pointDescriptions = null;
            return;
        }
    }

    public boolean start()
    {
        currentIndex = 0;
        if (pointDescriptions == null || currentIndex > pointDescriptions.length - 1) {
            currentIndex = -1;
            return false;
        }
        return true;
    }

    public boolean next()
    {
        currentIndex++;
        if (pointDescriptions == null || currentIndex > pointDescriptions.length - 1) {
            currentIndex = -1;
            return false;
        }
        return true;
    }

    /**
     * @return the pointDescriptions
     */
    public String[] getPointDescriptions()
    {
        return pointDescriptions;
    }

    public String getPointDescription(int i)
    {
        if (i >= 0 && i < pointDescriptions.length)
            return pointDescriptions[i];
        else
            return "";
    }

    /**
     * @return the currentIndex
     */
    public int getCurrentIndex()
    {
        return currentIndex;
    }

    public void back()
    {
        if (currentIndex > 0)
            currentIndex--;
    }

}
