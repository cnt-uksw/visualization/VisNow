/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Point;
import java.awt.Window;

/**
 * Centers itself over a component (see method {@link #centerOn(java.awt.Component) }).
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class CenterableJDialog extends javax.swing.JDialog
{

    /**
     * Set's the dialog's position to the center of the component given.
     *
     * @param component component to be centered over
     */
    public void centerOn(Component component)
    {
        if (component == null) {
            throw new NullPointerException("component must not be null");
        }

        Point p = component.getLocationOnScreen();
        p.x += component.getWidth() / 2;
        p.x -= this.getWidth() / 2;

        p.y += component.getHeight() / 2;
        p.y -= this.getHeight() / 2;

        setLocation(p);

    }

    // below all the constructors - only super(...) is called there
    public CenterableJDialog()
    {
        super();
    }

    public CenterableJDialog(Frame owner)
    {
        super(owner);
    }

    public CenterableJDialog(Frame owner, boolean modal)
    {
        super(owner, modal);
    }

    public CenterableJDialog(Frame owner, String title)
    {
        super(owner, title);
    }

    public CenterableJDialog(Frame owner, String title, boolean modal)
    {
        super(owner, title, modal);
    }

    public CenterableJDialog(Frame owner, String title, boolean modal, GraphicsConfiguration gc)
    {
        super(owner, title, modal, gc);
    }

    public CenterableJDialog(Dialog owner)
    {
        super(owner);
    }

    public CenterableJDialog(Dialog owner, boolean modal)
    {
        super(owner, modal);
    }

    public CenterableJDialog(Dialog owner, String title)
    {
        super(owner, title);
    }

    public CenterableJDialog(Dialog owner, String title, boolean modal)
    {
        super(owner, title, modal);
    }

    public CenterableJDialog(Dialog owner, String title, boolean modal, GraphicsConfiguration gc)
    {
        super(owner, title, modal, gc);
    }

    public CenterableJDialog(Window owner)
    {
        super(owner);
    }

    public CenterableJDialog(Window owner, ModalityType modalityType)
    {
        super(owner, modalityType);
    }

    public CenterableJDialog(Window owner, String title)
    {
        super(owner, title);
    }

    public CenterableJDialog(Window owner, String title, ModalityType modalityType)
    {
        super(owner, title, modalityType);
    }

    public CenterableJDialog(Window owner, String title, ModalityType modalityType, GraphicsConfiguration gc)
    {
        super(owner, title, modalityType, gc);
    }
}
//revised.
