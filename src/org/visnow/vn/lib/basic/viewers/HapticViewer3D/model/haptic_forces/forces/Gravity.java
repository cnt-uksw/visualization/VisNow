/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces;

import java.math.BigDecimal;
import org.jogamp.vecmath.Point3f;
import org.jogamp.vecmath.Tuple3f;
import org.jogamp.vecmath.Vector3f;
import org.apache.log4j.Logger;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ILocalToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ITrackerToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.CoordinateSystem;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.HapticLocationData;
import static org.apache.commons.math3.util.FastMath.*;

/**
 * Gravity force. Its center is described in local coordinates, so it will be in the same point of
 * data even after rotating the scene.
 * <p/>
 * NOTE: The force is generated also when cursor is outside of the haptic outline.
 * <p/>
 * The formula used to be simply inversly proportional to the square of the distance,
 * but now it is more complicated:
 * <pre>F = (c * x^2) / ( eps + x^4 )</pre>
 * <p/>
 * It has maximum in
 * <pre>c / (2 * sqrt(eps))</pre>
 * 0 in x = 0 and is more or less inversly proportional for
 * distances greater than max point.
 * <p/>
 * <
 * p/>
 * TODO: Depending on field dimensions pointer may behave stable or sway around the center of
 * gravity.
 * To avoid swaying either epsilon constant should be heuristically computed based on local->tracker
 * transform (relation of distances between those two worlds) or at least epsilon should be editable
 * in the edit gravity dialog.
 * <p/>
 * @see <a href="http://fooplot.com/plot/9rj6qsxeb6">FooPlot with the plot of gravity force</a>
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 */
public class Gravity extends AbstractForce
{

    private static final float epsilon;
    /**
     * The proper value is rather INDEPENDENT on size of haptic field. It's epsilon that should be
     * modified according to size of haptic field.
     */
    private static final BigDecimal defaultMaxForceValue = new BigDecimal(5);
    //
    private final static CoordinateSystem forceCoordinateSystem = CoordinateSystem.LOCAL;

    static {
        epsilon = (float) pow(0.02, 4); //TODO: should be editable or changing automagically depending on dimensions of haptic field
    }
    private BigDecimal maxForce;
    private double constant;
    private Tuple3f center;
    private float cuttingForceLevel = 0;
    //
    private static final Logger LOGGER = Logger.getLogger(new Throwable().getStackTrace()[0].getClassName());

    public Gravity()
    {
        this(defaultMaxForceValue, new Point3f());
    }

    public Gravity(BigDecimal maxForce, Tuple3f center)
    {
        super(forceCoordinateSystem);

        if (center == null) {
            throw new NullPointerException("Argument 'center' cannot be null");
        }
        this.setMaximumForce(maxForce);

        this.center = center;
    }

    /**
     * Copy constructor. To be used only by
     * <code>clone()</code> method.
     * <p/>
     * @param force Object to be copied from
     */
    public Gravity(Gravity force)
    {
        super(force);

        center = new Point3f(force.center);
        constant = force.constant;
        maxForce = force.maxForce;
        setCuttingForceLevel(force.cuttingForceLevel);
    }

    /**
     * <pre>maxForce = c / (2 * sqrt(eps))  =>  c = 2 * sqrt(eps) * maxForce </pre>
     */
    public static double maxForceToConstant(float maxForce, float epsilon)
    {
        return 2 * maxForce * sqrt(epsilon);
    }

    /**
     * <pre>F = (c * x^2) / ( eps + x^4 )</pre>
     */
    public static float calculateForce(double constant, float epsilon, float distSquared)
    {
        return (float) (constant * distSquared / (epsilon + distSquared * distSquared));
    }

    /**
     * Calculates gravity force using formula: k / x^2, where:
     * k - constant
     * x - distance between position and
     * <code>center</code> passed to the constructor.
     *
     * @param locationData Position and velocity getter
     * @param out_force    Output force calculated by this method (<code>outForce</code> doesn't have to
     *                     have been initialized)
     */
    @Override
    public void getForce(HapticLocationData locationData, Vector3f out_force)
        throws ITrackerToVworldGetter.NoDataException,
        ILocalToVworldGetter.NoDataException
    {

        float forceMagnitude, distSquared;

        Point3f position = locationData.getCurrentLocalPosition();
        out_force.sub(center, position);

        // now outForce contains a vector with proper direction
        distSquared = out_force.lengthSquared();

        out_force.normalize();
        // now outForce vector has length 1

        // calculate force value
        forceMagnitude = calculateForce(constant, epsilon, distSquared);
        //        forceMagnitude = constant.floatValue() * distSquared / (epsilon + distSquared * distSquared);

        // safety force scale
        if (cuttingForceLevel != 0 &&
            forceMagnitude > cuttingForceLevel) {
            String message = "CUTTING gravity force (" + forceMagnitude + "N) " +
                " to the magnitude of " + cuttingForceLevel + "N";
            LOGGER.info(message);
            forceMagnitude = cuttingForceLevel;
        }

        // set force value 
        out_force.scale(forceMagnitude);
    }

    public final void setCuttingForceLevel(float cuttingForceLevel)
    {
        this.cuttingForceLevel = cuttingForceLevel;
    }

    public Tuple3f getCenter()
    {
        return center;
    }

    public void setCenter(Tuple3f center)
    {
        this.center = center;
    }

    public double getConstant()
    {
        return constant;
    }

    public static float getEpsilon()
    {
        return epsilon;
    }

    public BigDecimal getDefaultMaxForceValue()
    {
        return defaultMaxForceValue;
    }

    public BigDecimal getMaxForceValue()
    {
        return maxForce;
    }

    public final void setMaximumForce(BigDecimal maxForce)
    {
        this.maxForce = maxForce;
        this.constant = maxForceToConstant(maxForce.floatValue(), epsilon);
    }

    @Override
    public IForce clone()
    {
        return new Gravity(this);
    }

    @Override
    public String getName()
    {
        String name = "Gravity [max F=" + maxForce + "N, p=" + center + "]";
        return name;
    }
}
