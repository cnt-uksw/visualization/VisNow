/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;
import javax.swing.event.ListDataListener;
import org.jogamp.vecmath.Vector3f;
import org.apache.log4j.Logger;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.IDragListener;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ILocalToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ILocalToVworldListener;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ITrackerToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.haptics.IHapticDevice;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.Damping;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.DampingField;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.Gravity;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.IForce;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.IForceListModel;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.Spring;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.VectorFieldForce;
import static org.apache.commons.math3.util.FastMath.*;

/**
 * Manages forces in a single device context. Enables adding and removing forces,
 * and calculates their values in realtime.
 *
 * This class is designed in such a way that function
 * <code>getLastAppliedForce()</code> can be called in
 * realtime.
 * Synchronization is enforced by a local copy of a set of forces.
 * <p/>
 * Note: There is one
 * <code>ForceContext</code> object per device (
 * <code>IHapticDevice</code>) per
 * <code>HapticViewer3D<code/>, so one
 * device can be
 * linked to several
 * <code>DeviceContext</code>s and several
 * <code>ForceContext</code>s.
 * <p/>
 * <
 * code>ForceContext</code> will be always created by {@link DeviceContext} and after creation it
 * will be added by DeviceContext as a listener to a
 * <code>localToVworldGetter</code>.
 * <p/>
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class ForceContext implements IForceListModel, ILocalToVworldListener, IDragListener
{

    private static final Logger LOGGER
        = Logger.getLogger(new Throwable().getStackTrace()[0].getClassName());
    /**
     * Stores forces to be shown in NewForceDialog. Each force that want to appear on the list,
     * must add itself to it in static block.
     */
    final public static Vector<NewForceWrapper> newForces = new Vector<NewForceWrapper>();

    static {
        newForces.add(new NewForceWrapper(new VectorFieldForce()));
        newForces.add(new NewForceWrapper(new DampingField()));
        newForces.add(new NewForceWrapper(new Gravity()));
        newForces.add(new NewForceWrapper(new Spring()));
        newForces.add(new NewForceWrapper(new Damping()));
    }
    /**
     *
     * Used for read-only operations on a forces list without acquiring a lock (esp. a real-time
     * thread).
     * <p/>
     * The idea of a copy synchronization (proxy synchronization) was used here. A read copy of a
     * list is
     * maintained in order to provide instant access for reading
     * <code>forces</code> in a real-time thread ({@link #computeForce}), while writing will require
     * synchronization. When write operation finishes, the copy is cloned to the read copy and
     * references are updated so
     * <code>forces</code> will point to the modified object and
     * <code>forcesCopy</code> will point to a newly cloned object.
     * <p/>
     * NOTE: This object should be used as a <b>read-only</b>. For write operations and for read
     * operations that will influence write operations (e.g. checking size of the list) use
     * {@link #forcesWrite}.
     * <p/>
     * NOTE2: This method of synchronization was called somewhere in the code
     * "proxy-synchronization" by Krzysztof Madejski, the first author of haptics module.
     * <p/>
     * NOTE3: If
     * <code>forces</code> are to be used in the real-time thread in other way than by iterating in
     * for loop or more than in one place, it is recommended to copy
     * <code>forces</code> to a local variable before first use -
     * <code>forces</code> might change reference in the meantime.
     * <p/>
     */
    private LinkedList<IForce> forcesRead = new LinkedList<IForce>();
    /**
     * Used for read and write operations on a forces list. Requires acquiring a lock on this
     * object (
     * <code>synchronized</code>) and {@link #synchronizeForces() } just before releasing the lock.
     * <p/>
     * For details - see comment for {@link #forcesRead}.
     * <p/>
     */
    final private LinkedList<IForce> forcesWrite = new LinkedList<IForce>();
    //
    protected DeviceContext deviceContext;
    protected IHapticDevice device;
    protected ILocalToVworldGetter localToVworldGetter;
    protected ITrackerToVworldGetter trackerToVworldGetter = null;
    //
    /**
     * This object handles lazy transformation of position and velocity between different
     * coordinate systems.
     */
    HapticLocationData locationData = new HapticLocationData();
    //
    protected Vector3f lastAppliedForce = new Vector3f();
    protected boolean enabled = false;
    /**
     * The ForceContext is dataBlocked when necessary data could not be retrieved - the
     * localToVworldGetter is not ready or trackerToVworldGetter was not set yet.
     */
    protected boolean dataBlocked = true;
    /**
     * Blocked while scene is being dragged by haptic device.
     */
    protected boolean dragBlocked = false;
    //
    protected boolean scaleAtan = true;
    /**
     * Store the state of localToVworldGetter
     */
    protected boolean localToVworldReady = false;
    //
    final ForceContextListenersSupport listenersSupport = new ForceContextListenersSupport();
    /**
     * TODO DEBUG: delete that KeyEventDispatcher.
     */
    private final KeyEventDispatcher keyEventDispatcher;

    /**
     * Only DeviceContext object should create ForceContext.
     *
     * @param deviceContext       caller DeviceContext
     * @param localToVworldGetter object that returns an up-to-date local-to-vworld transform
     */
    ForceContext(DeviceContext deviceContext,
                 ILocalToVworldGetter localToVworldGetter)
    {
        this.deviceContext = deviceContext;
        this.device = deviceContext.getDevice();
        this.localToVworldGetter = localToVworldGetter;
        this.locationData.setLocalToVworldGetter(localToVworldGetter);

        keyEventDispatcher = new KeyEventDispatcher()
        {
            @Override
            public boolean dispatchKeyEvent(KeyEvent e)
            {
                // space pressed - emergency disable all forces
                if (e.getID() == KeyEvent.KEY_PRESSED) {
                    if (e.getKeyCode() == KeyEvent.VK_A) {
                        scaleAtan = true;
                        LOGGER.error("scale set to atan()");
                    }
                    if (e.getKeyCode() == KeyEvent.VK_S) {
                        scaleAtan = false;
                        LOGGER.error("scale set to identity");
                    }
                }
                return false;
            }
        };

        //TODO: this is a debug key listener for testing clamping force using Know's atan formula or not
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(keyEventDispatcher);
    }

    void setTrackerToVworld(ITrackerToVworldGetter trackerToVworldGetter)
    {
        if (this.trackerToVworldGetter != null)
            throw new IllegalArgumentException("Cannot set again trackerToVworldGetter");
        this.trackerToVworldGetter = trackerToVworldGetter;
        this.locationData.setTrackerToVworldGetter(trackerToVworldGetter);
        updateDataBlocked();
    }

    /**
     * Computes forces in this context. Method is called at real-time rate (~1000 times a second).
     * <p/>
     * This method doesn't check whether ForceContext is enabled or not. This should be checked
     * in DeviceContext.
     * <p/>
     * @param outputForce Cumulated force - in device (tracker) coordinate system
     */
    public void computeForce(Vector3f outputForce)
    {
        if (dataBlocked)
            throw new IllegalStateException("ForceContext is blocked, computeForce should not be called");

        //reset values
        outputForce.set(0, 0, 0);

        // TODO: maybe here should be some exception handling?
        device.getPosition(locationData.currentTrackerPosition);
        device.getVelocity(locationData.currentTrackerVelocity);

        locationData.notifyNewDataSet();

        // add to the output force all forces from forces Set
        Vector3f tmpForce = new Vector3f();
        Iterable<IForce> localForcesRead = forcesRead;
        for (IForce force : localForcesRead) {
            if (force.isEnabled()) {
                try {
                    force.getForce(locationData,
                                   tmpForce);

                    float forceValue = tmpForce.length();

                    // transform between coordinate systems (aka c.s.)
                    switch (force.getForceCoordinateSystem()) {
                        case LOCAL:
                            locationData.getLocalToVworld().transform(tmpForce); // now tmpForce is in vworld c.s.
                        // no break is intentional
                        case VWORLD:
                            locationData.getVworldToTracker().transform(tmpForce); // now tmpForce is in tracker c.s.
                            break;

                        case TRACKER:
                            // we are there, nothing to do :)
                            break;

                        default: // including null - throw exception
                            throw new IllegalStateException("Coordinate system cannot be null!");
                    }

                    // calculate scale that will restore the value of the force
                    float newForceValue = tmpForce.length();

                    if (newForceValue != 0) {
                        float scale = forceValue / newForceValue;
                        tmpForce.scale(scale);
                    }

                    /* 
                     * If maximum force was provided (see IForce.getMaxAllowedValue),
                     * scale force using arc tan function:
                     * F = F * (|F_max| * atan(s * |F| / |F_max|) / (|F| * PI/2)),
                     * where:
                     *      |F_max| - maximum value of force
                     *      s - scale (per force)
                     * 
                     * Formula was devised by Know on 2013.09.16.
                     */
                    float value = tmpForce.length();
                    float maxValue = force.getMaxAllowedValue();
                    float scale = force.getForceScale();
                    if (maxValue != Float.POSITIVE_INFINITY &&
                        value != 0 &&
                        maxValue != 0) {

                        //                        float forceScale =
                        //                                (float) (maxValue * atan(scale * value / maxValue) * 2
                        //                                / (value * PI));
                        float forceScale;

                        if (scaleAtan)
                            forceScale = (float) (maxValue * atan(scale * value / maxValue) * 2 /
                                (value * PI));
                        else
                            forceScale = scale;

                        tmpForce.scale(forceScale);
                    }
                    outputForce.add(tmpForce);
                } catch (ILocalToVworldGetter.NoDataException ex) {
                    /*
                     * such a situation may be temporary (when detaching port data) and it's 
                     * perfectly normal, but if it lasts longer then for a fraction of second,
                     * it might be a bug 
                     */
                    LOGGER.info("Force " + force.getName() + " could not be computed. Error: \n" + ex);
                } catch (ITrackerToVworldGetter.NoDataException ex) {
                    /*
                     * such a situation may be temporary (when detaching port data) and it's 
                     * perfectly normal, but if it lasts longer then for a fraction of second,
                     * it might be a bug 
                     */
                    LOGGER.info("Force " + force.getName() + " could not be computed. Error: \n" + ex);
                }
            }
        }

        //cache force to use with external devices
        lastAppliedForce.set(outputForce);
    }

    /**
     * Return last applied force
     *
     * @return last applied force (vector x, y, z)
     */
    public Vector3f getLastAppliedForce()
    {
        return lastAppliedForce;
    }

    /* ========= enabled flag ========== */
    /**
     * Sets enabled status of this force context
     *
     * @param enabled new value
     */
    public void setEnabled(boolean enabled)
    {
        if (enabled != this.enabled) {
            this.enabled = enabled;
            //            fireForcesEnableChanged(enabled);
        }
    }

    /**
     * Is this context enabled
     *
     * @return enabled
     */
    public boolean isEnabled()
    {
        return enabled;
    }

    /* ========= dataBlocked flag ========== */
    /**
     * Block or unblock computing forces because of data changes. It changes {@link #dataBlocked}
     * flag. This method should not be called by other objects, only by onLocalToVworldSet().
     * <p/>
     * @param dataBlocked new value
     */
    protected void setDataBlocked(boolean dataBlocked)
    {
        if (dataBlocked != this.dataBlocked) {
            this.dataBlocked = dataBlocked;
            // fireStateChanged()
        }
    }

    /**
     * Is this context dataBlocked or not (see {@link #dataBlocked} flag).
     *
     * @return blocked or not
     */
    public boolean isDataBlocked()
    {
        return dataBlocked;
    }

    /**
     * Called after change of state of localToVworldReady or setting trackerToVworldGetter
     */
    private void updateDataBlocked()
    {
        boolean oldDataBlocked = isDataBlocked();
        setDataBlocked(localToVworldReady == false || trackerToVworldGetter == null);
        boolean newDataBlocked = isDataBlocked();
        if (!oldDataBlocked && newDataBlocked) {
            // if forces were blocked because of lack of data, delete all forces except for safety damping
            removeAllUserForces();
        }
    }

    /* ========= dragBlocked flag ========== */
    /**
     * Block or unblock computing forces because of dragging using haptic device. It changes
     * {@link #dragBlocked}
     * flag. This method should not be called by other objects, only by onDragEvent().
     * <p/>
     * @param dragBlocked new value
     */
    protected void setDragBlocked(boolean dragBlocked)
    {
        if (dragBlocked != this.dragBlocked) {
            this.dragBlocked = dragBlocked;
            // fireStateChanged()
        }
    }

    /**
     * Is this context dragBlocked or not (see {@link #dragBlocked} flag).
     *
     * @return blocked or not
     */
    public boolean isDragBlocked()
    {
        return dragBlocked;
    }

    ///***** forcesWrite *******/
    //
    @Override
    public void addForce(IForce force)
    {
        addForce(force, -1);
    }

    @Override
    public void addForce(IForce force, int onIndex)
    {
        addForce(force, onIndex, true);
    }

    /**
     * Inserts
     * <code>force</code> on index
     * <code>onIndex</code> (shifts all following forces). To insert force at the end, set
     * <code>onIndex</code> to -1 or call {@link #addForce(IForce)
     * }.
     * <p/>
     * @param force
     * @param onIndex
     * @param fireAdded whether an event notyfing about the force added should be fired - used in
     *                  {@link #duplicateForce} where <code>addForce</code> is called
     *                  inside <code>synchronized</code> block, what should be avoided.
     */
    protected void addForce(IForce force, int onIndex, boolean fireAdded)
    {
        synchronized (forcesWrite) {
            if (onIndex == -1)
                onIndex = forcesWrite.size();
            forcesWrite.add(onIndex, force);
            synchronizeForces();
        }
        if (fireAdded)
            listenersSupport.fireForceAdded(force, onIndex, this);
    }

    @Override
    public void remove(int index)
    {
        synchronized (forcesWrite) {
            IForce force = forcesWrite.get(index);
            if (!force.canBeChanged())
                return;
            forcesWrite.remove(index);
            synchronizeForces();
        }
        listenersSupport.fireForceRemoved(index, this);
    }

    /**
     * Removes all forces except those uneditable by user (so far it's only safety damping)
     */
    protected void removeAllUserForces()
    {
        int fromIndex = 0, toIndex;
        synchronized (forcesWrite) {
            toIndex = forcesWrite.size() - 1;
            Iterator<IForce> it = forcesWrite.iterator();
            while (it.hasNext()) {
                IForce force = it.next();
                if (force.canBeChanged())
                    it.remove();
            }
            synchronizeForces();
        }

        // TODO LOW: list will be notified that ALL forces were removed. It's not true, because unchangeable ones weren't. So far it works, but it may need a fix.
        listenersSupport.fireForceRemoved(fromIndex, toIndex, this);

    }

    /**
     * Duplicates a force from a given index (inserts it as the next index).
     * <p/>
     * NOTE: It DOES NOT check whether
     * <code>index</code> is valid.
     */
    @Override
    public int duplicateForce(int index)
    {
        IForce force;
        int newIndex;

        synchronized (forcesWrite) {
            IForce selectedForce = forcesWrite.get(index);
            if (!selectedForce.canBeChanged())
                return -1;
            newIndex = index + 1;
            force = selectedForce.clone();
            addForce(force, newIndex, false);
        }
        listenersSupport.fireForceAdded(force, newIndex, this);
        return newIndex;
    }

    /**
     * Get size of forces list (acquires a lock and uses {@link #forcesWrite}).
     * <p/>
     * @return forces count
     */
    @Override
    public int getSize()
    {
        synchronized (forcesWrite) {
            return forcesWrite.size();
        }
    }

    /**
     * The same as {@link #get}. Needed for
     * <code>ListModel</code> interface (
     * <code>IForceListModel</code>).
     * <p/>
     * @param index index
     * <p>
     * @return force from the list
     */
    @Override
    public Object getElementAt(int index)
    {
        return this.get(index);
    }

    @Override
    public void set(IForce oldForce, IForce newForce)
    {
        if (!oldForce.canBeChanged())
            return;

        int index;
        synchronized (forcesWrite) {
            index = forcesWrite.indexOf(oldForce);
            if (index < 0)
                throw new IllegalArgumentException(); // should not happen if forces are not edited in other threads
            forcesWrite.set(index, newForce);
            synchronizeForces();
        }
        listenersSupport.fireForceChanged(index, this);
    }

    @Override
    public void swapEnabled(int index)
    {
        IForce force;
        force = get(index);
        if (!force.canBeChanged())
            return;

        force.setEnabled(!force.isEnabled());
        listenersSupport.fireForceChanged(index, this);
    }

    /**
     * Gets force from the specified index (acquires a lock and uses {@link #forcesWrite}).
     * <p/>
     * @param index index
     * <p>
     * @return force from the list
     */
    @Override
    public IForce get(int index)
    {
        synchronized (forcesWrite) {
            return forcesWrite.get(index);
        }
    }

    /**
     * Clones (shallow copy) {@link #forcesWrite} to {@link #forcesRead}. Use it to synchronize
     * forcesRead with
     * forcesWrite - call it just before exiting each
     * <code>synchronized(forcesWrite)</code> block in which the list itself was changed. Note that
     * changing the elements of the list doesn't require synchronization, because both forcesRead
     * and forcesWrite store references to the same objects.
     */
    private void synchronizeForces()
    {
        synchronized (forcesWrite) {
            forcesRead = (LinkedList<IForce>) forcesWrite.clone();
        }
    }

    @Override
    public void onLocalToVworldSet()
    {
        localToVworldReady = true;
        updateDataBlocked();
    }

    @Override
    public void onLocalToVworldUnset()
    {
        localToVworldReady = false;
        updateDataBlocked();
    }

    @Override
    public void onDragEvent(DragEvent e)
    {
        setDragBlocked(e.started);
    }

    /* ============ LISTENERS =========== */
    @Override
    public void addListDataListener(ListDataListener l)
    {
        listenersSupport.addListDataListener(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l)
    {
        listenersSupport.removeListDataListener(l);
    }

    @Override
    public void addForceContextChangeListener(IForceContextChangeListener l)
    {
        listenersSupport.addForceContextChangeListener(l);
    }

    @Override
    public void removeForceContextChangeListener(IForceContextChangeListener l)
    {
        listenersSupport.removeForceChangeListener(l);
    }

    /**
     * When ForceContext is about to be unused. To be called by {@link DeviceContext#close()}.
     */
    void close()
    {
        KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(keyEventDispatcher);
    }
}
// revised.
