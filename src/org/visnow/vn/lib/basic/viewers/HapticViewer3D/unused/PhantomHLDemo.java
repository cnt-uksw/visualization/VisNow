/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.unused;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import openhaptics.OH;
import openhaptics.hdapi.Device;
import openhaptics.hdapi.HDException;
import openhaptics.hlapi.Context;
import openhaptics.hlapi.HLException;
import openhaptics.hlapi.effects.Viscous;

/**
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 */
public class PhantomHLDemo
{

    static {
        OH.init();
    }

    public static void main(String[] args)
    {
        try {
            //            int hHD = Device.initDevice(Device.DEFAULT_PHANTOM);
            int hHD = Device.initDevice("DEFAULT_PHANTOM");
            Context context = Context.createContext(hHD);

            /*
             CustomEffect ce = new CustomEffect(context, new CustomEffectCallback() {

             public void computeForce(Vector3d force, Cache cache) {
             force.set(2,0,0);
             }

             public void startForceProc(Cache cache) {
             }

             public void stopForceProc(Cache cache) {
             }
             });
             *
             */
            Viscous ce = new Viscous(context, 1.0, 1.0);

            ce.start();

            System.out.println("Press any thing to stop force");
            try {
                System.in.read();
            } catch (IOException ex) {
                Logger.getLogger(PhantomHLDemo.class.getName()).log(Level.SEVERE, null, ex);
            }

            ce.stop();

            System.out.println("Press any thing to exit");
            try {
                System.in.read();
            } catch (IOException ex) {
                Logger.getLogger(PhantomHLDemo.class.getName()).log(Level.SEVERE, null, ex);
            }

            context.delete();
            Device.disableDevice(hHD);
        } catch (HLException ex) {
            Logger.getLogger(PhantomHLDemo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (HDException ex) {
            Logger.getLogger(PhantomHLDemo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private PhantomHLDemo()
    {
    }
}
