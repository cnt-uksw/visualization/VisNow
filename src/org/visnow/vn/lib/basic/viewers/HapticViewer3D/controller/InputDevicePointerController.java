/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller;

import org.jogamp.java3d.InputDevice;
import org.jogamp.java3d.Sensor;
import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.Tuple3f;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.pointer3d.IViewPointer;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.pointer3d.Pointer3DViewBehavior;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.IPassiveDevice;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.haptics.IHapticReadOnlyDevice;

/**
 * Maintains both visual and physical aspects of a physical input device for use in Java3D
 * library.
 * <p/>
 * It contains: <ul>
 * <li>an object of a view class for displaying a grafical cursor (implementing {@link IViewPointer}
 * interface),</li>
 * <li>an object of a device class for reading and writing into a device - reading position and
 * velocity and setting forces or torque (implementing {@link IPassiveDevice} interface).</li></ul>
 * <p/>
 * One
 * <code>InputDevicePointerController</code> mantains exactly one device, and the device is never
 * changed.
 * <p/>
 * The class must implement methods from Java3D's
 * <code>InputDevice</code> interface, which are
 * responsible for:<ul>
 * <li>device initialization ({@link #initialize() initialize()}),
 * <li>reading data from it ({@link #pollAndProcessInput() pollAndProcessInput()}) and
 * <li>closing the connection ({@link #close() close()}).
 * </ul>
 *
 * Initialization of the device must be run manually:
 * <pre>
 *      inputDevice = new InputDevicePointerController();
 *      if (inputDevice.initialize()) {
 *          u.getViewer().getPhysicalEnvironment().addInputDevice(inputDevice);
 *          ....
 * </pre>
 *
 * On the contrary, {@link #pollAndProcessInput() pollAndProcessInput()} shouldn't be
 * called by user - for a devices with BLOCKING or NON_BLOCKING processing mode (that's the case
 * here) Java3D will call {@link #pollAndProcessInput() pollAndProcessInput()} on its own in regular
 * time intervals.
 *
 *
 * When ending using the device, {@link #close() close()} method should be called manually.
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 *
 * @see
 * <a
 * href="http://docs.oracle.com/cd/E17802_01/j2se/javase/technologies/desktop/java3d/forDevelopers/j3dguide/InputEvents.doc.html">Java
 * 3D API Specification, chapter 10 (Input Devices and Picking)</a>
 *
 * @see
 * <a
 * href="https://svn.java.net/svn/j3d-examples~svn/">SVN with Java3D example</a> (checkout trunk and
 * look at virtual_input_device example)
 */
abstract public class InputDevicePointerController implements InputDevice
{

    private Sensor[] sensors;
    private IPassiveDevice passiveDevice;
    private Pointer3DViewBehavior pointer = null;
    //
    /**
     * For button state. Must be initialized in {@link #initialize()}.
     */
    private int[] buttons;
    /**
     * For transform matrix storing device's position and rotation.
     */
    private Transform3D deviceTransform = new Transform3D();
    //
    //
    int tempCounter = 0;
    long tempLastMilisecondsRead = System.currentTimeMillis();
    static final org.apache.log4j.Logger LOGGER
        = org.apache.log4j.Logger.getLogger(new Throwable().getStackTrace()[0].getClassName());

    /**
     * Constructor - to be used <strong>only by {@link HapticPointerController}, {@link TabletPointerController}</strong> (and probably in
     * future by other similar classes)
     *
     * @param passiveDevice device to be controlled by this object
     */
    public InputDevicePointerController(IPassiveDevice passiveDevice)
    {
        if (passiveDevice == null) {
            throw new IllegalArgumentException("Device cannot be null");
        }
        this.passiveDevice = passiveDevice;
    }

    public void setPointer(Pointer3DViewBehavior pointer)
    {
        if (pointer == null) {
            throw new IllegalArgumentException("3D pointer cannot be null");
        }
        if (this.pointer != null) {
            throw new IllegalArgumentException("Cannot redeclare pointer");
        }

        this.pointer = pointer;

    }

    /**
     * @return Always returns true :)
     */
    @Override
    public boolean initialize()
    {

        this.sensors = new Sensor[getSensorCount()];
        for (int i = 0; i < getSensorCount(); ++i) {
            sensors[i] = new Sensor(this, 3, passiveDevice.getButtonsCount());
        }
        this.buttons = new int[passiveDevice.getButtonsCount()];

        LOGGER.info("initialize(): succeeded");
        return true;
    }

    //TODO MEDIUM: implement this!
    @Override
    public void setNominalPositionAndOrientation()
    {
    }

    //TODO w jakich jednostkach to jest wyrazone??
    /**
     * A callback method called by Java 3D to read the data from a device and
     * write it to a proper {@link Sensor Sensor} object.
     *
     * @see InputDevice#pollAndProcessInput()
     */
    @Override
    public void pollAndProcessInput()
    {

        long tempMilisecondsRead = System.currentTimeMillis();

        passiveDevice.getTransform(deviceTransform);
        passiveDevice.getButtons(buttons);

        sensors[0].setNextSensorRead(tempMilisecondsRead, deviceTransform, buttons);

        // DEBUG - for checking Java3D's polling interval (it could be set manually in command line)
        //        LOGGER.info("time diff: " + (tempMilisecondsRead - tempLastMilisecondsRead));
        //        tempLastMilisecondsRead = tempMilisecondsRead;
        // END OF DEBUG
    }

    @Override
    public void processStreamInput()
    {
        // according to Java3D documentation, this should be empty
    }

    //TODO: close() - what should be here?
    @Override
    public void close()
    {
    }

    @Override
    public int getProcessingMode()
    {
        return InputDevice.NON_BLOCKING;
    }

    // TODO MEDIUM: setProcessingMode(int mode) - what should be here? so far it's empty...
    @Override
    public void setProcessingMode(int mode)
    {
    }

    @Override
    public int getSensorCount()
    {
        return 1;
    }

    /**
     * Returns the only sensor of an input device (calls {@link #getSensor(0)})
     */
    public Sensor getSensor()
    {
        return getSensor(0);
    }

    @Override
    public Sensor getSensor(int sensorIndex)
    {
        if (sensorIndex != 0) {
            throw new ArrayIndexOutOfBoundsException("Only index=0 is valid");
        }

        return sensors[0];
    }

    /**
     * Returns pointer to a (read-only) device. It could be overriden in subclasses to return a
     * more specific type of interface (e.g. {@link IHapticReadOnlyDevice}, ITabletDevice etc.).
     */
    public IPassiveDevice getDevice()
    {
        return passiveDevice;
    }

    public Pointer3DViewBehavior getPointer()
    {
        return pointer;
    }

    /**
     * Get current position
     *
     * @param outPosition Current position
     */
    public void getPosition(Tuple3f outPosition)
    {
        passiveDevice.getPosition(outPosition);
    }
}
