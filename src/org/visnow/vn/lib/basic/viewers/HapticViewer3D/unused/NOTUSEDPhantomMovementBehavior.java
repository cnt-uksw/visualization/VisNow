/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.unused;

import org.jogamp.java3d.utils.behaviors.sensor.SensorEvent;
import org.jogamp.java3d.utils.behaviors.sensor.SensorEventAgent;
import org.jogamp.java3d.utils.behaviors.sensor.SensorInputAdaptor;
import java.util.Enumeration;
import java.util.Iterator;
import org.jogamp.java3d.Behavior;
import org.jogamp.java3d.Sensor;
import org.jogamp.java3d.Transform3D;
import org.jogamp.java3d.TransformGroup;
import org.jogamp.java3d.WakeupCondition;
import org.jogamp.java3d.WakeupCriterion;
import org.jogamp.java3d.WakeupOnElapsedFrames;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.InputDevicePointerController;

/**
 * @deprecated NOT USED!
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 */
public class NOTUSEDPhantomMovementBehavior extends Behavior
{

    private Sensor sensor = null;
    private SensorEventAgent eventAgent = null;
    private TransformGroup transformGroup = null;
    private WakeupCondition conditions = new WakeupOnElapsedFrames(0);
    private InputDevicePointerController phantom;

    public NOTUSEDPhantomMovementBehavior(InputDevicePointerController phantom, TransformGroup tg)
    {
        transformGroup = tg;
        this.phantom = phantom;
        sensor = phantom.getSensor(0);
        eventAgent = new SensorEventAgent(this);
        eventAgent.addSensorButtonListener(sensor, 0, new ButtonSL());
    }

    @Override
    public void initialize()
    {
        wakeupOn(conditions);
    }

    @Override
    public void processStimulus(Iterator<WakeupCriterion> itrtr) {
        eventAgent.dispatchEvents();
        wakeupOn(conditions);
    }

    class ButtonSL extends SensorInputAdaptor
    {

        private boolean dragging = false;
        private Transform3D baseSceneTransform = new Transform3D();
        private Transform3D basePhantomTransform = new Transform3D();
        private Transform3D currentPhantomTransform = new Transform3D();

        @Override
        public void dragged(SensorEvent e)
        {
            phantom.getSensor(0).lastRead(currentPhantomTransform);
            if (!dragging) {
                transformGroup.getTransform(baseSceneTransform);
                basePhantomTransform.set(currentPhantomTransform);
                dragging = true;
            }
            Transform3D phantomDiff = new Transform3D();
            phantomDiff.mulInverse(currentPhantomTransform, basePhantomTransform);
            phantomDiff.mul(baseSceneTransform);
            transformGroup.setTransform(phantomDiff);
        }

        @Override
        public void released(SensorEvent e)
        {
            dragging = false;
        }
    }
}
