/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui;

import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.JList;

/**
 * Helper class for determining which index was clicked in JList.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class JListClickHelper
{

    /**
     * Method returns the actually clicked position. The difference between this and locationToIndex
     * is that the latter returns <b>the closest</b> index to the clicked point and not the actually
     * clicked position. So we must use getCellBounds to check that.
     *
     * Official documentation: "To determine if the cell actually contains the specified location,
     * compare the point against the cell's bounds, as provided by getCellBounds. This method
     * returns -1 if the model is empty".
     *
     * @param p clicked point
     * <p>
     * @return index of a clicked element or -1 if none clicked
     * <p>
     * @see <a
     * href="http://docs.oracle.com/javase/6/docs/api/javax/swing/JList.html#locationToIndex%28java.awt.Point%29">locationToIndex()
     * in Java documentation</a>
     */
    public static int getClickedLocation(JList list, Point p)
    {
        int index = list.locationToIndex(p);
        if (index < 0) { // model is empty
            return -1;
        }

        Rectangle r = list.getCellBounds(index, index);
        if (r.contains(p)) {
            return index;
        } else {
            return -1;
        }
    }
}
