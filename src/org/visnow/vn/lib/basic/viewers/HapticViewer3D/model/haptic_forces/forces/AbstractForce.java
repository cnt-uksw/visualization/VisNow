/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces;

import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.CoordinateSystem;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.ForceContext;

/**
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 */
public abstract class AbstractForce implements IForce
{

    /**
     * Scale used by
     * <code>ForceContext</code> in arc tan formula that reduces force.
     */
    protected float forceScale;
    //
    protected boolean enabled = true;
    //
    /**
     * Reflects coordinate system used when computing force in {@link #getForce}. Must be set in
     * each class separately.
     */
    public final CoordinateSystem forceCoordinateSystem_;

    /**
     * Returns {@link #forceCoordinateSystem_} - coordinate system used when computing force in
     * {@link #getForce}.
     */
    @Override
    public CoordinateSystem getForceCoordinateSystem()
    {
        return forceCoordinateSystem_;
    }

    /**
     * Used by concrete classes.
     */
    protected AbstractForce(CoordinateSystem forceCoordinateSystem)
    {
        this.forceCoordinateSystem_ = forceCoordinateSystem;
        forceScale = 1;
    }

    /**
     * Copy constructor
     */
    protected AbstractForce(AbstractForce af)
    {
        this.forceCoordinateSystem_ = af.forceCoordinateSystem_;
        this.forceScale = af.forceScale;
        this.enabled = af.enabled;
    }

    /**
     * To be used by {@link ForceContext} and by GUI.
     * <p/>
     * @return value of {@link #forceScale}
     */
    @Override
    public float getForceScale()
    {
        return forceScale;
    }

    /**
     * Setter for {@link #forceScale}.
     */
    public void setForceScale(float forceScale)
    {
        this.forceScale = forceScale;
    }

    /**
     * Returns maximum allowed value for this force. It's responsibility of
     * <code>ForceContext</code> to handle it properly - to reduce value of this force below or
     * equal to that limit.
     * <p/>
     * @return {@link #DEFAULT_MAX_FORCE}
     */
    @Override
    public float getMaxAllowedValue()
    {
        return DEFAULT_MAX_FORCE; //TODO MEDIUM: should depend on getDeviceNominalMaxContinuousForce() (device method)
    }

    @Override
    public abstract IForce clone();

    @Override
    public String getName()
    {
        return getClassSimpleName();
    }

    @Override
    public String toString()
    {
        String name = getName();
        if (!enabled)
            name += " (disabled)";
        return name;
    }

    @Override
    public String getClassSimpleName()
    {
        return getClass().getSimpleName();
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        if (!canBeChanged())
            return;

        if (this.enabled != enabled) {
            this.enabled = enabled;
        }
    }

    /**
     * Returns whether the force is enabled. It is responsibility of {@link ForceContext} to omit
     * such forces when computing haptic force.
     */
    @Override
    public boolean isEnabled()
    {
        return enabled;
    }

    @Override
    public boolean canBeChanged()
    {
        return true;
    }
}
