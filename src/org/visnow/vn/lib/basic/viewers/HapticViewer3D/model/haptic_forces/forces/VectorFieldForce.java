/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces;

import org.jogamp.vecmath.Point3f;
import org.jogamp.vecmath.Vector3f;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ILocalToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ITrackerToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.CoordinateSystem;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.HapticLocationData;

/**
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class VectorFieldForce extends AbstractRegularFieldForce
{

    boolean errorLogged = false;
    private final static CoordinateSystem forceCoordinateSystem = CoordinateSystem.LOCAL;

    public VectorFieldForce()
    {
        super(forceCoordinateSystem);
    }

    protected VectorFieldForce(VectorFieldForce aThis)
    {
        super(aThis);
    }

    @Override
    public void getForce(HapticLocationData locationData, Vector3f out_force)
        throws ITrackerToVworldGetter.NoDataException,
        ILocalToVworldGetter.NoDataException
    {

        Point3f position = locationData.getCurrentLocalPosition();

        if (dataChangingOrNull) {
            out_force.set(ZERO_FORCE);
            return;
        }

        //transform coordinates to geom: indexes to data arrays; stored in point, pointDiffArr, indexes, indexesArr
        convertPointToLocalGeom(position);

        //is point outside the field
        if (pointIsOutsideGeom(indexesArr)) {
            out_force.set(ZERO_FORCE);
            return;
        }

        // this is instead of all those lines below
        out_force.set(field.getInterpolatedData(new FloatLargeArray(floatData), indexesArr[0], indexesArr[1], indexesArr[2]));

        //
        //        //point is inside the field, interpolate output force
        //        switch (data.getType()) {
        //            case FIELD_DATA_FLOAT:
        //                out_force.set(field.getInterpolatedData(data.getFData().getData(), indexesArr[0], indexesArr[1], indexesArr[2]));
        //                break;
        //            case FIELD_DATA_DOUBLE:
        //                double[] ddata = field.getInterpolatedData(data.getDData().getData(), indexesArr[0], indexesArr[1], indexesArr[2]);
        //                out_force.set((float) ddata[0], (float) ddata[1], (float) ddata[2]);
        //                break;
        //            case FIELD_DATA_BYTE:
        //                byte[] bdata = field.getInterpolatedData(data.getBData().getData(), indexesArr[0], indexesArr[1], indexesArr[2]);
        //                out_force.set((float) bdata[0], (float) bdata[1], (float) bdata[2]);
        //                break;
        //            case FIELD_DATA_INT:
        //                int[] idata = field.getInterpolatedData(data.getIData().getData(), indexesArr[0], indexesArr[1], indexesArr[2]);
        //                out_force.set((float) idata[0], (float) idata[1], (float) idata[2]);
        //                break;
        //            case FIELD_DATA_SHORT:
        //                short[] sdata = field.getInterpolatedData(data.getSData().getData(), indexesArr[0], indexesArr[1], indexesArr[2]);
        //                out_force.set((float) sdata[0], (float) sdata[1], (float) sdata[2]);
        //                break;
        //            default:
        //                if (!errorLogged) {
        //                    LOGGER.error(getName() + ": Unhandled data type: " + data.getType());
        //                    errorLogged = true;
        //                }
        //                out_force.set(ZERO_FORCE);
        //        }
    }

    @Override
    public synchronized void setData(
        int dataComponentIndex, RegularField field, DataArray data,
        float forceScale)
    {
        if (data.getVectorLength() != 3) {
            throw new IllegalArgumentException("Data must contain 3D vectors");
        }
        super.setData(dataComponentIndex, field, data, forceScale);
    }

    @Override
    public IForce clone()
    {
        return new VectorFieldForce(this);
    }

    @Override
    public String getClassSimpleName()
    {
        return "Vector field";
    }
}
// revised.
