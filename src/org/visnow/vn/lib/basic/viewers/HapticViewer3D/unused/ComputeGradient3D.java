/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.unused;

/**
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 */
public class ComputeGradient3D implements Runnable
{

    private float[] outData;
    private float[] inData;
    private int[] dims;
    int nThreads;

    public ComputeGradient3D(int[] dims, float[] inData, int nThreads)
    {
        this.dims = dims;
        this.inData = inData;
        this.nThreads = nThreads;
        outData = new float[dims.length * inData.length];
    }

    public void run()
    {
        Thread[] threads = new Thread[nThreads];
        for (int i = 0; i < threads.length; ++i) {
            threads[i] = new Thread(new Compute3D(i));
            threads[i].start();
        }
        for (int i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
            } catch (Exception e) {
            }
        }
    }

    public float[] getOutData()
    {
        return outData;
    }

    private class Compute3D implements Runnable
    {

        int iThread;

        public Compute3D(int iThread)
        {
            this.iThread = iThread;
        }

        public void run()
        {
            int m = dims[0] * dims[1];
            for (int i = iThread; i < dims[2]; i += nThreads) {
                for (int j = 0, n = i * m; j < dims[1]; j++) {
                    for (int k = 0; k < dims[0]; k++, n++) {
                        if (k == 0) {
                            outData[3 * n] = inData[n + 1] - inData[n];
                        } else if (k == dims[0] - 1) {
                            outData[3 * n] = inData[n] - inData[n - 1];
                        } else {
                            outData[3 * n] = .5f * (inData[n + 1] - inData[n - 1]);
                        }
                        if (j == 0) {
                            outData[3 * n + 1] = inData[n + dims[0]] - inData[n];
                        } else if (j == dims[1] - 1) {
                            outData[3 * n + 1] = inData[n] - inData[n - dims[0]];
                        } else {
                            outData[3 * n + 1] = .5f * (inData[n + dims[0]] - inData[n - dims[0]]);
                        }
                        if (i == 0) {
                            outData[3 * n + 2] = inData[n + m] - inData[n];
                        } else if (i == dims[2] - 1) {
                            outData[3 * n + 2] = inData[n] - inData[n - m];
                        } else {
                            outData[3 * n + 2] = .5f * (inData[n + m] - inData[n - m]);
                        }
                        /*
                         outNorm[n] = (float) sqrt(outData[3 * n] * outData[3 * n] +
                         outData[3 * n + 1] * outData[3 * n + 1] +
                         outData[3 * n + 2] * outData[3 * n + 2]);
                         */
                    }
                }
            }
        }
    }
}
