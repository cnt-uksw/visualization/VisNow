/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces;

import org.jogamp.vecmath.Vector3f;

/**
 * Returns read-only force parameters: force vector, general force clamp and flags: force enabled,
 * force blocked on drag, force blocked because of lack of data.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public interface IForceGetter
{

    public float getForceClamp();

    public float getForceScale();

    /**
     * Returns a recently computed force.
     * Even if force output is disabled (not to be mixed with disabling each of forces!), it will
     * return non-zero value.
     *
     * @param out_force (output parameter) resultant force - computed using forces there were
     *                  enabled.
     */
    public void getLastComputedForce(Vector3f out_force);

    /**
     * Return true if the value of a force was too big and needed to be clamped.
     */
    public boolean wasLastForceClamped();

    /**
     * Returns whether force output of the ForceContext, connected to the device context in
     * IForceGetter, is enabled.
     *
     * @return whether forces will be generated
     */
    public boolean isForceOutputEnabled();

    /**
     * Returns whether the force context connected to the device context in IForceGetter has blocked
     * force output because of inability to compute it (probably there is no link to haptic data in
     * the net of VisNow modules).
     *
     * @return true if blocked because of lack of data, false otherwise
     */
    public boolean isForceOutputDataBlocked();

    /**
     * Returns whether the force context connected to the device context in IForceGetter has blocked
     * force output because of scene being dragged by a haptic device.
     *
     * @return true if blocked because of dragging, false otherwise
     */
    public boolean isForceOutputDragBlocked();
}
