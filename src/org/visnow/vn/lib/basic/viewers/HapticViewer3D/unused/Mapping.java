/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.unused;

import java.util.HashMap;

/**
 * @deprecated NOT USED!
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 */
public class Mapping
{

    private static Mapping instance;
    private HashMap<String, MappingFunction> functions = new HashMap<String, MappingFunction>();

    private Mapping()
    {
        functions.put("Rotate", new MappingFunction("Rotate"));
        functions.put("Translate", new MappingFunction("Translate"));
        functions.put("Scale", new MappingFunction("Scale"));
        functions.put("Pick", new MappingFunction("Pick"));
    }

    public static Mapping getInstance()
    {
        if (instance == null)
            instance = new Mapping();
        return instance;
    }

    public String[] getFunctionNames()
    {
        return (String[]) functions.keySet().toArray();
    }

    public synchronized boolean lock(String functionName, Object caller)
    {
        MappingFunction fun = functions.get(functionName);
        if (fun == null)
            throw new RuntimeException("No such function: " + functionName);
        return fun.lock(caller);
    }

    public synchronized void unlock(String functionName, Object caller)
    {
        MappingFunction fun = functions.get(functionName);
        if (fun == null)
            throw new RuntimeException("No such function: " + functionName);
        fun.unlock(caller);
    }

    protected class MappingFunction
    {

        private Object lockedOn = null;
        private String name;

        MappingFunction(String name)
        {
            this.name = name;
        }

        /**
         * Get name of function
         *
         * @return name of the function
         */
        public String getName()
        {
            return name;
        }

        /**
         * Lock function on specific device
         *
         * Lock function on specific device, such that other can't access it.
         * Got some error checking to force proper lock and unlock calls.
         * Otherwise it could be as simply as lock=true.
         *
         * @param caller who wants to get a lock, you must later call unlock with the same argument
         * <p>
         * @return true, if lock was assigned to caller; false, if it is locked on another object
         */
        public boolean lock(Object caller)
        {
            if (caller == null) {
                throw new NullPointerException("Argument cannot be null");
            }
            if (lockedOn == caller) {
                throw new RuntimeException("You [" + caller + "] already got lock");
            }

            if (lockedOn != null) {
                return false;
            }

            lockedOn = caller;
            return true;
        }

        public void unlock(Object caller)
        {
            if (caller == null) {
                throw new NullPointerException("Argument cannot be null");
            }
            if (lockedOn != caller) {
                throw new RuntimeException("You don't own this lock");
            }

            lockedOn = null;
        }
    }
}
