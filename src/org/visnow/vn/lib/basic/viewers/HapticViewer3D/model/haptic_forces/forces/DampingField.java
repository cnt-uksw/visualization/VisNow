/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces;

import org.jogamp.vecmath.Point3f;
import org.jogamp.vecmath.Vector3f;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ILocalToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ITrackerToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.CoordinateSystem;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.HapticLocationData;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.FloatLargeArray;

/**
 * Represents a damping force reflecting values of a scalar field. Formula for the force is:
 * <pre>F = k * v</pre>
 * <p/>
 * where: <br/>
 * k - damping constant depending on value of a field in given point<br/>
 * v - current velocity
 * <p/>
 * Simulating fields like ct.vnf (a foot) result in buzzing when touching regions with high damping
 * (bones).<br/>
 * Some ideas how to avoid that:<br/>
 * <ol><li>extend AVERAGE_FORCE_50 to take an average from 10 or 50 forces instead of 2</li>
 * <li>extend AVERAGE_FORCE_50 to take a weighted average from 10 or 50 forces instead of 2 with
 * current force with the biggest weight and previous forces with declining weight (the "older"
 * force, the smaller weight)</li>
 * <li>"anticipate position" - a method from Łukasz Czerwiński's master thesis - instead of taking a
 * position from this moment, take a position that will be reached in 2-10 miliseconds. When
 * simulating stiff walls using a Novint Falcon haptic device this removed buzzing when touching the
 * wall.</li></ol>
 * <p/>
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 * <p/>
 */
public class DampingField extends AbstractRegularFieldForce
{

    public static final float MAX_VELOCITY_DERIVATE = 4;

    public enum Algorithm
    {

        DIRECT,
        AVERAGE_FORCE_50,
        AVERAGE_FORCE_50_WFUSE,
        AVERAGE_FORCE_50_MEMORY,
    }

    private final static CoordinateSystem forceCoordinateSystem = CoordinateSystem.TRACKER;
    protected Point3f lastVelocity = new Point3f();
    protected Point3f lastForce = new Point3f();
    protected Algorithm algorithm = Algorithm.AVERAGE_FORCE_50_MEMORY;

    public DampingField()
    {
        super(forceCoordinateSystem);
    }

    private DampingField(DampingField aThis)
    {
        super(aThis);
        this.algorithm = aThis.algorithm;
    }

    static int debugCounter = 0;

    @Override
    public void getForce(HapticLocationData locationData, Vector3f out_force)
        throws ITrackerToVworldGetter.NoDataException,
        ILocalToVworldGetter.NoDataException
    {

        Point3f position = locationData.getCurrentLocalPosition();

        if (dataChangingOrNull) {
            out_force.set(ZERO_FORCE);
            return;
        }
        //        updateGeomToVworld();

        //transform coordinates to geom: indexes to data arrays
        convertPointToLocalGeom(position);

        //is point outside the field
        if (pointIsOutsideGeom(indexesArr)) {
            out_force.set(ZERO_FORCE);
            return;
        }

        //point is inside the field, interpolate damping constant
        float damping = field.getInterpolatedData(new FloatLargeArray(floatData), indexesArr[0], indexesArr[1], indexesArr[2])[0];
        /* Compute force:
         *   F = -k * v
         *      k - damping constant depending on value of a field in given point
         *      v - current velocity
         */
        damping = -abs(damping);
        Vector3f velocity = locationData.getCurrentTrackerVelocity();
        out_force.scale(damping, velocity);

        switch (algorithm) {
            case AVERAGE_FORCE_50_MEMORY:
                //tlumienie z usrednianiem sił
                out_force.x = (damping * velocity.x + lastForce.x) / 2;
                out_force.y = (damping * velocity.y + lastForce.y) / 2;
                out_force.z = (damping * velocity.z + lastForce.z) / 2;

                //ograniczenie sil
                if (abs(velocity.x - lastVelocity.x) > MAX_VELOCITY_DERIVATE) {
                    out_force.x = (lastForce.x * 3 + out_force.x) * 0.25f;
                }
                if (abs(velocity.y - lastVelocity.y) > MAX_VELOCITY_DERIVATE) {
                    out_force.y = (lastForce.y * 3 + out_force.y) * 0.25f;
                }
                if (abs(velocity.z - lastVelocity.z) > MAX_VELOCITY_DERIVATE) {
                    out_force.z = (lastForce.z * 3 + out_force.z) * 0.25f;
                }

                lastVelocity.set(velocity);
                lastForce.set(out_force);
                break;

            case AVERAGE_FORCE_50:
                out_force.scale(damping, velocity);
                out_force.add(lastForce);
                out_force.scale(0.5f);

                lastForce.scale(damping, velocity);
                break;

            case AVERAGE_FORCE_50_WFUSE:
                out_force.scale(damping, velocity);
                out_force.add(lastForce);
                out_force.scale(0.5f);

                LOGGER.info("Force: " + abs(out_force.x) + ";" +
                    abs(out_force.y) + ";" +
                    abs(out_force.z) + ";");
                //TODO MEDIUM: probably there is a typo below: three times "velocity.z".
                // force limits (by Krzysztof Madejski)
                if (abs(out_force.x) > 8) {
                    out_force.x = damping * signum(velocity.z);
                }
                if (abs(out_force.y) > 8) {
                    out_force.y = damping * signum(velocity.z);
                }
                if (abs(out_force.z) > 8) {
                    out_force.z = damping * signum(velocity.z);
                }

                /*
                 * TODO: Jakoś działa, choć jak szybko machnę sam, to się włącza
                 * i leciutko samo leci
                 if (abs(velocity[0] - lastVelocity[0]) > MAX_VELOCITY_DERIVATE) {
                 outForce[0] = damping * signum(velocity[0]);
                 }
                 if (abs(velocity[1] - lastVelocity[1]) > MAX_VELOCITY_DERIVATE) {
                 outForce[1] = damping * signum(velocity[1]);
                 }
                 if (abs(velocity[2] - lastVelocity[2]) > MAX_VELOCITY_DERIVATE) {
                 outForce[2] = damping * signum(velocity[2]);
                 }
                 */
                lastVelocity.set(velocity);
                lastForce.scale(damping, velocity);
                break;

            case DIRECT:
                out_force.scale(damping, velocity);
                break;
        }

        ++debugCounter;
        if (debugCounter % 100 == 0) {
            LOGGER.info("\n" +
                "local position: " + position + "\n" +
                "indexes: (" + indexesArr[0] + ", " + indexesArr[1] + ", " + indexesArr[2] + ")\n" +
                "damping: " + damping + "\n" +
                "force: " + out_force + " (length: " + out_force.length() + ")");
        }

        //        out_force.set(1, 0, 0);
        //        out_force.scale(damping);
    }

    public Algorithm getAlgorithm()
    {
        return algorithm;
    }

    public synchronized void setData(Algorithm algorithm,
                                     int dataComponentIndex, RegularField field, DataArray data,
                                     float forceScale)
    {
        if (data.getVectorLength() != 1) {
            throw new IllegalArgumentException("Data must containt scalar values");
        }
        this.algorithm = algorithm;
        resetLastFields();

        super.setData(dataComponentIndex, field, data, forceScale);
    }

    private void resetLastFields()
    {
        lastVelocity.scale(0);
        lastForce.scale(0);
    }

    @Override
    public IForce clone()
    {
        return new DampingField(this);
    }

    @Override
    public String getClassSimpleName()
    {
        return "Damping field";
    }
}
