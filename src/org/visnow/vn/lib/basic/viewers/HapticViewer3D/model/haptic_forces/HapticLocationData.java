/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces;

import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.Point3f;
import org.jogamp.vecmath.Vector3f;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ILocalToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ITrackerToVworldGetter;

/**
 * A container for position and velocity in different coordinate systems and for matrixes
 * transforming between them. Data are computed lazy - on first use.
 * <p/>
 * Use this object in the following way:
 * <ol>
 * <li>update values of
 * <code>trackerToVworld</code>,
 * <code>localToVworld</code>,
 * <code>currentTrackerPosition</code> and
 * <code>currentTrackerVelocity</code> (or some of them)
 * <li>call {@link #notifyNewDataSet() } to clear up previously lazy-computed values
 * <li>call to {@link #getVworldToLocal()}, {@link #getVworldToTracker()} or any other
 * <code>getCurrent...()</code> to get a reference to a computed values.
 * </ol>
 * <p/>
 * NOTE: use values from the functions as <b>read only</b>!
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 * <p/>
 */
public class HapticLocationData
{

    ILocalToVworldGetter localToVworldGetter;
    ITrackerToVworldGetter trackerToVworldGetter;
    //
    private Transform3D trackerToVworld = new Transform3D();
    boolean trackerToVworldFetched = false;
    //  
    private Transform3D vworldToTracker = new Transform3D();
    boolean vworldToTrackerComputed = false;
    //
    private Transform3D localToVworld = new Transform3D();
    boolean localToVworldFetched = false;
    //
    private Transform3D vworldToLocal = new Transform3D();
    boolean vworldToLocalComputed = false;
    //
    //
    /**
     * Current position in tracker (device) coordinate system - to be set by external class
     * (ForceContext). After setting parameters call {@link #notifyNewDataSet()}.
     */
    public Point3f currentTrackerPosition = new Point3f();
    //
    /**
     * Current velocity in tracker (device) coordinate system - to be set by external class
     * (ForceContext). After setting parameters call {@link #notifyNewDataSet()}.
     */
    public Vector3f currentTrackerVelocity = new Vector3f();
    //
    //
    /**
     * Current position in Java3D's vworld coordinate system. Transforming coordinates system is
     * the only case where forces are dependant on Java3D.
     */
    private Point3f currentVworldPosition = new Point3f();
    boolean currentVworldPositionComputed = false;
    //
    /**
     * Current velocity in Java3D's vworld coordinate system. Transforming coordinates system is
     * the only case where forces are dependant on Java3D.
     */
    private Vector3f currentVworldVelocity = new Vector3f();
    boolean currentVworldVelocityComputed = false;
    //
    //
    /**
     * Current position in local coordinate system of a node containing haptic outline.
     * Transforming coordinates system is the only case where forces are dependant on Java3D.
     */
    private Point3f currentLocalPosition = new Point3f();
    boolean currentLocalPositionComputed = false;
    //
    /**
     * Current velocity in local coordinate system of a node containing haptic outline.
     * Transforming coordinates system is the only case where forces are dependant on Java3D.
     */
    private Vector3f currentLocalVelocity = new Vector3f();
    boolean currentLocalVelocityComputed = false;

    // ===== Getting transforms from getters =========
    public Transform3D getTrackerToVworld() throws ITrackerToVworldGetter.NoDataException
    {
        if (!trackerToVworldFetched) {
            if (trackerToVworldGetter == null)
                throw new ITrackerToVworldGetter.NoDataException();
            trackerToVworldGetter.getTrackerToVworld(trackerToVworld);
        }
        return trackerToVworld;
    }

    //
    public Transform3D getLocalToVworld() throws ILocalToVworldGetter.NoDataException
    {
        if (!localToVworldFetched) {
            if (localToVworldGetter == null)
                throw new ILocalToVworldGetter.NoDataException();
            localToVworldGetter.getLocalToVworld(localToVworld);
        }
        return localToVworld;
    }

    // ======= Computing inverted transforms ======
    public Transform3D getVworldToTracker() throws ITrackerToVworldGetter.NoDataException
    {
        if (!vworldToTrackerComputed)
            vworldToTracker.invert(getTrackerToVworld());
        return vworldToTracker;
    }

    public Transform3D getVworldToLocal() throws ILocalToVworldGetter.NoDataException
    {
        if (!vworldToLocalComputed)
            vworldToLocal.invert(getLocalToVworld());
        return vworldToLocal;
    }

    // ======= Computing position and velocity =========
    /**
     * Returns current position in tracker (device) coordinate system. It's a simple getter - it
     * does not need any computations.
     */
    public Point3f getCurrentTrackerPosition()
    {
        return currentTrackerPosition;
    }

    /**
     * Returns current velocity in tracker (device) coordinate system. It's a simple getter - it
     * does not need any computations.
     */
    public Vector3f getCurrentTrackerVelocity()
    {
        return currentTrackerVelocity;
    }

    /**
     * Returns current position in vworld (Java3D) coordinate system. It may make a transformation
     * between coordinate systems.
     */
    public Point3f getCurrentVworldPosition() throws ITrackerToVworldGetter.NoDataException
    {
        if (!currentVworldPositionComputed)
            getTrackerToVworld().transform(currentTrackerPosition, currentVworldPosition);
        return currentVworldPosition;
    }

    /**
     * Returns current velocity in vworld (Java3D) coordinate system. It may make a transformation
     * between coordinate systems.
     */
    public Vector3f getCurrentVworldVelocity() throws ITrackerToVworldGetter.NoDataException
    {
        if (!currentVworldVelocityComputed)
            getTrackerToVworld().transform(currentTrackerVelocity, currentVworldVelocity);
        return currentVworldVelocity;
    }

    /**
     * Returns current position in local (Java3D, local to the haptic outline object) coordinate
     * system. It may make a transformation between coordinate systems.
     */
    public Point3f getCurrentLocalPosition()
        throws ITrackerToVworldGetter.NoDataException,
        ILocalToVworldGetter.NoDataException
    {
        if (!currentLocalPositionComputed) {
            getVworldToLocal().transform(getCurrentVworldPosition(), currentLocalPosition);
        }
        return currentLocalPosition;
    }

    /**
     * Returns current position in local (Java3D, local to the haptic outline object) coordinate
     * system. It may make a transformation between coordinate systems.
     */
    public Vector3f getCurrentLocalVelocity()
        throws ITrackerToVworldGetter.NoDataException,
        ILocalToVworldGetter.NoDataException
    {
        if (!currentLocalVelocityComputed) {
            getVworldToLocal().transform(getCurrentVworldVelocity(), currentLocalVelocity);
        }
        return currentLocalVelocity;
    }

    /**
     * Must be called after setting new values for
     * <code>trackerToVworld</code>,
     * <code>localToVworld</code>,
     * <code>currentTrackerPosition</code> and
     * <code>currentTrackerVelocity</code> (just before calling to first
     * <code>get...()</code> method) to clean up cache.
     */
    public void notifyNewDataSet()
    {
        localToVworldFetched = false;
        trackerToVworldFetched = false;

        vworldToTrackerComputed = false;
        vworldToLocalComputed = false;

        currentVworldPositionComputed = false;
        currentVworldVelocityComputed = false;

        currentLocalPositionComputed = false;
        currentLocalVelocityComputed = false;

    }

    void setLocalToVworldGetter(ILocalToVworldGetter localToVworldGetter)
    {
        this.localToVworldGetter = localToVworldGetter;
    }

    void setTrackerToVworldGetter(ITrackerToVworldGetter trackerToVworldGetter)
    {
        this.trackerToVworldGetter = trackerToVworldGetter;
    }
}
