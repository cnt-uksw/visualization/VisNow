/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces;

import java.util.*;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.haptics.IHapticListener;

/**
 *
 * Helper class for managing haptic listeners (
 * <code>IHapticListener</code>).
 * <p/>
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 */
public class HapticListenerSupport
{

    private final HashSet<IHapticListener> listeners = new HashSet<IHapticListener>();

    public HapticListenerSupport()
    {
    }

    /**
     * Adds a new IHapticListener to the notify list of any events that are fired
     *
     * @param listener The IHapticListener to add
     */
    public void addHapticListener(IHapticListener listener)
    {
        synchronized (listeners) {
            listeners.add(listener);
        }
    }

    /**
     * Removes a IHapticListener from the notify list for any events fired from then on
     *
     * @param listener The IHapticListener to remove
     */
    public void removeHapticListener(IHapticListener listener)
    {
        synchronized (listeners) {
            listeners.remove(listener);
        }
    }

    /**
     * Returns the list of IHapticListener objects to notify as an Array of IHapticListener objects
     *
     * @return An array of IHapticListener objects
     */
    public IHapticListener[] getHapticListeners()
    {
        synchronized (listeners) {
            return this.listeners.toArray(new IHapticListener[listeners.size()]);
        }
    }

    /**
     * Returns the number of IHapticListener objects on the notify list
     *
     * @return A integer value for the number of IHapticListener objects
     */
    public int getNumOfListeners()
    {
        synchronized (listeners) {
            return listeners.size();
        }
    }

    /**
     * Fires a Device callback event to all IHapticListener objects that are set to be notified
     */
    public void fireDeviceCallback()
    {
        synchronized (listeners) {
            for (IHapticListener l : listeners) {
                l.callback();
            }
        }
    }
}
