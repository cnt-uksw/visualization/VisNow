/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces;

import org.jogamp.vecmath.Vector3f;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ILocalToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ITrackerToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.CoordinateSystem;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.ForceContext;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.HapticLocationData;

/**
 * Implemented by all forces - damping, damping field force, vector field force, gravity and spring.
 * <p/>
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 */
public interface IForce
{

    public final static float[] ZERO_FORCE = new float[3];
    public final static int DEFAULT_MAX_FORCE = 6;

    /**
     * Returns the coordinate system used in which the force computed in {@link #getForce} is
     * expressed. This is used by {@link ForceContext} to convert force between coordinate systems
     * between aplying to the haptic device.
     */
    public CoordinateSystem getForceCoordinateSystem();

    /**
     * Computes force to be generated and stores it in
     * <code>outForce</code>. {@link #getForceCoordinateSystem} must reflect the coordinate system
     * of the force.
     * <p/>
     * @param locationData Position and velocity getter
     * @param out_force    output: vector with a computed force
     */
    public void getForce(HapticLocationData locationData, Vector3f out_force)
        throws ITrackerToVworldGetter.NoDataException,
        ILocalToVworldGetter.NoDataException;

    /**
     * Returns maximum allowed value for this force. It's responsibility of
     * <code>ForceContext</code> to handle it properly - to reduce value of this force below or
     * equal to that limit.
     * <p/>
     * @return max allowed value of the force or <code>Float.POSITIVE_INFINITY</code> if no force
     *         scaling should be performed
     */
    public float getMaxAllowedValue();

    public float getForceScale();

    /**
     * Returns a string to be displayed as a force description.
     * This method is implemented in AbstractForce as a call to {@link #getClassSimpleName}, but
     * should be overriden and print the name of force type and values of some basic parameters
     * (e.g. spring constant).
     * <p/>
     */
    public String getName();

    /**
     * Returns a friendly name for the class (without parameters).
     * By default it returns name of the class.
     */
    public String getClassSimpleName();

    /**
     * In AbstractForce: returns name from getName() and if disabled, append "(disabled)" text.
     * Rather do not override toString - override getName().
     * <p/>
     * @return friendly name for the force
     */
    @Override
    public String toString();

    public IForce clone();

    public void setEnabled(boolean enabled);

    public boolean isEnabled();

    /**
     * Whether the force can be change (by user), e.g. edited, disabled or removed. True for all
     * forces except for safety damping
     */
    public boolean canBeChanged();
}
