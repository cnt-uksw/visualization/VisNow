/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces;

import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.Matrix4f;
import org.jogamp.vecmath.Point3f;
import org.jogamp.vecmath.Tuple3f;
import org.jogamp.vecmath.Vector3f;
import org.apache.log4j.Logger;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.CoordinateSystem;

/**
 * Base class for all classes handling forces depending on regular fields. It maintains transforminValueg
 coordinate systems and value interpolation.
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 */
public abstract class AbstractRegularFieldForce extends AbstractForce
{

    protected static final Logger LOGGER
        = Logger.getLogger(new Throwable().getStackTrace()[0].getClassName());
    protected RegularField field;
    protected DataArray data;
    /**
     * Float data is needed to get float values in {@link #getForce} even when field is other than
     * float.
     * TODO: Note that this way (suggested by Babor) results in having a COPY of all values of the
     * field in EACH force object!! It could be EXTREMELY MEMORY CONSUMING!
     */
    protected float[] floatData;
    /**
     * Stores a temporary point. Declared as a field to avoid allocating it 1000 times a second.
     */
    protected Vector3f pointVworld = new Vector3f();
    /**
     * Stores a temporary point. Declared as a field to avoid allocating it 1000 times a second.
     */
    //    protected Point3f pointLocal = new Point3f();
    /**
     * Stores a temporary point. Declared as a field to avoid allocating it 1000 times a second.
     */
    protected Point3f pointDiff = new Point3f();
    /**
     * Stores a temporary object. Declared as a field to avoid allocating it 1000 times a second.
     */
    protected float[] pointDiffArr = new float[3];
    protected Point3f origin;
    /**
     * Stores <b>float coordinates</b> of data indexes. Not to be confused with coordinates in
     * vworld or local coordinates -
     * <code>point</code> stores numbers that if they were integers, they would be indexes in array
     * of data. They are not integers, so the value will be interpolated.
     */
    protected Point3f indexes = new Point3f();
    protected float[] indexesArr = new float[3];
    protected int[] dims;
    protected int dataComponentIndex;
    protected boolean dataChangingOrNull = true;
    /**
     * Transforms vworld coordinates to coordinates local to rootObject from Display3DPanel.
     */
    protected Transform3D vworldToLocal = new Transform3D();
    // private Transform3D vworldToGeomProxy;
    protected Transform3D localToVworld = new Transform3D();
    protected Transform3D sensorToVworld = new Transform3D();
    protected Matrix4f mVWorldToIndexes;

    protected AbstractRegularFieldForce(CoordinateSystem forceCoordinateSystem)
    {
        super(forceCoordinateSystem);
    }

    protected AbstractRegularFieldForce(AbstractRegularFieldForce aThis)
    {
        this(aThis.dataComponentIndex,
             aThis.field,
             aThis.data,
             aThis.forceScale,
             aThis.forceCoordinateSystem_);
    }

    private AbstractRegularFieldForce(int dataComponentIndex, RegularField field,
                                      DataArray data, float forceScale,
                                      CoordinateSystem forceCoordinateSystem)
    {
        super(forceCoordinateSystem);

        _setData(dataComponentIndex, field, data, forceScale);
        dataChangingOrNull = false;
    }

    public int getDataComponentIndex()
    {
        return dataComponentIndex;
    }

    protected void setData(int dataComponentIndex,
                           RegularField field, DataArray data,
                           float forceScale)
    {
        _setData(dataComponentIndex, field, data, forceScale);
    }

    //TODO: trochę mylące jest podawanie i dataComponentIndex i data (moze prowadzic do bledow)
    private synchronized void _setData(int dataComponentIndex,
                                       RegularField field, DataArray data,
                                       float forceScale)
    {
        // set synchronization as active and wait 10 ms
        // this was done not to make the thread computing forces to hang on a semaphore - 
        // when dataChangingOrNull is set to true, it will return force = 0)
        //
        // A better solution would be to wait here for the thread to finish computations, because
        // there is NO GUARANTEE that 10 ms will suffice - it's only very probable as force should 
        // be computed 1000 times a second, so it should not take more than 1 ms
        this.dataChangingOrNull = true;
        try {
            wait(10);
        } catch (Exception ex) {
        }

        this.dataComponentIndex = dataComponentIndex;
        this.field = field;
        // prepare data for transforming coordinates of a point to indexes of a data array (used in convertPointToLocalGeom())
        if (field != null) {
            float[][] affine = field.getAffine();
            this.origin = new Point3f(affine[3]);
            this.dims = field.getDims();
        }

        //        if (localToVWorldGetter != null)
        //            localToVWorldGetter.onHapticForceFieldCreated();
        this.data = data;
        if (data != null)
            this.floatData = data.getRawFloatArray().getData();

        //        setGeomScaling();
        //
        //        if (data != null) {
        //            // normalize forces
        //            float preferredMinValue = data.getMin(); // for vector it will be 0
        //            float preferredMaxValue = data.getPreferredMaxValue(); // for vector it will be length of the longest vector
        //            float maxForce = preferredMaxValue(abs(preferredMinValue), abs(preferredMaxValue));
        //            float scale = DEFAULT_MAX_FORCE / maxForce;
        //            setForceScale(scale);
        //        }
        setForceScale(forceScale);

        if (field != null) {
            // end synchronizing data
            if (dataComponentIndex >= 0 && dataComponentIndex < field.getNComponents()) {
                this.dataChangingOrNull = false;
            }
        } else
            this.dataChangingOrNull = false;
    }
    
    
    /**
     * Multipies a 3x3 matrix by vector of length 3
     * <p/>
     * NOT TESTED YET. Please test before using and remove this comment.
     * <p/>
     * @param in_matrix  float[3][3] matrix
     * @param in_vector  float[3] vector
     * @param out_vector = matrx * vector - out param
     */
    private static void multMatrix33ByVector3(float[][] in_matrix, float[] in_vector, float[] out_vector)
    {
        for (int j = 0; j < 3; ++j) {
            out_vector[j] = 0;
            for (int i = 0; i < 3; ++i) {
                out_vector[j] += in_matrix[i][j] * in_vector[i];
            }
        }
    }

    /**
     * Converts local position to indexes (float values!). Those float indexes should be used to
     * interpolate value of the field.
     * <p/>
     * Changes values of variables:
     * <code>pointDiff</code> and
     * <code>pointDiffArr</code>,
     * <code>indexes</code> and
     * <code>indexesArr</code>
     * <p/>
     * @param position position in local coordinate system
     */
    protected void convertPointToLocalGeom(Point3f position)
    {

        //calculate indexes: indexes = invAffine * (point - origin) - see doc for RegularField.invAffine
        pointDiff.sub(position, origin);
        pointDiff.get(pointDiffArr);
        float[][] invAffine = field.getInvAffine();
        multMatrix33ByVector3(invAffine, pointDiffArr, indexesArr);

        // now indexesArr contain (float) indexes
    }

    /**
     * Checks whether
     * <code>point</code> is inside the field or not.
     * <p/>
     * @param point interpolated (float) indexes
     */
    protected boolean pointIsOutsideGeom(Tuple3f point)
    {
        return point.x < 0 || point.y < 0 || point.z < 0 || point.x >= dims[0] || point.y >= dims[1] || point.z >= dims[2];
    }

    /**
     * Checks whether
     * <code>point</code> is inside the field or not.
     * <p/>
     * @param point interpolated (float) indexes
     */
    protected boolean pointIsOutsideGeom(float[] point)
    {
        return point[0] < 0 || point[1] < 0 || point[2] < 0 || point[0] >= dims[0] || point[1] >= dims[1] || point[2] >= dims[2];
    }

    @Override
    public String getName()
    {
        return getClassSimpleName() + "." + data.getName() + " [scale = " + forceScale + "]";
    }
}
//revised.
