/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller;

import org.jogamp.vecmath.Tuple3f;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ILocalToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.pointer3d.Pointer3DViewBehavior;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.InputFields;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.DeviceContext;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.haptics.HapticException;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.IForceGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.haptics.IHapticDevice;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.haptics.IHapticReadOnlyDevice;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.IForce;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.IForceListModel;

/**
 * The class is responsible for handling an haptic device. It
 * notifies the device when it's been selected or deselected.
 * Also it takes care of handling forces (adding a list of forces - damping,
 * gravity etc. to a ForceContext of a current device).
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 */
public class HapticPointerController
    extends InputDevicePointerController
{

    /**
     * Reference to components from data input that could be used to generate haptic forces.
     */
    private InputFields fields;
    /**
     * HapticPointerController owns this object.
     */
    private DeviceContext deviceContext;
    //
    private IHapticDevice hapticDevice; // added by Łukasz Czerwiński (milimetr)
    //

    public HapticPointerController(IHapticDevice hapticDevice,
                                   final InputFields fields,
                                   ILocalToVworldGetter localToVworldGetter) throws HapticException
    {
        super(hapticDevice);

        this.fields = fields;
        this.hapticDevice = hapticDevice;
        this.deviceContext = new DeviceContext(hapticDevice, localToVworldGetter);
    }

    /**
     * Sets pointer. Should be called once, just after constructing
     * <code>HapticPointerController</code> and {@link Pointer3DViewBehavior}.
     */
    @Override
    public void setPointer(Pointer3DViewBehavior pointer)
    {
        super.setPointer(pointer);
        this.deviceContext.setTrackerToVworld(pointer);
        pointer.addDragListener(this.deviceContext.getForceContext());
    }

    public InputFields getParams()
    {
        return fields;
    }

    public void addForce(IForce force)
    {
        deviceContext.getForceContext().addForce(force);
    }

    public float getForceScale()
    {
        return deviceContext.getForceScale();
    }

    public void setForceScale(float forceScale)
    {
        deviceContext.setForceScale(forceScale);
    }

    public void setForceClamp(float forceClamp)
    {
        deviceContext.setForceClamp(forceClamp);
    }

    public void setForcesEnabled(boolean enabled)
    {
        deviceContext.setForceEnabled(enabled);
    }

    /**
     * Get current position
     *
     * @param outPosition Current position
     */
    @Override
    public void getPosition(Tuple3f outPosition)
    {
        hapticDevice.getPosition(outPosition);
    }

    /**
     * Returns a reference to a read-only haptic device.
     */
    @Override
    public IHapticReadOnlyDevice getDevice()
    {
        return hapticDevice;
    }

    public IForceListModel getForceListModel()
    {
        return deviceContext.getForceContext();
    }

    public IForceGetter getForceGetter()
    {
        return deviceContext;
    }

    @Override
    public void close()
    {
        deviceContext.close();
        super.close();
    }
}
