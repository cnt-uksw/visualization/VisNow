/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.DeviceName;

/**
 * Shows a list of devices with a ToogleButton. Pressing on a toggle button attaches the device to
 * the HapticViewer3D (registers its usage). Some devices maybe inactive - either cannot be
 * attached/detached (mouse - it is always present) or are being used in another HapticViewer3D at
 * that time. A label on a button and its state reflects state of a device.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class DevicesListGUI
    extends javax.swing.JPanel
    implements ViewerDeviceManager.IDeviceListener
{

    /**
     * HapticViewer3D for which registering and unregistering devices will be performed.
     */
    private HapticViewer3D viewer;
    /**
     * List of devices available in VisNow.
     */
    private List<DeviceName> deviceNames = new ArrayList<DeviceName>();
    //
    private javax.swing.GroupLayout layout;
    private javax.swing.GroupLayout.SequentialGroup hGroup;
    private javax.swing.GroupLayout.SequentialGroup vGroup;
    javax.swing.GroupLayout.ParallelGroup col1;
    javax.swing.GroupLayout.ParallelGroup col2;

    // <editor-fold defaultstate="collapsed" desc=" DeviceComponents ">   
    protected class DeviceComponents
    {

        JDeviceToggleButton button;
        javax.swing.JLabel label;

        public DeviceComponents(JDeviceToggleButton button, JLabel label)
        {
            this.button = button;
            this.label = label;
        }
    }

    private Map<String, DeviceComponents> deviceComponents = new HashMap<String, DeviceComponents>();

    private DeviceComponents getDeviceComponents(DeviceName deviceName)
    {

        DeviceComponents device = deviceComponents.get(deviceName.getName());
        if (device == null)
            throw new IllegalArgumentException(
                "Device with the name '" + deviceName.getName() + "' " +
                "(" + deviceName.getFriendlyName() + ") doesn't have its controls!");
        return device;
    }// </editor-fold>       

    // <editor-fold defaultstate="collapsed" desc=" Constructors ">   
    /**
     * Creates new form DevicesListGUI. Used only by GUI Builder
     *
     * @deprecated
     */
    public DevicesListGUI()
    {
        initComponents();
    }

    /**
     *
     * @param viewer HapticViewer3D posessing this component. Cannot be null.
     */
    public DevicesListGUI(HapticViewer3D viewer)
    {

        if (viewer == null) {
            throw new IllegalArgumentException("viewer cannot be null!");
        }
        this.viewer = viewer;
        myInitComponents();
        viewer.addRegisteredDevicesListener(this);
    }// </editor-fold>  

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 400, Short.MAX_VALUE)
                );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 300, Short.MAX_VALUE)
                );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    private void myInitComponents()
    {

        layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);

        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        hGroup = layout.createSequentialGroup();
        vGroup = layout.createSequentialGroup();

        col1 = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);
        col2 = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);

        hGroup.addGroup(col1);
        hGroup.addGroup(col2);

        javax.swing.GroupLayout.ParallelGroup row
            = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE);
        vGroup.addGroup(row);

        layout.setHorizontalGroup(hGroup);
        layout.setVerticalGroup(vGroup);

    }

    // <editor-fold defaultstate="collapsed" desc=" DeviceManager.IDeviceManagerListener methods "> 
    @Override
    public void devicesInitialList(List<DeviceName> devicesRefs)
    {
        this.setDevices(devicesRefs);
        this.recreateDevicesComponents();
    }

    @Override
    public void deviceAdded(DeviceName deviceRef)
    {
        addDeviceComponent(deviceRef);
    }

    @Override
    public void deviceRemoved(DeviceName deviceRef)
    {
        removeDeviceComponent(deviceRef);
    }

    @Override
    public void deviceChangedState(DeviceName deviceRef)
    {
        changeStateDeviceComponent(deviceRef);
    }

    public void registeredDevicesChanged(List<DeviceName> deviceRefs)
    {
    }

    // </editor-fold>  
    //
    private void setDevices(List<DeviceName> deviceRefs_)
    {
        this.deviceNames.clear();
        this.deviceNames.addAll(deviceRefs_);
    }

    // <editor-fold defaultstate="collapsed" desc=" Creating and removing buttons for devices ">   
    private void recreateDevicesComponents()
    {

        this.removeAll();

        for (DeviceName deviceRef : deviceNames) {
            addDeviceComponent(deviceRef);
        }

        this.doLayout();
    }

    private void addDeviceComponent(DeviceName deviceRef)
    {

        if (deviceComponents.containsKey(deviceRef.getName()))
            throw new IllegalArgumentException(
                "Device with the name '" + deviceRef.getName() + "' " +
                "(" + deviceRef.getFriendlyName() + ") already has its controls!");

        javax.swing.GroupLayout.ParallelGroup row = layout.createParallelGroup(GroupLayout.Alignment.CENTER);

        JDeviceToggleButton btn;

        btn = new JDeviceToggleButton(deviceRef);
        Dimension dim = new Dimension(100, 25);
        btn.setPreferredSize(dim);
        btn.setMinimumSize(dim);
        btn.setMargin(new Insets(4, 4, 4, 4));
        col1.addComponent(btn);
        row.addComponent(btn);

        javax.swing.JLabel label = new javax.swing.JLabel(deviceRef.getFriendlyName());
        col2.addComponent(label);
        row.addComponent(label);

        vGroup.addGroup(row);

        deviceComponents.put(deviceRef.getName(), new DeviceComponents(btn, label));
    }

    private void removeDeviceComponent(DeviceName deviceName)
    {

        DeviceComponents device = getDeviceComponents(deviceName);

        this.remove(device.button);
        this.remove(device.label);

        deviceComponents.remove(deviceName.getName());

    }

    private void changeStateDeviceComponent(DeviceName deviceName)
    {
        DeviceComponents device = getDeviceComponents(deviceName);
        device.button.update();
    }

    // </editor-fold>  
    //
    /**
     * A toggle button representing state of a device. With one JDeviceToggleButton exactly one
     * device is linked and it doesn't change throughout the life of the component.
     * <p/>
     * Clicking on a button registers/unregisters using this device in the HapticViewer3D.
     */
    public class JDeviceToggleButton extends javax.swing.JToggleButton
    {

        private final DeviceName deviceRef;

        public JDeviceToggleButton(DeviceName deviceRef_)
        {
            this.deviceRef = deviceRef_;

            update();

            this.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {

                    JDeviceToggleButton button = (JDeviceToggleButton) e.getSource();

                    if (!button.isSelected()) {
                        /* the button was just unselected - unregister using this device */
                        unregisterDevice();
                    } else {
                        registerDevice();
                    }

                    /* all GUI changes will be done after handling a callback - 
                     * probably while being in this line the callback has already been fired and processed */
                }
            });
        }

        /**
         * Updates the button - text and enabled, according to status of the device.
         */
        public final void update()
        {

            String text;
            boolean pressed, enabled;
            if (deviceRef.isUsed()) {
                pressed = true;
                if (deviceRef.isOwnedByMe(viewer) &&
                    deviceRef.isAttachable()) {
                    text = "IN USE";
                    enabled = true;
                } else {
                    text = "BUSY";
                    enabled = false;
                }
            } else {
                pressed = false;
                if (deviceRef.isAttachable()) {
                    text = "USE IT";
                    enabled = true;
                } else {
                    text = "UNAVAILABLE";
                    enabled = false;
                }
            }
            this.setText(text);
            this.setEnabled(enabled);
            this.setSelected(pressed);

        }

        /**
         * Register using a device by HapticViewer3D
         */
        protected void registerDevice()
        {
            viewer.registerDevice(deviceRef.getName());
        }

        /**
         * Unregister using a device by HapticViewer3D
         */
        public void unregisterDevice()
        {
            viewer.unregisterDevice(deviceRef.getName());
        }
    }

    /**
     * Called when DevicesListGUI is no longer needed (probably HapticViewer3D is being deleted).
     */
    public void onDelete()
    {
        viewer.removeRegisteredDevicesListener(this);
    }
}
// revised.
