/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import openhaptics.hdapi.misc.CalibrationState;
import openhaptics.hdapi.misc.CalibrationStyle;
import org.apache.log4j.Logger;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.DeviceWarningsHandler;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.haptics.HapticException;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.haptics.IHapticDevice;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.haptics.PhantomDeviceOH2;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.mouse.DummyMouseRegisteredDevice;
import org.visnow.vn.system.main.VisNow;

/**
 * DeviceManager stores a list of all connected devices (both of active and passive type).
 * There is exactly one DeviceManager per VisNow application.
 *
 * Each device that could be used by Viewer3D should be registered using {@link #registerDevice}
 * first.
 *
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class DeviceManager
{

    private static boolean devicesInitialized = false;
    /**
     * A list of all registered devices.
     * If IPassiveDevice is null, it is a mouse or other standard device that cannot be disabled.
     */
    private static final Map<String, IPassiveDevice> devices = new HashMap<String, IPassiveDevice>();
    //
    private static final Logger LOGGER
        = Logger.getLogger(new Throwable().getStackTrace()[0].getClassName());

    /* 
     * TODO MEDIUM: write static initialize that initializes all the devices from a configuration 
     * file: both haptic and non-haptic. It should use reflection and IPassiveDevice.createDevice() for each
     * class from a config file.
     */
    /**
     * Adds a device to a list of devices available in Viewer3D in VisNow.
     * The device must have a unique name and each device can be added exactly once. The name of the
     * device must not be changed after registering it here, as it could lead to errors.
     *
     * @param device The device to be registered.
     * <p>
     * @throws IllegalArgumentException if this device was added already or a device with such a
     *                                  name was added already
     */
    public static synchronized void registerDevice(IPassiveDevice device) throws IllegalArgumentException
    {
        if (devices.containsKey(device.getDeviceName())) {
            throw new IllegalArgumentException("A device with name \"" + device.getDeviceName() +
                "\" was already registered in DeviceManager!");
        }

        if (devices.containsValue(device)) {
            throw new IllegalArgumentException("This device (\"" + device.getDeviceName() + "\") " +
                "was already registered in DeviceManager");
        }

        devices.put(device.getDeviceName(), device);
        support.fireDeviceAdded(device);
    }

    /**
     * NOTE: this method is intended to be used <b>ONLY</b> by DeviceManager itself. All other classes
     * should register using a device by calling {@link #registerUsage}
     * <p/>
     * @param deviceName internal name of a device to be returned
     * <p>
     * @return device with the intenal name <code>deviceName</code>
     * <p>
     * @throws IllegalArgumentException when there is no device with such name
     */
    protected static synchronized IPassiveDevice getDevice(String deviceName) throws IllegalArgumentException
    {
        IPassiveDevice device = devices.get(deviceName);
        if (device == null) {
            throw new IllegalArgumentException("No device with name \"" + deviceName +
                "\" was registered in DeviceManager!");
        }
        return device;
    }

    // <editor-fold defaultstate="collapsed" desc=" Listeners ">     
    //TODO: maybe split change event into several events?
    // - newDeviceRegistered
    // - deviceUnregistered
    // - deviceNotUsed
    // - deviceUsed
    public interface IDeviceManagerListener
    {

        /**
         * Fired once - after a listener was registered.
         */
        public void devicesInitialList(List<DeviceName> devicesNames);

        /**
         * Fired after a new device was added.
         */
        public void deviceAdded(DeviceName deviceName);

        /**
         * Fired after a device was removed.
         */
        public void deviceRemoved(DeviceName deviceName);

        /**
         * Fired when a state of a device has changed (it started being used or stopped being
         * used).
         */
        public void deviceChangedState(DeviceName deviceName);
    }

    protected static DeviceManagerListenerSupport support = new DeviceManagerListenerSupport();

    // </editor-fold>       
    // <editor-fold defaultstate="collapsed" desc=" Other ">   
    /**
     * Returns a list of devices' references that are connected to the computer and properly initialized.
     *
     * @return list of devices
     */
    public static synchronized List<DeviceName> getDevicesNamesList()
    {

        LinkedList<DeviceName> deviceNames = new LinkedList<DeviceName>();

        IPassiveDevice[] a = devices.values().toArray(new IPassiveDevice[0]);
        Arrays.sort(a, new Comparator<IPassiveDevice>()
        {
            /**
             * Sorts in such a way that first there are all devices that can be attached (sorted
             * lexicografically) and then all that cannot be attached (e.g. mouse and devices that
             * are used in other viewers), also sorted lexicografically.
             */
            @Override
            public int compare(IPassiveDevice o1, IPassiveDevice o2)
            {
                // both devices are attachable - sort lexicografically
                if (o1.isAttachable() == o2.isAttachable())
                    return o1.getDeviceFriendlyName().compareTo(o2.getDeviceFriendlyName());

                // o1 is attachable - display it first
                if (o1.isAttachable())
                    return -1;

                // o2 is attachable - display it first
                return 1;

            }
        });

        //        Collection<IPassiveDevice> devicesColl = devices.values();
        //        Collections.sort(devicesColl, new Comparator<IPassiveDevice>() {
        //
        //            @Override
        //            public int compare(IPassiveDevice o1, IPassiveDevice o2) {
        //                throw new UnsupportedOperationException("Not supported yet.");
        //            }
        //        });
        for (IPassiveDevice device : devices.values()) {
            deviceNames.add(new DeviceName(device));
        }

        return deviceNames;
    }

    //
    //    /**
    //     * Returns a list of devices' objects (a copy linked to a map!)
    //     *
    //     * @return
    //     */
    //    protected static synchronized Collection<IPassiveDevice> getDevicesList() {
    //        return devices.values();
    //    }
    //TODO MEDIUM: call it in VisNow when exiting application
    public static void doCleanup()
    {
        for (IPassiveDevice dev : devices.values()) {
            System.out.println("CLOSING device connection with: " + dev.getDeviceFriendlyName());
            dev.close();
        }
    }

    /**
     * Initializes all external devices: haptic devices, tablets and so on. Should use reflection
     * or some other method to half-automatically detect devices to be initialized - maybe
     * initialize
     * all classes from a specific module or directory, or parse a configuration file with a list
     * of devices?
     */
    public static void initializeDevicesOnce(DeviceWarningsHandler warningsHandler)
    {
        // ------------- TODO MEDIUM: use a separate module with reflection or configuration files
        if (!devicesInitialized) {

            /* initialize mouse (a dummy object) */
            registerDevice(DummyMouseRegisteredDevice.getInstance());

            /* initialize Phantom device */
            try {
                IHapticDevice phantomDevice = new PhantomDeviceOH2();
                DeviceManager.registerDevice(phantomDevice);
            } catch (HapticException ex) {
                String message
                    = "Problem with Phantom\n\n" +
                    "Error: " + ex;
                String title = "Error";
                LOGGER.error(message, ex);
                if (VisNow.get().getMainWindow().isVisible())
                    JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
            }

            /* here initialize tablets, fridges and microwaves ;) */
            devicesInitialized = true;
        }
    }

    public static void fireDeviceIsUsed(IPassiveDevice device)
    {
        support.fireDeviceChangedState(device);
    }

    public static void fireDeviceIsUnused(IPassiveDevice device)
    {
        support.fireDeviceChangedState(device);
    }

    public static void registerAsListener(IDeviceManagerListener l)
    {
        support.addRegisteredDevicesListener(l);
    }

    // </editor-fold>       
    // <editor-fold defaultstate="collapsed" desc=" Check device state ">    
    public static boolean isUsed(String deviceName) throws IllegalArgumentException
    {
        IPassiveDevice device = getDevice(deviceName);

        if (device == null) // an internal device - always return true
            return true;
        else
            return device.isUsed();
    }

    @SuppressWarnings("deprecation")
    // this is only to suppress warning for device.isOwnedByMe() - here is the only place in the whole code, where this method SHOULD be called.
    public static boolean isOwnedByMe(String deviceName, Object owner) throws IllegalArgumentException
    {
        IPassiveDevice device = getDevice(deviceName);

        if (device == null) // an internal device - always return false
            return false;
        else
            return device.isOwnedByMe(owner);
    }

    public static boolean isAttachable(String deviceName)
    {
        IPassiveDevice device = getDevice(deviceName);

        return device.isAttachable();
    }

    //
    //    public static boolean isDetachable(String deviceName) {
    //        IPassiveDevice device = getDevice(deviceName);
    //        
    //        return device.isDetachable();
    //    }
    //    
    // </editor-fold> 
    //
    // <editor-fold defaultstate="collapsed" desc=" Register / unregister usage ">    
    @SuppressWarnings("deprecation")
    // this is only to suppress warning for device.registerUsage() - here is the only place in the whole code, where this method SHOULD be called.
    public static IPassiveDevice registerUsage(String deviceName, Object ownerWannabe, DeviceWarningsHandler warningsHandler)
        throws IPassiveDevice.DeviceRegisterException
    {
        //        if (isUsed(deviceName))
        //            throw new RuntimeException(
        //                    "Could not get the device " + deviceName
        //                    + " - it's already being used!");

        IPassiveDevice device = getDevice(deviceName);
        device.registerUsage(ownerWannabe);
        support.fireDeviceChangedState(device);

        //TODO IMPORTANT: calibration should be moved one day to Phantom class
        if (device instanceof PhantomDeviceOH2) {
            PhantomDeviceOH2 phantomDevice = (PhantomDeviceOH2) device;

            try {
                boolean loop = true;
                while (loop) {
                    /* Check calibration status */
                    int calibrationStatus = phantomDevice.getCalibrationStatus();
                    LOGGER.info("calibration: " + calibrationStatus);
                    LOGGER.info("OK: " + CalibrationState.OK);
                    LOGGER.info("NEEDS_UPDATE: " + CalibrationState.NEEDS_UPDATE);
                    LOGGER.info("NEEDS_MANUAL_INPUT: " + CalibrationState.NEEDS_MANUAL_INPUT);
                    switch (calibrationStatus) {
                        case CalibrationState.OK:
                            loop = false;
                            break; // Ok, go further
                        case CalibrationState.NEEDS_UPDATE:
                        // TODO MEDIUM: here could be call to updateCalibration(), so far a message is generated

                        case CalibrationState.NEEDS_MANUAL_INPUT: // generate a warning that the device is not calibrated

                            int calibrationStyles = phantomDevice.getCalibrationStyles();
                            LOGGER.info("calibrationStyles: " + calibrationStyles);
                            if ((calibrationStyles & CalibrationStyle.ENCODER_RESET) == 0) // no ENCODER_RESET style of calibration available - show warning
                                warningsHandler.handleDeviceWarning(
                                    new DeviceName(phantomDevice),
                                    "Device is not calibrated and it could not be calibrated automatically. " +
                                    "Without calibration the device can be imprecise. \n" +
                                    "Please close the program and calibrate using the software provided by device vendor.");
                            else {
                                boolean ok
                                    = warningsHandler.handleDeviceWarningOkCancel(
                                        new DeviceName(phantomDevice),
                                        "Device is not calibrated. \n" +
                                        "Please hold the stylus in such a way, that all links are orthogonal and press OK.\n" +
                                        "Pressing Cancel will result in continuing without calibration.");
                                if (ok) {
                                    phantomDevice.calibrate();
                                } else {
                                    loop = false;
                                }
                            }
                            break;
                    }
                }
            } catch (HapticException ex) {
                warningsHandler.handleDeviceWarning(
                    new DeviceName(phantomDevice),
                    "An error occured and the device may be uncalibrated. \n" +
                    "Without calibration the device can be imprecise. " +
                    "If you experience such problems, please try reattaching this device.");
            }
        }

        return device;
    }

    @SuppressWarnings("deprecation")
    // this is only to suppress warning for device.unregisterUsage() - here is the only place in the whole code, where this method SHOULD be called.
    public static void unregisterUsage(IPassiveDevice device, Object deviceOwner)
        throws IPassiveDevice.DeviceRegisterException
    {
        device.unregisterUsage(deviceOwner);
        support.fireDeviceChangedState(device);
    }
}

// </editor-fold> 
