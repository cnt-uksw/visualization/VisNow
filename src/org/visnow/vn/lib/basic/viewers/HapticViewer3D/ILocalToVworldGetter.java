/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D;

import org.jogamp.java3d.Transform3D;

/**
 * Implemented by an object that is capable of returning a local-to-vworld transform. Probably it is
 * HapticViewer3D which adds haptic outline box and returns its localToVworld transform.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public interface ILocalToVworldGetter
{

    /**
     * Stores local-to-vworld transform in
     * <code>localToVWorld</code>.
     * Due to race conditions, being notified by onLocalToVworldSet() does NOT mean that this
     * transform will be for sure available by the time
     * {@link ILocalToVworldGetter#getLocalToVworld} is called. If it's not,
     * ({@link ILocalToVworldGetter.NoDataException} will be thrown. For a short period of time it's
     * perfectly normal to see this exception.
     * <p/>
     * @throws NoDataException if the transform is not available
     */
    void getLocalToVworld(Transform3D localToVWorld) throws NoDataException;

    void addLocalToVworldChangeListener(ILocalToVworldListener listener);

    void removeLocalToVworldChangeListener(ILocalToVworldListener listener);

    /**
     * Used by ITrackerToVworldGetter or OTHER CLASSES (ForceContext, HapticViewer3D) when a
     * transform local -> vworld could not be fetched.<br/>
     * This can be either because an haptic outline box was not added to the scene or because a
     * reference to ILocalToVworldGetter in ForceContext is null.
     */
    public class NoDataException extends Exception
    {
    }
}
// revised.
