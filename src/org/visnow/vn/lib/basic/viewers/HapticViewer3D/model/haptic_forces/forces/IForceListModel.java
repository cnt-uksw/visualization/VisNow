/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces;

import javax.swing.ListModel;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.IForceContextChangeListener;

/**
 * An interface for a JList with forces in HapticGUI
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public interface IForceListModel extends ListModel
{

    /**
     * Replaces one force with another.
     * <p/>
     * @param oldForce force to be replaced
     * @param newForce new force
     * <p>
     * @throws IllegalArgumentException if <code>oldForce</code> doesn't exist
     */
    public void set(IForce oldForce, IForce newForce);

    /**
     * Deletes force from a given index. Index must exist.
     *
     * @param index index to be removed
     * <p/>
     */
    public void remove(int index);

    /**
     * Sets from enabled to disabled or opposite.
     */
    public void swapEnabled(int index);

    public IForce get(int index);

    /**
     * Duplicates forces from a given index and returns the index of a duplicate. Returns -1 on
     * error (unchangeable force to be duplicated).
     * <p/>
     * @param index index of a force to be duplicated
     * <p>
     * @return index of a duplicated item or -1 on error
     */
    int duplicateForce(int index);

    /**
     * Adds a new force at the end of the list.
     *
     * Synchronization is made on a local copy of forces set. Blocking
     * synchronization is unacceptable due to real-time requirements
     * of haptic devices.
     *
     * @param force Force to be added
     */
    public void addForce(IForce force);

    /**
     * Inserts
     * <code>force</code> on index
     * <code>onIndex</code> (shifts all following forces). To insert force at the end, set
     * <code>onIndex</code> to -1 or call {@link #addForce(IForce)
     * }.
     * <p/>
     * @param force
     * @param onIndex
     */
    public void addForce(IForce force, int onIndex);

    /**
     * Currently unused.
     */
    public void addForceContextChangeListener(IForceContextChangeListener l);

    /**
     * Currently unused.
     */
    public void removeForceContextChangeListener(IForceContextChangeListener l);
}
// revised.
