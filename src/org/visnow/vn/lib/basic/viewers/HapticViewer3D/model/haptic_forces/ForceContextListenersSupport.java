/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces;

import java.util.ArrayList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.IForce;

/**
 * Helper object for managing {@link ListDataListener} and {@link IForceContextChangeListener}
 * objects in {@link ForceContext}.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
class ForceContextListenersSupport
{

    protected final ArrayList<ListDataListener> basicListeners = new ArrayList<ListDataListener>();
    protected final ArrayList<IForceContextChangeListener> changeListeners = new ArrayList<IForceContextChangeListener>();

    //
    /**
     * Notify all listeners ({@link #basicListeners}) that a force on index
     * <code>index</code> has been added.
     * <p/>
     * Should be called outside of a
     * <code>synchronized</code> block.
     * <p/>
     * @param index index with an added force
     */
    protected void fireForceAdded(IForce f, int index, ForceContext fc)
    {
        ListDataEvent e = new ListDataEvent(fc, ListDataEvent.INTERVAL_ADDED, index, index);
        synchronized (basicListeners) {
            for (ListDataListener l : basicListeners) {
                l.intervalAdded(e);
            }
        }
    }

    /**
     * Notify all listeners ({@link #basicListeners}) that a force on index
     * <code>index</code> has been removed.
     * <p/>
     * Should be called outside of a
     * <code>synchronized</code> block.
     * <p/>
     * @param index index with a removed force
     */
    protected void fireForceRemoved(int index, ForceContext fc)
    {
        fireForceRemoved(index, index, fc);
    }

    /**
     * Notify all listeners ({@link #basicListeners}) that a force on index
     * <code>index</code> has been removed.
     * <p/>
     * Should be called outside of a
     * <code>synchronized</code> block.
     * <p/>
     * @param fromIndex first index with a removed force
     * @param toIndex   last index with a removed force
     */
    protected void fireForceRemoved(int fromIndex, int toIndex, ForceContext fc)
    {
        ListDataEvent e = new ListDataEvent(fc, ListDataEvent.INTERVAL_REMOVED, fromIndex, toIndex);
        synchronized (basicListeners) {
            for (ListDataListener l : basicListeners) {
                l.intervalRemoved(e);
            }
        }
    }

    /**
     * Notify all listeners ({@link #basicListeners}) that a force on index
     * <code>index</code> has changed.
     * <p/>
     * Should be called outside of a
     * <code>synchronized</code> block.
     * <p/>
     * @param index index of a modified force
     */
    protected void fireForceChanged(int index, ForceContext fc)
    {
        fireForceChanged(index, index, fc);
    }

    /**
     * Notify all listeners ({@link #basicListeners}) that forces from indexes
     * <code>fromIndex</code>-
     * <code>toIndex</code>
     * have changed.
     * <p/>
     * Should be called outside of a
     * <code>synchronized</code> block.
     * <p/>
     * @param fromIndex one end of the interval
     * @param toIndex   the other end of the interval
     */
    protected void fireForceChanged(int fromIndex, int toIndex, ForceContext fc)
    {
        ListDataEvent e = new ListDataEvent(fc, ListDataEvent.CONTENTS_CHANGED, fromIndex, toIndex);

        synchronized (basicListeners) {
            for (ListDataListener l : basicListeners) {
                l.contentsChanged(e);
            }
        }
    }

    public void addListDataListener(ListDataListener l)
    {
        synchronized (basicListeners) {
            basicListeners.add(l);
        }
    }

    public void removeListDataListener(ListDataListener l)
    {
        synchronized (basicListeners) {
            basicListeners.remove(l);
        }
    }

    public void addForceContextChangeListener(IForceContextChangeListener l)
    {
        synchronized (changeListeners) {
            changeListeners.add(l);
        }
    }

    public void removeForceChangeListener(IForceContextChangeListener l)
    {
        synchronized (changeListeners) {
            changeListeners.remove(l);
        }
    }
}
//revised
