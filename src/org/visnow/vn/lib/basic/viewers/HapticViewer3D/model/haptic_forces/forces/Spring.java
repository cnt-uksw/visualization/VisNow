/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces;

import org.jogamp.vecmath.Point3f;
import org.jogamp.vecmath.Tuple3f;
import org.jogamp.vecmath.Vector3f;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ILocalToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ITrackerToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.CoordinateSystem;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.HapticLocationData;

/**
 * Spring or any other force directly proportional to the distance. Although its center is described
 * in local coordinates, the force is generated also
 * when cursor is outside of the haptic outline.
 * <p/>
 * Its value is dependent on size of haptic field - the same spring constant might be good for small
 * haptic field and very bad for large one. Default value is good for test regular field 3D.
 * <p/>
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 */
public class Spring extends AbstractForce
{

    private final static CoordinateSystem forceCoordinateSystem = CoordinateSystem.LOCAL;
    //
    /**
     * The proper value is dependent on size of haptic field - the same spring constant might be
     * good for small haptic field and very bad for large one. Default value is good for test
     * regular field 3D.
     */
    public final static float DEFAULT_SPRING_CONSTANT = 50.0f;
    private float springConstant;
    private Tuple3f center;

    public Spring()
    {
        this(DEFAULT_SPRING_CONSTANT, new Point3f());
    }

    public Spring(float springConstant, Tuple3f center)
    {
        super(forceCoordinateSystem);

        if (center == null) {
            throw new NullPointerException("Argument 'center' cannot be null");
        }
        this.springConstant = springConstant;
        this.center = center;
    }

    /**
     * Copy constructor. To be used only by
     * <code>clone()</code> method.
     * <p/>
     * @param force Object to be copied from
     */
    protected Spring(Spring force)
    {
        super(force);
        center = new Point3f(force.center);
        springConstant = force.springConstant;
    }

    @Override
    public void getForce(HapticLocationData locationData, Vector3f out_force)
        throws ITrackerToVworldGetter.NoDataException,
        ILocalToVworldGetter.NoDataException
    {

        Point3f position = locationData.getCurrentLocalPosition();
        out_force.sub(center, position);
        out_force.scale(springConstant);
    }

    public Tuple3f getCenter()
    {
        return center;
    }

    public void setCenter(Tuple3f center)
    {
        this.center.set(center);
    }

    public float getConstant()
    {
        return springConstant;
    }

    public void setConstant(float constant)
    {
        this.springConstant = constant;
    }

    @Override
    public IForce clone()
    {
        return new Spring(this);
    }

    @Override
    public String getName()
    {
        return getClassSimpleName() + " [k=" + springConstant + ", c=" + center + "]";
    }
}
// revised.
