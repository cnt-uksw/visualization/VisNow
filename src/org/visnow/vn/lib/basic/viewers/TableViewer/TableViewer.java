//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
//</editor-fold>
package org.visnow.vn.lib.basic.viewers.TableViewer;

import java.awt.Component;
import java.awt.Frame;
import org.visnow.vn.lib.basic.viewers.TableViewer.ExtendedTable.ExtendedTablePanel;
import org.visnow.vn.lib.basic.viewers.TableViewer.ExtendedTable.TableData;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.Input;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.Inputs;
import org.visnow.vn.engine.core.Link;
import org.visnow.vn.engine.core.LinkFace;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.lib.basic.viewers.TableViewer.ExtendedTable.ExtendedTable;
import static org.visnow.vn.lib.basic.viewers.TableViewer.TableViewerShared.*;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.gui.utils.ExtendedMenuItem;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.gui.utils.RemoveComponentActionListener;
import org.visnow.vn.lib.gui.SimpleOneButtonGUI;

/**
 * Table viewer
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 * @author norkap
 */
public class TableViewer extends ModuleCore implements RemoveComponentActionListener
{

    protected ArrayList<ExtendedTable> tables;
    protected TableViewerFrame frame = new TableViewerFrame();
    private SimpleOneButtonGUI ui = null;
    private boolean seriesChanged = false;
    private Map<String, String> currentConnectedComponents;

    public TableViewer()
    {
        this.tables = new ArrayList<>();
        this.currentConnectedComponents = new HashMap<>();

        parameters.addParameterChangelistener((String name) -> {
            if (name.equals(SERIES.getName())) {
                seriesChanged = true;
                startAction();
            } else {
                seriesChanged = false;
            }
        });

        frame.addTableActionListener(new AddChartListener());

        SwingInstancer.swingRunAndWait(() -> {
            ui = new SimpleOneButtonGUI();

            ExtendedTablePanel chartPanel = new ExtendedTablePanel();
            chartPanel.addRemoveChartListener(getInstance());
            TableData data = new TableData();

            ExtendedTable chart = new ExtendedTable(data, chartPanel);
            tables.add(chart);

            frame.addTableToDisplay(chartPanel);
            frame.pack();
            frame.setTitle("VisNow Table Viewer");
            frame.setVisible(true);

            ui.addChangeListener(new ChangeListener()
            {
                @Override
                public void stateChanged(ChangeEvent evt)
                {
                    frame.setVisible(true);
                    frame.setExtendedState(Frame.NORMAL);
                }
            });
            setPanel(ui);
        });

    }

    private TableViewer getInstance()
    {
        return this;
    }

    public static InputEgg[] inputEggs = null;

    @Override
    public void onActive()
    {
        Parameters p = parameters.getReadOnlyClone();

        if (seriesChanged) {

            Map<String, DataSet> series = p.get(SERIES);

            for (ExtendedTable chart : tables) {
                chart.setSeries(series);
                chart.update();
            }
            seriesChanged = false;
        } else {
            Inputs moduleInputs = getInputs();
            Iterator it = moduleInputs.iterator();

            if (it.hasNext()) {

                currentConnectedComponents = new HashMap<>();
                Map<String, DataSet> series = new LinkedHashMap<>();
                Vector<Link> links = ((Input) it.next()).getLinks();
                for (Link link : links) {
                    String moduleName = link.getName().getOutputModule();
                    VNRegularField field = (VNRegularField) link.getOutput().getData().getValue();

                    if (field != null) {
                        RegularField inFld = ((VNRegularField) field).getField();
                        if (inFld.getDimNum() == 3) {
                            VisNow.get().userMessageSend(this, "3D fields are not supported", "", Level.ERROR);
                        } else {
                            updateSeries(series, inFld, moduleName);
                        }
                    }
                }
                parameters.set(SERIES, series);
            }
        }
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(SERIES, new LinkedHashMap<>())
        };
    }

    private void setSeries(Map<String, DataSet> series)
    {
        parameters.set(SERIES, series);
    }

    @Override
    public void onInputAttach(LinkFace link)
    {

        VNRegularField field = (VNRegularField) link.getOutput().getData().getValue();
        String moduleName = link.getOutput().getModuleBox().getName();

        if (field == null)
            return;

        Map<String, DataSet> series = parameters.get(SERIES);
        RegularField inFld = ((VNRegularField) field).getField();
        if (inFld.getDimNum() == 3) {
            VisNow.get().userMessageSend(this, "3D fields are not supported", "", Level.ERROR);
            return;
        }
        boolean daAdded = updateSeries(series, inFld, moduleName);
        if (daAdded) {
            setSeries(series);
        }
    }

    @Override
    public void onInputDetach(LinkFace link)
    {

        String moduleName = link.getOutput().getModuleBox().getName();
        HashMap<String, String> newCurrentConnectedComponents = new HashMap<>();

        currentConnectedComponents.entrySet().forEach(entry -> {
            if ((entry.getKey()).split("&")[1].equals(moduleName)) {
                parameters.get(SERIES).remove(entry.getValue());
            } else {
                newCurrentConnectedComponents.put(entry.getKey(), entry.getValue());
            }
        });
        if (newCurrentConnectedComponents.size() != currentConnectedComponents.size()) {
            currentConnectedComponents = newCurrentConnectedComponents;
            parameters.fireParameterChanged(SERIES.getName());
        }
    }

    @Override
    public void removeComponent(Component comp)
    {
        int idx = frame.getComponentIndex(comp);
        tables.remove(idx);
        frame.removeTableFromDisplay((ExtendedTablePanel) comp);
        showLastComponentManagerContainer();
    }

    private void addChart()
    {
        ExtendedTablePanel chartPanel = new ExtendedTablePanel();
        chartPanel.addRemoveChartListener(this);
        TableData data = new TableData();
        ExtendedTable chart = new ExtendedTable(data, chartPanel);

        Parameters p = parameters.getReadOnlyClone();
        chart.setSeries(p.get(SERIES));
        chart.update();

        tables.add(chart);
        frame.addTableToDisplay(chartPanel);
        showLastComponentManagerContainer();
    }

    private void showLastComponentManagerContainer()
    {
        boolean show;
        for (int i = 0; i < tables.size(); i++) {
            show = (i == (tables.size() - 1));
            tables.get(i).getTablePanel().showComponentsList(show);
        }
    }

    class RemoveChartListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (e.getSource() instanceof ExtendedMenuItem) {
                ExtendedMenuItem mItem = (ExtendedMenuItem) e.getSource();
                removeComponent(SwingUtilities.getAncestorOfClass(ExtendedTablePanel.class, mItem.getAncestor()));
            }
        }

    }

    class AddChartListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            addChart();
        }

    }

    @Override
    public void onInitFinished()
    {
        updateFrameName();
    }

    @Override
    public void onNameChanged()
    {
        updateFrameName();
    }

    private void updateFrameName()
    {
        if (frame != null)
            frame.setTitle("VisNow Table Viewer - " + this.getApplication().getTitle() + " - " + this.getName());
    }

    @Override
    public void onDelete()
    {
        frame.dispose();
    }

    private boolean updateSeries(Map<String, DataSet> series, RegularField inFld, String moduleName)
    {
        boolean daAdded = false;
        for (int i = 0; i < inFld.getNComponents(); i++) {
            DataArray da = inFld.getComponent(i);
            String name = da.getName();
            int j = 2;
            String temp_name = name;
            while (series.containsKey(name)) {
                name = temp_name + j;
                j++;
            }
            series.put(name, TableViewerCore.createSeries(da, inFld, name));
            currentConnectedComponents.put(da.getName() + "&" + moduleName, name);
            daAdded = true;
        }
        return daAdded;
    }

}
