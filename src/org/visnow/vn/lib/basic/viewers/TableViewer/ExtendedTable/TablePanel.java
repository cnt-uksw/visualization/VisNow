//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
//</editor-fold>
package org.visnow.vn.lib.basic.viewers.TableViewer.ExtendedTable;

import org.visnow.vn.gui.utils.TableColumnAdjuster;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import javax.swing.AbstractListModel;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import org.apache.commons.math3.util.FastMath;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.vn.lib.basic.viewers.TableViewer.DataSet;
import org.visnow.vn.lib.utils.StringUtils;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import java.util.Arrays;

/**
 * Tabbed pane storing visual representation (as JTable) of selected components.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class TablePanel extends JTabbedPane
{

    public void addTable(DataSet dataSet)
    {

        final int MIN_COLUMN_WIDTH = 30;
        String[][] data = datasetToStringData(dataSet, dataSet.getFormat());
        int width = data[0].length;
        int height = data.length;
        String[] columnHeaders = StringUtils.generateExcelColumnNames(width, true, 0);
        String[] rowHeaders = new String[height];
        for (int i = 0; i < height; i++) {
            rowHeaders[i] = Integer.toString(i);
        }
        ListModel<String> rowHeadersListModel = new RowHeaderListModel(rowHeaders);
        DefaultTableModel tableModel = new DefaultTableModel(width, height);
        tableModel.setDataVector(data, columnHeaders);

        JTable table = new JTable(tableModel)
        {
            DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

            {
                renderRight.setHorizontalAlignment(SwingConstants.RIGHT);
            }

            DefaultTableCellRenderer headerRenderer = new DefaultTableCellRenderer();

            @Override
            public TableCellRenderer getCellRenderer(int arg0, int arg1)
            {
                return renderRight;
            }

            @Override
            public boolean editCellAt(int row, int column, java.util.EventObject e)
            {
                return false;
            }

        };
        table.setFont(new Font(Font.DIALOG_INPUT, Font.BOLD, 12));
        table.setPreferredScrollableViewportSize(new Dimension(500, 100));
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setCellSelectionEnabled(true);
        table.changeSelection(0, 1, false, false);

        TableColumnAdjuster tca = new TableColumnAdjuster(table);
        tca.adjustColumns();

        JList<String> rowHeaderList = new JList<>(rowHeadersListModel);
        rowHeaderList.setFixedCellWidth(FastMath.max(MIN_COLUMN_WIDTH, String.valueOf(height).length() * 15));
        rowHeaderList.setFixedCellHeight(table.getRowHeight());
        rowHeaderList.setCellRenderer(new RowHeaderRenderer(table));

        JScrollPane scrollPane = new JScrollPane(table, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setPreferredSize(new Dimension(100, 100));
        scrollPane.setRowHeaderView(rowHeaderList);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.add(Box.createRigidArea(new Dimension(0, 5)));
        panel.add(scrollPane);

        addTab(dataSet.getName(), null, panel);
    }

    public void updateTable(DataSet dataSet)
    {

        int tabIdx;
        if ((tabIdx = findTabByName(dataSet.getName())) >= 0) {
            JPanel panel = (JPanel) getComponentAt(tabIdx);
            JScrollPane scrollPane = (JScrollPane) panel.getComponent(1);
            JTable table = (JTable) scrollPane.getViewport().getView();
            DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
            String[][] data = datasetToStringData(dataSet, dataSet.getFormat());
            String[] columnHeaders = StringUtils.generateExcelColumnNames(data[0].length, true, 0);
            tableModel.setDataVector(data, columnHeaders);
            table.setModel(tableModel);
            TableColumnAdjuster tca = new TableColumnAdjuster(table);
            tca.adjustColumns();
        }
    }

    public void removeAllTables()
    {
        removeAll();
    }

    private int findTabByName(String title)
    {
        int tabCount = getTabCount();
        for (int i = 0; i < tabCount; i++) {
            String tabTitle = getTitleAt(i);
            if (tabTitle.equals(title)) return i;
        }
        return -1;
    }

    private static class RowHeaderListModel extends AbstractListModel<String>
    {

        private final String[] rowHeaders;

        public RowHeaderListModel(String[] rowHeaders)
        {
            this.rowHeaders = rowHeaders;
        }

        @Override
        public int getSize()
        {
            return rowHeaders.length;
        }

        @Override
        public String getElementAt(int index)
        {
            return rowHeaders[index];
        }
    }

    private static class RowHeaderRenderer extends JLabel implements ListCellRenderer<String>
    {

        RowHeaderRenderer(JTable table)
        {
            JTableHeader header = table.getTableHeader();
            setOpaque(true);
            setBorder(UIManager.getBorder("TableHeader.cellBorder"));
            setHorizontalAlignment(CENTER);
            setForeground(header.getForeground());
            setBackground(header.getBackground());
            setFont(header.getFont());
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends String> jlist, String e, int i, boolean bln, boolean bln1)
        {
            setText((e == null) ? "" : e);
            return this;
        }
    }

    private static String[][] datasetToStringData(DataSet dataset, String format)
    {
        LargeArray data = dataset.getData();
        LargeArrayType type = data.getType();
        int vlen = dataset.getVectorLength();
        int[] dims = dataset.getDimentions();
        final String[][] stringData;
        if (dims.length == 2) {
            stringData = new String[dims[1]][dims[0]];
            int nthreads = (int) FastMath.min(dims[1], ConcurrencyUtils.getNumberOfThreads());
            Future[] futures = new Future[nthreads];
            int k = dims[1] / nthreads;
            for (int t = 0; t < nthreads; t++) {
                final int firstIdx = t * k;
                final int lastIdx = (t == nthreads - 1) ? dims[1] : firstIdx + k;
                if (!type.isComplexNumericType()) {
                    futures[t] = ConcurrencyUtils.submit(() -> {
                        String[] elem = new String[vlen];
                        for (int i = firstIdx; i < lastIdx; i++) {
                            for (int j = 0; j < dims[0]; j++) {
                                for (int v = 0; v < vlen; v++) {
                                    elem[v] = String.format(format, data.get(i * dims[0] * vlen + j * vlen + v));
                                }
                                if (vlen > 1) {
                                    stringData[i][j] = Arrays.toString(elem);
                                } else {
                                    stringData[i][j] = elem[0];
                                }
                            }
                        }
                    });
                } else {
                    futures[t] = ConcurrencyUtils.submit(() -> {
                        String[] elem = new String[vlen];
                        for (int i = firstIdx; i < lastIdx; i++) {
                            for (int j = 0; j < dims[0]; j++) {
                                for (int v = 0; v < vlen; v++) {
                                    elem[v] = StringUtils.complexToString((float[]) data.get(i * dims[0] * vlen + j * vlen + v), format);
                                }
                                if (vlen > 1) {
                                    stringData[i][j] = Arrays.toString(elem);
                                } else {
                                    stringData[i][j] = elem[0];
                                }
                            }
                        }
                    });

                }
            }
            try {
                ConcurrencyUtils.waitForCompletion(futures);
            } catch (InterruptedException | ExecutionException ex) {
                // don't do anything - return incomplete stringData
            }

        } else {
            stringData = new String[1][dims[0]];
            int nthreads = (int) FastMath.min(dims[0], ConcurrencyUtils.getNumberOfThreads());
            Future[] futures = new Future[nthreads];
            int k = dims[0] / nthreads;
            for (int t = 0; t < nthreads; t++) {
                final int firstIdx = t * k;
                final int lastIdx = (t == nthreads - 1) ? dims[0] : firstIdx + k;
                if (!type.isComplexNumericType()) {
                    futures[t] = ConcurrencyUtils.submit(() -> {
                        String[] elem = new String[vlen];
                        for (int i = firstIdx; i < lastIdx; i++) {
                            for (int v = 0; v < vlen; v++) {
                                elem[v] = String.format(format, data.get(i * vlen + v));
                            }
                            if (vlen > 1) {
                                stringData[0][i] = Arrays.toString(elem);
                            } else {
                                stringData[0][i] = elem[0];
                            }
                        }
                    });
                } else {
                    futures[t] = ConcurrencyUtils.submit(() -> {
                        String[] elem = new String[vlen];
                        for (int i = firstIdx; i < lastIdx; i++) {
                            for (int v = 0; v < vlen; v++) {
                                elem[v] = StringUtils.complexToString((float[]) data.get(i * vlen + v), format);
                            }
                            if (vlen > 1) {
                                stringData[0][i] = Arrays.toString(elem);
                            } else {
                                stringData[0][i] = elem[0];
                            }
                        }
                    });
                }
            }
            try {
                ConcurrencyUtils.waitForCompletion(futures);
            } catch (InterruptedException | ExecutionException ex) {
                // don't do anything - return incomplete stringData
            }
        }
        return stringData;
    }
}
