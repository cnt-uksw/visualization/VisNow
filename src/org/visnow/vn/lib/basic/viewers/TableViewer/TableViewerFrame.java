//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
//</editor-fold>
package org.visnow.vn.lib.basic.viewers.TableViewer;

import java.awt.Component;
import org.visnow.vn.lib.basic.viewers.TableViewer.ExtendedTable.ExtendedTablePanel;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.plaf.IconUIResource;
import org.visnow.vn.gui.utils.NodeIcon;

/**
 * The main frame of TableViewer.
 * 
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 * @author norkap
 */
public class TableViewerFrame extends JFrame
{

    private final JMenuBar menuBar;
    private final JMenuItem addChartMenuItem;
    private final JButton addChartButton;
    private final JScrollPane scrollPane;
    private final JPanel mainPanel;

    public TableViewerFrame()
    {
        setLayout(new GridBagLayout());
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setMinimumSize(new Dimension(810, 200));
        setPreferredSize(new Dimension(810, 400));
        setIconImage(new ImageIcon(getClass().getResource("/org/visnow/vn/gui/icons/big/visnow.png")).getImage());
        
        menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");

        addChartMenuItem = new JMenuItem("Add Table");

        JMenuItem menuItem3 = new JMenuItem("Exit");
        menuItem3.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                dispose();
            }
        });

        menu.add(addChartMenuItem);
        menu.addSeparator();
        menu.add(menuItem3);
        menuBar.add(menu);
        setJMenuBar(menuBar);

        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        scrollPane = new JScrollPane(mainPanel);
        scrollPane.setPreferredSize(new Dimension(1000, 400));

        GridBagConstraints gridBC = new GridBagConstraints();
        gridBC.weightx = 1;
        gridBC.weighty = 1;
        gridBC.fill = GridBagConstraints.BOTH;
        add(scrollPane, gridBC);
        
        
        addChartButton = new JButton("Add Table"); 
        Icon icon = new IconUIResource(new NodeIcon('+'));
        addChartButton.setIcon(icon);
        
        gridBC = new GridBagConstraints();
        gridBC.gridy = 2;
        gridBC.weightx = 1;
        gridBC.weighty = 0;
        gridBC.fill = GridBagConstraints.HORIZONTAL;
        add(addChartButton, gridBC);
    }

    public int getComponentIndex(Component comp)
    {
        for (int i = 0; i < mainPanel.getComponentCount(); i++) {
            if (comp == mainPanel.getComponent(i))
                return i;
        }
        return -1;
    }

    public Component getComponent(String name)
    {
        for (int i = 0; i < getContentPane().getComponentCount(); i++) {
            if (name.equals(getContentPane().getComponent(i).getName()))
                return getContentPane().getComponent(i);
        }
        return null;
    }

    public void addTableToDisplay(ExtendedTablePanel chart)
    {
        GridBagConstraints gridBC = new GridBagConstraints();
        gridBC.weightx = 1;
        gridBC.weighty = 1;
        gridBC.fill = GridBagConstraints.BOTH;
        gridBC.gridx = 0;
        mainPanel.add(chart, gridBC);
        Dimension dims = getMinimumSize();
        mainPanel.setMinimumSize(new Dimension(800, dims.height + 200));

        validate();
    }

    public void removeTableFromDisplay(ExtendedTablePanel chart)
    {
        mainPanel.remove(chart);
        Dimension minDims = getMinimumSize();
        mainPanel.setMinimumSize(new Dimension(800, minDims.height - 200));
        validate();
        repaint();
    }

    public void addTableActionListener(ActionListener addChartAL)
    {
        addChartMenuItem.addActionListener(addChartAL);
        addChartButton.addActionListener(addChartAL);
    }

}
