//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
//</editor-fold>
package org.visnow.vn.lib.basic.viewers.TableViewer;

import java.util.Arrays;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.RegularField;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 * Class holding static methods for converting VisNow components to data series (string representation of a component).
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 * @author norkap
 */
public class TableViewerCore
{

    /**
     * Creates data series from a given component.
     *
     * @param da    component
     * @param inFld field
     *
     * @return data series
     */
    public static DataSet createSeries(DataArray da, RegularField inFld)
    {
        if (da == null || inFld == null) {
            throw new IllegalArgumentException("Null arguments are not supported.");
        }

        if (inFld.getDimNum() > 2) {
            throw new IllegalArgumentException("3D fields are not supported.");
        }

        long[] ldims = inFld.getLDims();
        int[] dims = new int[ldims.length];
        for (int i = 0; i < dims.length; i++) {
            if (ldims[i] >= Integer.MAX_VALUE) {
                throw new IllegalArgumentException("Dimensions of this field are too large.");
            } else {
                dims[i] = (int) ldims[i];
            }
        }
        
        LargeArray data;
        double[] stats;
        DataArraySchema schema = da.getSchema();
        if (da.isNumeric() && da.getType() != DataArrayType.FIELD_DATA_COMPLEX && !Arrays.equals(schema.getPhysicalMappingCoefficients(), new double[]{1., 0.})) {
            data = da.getPhysicalDoubleArray();
            stats = new double[]{da.getPhysMinValue(), schema.dataRawToPhys(da.getMeanValue()), da.getPhysMaxValue(), schema.dataRawToPhys(da.getStandardDeviationValue())};
        } else {
            data = da.getRawArray();
            stats = new double[]{da.getMinValue(), da.getMeanValue(), da.getMaxValue(), da.getStandardDeviationValue()};

        }
        return new DataSet(da.getName(), data, dims, da.getVectorLength(), stats);
        
    }

    /**
     * Creates data series from a given component.
     *
     * @param da         component
     * @param inFld      field
     * @param seriesName series name
     *
     * @return data series
     */
    public static DataSet createSeries(DataArray da, RegularField inFld, String seriesName)
    {
        DataArray daClone = da.cloneShallow();
        daClone.setName(seriesName);
        return createSeries(daClone, inFld);
    }

    private TableViewerCore()
    {

    }
}
