//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
//</editor-fold>
package org.visnow.vn.lib.basic.viewers.TableViewer.ExtendedTable;

import org.visnow.vn.gui.utils.DataSeriesChangedListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Map;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import org.visnow.vn.lib.basic.viewers.TableViewer.DataSet;
import org.visnow.vn.gui.utils.KeyListenerAdapter;

/**
 * Extended table.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 * @author norkap
 */
public class ExtendedTable implements DataSeriesChangedListener
{

    private final TableData data;
    private final ExtendedTablePanel tableGUI;

    public ExtendedTable(TableData data, ExtendedTablePanel tableGUI)
    {
        this.data = data;
        this.tableGUI = tableGUI;

        tableGUI.addSeriesListener(this);
        tableGUI.addRemoveSeriesListener(new RemoveSeriesListener());
        tableGUI.addSeriesFormatChangedListener(new SeriesFormatChangedListener());
        tableGUI.addKeyPressedOnSeriesTableListener(new KeyListenerAdapter()
        {

            @Override
            public void keyPressed(KeyEvent e)
            {
                if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                    removeSeries();
                }
            }
        });
    }

    public ExtendedTablePanel getTablePanel()
    {
        return tableGUI;
    }

    public void setSeries(Map<String, DataSet> series)
    {
        data.setSeries(series);
    }

    public void update()
    {
        data.updateDisplayedSeries();
        tableGUI.initGUIComponents(data.getSeriesNames());
        tableGUI.update(data.getDisplayedSeries());
        tableGUI.showChart();
    }

    private void addSeries(String[] names)
    {
        boolean res = true;
        boolean seriesAdded = false;
        if (data.getSeriesCount() > 0) {
            for (String name : names) {
                if (data.addDatasetToDisplayedSeries(name)) {
                    seriesAdded = true;
                }
            }
            if (seriesAdded) {
                tableGUI.update(data.getDisplayedSeries());
            }
        }
    }

    private void addSeries()
    {
        if (data.getSeriesCount() > 0 && data.addDatasetToDisplayedSeries(tableGUI.getSelectedComponentName())) {
            tableGUI.update(data.getDisplayedSeries());
        }
    }

    private void removeSeries()
    {
        String[] names = tableGUI.getRemovedSeriesNames();
        if (names != null) {
            data.removeDatasetsFromDisplayedSeries(names);
            tableGUI.update(data.getDisplayedSeries());
        }
    }
    
    
    private void updateSeriesFormat(FormatEditor fEditor)
    {
        String f = tableGUI.getSeriesFormat(fEditor);
        data.getDisplayedSeries().get(tableGUI.getSelectedSeriesName()).setFormat(f);
        tableGUI.updateTable(data.getDisplayedSeries().get(tableGUI.getSelectedSeriesName()));
    }

    @Override
    public void addSeriesToDisplay(String[] names)
    {
        addSeries(names);
    }

    class AddSeriesActionListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            addSeries();
        }

    }

    class RemoveSeriesListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            removeSeries();
        }

    }
    
    class SeriesFormatChangedListener implements CellEditorListener
    {

        @Override
        public void editingStopped(ChangeEvent e)
        {
            if (e.getSource() instanceof FormatEditor) {
                updateSeriesFormat((FormatEditor) e.getSource());
            }
        }

        @Override
        public void editingCanceled(ChangeEvent e)
        {

        }

    }

}
