/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.utilities.FieldStats;

import java.util.Arrays;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.utilities.FieldStats.FieldStatsShared.*;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.jscic.utils.DataArrayStatistics;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class FieldStats extends ModuleCore
{

    public static InputEgg[] inputEggs = null;
    protected GUI ui = new GUI();
    Field inField;

    public FieldStats()
    {

        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                ui.setParameters(parameters);
                setPanel(ui);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(LOG_SCALE, false),
            new Parameter<>(SELECTED_COMPONENT, 0),
            new Parameter<>(BIN_COUNT, 64),
            new Parameter<>(META_NAME, ""),
            new Parameter<>(META_COMPONENT_NAMES, new String[]{}),
            new Parameter<>(META_SCHEMA, null),
            new Parameter<>(META_NNODES, 0l),
            new Parameter<>(META_DIMS, new int[0]),
            new Parameter<>(META_EXTENDS, new float[2][3]),
            new Parameter<>(META_PHYS_EXTENDS, new float[2][3]),
            new Parameter<>(META_AVG_GRAD, new double[0]),
            new Parameter<>(META_STDDEV_GRAD, new double[0]),
            new Parameter<>(META_HISTOGRAM, new long[0][0]),
            new Parameter<>(META_DERIV_HISTOGRAM, new long[0][0]),
            new Parameter<>(META_THR_HISTOGRAM, new double[0][0]),
            new Parameter<>(META_IS_REGULAR_FIELD, false),
            new Parameter<>(META_IS_HAS_MASK_OR_IS_TIME_DEPENDENT, false)
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        parameters.set(META_COMPONENT_NAMES, inField.getComponentNames());
        parameters.set(META_NAME, inField.getName());
        parameters.set(META_EXTENDS, inField.getPreferredExtents());
        parameters.set(META_PHYS_EXTENDS, inField.getPreferredPhysicalExtents());
        parameters.set(META_COMPONENT_NAMES, inField.getComponentNames());
        parameters.set(META_SCHEMA, inField.getSchema());
        long[][] histograms = new long[inField.getNComponents()][];
        for (int i = 0; i < inField.getNComponents(); i++) {
            DataArray comp = inField.getComponent(i);
            histograms[i] = inField.getComponent(i).getHistogram(comp.getMinValue(), comp.getMaxValue(), 256, false, inField.getMask());
        }
        parameters.set(META_HISTOGRAM, histograms);
        parameters.set(META_IS_HAS_MASK_OR_IS_TIME_DEPENDENT, inField.isTimeDependant() || inField.hasMask());
        if (inField instanceof RegularField) {
            parameters.set(META_DIMS, ((RegularField) inField).getDims());
            parameters.set(META_AVG_GRAD, ((RegularField) inField).getAvgGrad());
            parameters.set(META_STDDEV_GRAD, ((RegularField) inField).getStdDevGrad());
            parameters.set(META_DERIV_HISTOGRAM, ((RegularField) inField).getDerivHistograms());
            parameters.set(META_THR_HISTOGRAM, ((RegularField) inField).getThrHistograms());
            parameters.set(META_IS_REGULAR_FIELD, true);

            parameters.set(META_NNODES, 0l);

        } else if (inField instanceof IrregularField) {
            parameters.set(META_NNODES, ((IrregularField) inField).getNNodes());
            parameters.set(META_IS_REGULAR_FIELD, false);

            parameters.set(META_DIMS, new int[0]);
            parameters.set(META_AVG_GRAD, new double[0]);
            parameters.set(META_STDDEV_GRAD, new double[0]);
            parameters.set(META_DERIV_HISTOGRAM, new long[0][0]);
            parameters.set(META_THR_HISTOGRAM, new double[0][0]);
        }
        if (resetParameters && inField.getNComponents() > 0) {
            parameters.set(LOG_SCALE, false);
            parameters.set(SELECTED_COMPONENT, 0);
        }
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        ui.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive()
    {

        if (getInputFirstValue("inField") != null) {
//            switchPanelToGUI();
            Field newField = ((VNField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA() && (inField == null || !Arrays.equals(inField.getComponentNames(), newField.getComponentNames()));
            boolean isNewField = !isFromVNA() && newField != inField;
            inField = newField;

            if (isDifferentField || isNewField) {
                DataContainerSchema schema = inField.getSchema();
                for (int i = 0; i < inField.getNComponents(); i++) {
                    inField.getComponent(i).recomputeStatistics(inField.getMask(), false);
                }
            }

            Parameters p;

            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentField);
                p = parameters.getReadOnlyClone();
            }

            notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

        }
//        else {
//            ui.removeAll();
//            switchPanelToDummy();
//        }
    }
}
