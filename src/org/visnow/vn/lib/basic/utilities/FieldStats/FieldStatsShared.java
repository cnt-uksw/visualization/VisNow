/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.utilities.FieldStats;

import org.visnow.jscic.DataContainerSchema;
import org.visnow.vn.engine.core.ParameterName;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class FieldStatsShared
{


    public static final ParameterName<Boolean> LOG_SCALE = new ParameterName("Log scale");
    public static final ParameterName<Integer> SELECTED_COMPONENT = new ParameterName("Selected component");
    public static final ParameterName<Integer> BIN_COUNT = new ParameterName("Number of bins");
    

    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic)    
    public static final ParameterName<String> META_NAME = new ParameterName("META field name");
    public static final ParameterName<String[]> META_COMPONENT_NAMES = new ParameterName("META component names");
    public static final ParameterName<DataContainerSchema> META_SCHEMA = new ParameterName("META field schema");
    public static final ParameterName<Long> META_NNODES = new ParameterName("META field nnodes");
    public static final ParameterName<int[]> META_DIMS = new ParameterName("META field dimensions");
    public static final ParameterName<float[][]> META_EXTENDS = new ParameterName("META field extends");
    public static final ParameterName<float[][]> META_PHYS_EXTENDS = new ParameterName("META field physical extends");
    public static final ParameterName<double[]> META_AVG_GRAD = new ParameterName("META field avgGrad");
    public static final ParameterName<double[]> META_STDDEV_GRAD = new ParameterName("META field stdDevGrad");
    public static final ParameterName<long[][]> META_HISTOGRAM = new ParameterName("META histogram");
    public static final ParameterName<long[][]> META_DERIV_HISTOGRAM = new ParameterName("META derivHistogram");
    public static final ParameterName<double[][]> META_THR_HISTOGRAM = new ParameterName("META thrHistogram");
    public static final ParameterName<Boolean> META_IS_REGULAR_FIELD = new ParameterName("META is regular field");
    public static final ParameterName<Boolean> META_IS_HAS_MASK_OR_IS_TIME_DEPENDENT = new ParameterName("META field has a mask or is time dependent");
}
