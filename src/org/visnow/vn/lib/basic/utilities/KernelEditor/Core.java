/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.utilities.KernelEditor;

import java.util.Arrays;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.VectorMath;
import org.visnow.vn.lib.basic.utilities.KernelEditor.KernelEditorShared.KernelType;

/**
 *
 * @author Piotr Wendykier (piotrw@icm.edu.pl)
 *
 */
public class Core
{

    private RegularField outField;

    public Core()
    {
    }

    public RegularField getOutField()
    {
        return outField;
    }

    public void update(int radius, int rank, float gaussianSigma, boolean normalized, KernelType type, float[] kernel)
    {

        int size = 2 * radius + 1;
        float[] data;
        int dimsSize = 1;

        if (rank == 1) {
            int[] dims = {size};
            outField = new RegularField(dims);
            dimsSize = size;
        } else if (rank == 2) {
            int[] dims = {size, size};
            outField = new RegularField(dims);
            dimsSize = size * size;
        } else if (rank == 3) {
            int[] dims = {size, size, size};
            outField = new RegularField(dims);
            dimsSize = size * size * size;
        }

        data = new float[dimsSize];

        float a = (size - 1) / 2.f;
        if (a < .5f) {
            a = .5f;
        }

        int i = 0;

        switch (rank) {
            case 1:
                if (type == KernelType.CONICAL) {
                    for (int x = 0; x < size; x++, i++) {
                        float u = abs(x - a) / a;
                        float t = u * u;
                        if (t > 1) {
                            t = 0;
                        } else {
                            t = 1 - (float) sqrt(1. * t);
                        }
                        data[i] = t;
                    }
                } else if (type == KernelType.LINEAR) {

                    for (int x = 0; x < size; x++, i++) {
                        float u = 1 - abs(x - a) / a;
                        data[i] = u;
                    }
                } else if (type == KernelType.GAUSSIAN) {
                    gaussian1D(data, gaussianSigma, radius, normalized);
                } else if (type == KernelType.CONSTANT) {

                    for (int x = 0; x < size; x++, i++) {
                        data[i] = 1.0f;

                    }
                } else if (type == KernelType.CUSTOM) {
                    if (data.length == kernel.length) {
                        System.arraycopy(kernel, 0, data, 0, data.length);
                    } else {
                        Arrays.fill(data, 0);
                    }
                }
                break;
            case 2:
                if (type == KernelType.CONICAL) {
                    for (int y = 0; y < size; y++) {
                        for (int x = 0; x < size; x++, i++) {
                            float u = abs(x - a) / a;
                            float v = abs(y - a) / a;

                            float t = u * u + v * v;

                            if (t > 1) {
                                t = 0;
                            } else {
                                t = 1 - (float) sqrt(1. * t);
                            }
                            data[i] = t;
                        }
                    }
                } else if (type == KernelType.LINEAR) {
                    for (int y = 0; y < size; y++) {
                        for (int x = 0; x < size; x++, i++) {
                            float u = 1 - abs(x - a) / a;
                            if (u > 1 - abs(y - a) / a) {
                                u = 1 - abs(y - a) / a;
                            }
                            data[i] = u;
                        }
                    }
                } else if (type == KernelType.GAUSSIAN) {
                    gaussian2D(data, gaussianSigma, radius, normalized);
                } else if (type == KernelType.CONSTANT) {
                    for (int y = 0; y < size; y++) {
                        for (int x = 0; x < size; x++, i++) {
                            data[i] = 1.0f;
                        }
                    }
                } else if (type == KernelType.CUSTOM) {
                    if (data.length == kernel.length) {
                        System.arraycopy(kernel, 0, data, 0, data.length);
                    } else {
                        Arrays.fill(data, 0);
                    }
                }
                break;
            case 3:
                if (type == KernelType.CONICAL) {
                    for (int z = 0; z < size; z++) {

                        for (int y = 0; y < size; y++) {
                            for (int x = 0; x < size; x++, i++) {
                                float u = abs(x - a) / a;
                                float v = abs(y - a) / a;
                                float w = abs(z - a) / a;
                                float t = u * u + v * v + w * w;
                                if (t > 1) {
                                    t = 0;
                                } else {
                                    t = 1 - (float) sqrt(1. * t);
                                }
                                data[i] = t;
                            }
                        }
                    }
                } else if (type == KernelType.LINEAR) {
                    for (int z = 0; z < size; z++) {

                        for (int y = 0; y < size; y++) {
                            for (int x = 0; x < size; x++, i++) {
                                float u = 1 - abs(x - a) / a;
                                if (u > 1 - abs(z - a) / a) {
                                    u = 1 - abs(z - a) / a;
                                }
                                data[i] = u;
                            }
                        }
                    }
                } else if (type == KernelType.GAUSSIAN) {
                    gaussian3D(data, gaussianSigma, radius, normalized);
                } else if (type == KernelType.CONSTANT) {
                    for (int z = 0; z < size; z++) {

                        for (int y = 0; y < size; y++) {
                            for (int x = 0; x < size; x++, i++) {
                                data[i] = 1.0f;
                            }
                        }
                    }
                } else if (type == KernelType.CUSTOM) {
                    if (data.length == kernel.length) {
                        System.arraycopy(kernel, 0, data, 0, data.length);
                    } else {
                        Arrays.fill(data, 0);
                    }
                }
                break;
        }
        if (normalized && type != KernelType.GAUSSIAN && type != KernelType.CUSTOM) {
            double data_sum = 0;

            for (int j = 0; j < data.length; j++) {
                data_sum += data[j];
            }

            if (data_sum != 0) {
                for (int j = 0; j < data.length; j++) {
                    data[j] = (float) (data[j] / data_sum);
                }
            }
        }

        outField.addComponent(DataArray.create(data, 1, "kernel"));
    }

    private static float computePDF(float mean, float sigma, float sample)
    {
        float SQRT_2_PI = (float) sqrt(2 * PI);
        float delta = sample - mean;
        return (float) exp(-delta * delta / (2.0 * sigma * sigma)) / (sigma * SQRT_2_PI);
    }

    public static void gaussian1D(float[] gaussian, float sigma, int radius, boolean normalize)
    {
        int index = 0;

        for (int i = radius; i >= -radius; i--) {
            gaussian[index++] = computePDF(0, sigma, i);
        }

        if (normalize) {
            VectorMath.vectorNormalizeToOne(gaussian, true);
        }
    }

    public static void gaussian2D(float[] gaussian, float sigma, int radius, boolean normalize)
    {
        float[] kernel1D = new float[2 * radius + 1];
        gaussian1D(kernel1D, sigma, radius, false);
        convolve2D(gaussian, kernel1D, kernel1D);
        if (normalize) {
            VectorMath.vectorNormalizeToOne(gaussian, true);
        }
    }

    private static void gaussian3D(float[] gaussian, float sigma, int radius, boolean normalize)
    {
        float[] kernel1D = new float[2 * radius + 1];
        gaussian1D(kernel1D, sigma, radius, false);
        convolve3D(gaussian, kernel1D, kernel1D, kernel1D);
        if (normalize) {
            VectorMath.vectorNormalizeToOne(gaussian, true);
        }
    }

    private static void convolve2D(float[] ret, float[] a, float[] b)
    {
        int w = a.length;

        int index = 0;
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < w; j++) {
                ret[index++] = a[i] * b[j];
            }
        }
    }

    private static void convolve3D(float[] ret, float[] a, float[] b, float[] c)
    {
        int w = a.length;

        int index = 0;
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < w; j++) {
                for (int k = 0; k < w; k++) {
                    ret[index++] = a[i] * b[j] * c[k];
                }
            }
        }
    }
}
