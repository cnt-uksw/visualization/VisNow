/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.utilities.Annotations;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.apache.log4j.Logger;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.geometries.parameters.FontParams;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.Pick3DEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.Pick3DListener;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.PickType;
import org.visnow.vn.lib.templates.visualization.modules.VisualizationModule;
import org.visnow.vn.lib.types.VNGeometryObject;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Annotations extends VisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(Annotations.class);
    /**
     * Creates a new instance of CreateGrid
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private Params params = new Params();
    private FontParams fontParams = new FontParams();
    private GUI ui = null;
    private AnnotationsObject glyphObj = new AnnotationsObject(params, fontParams);
    protected Pick3DListener pick3DListener = new Pick3DListener(PickType.POINT)
    {
        @Override
        public void handlePick3D(Pick3DEvent e)
        {
            if (params.isActivated() && e.getEvt().isShiftDown()) {
                float[] x = e.getPoint(); // x will always be not-null
                params.add(x, new String[] {String.format("[%.3f%n %.3f%n %.3f]", x[0], x[1], x[2])});
                ui.updateGUI();
                glyphObj.update(); 
            }
        }
    };
    
    public Annotations()
    {
        params.addParameterChangelistener(new ParameterChangeListener() {
            @Override
            public void parameterChanged(String name) {
                glyphObj.update(); 
                if (params.isOutput()) {
                    startAction();
                    params.clearOutput();
                }
                    
            }
        });
        fontParams.setDecoration(FontParams.Decoration.NONE);
        fontParams.setPosition(FontParams.Position.N);
        fontParams.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                glyphObj.update(); 
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                ui = new GUI();
                setPanel(ui);
                ui.setParams(params);
                ui.setFontParams(fontParams);
            }
        });
    }

    @Override
    public void onInitFinishedLocal()
    {
        outObj = glyphObj;
        outObj.setCreator(this);
        outObj.getGeometryObj().setUserData(new ModuleIdData(this));
        outObj2DStruct.setParentModulePort(this.getName() + ".out.outObj");
        setOutputValue("outObj", new VNGeometryObject(outObj, outObj2DStruct));
    }
    
    @Override
    public void onActive()
    {
        glyphObj.update(); 
        float[] coords = glyphObj.getTextCoords();
        if (coords == null || coords.length < 3)
            return;
        String[][] texts = glyphObj.getTexts();
        int n = coords.length / 3;
        IrregularField outField = new IrregularField(n);
        int[] ind = new int[n];
        for (int i = 0; i < ind.length; i++)
            ind[i] = i;
        CellArray pts = new CellArray(CellType.POINT, ind, null, null);
        CellSet cs = new CellSet("pts");
        cs.addCells(pts);
        outField.addCellSet(cs);
        outField.setCurrentCoords(new FloatLargeArray(coords));
        String[] outTexts = new String[n];
        for (int i = 0; i < texts.length; i++) {
            StringBuilder str = new StringBuilder();
            for (int j = 0; j < texts[i].length; j++) {
                if (j > 0)
                    str.append(";");
                str.append(texts[i][j].replaceAll("\n", "_;_"));
            }
            outTexts[i] = str.toString();
        }
        outField.addComponent(DataArray.create(outTexts, 1, "texts"));
        setOutputValue("annotationsField", new VNIrregularField(outField));
    } 

    @Override
    public Pick3DListener getPick3DListener() {
        return pick3DListener;
    }

}
