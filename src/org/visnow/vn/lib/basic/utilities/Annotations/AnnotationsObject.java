/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.utilities.Annotations;

import java.util.ArrayList;
import org.jogamp.java3d.*;
import org.visnow.vn.geometries.objects.DataMappedGeometryObject;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.parameters.FontParams;
import org.visnow.vn.geometries.textUtils.Texts2D;
import org.visnow.vn.geometries.utils.Texts3D;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import org.visnow.vn.geometries.viewer3d.Display3DPanel;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class AnnotationsObject extends DataMappedGeometryObject
{

    private Texts2D glyphs = null;
    private OpenBranchGroup outGroup = null;
    private final Params params;
    private final FontParams fontParams;

    public AnnotationsObject(Params params, FontParams fontParams)
    {
        name = "text glyphs";
        this.fontParams = fontParams;
        this.params = params;
    }

    @Override
    public void drawLocal2D(J3DGraphics2D vGraphics, LocalToWindow ltw, int width, int height)
    {
        glyphs.draw(vGraphics, ltw, width, height);
    }

    public void prepareGlyphs()
    {
        Display3DPanel panel = getCurrentViewer();
        if (panel == null)
            return;
        fontParams.createFontMetrics(localToWindow, panel.getWidth(), panel.getHeight());
        if (outGroup != null)
            outGroup.detach();
        outGroup = null;
        glyphs = null;
        if (params.getTexts() == null)
            return;
        ArrayList<String[]> fullTexts = params.getTexts();
        String[][] texts = new String[fullTexts.size()][1];
        String[] texts3 = new String[fullTexts.size()];
        for (int i = 0; i < texts.length; i++) {
            texts3[i] = texts[i][0] = fullTexts.get(i)[0];
        }
        float[] crds = new float[3 * params.getCoords().size()];
        for (int i = 0; i < params.getCoords().size(); i++)
            System.arraycopy(params.getCoords().get(i), 0, crds, 3 * i, 3);
        if (fontParams.isThreeDimensional()) {
            outGroup = new Texts3D(crds, texts3, fontParams);
            addNode(outGroup);
            setExtents(((Texts3D) outGroup).getExtents());
        } else {
            glyphs = new Texts2D(crds, texts, fontParams);
            geometryObj.getCurrentViewer().refresh();
        }
    }

    public String[][] getTexts() {
        return glyphs == null ? null : glyphs.getTexts();
    }

    public float[] getTextCoords() {
        return glyphs == null ? null : glyphs.getTextCoords();
    }
    
    public void update()
    {
        prepareGlyphs();
    }

}
