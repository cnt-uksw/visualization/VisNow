/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.utilities.Annotations;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.parameters.FontParams;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class Params extends Parameters
{
    protected FontParams fontParams = new FontParams();
    protected static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Boolean>("activated", ParameterType.dependent, true),
        new ParameterEgg<Boolean>("output", ParameterType.dependent, false),
        new ParameterEgg<ArrayList<float[]>>("coords", ParameterType.dependent, new ArrayList<>()),
        new ParameterEgg<ArrayList<String[]>>("texts", ParameterType.dependent, new ArrayList<>()),
    };

    public Params()
    {
        super(eggs);
        fontParams.addChangeListener(new ChangeListener()
        {

            @Override
            public void stateChanged(ChangeEvent e)
            {
                fireStateChanged();
            }
        });
    }
    
    public boolean isActivated()
    {
        return (Boolean)getValue("activated");
    }
    
    public void setActivated(boolean activated)
    {
        setValue("activated", activated);
    }
    
    
    public boolean isOutput()
    {
        return (Boolean)getValue("output");
    }
    
    public void output()
    {
        setValue("output", true);
        fireStateChanged();
    }
    
    
    public void clearOutput()
    {
        setValue("output", false);
    }

    public ArrayList<String[]> getTexts()
    {
        return (ArrayList<String[]>) getValue("texts");
    }

    public void setTexts(ArrayList<String[]> texts)
    {
        setValue("texts", texts);
        fireStateChanged();
    }
    
    public ArrayList<float[]> getCoords()
    {
        return (ArrayList<float[]>) getValue("coords");
    }

    public void setCoords(ArrayList<float[]> coords)
    {
        setValue("coords", coords);
        fireStateChanged();
    }
    
    public void add(float[] crds, String[] txts)
    {
        getCoords().add(crds);
        getTexts().add(txts);
        fireStateChanged();
    }
    
    public void remove(int i)
    {
        if (i >= 0 && i < getCoords().size()) {
            getCoords().remove(i);
            getTexts().remove(i);
            fireStateChanged();
        }
    }
    
    public void clear()
    {
        getCoords().clear();
        getTexts().clear();
        fireStateChanged();
    }
            
    @Override
    public void fireStateChanged()
    {
        if (!active)
            return;
        ChangeEvent e = new ChangeEvent(this);
        for (int i = 0; i < changeListenerList.size(); i++) {
            changeListenerList.get(i).stateChanged(e);
        }
    }

}
