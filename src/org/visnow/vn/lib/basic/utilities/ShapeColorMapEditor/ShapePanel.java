/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.utilities.ShapeColorMapEditor;

import org.visnow.vn.datamaps.colormap2d.shape.EllipseDrawModel;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Random;
import org.visnow.vn.datamaps.colormap2d.ShapeColorMap2D;
import org.visnow.vn.datamaps.colormap2d.shape.ShapeDrawModel;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ShapePanel extends javax.swing.JPanel
{

    private ShapeColorMap2D shapeColorMap2D;
    private BufferedImage backgroundImage;
    private int backgroundAlpha;

    public int getBackgroundAlpha()
    {
        return backgroundAlpha;
    }

    public void setBackgroundAlpha(int backgroundAlpha)
    {
        this.backgroundAlpha = backgroundAlpha;
        repaint();
    }

    public BufferedImage getBackgroundImage()
    {
        return backgroundImage;
    }

    public void setBackgroundImage(BufferedImage backgroundImage)
    {
        this.backgroundImage = backgroundImage;
        repaint();
    }

    public void setEllipses(ShapeColorMap2D ellipses)
    {
        this.shapeColorMap2D = ellipses;
        ellipses.propertyChangeSupport.addPropertyChangeListener(new PropertyChangeListener()
        {

            public void propertyChange(PropertyChangeEvent evt)
            {
                if (evt.getPropertyName().equals("add")) {
                    ShapePanel.this.addMouseMotionListener((EllipseDrawModel) evt.getNewValue());
                    ShapePanel.this.addMouseListener((EllipseDrawModel) evt.getNewValue());
                }
                ShapePanel.this.repaint();
            }
        });
    }

    /**
     * Creates new form EllipsePanel
     */
    public ShapePanel()
    {
        initComponents();

        addMouseMotionListener(new MouseAdapter()
        {

            @Override
            public void mouseMoved(MouseEvent e)
            {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        });
        addMouseListener(new MouseAdapter()
        {

            @Override
            public void mouseClicked(MouseEvent e)
            {

                if (e.getClickCount() > 1) {
                    Random r = new Random(e.getX());
                    if (shapeColorMap2D != null) {
                        Color c = new Color(r.nextInt(255), r.nextInt(255), r.nextInt(255));
                        shapeColorMap2D.add(new EllipseDrawModel("Name", e.getX(), 0, e.getY(), e.getY(), 10, c));
                        repaint();
                    }
                }

            }
        });

    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 400, Short.MAX_VALUE)
                );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 300, Short.MAX_VALUE)
                );
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void paint(Graphics g)
    {

        super.paint(g);

        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g.setColor(Color.black);
        g.fillRect(0, 0, getWidth(), getHeight());

        if (shapeColorMap2D != null) {
            for (ShapeDrawModel shape : shapeColorMap2D.getShapes()) {
                shape.paintShape(g2d, PaintMode.COLOR_ALPHA);
                shape.paintShapeOutline(g2d);
                shape.paintControlPoints(g);

            }
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
