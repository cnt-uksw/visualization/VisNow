/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.ReadImage;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.imageio.ImageTypeSpecifier;
import javax.swing.SwingUtilities;
import org.apache.log4j.Logger;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.datamaps.ColorMapManager;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.parameters.PresentationParams;
import org.visnow.vn.geometries.parameters.RenderingParams;
import static org.visnow.vn.lib.basic.readers.ReadImage.ReadImageShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.ImageUtils;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;

//TODO: add proper description here... (describe logic)
/**
 *
 * <p>
 * @author Bartosz Borucki (babor@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class ReadImage extends OutFieldVisualizationModule
{

    private static final Logger LOGGER = Logger.getLogger(ReadImage.class);
    private ReadImageComputeUI computeUI = null;
    public static OutputEgg[] outputEggs = null;
    private static final Logger log = Logger.getLogger(ReadImage.class);
    private Parameters previousParameters;

    /**
     * Creates a new instance of ReadImage
     */
    public ReadImage()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new ReadImageComputeUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILENAMES, new String[]{}),
            new Parameter<>(CONVERT_TO_GRAYSCALE, false),
            new Parameter<>(RETAIN_ORIGINAL, false),
            new Parameter<>(RGB_WEIGHTS, ColorMode.getWeights(ColorMode.BT709)),
            new Parameter<>(LAYERING_MODE, ReadImage.LayeringMode.IMAGES_AS_3RD_DIMENSION),
            new Parameter<>(AFFINE_SCALING_FACTORS, new float[]{1, 1, 1}),
            new Parameter<>(AFFINE_UNITS, new String[]{"1", "1", "1"}),
            new Parameter<>(META_FIELD_RANK, 3),
            new Parameter<>(META_FIELD_TIME_DEPENDENT, false)
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        parameters.set(META_FIELD_RANK, outRegularField != null ? outRegularField.getDimNum() : 3);
        parameters.set(META_FIELD_TIME_DEPENDENT, outRegularField != null ? outRegularField.isTimeDependant() : false);
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        ProgressAgent progressAgent = getProgressAgent(100);
        BufferedImage[] images = null;
        BufferedImage[] originalImages = null;
        String[] imageNames = null;
        int[][] imageDimensions = null;
        ImageTypeSpecifier[] imageTypes = null;

        LOGGER.debug("fromVNA: " + isFromVNA());

        Parameters p;
        synchronized (parameters) {
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, false, false);

        if (p.get(FILENAMES).length == 0) {
            outField = null;
            progressAgent.setProgress(1.0);
            show();
            setOutputValue("outRegularField", null);
            previousParameters = p;
            return;
        } else if (outRegularField != null && affineChangeOnly(p, previousParameters)) {
            updateAffine(outRegularField, p);
            setOutputValue("outRegularField", new VNRegularField(outRegularField));
            VisNow.get().userMessageSend(this, "<html>Affine successfully updated", outRegularField.toMultilineString(), Level.INFO);
         } else {
            int nfiles = p.get(FILENAMES).length;
            try {
                if (!(new File(p.get(FILENAMES)[0]).exists())) {
                    VisNow.get().userMessageSend(this, "Error loading image(s)", "File " + p.get(FILENAMES)[0] + " does not exist.", Level.ERROR);
                    outField = null;
                    progressAgent.setProgress(1.0);
                    show();
                    setOutputValue("outRegularField", null);
                    previousParameters = p;
                    return;
                }
                if (nfiles == 1 && ImageUtils.readNumImages(p.get(FILENAMES)[0]) > 1) {
                    String path = p.get(FILENAMES)[0];
                    File file = new File(path);
                    nfiles = ImageUtils.readNumImages(file);
                    imageDimensions = ImageUtils.readImageListDimensions(file);
                    imageTypes = ImageUtils.readImageListType(file);
                    imageNames = new String[nfiles];
                    for (int i = 0; i < nfiles; i++) {
                        if (!Arrays.equals(imageDimensions[0], imageDimensions[i])) {
                            VisNow.get().userMessageSend(this, "Error loading image(s): unequal dimensions.", generateTable(imageNames, imageDimensions, imageTypes), Level.ERROR);
                            outField = null;
                            progressAgent.setProgress(1.0);
                            show();
                            setOutputValue("outRegularField", null);
                            previousParameters = p;
                            return;
                        }
                        if (!p.get(CONVERT_TO_GRAYSCALE) && !imageTypes[0].equals(imageTypes[i])) {
                            VisNow.get().userMessageSend(this, "Error loading image(s): unequal colorspaces.", generateTable(imageNames, imageDimensions, imageTypes), Level.ERROR);
                            outField = null;
                            progressAgent.setProgress(1.0);
                            show();
                            setOutputValue("outRegularField", null);
                            previousParameters = p;
                            return;
                        }
                        imageNames[i] = file.getName();
                    }
                    images = ImageUtils.readImageList(file);
                    progressAgent.setProgress(0.5);
                    if (p.get(CONVERT_TO_GRAYSCALE) && p.get(RETAIN_ORIGINAL)) {
                        originalImages = new BufferedImage[nfiles];
                    }
                    for (int i = 0; i < nfiles; i++) {
                        if (p.get(CONVERT_TO_GRAYSCALE)) {
                            images[i] = ImageUtils.convertColorspaceToGray(images[i], p.get(RGB_WEIGHTS), false);
                            if (p.get(RETAIN_ORIGINAL)) {
                                switch (imageTypes[0].getBufferedImageType()) {
                                    case BufferedImage.TYPE_BYTE_GRAY:
                                        originalImages[i] = ImageUtils.convertColorspaceToGray(images[i], null, false);
                                        break;
                                    case BufferedImage.TYPE_USHORT_GRAY:
                                        originalImages[i] = ImageUtils.convertColorspaceToGray(images[i], null, true);
                                        break;
                                    default:
                                        originalImages[i] = ImageUtils.convertColorspaceToRGB(images[i]);
                                        break;
                                }
                            }
                        } else {
                            switch (imageTypes[0].getBufferedImageType()) {
                                case BufferedImage.TYPE_BYTE_GRAY:
                                    images[i] = ImageUtils.convertColorspaceToGray(images[i], null, false);
                                    break;
                                case BufferedImage.TYPE_USHORT_GRAY:
                                    images[i] = ImageUtils.convertColorspaceToGray(images[i], null, true);
                                    break;
                                default:
                                    images[i] = ImageUtils.convertColorspaceToRGB(images[i]);
                                    break;
                            }
                        }
                        progressAgent.setProgress(0.5 + 0.2 * (i / (double) nfiles));
                    }
                } else {
                    images = new BufferedImage[nfiles];
                    originalImages = new BufferedImage[nfiles];
                    imageNames = new String[nfiles];
                    imageDimensions = new int[nfiles][];
                    imageTypes = new ImageTypeSpecifier[nfiles];
                    boolean equalDimensions = true;
                    boolean equalColorspaces = true;

                    for (int i = 0; i < nfiles; i++) {
                        String path = p.get(FILENAMES)[i];
                        File file = new File(path);
                        if (!file.exists()) {
                            VisNow.get().userMessageSend(this, "Error loading image(s)", "File " + path + " does not exist.", Level.ERROR);
                            outField = null;
                            progressAgent.setProgress(1.0);
                            show();
                            setOutputValue("outRegularField", null);
                            previousParameters = p;
                            return;
                        }
                        try {
                            imageDimensions[i] = ImageUtils.readImageDimensions(path);
                            imageTypes[i] = ImageUtils.readImageType(path);
                        } catch (IOException ex) {
                            VisNow.get().userMessageSend(this, "Error loading image(s)", "Cannot read file " + path, Level.ERROR);
                            outField = null;
                            progressAgent.setProgress(1.0);
                            show();
                            setOutputValue("outRegularField", null);
                            previousParameters = p;
                            return;
                        }
                        imageNames[i] = file.getName();
                        if (i == 0) {
                            if (imageDimensions[0] == null || imageTypes[0] == null) {
                                VisNow.get().userMessageSend(this, "Error loading image(s)", "Cannot read file " + path, Level.ERROR);
                                outField = null;
                                progressAgent.setProgress(1.0);
                                show();
                                setOutputValue("outRegularField", null);
                                previousParameters = p;
                                return;
                            }
                        } else {
                            if (!Arrays.equals(imageDimensions[0], imageDimensions[i])) {
                                equalDimensions = false;
                            }
                            if (!imageTypes[0].equals(imageTypes[i])) {
                                equalColorspaces = false;
                            }
                        }
                    }

                    if (!equalDimensions) {
                        VisNow.get().userMessageSend(this, "Error loading image(s): unequal dimensions.", generateTable(imageNames, imageDimensions, imageTypes), Level.ERROR);
                        outField = null;
                        progressAgent.setProgress(1.0);
                        show();
                        setOutputValue("outRegularField", null);
                        previousParameters = p;
                        return;
                    } else if ((!p.get(CONVERT_TO_GRAYSCALE) && !equalColorspaces) || (p.get(CONVERT_TO_GRAYSCALE) && p.get(RETAIN_ORIGINAL) && !equalColorspaces)) {
                        VisNow.get().userMessageSend(this, "Error loading image(s): unequal colorspaces.", generateTable(imageNames, imageDimensions, imageTypes), Level.ERROR);
                        outField = null;
                        progressAgent.setProgress(1.0);
                        show();
                        setOutputValue("outRegularField", null);
                        previousParameters = p;
                        return;
                    }

                    for (int i = 0; i < nfiles; i++) {
                        String path = p.get(FILENAMES)[i];
                        BufferedImage tmp_img;
                        try {
                            tmp_img = ImageUtils.readImage(path);
                        } catch (IOException ex) {
                            VisNow.get().userMessageSend(this, "Error loading image(s)", "Cannot read file " + path, Level.ERROR);
                            outField = null;
                            progressAgent.setProgress(1.0);
                            show();
                            setOutputValue("outRegularField", null);
                            previousParameters = p;
                            return;
                        }
                        if (tmp_img == null) {
                            VisNow.get().userMessageSend(this, "Error loading image(s)", "Cannot read file " + path, Level.ERROR);
                            outField = null;
                            progressAgent.setProgress(1.0);
                            show();
                            setOutputValue("outRegularField", null);
                            previousParameters = p;
                            return;
                        }
                        if (p.get(CONVERT_TO_GRAYSCALE)) {
                            images[i] = ImageUtils.convertColorspaceToGray(tmp_img, p.get(RGB_WEIGHTS), false);
                            if (p.get(RETAIN_ORIGINAL)) {
                                switch (imageTypes[0].getBufferedImageType()) {
                                    case BufferedImage.TYPE_BYTE_GRAY:
                                        originalImages[i] = ImageUtils.convertColorspaceToGray(tmp_img, null, false);
                                        break;
                                    case BufferedImage.TYPE_USHORT_GRAY:
                                        originalImages[i] = ImageUtils.convertColorspaceToGray(tmp_img, null, true);
                                        break;
                                    default:
                                        originalImages[i] = ImageUtils.convertColorspaceToRGB(tmp_img);
                                        break;
                                }
                            }
                        } else {
                            switch (imageTypes[0].getBufferedImageType()) {
                                case BufferedImage.TYPE_BYTE_GRAY:
                                    images[i] = ImageUtils.convertColorspaceToGray(tmp_img, null, false);
                                    break;
                                case BufferedImage.TYPE_USHORT_GRAY:
                                    images[i] = ImageUtils.convertColorspaceToGray(tmp_img, null, true);
                                    break;
                                default:
                                    images[i] = ImageUtils.convertColorspaceToRGB(tmp_img);
                                    break;
                            }
                        }
                        progressAgent.setProgress(0.7 * (i / (double) nfiles));
                    }
                }
            } catch (IOException ex) {
                VisNow.get().userMessageSend(this, "Error loading image(s)", "Cannot read file " + p.get(FILENAMES)[0], Level.ERROR);
                outField = null;
                progressAgent.setProgress(1.0);
                show();
                setOutputValue("outRegularField", null);
                previousParameters = p;
                return;
            }
            outField = ImageUtils.bufferedImages2RegularField(images, true, p.get(LAYERING_MODE) == ReadImage.LayeringMode.IMAGES_AS_TIMEDATA, p.get(AFFINE_SCALING_FACTORS), p.get(AFFINE_UNITS));
            if (p.get(RETAIN_ORIGINAL)) {
                Field outFieldOriginal = ImageUtils.bufferedImages2RegularField(originalImages, true, p.get(LAYERING_MODE) == ReadImage.LayeringMode.IMAGES_AS_TIMEDATA, p.get(AFFINE_SCALING_FACTORS), p.get(AFFINE_UNITS));
                for (DataArray comp : outFieldOriginal.getComponents()) {
                    comp.setHasParent(false);
                    outField.addComponent(comp);
                }
            }
            outRegularField = (RegularField) outField;
            if (outRegularField == null) {
                VisNow.get().userMessageSend(this, "Error loading image(s)", "Cannot read file " + p.get(FILENAMES)[0], Level.ERROR);
                outField = null;
                progressAgent.setProgress(1.0);
                show();
                setOutputValue("outRegularField", null);
                previousParameters = p;
                return;
            }
            outRegularField.setName(imageNames[0] + (imageNames.length > 1 ? "+" : ""));
            VisNow.get().userMessageSend(this, "<html>Image(s) successfully loaded", outRegularField.toMultilineString(), Level.INFO);
            setOutputValue("outRegularField", new VNRegularField(outRegularField));
            synchronized (parameters) {
                validateParamsAndSetSmart(true);
                p = parameters.getReadOnlyClone();
            }
            notifyGUIs(p, false, false);
        }
        progressAgent.setProgress(0.9);
        prepareOutputGeometry();
        renderingParams.setShadingMode(RenderingParams.UNSHADED);
        for (PresentationParams csParams : presentationParams.getChildrenParams()) {
            csParams.getRenderingParams().setShadingMode(RenderingParams.UNSHADED);
        }
        List names = Arrays.asList(outRegularField.getComponentNames());
        if (names.indexOf("image_data_gray") == 0) {
            dataMappingParams.setColorMode(DataMappingParams.COLORMAPPED);
            dataMappingParams.getColorMap0().setMapIndex(ColorMapManager.COLORMAP1D_GRAY);
        } 
        ui.getPresentationGUI().getRenderingGUI().updateDataValuesFromParams();
        ui.getPresentationGUI().getDataMappingGUI().updateDataValuesFromParams();
        ui.getPresentationGUI().getDataMappingGUI().panelsUpdate();
        progressAgent.setProgress(1.0);
        previousParameters = p;
        show();
    }

    private boolean affineChangeOnly(Parameters current, Parameters previous)
    {
        return current != null && previous != null &&
            Arrays.equals(current.get(FILENAMES), previous.get(FILENAMES)) &&
            Objects.equals(current.get(CONVERT_TO_GRAYSCALE), previous.get(CONVERT_TO_GRAYSCALE)) &&
            Objects.equals(current.get(RETAIN_ORIGINAL), previous.get(RETAIN_ORIGINAL)) &&
            Arrays.equals(current.get(RGB_WEIGHTS), previous.get(RGB_WEIGHTS)) &&
            current.get(LAYERING_MODE).equals(previous.get(LAYERING_MODE));
    }

    private void updateAffine(RegularField f, Parameters p)
    {
        float[][] affine = f.getAffine();
        float[] scalingFactors = p.get(AFFINE_SCALING_FACTORS);
        affine[0][0] = scalingFactors[0];
        affine[1][1] = scalingFactors[1];
        affine[2][2] = scalingFactors[2];
        f.setAffine(affine);
        f.setCoordsUnits(p.get(AFFINE_UNITS));
    }

    private final static Map<Integer, String> colorSpaceNames = new HashMap<Integer, String>()
    {
        {
            put(BufferedImage.TYPE_CUSTOM, "CUSTOM");
            put(BufferedImage.TYPE_INT_RGB, "INT RGB");
            put(BufferedImage.TYPE_INT_ARGB, "INT ARGB");
            put(BufferedImage.TYPE_INT_ARGB_PRE, "INT ARGB PRE");
            put(BufferedImage.TYPE_INT_BGR, "INT BGR");
            put(BufferedImage.TYPE_3BYTE_BGR, "3BYTE BGR");
            put(BufferedImage.TYPE_4BYTE_ABGR, "4BYTE ABGR");
            put(BufferedImage.TYPE_4BYTE_ABGR_PRE, "4BYTE ABGR PRE");
            put(BufferedImage.TYPE_USHORT_565_RGB, "USHORT 565 RGB");
            put(BufferedImage.TYPE_USHORT_555_RGB, "USHORT 555 RGB");
            put(BufferedImage.TYPE_BYTE_GRAY, "BYTE GRAY");
            put(BufferedImage.TYPE_USHORT_GRAY, "USHORT GRAY");
            put(BufferedImage.TYPE_BYTE_BINARY, "BYTE BINARY");
            put(BufferedImage.TYPE_BYTE_INDEXED, "BYTE INDEXED");
        }
    };

    private static String generateTable(String[] imageNames, int[][] imageDimensions, ImageTypeSpecifier[] imageColorspaces)
    {
        StringBuilder s = new StringBuilder();
        s.append("<html><table><tr>");
        s.append("<td><b>File name</b></td><td><b>Dimensions</b></td><td><b>Color space</b></td></tr>");
        for (int i = 0; i < imageNames.length; i++) {
            String colorSpaceName = colorSpaceNames.get(imageColorspaces[i].getBufferedImageType());
            s.append("<tr>");
            s.append("<td>").append(imageNames[i]).append("</td><td>").append(imageDimensions[i][0]).append("x").append(imageDimensions[i][1]).append("</td><td>").append(colorSpaceName).append("</td><br>");
            s.append("</tr>");
        }
        return s.toString() + "</table></html>";
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    static enum LayeringMode
    {

        IMAGES_AS_3RD_DIMENSION("As 3rd dimension"),
        IMAGES_AS_TIMEDATA("As timedata");

        final String text;

        private LayeringMode(String text)
        {
            this.text = text;
        }

        @Override
        public String toString()
        {
            return text;
        }
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag())
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    computeUI.activateOpenDialog();
                }
            });
    }
}
