/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.ReadImage;

import java.util.HashMap;
import java.util.Map;

/**
 * Color modes used in ReadImage (UI and Params).
 * <p>
 * @author szpak
 */
public enum ColorMode
{
    ORIGINAL("Original"),
    BT709("Grayscale BT.709"),
    YIQ("Grayscale YIQ"),
    AVERAGE("Average grayscale"),
    CUSTOM("Custom grayscale");

    final String text;

    private ColorMode(String text)
    {
        this.text = text;
    }

    @Override
    public String toString()
    {
        return text;
    }

    private static Map<Enum, float[]> desaturateWeights = new HashMap<Enum, float[]>()
    {
        {
            put(ColorMode.BT709, new float[]{0.2126f, 0.7152f, 0.0722f});
            put(ColorMode.YIQ, new float[]{0.299f, 0.587f, 0.114f});
            put(ColorMode.AVERAGE, new float[]{1 / 3f, 1 / 3f, 1 / 3f});
        }
    };

    /**
     * Returns cloned weights for passed {@code colorMode}.
     */
    static float[] getWeights(ColorMode colorMode)
    {
        float[] weights = desaturateWeights.get(colorMode);
        if (weights != null) return weights.clone();
        else throw new IllegalArgumentException("Weight for " + colorMode.name() + " not found");
    }
}
