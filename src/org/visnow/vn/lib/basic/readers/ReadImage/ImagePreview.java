/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.ReadImage;

import javax.swing.*;
import java.beans.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import org.visnow.vn.lib.utils.ImageUtils;

public class ImagePreview extends JComponent implements PropertyChangeListener
{

    private static final long serialVersionUID = -5208218060785769738L;

    ImageIcon thumbnail = null;
    File file = null;

    public ImagePreview(JFileChooser fc)
    {
        setPreferredSize(new Dimension(200, 200));
        fc.addPropertyChangeListener(this);
    }

    public void loadImage()
    {
        if (file == null || !file.exists()) {
            thumbnail = null;
            return;
        }

        //Don't use createImageIcon (which is a wrapper for getResource)
        //because the image we're trying to load is probably not one
        //of this program's own resources.
        BufferedImage tmpImage;
        try {
            tmpImage = ImageUtils.readImage(file);
        } catch (IOException ex) {
            thumbnail = null;
            return;
        }
        if (tmpImage != null) {
            if (tmpImage.getWidth() >= tmpImage.getHeight()) {
                if (tmpImage.getWidth() > 200) {
                    thumbnail = new ImageIcon(tmpImage.getScaledInstance(200, -1, Image.SCALE_DEFAULT));
                } else { //no need to miniaturize
                    thumbnail = new ImageIcon(tmpImage.getScaledInstance(-1, -1, Image.SCALE_DEFAULT));
                }
            }
            else {
                if (tmpImage.getHeight() > 200) {
                    thumbnail = new ImageIcon(tmpImage.getScaledInstance(-1, 200, Image.SCALE_DEFAULT));
                } else { //no need to miniaturize
                    thumbnail = new ImageIcon(tmpImage.getScaledInstance(-1, -1, Image.SCALE_DEFAULT));
                }
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent e)
    {
        boolean update = false;
        String prop = e.getPropertyName();
        //If the directory changed, don't show an image.
        switch (prop) {
            case JFileChooser.DIRECTORY_CHANGED_PROPERTY:
                file = null;
                update = true;

                //If a file became selected, find out which one.
                break;
            case JFileChooser.SELECTED_FILE_CHANGED_PROPERTY:
                file = (File) e.getNewValue();
                update = true;
                break;
        }

        //Update the preview accordingly.
        if (update) {
            thumbnail = null;
            if (isShowing()) {
                loadImage();
                repaint();
            }
        }
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        if (thumbnail == null) {
            loadImage();
        }
        if (thumbnail != null) {
            int x = getWidth() / 2 - thumbnail.getIconWidth() / 2;
            int y = getHeight() / 2 - thumbnail.getIconHeight() / 2;

            if (y < 0) {
                y = 0;
            }

            if (x < 5) {
                x = 5;
            }
            thumbnail.paintIcon(this, g, x, y);
        }
    }
}
