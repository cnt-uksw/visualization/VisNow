/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.ReadImage;

import java.awt.CardLayout;
import java.io.File;
import java.util.Arrays;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.log4j.Logger;
import org.visnow.jscic.utils.UnitUtils;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.lib.basic.readers.ReadImage.ReadImage.LayeringMode;
import static org.visnow.vn.lib.basic.readers.ReadImage.ReadImageShared.*;
import org.visnow.vn.lib.utils.StringUtils;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.swing.FixedGridBagLayoutPanel;
import org.visnow.vn.system.swing.UIStyle;

/**
 * @author szpak, Bartosz Borucki (babor@icm.edu.pl) Warsaw University)
 */
public class ReadImageComputeUI extends FixedGridBagLayoutPanel
{

    private static final Logger LOGGER = Logger.getLogger(ReadImageComputeUI.class);

    private JFileChooser fileChooser;

    private Parameters parameters;
    private String lastPath = null;
    private int rank;
    private boolean timeDependent;

    private final FileNameExtensionFilter allImagesFilter = new FileNameExtensionFilter("All image files",
                                                                                        "bmp", "BMP", "gif", "GIF", "iff", "IFF", "jpg", "jpeg", "JPG", "JPEG",
                                                                                        "icns", "ICNS", "ico", "ICO", "cur", "CUR", "pnm", "PNM", "pgm",
                                                                                        "PGM", "pbm", "PBM", "ppm", "PPM", "pam", "PAM", "pcx", "PCX",
                                                                                        "pict", "PICT", "pct", "PCT", "png", "PNG", "psd", "PSD",
                                                                                        "hdr", "HDR", "sgi", "SGI", "tga", "TGA", "tif", "TIF", "tiff", "TIFF");
    private final FileNameExtensionFilter bmpImagesFilter = new FileNameExtensionFilter("BMP images (*.bmp)", "bmp", "BMP");
    private final FileNameExtensionFilter gifImagesFilter = new FileNameExtensionFilter("GIF images (*.gif)", "gif", "GIF");
    private final FileNameExtensionFilter iffImagesFilter = new FileNameExtensionFilter("IFF images (*.iff)", "iff", "IFF");
    private final FileNameExtensionFilter jpegImagesFilter = new FileNameExtensionFilter("JPEG images (*.jpg, *.jpeg)", "jpg", "jpeg", "JPG", "JPEG");
    private final FileNameExtensionFilter icnsImagesFilter = new FileNameExtensionFilter("ICNS images (*.icns)", "icns", "ICNS");
    private final FileNameExtensionFilter icoImagesFilter = new FileNameExtensionFilter("ICO images (*.ico, *.cur)", "ico", "ICO", "cur", "CUR");
    private final FileNameExtensionFilter pcxImagesFilter = new FileNameExtensionFilter("PCX images (*.pcx)", "pcx", "PCX");
    private final FileNameExtensionFilter pictImagesFilter = new FileNameExtensionFilter("PICT images (*.pict)", "pict", "PICT", "pct", "PCT");
    private final FileNameExtensionFilter pnmImagesFilter = new FileNameExtensionFilter("PNM images (*.pnm, *.pgm, *.pbm, *.ppm, *.pam)", "pnm", "PNM", "pgm", "PGM",
                                                                                        "pbm", "PBM", "ppm", "PPM", "pam", "PAM");
    private final FileNameExtensionFilter pngImagesFilter = new FileNameExtensionFilter("PNG images (*.png)", "png", "PNG");
    private final FileNameExtensionFilter psdImagesFilter = new FileNameExtensionFilter("PSD images (*.psd)", "psd", "PSD");
    private final FileNameExtensionFilter rgbeImagesFilter = new FileNameExtensionFilter("RGBE images (*.hdr)", "hdr", "HDR");
    private final FileNameExtensionFilter sgiImagesFilter = new FileNameExtensionFilter("SGI images (*.sgi)", "sgi", "SGI");
    private final FileNameExtensionFilter tgaImagesFilter = new FileNameExtensionFilter("TGA images (*.tga)", "tha", "TGA");
    private final FileNameExtensionFilter tiffImagesFilter = new FileNameExtensionFilter("TIFF images (*.tif, *.tiff)", "tif", "TIF", "tiff", "TIFF");

    /**
     * Creates new form ReadImageComputeUI
     */
    public ReadImageComputeUI()
    {
        initComponents();
        postInitComponents();

        fileChooser = new JFileChooser();
        fileChooser.addChoosableFileFilter(allImagesFilter);
        fileChooser.addChoosableFileFilter(bmpImagesFilter);
        fileChooser.addChoosableFileFilter(gifImagesFilter);
        fileChooser.addChoosableFileFilter(iffImagesFilter);
        fileChooser.addChoosableFileFilter(jpegImagesFilter);
        fileChooser.addChoosableFileFilter(icnsImagesFilter);
        fileChooser.addChoosableFileFilter(icoImagesFilter);
        fileChooser.addChoosableFileFilter(pcxImagesFilter);
        fileChooser.addChoosableFileFilter(pictImagesFilter);
        fileChooser.addChoosableFileFilter(pnmImagesFilter);
        fileChooser.addChoosableFileFilter(pngImagesFilter);
        fileChooser.addChoosableFileFilter(psdImagesFilter);
        fileChooser.addChoosableFileFilter(rgbeImagesFilter);
        fileChooser.addChoosableFileFilter(sgiImagesFilter);
        fileChooser.addChoosableFileFilter(tgaImagesFilter);
        fileChooser.addChoosableFileFilter(tiffImagesFilter);

        fileChooser.setLocation(0, 0);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setFileFilter(allImagesFilter);
        fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setAccessory(new ImagePreview(fileChooser));
    }

    void setParameters(Parameters parameters)
    {
        this.parameters = parameters;
    }

    private void postInitComponents()
    {
        colorMode.setListData(ColorMode.values());
        layeringMode.setListData(LayeringMode.values());

        updateRatioPanel();
    }

    protected void updateGUI(final ParameterProxy p)
    {
        layeringMode.setSelectedItem(p.get(LAYERING_MODE));
        float[] ratios = p.get(RGB_WEIGHTS);
        setRatiosNTF(ratios);
        ColorMode cm = null;
        if (!p.get(CONVERT_TO_GRAYSCALE)) {
            cm = ColorMode.ORIGINAL;
        } else {
            for (ColorMode c : ColorMode.values()) {
                if (c != ColorMode.ORIGINAL && c != ColorMode.CUSTOM &&
                    Arrays.toString(ratios).equals(Arrays.toString(ColorMode.getWeights(c)))) {
                    cm = c;
                    break;
                }
            }
            if (cm == null) {
                cm = ColorMode.CUSTOM;
            }
        }

        colorMode.setSelectedItem(cm);
        filenameTF.setText(joinFilenames(p.get(FILENAMES)));

        updateRatioPanel();
        
        timeDependent = p.get(META_FIELD_TIME_DEPENDENT);
        rank = p.get(META_FIELD_RANK);
        if(!automaticScalingCB.isSelected()) {
            zNTF.setEnabled(rank == 3);
            zUnitsTF.setEnabled(rank == 3);
        }
        layeringMode.setEnabled(rank == 3 || timeDependent);
    }

    private boolean areValidAndCompatibleLengthUnits(String xUnit, String yUnit, String zUnit, boolean checkZUnit)
    {
        if (xUnit.equals("1") && yUnit.equals("1") && zUnit.equals("1")) {
            return true;
        }
        //All must be units of length, so we check if they are compatible with meters. 
        if(!UnitUtils.areValidAndCompatibleUnits(xUnit, "m")) {
            return false;
        }
        if(!UnitUtils.areValidAndCompatibleUnits(yUnit, "m")) {
            return false;
        }
        if (checkZUnit && !UnitUtils.areValidAndCompatibleUnits(zUnit, "m")) {
            return false;
        }
        return true;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        titledSeparator1 = new org.visnow.vn.gui.widgets.SectionHeader();
        browsePanel = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        filenameTF = new org.visnow.vn.gui.swingwrappers.TextField();
        browseButton = new javax.swing.JButton();
        colorOptionsPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        colorMode = new org.visnow.vn.gui.widgets.WidePopupComboBox();
        rgbRatiosCards = new javax.swing.JPanel();
        rgbRatioPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        redNTF = new org.visnow.vn.gui.components.NumericTextField();
        jLabel5 = new javax.swing.JLabel();
        greenNTF = new org.visnow.vn.gui.components.NumericTextField();
        jLabel6 = new javax.swing.JLabel();
        blueNTF = new org.visnow.vn.gui.components.NumericTextField();
        hideRGBPanel = new javax.swing.JPanel();
        retainOriginal = new javax.swing.JCheckBox();
        titledSeparator2 = new org.visnow.vn.gui.widgets.SectionHeader();
        readOptionsPanel = new javax.swing.JPanel();
        layeringMode = new org.visnow.vn.gui.widgets.WidePopupComboBox();
        jLabel4 = new javax.swing.JLabel();
        automaticScalingCB = new javax.swing.JCheckBox();
        xL = new javax.swing.JLabel();
        xNTF = new org.visnow.vn.gui.components.NumericTextField();
        yL = new javax.swing.JLabel();
        yNTF = new org.visnow.vn.gui.components.NumericTextField();
        zL = new javax.swing.JLabel();
        zNTF = new org.visnow.vn.gui.components.NumericTextField();
        xUnitsTF = new javax.swing.JTextField();
        yUnitsTF = new javax.swing.JTextField();
        zUnitsTF = new javax.swing.JTextField();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        titledSeparator1.setExpandable(false);
        titledSeparator1.setOpaque(false);
        titledSeparator1.setShowCheckBox(false);
        titledSeparator1.setShowIcon(false);
        titledSeparator1.setText("Input");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        add(titledSeparator1, gridBagConstraints);

        browsePanel.setLayout(new java.awt.GridBagLayout());

        jLabel7.setText("File(s):");
        browsePanel.add(jLabel7, new java.awt.GridBagConstraints());

        filenameTF.setToolTipText("Browse image files");
        filenameTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                filenameTFUserChangeAction(evt);
            }
        });
        filenameTF.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusGained(java.awt.event.FocusEvent evt)
            {
                filenameTFFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        browsePanel.add(filenameTF, gridBagConstraints);

        browseButton.setText("...");
        browseButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                browseButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        browsePanel.add(browseButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 8, 0, 4);
        add(browsePanel, gridBagConstraints);

        colorOptionsPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel3.setText("Color mode:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        colorOptionsPanel1.add(jLabel3, gridBagConstraints);

        colorMode.setToolTipText("<html>Keep original color or convert color to grayscale according to specified luminance coefficients.");
        colorMode.setListData(ColorMode.values());
        colorMode.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                colorModeUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        colorOptionsPanel1.add(colorMode, gridBagConstraints);

        rgbRatiosCards.setLayout(new java.awt.CardLayout());

        rgbRatioPanel.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("R:");
        rgbRatioPanel.add(jLabel1, new java.awt.GridBagConstraints());

        redNTF.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        redNTF.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        redNTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                ratiosUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 0);
        rgbRatioPanel.add(redNTF, gridBagConstraints);

        jLabel5.setText("G:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        rgbRatioPanel.add(jLabel5, gridBagConstraints);

        greenNTF.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        greenNTF.setText("1.0");
        greenNTF.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        greenNTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                ratiosUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 0);
        rgbRatioPanel.add(greenNTF, gridBagConstraints);

        jLabel6.setText("B:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        rgbRatioPanel.add(jLabel6, gridBagConstraints);

        blueNTF.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        blueNTF.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        blueNTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                ratiosUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 0);
        rgbRatioPanel.add(blueNTF, gridBagConstraints);

        rgbRatiosCards.add(rgbRatioPanel, "card2");
        rgbRatiosCards.add(hideRGBPanel, "card3");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(4, 8, 0, 0);
        colorOptionsPanel1.add(rgbRatiosCards, gridBagConstraints);

        retainOriginal.setText("Retain original");
        retainOriginal.setEnabled(false);
        retainOriginal.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                retainOriginalActionPerformed(evt);
            }
        });
        colorOptionsPanel1.add(retainOriginal, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 8, 0, 4);
        add(colorOptionsPanel1, gridBagConstraints);

        titledSeparator2.setExpandable(false);
        titledSeparator2.setOpaque(false);
        titledSeparator2.setShowCheckBox(false);
        titledSeparator2.setShowIcon(false);
        titledSeparator2.setText("Image sequence options");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(12, 2, 0, 0);
        add(titledSeparator2, gridBagConstraints);

        readOptionsPanel.setLayout(new java.awt.GridBagLayout());

        layeringMode.setListData(LayeringMode.values());
        layeringMode.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                layeringModeUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        readOptionsPanel.add(layeringMode, gridBagConstraints);

        jLabel4.setText("Layers:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        readOptionsPanel.add(jLabel4, gridBagConstraints);

        automaticScalingCB.setSelected(true);
        automaticScalingCB.setText("Automatic scaling factors and units");
        automaticScalingCB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                automaticScalingCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        readOptionsPanel.add(automaticScalingCB, gridBagConstraints);
        automaticScalingCB.getAccessibleContext().setAccessibleName("automaticScaling");

        xL.setText("X:");
        xL.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        readOptionsPanel.add(xL, gridBagConstraints);

        xNTF.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        xNTF.setText("1.0");
        xNTF.setEnabled(false);
        xNTF.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        xNTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                xNTFratiosUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        readOptionsPanel.add(xNTF, gridBagConstraints);

        yL.setText("Y:");
        yL.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        readOptionsPanel.add(yL, gridBagConstraints);

        yNTF.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        yNTF.setText("1.0");
        yNTF.setEnabled(false);
        yNTF.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        yNTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                yNTFratiosUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        readOptionsPanel.add(yNTF, gridBagConstraints);

        zL.setText("Z:");
        zL.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        readOptionsPanel.add(zL, gridBagConstraints);

        zNTF.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        zNTF.setText("1.0");
        zNTF.setEnabled(false);
        zNTF.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        zNTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                zNTFratiosUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        readOptionsPanel.add(zNTF, gridBagConstraints);

        xUnitsTF.setText("1");
        xUnitsTF.setToolTipText("Unit for X coordinate");
        xUnitsTF.setEnabled(false);
        xUnitsTF.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                xUnitsTFActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        readOptionsPanel.add(xUnitsTF, gridBagConstraints);

        yUnitsTF.setText("1");
        yUnitsTF.setToolTipText("Unit for Y coordinate");
        yUnitsTF.setEnabled(false);
        yUnitsTF.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                yUnitsTFActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        readOptionsPanel.add(yUnitsTF, gridBagConstraints);

        zUnitsTF.setText("1");
        zUnitsTF.setToolTipText("Unit for Z coordinate");
        zUnitsTF.setEnabled(false);
        zUnitsTF.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                zUnitsTFActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        readOptionsPanel.add(zUnitsTF, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 8, 0, 4);
        add(readOptionsPanel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(filler1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    protected void activateOpenDialog()
    {
        browseButtonActionPerformed(null);
    }

    private void browseButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_browseButtonActionPerformed
    {//GEN-HEADEREND:event_browseButtonActionPerformed

        fileChooser.setCurrentDirectory(lastPath != null ? new File(lastPath) : new File(VisNow.get().getMainConfig().getUsableDataPath(ReadImage.class)));

        int returnVal = fileChooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File[] files = fileChooser.getSelectedFiles();

            lastPath = files[0].getParent();
            VisNow.get().getMainConfig().setLastDataPath(lastPath, ReadImage.class);

            String[] absolutePaths = new String[files.length];
            for (int i = 0; i < files.length; i++) {
                absolutePaths[i] = files[i].getAbsolutePath();
            }

            StringUtils.sortFilenameList(absolutePaths);

            filenameTF.setText(joinFilenames(absolutePaths));

            parameters.set(FILENAMES, absolutePaths);
        }
    }//GEN-LAST:event_browseButtonActionPerformed

    private String joinFilenames(String[] filenames)
    {
        String filenamesString = "";
        for (int i = 0; i < filenames.length; i++) {
            if (i != 0) {
                filenamesString += File.pathSeparator;
            }
            filenamesString += filenames[i];
        }
        return filenamesString;
    }

    private void updateRatioPanel()
    {
        ColorMode cm = (ColorMode) colorMode.getSelectedItem();
        if (cm == ColorMode.ORIGINAL) {
            ((CardLayout) rgbRatiosCards.getLayout()).last(rgbRatiosCards);
        } else {
            float[] ratios = null;
            if (cm != ColorMode.CUSTOM) {
                ratios = ColorMode.getWeights(cm);
                setRatiosNTF(ratios);
            }
            ((CardLayout) rgbRatiosCards.getLayout()).first(rgbRatiosCards);
        }
    }

    private void setRatiosNTF(float[] ratios)
    {
        redNTF.setValue(ratios[0], "%4.3f");
        greenNTF.setValue(ratios[1], "%4.3f");
        blueNTF.setValue(ratios[2], "%4.3f");
    }

    private void colorModeUserChangeAction(java.util.EventObject evt)//GEN-FIRST:event_colorModeUserChangeAction
    {//GEN-HEADEREND:event_colorModeUserChangeAction
        //flow is: get colorMode from ComboBox -> set red/green/blue TextFields -> getValue -> pass to param
        updateRatioPanel();
        boolean original = ((ColorMode) colorMode.getSelectedItem()).equals(ColorMode.ORIGINAL);
        if (!original) {
            retainOriginal.setEnabled(true);
            parameters.set(RGB_WEIGHTS, new float[]{(float) redNTF.getValue(), (float) greenNTF.getValue(), (float) blueNTF.getValue()},
                           CONVERT_TO_GRAYSCALE, !original, RETAIN_ORIGINAL, retainOriginal.isSelected());
        } else {
            retainOriginal.setSelected(false);
            retainOriginal.setEnabled(false);
            parameters.set(RETAIN_ORIGINAL, false, CONVERT_TO_GRAYSCALE, !original);
        }
    }//GEN-LAST:event_colorModeUserChangeAction

    private void ratiosUserChangeAction(java.util.EventObject evt)//GEN-FIRST:event_ratiosUserChangeAction
    {//GEN-HEADEREND:event_ratiosUserChangeAction
        colorMode.setSelectedItem(ColorMode.CUSTOM);
        parameters.set(RGB_WEIGHTS, new float[]{(float) redNTF.getValue(), (float) greenNTF.getValue(), (float) blueNTF.getValue()},
                       CONVERT_TO_GRAYSCALE, true);
    }//GEN-LAST:event_ratiosUserChangeAction

    private void filenameTFFocusGained(java.awt.event.FocusEvent evt)//GEN-FIRST:event_filenameTFFocusGained
    {//GEN-HEADEREND:event_filenameTFFocusGained
        filenameTF.selectAll();
    }//GEN-LAST:event_filenameTFFocusGained

    private void filenameTFUserChangeAction(java.util.EventObject evt)//GEN-FIRST:event_filenameTFUserChangeAction
    {//GEN-HEADEREND:event_filenameTFUserChangeAction
        String filenamesJoined = filenameTF.getText();
        String[] filenames = filenamesJoined.split("\\" + File.pathSeparatorChar);
        LOGGER.debug("filenames = " + Arrays.toString(filenames));
        if (filenames.length == 1 && filenames[0].equals("")) {
            parameters.set(FILENAMES, new String[0]);
        } else {
            parameters.set(FILENAMES, filenames);
        }
    }//GEN-LAST:event_filenameTFUserChangeAction

    private void retainOriginalActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_retainOriginalActionPerformed
    {//GEN-HEADEREND:event_retainOriginalActionPerformed
        parameters.set(RETAIN_ORIGINAL, retainOriginal.isSelected());
    }//GEN-LAST:event_retainOriginalActionPerformed

    private void parseAndSetAffineFactorsAndUnits()
    {
        String xu = xUnitsTF.getText().trim();
        String yu = yUnitsTF.getText().trim();
        String zu = zUnitsTF.getText().trim();
        if (!areValidAndCompatibleLengthUnits(xu, yu, zu, zUnitsTF.isEnabled())) {
            xu = yu = zu = "1";
            xUnitsTF.setText(xu);
            yUnitsTF.setText(yu);
            zUnitsTF.setText(zu);
        }
        parameters.set(AFFINE_UNITS, new String[]{xu, yu, zu}, AFFINE_SCALING_FACTORS, new float[]{(float) xNTF.getValue(), (float) yNTF.getValue(), (float) zNTF.getValue()});
    }

    private void xNTFratiosUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_xNTFratiosUserChangeAction
        parseAndSetAffineFactorsAndUnits();
    }//GEN-LAST:event_xNTFratiosUserChangeAction

    private void yNTFratiosUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_yNTFratiosUserChangeAction
        parseAndSetAffineFactorsAndUnits();
    }//GEN-LAST:event_yNTFratiosUserChangeAction

    private void zNTFratiosUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_zNTFratiosUserChangeAction
        parseAndSetAffineFactorsAndUnits();
    }//GEN-LAST:event_zNTFratiosUserChangeAction

    private void layeringModeUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_layeringModeUserChangeAction
        parameters.set(LAYERING_MODE, (LayeringMode) layeringMode.getSelectedItem());
    }//GEN-LAST:event_layeringModeUserChangeAction

    private void automaticScalingCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_automaticScalingCBActionPerformed
        if (automaticScalingCB.isSelected()) {
            xL.setEnabled(false);
            yL.setEnabled(false);
            zL.setEnabled(false);
            xNTF.setEnabled(false);
            yNTF.setEnabled(false);
            zNTF.setEnabled(false);
            xUnitsTF.setEnabled(false);
            yUnitsTF.setEnabled(false);
            zUnitsTF.setEnabled(false);
            xNTF.setValue(1);
            yNTF.setValue(1);
            zNTF.setValue(1);
            xUnitsTF.setText("1");
            yUnitsTF.setText("1");
            zUnitsTF.setText("1");
            parseAndSetAffineFactorsAndUnits();
        } else {
            xL.setEnabled(true);
            yL.setEnabled(true);
            zL.setEnabled(true);
            xNTF.setEnabled(true);
            yNTF.setEnabled(true);
            zNTF.setEnabled(rank == 3);
            xUnitsTF.setEnabled(true);
            yUnitsTF.setEnabled(true);
            zUnitsTF.setEnabled(rank == 3);
        }
    }//GEN-LAST:event_automaticScalingCBActionPerformed


    private void xUnitsTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xUnitsTFActionPerformed
        parseAndSetAffineFactorsAndUnits();
    }//GEN-LAST:event_xUnitsTFActionPerformed

    private void yUnitsTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_yUnitsTFActionPerformed
        parseAndSetAffineFactorsAndUnits();
    }//GEN-LAST:event_yUnitsTFActionPerformed

    private void zUnitsTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zUnitsTFActionPerformed
        parseAndSetAffineFactorsAndUnits();
    }//GEN-LAST:event_zUnitsTFActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox automaticScalingCB;
    private org.visnow.vn.gui.components.NumericTextField blueNTF;
    private javax.swing.JButton browseButton;
    private javax.swing.JPanel browsePanel;
    private org.visnow.vn.gui.widgets.WidePopupComboBox colorMode;
    private javax.swing.JPanel colorOptionsPanel1;
    private org.visnow.vn.gui.swingwrappers.TextField filenameTF;
    private javax.swing.Box.Filler filler1;
    private org.visnow.vn.gui.components.NumericTextField greenNTF;
    private javax.swing.JPanel hideRGBPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private org.visnow.vn.gui.widgets.WidePopupComboBox layeringMode;
    private javax.swing.JPanel readOptionsPanel;
    private org.visnow.vn.gui.components.NumericTextField redNTF;
    private javax.swing.JCheckBox retainOriginal;
    private javax.swing.JPanel rgbRatioPanel;
    private javax.swing.JPanel rgbRatiosCards;
    private org.visnow.vn.gui.widgets.SectionHeader titledSeparator1;
    private org.visnow.vn.gui.widgets.SectionHeader titledSeparator2;
    private javax.swing.JLabel xL;
    private org.visnow.vn.gui.components.NumericTextField xNTF;
    private javax.swing.JTextField xUnitsTF;
    private javax.swing.JLabel yL;
    private org.visnow.vn.gui.components.NumericTextField yNTF;
    private javax.swing.JTextField yUnitsTF;
    private javax.swing.JLabel zL;
    private org.visnow.vn.gui.components.NumericTextField zNTF;
    private javax.swing.JTextField zUnitsTF;
    // End of variables declaration//GEN-END:variables
    public static void main(String[] a)
    {
        VisNow.initLogging(true);
        VisNow.mockVisNow();
        UIStyle.initStyle();
        System.out.println(VisNow.get().getMainConfig());

        final JFrame f = new JFrame();
        f.add(new ReadImageComputeUI());
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.setLocation(600, 300);
        f.pack();
        f.setVisible(true);

    }
}
