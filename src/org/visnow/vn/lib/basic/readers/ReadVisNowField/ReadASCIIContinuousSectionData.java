/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.basic.readers.ReadVisNowField;

import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Scanner;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.LongLargeArray;
import org.visnow.jlargearrays.ShortLargeArray;
import org.visnow.jlargearrays.StringLargeArray;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FileSectionSchema;
import org.visnow.vn.lib.utils.io.VNIOException;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ReadASCIIContinuousSectionData
{

    public static int readSectionData(SectionModel model, Scanner scanner, String filePath, String decimalSep, String entrySep)
            throws VNIOException
    {
        int nComps = model.nItems;
        int nNodes = (int)model.nData;
        int[] offsets = model.offsets;
        DataArrayType[] types = model.types;
        int[] vlens = model.vlens;
        long[] ind = model.ind;
        LogicLargeArray[] boolArrs =        model.boolArrs;
        UnsignedByteLargeArray[] byteArrs = model.byteArrs;
        ShortLargeArray[] shortArrs =       model.shortArrs;
        IntLargeArray[] intArrs =           model.intArrs;
        LongLargeArray[] longArrs =         model.longArrs;
        FloatLargeArray[] floatArrs =       model.floatArrs;
        DoubleLargeArray[] dblArrs =        model.dblArrs;
        ComplexFloatLargeArray[] cplxArrs = model.cplxArrs;
        StringLargeArray[] strArrs =        model.strArrs;
        FileSectionSchema schema = model.sectionSchema;
        if (",".equals(decimalSep)) {
            scanner.useLocale(Locale.FRENCH);
        } else {
            scanner.useLocale(Locale.US);
        }
        try {
            if (schema.getComponents().isEmpty()) {
                return 0;
            }
            String entrySeparator = entrySep;
            if (entrySeparator != null && !entrySeparator.isEmpty()) {
                scanner.useDelimiter("\\s*" + entrySeparator + "\\s*");
            }
            for (int k = 0; k < nNodes; k++) {
                int cPos = 0;
                for (int l = 0; l < nComps; l++) {
                    int ll = offsets[l] - cPos;
                    cPos = offsets[l] + 1;
                    for (int i = 0; i < ll; i++) {
                        scanner.next();
                    }
                    switch (types[l]) {
                        case FIELD_DATA_LOGIC:
                            String s = scanner.next();
                            boolArrs[l].setBoolean(ind[l], s.startsWith("1") || s.startsWith("t"));
                            ind[l] += vlens[l];
                            break;
                        case FIELD_DATA_BYTE:
                            byteArrs[l].setByte(ind[l], (byte) (0xff & scanner.nextInt()));
                            ind[l] += vlens[l];
                            break;
                        case FIELD_DATA_SHORT:
                            shortArrs[l].setShort(ind[l], scanner.nextShort());
                            ind[l] += vlens[l];
                            break;
                        case FIELD_DATA_INT:
                            intArrs[l].setInt(ind[l], scanner.nextInt());
                            ind[l] += vlens[l];
                            break;
                        case FIELD_DATA_LONG:
                            longArrs[l].setLong(ind[l], scanner.nextLong());
                            ind[l] += vlens[l];
                            break;
                        case FIELD_DATA_FLOAT:
                            floatArrs[l].setFloat(ind[l], scanner.nextFloat());
                            ind[l] += vlens[l];
                            break;
                        case FIELD_DATA_DOUBLE:
                            dblArrs[l].setDouble(ind[l], scanner.nextDouble());
                            ind[l] += vlens[l];
                            break;
                        case FIELD_DATA_COMPLEX:
                            cplxArrs[l].setComplexFloat(ind[l], new float[] {scanner.nextFloat(), scanner.nextFloat()});
                            break;
                        case FIELD_DATA_STRING:
                            strArrs[l].set(ind[l], scanner.next().replaceAll("_;_", "\n"));
                            break;
                    }
                }
            }
            return 0;
        }
        catch (NoSuchElementException e) {
            throw new VNIOException("premature end of file ", filePath, 0, model.sectionSchema.getHeaderFile(), model.sectionSchema.getHeaderLine());
        }
        catch (Exception e) {
            throw new VNIOException("error in data file ", filePath, 0, model.sectionSchema.getHeaderFile(), model.sectionSchema.getHeaderLine());
        }
    }
}
