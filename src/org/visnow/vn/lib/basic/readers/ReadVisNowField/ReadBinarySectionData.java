/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.basic.readers.ReadVisNowField;

import javax.imageio.stream.ImageInputStream;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.ShortLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.LongLargeArray;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FileSectionSchema;
import org.visnow.vn.lib.utils.io.VNIOException;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ReadBinarySectionData
{

    public static int readSectionData(SectionModel model, ImageInputStream inStream, String filePath) throws VNIOException
    {
        int nComps = model.nItems;
        long nNodes = model.nData;
        int[] offsets = model.offsets;
        DataArrayType[] types = model.types;
        int[] vlens = model.vlens;
        long[] ind = model.ind;
        LogicLargeArray[] boolArrs =        model.boolArrs;
        UnsignedByteLargeArray[] byteArrs = model.byteArrs;
        ShortLargeArray[] shortArrs =       model.shortArrs;
        IntLargeArray[] intArrs =           model.intArrs;
        LongLargeArray[] longArrs =         model.longArrs;
        FloatLargeArray[] floatArrs =       model.floatArrs;
        DoubleLargeArray[] dblArrs =        model.dblArrs;
        ComplexFloatLargeArray[] cplxArrs = model.cplxArrs;
        FileSectionSchema schema = model.sectionSchema;
        try {
            if (schema.getComponents().isEmpty())
                return 0;
            if (schema.getNComponents() == 1) {
                switch (types[0]) {
                    case FIELD_DATA_LOGIC: {
                        long dataLen = boolArrs[0].length();
                        int bufferlen = (int) Math.min(dataLen, 1 << 27);
                        byte[] buffer = new byte[bufferlen];
                        long iters = dataLen <= bufferlen ? 1 : (int) Math.ceil((double) dataLen / (double) bufferlen);
                        for (long i = 0; i < iters; i++) {
                            int length = i < iters - 1 ? bufferlen : (int) (dataLen - i * bufferlen);
                            inStream.readFully(buffer, 0, length);
                            int k = 0;
                            for (long j = i * bufferlen; j < i * bufferlen + length; j++) {
                                boolArrs[0].setByte(j, buffer[k++]);
                            }
                        }
                    }
                    break;
                    case FIELD_DATA_BYTE: {
                        long dataLen = byteArrs[0].length();
                        int bufferlen = (int) Math.min(dataLen, 1 << 27);
                        byte[] buffer = new byte[bufferlen];
                        long iters = dataLen <= bufferlen ? 1 : (int) Math.ceil((double) dataLen / (double) bufferlen);
                        for (long i = 0; i < iters; i++) {
                            int length = i < iters - 1 ? bufferlen : (int) (dataLen - i * bufferlen);
                            inStream.readFully(buffer, 0, length);
                            LargeArrayUtils.arraycopy(buffer, 0, byteArrs[0], i * bufferlen, length);
                        }
                    }
                    break;
                    case FIELD_DATA_SHORT: {
                        long dataLen = shortArrs[0].length();
                        int bufferlen = (int) Math.min(dataLen, 1 << 27);
                        short[] buffer = new short[bufferlen];
                        long iters = dataLen <= bufferlen ? 1 : (int) Math.ceil((double) shortArrs[0].length() / (double) bufferlen);
                        for (long i = 0; i < iters; i++) {
                            int length = i < iters - 1 ? bufferlen : (int) (dataLen - i * bufferlen);
                            inStream.readFully(buffer, 0, length);
                            LargeArrayUtils.arraycopy(buffer, 0, shortArrs[0], i * bufferlen, length);
                        }
                    }
                    break;
                    case FIELD_DATA_INT: {
                        long dataLen = intArrs[0].length();
                        int bufferlen = (int) Math.min(dataLen, 1 << 27);
                        int[] buffer = new int[bufferlen];
                        long iters = dataLen <= bufferlen ? 1 : (int) Math.ceil((double) dataLen / (double) bufferlen);
                        for (long i = 0; i < iters; i++) {
                            int length = i < iters - 1 ? bufferlen : (int) (dataLen - i * bufferlen);
                            inStream.readFully(buffer, 0, length);
                            LargeArrayUtils.arraycopy(buffer, 0, intArrs[0], i * bufferlen, length);
                        }
                    }
                    break;
                    case FIELD_DATA_LONG: {
                        long dataLen = longArrs[0].length();
                        int bufferlen = (int) Math.min(dataLen, 1 << 27);
                        long[] buffer = new long[bufferlen];
                        long iters = dataLen <= bufferlen ? 1 : (int) Math.ceil((double) dataLen / (double) bufferlen);
                        for (long i = 0; i < iters; i++) {
                            int length = i < iters - 1 ? bufferlen : (int) (dataLen - i * bufferlen);
                            inStream.readFully(buffer, 0, length);
                            LargeArrayUtils.arraycopy(buffer, 0, longArrs[0], i * bufferlen, length);
                        }
                    }
                    break;
                    case FIELD_DATA_FLOAT: {
                        long dataLen = floatArrs[0].length();
                        int bufferlen = (int) Math.min(dataLen, 1 << 27);
                        float[] buffer = new float[bufferlen];
                        long iters = dataLen <= bufferlen ? 1 : (int) Math.ceil((double) dataLen / (double) bufferlen);
                        for (long i = 0; i < iters; i++) {
                            int length = i < iters - 1 ? bufferlen : (int) (dataLen - i * bufferlen);
                            inStream.readFully(buffer, 0, length);
                            LargeArrayUtils.arraycopy(buffer, 0, floatArrs[0], i * bufferlen, length);
                        }
                    }
                    break;
                    case FIELD_DATA_DOUBLE: {
                        long dataLen = dblArrs[0].length();
                        int bufferlen = (int) Math.min(dataLen, 1 << 27);
                        double[] buffer = new double[bufferlen];
                        long iters = dataLen <= bufferlen ? 1 : (int) Math.ceil((double) dataLen / (double) bufferlen);
                        for (long i = 0; i < iters; i++) {
                            int length = i < iters - 1 ? bufferlen : (int) (dataLen - i * bufferlen);
                            inStream.readFully(buffer, 0, length);
                            LargeArrayUtils.arraycopy(buffer, 0, dblArrs[0], i * bufferlen, length);
                        }
                    }
                    break;
                    case FIELD_DATA_COMPLEX: {
                        long dataLen = cplxArrs[0].length();
                        int bufferlen = (int) Math.min(dataLen, 1 << 27);
                        float[] buffer = new float[bufferlen];
                        long iters = dataLen <= bufferlen ? 1 : (int) Math.ceil((double) dataLen / (double) bufferlen);
                        for (long i = 0; i < iters; i++) {
                            int length = i < iters - 1 ? bufferlen : (int) (dataLen - i * bufferlen);
                            inStream.readFully(buffer, 0, length);
                            LargeArrayUtils.arraycopy(buffer, 0, cplxArrs[0].getRealArray(), i * bufferlen, length);
                        }
                        for (long i = 0; i < iters; i++) {
                            int length = i < iters - 1 ? bufferlen : (int) (dataLen - i * bufferlen);
                            inStream.readFully(buffer, 0, length);
                            LargeArrayUtils.arraycopy(buffer, 0, cplxArrs[0].getImaginaryArray(), i * bufferlen, length);
                        }
                    }
                    break;
                    default:
                }
                return 0;
            } else
                for (long k = 0; k < nNodes; k++) {
                    int cPos = 0;
                    for (int l = 0; l < nComps; l++) {
                        int ll = offsets[l] - cPos;
                        cPos = offsets[l] + types[l].getSize();
                        inStream.skipBytes(ll);
                        switch (types[l]) {
                            case FIELD_DATA_LOGIC:
                                boolArrs[l].setByte(ind[l], inStream.readByte());
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_BYTE:
                                byteArrs[l].setByte(ind[l], inStream.readByte());
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_SHORT:
                                shortArrs[l].setShort(ind[l], inStream.readShort());
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_INT:
                                intArrs[l].setInt(ind[l], inStream.readInt());
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_LONG:
                                longArrs[l].setLong(ind[l], inStream.readLong());
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_FLOAT:
                                floatArrs[l].setFloat(ind[l], inStream.readFloat());
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_DOUBLE:
                                dblArrs[l].setDouble(ind[l], inStream.readDouble());
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_COMPLEX:
                                cplxArrs[l].setComplexFloat(ind[l], new float[]{inStream.readFloat(), inStream.readFloat()});

                        }
                    }
                }
        } catch (Exception e) {
            throw new VNIOException("error in data file ", filePath, -1, schema.getHeaderFile(), schema.getHeaderLine(), e);
        }
        return 0;
    }
}
