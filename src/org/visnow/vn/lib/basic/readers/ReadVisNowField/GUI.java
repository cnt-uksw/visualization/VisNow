/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.basic.readers.ReadVisNowField;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.swingwrappers.TextField;
import static org.visnow.vn.lib.basic.readers.ReadVisNowField.ReadVisNowFieldShared.*;
import org.visnow.vn.system.main.VisNow;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class GUI extends JPanel //implements VariablePresentation
{
    private JFileChooser fileChooser = new JFileChooser();
    private Parameters parameters;

    /**
     * Creates new form GUI
     */
    public GUI()
    {
        initComponents();
        fileChooser.setFileFilter(new FileNameExtensionFilter("VisNow Field files", "vns", "VNS", "vnf", "VNF"));
    }

    void setParameters(Parameters parameters)
    {
        this.parameters = parameters;
    }

    void updateGUI(ParameterProxy p)
    {
        fileNameTF.setText(p.get(ReadVisNowFieldShared.FILENAME));
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        description = new javax.swing.JLabel();
        fileNameTF = new org.visnow.vn.gui.swingwrappers.TextField();
        browseButton = new javax.swing.JButton();
        rereadButton = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createEmptyBorder(4, 4, 4, 4));
        setLayout(new java.awt.GridBagLayout());

        description.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        description.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        jScrollPane1.setViewportView(description);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(jScrollPane1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(fileNameTF, gridBagConstraints);

        browseButton.setText("Browse...");
        browseButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                browseButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        add(browseButton, gridBagConstraints);

        rereadButton.setText("reread");
        rereadButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                rereadButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(rereadButton, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    public void activateOpenDialog()
    {
        browseButtonActionPerformed(null);
    }

    private void fileNameTFUserAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_fileNameTFUserAction
    {//GEN-HEADEREND:event_fileNameTFUserAction
        if (evt.getEventType() == TextField.EVENT_CHANGE_VALUE || evt.getEventType() == TextField.EVENT_NO_CHANGE_ENTER_KEY)
        parameters.set(FILENAME, fileNameTF.getText());
    }//GEN-LAST:event_fileNameTFUserAction

    private void browseButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_browseButtonActionPerformed
    {//GEN-HEADEREND:event_browseButtonActionPerformed
        String currentPath = parameters.get(FILENAME);
        if(currentPath == null || currentPath.isEmpty())
            currentPath = VisNow.get().getMainConfig().getUsableDataPath(ReadVisNowField.class);
        fileChooser.setCurrentDirectory(new File(currentPath));

        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
        {
            String path = fileChooser.getSelectedFile().getAbsolutePath();
            VisNow.get().getMainConfig().setLastDataPath(path.substring(0, path.lastIndexOf(File.separator)), ReadVisNowField.class);
            fileNameTF.setText(path);
            parameters.set(ReadVisNowFieldShared.FILENAME, path);
        }
    }//GEN-LAST:event_browseButtonActionPerformed

    private void rereadButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_rereadButtonActionPerformed
    {//GEN-HEADEREND:event_rereadButtonActionPerformed
        parameters.set(ReadVisNowFieldShared.REREAD, true);
    }//GEN-LAST:event_rereadButtonActionPerformed

    public void setDescription(String desc)
    {
        description.setText(desc);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JButton browseButton;
    protected javax.swing.ButtonGroup buttonGroup;
    protected javax.swing.JLabel description;
    protected org.visnow.vn.gui.swingwrappers.TextField fileNameTF;
    protected javax.swing.JScrollPane jScrollPane1;
    protected javax.swing.JButton rereadButton;
    // End of variables declaration//GEN-END:variables
}
