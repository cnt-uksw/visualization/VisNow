/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.basic.readers.ReadVisNowField;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import org.visnow.vn.gui.widgets.FileErrorFrame;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FieldIOSchema;
import org.visnow.vn.lib.utils.io.VNIOException;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Parser
{

    private LineNumberReader r;
    private String fileName;
    private File headerFile;
    private String line;
    private boolean rangesSet = false;

    public Parser(String fileName, boolean isURL, FileErrorFrame errorFrame) throws IOException
    {
        this.fileName = fileName;
        if (isURL) {
            URL url = new URL(fileName);
            URLConnection urlConnection = url.openConnection();
            r = new LineNumberReader(new InputStreamReader(urlConnection.getInputStream()));
        } else {
            headerFile = new File(fileName);
            r = new LineNumberReader(new FileReader(headerFile));
        }
        line = "";
    }

    public FieldIOSchema parseFieldHeader()
            throws VNIOException
    {
        FieldIOSchema out = null;
        HeaderParser parser = null;
        try {
            line = r.readLine().trim();
            if (line.startsWith("#VisNow regular field")) {
                parser = new RegularFieldHeaderParser(r, headerFile, fileName);
                out = ((RegularFieldHeaderParser)parser).parseHeader();
            } else if (line.startsWith("#VisNow irregular field")) {
                parser = new IrregularFieldHeaderParser(r, headerFile, fileName);
                out = ((IrregularFieldHeaderParser)parser).parseHeader();
            } else if (line.startsWith("#VisNow point field")) {
                parser = new PointFieldHeaderParser(r, headerFile, fileName);
                out = ((PointFieldHeaderParser)parser).parseHeader();
            } else {
                throw new VNIOException("field description file should start with \"#VisNow (ir)regular field\" or \"#VisNow point field\"", fileName, 0);
            }
        } catch (IOException e) {
            throw new VNIOException("could not read", fileName, -1);
        }
        rangesSet = false;
        if(parser != null)
           rangesSet = parser.isRangesSet();
        return out;
    }

    /**
     * @return the rangesSet
     */
    public boolean isRangesSet() {
        return rangesSet;
    }

}
