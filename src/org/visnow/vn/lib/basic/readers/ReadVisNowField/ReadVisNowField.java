/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.basic.readers.ReadVisNowField;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import javax.swing.SwingUtilities;
import org.apache.log4j.Logger;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.PointField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import org.visnow.vn.geometries.parameters.PresentationParams;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.gui.widgets.FileErrorFrame;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FieldIOSchema;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNPointField;
import org.visnow.vn.lib.utils.field.RemoveEmptyComponents;
import org.visnow.vn.lib.utils.io.VNIOException;
import org.visnow.vn.system.main.VisNow;

/**
 * VisNow field (VNF) format handling.
 *
 * @author Krzysztof Nowinski, University of Warsaw, ICM
 */
public class ReadVisNowField extends OutFieldVisualizationModule
{

    private static final Logger LOGGER = Logger.getLogger(ReadVisNowField.class);

    public static OutputEgg[] outputEggs = null;

    protected GUI computeUI = null;
    protected Parser headerParser = null;
    protected FieldIOSchema schema = null;
    protected String fileName = null;
    protected int nCurrentThreads;
    /**
     * Creates a new instance of CreateGrid
     */
    public ReadVisNowField()
    {
        parameters.addParameterChangelistener((String name) -> {
            startAction();
        });
        SwingInstancer.swingRunAndWait(() -> {
            computeUI = new GUI();
            computeUI.setParameters(parameters);
            ui.addComputeGUI(computeUI);
        });
        setPanel(ui);
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(ReadVisNowFieldShared.FILENAME, ""),
            new Parameter<>(ReadVisNowFieldShared.REREAD, false)};
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    private void outputNullField()
    {
        outField = null;
        outRegularField = null;
        outIrregularField = null;
        outPointField = null;
        setOutputValue("regularOutField", null);
        setOutputValue("irregularOutField", null);
        setOutputValue("pointOutField", null);
        prepareOutputGeometry();
        show();
    }

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    public static ObjectInputStream substituteOISClassNames(InputStream in ,String fromClass,String toClass)
        throws IOException ,ClassNotFoundException {
        final String from="^"+fromClass,fromArray="^\\[L"+fromClass,toArray="[L"+toClass;
        return new ObjectInputStream(in) {
            @Override
            protected Class<?> resolveClass(ObjectStreamClass desc)
                throws IOException, ClassNotFoundException
            {
                String name = desc.getName().replaceFirst(from, toClass);
                name = name.replaceFirst(fromArray, toArray);
                return Class.forName(name);
            }
            @Override
            protected ObjectStreamClass readClassDescriptor()
                throws IOException, ClassNotFoundException
            {
                ObjectStreamClass cd = super.readClassDescriptor();
                String name = cd.getName().replaceFirst(from, toClass);
                name = name.replaceFirst(fromArray, toArray);
                if(!name.equals(cd.getName())) {
                    cd = ObjectStreamClass.lookup(Class.forName(name));
                }
                return cd;
            }
        };
    }

    @Override
    public void onActive()
    {
        long startTime = System.nanoTime();

        LOGGER.debug("isFromVNA: " + isFromVNA());
        Parameters parametersClone = parameters.getReadOnlyClone();
        notifyGUIs(parametersClone, false, false);

        fileName = parametersClone.get(ReadVisNowFieldShared.FILENAME);
        if (fileName.isEmpty()) {
            outputNullField();
            return;
        }
        boolean isSerializedField;
        try (FileInputStream in = new FileInputStream(fileName)) {
            byte[] magic = new byte[2];
            int count = in.read(magic);
            if (count < 2) {
                isSerializedField = false;
            } else {
                isSerializedField = (magic[0] == -84 && magic[1] == -19);
            }
            in.close();
        } catch (IOException ex) {
            outputNullField();
            return;
        }
        if (isSerializedField) {
            try (BufferedInputStream bin = new BufferedInputStream(new FileInputStream(fileName));
                    ObjectInputStream in = new ObjectInputStream(bin)) {
                outField = (Field) in.readObject();
                in.close();
            } catch (IOException ex) {
                outputNullField();
                return;
            } catch (ClassNotFoundException ex) {
                try (ObjectInputStream ois = substituteOISClassNames(new BufferedInputStream(new FileInputStream(fileName)),
                                                                     "pl.edu.icm",
                                                                     "org.visnow")) {
                    outField = (Field) ois.readObject();
                    ois.close();
                } catch (Exception e) {
                    outputNullField();
                }
                return;
            }
        } else {
            try {
                headerParser = new Parser(fileName, false, null);
                schema = headerParser.parseFieldHeader();
                schema.checkMissingData();
                if (schema == null) {
                    outputNullField();
                    return;
                }
                outField = schema.getField();
                if (outField == null) {
                    outputNullField();
                    return;
                }
            } catch (VNIOException e) {
                FileErrorFrame.display(e);
                outputNullField();
                return;
            } catch (IOException e) {
                outputNullField();
                return;
            }

            ProgressAgent progressAgent = getProgressAgent(100 * (1 + schema.getFileSchemas().size()));

            try {
                for (int i = 0; i < schema.getFileSchemas().size(); i++) {
                    ReadFile rf = new ReadFile(outField, schema, i, schema.getFileSchemas().size(), false, progressAgent);
                    rf.run();
                    setProgress((float) i / schema.getFileSchemas().size());
                }
                outField = RemoveEmptyComponents.removeEmptyData(outField);
                outField.setCurrentTime(0);
                if (outField.hasCoords())
                    outField.updatePreferredExtents();

                if (outField.hasMask())
                    for (DataArray da : outField.getComponents())
                        da.recomputeStatistics(outField.getMask(), false);
                else
                    for (DataArray da : outField.getComponents())
                        da.recomputeStatistics(false);
                if (outField instanceof IrregularField) {
                    outIrregularField = (IrregularField) outField;
                    for (CellSet cs : outIrregularField.getCellSets())
                        for (DataArray da : cs.getComponents())
                            da.recomputeStatistics();
                }
            } catch (VNIOException e) {
                FileErrorFrame.display(e);
                outputNullField();
                return;
            }

        }
        outRegularField = null;
        outIrregularField = null;
        outPointField = null;
        if (outField != null && outField.getNComponents() > 0) {
            computeUI.setDescription(outField.description());
            switch (outField.getType()) {
            case FIELD_REGULAR:
                outRegularField = (RegularField) outField;
                setOutputValue("regularOutField", new VNRegularField(outRegularField));
                setOutputValue("irregularOutField", null);
                setOutputValue("pointOutField", null);
                break;
            case FIELD_IRREGULAR:
                outIrregularField = (IrregularField) outField;
                setOutputValue("regularOutField", null);
                setOutputValue("irregularOutField", new VNIrregularField((outIrregularField)));
                setOutputValue("pointOutField", null);
                break;
            case FIELD_POINT:
                outPointField = (PointField) outField;
                setOutputValue("regularOutField", null);
                setOutputValue("irregularOutField", null);
                setOutputValue("pointOutField", new VNPointField(outPointField));
                break;
            default:
                outputNullField();
            }
            prepareOutputGeometry();
            if (outField instanceof IrregularField) {
                renderingParams.setShadingMode(RenderingParams.FLAT_SHADED);
                for (PresentationParams csParams : presentationParams.getChildrenParams())
                    csParams.getRenderingParams().setShadingMode(RenderingParams.FLAT_SHADED);
                if (VisNow.get().getMainConfig().isDefaultOutline() && outField.getTrueNSpace() == 3) {
                    ui.getPresentationGUI().getRenderingGUI().setOutlineMode(true);
                    renderingParams.setMinEdgeDihedral(10);
                    renderingParams.setDisplayMode(RenderingParams.EDGES);
                }
            }
            show();

            long endTime = System.nanoTime();
            long duration = (endTime - startTime);
            LOGGER.info("duration (s): " + (double) duration / 1000000000.0);
        }
        else {
            setOutputValue("regularOutField", null);
            setOutputValue("irregularOutField", null);
            setOutputValue("pointOutField", null);
        }
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag())
            SwingUtilities.invokeLater(() -> {
                computeUI.activateOpenDialog();
        });
    }

    /**
     * Compute interface getter.
     *
     * @return Compute UI
     */
    public GUI getGUI()
    {
        return computeUI;
    }

    /**
     * Reads a file in VisNow field format from given URL.
     *
     * @param url URL to file
     *
     * @return Field stored in a given file
     * @throws org.visnow.vn.lib.utils.io.VNIOException
     */
    public Field readVnfFromURL(String url)
            throws VNIOException
    {
        try {
            headerParser = new Parser(url, true, null);
            schema = headerParser.parseFieldHeader();
            if (schema == null)
                return null;
            outField = schema.getField();
            if (outField == null)
                return null;
            ProgressAgent progressAgent = getProgressAgent(100 * schema.getFileSchemas().size());
            for (int i = 0; i < schema.getFileSchemas().size(); i++) {
                ReadFile rf = new ReadFile(outField, schema, i, schema.getFileSchemas().size(), true, progressAgent);
                rf.run();
            }
            outField.setCurrentTime(0);
            if (outField.getCurrentCoords() != null)
                outField.updatePreferredExtents();

            for (DataArray da : outField.getComponents())
                da.recomputeStatistics();

            return outField;
        }
        catch (IOException e) {
            return null;
        }
    }

    /**
     * Reads a file in VisNow field format.
     *
     * @param filePath path to file
     *
     * @return Field stored in a given file
     * @throws org.visnow.vn.lib.utils.io.VNIOException
     */
    public Field readVnf(String filePath)
            throws VNIOException
    {
        ProgressAgent progressAgent = getProgressAgent(100 * schema.getFileSchemas().size());
        return readVnf(filePath, progressAgent);
    }

    /**
     * Reads a file in VisNow field format.
     *
     * @param filePath      path to file
     * @param progressAgent progress agent to monitor read progress
     *
     * @return Field stored in a given file
     * @throws org.visnow.vn.lib.utils.io.VNIOException
     */
    public static Field readVnf(String filePath, ProgressAgent progressAgent)
            throws VNIOException
    {
        try {
            FieldIOSchema schema;
            Field outField;
            Parser headerParser;

            if (filePath == null)
                return null;

            File f = new File(filePath);
            if (!f.exists() || !f.canRead())
                return null;

            headerParser = new Parser(filePath, false, null);
            schema = headerParser.parseFieldHeader();
            if (schema == null)
                return null;
            outField = schema.getField();
            for (int i = 0; i < schema.getFileSchemas().size(); i++) {
                ReadFile rf = new ReadFile(outField, schema, i, schema.getFileSchemas().size(), false, progressAgent);
                rf.run();
            }
            outField.setCurrentTime(0);
            if (outField.getCurrentCoords() != null)
                outField.updatePreferredExtents();

            for (DataArray da : outField.getComponents())
                da.recomputeStatistics();

            if (outField instanceof IrregularField) {
                IrregularField outIrregularField = (IrregularField) outField;
                for (CellSet cs : outIrregularField.getCellSets())
                    for (DataArray da : cs.getComponents())
                        da.recomputeStatistics();
            }
            return outField;
        }
        catch (IOException e) {
            return null;
        }
    }
}
