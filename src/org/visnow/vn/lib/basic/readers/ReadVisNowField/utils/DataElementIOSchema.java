/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.basic.readers.ReadVisNowField.utils;

import org.visnow.jscic.DataContainer;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public abstract class DataElementIOSchema
{

    public static final int COORDS = -1;
    public static final int MASK = -2;
    public static final int GLOBAL = -1;
    protected final DataContainer dataset;
    protected final int coord;
    protected final DataArrayType type;
    protected final int offsetFrom;
    protected final int offsetTo;
    protected final long nData;
    protected final int veclen;
    protected final String name;

    public DataElementIOSchema(String name, DataContainer dataset, int coord, DataArrayType type, int veclen, long nData, int offsetFrom, int offsetTo)
    {
        this.name = name;
        this.dataset = dataset;
        this.coord = coord;
        this.type = type;
        this.veclen = veclen;
        this.nData = nData;
        this.offsetFrom = offsetFrom;
        this.offsetTo = offsetTo;
    }

    public DataElementIOSchema(DataContainer dataset, int cmp, int coord, DataArrayType type, int veclen, long nData, int offsetFrom, int offsetTo)
    {
        this.name = cmp <  dataset.getNComponents() ? dataset.getComponent(cmp).getName() :
                    cmp == dataset.getNComponents() ? "coords" : "mask";
        this.dataset = dataset;
        this.coord = coord;
        this.type = type;
        this.veclen = veclen;
        this.nData = nData;
        this.offsetFrom = offsetFrom;
        this.offsetTo = offsetTo;
    }



    @Override
    public abstract String toString();

    public String getName()
    {
        return name;
    }

    /**
     * Get the value of offsetTo
     *
     * @return the value of offsetTo
     */
    public int getOffsetTo()
    {
        return offsetTo;
    }


    /**
     * Get the value of type
     *
     * @return the value of type
     */
    public DataArrayType getType()
    {
        return type;
    }

    /**
     * Get the value of offsetFrom
     *
     * @return the value of offsetFrom
     */
    public int getOffsetFrom()
    {
        return offsetFrom;
    }

    /**
     * Get the value of coord
     *
     * @return the value of coord
     */
    public int getCoord()
    {
        return coord;
    }

    /**
     * Get the value of dataset
     *
     * @return the value of dataset
     */
    public DataContainer getDataset()
    {
        return dataset;
    }

    public int getVectorLength()
    {
        return veclen;
    }

    public long getnData()
    {
        return nData;
    }

}
