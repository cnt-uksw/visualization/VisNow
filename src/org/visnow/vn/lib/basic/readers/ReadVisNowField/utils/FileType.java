//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
//</editor-fold>

package org.visnow.vn.lib.basic.readers.ReadVisNowField.utils;

/**
 *
 * @author know
 */
public enum FileType
{
    ASCII_COLUMN(        "a", "col", true,  true,  "ascii column"),
    ASCII_CONTINUOUS(    "a", "con", false, true,  "ascii continuous"),
    ASCII_FIXED_COLUMN(  "a", "f",   false, true,  "ascii fixed char column"),
    BINARY_BIG_ENDIAN(   "b", "b",   true,  false, "binary big endian"),
    BINARY_LITTLE_ENDIAN("b", "l",   false, false, "binary big endian"),
    UNKNOWN(             "?", "",    true,  false, "unknown");

    private final String prefix;
    private final String subprefix;
    private final boolean isDefault;
    private final boolean isAscii;
    private final String text;

    FileType(String prefix, String subprefix, boolean isDefault, boolean isAscii, String text)
    {
        this.prefix = prefix;
        this.subprefix = subprefix;
        this.isDefault = isDefault;
        this.isAscii = isAscii;
        this.text = text;
    }

    public static FileType getType(String prefix, String subprefix)
    {
        if (subprefix != null && !subprefix.isEmpty())
            for (FileType value : FileType.values())
                if (prefix.toLowerCase().startsWith(value.prefix) &&
                    subprefix.toLowerCase().startsWith(value.subprefix))
                    return value;
        for (FileType value : FileType.values())
            if (prefix.toLowerCase().startsWith(value.prefix) && value.isDefault)
                return value;
        return UNKNOWN;
    }

    public boolean isAscii()
    {
        return isAscii;
    }

    public boolean isBinary()
    {
        return !isAscii;
    }

    public FileType defaultType()
    {
        if (prefix.equals("a"))
            return ASCII_COLUMN;
        if (prefix.equals("b"))
            return BINARY_BIG_ENDIAN;
        return UNKNOWN;
    }

    public static boolean isSubprefix(String s)
    {
        for (FileType value : FileType.values())
            if (s.toLowerCase().startsWith(value.subprefix) && value != UNKNOWN)
                return true;
        return false;
    }

    @Override
    public String toString()
    {
        return text;
    }
}
