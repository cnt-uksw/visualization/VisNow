/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.basic.readers.ReadVisNowField;

import java.util.Arrays;
import org.apache.log4j.Logger;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.Field;
import org.visnow.jscic.dataarrays.ByteDataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.dataarrays.DoubleDataArray;
import org.visnow.jscic.dataarrays.FloatDataArray;
import org.visnow.jscic.dataarrays.IntDataArray;
import org.visnow.jscic.dataarrays.LogicDataArray;
import org.visnow.jscic.dataarrays.ShortDataArray;
import org.visnow.jscic.dataarrays.StringDataArray;
import org.visnow.jscic.dataarrays.ComplexDataArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.ShortLargeArray;
import org.visnow.jlargearrays.StringLargeArray;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.LongLargeArray;
import org.visnow.jscic.dataarrays.LongDataArray;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.BooleanArrayIOSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.ComponentIOSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.DataElementIOSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FileSectionSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FileType;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.IntArrayIOSchema;
import org.visnow.vn.lib.utils.io.VNIOException;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
/**
 * Section model holds all data necessary for reading a file section<p>
 * In the case of a single component section, data table is produced and hold in a proper ...Arrs table<p>
 * In general, each data item described in the sectionSchema provides:
 * <p>
 * an entry in ...Arrs table providing place where the data will be read in<p>
 * <p>
 *
 */
class SectionModel
{
    private static final Logger LOGGER = Logger.getLogger(SectionModel.class);

    final FileSectionSchema sectionSchema;
    final Field outField;

    final float time;
    long  nData = 1;
    int   nItems; //number of items to be read for each node
    /**
     *
     */
    final int[] cindex;
    final int[] comps;
    final int[] coords;
    final double[] minVals;
    final double[] maxVals;
    /**
     * offset of the (first) item from the beginning of data series
     * in bytes for binary files
     * in items for ascii continuous or column files
     * in characters for ascii fixed column files (position of the first char to be read)
     */
    final int[] offsets;
    /**
     * offset of the last character of the item from the beginning of data series
     * in characters, used only for ascii fixed column files (position of the last char to be read)
     */
    final int[] offsets1;
    /**
     * component type (as in DataArray class - see constant values there)
     */
    final DataArrayType[] types;
    final int[] vlens;
    final LogicLargeArray[]        boolArrs;
    final UnsignedByteLargeArray[] byteArrs;
    final ShortLargeArray[]        shortArrs;
    final IntLargeArray[]          intArrs;
    final LongLargeArray[]         longArrs;
    final FloatLargeArray[]        floatArrs;
    final DoubleLargeArray[]       dblArrs;
    final StringLargeArray[]       strArrs;
    final ComplexFloatLargeArray[] cplxArrs;
    final DataElementIOSchema[]    schemas;
    long[] ind;

    SectionModel(FileSectionSchema sectionSchema, Field outField, float time, FileType fileType) throws VNIOException
    {
        this.sectionSchema = sectionSchema;
        nItems = 0;
        nData = -1;
        cindex = new int[sectionSchema.getNComponents()];
        for (int sectionSchemaCompIndex = 0; sectionSchemaCompIndex < sectionSchema.getNComponents(); sectionSchemaCompIndex++) {
            long k = sectionSchema.getComponent(sectionSchemaCompIndex).getnData();
            if (nData == -1)
                nData = k;
            if (nData != k)
                throw new VNIOException("all data specified in a file saction must have the same number of elements", sectionSchema.getHeaderFile(), sectionSchema.getHeaderLine());
            if (sectionSchema.getComponent(sectionSchemaCompIndex) instanceof IntArrayIOSchema) {
                IntArrayIOSchema iArr = (IntArrayIOSchema) sectionSchema.getComponent(sectionSchemaCompIndex);
                cindex[sectionSchemaCompIndex] = nItems;
                if (sectionSchema.isSingleComponent()) //everything will be read with a single command
                    nItems = 1;
                else if (iArr.getCoord() >= 0) // single coordinate
                    nItems += 1;
                else
                    nItems += iArr.getVectorLength();
            } else if (sectionSchema.getComponent(sectionSchemaCompIndex) instanceof BooleanArrayIOSchema) {
                BooleanArrayIOSchema bArr = (BooleanArrayIOSchema) sectionSchema.getComponent(sectionSchemaCompIndex);
                if (sectionSchema.isSingleComponent()) //everything will be read with a single command
                    nItems = 1;
                else if (bArr.getCoord() >= 0) // single coordinate
                    nItems += 1;
                else
                    nItems += bArr.getVectorLength();
            } else {
                ComponentIOSchema comp = (ComponentIOSchema) sectionSchema.getComponent(sectionSchemaCompIndex);
                cindex[sectionSchemaCompIndex] = nItems;
                if (sectionSchema.isSingleComponent()) //everything will be read with a single command
                    if (fileType.isBinary())
                        nItems = 1;
                    else
                        nItems = comp.getVectorLength();
                else
                    if (comp.getCoord() >= 0) // single coordinate
                    nItems += 1;
                else {
                    if (comp.getComponent() < outField.getNComponents())
                        nItems += comp.getVectorLength();
                    else if (comp.getCmpName().startsWith("coord"))
                        nItems += 3;
                    else
                        nItems += 1; // whole component read
                }
            }
        }
        this.time = time;
        this.outField = outField;
        schemas = new DataElementIOSchema[nItems];
        comps = new int[nItems];
        coords = new int[nItems];
        offsets = new int[nItems];
        offsets1 = new int[nItems];
        types = new DataArrayType[nItems];
        vlens = new int[nItems];
        ind = new long[nItems];
        minVals = new double[nItems];
        maxVals = new double[nItems];
        Arrays.fill(minVals, Double.NEGATIVE_INFINITY);
        Arrays.fill(maxVals, Double.POSITIVE_INFINITY);
        boolArrs =  new LogicLargeArray[nItems];
        byteArrs =  new UnsignedByteLargeArray[nItems];
        shortArrs = new ShortLargeArray[nItems];
        intArrs =   new IntLargeArray[nItems];
        longArrs =  new LongLargeArray[nItems];
        floatArrs = new FloatLargeArray[nItems];
        dblArrs =   new DoubleLargeArray[nItems];
        strArrs =   new StringLargeArray[nItems];
        cplxArrs =  new ComplexFloatLargeArray[nItems];
        for (int sectionSchemaCompIndex = 0, modelComponentIndex = 0; sectionSchemaCompIndex < sectionSchema.getNComponents(); sectionSchemaCompIndex++) {
            DataElementIOSchema deSchema = sectionSchema.getComponent(sectionSchemaCompIndex);
            int cmp = -1;
            int coord = deSchema.getCoord();
            int nCoords = 1;
            int vlen = deSchema.getVectorLength();
            if (coord == -1 || sectionSchema.isSingleComponent() && fileType.isAscii())
                nCoords = vlen; // all coordinates read in proper order
            schemas[modelComponentIndex] = deSchema;
            if (deSchema instanceof IntArrayIOSchema)
                intArrs[modelComponentIndex] = ((IntArrayIOSchema) deSchema).getIntArray();
            else if (deSchema instanceof BooleanArrayIOSchema)
                boolArrs[modelComponentIndex] = new LogicLargeArray(nData * vlen);
            else {
                ComponentIOSchema compIOSchema = (ComponentIOSchema) deSchema;
                cmp = compIOSchema.getComponent();
                // allocating / getting data arrays
                if (sectionSchema.getTile() != null) {
                    long tileSize = nData = sectionSchema.getTileSize();
                    switch (compIOSchema.getType()) {
                        case FIELD_DATA_LOGIC:
                            boolArrs[modelComponentIndex] = new LogicLargeArray(tileSize * nCoords);
                            break;
                        case FIELD_DATA_BYTE:
                            byteArrs[modelComponentIndex] = new UnsignedByteLargeArray(tileSize * nCoords);
                            break;
                        case FIELD_DATA_SHORT:
                            shortArrs[modelComponentIndex] = new ShortLargeArray(tileSize * nCoords);
                            break;
                        case FIELD_DATA_INT:
                            intArrs[modelComponentIndex] = new IntLargeArray(tileSize * nCoords);
                            break;
                        case FIELD_DATA_LONG:
                            longArrs[modelComponentIndex] = new LongLargeArray(tileSize * nCoords);
                            break;
                        case FIELD_DATA_FLOAT:
                            floatArrs[modelComponentIndex] = new FloatLargeArray(tileSize * nCoords);
                            break;
                        case FIELD_DATA_DOUBLE:
                            dblArrs[modelComponentIndex] = new DoubleLargeArray(tileSize * nCoords);
                            break;
                        case FIELD_DATA_COMPLEX:
                            cplxArrs[modelComponentIndex] = new ComplexFloatLargeArray(tileSize * nCoords);
                            break;
                        case FIELD_DATA_STRING:
                            strArrs[modelComponentIndex] = new StringLargeArray(tileSize * nCoords);
                            break;
                        default:
                            throw new VNIOException("unknown error in the .vnf file",
                                                    sectionSchema.getHeaderFile(), sectionSchema.getHeaderLine());
                    }
                } else {
                    DataContainer container = compIOSchema.getDataset();
                    switch (compIOSchema.getType()) {
                        case FIELD_DATA_LOGIC:
                            if (coord != -1)
                                boolArrs[modelComponentIndex] = new LogicLargeArray(nData);
                            else if (cmp < container.getNComponents())
                                boolArrs[modelComponentIndex] = (((LogicDataArray) container.getComponent(cmp)).produceData(time));
                            else if (cmp == container.getNComponents() + 1 && container == outField) {
                                if (!outField.hasMask())
                                    outField.setCurrentMask(new LogicLargeArray(outField.getNNodes()));
                                boolArrs[modelComponentIndex] = ((Field)container).getMask(time);
                            }
                            break;
                        case FIELD_DATA_BYTE:
                            if (coord != -1)
                                byteArrs[modelComponentIndex] = new UnsignedByteLargeArray(nData);
                            else if (cmp < container.getNComponents())
                                byteArrs[modelComponentIndex] = (((ByteDataArray) container.getComponent(cmp)).produceData(time));
                            break;
                        case FIELD_DATA_SHORT:
                            if (coord != -1)
                                shortArrs[modelComponentIndex] = new ShortLargeArray(nData);
                            else if (cmp < container.getNComponents())
                                shortArrs[modelComponentIndex] = (((ShortDataArray) container.getComponent(cmp)).produceData(time));
                            break;
                        case FIELD_DATA_INT:
                            if (coord != -1)
                                intArrs[modelComponentIndex] = new IntLargeArray(nData);
                            else if (cmp < container.getNComponents())
                                intArrs[modelComponentIndex] = (((IntDataArray) container.getComponent(cmp)).produceData(time));
                            break;
                        case FIELD_DATA_LONG:
                            if (coord != -1)
                                longArrs[modelComponentIndex] = new LongLargeArray(nData);
                            else if (cmp < container.getNComponents())
                                longArrs[modelComponentIndex] = (((LongDataArray) container.getComponent(cmp)).produceData(time));
                            break;
                        case FIELD_DATA_FLOAT:
                            if (coord != -1)
                                floatArrs[modelComponentIndex] = new FloatLargeArray(nData);
                            else if (cmp < container.getNComponents())
                                floatArrs[modelComponentIndex] = (((FloatDataArray) container.getComponent(cmp)).produceData(time));
                            else if (cmp == outField.getNComponents() && container == outField) {
                                if (!outField.hasCoords())
                                    outField.setCurrentCoords(new FloatLargeArray(3 * outField.getNNodes()));
                                floatArrs[modelComponentIndex] = outField.produceCoords(time);
                            }
                            break;
                        case FIELD_DATA_DOUBLE:
                            if (coord != -1)
                                dblArrs[modelComponentIndex] = new DoubleLargeArray(outField.getNNodes());
                            else if (cmp < outField.getNComponents())
                                dblArrs[modelComponentIndex] = (((DoubleDataArray) container.getComponent(cmp)).produceData(time));
                            break;
                        case FIELD_DATA_COMPLEX:
                            if (coord != -1)
                                cplxArrs[modelComponentIndex] = new ComplexFloatLargeArray(outField.getNNodes());
                            else if (cmp < outField.getNComponents())
                                cplxArrs[modelComponentIndex] = (((ComplexDataArray)container.getComponent(cmp)).produceData(time));
                            break;
                        case FIELD_DATA_STRING:
                            if (coord != -1)
                                strArrs[modelComponentIndex] = new StringLargeArray(outField.getNNodes());
                            else if (cmp < outField.getNComponents())
                                strArrs[modelComponentIndex] = ((StringDataArray) container.getComponent(cmp)).produceData(time);
                            break;
                        default:
                            throw new VNIOException("unknown error in the .vnf file",
                                                    sectionSchema.getHeaderFile(), sectionSchema.getHeaderLine());
                    }
                }
            }
            if (sectionSchema.isSingleComponent() && fileType.isBinary())
                nCoords = 1;
            for (int l = 0; l < nCoords; l++) {
                try {
                    schemas[modelComponentIndex + l] = schemas[modelComponentIndex];
                    comps[modelComponentIndex + l] = cmp;
                    types[modelComponentIndex + l] = deSchema.getType();
                    vlens[modelComponentIndex + l] = nCoords;
                    boolArrs[modelComponentIndex + l] =  boolArrs[modelComponentIndex];
                    byteArrs[modelComponentIndex + l] =  byteArrs[modelComponentIndex];
                    shortArrs[modelComponentIndex + l] = shortArrs[modelComponentIndex];
                    intArrs[modelComponentIndex + l] =   intArrs[modelComponentIndex];
                    longArrs[modelComponentIndex + l] =  longArrs[modelComponentIndex];
                    floatArrs[modelComponentIndex + l] = floatArrs[modelComponentIndex];
                    dblArrs[modelComponentIndex + l] =   dblArrs[modelComponentIndex];
                    cplxArrs[modelComponentIndex + l] =  cplxArrs[modelComponentIndex];
                    strArrs[modelComponentIndex + l] =   strArrs[modelComponentIndex];
                    offsets[modelComponentIndex + l] =  deSchema.getOffsetFrom() + l;
                    offsets1[modelComponentIndex + l] = deSchema.getOffsetTo();
                    ind[modelComponentIndex + l] = l;
                    if (coord == -1 && !sectionSchema.isSingleComponent())
                        coords[modelComponentIndex + l] = l;
                    else
                        coords[modelComponentIndex + l] = coord;
                } catch (Exception e) {
                    throw new VNIOException("error in section model creation ",
                                            sectionSchema.getHeaderFile(), sectionSchema.getHeaderLine());
                }
            }
            modelComponentIndex += nCoords;
        }
    }
}
