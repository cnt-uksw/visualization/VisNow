/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.basic.readers.ReadVisNowField;

import java.io.File;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Vector;
import org.apache.log4j.Logger;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.Field;
import static org.visnow.jscic.dataarrays.DataArrayType.*;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.dataarrays.DataArraySchema.DataOutsideRangeAction;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.*;
import org.visnow.vn.lib.utils.io.VNIOException;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
abstract public class HeaderParser
{
    private static final Logger LOGGER = Logger.getLogger(HeaderParser.class);
    public static enum ParseResult {EMPTY, ACCEPTED, BREAK, UNKNOWN, EOF, ERROR};
    public static final DataArrayType[] ALLOWED_TYPES =  new DataArrayType[]{
            FIELD_DATA_LOGIC,
            FIELD_DATA_BYTE,
            FIELD_DATA_SHORT,
            FIELD_DATA_INT,
            FIELD_DATA_LONG,
            FIELD_DATA_FLOAT,
            FIELD_DATA_DOUBLE,
            FIELD_DATA_COMPLEX,
            FIELD_DATA_STRING};
    protected String[] entries;
    protected String line;
    String[] stringsInLine;
    protected LineNumberReader r;
    protected Field field;
    protected String[] names;
    protected DataArrayType[] types;
    protected int[] vlens;
    protected String fileName;
    protected File headerFile;
    protected boolean parsingTimestep = false;
    protected boolean errorFlag = false;
    private boolean rangesSet = false;
    protected FieldIOSchema parsedSchema;

    public HeaderParser(LineNumberReader r, File headerFile, String fileName)
    {
        this.r = r;
        this.headerFile = headerFile;
        this.fileName = fileName;
    }


    protected String nextLine()
            throws VNIOException
    {
        String lineIn = "";
        try {
            while (lineIn != null && (lineIn.isEmpty() || lineIn.startsWith("#")))
                lineIn = r.readLine();
        } catch (IOException ex) {
            throw new VNIOException("line could not be read ", fileName, r.getLineNumber());
        }
        return lineIn;
    }

    public ParseResult processLine(String lineIn, String[] acceptStrings, String[] endStrings, Vector<String[]> result)
            throws VNIOException
    {
        if (result == null)
            throw new VNIOException("invalid keyword ", fileName, r.getLineNumber());
        if (lineIn == null)
            return ParseResult.EOF;
        if (lineIn.isEmpty() || lineIn.startsWith("#"))
            return ParseResult.EMPTY;
        for (String s : endStrings)
            if (lineIn.startsWith(s))
                return ParseResult.BREAK;
        Vector<String> substrings = new Vector<String>();
        lineIn = findSubstrings(lineIn.trim(), substrings).replaceFirst("#.*", "");
        stringsInLine = new String[substrings.size()];
        for (int i = 0; i < substrings.size(); i++)
            stringsInLine[i] = substrings.get(i);
        String ll = lineIn.replaceFirst("\"", "");
        for (String s : acceptStrings)
            if (DataArraySchema.correctDataArrayName(ll).toLowerCase().startsWith(s.toLowerCase())) {
                splitLine(ll, substrings, result);
                return ParseResult.ACCEPTED;
            }
        return ParseResult.UNKNOWN;
    }

    /**
     * finds all quote enclosed substrings in processedLine[0] and replaces each such
     * substring by __n, where n is its number
     * <p>
     * @param processedLine - size 1 array containing processed string; on exit,
     *                      processedLine[0] has all quota enclosed substring replaced by __0, __1,...
     * @param substrings    vector of substrings found in the processed string
     * <p>
     * @return processedLine after substitution
     */
    public static String findSubstrings(String processedLine, Vector<String> substrings)
    {
        String[] stringsInLine = processedLine.split("\"");
        substrings.clear();
        if (stringsInLine.length >= 2) {
            for (int i = 1; i < stringsInLine.length; i += 2) {
                substrings.add(stringsInLine[i]);
                processedLine = processedLine.replaceFirst("\"[^\"]*\"", "__" + (i / 2));
            }
        }
        return processedLine;
    }

    /**
     * splits line into comma separated sections and splits each section into space separated tokens
     * <p>
     * @param line       processed line
     * @param substrings vector of substrings substituted by __n in the string line
     * @param result     vector of line sections split into tokens
     */
    public static void splitLine(String line, Vector<String> substrings, Vector<String[]> result)
    {
        String[] entries = line.split("\\s*,\\s*");
        if (entries == null || entries.length < 1)
            return;
        result.clear();
        for (String entry : entries) {
            String[] res = entry.split("[=\\s]+");
            for (int i = 0; i < res.length; i++)
                if (res[i].startsWith("__"))
                    try {
                        int k = Integer.parseInt(res[i].substring(2));
                        res[i] = substrings.get(k);
                    } catch (NumberFormatException e) {
                    }
            result.add(res);
        }
    }

    protected int getComponentIndex(DataContainer d, String s)
    {
        if (s == null || s.isEmpty() || d == null || d.isEmpty())
            return -1;
        for (int i = 0; i < d.getNComponents(); i++)
            if (s.equalsIgnoreCase(d.getComponent(i).getName()))
                return i;

        return -1;
    }

    public class FieldUnits
    {
        public final String[] geomUnits;
        public final String timeUnit;

        public FieldUnits(String[] geomUnits, String timeUnit) {
            this.geomUnits = geomUnits;
            this.timeUnit = timeUnit;
        }
    }

    protected FieldUnits parseUnitsEntry(String line)
            throws VNIOException
    {
        if (!line.startsWith("unit"))
            return null;
        Vector<String> substrings = new Vector<>();
        Vector<String[]> result = new Vector<>();
        splitLine(line, substrings, result);
        String[] res0 = result.get(0);
        String[] res0s = new String[res0.length - 1];
        for (int i = 0; i < res0.length - 1; i++)
            res0s[i] = res0[i + 1];
        result.set(0, res0s);
        String timeUnit = "";
        String geomUnit = "";
        String xUnit = "", yUnit = "", zUnit = "";
        for (String[] res : result)
            if (res.length > 1) {
                if (res[0].startsWith("time"))
                    timeUnit = res[1];
                else if (res[0].startsWith("x"))
                    xUnit = res[1];
                else if (res[0].startsWith("y"))
                    yUnit = res[1];
                else if (res[0].startsWith("z"))
                    zUnit = res[1];
                else if (res[0].startsWith("geom") || res[0].startsWith("coords") || res[0].startsWith("len") || res[0].startsWith("dist"))
                    geomUnit = res[1];
            }
        if (!geomUnit.isEmpty())
            return new FieldUnits(new String[]{geomUnit, geomUnit, geomUnit}, timeUnit);
        else
            return new FieldUnits(new String[]{xUnit, yUnit, zUnit}, timeUnit);
    }

    protected DataArray parseComponentEntry(String line, long nNodes, String fileName, int lineNumber)
            throws VNIOException
    {
        Vector<String[]> res = new Vector<>();
        ParseResult result = processLine(line, new String[]{"comp", "cmp"}, new String[]{"file", "cell"}, res);
        if (result == ParseResult.ERROR)
            throw new VNIOException("invalid keyword", fileName, r.getLineNumber());
        if (result != ParseResult.ACCEPTED)
            return null;
        if (nNodes < 0)
            throw new VNIOException("cannot create component with empty data; <p>it seems that the cell set ndata description is missing", fileName, r.getLineNumber());
        String name = res.get(0)[1];
        DataArrayType type = DataArrayType.FIELD_DATA_UNKNOWN;
        if (res.get(0).length > 2) {
            parse_type:
            for (DataArrayType allowed : ALLOWED_TYPES)
                for (String item : allowed.getNameAnalogies())
                    if (res.get(0)[2].startsWith(item)) {
                        type = allowed;
                        break parse_type;
                    }
        }
        if (type == DataArrayType.FIELD_DATA_UNKNOWN)
            throw new VNIOException("data type specification missing", fileName, lineNumber);
        String unit = "";
        int veclen = 1;
        int[] dims = new int[]{1};
        boolean symmetric = false;
        boolean isPreferredPhysMin = false, isPreferredPhysMax = false;
        double preferredPhysMin = 0, preferredPhysMax = 1;
        boolean isPreferredMin = false, isPreferredMax = false;
        double preferredMin = 0, preferredMax = 1;
        String[] userData = null;
        DataOutsideRangeAction dataOutsideRange = DataOutsideRangeAction.LEAVE;
        for (int i = 1; i < res.size(); i++) {
            String[] strings = res.get(i);
            dims[0] = -1;
            symmetric = true;
            try {
                if (strings[0].startsWith("vec") || strings[0].startsWith("vlen") && strings.length > 1) {
                    veclen = Integer.parseInt(strings[1]);
                    continue;
                }
                if (strings[0].startsWith("arr") && strings.length > 1) {
                    int d = strings.length - 1;
                    if (strings[1].startsWith("sym") && d >= 2) {
                        symmetric = true;
                        dims = new int[2];
                        dims[0] = dims[1] = Integer.parseInt(strings[2]);
                        veclen = (int) ((dims[0] * (dims[0] + 1)) / 2);
                    } else {
                        int vl = 1;
                        dims = new int[d];
                        for (int j = 0; j < d; j++) {
                            dims[j] = Integer.parseInt(strings[j + 1]);
                            vl *= dims[j];
                        }
                        veclen = vl;
                    }
                    continue;
                }

                if (strings[0].startsWith("unit") && strings.length > 1) {
                    unit = strings[1].trim();
                    continue;
                }
                if (strings[0].startsWith("type") && strings.length > 1) {
                    String val = res.get(0)[2].toLowerCase();
                    type = FIELD_DATA_UNKNOWN;
                    parse:
                    for (DataArrayType allowed : ALLOWED_TYPES)
                        for (String item : allowed.getNameAnalogies())
                            if (val.startsWith(item)) {
                                type = allowed;
                                break parse;
                    }
                    if (type == FIELD_DATA_UNKNOWN)
                        throw new VNIOException("invalid component type", fileName, lineNumber);
                }
                if (strings[0].startsWith("user:")) {
                    userData = strings[0].substring(5).split(";");
                    for (int j = 0; j < userData.length; j++)
                        if (userData[j].startsWith("__"))
                            try {
                                int k = Integer.parseInt(userData[j].substring(2));
                                userData[j] = stringsInLine[k];
                            } catch (NumberFormatException e) {
                            }
                    continue;
                }
                if ((strings[0].startsWith("preferred_phys_min") || strings[0].startsWith("phys_min")) && strings.length > 1) {
                    isPreferredPhysMin = true;
                    preferredPhysMin = Double.parseDouble(strings[1]);
                    continue;
                }
                if ((strings[0].startsWith("preferred_phys_max") || strings[0].startsWith("phys_max")) && strings.length > 1) {
                    isPreferredPhysMax = true;
                    preferredPhysMax = Double.parseDouble(strings[1]);
                    continue;
                }
                if ((strings[0].startsWith("preferred_min") || strings[0].startsWith("min")) && strings.length > 1) {
                    isPreferredMin = true;
                    preferredMin = Double.parseDouble(strings[1]);
                    continue;
                }
                if ((strings[0].startsWith("preferred_max") || strings[0].startsWith("max"))&& strings.length > 1) {
                    isPreferredMax = true;
                    preferredMax = Double.parseDouble(strings[1]);
                    continue;
                }
                throw new VNIOException("invalid keyword", fileName, lineNumber);
            } catch (NumberFormatException e) {
                throw new VNIOException("invalid number", fileName, lineNumber);
            }
        }

        DataArraySchema schema = new DataArraySchema(name, unit, userData, type, nNodes, veclen, false);
        rangesSet = false;
        if (isPreferredMin && isPreferredMax && isPreferredPhysMin && isPreferredPhysMax) {
            if (preferredMin >= preferredMax)
                throw new VNIOException("preferredMin > preferredMax", fileName, lineNumber);
            if (preferredPhysMin >= preferredPhysMax)
                throw new VNIOException("preferredPhysMin > preferredPhysMax", fileName, lineNumber);
            schema.setPreferredRanges(preferredMin, preferredMax, preferredPhysMin, preferredPhysMax);
            rangesSet = true;
        } else if (isPreferredMin && isPreferredMax) {
            schema.setPreferredRange(preferredMin, preferredMax);
            rangesSet = true;
        }
        schema.setMatrixProperties(dims, symmetric);
        return DataArray.create(schema);
    }

    protected SkipSchema parseSkipSection(String[] items)
    {
        try {
            int k = Integer.parseInt(items[1]);
            return new SkipSchema(k);
        } catch (NumberFormatException e) {
            if (items.length > 1)
                return new SkipSchema(items[1]);
        }
        return null;
    }

    protected TimestepSchema parseTimestepEntry(FileType fileType)
            throws VNIOException
    {
        TimestepSchema timestep = null;
        Vector<String[]> res = new Vector<String[]>();
        ParseResult result = processLine(line, new String[]{"timestep"}, new String[]{"file", "end"}, res);
        switch (result) {
            case ACCEPTED:
                timestep = new TimestepSchema();
                if (res.get(0).length > 1)
                    try {
                        timestep.setTime(Float.parseFloat(res.get(0)[1]));
                    } catch (NumberFormatException e) {
                        throw new VNIOException("time value format error", fileName, r.getLineNumber());
                    }
                if (res.get(0).length > 2)
                    try {
                        timestep.setDt(Float.parseFloat(res.get(0)[2]));
                    } catch (NumberFormatException e) {
                        throw new VNIOException("delta time format error", fileName, r.getLineNumber());
                    }
                timestep_loop:
                while (true) {
                    line = nextLine();
                    Vector<String[]> tres = new Vector<>();
                    result = processLine(line, names, new String[]{"file", "end"}, tres);
                    switch (result) {
                        case ACCEPTED:
                            if (tres.get(0)[0].equalsIgnoreCase("repeat")) {
                                try {
                                    timestep.setRepeat(Integer.parseInt(tres.get(0)[1]));
                                } catch (NumberFormatException e) {
                                }
                                break timestep_loop;
                            } else if (tres.get(0)[0].equalsIgnoreCase("skip"))
                                timestep.addSection(parseSkipSection(tres.get(0)));
                            else {
                                FilePartSchema sch = parseFileSectionEntry(fileType, tres);
                                if (sch != null)
                                    timestep.addSection(sch);
                                else
                                    return null;
                            }
                            break;
                        case EOF:
                            break timestep_loop;
                        case ERROR:
                            throw new VNIOException("invalid component name or keyword", fileName, r.getLineNumber());
                        case BREAK:
                            if (res.get(0)[0].startsWith("end"))
                                line = nextLine();
                            break timestep_loop;
                        case UNKNOWN:
                            throw new VNIOException("invalid component name or keyword", fileName, r.getLineNumber());
                    }
                }
        }
        if (errorFlag)
            throw new VNIOException("invalid time step entry ", fileName, r.getLineNumber());
        return timestep;
    }

    // parseFileSectionEntry valid for Irrregular and PointFieldHeaderParser, overriden (tiles) for Regular
    protected FilePartSchema parseFileSectionEntry(FileType fileType, Vector<String[]> tokens)
            throws VNIOException
    {
        int stride = -1;
        int[][] tile = null;
        int cOffset = 0;
        Vector<DataElementIOSchema> compSchemas = new Vector<DataElementIOSchema>();

        for (int i = 0; i < tokens.size(); i++) {
            String[] strings = tokens.get(i); // parsing i-th item with file part description line
            if (strings[0].startsWith("stride") && strings.length > 1)
                stride = Integer.parseInt(strings[1]);
            else
                cOffset = parseComponentSchema(field, strings, fileType, compSchemas, cOffset);
            if (cOffset == -1)
                throw new VNIOException("invalid file section entry ", fileName, r.getLineNumber());
        }
        if (stride == -1)
            stride = cOffset;
        FileSectionSchema secSchema = new FileSectionSchema(fileName, r.getLineNumber(), stride, compSchemas,
                                                            compSchemas.isEmpty() ? null : vlens,
                                                            fileType.isBinary());
        if (tile != null)
            secSchema.setTile(tile);
        return secSchema;
    }


    protected DataFileSchema parseFileEntry()
            throws VNIOException
    {
        FileType type = FileType.UNKNOWN;
        DataFileSchema fileSchema;
        String name = "";
        String decimalSeparator = "";
        String entrySeparator = "";
        try {
            Vector<String[]> res = new Vector<String[]>();
            ParseResult result = processLine(line, new String[]{"file"}, new String[]{"end"}, res);
            switch (result) {
                case ACCEPTED:
                    if (res.get(0).length < 3)
                        throw new VNIOException("invalid data file entry: no file type specified ", fileName, r.getLineNumber());
                    String[] strings = res.get(0);
                    name = strings[1];
                    if (strings.length > 3)
                        type = FileType.getType(strings[2], strings[3]);
                    else
                        type = FileType.getType(strings[2], null);
                    if (type == FileType.UNKNOWN)
                        throw new VNIOException("invalid data file entry: unknown file type specified ", fileName, r.getLineNumber());
                    if (strings.length > 4) {
                        int startSeparatorEntry = FileType.isSubprefix(strings[3]) ? 4 : 3;
                        if (strings.length > startSeparatorEntry + 2) {
                            if (strings[startSeparatorEntry].toLowerCase().startsWith("sep"))
                                entrySeparator = strings[startSeparatorEntry + 1];
                            if (strings[startSeparatorEntry].toLowerCase().startsWith("dec"))
                                decimalSeparator = strings[startSeparatorEntry + 1];
                            startSeparatorEntry += 2;
                            if (strings.length > startSeparatorEntry + 2) {
                                if (strings[startSeparatorEntry].toLowerCase().startsWith("sep"))
                                    entrySeparator = strings[startSeparatorEntry + 1];
                                if (strings[startSeparatorEntry].toLowerCase().startsWith("dec"))
                                    decimalSeparator = strings[startSeparatorEntry + 1];
                            }
                        }
                    }
                    try {
                        String filePath = new File(name).isAbsolute() ? name :
                                          headerFile.getParent() + File.separator + name;
                        File f = new File(filePath);
                        if (!f.exists()) {
                            throw new VNIOException("file does not exist: ", fileName, r.getLineNumber());
                        }
                        if (f.isDirectory()) {
                            throw new VNIOException("file is a directory: ", fileName, r.getLineNumber());
                        }
                        if (!f.canRead())  {
                            throw new VNIOException("can not read file: ", fileName, r.getLineNumber());
                        }
                    } catch (Exception e) {
                        throw new VNIOException("file error ", fileName, r.getLineNumber());
                    }
                    break;
                case EOF:
                    return null;
                case ERROR:
                     throw new VNIOException("invalid keyword ", fileName, r.getLineNumber());
                case BREAK:
                    throw new VNIOException("invalid data file entry ", fileName, r.getLineNumber());
                case UNKNOWN:
                    throw new VNIOException("invalid data file entry ", fileName, r.getLineNumber());
                default:
                    break;
            }
            fileSchema = new DataFileSchema(name, type, decimalSeparator, entrySeparator);
            fileSectionLoop:
            while (true) {
                line = nextLine();
                result = processLine(line, names, new String[]{"file", "end"}, res);
                switch (result) {
                    case ACCEPTED:
                        if (res.get(0)[0].equalsIgnoreCase("skip")) {
                            SkipSchema sSch = parseSkipSection(res.get(0));
                            if (sSch != null)
                                fileSchema.addSection(sSch);
                        } else if (res.get(0)[0].equalsIgnoreCase("timestep"))
                            fileSchema.addSection(parseTimestepEntry(type));
                        else
                            fileSchema.addSection(parseFileSectionEntry(type, res));
                        break;
                    case EOF:
                        return fileSchema;
                    case ERROR:
                        throw new VNIOException("bad file section entry ", fileName, r.getLineNumber());
                    case BREAK:
                        return fileSchema;
                    case UNKNOWN:
                        throw new VNIOException("bad data file entry ", fileName, r.getLineNumber());
                    default:
                        break fileSectionLoop;
                }
            }
            return fileSchema;
        } catch (Exception e) {
            if (e instanceof VNIOException)
                throw e;
            throw new VNIOException("error parsing file entry ", fileName, r.getLineNumber(), e);
        }

    }

    protected int parseOffset(String[] strings, FileType fileType, int cOffset, int[] offsets)
            throws VNIOException
    {
        if (fileType == FileType.ASCII_FIXED_COLUMN) {
            String[] keyData = strings[1].split("-");
            try {
                if (keyData[0].startsWith("+")) {
                    offsets[0] = cOffset + Integer.parseInt(keyData[0]);
                    offsets[1] = offsets[0] + Integer.parseInt(keyData[1]);
                } else {
                    offsets[0] = Integer.parseInt(keyData[0]);
                    offsets[1] = Integer.parseInt(keyData[1]);
                }
                cOffset = offsets[1];
            } catch (NumberFormatException e) {
                throw new VNIOException(" error in fixed char columns offset ", fileName, r.getLineNumber());
            }
        } else {
            if (strings.length > 1)
                try {
                    if (strings[1].startsWith("+"))
                        offsets[0] = cOffset + Integer.parseInt(strings[1]);
                    else {
                        offsets[0] = Integer.parseInt(strings[1]);
                        if (offsets[0] < 0) {
                            throw new VNIOException(" error in columns offset ", fileName, r.getLineNumber());
                        }
                    }
                } catch (NumberFormatException e) {
                    throw new VNIOException(" error in columns offset ", fileName, r.getLineNumber());
                }
            else
                offsets[0] = cOffset;
        }
        return cOffset;
    }

    protected int parseComponentSchema(DataContainer field, String[] strings, FileType fileType, Vector<DataElementIOSchema> compSchemas, int cOffset)
            throws VNIOException
    {
        int crd = 0, cmp = -1, offUnitMult = 1;
        String[] keyData = strings[0].split("\\.");
        int nData = field.getNComponents();
        if (keyData.length == 1) {
            for (int j = 0; j < names.length; j++)
                if (DataArraySchema.correctDataArrayName(keyData[0]).equalsIgnoreCase(names[j]) ||
                                                         j >= nData && keyData[0].startsWith(names[j])) {
                    cmp = j;
                    if (cmp < field.getNComponents() && field.getComponent(cmp).getType() == FIELD_DATA_COMPLEX)
                        offUnitMult = 2;
                    crd = -1;
                    break;
                }
        } else {
            try {
                crd = Integer.parseInt(keyData[keyData.length - 1]);
            } catch (NumberFormatException e) {
                crd = -1;
            }
            String cmpName = keyData[keyData.length - 2];
            if (crd == -1)
                cmpName = keyData[keyData.length - 1];
            for (int j = 0; j < names.length; j++)
                if (DataArraySchema.correctDataArrayName(cmpName).equalsIgnoreCase(names[j])) {
                    cmp = j;
                    break;
                }
        }
        if (cmp == -1)
            throw new VNIOException("invalid component name", fileName, r.getLineNumber());

        int[] offsets = {-1, -1};
        cOffset = parseOffset(strings, fileType, cOffset, offsets);
        ComponentIOSchema cmpSchema;
        if (cmp < field.getNComponents()) {
            DataArray da = field.getComponent(cmp);
            cmpSchema = new ComponentIOSchema(field, cmp, crd, da.getType(),
                    da.getVectorLength(), (int) field.getNElements(), offsets[0], offsets[1]);
            cmpSchema.setCmpName(da.getName());
            parsedSchema.dataElementFound(da.getName());
        } else if (cmp == field.getNComponents()) {
            cmpSchema = new ComponentIOSchema(field, cmp, crd, DataArrayType.FIELD_DATA_FLOAT,
                    3, (int) field.getNElements(), offsets[0], offsets[1]);
            parsedSchema.dataElementFound("coords");
            cmpSchema.setCmpName(names[cmp]);
        } else {
            cmpSchema = new ComponentIOSchema(field, cmp, crd, DataArrayType.FIELD_DATA_LOGIC,
                    1, (int) field.getNElements(), offsets[0], offsets[1]);
            cmpSchema.setCmpName(names[cmp + 1]);
            parsedSchema.dataElementFound("mask");
        }
        compSchemas.add(cmpSchema);
        int offUnit = 1;
        if (fileType.isBinary())
            offUnit = types[cmp].getSize();
        if (crd == -1)
            return cOffset + offUnitMult * offUnit * vlens[cmp];
        else
            return cOffset + offUnitMult * offUnit;
    }

    /**
     * @return the rangesSet
     */
    public boolean isRangesSet() {
        return rangesSet;
    }
}
