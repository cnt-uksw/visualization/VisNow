/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.basic.readers.ReadVisNowField;

import org.apache.log4j.Logger;
import java.io.File;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Vector;
import org.visnow.jscic.PointField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.DataElementIOSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.DataFileSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FilePartSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FileSectionSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FileType;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.PointFieldIOSchema;
import org.visnow.vn.lib.utils.io.VNIOException;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class PointFieldHeaderParser extends HeaderParser
{
    private static final Logger LOGGER = Logger.getLogger(PointFieldHeaderParser.class);

    String[] axesNames = new String[]{"x", "y", "z"};

    //   private String line = "";
    protected PointField pointField;

    public PointFieldHeaderParser(LineNumberReader r, File headerFile, String fileName)
    {
        super(r, headerFile, fileName);
    }

    public PointFieldIOSchema parseHeader()
            throws VNIOException
    {
        PointFieldIOSchema schema;
        Vector<String[]> res = new Vector<>();
        String name = "";
        long nnodes = 0;
        String[] userData = null;
        boolean hasMask = false;
        try {
            line = nextLine();
            ParseResult result = processLine(line, new String[]{"field", "name"},
                                     new String[]{"c", "file"}, res);
            switch (result) {
                case ACCEPTED:
                    for (int i = 0; i < res.size(); i++) {
                        String[] strings = res.get(i);
                        if ((strings[0].startsWith("name") || strings[0].startsWith("field")) && strings.length > 1)
                            name = strings[1];
                        else if (strings[0].startsWith("nnodes") || strings[0].startsWith("nodes") || strings[0].startsWith("el")) {
                            if (strings.length < 2) {
                                throw new VNIOException("no nodes count specified", fileName, r.getLineNumber());
                            }
                            try {
                                nnodes = Long.parseLong(strings[1]);
                            } catch (NumberFormatException e) {
                                throw new VNIOException("node count " + strings[1] + " is not integer ", fileName, r.getLineNumber());
                            }
                        } else if (strings[0].startsWith("valid") || strings[0].startsWith("mask"))
                            hasMask = true;
                        else  if (strings[0].startsWith("user:")) {
                            userData = strings[0].substring(5).split(";");
                            for (int j = 0; j < userData.length; j++) {
                                if (userData[j].startsWith("__"))
                                    try {
                                        int k = Integer.parseInt(userData[j].substring(2));
                                        userData[j] = stringsInLine[k];
                                    } catch (NumberFormatException e) {
                                    }
                            }
                        }
                    }
                    break;
                case EOF:
                    throw new VNIOException("no field description line ", fileName, r.getLineNumber());
                case ERROR:
                    throw new VNIOException("bad field file ", fileName, r.getLineNumber());
                case BREAK:
                    throw new VNIOException("no field description line ", fileName, r.getLineNumber());
                default:
                    break;
            }

            if (nnodes < 1)
                throw new VNIOException("no nodes count specified ", fileName, r.getLineNumber());
            pointField = new PointField(nnodes);
            pointField.setName(name);
            if (hasMask)
                pointField.setCurrentMask(new LogicLargeArray(pointField.getNNodes(), true));
            pointField.setUserData(userData);
            schema = new PointFieldIOSchema(pointField, headerFile, fileName);
            line = nextLine();
            if (processLine(line, new String[]{"time"}, new String[]{"c"}, res) == ParseResult.ACCEPTED) {
                if (res.get(0).length == 3 && res.get(0)[0].startsWith("time") && res.get(0)[1].startsWith("unit"))
                    pointField.setTimeUnit(res.get(0)[2]);
                LOGGER.info("time unit " + res.get(0)[2]);
                nextLine();
            }
            boolean isUserExtent = false;
            float[][] user_extents = new float[][]{{-1, -1, -1}, {1, 1, 1}};
            user_extent_loop:
            while (true) {
                result = processLine(line, axesNames, new String[]{"c", "f", "v", "r", "o", "t"}, res);
                switch (result) {
                    case ACCEPTED:
                        try {
                            for (int i = 0; i < 3; i++)
                                if (res.get(0).length >= 5 && res.get(0)[2].startsWith(axesNames[i])) {
                                    user_extents[0][i] = Float.parseFloat(res.get(0)[3]);
                                    user_extents[1][i] = Float.parseFloat(res.get(0)[4]);
                                    isUserExtent = true;
                                    break;
                                }
                        } catch (NumberFormatException e) {
                            throw new VNIOException("invalid extents line ", fileName, r.getLineNumber());
                        }
                        break;
                    case EOF:
                        throw new VNIOException("no data section ", fileName, r.getLineNumber());
                    case ERROR:
                        throw new VNIOException("bad keyword in user extents section ", fileName, r.getLineNumber());
                    case BREAK:
                        break user_extent_loop;
                    default:
                        break;
                }
                line = nextLine();
            }
            if (isUserExtent)
                pointField.setPreferredExtents(pointField.getPreferredExtents(), user_extents);
            Vector<String> tNames = new Vector<>();
            Vector<DataArrayType> tTypes = new Vector<>();
            Vector<Integer> tVlens = new Vector<>();
            DataArray currentComponent = null;
            component_loop:
            while ((currentComponent = parseComponentEntry(line, pointField.getNNodes(),
                                                           fileName, r.getLineNumber())) != null) {
                for (int i = 0; i < tNames.size(); i++)
                    if (currentComponent.getName().equalsIgnoreCase(tNames.get(i)))
                        throw new VNIOException("duplicate component name " + currentComponent.getName(), fileName, r.getLineNumber());
                tNames.add(currentComponent.getName());
                tTypes.add(currentComponent.getType());
                tVlens.add(currentComponent.getVectorLength());
                pointField.addComponent(currentComponent);
                line = nextLine();
            }

            tNames.add("coord");
            tTypes.add(DataArrayType.FIELD_DATA_FLOAT);
            tVlens.add(3);

            tNames.add("mask");
            tTypes.add(DataArrayType.FIELD_DATA_LOGIC);
            tVlens.add(1);

            String[] specialNames = new String[]{"skip", "stride", "sep", "tile", "timestep", "repeat"};
            names = new String[tNames.size() + specialNames.length];
            for (int i = 0; i < tNames.size(); i++)
                names[i] = tNames.get(i);
            System.arraycopy(specialNames, 0, names, tNames.size(), specialNames.length);
            types = new DataArrayType[tTypes.size()];
            for (int i = 0; i < types.length; i++)
                types[i] = tTypes.get(i);
            vlens = new int[tVlens.size()];
            for (int i = 0; i < vlens.length; i++)
                vlens[i] = tVlens.get(i);
            DataFileSchema dataFileSchema;
            schema.generateDataInputStatus();
            parsedSchema = schema;

            file_loop:
            while ((dataFileSchema = parseFileEntry()) != null)
                schema.addFileSchema(dataFileSchema);
            r.close();

        } catch (IOException e) {
            throw new VNIOException("bad header file ", fileName, r.getLineNumber());
        }
        return schema;
    }

    @Override
    protected FilePartSchema parseFileSectionEntry(FileType fileType, Vector<String[]> tokens)
            throws VNIOException
    {
        int stride = -1;
        int[][] tile = null;
        String separator = "";
        int cOffset = 0;
        Vector<DataElementIOSchema> compSchemas = new Vector<DataElementIOSchema>();

        for (int i = 0; i < tokens.size(); i++) {
            String[] strings = tokens.get(i); // parsing i-th item with file part description line
            if (strings[0].startsWith("stride") && strings.length > 1)
                stride = Integer.parseInt(strings[1]);
            else if (strings[0].startsWith("separator") && strings.length > 1)
                separator = strings[1];
            else
                cOffset = parseComponentSchema(pointField, strings, fileType, compSchemas, cOffset);
        }
        if (stride == -1)
            stride = cOffset;
        FileSectionSchema secSchema;
        if (compSchemas.isEmpty())
            secSchema = new FileSectionSchema(fileName, r.getLineNumber(), stride, compSchemas, null, fileType.isBinary());
        else
            secSchema = new FileSectionSchema(fileName, r.getLineNumber(), stride, compSchemas, vlens, fileType.isBinary());
        return secSchema;
    }

}
