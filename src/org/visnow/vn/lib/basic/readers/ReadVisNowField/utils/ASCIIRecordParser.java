/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 

package org.visnow.vn.lib.basic.readers.ReadVisNowField.utils;

import java.util.Arrays;
import java.util.Scanner;
import java.util.Vector;
import org.visnow.jlargearrays.ByteLargeArray;
import org.visnow.jlargearrays.ComplexDoubleLargeArray;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.StringLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 * Description of a field section record holding data for a field/data container element
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ASCIIRecordParser
{
    /**
     * Single item within a data record:
     * data is an coords/mask/data array content that will hold this record read in from a file; if data == null, this Item denotes amount of data to be skipped
     * veclen is the vector length of the data array or an implicit vector length in the case of coords/mask/cell set nodes array
     * length is an actual number of data in this record item (usually 1 or veclen)
     * position is the first data position
     *
     */
    public static class Item
    {
        public final LargeArray data;
        public final int veclen;
        public final int length;
        public final int position;

        public Item(LargeArray data, int veclen, int length, int position)
        {
            this.data = data;
            this.veclen = veclen;
            this.length = length;
            this.position = position;
        }

        public Item(Field field, float time, String s)
        {
            if (s.isEmpty()) {
                data = null;
                veclen = 0;
                length = 1;
                position = 0;
                return;
            }
            String[] split = s.split("\\.");
            if (split[0].toLowerCase().startsWith("coord"))
            {
                data = field.produceCoords(time);
                veclen = 3;
                if (split.length == 1) {
                    length = veclen;
                    position = 0;
                }
                else {
                    length = 1;
                    position = Integer.parseInt(split[1]);
                }
                return;
            }
            if (split[0].toLowerCase().startsWith("mask"))
            {
                data = field.produceMask(time);
                length = veclen = 1;
                position = 0;
                return;
            }
            DataArray da = field.getComponent(split[0]);
            if (da == null) {
                int k = 1;
                try {
                    k = Integer.parseInt(split[0]);
                } catch (NumberFormatException e) {
                }
                data = null;
                length = veclen = k;
                position = 0;
                return;
            }
            data = da.produceData(time);
            veclen = da.getVectorLength();
            int mult = 1;
            if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX)
                mult = 2;
            if (split.length == 1) {
                length = mult * veclen;
                position = 0;
            }
            else {
                length = mult;
                position = Integer.parseInt(split[1]);
            }
        }

        public Item(CellSet set, float time, String s)
        {
            if (s.isEmpty()) {
                data = null;
                veclen = 0;
                length = 1;
                position = 0;
                return;
            }
            String[] des = s.split(":");
            if (des.length == 1 || !des[0].equalsIgnoreCase(set.getName())) {
                int k = 1;
                try {
                    k = Integer.parseInt(des[0]);
                } catch (NumberFormatException e) {
                }
                data = null;
                veclen = k;
                length = veclen;
                position = 0;
                return;
            }
            if (des.length == 3) {
                int vl = -1;
                int[] dta = null;
                byte[] ori = null;
                String[] arDes = des[2].toLowerCase().trim().split("\\.");
                for (CellArray c : set.getCellArrays()) {
                    if (c == null)
                        continue;
                    for (String nameAnalogy : c.getType().getNameAnalogies())
                        if (nameAnalogy.equalsIgnoreCase(des[1]) && c.getDim() >= 0) {
                            if (arDes[0].startsWith("nod")){
                                dta = c.getNodes();
                                vl = c.getNCellNodes();
                            }
                            else if (arDes[0].startsWith("ori")){
                                ori = c.getOrientations();
                                vl = 1;
                            }
                            else if (arDes[0].startsWith("ind")){
                                dta = c.getDataIndices();
                                vl = 1;
                                break;
                            }
                        }
                    if (vl != -1) {
                        if (dta != null)
                            data = new IntLargeArray(dta);
                        else
                            data = new ByteLargeArray(ori);
                        veclen = vl;
                        if (arDes.length == 1) {
                            length = vl;
                            position = 0;
                        }
                        else {
                            length = 1;
                            position = Integer.parseInt(arDes[1]);
                        }
                        return;
                    }
                }
                data = null;
                length = veclen = 1;
                position = 0;
                return;
            }
            String[] arDes = des[2].toLowerCase().trim().split("\\.");
            DataArray da = set.getComponent(arDes[0]);
            if (da == null) {
                int k = 1;
                try {
                    k = Integer.parseInt(arDes[0]);
                } catch (NumberFormatException e) {
                }
                data = null;
                veclen = k;
            }
            else {
                data = da.produceData(time);
                veclen = da.getVectorLength();
            }
            if (arDes.length == 1) {
                length = veclen;
                position = 0;
            }
            else {
                length = 1;
                position = Integer.parseInt(arDes[1]);
            }
        }
    }

    public static enum ReadResult {SUCCESS, FORMAT_ERROR, TOO_FEW_ITEMS, TOO_MANY_ITEMS};

    public final Item[] items;
    public final int[][] boundaries;

    public ASCIIRecordParser(Item[] items, int[][] boundaries)
    {
        this.items = items;
        if (boundaries != null && boundaries.length == items.length)
            this.boundaries = boundaries;
        else
            this.boundaries = null;
    }

    public ASCIIRecordParser(DataContainer container, float time, String s, int[][] boundaries)
    {
        String[] itemStrings = s.trim().split(", *");
        items = new Item[itemStrings.length];
        for (int i = 0; i < itemStrings.length; i++)
            items[i] = container instanceof CellSet ?
                       new Item((CellSet) container, time, itemStrings[i].trim()) :
                       new Item((Field)container,    time, itemStrings[i].trim());
        this.boundaries = boundaries;
    }

    public ASCIIRecordParser(RegularField inField, float time, String s, int[][] boundaries)
    {
        String[] itemStrings = s.trim().split(", *");
        items = new Item[itemStrings.length];
        for (int i = 0; i < itemStrings.length; i++)
            items[i] = new Item(inField,
                    time, itemStrings[i].trim());
        this.boundaries = boundaries;
    }

    public int length()
    {
        int l = 0;
        for (Item item : items)
            l += item.length;
        return l;
    }

    /**
     * returns description of the i-th item
     * @param i item position
     * @return  i-th item description
     */
    public Item getItem(int i)
    {
        return items[i];
    }

    private ReadResult readRecord(String[] vals, long element)
    {
        ReadResult result = ReadResult.SUCCESS;
        if (vals.length > length())
            result = ReadResult.TOO_MANY_ITEMS;
        if (vals.length < length())
            result = ReadResult.TOO_FEW_ITEMS;
        int recordPos = 0;
        int itemsToRead = 0, valsToRead = 0;
        for (itemsToRead = 0; itemsToRead < items.length; itemsToRead++)
            if (valsToRead + items[itemsToRead].length > vals.length)
                break;
        for (int i = 0; i < itemsToRead; i++) {
            Item currentItem = items[i];
            if (currentItem.data == null) {
                recordPos += currentItem.length;
                continue;                            // as _data_ == null this signals that next _length_ items are to be skipped
            }
            try {
                switch (currentItem.data.getType()) {
                case LOGIC:
                    for (int j = 0; j < currentItem.length; j++, recordPos++)
                        if (vals[recordPos].equalsIgnoreCase("t") || vals[recordPos].equals(1))
                            currentItem.data.setBoolean(element * currentItem.veclen + currentItem.position + j, true);
                        else if (vals[recordPos].equalsIgnoreCase("f") || vals[recordPos].equals(0))
                            currentItem.data.setBoolean(element * currentItem.veclen + currentItem.position + j, false);
                    break;
                case BYTE:
                case UNSIGNED_BYTE:
                case SHORT:
                case INT:
                    for (int j = 0; j < currentItem.length; j++, recordPos++)
                        currentItem.data.setInt(element * currentItem.veclen + currentItem.position + j, Integer.parseInt(vals[recordPos]));
                    break;
                case  LONG:
                    for (int j = 0; j < currentItem.length; j++, recordPos++)
                        currentItem.data.setLong(element * currentItem.veclen + currentItem.position + j, Long.parseLong(vals[recordPos]));
                    break;
                case FLOAT:
                case DOUBLE:
                    for (int j = 0; j < currentItem.length; j++, recordPos++)
                        currentItem.data.setDouble(element * currentItem.veclen + currentItem.position + j, Double.parseDouble(vals[recordPos]));
                    break;
                case COMPLEX_FLOAT:
                    for (int j = 0; j < currentItem.length / 2; j++, recordPos += 2)
                        ((ComplexFloatLargeArray)currentItem.data).
                                setComplexFloat(element * currentItem.veclen + currentItem.position + j,
                                                new float[] {Float.parseFloat(vals[recordPos]), Float.parseFloat(vals[recordPos + 1])});
                    break;
                case COMPLEX_DOUBLE:
                    for (int j = 0; j < currentItem.length / 2; j++, recordPos += 2)
                        ((ComplexDoubleLargeArray)currentItem.data).
                                setComplexDouble(element * currentItem.veclen + currentItem.position + j,
                                                 new double[] {Double.parseDouble(vals[recordPos]), Double.parseDouble(vals[recordPos + 1])});
                    break;
                case STRING:
                    for (int j = 0; j < currentItem.length; j++, recordPos++)
                        ((StringLargeArray)currentItem.data).set(element * currentItem.veclen + currentItem.position + j, vals[recordPos]);
                    break;

                }
            } catch (NumberFormatException e) {
                result = ReadResult.FORMAT_ERROR;
            }
        }
        return result;
    }

    /**
     * finds all quote enclosed substrings in processedLine[0] and replaces each such
     * substring by ___n, where n is its number; collects the substrings found in the vector substrings
     * @param processedLine - processed string
     * @param substrings    vector of substrings found in the processed string - must be a non-null empty vector
     * @return processedLine after substitution of ll quota enclosed substring by ___0, ___1,...
     */
    public static String findSubstrings(String processedLine, Vector<String> substrings)
    {
        String[] stringsInLine = processedLine.split("\"");
        substrings.clear();
        if (stringsInLine.length >= 2) {
            for (int i = 1; i < stringsInLine.length; i += 2) {
                substrings.add(stringsInLine[i]);
                processedLine = processedLine.replaceFirst("\"[^\"]*\"", "___" + (i / 2));
            }
        }
        return processedLine;
    }

    /**
     * Interprets a single input line
     * @param inLine     an input line to be interpreted
     * @param delimiter  a regular expression describing item delimiters
     * @param element    the position of data (e.g. node index)
     * @return           SUCCESS        if exact number of required items has been found and successfully parsed,
     *                   FORMAT_ERROR   if improperly formatted item has been found,
     *                   TOO_FEW_ITEMS  if the token count is less than the required items count,
     *                   TOO_MANY_ITEMS if some data hes been left after parsing the record
     */
    public ReadResult readRecordFromLine(String inLine, String delimiter, long element)
    {
        Vector<String> substrings = new Vector<>();
        String line = findSubstrings(inLine, substrings);
        String[] vals = line.split(delimiter);
        for (int i = 0; i < vals.length; i++)
            if (vals[i].startsWith("___"))
                vals[i] = substrings.get(Integer.parseInt(vals[i].substring(3)));
        return readRecord(vals, element);
    }

    /**
     * Interprets a single input line
     * @param inLine     an input line to be interpreted
     * @param delimiter  a regular expression describing item delimiters
     * @param element    the position of data (e.g. node index)
     * @return           SUCCESS        if exact number of required items has been found and successfully parsed,
     *                   FORMAT_ERROR   if improperly formatted item has been found,
     *                   TOO_FEW_ITEMS  if the token count is less than the required items count,
     *                   TOO_MANY_ITEMS if some data hes been left after parsing the record
     */
    public ReadResult readRecordFromFixedPositionLine(String line, long element)
    {
        String[] vals = new String[items.length];
        for (int i = 0; i < vals.length; i++)
            vals[i] = line.substring(boundaries[i][0], boundaries[i][1]);
        return readRecord(vals, element);
    }

    /**
     * Scans the input for a series of tokens and interprets them
     * @param in         a Scaner object providing input from the current position
     * @param delimiter  a regular expression describing item delimiters
     * @param element    the position of data (e.g. node index)
     * @return           SUCCESS        if exact number of required items has been found and successfully parsed,
     *                   FORMAT_ERROR   if improperly formatted item has been found,
     *                   TOO_FEW_ITEMS  if the token count is less than the required items count
     */
    public ReadResult readRecordFromScanner(Scanner in, String delimiter, long element)
    {
        String[] vals = new String[length()];
        for (int i = 0; i < vals.length; i++) {
            String val = in.next();
            if (val.startsWith("\"")) {
                val = val.substring(1);
                if (val.endsWith("\""))
                    vals[i] = val.substring(0, val.length() - 1);
                else {
                    StringBuilder b = new StringBuilder();
                    do {
                        try {
                            b.append(val).append(" ");
                            val = in.next();
                        } catch (Exception e) {
                            break;
                        }
                    } while (val != null && !val.isEmpty() && !val.endsWith("\""));
                    if (val != null && val.endsWith("\""))
                        b.append(val.substring(0, val.length() - 1));
                    vals[i] = b.toString();

                }
            }
            else
                vals[i] = val;
        }
        return readRecord(vals, element);
    }



    public static void main(String[] args)
    {
        String parserDesc;
        byte[]   bLA = new byte[4];
        int[]    iLA = new int[8];
        float[]  fLA = new float[12];
        String[] sLA = new String[4];
        double[] dLA = new double[4];
        float[] reC  = new float[8];
        float[] imC  = new float[8];
        String[] in = new String[] {
            "1 2 3 4 5 6 aqq 999 0 1 2 3",
            "6 5 4 \"3\" 2 1 bqq 777 0 1 2 3",
            "2 4 5 7 8 9 \"foo foo foo\" 444 5 7 8 9",
            "2 4 7 8 9 0 \"foo bar\" 456 7 8 9 0"
        };
        String inStream = "1 2 3 4 5 6 aqq 999 0 1 2 3 " +
                          "6 5 4 \"3\" 2 1 bqq 777 0 1 2 3 " +
                          "2 4 5 7 8 9 \"foo foo foo\" 444 5 7 8 9 " +
                          "2 4 7 8 9 0 \"foo bar\" 456 7 8 9 0 ";
        RegularField out = new RegularField(new int[] {2,2});
        out.addComponent(DataArray.create(bLA, 1, "bd"));
        out.addComponent(DataArray.create(iLA, 2, "id"));
        out.addComponent(DataArray.create(fLA, 3, "fd"));
        out.addComponent(DataArray.create(sLA, 1, "sd"));
        out.addComponent(DataArray.create(dLA, 1, "dd"));
        ComplexFloatLargeArray cfLA = new ComplexFloatLargeArray(reC, imC);
        out.addComponent(DataArray.create(cfLA, 2, "cd"));

        parserDesc = "bd,id,fd,sd,dd,cd";
        System.out.println(parserDesc);
        ASCIIRecordParser schema = new ASCIIRecordParser(out, 0, parserDesc, null);
        for (int i = 0; i < in.length; i++) {
            schema.readRecordFromLine(in[i], " ", i);
            System.out.printf("%3d    %5d %5d    %8.3f %8.3f %8.3f     %15s  %8.3f     %8.3f %8.3f    %8.3f %8.3f%n",
                              bLA[i],
                              iLA[2 * i], iLA[2 * i + 1],
                              fLA[3 * i], fLA[3 * i + 1], fLA[3 * i + 2],
                              sLA[i], dLA[i], reC[2 * i], imC[2 * i], reC[2 * i + 1], imC[2 * i + 1]);
        }
        Arrays.fill(iLA, 0);
        Arrays.fill(fLA, 0);
        Arrays.fill(dLA, 0);
        Arrays.fill(sLA, "");
        Arrays.fill(reC, 0);
        Arrays.fill(imC, 0);
        System.out.println("");

        parserDesc = "bd,id.0,id.1,,,,sd,dd,cd";
        System.out.println(parserDesc);
        schema = new ASCIIRecordParser(out, 0, parserDesc, null);
        for (int i = 0; i < in.length; i++) {
            schema.readRecordFromLine(in[i], " ", i);
            System.out.printf("%3d    %5d %5d    %8.3f %8.3f %8.3f     %15s  %8.3f     %8.3f %8.3f    %8.3f %8.3f%n",
                              bLA[i],
                              iLA[2 * i], iLA[2*i + 1],
                              fLA[3 * i], fLA[3 * i + 1], fLA[3 * i + 2],
                              sLA[i], dLA[i], reC[2 * i], imC[2 * i], reC[2 * i + 1], imC[2 * i + 1]);
        }
        Arrays.fill(iLA, 0);
        Arrays.fill(fLA, 0);
        Arrays.fill(dLA, 0);
        Arrays.fill(sLA, "");
        Arrays.fill(reC, 0);
        Arrays.fill(imC, 0);

        System.out.println("");
        parserDesc = "bd,2,fd,,dd";
        System.out.println(parserDesc);
        schema = new ASCIIRecordParser(out, 0, parserDesc, null);
        Arrays.fill(iLA, 0);
        Arrays.fill(fLA, 0);
        for (int i = 0; i < in.length; i++) {
            schema.readRecordFromLine(in[i], " ", i);
            System.out.printf("%3d    %5d %5d    %8.3f %8.3f %8.3f     %15s  %8.3f     %8.3f %8.3f    %8.3f %8.3f%n",
                              bLA[i],
                              iLA[2 * i], iLA[2*i + 1],
                              fLA[3 * i], fLA[3 * i + 1], fLA[3 * i + 2],
                              sLA[i], dLA[i], reC[2 * i], imC[2 * i], reC[2 * i + 1], imC[2 * i + 1]);
        }

        Scanner s = new Scanner(inStream);

        parserDesc = "bd,id,fd,sd,dd,cd";
        System.out.println(parserDesc + " scanner");
        schema = new ASCIIRecordParser(out, 0, parserDesc, null);
        Arrays.fill(iLA, 0);
        Arrays.fill(fLA, 0);
        Arrays.fill(dLA, 0);
        Arrays.fill(sLA, "");
        Arrays.fill(reC, 0);
        Arrays.fill(imC, 0);
        System.out.println("");
        for (int i = 0; i < in.length; i++) {
            schema.readRecordFromScanner(s, " ", i);
            System.out.printf("%3d    %5d %5d    %8.3f %8.3f %8.3f     %15s  %8.3f     %8.3f %8.3f    %8.3f %8.3f%n",
                              bLA[i],
                              iLA[2 * i], iLA[2*i + 1],
                              fLA[3 * i], fLA[3 * i + 1], fLA[3 * i + 2],
                              sLA[i], dLA[i], reC[2 * i], imC[2 * i], reC[2 * i + 1], imC[2 * i + 1]);
        }




    }
}
