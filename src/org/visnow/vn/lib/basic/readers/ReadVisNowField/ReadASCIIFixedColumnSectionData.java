/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.basic.readers.ReadVisNowField;

import java.io.IOException;
import java.io.LineNumberReader;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.utils.FloatingPointUtils;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.LongLargeArray;
import org.visnow.jlargearrays.ShortLargeArray;
import org.visnow.jlargearrays.StringLargeArray;
import static org.visnow.jscic.dataarrays.DataArrayType.FIELD_DATA_DOUBLE;
import static org.visnow.jscic.dataarrays.DataArrayType.FIELD_DATA_FLOAT;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FileSectionSchema;
import org.visnow.vn.lib.utils.io.FileIOUtils;
import org.visnow.vn.lib.utils.io.VNIOException;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ReadASCIIFixedColumnSectionData
{

    public static int readSectionData(SectionModel model, LineNumberReader reader, String filePath, String decimalSep)
            throws VNIOException
    {
        int nComps = model.nItems;
        int nNodes = (int)model.nData;
        int[] offsets = model.offsets;
        int[] offsets1 = model.offsets1;
        DataArrayType[] types = model.types;
        int[] vlens = model.vlens;
        long[] ind = model.ind;
        LogicLargeArray[] boolArrs = model.boolArrs;
        UnsignedByteLargeArray[] byteArrs = model.byteArrs;
        ShortLargeArray[] shortArrs =       model.shortArrs;
        IntLargeArray[] intArrs =           model.intArrs;
        LongLargeArray[] longArrs =         model.longArrs;
        FloatLargeArray[] floatArrs =       model.floatArrs;
        DoubleLargeArray[] dblArrs =        model.dblArrs;
        ComplexFloatLargeArray[] cplxArrs = model.cplxArrs;
        StringLargeArray[] strArrs =        model.strArrs;
        FileSectionSchema schema = model.sectionSchema;
        try {
            if (schema.getComponents().isEmpty()) {
                return 0;
            }
            for (int k = 0; k < nNodes; k++) {
                String line = FileIOUtils.nextLine(reader, false, false);
                if (line == null) {
                    return 1;
                }
                line = line.trim();
                for (int l = 0; l < nComps; l++) {
                    String item = line.substring(offsets[l], offsets1[l]);
                    item = item.trim();
                    String tmp;
                    switch (types[l]) {
                        case FIELD_DATA_LOGIC:
                            boolArrs[l].setBoolean(ind[l], item.startsWith("1") || item.startsWith("t"));
                            ind[l] += vlens[l];
                            break;
                        case FIELD_DATA_BYTE:
                            byteArrs[l].setByte(ind[l], (byte) (0xff & Integer.parseInt(item)));
                            ind[l] += vlens[l];
                            break;
                        case FIELD_DATA_SHORT:
                            shortArrs[l].setShort(ind[l], Short.parseShort(item));
                            ind[l] += vlens[l];
                            break;
                        case FIELD_DATA_INT:
                            intArrs[l].setInt(ind[l], Integer.parseInt(item));
                            ind[l] += vlens[l];
                            break;
                        case FIELD_DATA_LONG:
                            longArrs[l].setLong(ind[l], Long.parseLong(item));
                            ind[l] += vlens[l];
                            break;
                        case FIELD_DATA_FLOAT:
                            tmp = item;
                            if (decimalSep != null) {
                                tmp = tmp.replaceAll(decimalSep, ".");
                            }
                            floatArrs[l].setFloat(ind[l], Float.parseFloat(tmp));
                            ind[l] += vlens[l];
                            break;
                        case FIELD_DATA_DOUBLE:
                            tmp = item;
                            if (decimalSep != null) {
                                tmp = tmp.replaceAll(decimalSep, ".");
                            }
                            dblArrs[l].setDouble(ind[l], Double.parseDouble(tmp));
                            ind[l] += vlens[l];
                            break;
                        case FIELD_DATA_COMPLEX:
                            tmp = item;
                            if (decimalSep != null) {
                                tmp = tmp.replaceAll(decimalSep, ".");
                            }
                            String item1 = line.substring(offsets[l], offsets1[l]).trim();
                            String tmp1 = item1;
                            if (decimalSep != null) {
                                tmp1 = tmp1.replaceAll(decimalSep, ".");
                            }
                            cplxArrs[l].setComplexFloat(ind[l], new float[] {Float.parseFloat(tmp), Float.parseFloat(tmp1)});
                            l += 1;
                            break;
                        case FIELD_DATA_STRING:
                            strArrs[l].set(ind[l], item.replaceAll("_;_", "\n"));
                            break;
                    }
                }
            }
            for (int l = 0; l < nComps; l++)
                if ((types[l] == FIELD_DATA_FLOAT || types[l] == FIELD_DATA_DOUBLE) &&
                    !FloatingPointUtils.processNaNs(floatArrs[l], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction))
                    throw new VNIOException("dataset contains NaN, Float.POSITIVE_INFINITY or Float.NEGATIVE_INFINITY elements",
                    filePath, reader.getLineNumber(), schema.getHeaderFile(), schema.getHeaderLine());
            return 0;
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new VNIOException("error in data file: line too short ", filePath, reader.getLineNumber(), schema.getHeaderFile(), schema.getHeaderLine());
        } catch (NumberFormatException e) {
            throw new VNIOException("error in data file: bad number format ", filePath, reader.getLineNumber(), schema.getHeaderFile(), schema.getHeaderLine());
        } catch (IOException e) {
            throw new VNIOException("error in data file ", filePath, reader.getLineNumber(), schema.getHeaderFile(), schema.getHeaderLine());
        }

    }
}
