/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.basic.readers.ReadVisNowField;

import org.apache.log4j.Logger;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.RegularFieldIOSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.DataFileSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FileSectionSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FilePartSchema;
import java.io.File;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Vector;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.DataElementIOSchema;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jscic.utils.MatrixMath;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FileType;
import org.visnow.vn.lib.utils.io.VNIOException;
/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class RegularFieldHeaderParser extends HeaderParser
{
    private static final Logger LOGGER = Logger.getLogger(RegularFieldHeaderParser.class);

    private RegularField regularField;
    private boolean extentsRead = false, affineRead = false, userExtentsRead = false;
    public RegularFieldHeaderParser(LineNumberReader r, File headerFile, String fileName)
    {
        super(r, headerFile, fileName);
    }
    public RegularFieldIOSchema parseHeader()
            throws VNIOException
    {
        Vector<String[]> res = new Vector<String[]>();
        ParseResult result;
        RegularFieldIOSchema schema;
        String name = "";
        int[] dims = null;
        float[][] affine = new float[][]{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {0, 0, 0}};
        float[][] extents = new float[][]{{-1, -1, -1}, {1, 1, 1}};
        String[] axesNames = new String[]{"x", "y", "z"};
        String[] affineEntries = new String[]{"v0", "v1", "v2", "orig"};
        String[] affineExtentEntries = new String[]{"r0", "r1", "r2", "orig"};
        String[] userData = null;
        boolean hasCoords = false, hasMask = false;
        int nDims = -1;
        try {
            line = nextLine();
            result = processLine(line, new String[]{"dim", "field", "name"},
                    new String[]{"c", "file", "x", "y", "z", "v", "o", "t"}, res);
            switch (result) {
                case ACCEPTED:
                    for (int i = 0; i < res.size(); i++) {
                        String[] strings = res.get(i);
                        if ((strings[0].startsWith("name") || strings[0].startsWith("field")) && strings.length > 1)
                            name = strings[1];
                        if (strings[0].startsWith("dim")) {
                            if (strings.length < 2) {
                                throw new VNIOException("no dimensions specified after \"dim\"", fileName, r.getLineNumber());
                            }
                            nDims = strings.length - 1;
                            dims = new int[nDims];
                            for (int j = 0; j < nDims; j++)
                                try {
                                    dims[j] = Integer.parseInt(strings[j + 1]);
                                } catch (NumberFormatException e) {
                                    throw new VNIOException(strings[j + 1] + " is not integer ", fileName, r.getLineNumber());
                                }
                        }
                        if (strings[0].startsWith("valid") || strings[0].startsWith("mask"))
                            hasMask = true;
                        if (strings[0].startsWith("coord"))
                            hasCoords = true;
                        if (strings[0].startsWith("user:")) {
                            userData = strings[0].substring(5).split(";");
                            for (int j = 0; j < userData.length; j++)
                                if (userData[j].startsWith("__"))
                                    try {
                                        int k = Integer.parseInt(userData[j].substring(2));
                                        userData[j] = stringsInLine[k];
                                    } catch (NumberFormatException e) {
                                    }
                        }
                    }
                    break;
                case EOF:
                    throw new VNIOException("no field description line ", fileName, r.getLineNumber());
                case ERROR:
                    throw new VNIOException("bad field file ", fileName, r.getLineNumber());
                case BREAK:
                    throw new VNIOException("field dimensions must be specified in the first non-comment line ", fileName, r.getLineNumber());
                default:
                    break;
            }
            if (nDims > 0) {
                regularField = new RegularField(dims);
                if (name.isEmpty())
                    name = "Field name";
                regularField.setName(name);
                if (hasCoords)
                    regularField.setCurrentCoords(new FloatLargeArray(3 * regularField.getNNodes()));
                if (hasMask)
                    regularField.setCurrentMask(new LogicLargeArray(regularField.getNNodes(), true));
                regularField.setUserData(userData);
            }
            else
                throw new VNIOException("field dimensions must be specified in the first non-comment line ", fileName, r.getLineNumber());
            schema = new RegularFieldIOSchema(regularField, headerFile, fileName);
            line = nextLine();
            if (processLine(line, new String[]{"unit"}, new String[]{"c", "f", "v", "o", "x", "y", "z"}, res) == ParseResult.ACCEPTED) {
                FieldUnits units = parseUnitsEntry(line);
                regularField.setTimeUnit(units.timeUnit);
                String[] geomUnits = units.geomUnits;
                if (geomUnits[0].equals(geomUnits[1]) && geomUnits[0].equals(geomUnits[2]))
                    regularField.setCoordsUnit(geomUnits[0]);
                regularField.setCoordsUnits(units.geomUnits);
                line = nextLine();
            }
            extent_loop:
            while (true) {
                result = processLine(line, axesNames, new String[]{"c", "f", "v", "r", "o", "t", "u"}, res);
                switch (result) {
                    case ACCEPTED:
                        try {
                            for (int i = 0; i < 3; i++)
                                if (res.get(0)[0].startsWith(axesNames[i])) {
                                    extents[0][i] = Float.parseFloat(res.get(0)[1]);
                                    extents[1][i] = Float.parseFloat(res.get(0)[2]);
                                    break;
                                }
                            extentsRead = true;
                        } catch (NumberFormatException e) {
                            throw new VNIOException("invalid extents line ", fileName, r.getLineNumber());
                        }
                        break;
                    case EOF:
                        throw new VNIOException("no data section ", fileName, r.getLineNumber());
                    case ERROR:
                        throw new VNIOException("bad keyword in user extents section ", fileName, r.getLineNumber());
                    case BREAK:
                        break extent_loop;
                    default:
                        break;
                }
                line = nextLine();
            }
            affine_loop:
            while (true) {
                result = processLine(line, new String[]{"o", "v", "r"}, new String[]{"c", "f", "t", "u"}, res);
                switch (result) {
                    case ACCEPTED:
                        try {
                            for (int i = 0; i < 4; i++)
                                if (res.get(0)[0].startsWith(affineEntries[i]))
                                    for (int j = 1; j < max(3, res.get(0).length); j++)
                                        affine[i][j - 1] = Float.parseFloat(res.get(0)[j]);
                                else if (res.get(0)[0].startsWith(affineExtentEntries[i]))
                                    for (int j = 1; j < max(3, res.get(0).length); j++)
                                        affine[i][j - 1] = Float.parseFloat(res.get(0)[j]) / (regularField.getDims()[i] - 1);
                            affineRead = true;
                        } catch (NumberFormatException e) {
                            throw new VNIOException("invalid affine table entry line", fileName, r.getLineNumber());
                        }
                        break;
                    case EOF:
                        throw new VNIOException("no data section ", fileName, r.getLineNumber());
                    case ERROR:
                        throw new VNIOException("bad keyword in affine data section ", fileName, r.getLineNumber());
                    case BREAK:
                        break affine_loop;
                    default:
                        break;
                }
                line = nextLine();
            }
            float[][] user_extents = new float[][]{{-1, -1, -1}, {1, 1, 1}};
            user_extent_loop:
            while (true) {
                result = processLine(line, new String[] {"user"}, new String[]{"c", "f", "v", "r", "o", "t"}, res);
                switch (result) {
                    case ACCEPTED:
                        try {
                            for (int i = 0; i < 3; i++)
                                if (res.get(0).length >= 5 && res.get(0)[2].startsWith(axesNames[i])) {
                                    user_extents[0][i] = Float.parseFloat(res.get(0)[3]);
                                    user_extents[1][i] = Float.parseFloat(res.get(0)[4]);
                                    userExtentsRead = true;
                                    break;
                                }
                        } catch (NumberFormatException e) {
                            throw new VNIOException("invalid extents line ", fileName, r.getLineNumber());
                        }
                        break;
                    case EOF:
                        throw new VNIOException("no data section ", fileName, r.getLineNumber());
                    case ERROR:
                        throw new VNIOException("bad keyword in user extents section ", fileName, r.getLineNumber());
                    case BREAK:
                        break user_extent_loop;
                    default:
                        break;
                }
                line = nextLine();
            }
            if (extentsRead)
                regularField.setAffine(MatrixMath.computeAffineFromExtents(regularField.getDims(), extents));
            else if (affineRead)
                regularField.setAffine(affine);
            if (userExtentsRead)
                regularField.setPreferredExtents(regularField.getPreferredExtents(), user_extents);
            Vector<String> tNames = new Vector<>();
            Vector<DataArrayType> tTypes = new Vector<>();
            Vector<Integer> tVlens = new Vector<>();
            DataArray currentComponent;
            component_loop:
            while ((currentComponent = parseComponentEntry(line, regularField.getNNodes(),
                    fileName, r.getLineNumber())) != null) {
                for (String tName : tNames)
                    if (currentComponent.getName().equalsIgnoreCase(tName))
                        throw new VNIOException("duplicate component name " + currentComponent.getName(), fileName, r.getLineNumber());
                tNames.add(currentComponent.getName());
                tTypes.add(currentComponent.getType());
                tVlens.add(currentComponent.getVectorLength());
                regularField.addComponent(currentComponent);
                line = nextLine();
            }

            tNames.add("coord");
            tTypes.add(DataArrayType.FIELD_DATA_FLOAT);
            tVlens.add(3);

            tNames.add("mask");
            tTypes.add(DataArrayType.FIELD_DATA_LOGIC);
            tVlens.add(1);

            names = new String[tNames.size() + 6];
            String[] specialNames = new String[]{"skip", "stride", "sep", "tile", "timestep", "repeat"};
            for (int i = 0; i < tNames.size(); i++)
                names[i] = tNames.get(i);
            System.arraycopy(specialNames, 0, names, tNames.size(), 6);
            types = new DataArrayType[tTypes.size()];
            for (int i = 0; i < types.length; i++)
                types[i] = tTypes.get(i);
            vlens = new int[tVlens.size()];
            for (int i = 0; i < vlens.length; i++)
                vlens[i] = tVlens.get(i);

            DataFileSchema dataFileSchema;
            schema.generateDataInputStatus();
            parsedSchema = schema;
            file_loop:
            while ((dataFileSchema = parseFileEntry()) != null)
                schema.addFileSchema(dataFileSchema);
            r.close();
        } catch (IOException e) {
            throw new VNIOException("bad header file ", fileName, r.getLineNumber());
        }
        return schema;
    }

    @Override
    protected FilePartSchema parseFileSectionEntry(FileType fileType, Vector<String[]> tokens)
            throws VNIOException
    {
        int stride = -1;
        int[][] tile = null;
        int cOffset = 0;
        Vector<DataElementIOSchema> compSchemas = new Vector<DataElementIOSchema>();

        for (int i = 0; i < tokens.size(); i++) {
            String[] strings = tokens.get(i);
            if (strings[0].startsWith("stride") && strings.length > 1)
                stride = Integer.parseInt(strings[1]);
            else if (strings[0].startsWith("tile"))
                try {
                    tile = new int[regularField.getDims().length][3];
                    for (int[] tile1 : tile)
                        tile1[2] = 1;
                    if (strings.length == 2 * regularField.getDims().length + 1)
                        for (int j = 0; j < regularField.getDims().length; j++) {
                            tile[j][0] = Integer.parseInt(strings[2 * j + 1]);
                            tile[j][1] = Integer.parseInt(strings[2 * j + 2]);
                        }
                    else if (strings.length == regularField.getDims().length + 1)
                        for (int j = 0; j < regularField.getDims().length; j++) {
                            String[] tileSpecs = strings[j + 1].split(":");
                            tile[j][0] = Integer.parseInt(tileSpecs[0]);
                            tile[j][1] = Integer.parseInt(tileSpecs[1]);
                            if (tileSpecs.length > 2)
                                tile[j][2] = Integer.parseInt(tileSpecs[2]);
                        }
                } catch (NumberFormatException e) {
                    throw new VNIOException("number format error in tile definition ", fileName, r.getLineNumber());
                } catch (ArrayIndexOutOfBoundsException e) {
                    throw new VNIOException("invalid tile boundaries ", fileName, r.getLineNumber());
                }
            else
                cOffset = parseComponentSchema(regularField, strings, fileType, compSchemas, cOffset);
        }
        if (stride == -1)
            stride = cOffset;
        FileSectionSchema secSchema;
        if (compSchemas.isEmpty())
            secSchema = new FileSectionSchema(fileName, r.getLineNumber(), stride, compSchemas, null,
                            fileType.isBinary());
        else
            secSchema = new FileSectionSchema(fileName, r.getLineNumber(), stride, compSchemas, vlens,
                            fileType.isBinary());
        if (tile != null)
            secSchema.setTile(tile);
        return secSchema;
    }
}
