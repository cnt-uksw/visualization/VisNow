/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 

package org.visnow.vn.lib.basic.readers.ReadVisNowField.utils;

/**
 *
 * @author know
 */


public enum FieldDescriptionKeywords
{
    FIELD_NAME       (new String[] {"field", "name"}),
    DIMENSIONS       (new String[] {"dim"}),
    EXTENTS          (new String[] {"ext", "xt"}),
    PHYSICAL_EXTENTS (new String[] {"phys"}),
    TIME_UNIT        (new String[] {"time"}),
    MASK             (new String[] {"mask", "valid"}),
    COORDS           (new String[] {"coord", "crd"}),
    COORD_UNIT       (new String[] {"unit", "coord unit", "crdd unit"}),
    COMPONENT        (new String[] {"comp", "cmp"}),
    USER_DATA        (new String[] {"us"}),
    ORIGIN           (new String[] {"o"}),
    V0               (new String[] {"v0", "vi", "vx", "vu"}),
    V1               (new String[] {"v1", "vj", "vy", "vv"}),
    V2               (new String[] {"v2", "vk", "vz"}),
    R0               (new String[] {"r0", "rx", "ru"}),
    R1               (new String[] {"r1", "ry", "rv"}),
    R2               (new String[] {"r2", "rz"}),
    FILE             (new String[] {"file"}),
    CELL_SET         (new String[] {"cell"}),
    UNKNOWN          (new String[] {});

    private final String[] keys;

    private FieldDescriptionKeywords(String[] keys)
    {
        this.keys = keys;
    }
    public static FieldDescriptionKeywords getKeyword(String key)
    {
        for (FieldDescriptionKeywords k : FieldDescriptionKeywords.values())
            for (String s : k.keys)
               if (key.toLowerCase().startsWith(s))
                   return k;
        return UNKNOWN;
    }

}
