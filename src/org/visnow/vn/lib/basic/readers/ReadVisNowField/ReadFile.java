/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.basic.readers.ReadVisNowField;

import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.Skip;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.SkipSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.TimestepSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.DataFileSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FileSectionSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FilePartSchema;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteOrder;
import java.util.Scanner;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.MemoryCacheImageInputStream;
import org.visnow.jscic.Field;
import org.visnow.vn.engine.core.ProgressAgent;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FieldIOSchema;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FileType;
import org.visnow.vn.lib.utils.io.VNIOException;

/**
 * VisNow field format file parser.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ReadFile
{

    protected Field outField = null;
    protected FieldIOSchema schema = null;
    protected DataFileSchema fileSchema = null;
    protected LineNumberReader reader = null;
    protected ImageInputStream inStream = null;
    protected FileSectionReader sectionReader = null;
    protected Scanner scanner = null;
    protected Object input = null;
    protected String filePath = "";
    protected URLConnection urlConnection;
    protected URL url;
    protected ProgressAgent progressAgent = null;
    protected int file;
    protected int nFiles = 1;

   /**
     * Parse VNF file.
     * Creates object that represents a VNF file.
     *
     * @param outField output field
     * @param schema creates schema of the output field
     * @param file iterates through files in a VNF
     * @param nFiles number of files in VNF file
     * @param isURL true if a file is given via URL
     * @param errorFrame error handling
     * @param progressAgent progress bar handling agent
     */
    public ReadFile(Field outField, FieldIOSchema schema, int file, int nFiles, boolean isURL, ProgressAgent progressAgent)
            throws VNIOException
    {
        this.outField = outField;
        this.schema = schema;
        this.progressAgent = progressAgent;
        this.nFiles = nFiles;
        this.file = file;
        fileSchema = schema.getFileSchema(file);
        if (isURL) {
            filePath = fileSchema.getName();
            boolean relative = true;
            try {
                url = new URL(filePath);
                urlConnection = url.openConnection();
                relative = urlConnection == null;
            } catch (IOException e) {
            }
            if (relative)
                try {
                    String hUrl = schema.getHeaderURL();
                    int k = hUrl.lastIndexOf("/");
                    url = new URL(hUrl.substring(0, k) + "/" + filePath);
                    urlConnection = url.openConnection();
                } catch (IOException e) {
                    throw new VNIOException("could not open URL ", schema.getHeaderURL() + filePath, -1);
                }
        } else if (new File(fileSchema.getName()).isAbsolute())
            filePath = fileSchema.getName();
        else
            filePath = schema.getHeaderFile().getParent() + File.separator + fileSchema.getName();
        try {
            switch (fileSchema.getType()) {
                case BINARY_BIG_ENDIAN:
                case BINARY_LITTLE_ENDIAN:
                    if (isURL)
                        inStream = new MemoryCacheImageInputStream(urlConnection.getInputStream());
                    else
                        try {
                            File inFile = new File(filePath);
                            if (!inFile.isFile() || !inFile.canRead())
                                throw new VNIOException("cannot open " + filePath, "", -1);
                            inStream = new FileImageInputStream(inFile);
                        } catch (IOException e) {
                            throw new VNIOException("cannot open " + filePath, "", -1);
                        }

                    inStream.setByteOrder(fileSchema.getType() == FileType.BINARY_BIG_ENDIAN ?
                                          ByteOrder.BIG_ENDIAN: ByteOrder.LITTLE_ENDIAN);
                    input = inStream;
                    break;
                case ASCII_COLUMN:
                case ASCII_FIXED_COLUMN:
                    if (isURL)
                        reader = new LineNumberReader(new InputStreamReader(urlConnection.getInputStream()));
                    else
                        reader = new LineNumberReader(new FileReader(filePath));
                    input = reader;
                    break;
                case ASCII_CONTINUOUS:
                    if (isURL)
                        scanner = new Scanner(new InputStreamReader(urlConnection.getInputStream()));
                    else
                        scanner = new Scanner(new FileReader(filePath));
                    input = scanner;
                    break;
            }
            sectionReader = new FileSectionReader(outField, schema, reader, inStream, scanner, fileSchema.getType(), filePath);
        } catch (FileNotFoundException e) {
            throw new VNIOException("cannot open " + filePath, "", -1);
        } catch (IOException e) {
            if (inStream != null)
                throw new VNIOException("error in " + filePath, "", 0);
            if (reader != null)
                throw new VNIOException("error in data file " + filePath, "", reader.getLineNumber());
        }
    }

    public void run() throws VNIOException
    {
        int partCount = 0;
        for (int part = 0; part < fileSchema.getNSections(); part++) {
            FilePartSchema partSchema = fileSchema.getPartSchema(part);
           if (partSchema instanceof FileSectionSchema)
               partCount += 1;
           if (partSchema instanceof TimestepSchema) {
               int timePartCount = 0;
               TimestepSchema timestepSchema = (TimestepSchema) partSchema;
               for (int section = 0; section < timestepSchema.getNSections(); section++) {
                   FilePartSchema fps = timestepSchema.getSection(section);
                   if (fps instanceof FileSectionSchema)
                       partCount += 1;
               }
               partCount += ((TimestepSchema) partSchema).getRepeat() * timePartCount;
           }
        }
        int currentPart = 0;
        try {
            for (int part = 0; part < fileSchema.getNSections(); part++) {
                FilePartSchema partSchema = fileSchema.getPartSchema(part);
                if (partSchema instanceof SkipSchema)
                    Skip.skip((SkipSchema) partSchema, input);
                else if (partSchema instanceof FileSectionSchema) {
                    sectionReader.setSectionSchema((FileSectionSchema) partSchema, 0, fileSchema.getDecimalSeparator(), fileSchema.getEntrySeparator());
                    if (sectionReader.readSection() > 1)
                        return;
                    fileSchema.setLastRead(0);
                    currentPart += 1;
                    if(progressAgent != null) progressAgent.setProgress(((float)file + (float)currentPart / partCount)/ nFiles );
                } else if (partSchema instanceof TimestepSchema) {
                    TimestepSchema timestepSchema = (TimestepSchema) partSchema;
                    int repeat = timestepSchema.getRepeat();
                    float stime = timestepSchema.getTime();
                    float dt = timestepSchema.getDt();
                    if (repeat < 1)
                        repeat = Integer.MAX_VALUE;
                    time_loop:
                    for (int iStep = 0; iStep < repeat; iStep++) {
                        for (int section = 0; section < timestepSchema.getNSections(); section++) {
                            FilePartSchema fps = timestepSchema.getSection(section);
                            if (fps instanceof SkipSchema)
                                Skip.skip((SkipSchema) fps, input);
                            if (fps instanceof FileSectionSchema) {
                                sectionReader.setSectionSchema((FileSectionSchema) fps, stime + iStep * dt, fileSchema.getDecimalSeparator(), fileSchema.getEntrySeparator());
                                if (sectionReader.readSection() > 1)
                                    break time_loop;
                            }
                            currentPart += 1;
                            if(progressAgent != null) progressAgent.setProgress(((float)file + (float)currentPart / partCount)/ nFiles );
                        }
                        fileSchema.setLastRead(iStep);
                    }
                }
            }
            if (inStream != null)
                inStream.close();
            if (reader != null)
                reader.close();
        } catch (VNIOException e) {
            throw  e;
        } catch (FileNotFoundException e) {
            throw new VNIOException("cannot open " + filePath, "", -1);
        } catch (IOException e) {
            if (reader != null)
                throw new VNIOException("error" + filePath, "", reader.getLineNumber());
            if (inStream != null)
                throw new VNIOException("error" + filePath, "", -1);
        }
    }
}
