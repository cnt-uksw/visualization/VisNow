/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.basic.readers.ReadVisNowField.utils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import org.apache.log4j.Logger;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.FieldSchema;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.lib.utils.io.VNIOException;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class FieldIOSchema
{
    private static final Logger LOGGER = Logger.getLogger(FieldIOSchema.class);

    protected final String headerURL;
    protected final File headerFile;
    protected final Field field;
    protected final Vector<DataFileSchema> fileSchemas = new Vector<DataFileSchema>();
    protected Map<String, Boolean> dataInputStatus;

    public FieldIOSchema(Field field, File headerFile, String headerURL)
    {
        this.field = field;
        this.headerFile = headerFile;
        this.headerURL = headerURL;
    }

    public void generateDataInputStatus()
    {
        dataInputStatus = new HashMap<>();
        if (!(field instanceof RegularField) || field.hasCoords())
            dataInputStatus.put("coords", Boolean.FALSE);
        if (field.hasMask())
            dataInputStatus.put("mask", Boolean.FALSE);
        for (String componentName : field.getComponentNames())
            dataInputStatus.put(componentName, Boolean.FALSE);
        if (field instanceof IrregularField)
            for (CellSet cellSet : ((IrregularField) field).getCellSets()) {
                String cName = cellSet.getName();
                if (cellSet.getNComponents() > 0)
                    for (String componentName : cellSet.getComponentNames())
                        dataInputStatus.put(cName + ":" + componentName, Boolean.FALSE);
                for (CellArray cellArray : cellSet.getCellArrays())
                    if (cellArray != null) {
                        dataInputStatus.put(cName + ":" + cellArray.getType().getPluralName() + ":nodes", Boolean.FALSE);
                        if (cellSet.getNComponents() > 0)
                            dataInputStatus.put(cName + ":" + cellArray.getType().getPluralName() + ":indices", Boolean.FALSE);
                    }
            }
    }

    public void dataElementFound(String s)
    {
        dataInputStatus.put(s, true);
    }

    public void checkMissingData() throws VNIOException
    {
        String explanation = "<html>data items missing from data files:<p>";
        StringBuilder missB = new StringBuilder(explanation);
        boolean somethingMissing = false;
        for (String s : dataInputStatus.keySet())
            if (!dataInputStatus.get(s)) {
                missB.append(s + "<p>");
                somethingMissing = true;
            }
        String out = missB.toString();
        if (somethingMissing)
            throw new VNIOException(out + "</html>", headerFile.getPath(), 1);
    }

    /**
     * Get the value of headerFile
     *
     * @return the value of headerFile
     */
    public File getHeaderFile()
    {
        return headerFile;
    }

    public String getHeaderURL()
    {
        return headerURL;
    }

    /**
     * Get the value of fileSchemas
     *
     * @return the value of fileSchemas
     */
    public Vector<DataFileSchema> getFileSchemas()
    {
        return fileSchemas;
    }

    /**
     * Get the value of fieldSchema
     *
     * @return the value of fieldSchema
     */
    public Field getField()
    {
        return field;
    }

    public FieldSchema getFieldSchema()
    {
        return (FieldSchema) field.getSchema();
    }

    public int getNFiles()
    {
        return fileSchemas.size();
    }

    public DataFileSchema getFileSchema(int i)
    {
        if (i < 0 || i >= fileSchemas.size())
            return null;
        return fileSchemas.get(i);
    }

    public void addFileSchema(DataFileSchema s)
    {
        if (s != null)
            fileSchemas.add(s);
    }

    public String[] getDescription()
    {
        String[] des = new String[10000];
        des[0] = "<html>";
        des[1] = field.toMultilineString();
        for (int i = 1; i < field.getNComponents(); i++)
            des[i + 1] = field.getComponent(i).toString();
        int nLines = 2 + field.getNComponents();
        for (int i = 0; i < fileSchemas.size(); i++) {
            String[] fSchema = fileSchemas.get(i).getDescription();
            System.arraycopy(fSchema, 0, des, nLines, fSchema.length);
            nLines += fSchema.length;
        }
        String[] description = new String[nLines];
        System.arraycopy(des, 0, description, 0, nLines);
        return description;
    }

    public void printDescription()
    {
        String[] des = getDescription();
        for (int i = 0; i < des.length; i++)
            LOGGER.info(des[i]);
    }

}
