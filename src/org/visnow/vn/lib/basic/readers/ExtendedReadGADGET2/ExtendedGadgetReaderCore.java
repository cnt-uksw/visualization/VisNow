/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.ExtendedReadGADGET2;

import java.util.HashMap;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.system.main.VisNow;
import static org.visnow.vn.lib.basic.readers.ExtendedReadGADGET2.ExtendedGadgetReaderShared.*;


//TODO: proper java doc 
//TODO: common "Core" interface
//TODO: Core classes should have also full knowledge about input/output port names/types - if they're going to be used as "batch" version of module.
/**
 * Core that provides main computational functionality: processing input
 * parameters and creating output fields.
 *
 * @author szpak
 */
public class ExtendedGadgetReaderCore
{

    final private ExtendedReadGadgetData extendedReadGadgetData;

    final private Parameters params;

    private RegularField outDensityField;
    private IrregularField outField;

    //FIXME: no field schema testing ... modules may be incorrectly connected now (in Unicore wizard).
    public final static String OUT_IFIELD_MAIN = "outField";
    public final static String OUT_RFIELD_DENSITY = "outDensityField";

    public ExtendedGadgetReaderCore(Parameters params)
    {
        this.params = params;
        extendedReadGadgetData = new ExtendedReadGadgetData();
    }

    public static boolean[] getReadMask(Parameters params)
    {
        boolean[] out = new boolean[7];
        out[0] = params.get(READ_VELOCITY);
        out[1] = params.get(READ_ID);
        out[2] = params.get(READ_TYPE);
        out[3] = params.get(READ_MASS);
        out[4] = params.get(READ_ENERGY);
        //out[5] = isReadDensity();
        out[5] = false;
        out[6] = params.get(READ_TEMPERATURE);
        return out;
    }


    //TODO: boolean flag can be used to mark failures
    /**
     * Processes params to create outField and outDensityField. On failure, null
     * fields are set.
     */
    public void recalculate()
    {
        //reset out fields
        outDensityField = null;
        outField = null;

        //if file exists
        if (params.get(FILE_PATHS).length > 0) {
            extendedReadGadgetData.read(
                params.get(FILE_PATHS),
                getReadMask(params),
                params.get(DOWNSIZE),
                params.get(DENSITY_FIELD_DIMS),
                params.get(DENSITY_FIELD_LOG),
                null,
                VisNow.getMemoryFree() / 3
            );

            outField = extendedReadGadgetData.getField();
            if (outField != null) {
                outDensityField = extendedReadGadgetData.getDensityField();
            }
        }
    }

    public static String[] getInFieldNames()
    {
        return new String[]{};
    }

    public static String[] getOutFieldNames()
    {
        return new String[]{OUT_IFIELD_MAIN, OUT_RFIELD_DENSITY};
    }

    public Field getOutField(String name)
    {
        if (name.equals(OUT_IFIELD_MAIN))
            return outField;
        else if (name.equals(OUT_RFIELD_DENSITY))
            return outDensityField;
        else
            throw new IllegalArgumentException("Incorrect field name: " + name);
    }
}
