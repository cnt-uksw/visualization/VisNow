/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.ReadFluent;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 *
 * @author babor
 */


public class MemoryFileImageInputStream {
    private File file;
    private byte[] data;
    private ByteOrder byteOrder;
    private int offset = 0;
    
    public MemoryFileImageInputStream(File file) throws FileNotFoundException, IOException {
        this.file = file;
        final FileChannel channel = new FileInputStream(file).getChannel();
        MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
        int size = buffer.remaining();
        data = new byte[size];
        buffer.get(data);
        channel.close();  
    }
        
    public void setByteOrder(ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
    }
    
    public ByteOrder getByteOrder() {
        return byteOrder;
    }
    
    public void close() {
        data = null;
    }
  
    /**
     * Reads a single byte from the min-memory array and returns it as an
     * <code>int</code> between 0 and 255.  If end of array is reached,
     * <code>-1</code> is returned.
     *
     * @return the value of the next byte in the in-memory array, or <code>-1</code>
     * if end of array is reached.
     *
     * @exception IOException if the array is null.
     */
    public int read() throws IOException {
        if(data == null)
            throw new IOException("No data available. Stream either not opened or closed.");
        if(offset < 0)
            throw new IOException("ERROR stream size overflow.");
        if(offset > data.length)
            throw new EOFException();
        
        if(offset == data.length)
            return -1;
        
        return 0xff&data[offset++];
    }
    
     /**
     * Seeks to 0.
     *
     */
    public void reset() {
        seek(0);
    }

    /**
     * Sets the current stream position to the desired location.  The
     * next read will occur at this location.  
     *
     * <p> An <code>IndexOutOfBoundsException</code> will be thrown if
     * <code>pos</code> is smaller than 0.
     *
     * <p> It is legal to seek past the end of the file; an
     * <code>java.io.EOFException</code> will be thrown only if a read is
     * performed.
     *
     * @param i an <code>int</code> containing the desired file
     * pointer position.
     *
     * @exception IndexOutOfBoundsException if <code>pos</code> is smaller
     * than 0.
     */    
    public void seek(int i) {
        if(i < 0)
            throw new IndexOutOfBoundsException();
        offset = i;    
    }
    
    
    
    
    
}
