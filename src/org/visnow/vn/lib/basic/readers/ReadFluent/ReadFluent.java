/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.ReadFluent;

import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.parameters.PresentationParams;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import static org.visnow.vn.lib.basic.readers.ReadFluent.ReadFluentShared.*;
import static org.visnow.vn.lib.utils.field.CellToNode.*;
import org.visnow.vn.lib.utils.field.GeometricOrientation;
import org.visnow.vn.lib.utils.field.VectorComponentCombiner;
import static org.visnow.vn.lib.utils.field.MergeIrregularField.mergeCellSet;
import org.visnow.vn.system.main.VisNow;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class ReadFluent extends OutFieldVisualizationModule
{

    protected GUI computeUI = null;
    protected Cell[] stdCells = new Cell[Cell.getNProperCellTypes()];
    protected boolean ignoreUI = false;


    
    public ReadFluent()
    {
        for (int i = 0; i < stdCells.length; i++) {
            stdCells[i] = Cell.createCell(CellType.getType(i), 3, new int[CellType.getType(i).getNVertices()], (byte)1);
        }
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }
    
    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(CAS_FILE_PATH, ""),
            new Parameter<>(DAT_FILE_PATHS, new String[0]),
            new Parameter<>(SHOW, true),
            new Parameter<>(SCALE, 1.0f),
            new Parameter<>(CELL_TO_NODE, true),
            new Parameter<>(DROP_CELL_DATA, true),
            new Parameter<>(MERGE_CELL_SETS, true),
            new Parameter<>(DROP_CONSTANT_DATA, true),
        };
    }
    
    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }
    

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    public static OutputEgg[] outputEggs = null;

    @Override
    public void onActive()
    {
        
        Parameters p;
        synchronized (parameters) {
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, false, false);
        
        if (p.get(CAS_FILE_PATH) != null && !p.get(CAS_FILE_PATH).isEmpty()) {
            this.setProgress(0.0f);
            String datFilePath = null;
            if(p.get(DAT_FILE_PATHS) != null && p.get(DAT_FILE_PATHS).length > 0) {
                datFilePath = p.get(DAT_FILE_PATHS)[0];
            }
            String casFilePath = p.get(CAS_FILE_PATH);

            FluentReader reader = new FluentReader(casFilePath, datFilePath);
            nTasks = 4;
            iTask = 0;
            reader.setFloatValueModificationListener(new FloatValueModificationListener()
            {
                @Override
                public void floatValueChanged(FloatValueModificationEvent e)
                {
                    setTaskedProgress(e.getVal());
                }
            });
            if (!reader.requestInformation()) {
                return;
            }
            this.setProgress(0.33f);
            iTask = 1;
            outIrregularField = reader.requestData(p.get(SCALE));

            this.setProgress(0.66f);
            iTask = 2;
            if (outIrregularField != null) {
                
                GeometricOrientation.recomputeOrientations(outIrregularField);
                this.setProgress(0.72f);
                
                VectorComponentCombiner.combineVectors(outIrregularField);
                this.setProgress(0.76f);
                
                if (parameters.get(DROP_CONSTANT_DATA)) {
                    for (int i = outIrregularField.getNComponents() - 1; i >= 0; i--) {
                        DataArray component = outIrregularField.getComponent(i);
                        if (component.getMinValue() == component.getMaxValue()) {
                            outIrregularField.removeComponent(i);
                        }
                    }
                    for (int c = 0; c < outIrregularField.getNCellSets(); c++) {
                        for (int i = outIrregularField.getCellSet(c).getNComponents() - 1; i >= 0; i--) {
                            DataArray component = outIrregularField.getCellSet(c).getComponent(i);
                            if (component.getMinValue() == component.getMaxValue()) {
                                outIrregularField.getCellSet(c).removeComponent(i);
                            }
                        }
                    }
                }
                
                if (parameters.get(CELL_TO_NODE)) 
                    outIrregularField = convertCellDataToNodeData(parameters.get(DROP_CELL_DATA), outIrregularField);
                this.setProgress(0.82f);
                
                if (parameters.get(MERGE_CELL_SETS))
                    outIrregularField = mergeCellSet(outIrregularField);
                this.setProgress(0.88f);

                if (outIrregularField.getNComponents() == 0) {
                    outIrregularField.addComponent(DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, outIrregularField.getNNodes(), 0, "dummy"));
                }
               
                FloatLargeArray coords = outIrregularField.getCurrentCoords();
                for (CellSet cellSet : outIrregularField.getCellSets()) 
                    cellSet.generateDisplayData(coords);
            } 
        }
        
        this.setProgress(0.95f);
        iTask = 3;
        if(outIrregularField != null) {        
            computeUI.setFieldDescription(outIrregularField.description());
            setOutputValue("outField", new VNIrregularField(outIrregularField));
            if (p.get(SHOW)) {
                outField = outIrregularField;
                prepareOutputGeometry();
                renderingParams.setShadingMode(RenderingParams.FLAT_SHADED);
                for (PresentationParams csParams : presentationParams.getChildrenParams()) 
                    csParams.getRenderingParams().setShadingMode(RenderingParams.FLAT_SHADED);
                if (VisNow.get().getMainConfig().isDefaultOutline() && outField.getTrueNSpace() == 3) {
                    ui.getPresentationGUI().getRenderingGUI().setOutlineMode(true);
                    renderingParams.setMinEdgeDihedral(10);
                    renderingParams.setDisplayMode(RenderingParams.EDGES);
                }
                show();
            } else {
                outField = null;
                prepareOutputGeometry();
            }
        } else {
            computeUI.setFieldDescription("no data");
            setOutputValue("outField", null);    
            outField = null;
            prepareOutputGeometry();
            renderingParams.setShadingMode(RenderingParams.FLAT_SHADED);
            for (PresentationParams csParams : presentationParams.getChildrenParams()) 
                csParams.getRenderingParams().setShadingMode(RenderingParams.FLAT_SHADED);
        }
        this.setProgress(1.0f);
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag()) {
            computeUI.activateOpenDialog();
        }
    }

    private int iTask = 0;
    private int nTasks = 1;

    private void setTaskedProgress(float currentTaskProgress)
    {
        float ppt = 1.0f / (float) nTasks;
        float p = (iTask + currentTaskProgress) * ppt;
        setProgress(p);
    }

}
