/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */

package org.visnow.vn.lib.basic.readers.ReadUCD;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.parameters.PresentationParams;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.gui.widgets.FileErrorFrame;
import static org.visnow.vn.lib.basic.readers.ReadUCD.ReadUCDShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import static org.visnow.vn.lib.utils.field.CellToNode.convertCellDataToNodeData;
import org.visnow.vn.lib.utils.field.GeometricOrientation;
import org.visnow.vn.lib.utils.field.VectorComponentCombiner;
import org.visnow.vn.lib.utils.io.InputSource;
import static org.visnow.vn.lib.utils.field.MergeIrregularField.mergeCellSet;
import org.visnow.vn.system.main.VisNow;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ReadUCD extends OutFieldVisualizationModule
{

    protected GUI computeUI = null;
    protected String lastFileName = " ";
    protected FileErrorFrame errorFrame = null;
    protected Cell[] stdCells = new Cell[Cell.getNProperCellTypes()];
    protected boolean ignoreUI = false;

    /**
     * Creates a new instance of CreateGrid
     */
    public ReadUCD()
    {
        for (int i = 0; i < stdCells.length; i++)
            stdCells[i] = Cell.createCell(CellType.getType(i), 3, new int[CellType.getType(i).getNVertices()], (byte)1);

        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParameters(parameters);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILENAME, ""),
            new Parameter<>(MATERIAL_AS_SETS, true),
            new Parameter<>(INDICES, true),
            new Parameter<>(INPUT_SOURCE, InputSource.FILE),
            new Parameter<>(CELL_TO_NODE, true),
            new Parameter<>(MERGE_CELL_SETS, true),
            new Parameter<>(DROP_CELL_DATA, true)
        };
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    public static OutputEgg[] outputEggs = null;

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    IrregularField tmpField;

    @Override
    public void onActive()
    {
        boolean binary = false;
        if (parameters.get(FILENAME) == null)
            return;
        Parameters p = parameters.getReadOnlyClone();
        notifyGUIs(p, isFromVNA(), false);

        if (p.get(FILENAME) != null && !p.get(FILENAME).isEmpty()  && !p.get(FILENAME).equals(lastFileName)) {
            lastFileName = p.get(FILENAME);
            try {
                InputStream test = new FileInputStream(p.get(FILENAME));
                binary = test.read() == 7;
            } catch (Exception ex) {
                Logger.getLogger(ReadUCD.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (binary)
                tmpField = new BinaryReader().readUCD(p);
            else
                tmpField = new ASCIIReader().readUCD(p);
            if (tmpField == null)
                return;
            GeometricOrientation.recomputeOrientations(tmpField);
            IrregularField transientField =
                    parameters.get(CELL_TO_NODE) ?
                    convertCellDataToNodeData(parameters.get(DROP_CELL_DATA), tmpField) :
                    tmpField.cloneShallow();
            outIrregularField = transientField.cloneShallow();
            outIrregularField.removeComponents();
            for (DataArray component : transientField.getComponents()) {
                double min = component.getPreferredMinValue();
                double max = component.getPreferredMaxValue();
                if ((1e6 * (max- min)) > Math.abs(max) + Math.abs(min))
                    outIrregularField.addComponent(component.cloneShallow());
            }
            VectorComponentCombiner.combineVectors(outIrregularField);

            if (parameters.get(MERGE_CELL_SETS))
                outIrregularField = mergeCellSet(outIrregularField);

            computeUI.setFieldDescription(outIrregularField.description());
            setOutputValue("UCD field", new VNIrregularField(outIrregularField));
        }
        if (outIrregularField == null)
            return;
        outField = outIrregularField;
        prepareOutputGeometry();
        renderingParams.setShadingMode(RenderingParams.FLAT_SHADED);
        for (PresentationParams csParams : presentationParams.getChildrenParams())
            csParams.getRenderingParams().setShadingMode(RenderingParams.FLAT_SHADED);
        if (VisNow.get().getMainConfig().isDefaultOutline() && outField.getTrueNSpace() == 3) {
            ui.getPresentationGUI().getRenderingGUI().setOutlineMode(true);
            renderingParams.setMinEdgeDihedral(10);
            renderingParams.setDisplayMode(RenderingParams.EDGES);
        }
        show();
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag())
            computeUI.activateOpenDialog();
    }

}
