/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.ReadGADGET2;

import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Params extends Parameters
{

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<String[]>("filePaths", ParameterType.independent, null),
        new ParameterEgg<Boolean>("readVelocity", ParameterType.independent, true),
        new ParameterEgg<Boolean>("readId", ParameterType.independent, true),
        new ParameterEgg<Boolean>("readType", ParameterType.independent, true),
        new ParameterEgg<Boolean>("readMass", ParameterType.independent, true),
        new ParameterEgg<Boolean>("readEnergy", ParameterType.independent, true),
        new ParameterEgg<Boolean>("readDensity", ParameterType.independent, true),
        new ParameterEgg<Boolean>("readTemperature", ParameterType.independent, true),
        new ParameterEgg<Integer>("downsize", ParameterType.independent, 1),
        new ParameterEgg<Boolean>("show", ParameterType.independent, false)
    };

    public Params()
    {
        super(eggs);
    }

    public String[] getFilePaths()
    {
        return (String[]) getValue("filePaths");
    }

    public void setFilePaths(String[] filePaths)
    {
        setValue("filePaths", filePaths);
        fireStateChanged();
    }

    public boolean isReadVelocity()
    {
        return (Boolean) getValue("readVelocity");
    }

    public void setReadVelocity(boolean value)
    {
        setValue("readVelocity", value);
    }

    public boolean isReadId()
    {
        return (Boolean) getValue("readId");
    }

    public void setReadId(boolean value)
    {
        setValue("readId", value);
    }

    public boolean isReadType()
    {
        return (Boolean) getValue("readType");
    }

    public void setReadType(boolean value)
    {
        setValue("readType", value);
    }

    public boolean isReadMass()
    {
        return (Boolean) getValue("readMass");
    }

    public void setReadMass(boolean value)
    {
        setValue("readMass", value);
    }

    public boolean isReadEnergy()
    {
        return (Boolean) getValue("readEnergy");
    }

    public void setReadEnergy(boolean value)
    {
        setValue("readEnergy", value);
        if (value = false)
            setReadTemperature(false);
    }

    public boolean isReadDensity()
    {
        return (Boolean) getValue("readDensity");
    }

    public void setReadDensity(boolean value)
    {
        setValue("readDensity", value);
    }

    public boolean isReadTemperature()
    {
        return (Boolean) getValue("readTemperature");
    }

    public void setReadTemperature(boolean value)
    {
        setValue("readTemperature", value);
        if (value == true)
            setReadEnergy(true);
    }

    public int getDownsize()
    {
        return (Integer) getValue("downsize");
    }

    public void setDownsize(int value)
    {
        setValue("downsize", value);
    }

    public boolean isShow()
    {
        return (Boolean) getValue("show");
    }

    public void setShow(boolean value)
    {
        setValue("show", value);
    }

}
