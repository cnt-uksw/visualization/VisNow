/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.ReadGADGET2;

import java.io.File;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.ArrayList;
import javax.imageio.stream.FileImageInputStream;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.cells.CellType;
import org.visnow.jlargearrays.LogicLargeArray;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
class ReadGadgetData
{

    private float progress = 0.0f;
    private Params params = null;

    public ReadGadgetData(Params params)
    {
        this.params = params;
    }

    public IrregularField readGadgetData(String filePath)
    {
        if (params == null) {
            return null;
        }

        if (params.getDownsize() == 1) {
            return readGadgetFullData(filePath);
        } else {
            return readGadgetDownsizedData(filePath);
        }

    }

    private IrregularField readGadgetFullData(String filePath)
    {
        return readGadgetFullData(filePath, false);
    }

    private IrregularField readGadgetFullData(String filePath, boolean silent)
    {
        if (filePath == null || filePath.length() < 1 || params == null) {
            return null;
        }

        progress = 0.0f;
        if (!silent) {
            fireStateChanged();
        }

        File f = new File(filePath);
        if (!f.exists()) {
            return null;
        }

        int[] header_npart = new int[6];
        double[] header_mass = new double[6];
        double header_time;
        double header_redshift;
        int header_flag_sfr;
        int header_flag_feedback;
        int[] header_npartTotal = new int[6];
        int header_flag_cooling;
        int header_num_files;
        double header_BoxSize;
        double header_Omega0;
        double header_OmegaLambda;
        double header_HubbleParam;
        byte[] header_fill = new byte[256 - 6 * 4 - 6 * 8 - 2 * 8 - 2 * 4 - 6 * 4 - 2 * 4 - 4 * 8]; // fills to 256 Bytes

        IrregularField outField = null;
        FileImageInputStream in;
        try {
            int pc = 0;
            in = new FileImageInputStream(f);
            in.setByteOrder(ByteOrder.BIG_ENDIAN);

            if (!silent) {
                System.out.println("Testing data endianness...");
            }
            int test0 = in.readInt();
            if (test0 != 256) {
                if (!silent) {
                    System.out.println("BIG ENDIAN: error");
                }
                in.setByteOrder(ByteOrder.LITTLE_ENDIAN);
                in.seek(0);
                test0 = in.readInt();
                if (test0 != 256) {
                    System.err.println("ERROR: Cannot establish file endianness.");
                    return null;
                } else {
                    if (!silent) {
                        System.out.println("LITTLE ENDIAN: OK");
                    }
                }
            } else {
                if (!silent) {
                    System.out.println("BIG ENDIAN: OK");
                }
            }

            //start read header
            in.readFully(header_npart, 0, 6);
            in.readFully(header_mass, 0, 6);
            header_time = in.readDouble();
            header_redshift = in.readDouble();
            header_flag_sfr = in.readInt();
            header_flag_feedback = in.readInt();
            in.readFully(header_npartTotal, 0, 6);
            header_flag_cooling = in.readInt();
            header_num_files = in.readInt();
            header_BoxSize = in.readDouble();
            header_Omega0 = in.readDouble();
            header_OmegaLambda = in.readDouble();
            header_HubbleParam = in.readDouble();
            in.readFully(header_fill, 0, header_fill.length);
            skipInt(in, 1);
            //end read header

            int NumPart = 0, ntot_withmasses = 0, Ngas = 0;
            int typeCount = 0;
            for (int k = 0; k < 6; k++) { //dlaczego 5 a nie 6 ???
                NumPart += header_npart[k];
                if (header_npart[k] > 0) {
                    typeCount++;
                }
            }
            Ngas = header_npart[0];

            for (int k = 0; k < 6; k++) { //dlaczego 5 a nie 6 ???
                if (header_mass[k] == 0) {
                    ntot_withmasses += header_npart[k];
                }
            }

            if (!silent) {
                System.out.println("Header represents " + NumPart + " particles");
            }
            if (!silent) {
                System.out.println("estimating memory needs...");
            }
            long mem = NumPart * 3 * 4; //pos
            if (params.isReadVelocity()) {
                mem += NumPart * 3 * 4;
            }
            if (params.isReadId()) {
                mem += NumPart * 4;
            }
            if (params.isReadType()) {
                mem += NumPart * 4;
            }
            if (params.isReadMass()) {
                mem += NumPart * 4;
            }

            if (params.isReadEnergy()) {
                mem += Ngas * 4;
            }
            if (params.isReadDensity()) {
                mem += Ngas * 4;
            }
            if (params.isReadTemperature()) {
                mem += Ngas * 4;
            }

            //memory for cellsets
            mem += NumPart * 4;
            mem += NumPart;

            if (!silent) {
                System.out.println("estimated size: " + (mem / (1024 * 1024)) + " MB");
            }

            if (!silent) {
                System.out.print("allocating memory...");
            }

            float[] pos = null;
            float[] vel = null;
            int[] id = null;
            int[] type = null;
            float[] mass = null;
            float[] u = null;
            float[] rho = null;
            float[] temp = null;

            pos = new float[NumPart * 3];
            if (params.isReadVelocity()) {
                //System.out.println("particle velocity");
                vel = new float[NumPart * 3];
            }
            if (params.isReadId()) {
                //System.out.println("particle ID");
                id = new int[NumPart];
            }
            if (params.isReadType()) {
                //System.out.println("particle type");
                type = new int[NumPart];
            }
            if (params.isReadMass()) {
                //System.out.println("particle mass");
                mass = new float[NumPart];
            }

            if (params.isReadEnergy()) {
                //System.out.println("internal energy");
                u = new float[Ngas];
            }
            if (params.isReadDensity()) {
                //System.out.println("density");
                rho = new float[Ngas];
            }
            if (params.isReadTemperature()) {
                //System.out.println("temperature");
                temp = new float[Ngas];
            }

            if (!silent) {
                System.out.println(" done.");
            }

            progress = 0.1f;
            if (!silent) {
                fireStateChanged();
            }

            if (!silent) {
                System.out.print("reading data...");
            }

            skipInt(in, 1);
            in.readFully(pos, 0, 3 * NumPart);
            skipInt(in, 1);

            progress = 0.1f + 0.8f * (float) in.getStreamPosition() / (float) in.length();
            if (!silent) {
                fireStateChanged();
            }

            skipInt(in, 1);

            if (params.isReadVelocity()) {
                in.readFully(vel, 0, 3 * NumPart);
            } else {
                in.skipBytes(4 * 3 * NumPart);
            }
            skipInt(in, 1);

            progress = 0.1f + 0.8f * (float) in.getStreamPosition() / (float) in.length();
            if (!silent) {
                fireStateChanged();
            }

            skipInt(in, 1);
            if (params.isReadId()) {
                in.readFully(id, 0, NumPart);
            } else {
                in.skipBytes(4 * NumPart);
            }
            skipInt(in, 1);

            progress = 0.1f + 0.8f * (float) in.getStreamPosition() / (float) in.length();
            if (!silent) {
                fireStateChanged();
            }

            if (ntot_withmasses > 0) {
                skipInt(in, 1);
            }
            for (int k = 0; k < 6; k++) {
                if (params.isReadType()) {
                    for (int n = 0; n < header_npart[k]; n++) {
                        type[pc + n] = k;
                    }
                }
                if (header_mass[k] == 0) {
                    if (params.isReadMass()) {
                        in.readFully(mass, pc, header_npart[k]);
                    } else {
                        in.skipBytes(4 * header_npart[k]);
                    }
                } else {
                    if (params.isReadMass()) {
                        for (int n = 0; n < header_npart[k]; n++) {
                            mass[pc + n] = (float) header_mass[k];
                        }
                    }
                }
                progress = 0.1f + 0.8f * (float) in.getStreamPosition() / (float) in.length();
                if (!silent) {
                    fireStateChanged();
                }
            }
            if (ntot_withmasses > 0) {
                skipInt(in, 1);
            }

            if (header_npart[0] > 0) {
                skipInt(in, 1);
                if (params.isReadEnergy()) {
                    in.readFully(u, 0, Ngas);
                } else {
                    in.skipBytes(Ngas * 4);
                }
                skipInt(in, 1);

                progress = 0.1f + 0.8f * (float) in.getStreamPosition() / (float) in.length();
                if (!silent) {
                    fireStateChanged();
                }

                skipInt(in, 1);
                if (params.isReadDensity()) {
                    in.readFully(rho, 0, Ngas);
                } else {
                    in.skipBytes(Ngas * 4);
                }
                //skipInt(in,1);
            }
            in.close();
            //end read file

            progress = 0.9f;
            if (!silent) {
                fireStateChanged();
            }

            //calculate temperature
            if (params.isReadTemperature() && params.isReadEnergy()) {
                double BOLTZMANN, PROTONMASS;
                double UnitLength_in_cm, UnitMass_in_g, UnitVelocity_in_cm_per_s;
                double UnitTime_in_s, UnitEnergy_in_cgs;
                double Xh;

                double MeanWeight, gamma, uu;

                /*
                 * physical constants in cgs units
                 */
                BOLTZMANN = 1.3806e-16;
                PROTONMASS = 1.6726e-24;

                /*
                 * internal unit system of the code
                 */
                UnitLength_in_cm = 3.085678e21; /*
                 * code length unit in cm/h
                 */

                UnitMass_in_g = 1.989e43; /*
                 * code mass unit in g/h
                 */

                UnitVelocity_in_cm_per_s = 1.0e5;
                UnitTime_in_s = UnitLength_in_cm / UnitVelocity_in_cm_per_s;
                UnitEnergy_in_cgs = UnitMass_in_g * pow(UnitLength_in_cm, 2) / pow(UnitTime_in_s, 2);
                Xh = 0.76; /*
                 * mass fraction of hydrogen
                 */

                gamma = 5.0 / 3;

                for (int i = 0; i < Ngas; i++) {
                    MeanWeight = 4.0 / (3 * Xh + 1 + 4 * Xh) * PROTONMASS; //Ne=1.0
                    uu = u[i] * UnitEnergy_in_cgs / UnitMass_in_g;
                    /*
                     * get temperature in Kelvin
                     */
                    temp[i] = (float) (MeanWeight / BOLTZMANN * (gamma - 1) * uu);
                }
            }
            //done data
            if (!silent) {
                System.out.println("done");
            }

            progress = 0.95f;
            if (!silent) {
                fireStateChanged();
            }

            if (!silent) {
                System.out.print("creating field...");
            }
            outField = new IrregularField(NumPart);
            //create data arrays
            //create cells
            //create cellsets

            outField.setCurrentCoords(new FloatLargeArray(pos));
            if (params.isReadVelocity()) {
                outField.addComponent(DataArray.create(vel, 3, "velocity"));
            }
            if (params.isReadId()) {
                outField.addComponent(DataArray.create(id, 1, "ID"));
            }
            if (params.isReadType()) {
                outField.addComponent(DataArray.create(type, 1, "type"));
            }
            if (params.isReadMass()) {
                outField.addComponent(DataArray.create(mass, 1, "mass"));
            }

            int pc_new = 0;
            for (int k = 0; k < 6; k++) {
                if (header_npart[k] == 0) {
                    continue;
                }
                int[] cells = new int[header_npart[k]];
                byte[] orient = new byte[header_npart[k]];
                for (int i = 0; i < cells.length; i++, pc_new++) {
                    cells[i] = pc_new;
                    orient[i] = 1;
                }
                CellArray ca = new CellArray(CellType.POINT, cells, orient, null);
                String csName = "cs_";
                switch (k) {
                    case 0:
                        csName += "Gas";
                        break;
                    case 1:
                        csName += "Halo";
                        break;
                    case 2:
                        csName += "Disk";
                        break;
                    case 3:
                        csName += "Bulge";
                        break;
                    case 4:
                        csName += "Stars";
                        break;
                    case 5:
                        csName += "Bndry";
                        break;
                }
                CellSet cs = new CellSet(csName);
                cs.setCellArray(ca);
                if (k == 0 && Ngas > 0) {
                    if (params.isReadEnergy()) {
                        cs.addComponent(DataArray.create(u, 1, "energy"));
                    }
                    if (params.isReadDensity()) {
                        cs.addComponent(DataArray.create(rho, 1, "density"));
                    }
                    if (params.isReadTemperature()) {
                        cs.addComponent(DataArray.create(temp, 1, "temperature"));
                    }
                }
                outField.addCellSet(cs);
            }

            progress = 1.0f;
            if (!silent) {
                fireStateChanged();
            }
            if (!silent) {
                System.out.println("done");
            }
        } catch (IOException ex) {
            System.err.println("ERROR!");
            ex.printStackTrace();
            return null;
        } catch (OutOfMemoryError err) {
            System.err.println("ERROR!");
            System.err.println("Out of memory!");
            return null;
        }
        return outField;
    }

    private IrregularField readGadgetDownsizedData(String filePath)
    {
        if (filePath == null || filePath.length() < 1 || params == null) {
            return null;
        }

        int dn = params.getDownsize();
        if (dn == 1) {
            return readGadgetFullData(filePath);
        }

        progress = 0.0f;
        fireStateChanged();

        File f = new File(filePath);
        if (!f.exists()) {
            return null;
        }

        int[] header_npart = new int[6];
        double[] header_mass = new double[6];
        double header_time;
        double header_redshift;
        int header_flag_sfr;
        int header_flag_feedback;
        int[] header_npartTotal = new int[6];
        int header_flag_cooling;
        int header_num_files;
        double header_BoxSize;
        double header_Omega0;
        double header_OmegaLambda;
        double header_HubbleParam;
        byte[] header_fill = new byte[256 - 6 * 4 - 6 * 8 - 2 * 8 - 2 * 4 - 6 * 4 - 2 * 4 - 4 * 8]; // fills to 256 Bytes

        IrregularField outField = null;
        FileImageInputStream in;
        try {
            int pc = 0;
            in = new FileImageInputStream(f);
            in.setByteOrder(ByteOrder.BIG_ENDIAN);

            System.out.println("Testing data endianness...");
            int test0 = in.readInt();
            if (test0 != 256) {
                System.out.println("BIG ENDIAN: error");
                in.setByteOrder(ByteOrder.LITTLE_ENDIAN);
                in.seek(0);
                test0 = in.readInt();
                if (test0 != 256) {
                    System.out.println("LITTLE ENDIAN: error");
                    return null;
                } else {
                    System.out.println("LITTLE ENDIAN: OK");
                }
            } else {
                System.out.println("BIG ENDIAN: OK");
            }

            //start read header
            in.readFully(header_npart, 0, 6);
            in.readFully(header_mass, 0, 6);
            header_time = in.readDouble();
            header_redshift = in.readDouble();
            header_flag_sfr = in.readInt();
            header_flag_feedback = in.readInt();
            in.readFully(header_npartTotal, 0, 6);
            header_flag_cooling = in.readInt();
            header_num_files = in.readInt();
            header_BoxSize = in.readDouble();
            header_Omega0 = in.readDouble();
            header_OmegaLambda = in.readDouble();
            header_HubbleParam = in.readDouble();
            in.readFully(header_fill, 0, header_fill.length);
            skipInt(in, 1);
            //end read header

            int NumPart = 0, ntot_withmasses = 0, Ngas = 0;
            int dn_NumPart = 0, dn_ntot_withmasses = 0, dn_Ngas = 0;
            int typeCount = 0;
            for (int k = 0; k < 6; k++) { //dlaczego 5 a nie 6 ???
                NumPart += header_npart[k];
                dn_NumPart += header_npart[k] / dn;
                if (header_npart[k] > 0) {
                    typeCount++;
                }
            }
            Ngas = header_npart[0];
            dn_Ngas = header_npart[0] / dn;
            for (int k = 0; k < 6; k++) { //dlaczego 5 a nie 6 ???
                if (header_mass[k] == 0) {
                    ntot_withmasses += header_npart[k];
                    dn_ntot_withmasses += header_npart[k] / dn;
                }
            }

            System.out.println("Header represents " + NumPart + " particles, reading with downsize: " + dn_NumPart + " particles");
            System.out.println("estimating memory needs...");
            long mem = dn_NumPart * 3 * 4; //pos
            if (params.isReadVelocity()) {
                mem += dn_NumPart * 3 * 4;
            }
            if (params.isReadId()) {
                mem += dn_NumPart * 4;
            }
            if (params.isReadType()) {
                mem += dn_NumPart * 4;
            }
            if (params.isReadMass()) {
                mem += dn_NumPart * 4;
            }

            if (params.isReadEnergy()) {
                mem += dn_Ngas * 4;
            }
            if (params.isReadDensity()) {
                mem += dn_Ngas * 4;
            }
            if (params.isReadTemperature()) {
                mem += dn_Ngas * 4;
            }

            //memory for cellsets
            mem += dn_NumPart * 4;
            mem += dn_NumPart;

            System.out.println("estimated size: " + (mem / (1024 * 1024)) + " MB");

            System.out.print("allocating memory...");

            float[] pos = null;
            float[] vel = null;
            int[] id = null;
            int[] type = null;
            float[] mass = null;
            float[] u = null;
            float[] rho = null;
            float[] temp = null;

            pos = new float[dn_NumPart * 3];
            if (params.isReadVelocity()) {
                //System.out.println("particle velocity");
                vel = new float[dn_NumPart * 3];
            }
            if (params.isReadId()) {
                //System.out.println("particle ID");
                id = new int[dn_NumPart];
            }
            if (params.isReadType()) {
                //System.out.println("particle type");
                type = new int[dn_NumPart];
            }
            if (params.isReadMass()) {
                //System.out.println("particle mass");
                mass = new float[dn_NumPart];
            }

            if (params.isReadEnergy()) {
                //System.out.println("internal energy");
                u = new float[dn_Ngas];
            }
            if (params.isReadDensity()) {
                //System.out.println("density");
                rho = new float[dn_Ngas];
            }
            if (params.isReadTemperature()) {
                //System.out.println("temperature");
                temp = new float[dn_Ngas];
            }

            System.out.println(" done.");

            progress = 0.1f;
            fireStateChanged();

            System.out.print("reading data...");

            skipInt(in, 1);
            for (int k = 0, pc_new = pc; k < 6; k++) {
                for (int n = 0; n < header_npart[k]; n++) {
                    if (n % dn == 0 && pc_new < header_npart[k] / dn) {
                        in.readFully(pos, 3 * pc_new, 3);
                        pc_new++;
                    } else {
                        in.skipBytes(3 * 4);
                    }
                }
            }
            skipInt(in, 1);

            progress = 0.1f + 0.8f * (float) in.getStreamPosition() / (float) in.length();
            fireStateChanged();

            skipInt(in, 1);

            if (params.isReadVelocity()) {
                for (int k = 0, pc_new = pc; k < 6; k++) {
                    for (int n = 0; n < header_npart[k]; n++) {
                        if (n % dn == 0 && pc_new < header_npart[k] / dn) {
                            in.readFully(vel, 3 * pc_new, 3);
                            pc_new++;
                        } else {
                            in.skipBytes(3 * 4);
                        }
                    }
                }
            } else {
                in.skipBytes(4 * 3 * NumPart);
            }
            skipInt(in, 1);

            progress = 0.1f + 0.8f * (float) in.getStreamPosition() / (float) in.length();
            fireStateChanged();

            skipInt(in, 1);
            if (params.isReadId()) {
                for (int k = 0, pc_new = pc; k < 6; k++) {
                    for (int n = 0; n < header_npart[k]; n++) {
                        if (n % dn == 0 && pc_new < header_npart[k] / dn) {
                            in.readFully(id, pc_new, 1);
                            pc_new++;
                        } else {
                            in.skipBytes(4);
                        }

                    }
                }
            } else {
                in.skipBytes(4 * NumPart);
            }
            skipInt(in, 1);

            progress = 0.1f + 0.8f * (float) in.getStreamPosition() / (float) in.length();
            fireStateChanged();

            if (ntot_withmasses > 0) {
                skipInt(in, 1);
            }

            for (int k = 0, pc_new = pc; k < 6; k++) {
                for (int n = 0; n < header_npart[k]; n++) {
                    if (n % dn == 0 && pc_new < header_npart[k] / dn) {
                        if (params.isReadType()) {
                            type[pc_new] = k;
                        }

                        if (header_mass[k] == 0) {
                            if (params.isReadMass()) {
                                in.readFully(mass, pc_new, 1);
                            } else {
                                in.skipBytes(4);
                            }
                        } else {
                            if (params.isReadMass()) {
                                mass[pc_new] = (float) header_mass[k];
                            }
                        }
                        pc_new++;
                    } else {
                        if (header_mass[k] == 0) {
                            in.skipBytes(4);
                        }
                    }
                }
                progress = 0.1f + 0.8f * (float) in.getStreamPosition() / (float) in.length();
                fireStateChanged();

            }
            if (ntot_withmasses > 0) {
                skipInt(in, 1);
            }

            if (header_npart[0] > 0) {
                skipInt(in, 1);
                if (params.isReadEnergy()) {
                    for (int n = 0, pc_sph = pc; n < header_npart[0]; n++) {
                        if (n % dn == 0 && pc_sph < header_npart[0] / dn) {
                            in.readFully(u, pc_sph, 1);
                            pc_sph++;
                        } else {
                            in.skipBytes(4);
                        }
                    }
                } else {
                    in.skipBytes(Ngas * 4);
                }
                skipInt(in, 1);

                progress = 0.1f + 0.8f * (float) in.getStreamPosition() / (float) in.length();
                fireStateChanged();

                skipInt(in, 1);
                if (params.isReadDensity()) {
                    for (int n = 0, pc_sph = pc; n < header_npart[0]; n++) {
                        if (n % dn == 0 && pc_sph < header_npart[0] / dn) {
                            in.readFully(rho, pc_sph, 1);
                            pc_sph++;
                        } else {
                            in.skipBytes(4);
                        }
                    }
                } else {
                    in.skipBytes(Ngas * 4);
                }
                //skipInt(in,1);
            }
            in.close();
            //end read file

            progress = 0.9f;
            fireStateChanged();

            //calculate temperature
            if (params.isReadTemperature() && params.isReadEnergy()) {
                double BOLTZMANN, PROTONMASS;
                double UnitLength_in_cm, UnitMass_in_g, UnitVelocity_in_cm_per_s;
                double UnitTime_in_s, UnitEnergy_in_cgs;
                double Xh;

                double MeanWeight, gamma, uu;

                /*
                 * physical constants in cgs units
                 */
                BOLTZMANN = 1.3806e-16;
                PROTONMASS = 1.6726e-24;

                /*
                 * internal unit system of the code
                 */
                UnitLength_in_cm = 3.085678e21; /*
                 * code length unit in cm/h
                 */

                UnitMass_in_g = 1.989e43; /*
                 * code mass unit in g/h
                 */

                UnitVelocity_in_cm_per_s = 1.0e5;
                UnitTime_in_s = UnitLength_in_cm / UnitVelocity_in_cm_per_s;
                UnitEnergy_in_cgs = UnitMass_in_g * pow(UnitLength_in_cm, 2) / pow(UnitTime_in_s, 2);
                Xh = 0.76; /*
                 * mass fraction of hydrogen
                 */

                gamma = 5.0 / 3;

                for (int i = 0; i < dn_Ngas; i++) {
                    MeanWeight = 4.0 / (3 * Xh + 1 + 4 * Xh) * PROTONMASS; //Ne=1.0
                    uu = u[i] * UnitEnergy_in_cgs / UnitMass_in_g;
                    /*
                     * get temperature in Kelvin
                     */
                    temp[i] = (float) (MeanWeight / BOLTZMANN * (gamma - 1) * uu);
                }
            }
            //done data
            System.out.println("done");

            progress = 0.95f;
            fireStateChanged();

            System.out.print("creating field...");
            outField = new IrregularField(dn_NumPart);
            //create data arrays
            //create cells
            //create cellsets

            outField.setCurrentCoords(new FloatLargeArray(pos));
            if (params.isReadVelocity()) {
                outField.addComponent(DataArray.create(vel, 3, "velocity"));
            }
            if (params.isReadId()) {
                outField.addComponent(DataArray.create(id, 1, "ID"));
            }
            if (params.isReadType()) {
                outField.addComponent(DataArray.create(type, 1, "type"));
            }
            if (params.isReadMass()) {
                outField.addComponent(DataArray.create(mass, 1, "mass"));
            }

            for (int k = 0, pc_new = 0; k < 6; k++) {
                if (header_npart[k] == 0) {
                    continue;
                }
                int[] cells = new int[header_npart[k] / dn];
                byte[] orient = new byte[header_npart[k] / dn];
                for (int i = 0; i < cells.length; i++, pc_new++) {
                    cells[i] = pc_new;
                    orient[i] = 1;
                }
                CellArray ca = new CellArray(CellType.POINT, cells, orient, null);
                String csName = "cs_";
                switch (k) {
                    case 0:
                        csName += "Gas";
                        break;
                    case 1:
                        csName += "Halo";
                        break;
                    case 2:
                        csName += "Disk";
                        break;
                    case 3:
                        csName += "Bulge";
                        break;
                    case 4:
                        csName += "Stars";
                        break;
                    case 5:
                        csName += "Bndry";
                        break;
                }
                CellSet cs = new CellSet(csName);
                cs.setCellArray(ca);
                if (k == 0 && dn_Ngas > 0) {
                    if (params.isReadEnergy()) {
                        cs.addComponent(DataArray.create(u, 1, "energy"));
                    }
                    if (params.isReadDensity()) {
                        cs.addComponent(DataArray.create(rho, 1, "density"));
                    }
                    if (params.isReadTemperature()) {
                        cs.addComponent(DataArray.create(temp, 1, "temperature"));
                    }
                }
                outField.addCellSet(cs);
            }

            progress = 1.0f;
            fireStateChanged();
            System.out.println("done");
        } catch (Exception ex) {
            System.out.println("ERROR!");
            ex.printStackTrace();
            return null;
        } catch (OutOfMemoryError err) {
            System.out.println("ERROR!");
            System.err.println("Out of memory!");
            return null;
        }
        return outField;
    }

    private static void skipInt(FileImageInputStream in, int n) throws IOException
    {
        in.skipBytes(n * 4);
    }

    public float getProgress()
    {
        return progress;
    }

    /**
     * Utility field holding list of ChangeListeners.
     */
    protected transient ArrayList<ChangeListener> changeListenerList = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     *
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     *
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param object Parameter #1 of the
     *               <CODE>ChangeEvent<CODE> constructor.
     */
    public void fireStateChanged()
    {
        ChangeEvent e = new ChangeEvent(this);
        for (int i = 0; i < changeListenerList.size(); i++) {
            changeListenerList.get(i).stateChanged(e);
        }
    }

    public GadgetFileHeader readGadgetFileHeader(String filePath)
    {
        return GadgetFileHeader.read(filePath);
    }

    public IrregularField readGadgetFullDataTimeSequence(String[] filePaths)
    {

        if (filePaths == null)
            return null;

        if (filePaths.length == 1)
            return readGadgetFullData(filePaths[0]);

        for (int i = 0; i < filePaths.length; i++) {
            File f = new File(filePaths[i]);
            if (!f.exists() || !f.canRead()) {
                System.err.println("ERROR: cannot read file " + filePaths[i]);
                return null;
            }
        }

        progress = 0.0f;
        fireStateChanged();

        int nFrames = filePaths.length;

        GadgetFileHeader[] headers = new GadgetFileHeader[nFrames];
        int[] nParts = new int[nFrames];
        int[] tmp;
        int nPartsMax = 0;
        int nPartsMaxGas = 0;
        int iPartsMax = 0;

        for (int i = 0; i < nFrames; i++) {
            headers[i] = GadgetFileHeader.read(filePaths[i]);
            if (headers[i] == null) {
                System.err.println("ERROR: error reading file " + filePaths[i]);
                return null;
            }
            nParts[i] = 0;
            tmp = headers[i].getNpart();
            for (int j = 0; j < tmp.length; j++) {
                nParts[i] += tmp[j];
            }
            if (nParts[i] > nPartsMax) {
                nPartsMax = nParts[i];
                nPartsMaxGas = tmp[0];
                iPartsMax = i;
            }
        }

        System.out.println("Maximum of " + nPartsMax + " particles found");
        System.out.println("estimating memory needs...");

        //positions
        long mem = nFrames * nPartsMax * 3 * 4;
        mem += nPartsMax * 3 * 4; //for tmp

        if (params.isReadVelocity()) {
            mem += nPartsMax * 3 * 4;
        }

        //ID
        mem += 3 * nPartsMax * 4; //for store, tmp and addressing

        if (params.isReadType()) {
            mem += nPartsMax * 4;
        }

        if (params.isReadMass()) {
            mem += nPartsMax * 4;
        }

        if (params.isReadEnergy()) {
            mem += nPartsMaxGas * 4;
        }
        if (params.isReadDensity()) {
            mem += nPartsMaxGas * 4;
        }
        if (params.isReadTemperature()) {
            mem += nPartsMaxGas * 4;
        }

        //memory for cellsets
        mem += nPartsMax * 4;
        mem += nPartsMax;

        //memory fo validity mask
        mem += nFrames * nPartsMax;
        System.out.println("estimated size: " + (mem / (1024 * 1024)) + " MB");

        System.out.print("allocating memory...");

        float[] pos = null;
        float[] vel = null;
        int[] ids = null;
        int[] type = null;
        float[] mass = null;
        float[] u = null;
        float[] rho = null;
        float[] temp = null;
        LogicLargeArray valid = null;

        pos = new float[nFrames * nPartsMax * 3];
        for (int i = 0; i < pos.length; i++) {
            pos[i] = 0;
        }

        valid = new LogicLargeArray(nFrames * nPartsMax, false);

        System.out.println(" done.");

        IrregularField tmpField = readGadgetFullData(filePaths[iPartsMax], true);
        if (params.isReadVelocity()) {
            vel = (float[])tmpField.getComponent("velocity").getRawArray().getData();
        }

        ids = (int[])tmpField.getComponent("ID").getRawArray().getData();

        if (params.isReadType()) {
            type = (int[])tmpField.getComponent("type").getRawArray().getData();
        }
        if (params.isReadMass()) {
            mass = (float[])tmpField.getComponent("mass").getRawArray().getData();
        }

        if (params.isReadEnergy()) {
            u = (float[])tmpField.getCellSet(0).getComponent(0).getRawArray().getData(); //energy
        }
        if (params.isReadDensity()) {
            rho = (float[])tmpField.getCellSet(0).getComponent(1).getRawArray().getData(); //density
        }
        if (params.isReadTemperature()) {
            temp = (float[])tmpField.getCellSet(0).getComponent(2).getRawArray().getData(); //tmperature
        }

        float[] posTmp;
        int[] idTmp;

        posTmp = tmpField.getCurrentCoords() == null ? null : tmpField.getCurrentCoords().getData();
        System.arraycopy(posTmp, 0, pos, iPartsMax * nPartsMax * 3, posTmp.length);
        for (int i = 0; i < nPartsMax; i++) {
            valid.setBoolean(iPartsMax * nPartsMax + i, true);
        }
        tmpField = null;

        if (params.isReadTemperature() && params.isReadEnergy()) {
            double BOLTZMANN, PROTONMASS;
            double UnitLength_in_cm, UnitMass_in_g, UnitVelocity_in_cm_per_s;
            double UnitTime_in_s, UnitEnergy_in_cgs;
            double Xh;

            double MeanWeight, gamma, uu;

            /* physical constants in cgs units */
            BOLTZMANN = 1.3806e-16;
            PROTONMASS = 1.6726e-24;

            /* internal unit system of the code */
            UnitLength_in_cm = 3.085678e21; /*  code length unit in cm/h */

            UnitMass_in_g = 1.989e43; /*  code mass unit in g/h */

            UnitVelocity_in_cm_per_s = 1.0e5;
            UnitTime_in_s = UnitLength_in_cm / UnitVelocity_in_cm_per_s;
            UnitEnergy_in_cgs = UnitMass_in_g * pow(UnitLength_in_cm, 2) / pow(UnitTime_in_s, 2);
            Xh = 0.76; /* mass fraction of hydrogen */

            gamma = 5.0 / 3;

            for (int i = 0; i < nPartsMaxGas; i++) {
                MeanWeight = 4.0 / (3 * Xh + 1 + 4 * Xh) * PROTONMASS; //Ne=1.0
                uu = u[i] * UnitEnergy_in_cgs / UnitMass_in_g;
                /* get temperature in Kelvin */
                temp[i] = (float) (MeanWeight / BOLTZMANN * (gamma - 1) * uu);
            }
        }
        //done data

        progress = 0.9f / (float) nFrames;
        fireStateChanged();

        //read position timesequence       
        for (int n = 0, c = 0; n < nFrames; n++) {
            if (n == iPartsMax)
                continue;

            c++;
            posTmp = new float[3 * nParts[n]];
            idTmp = new int[nParts[n]];
            if (!readGadgetFullPositionsIds(headers[n], posTmp, idTmp)) {
                System.err.println("ERROR: error reading data from file " + headers[n].getFilePath());
                return null;
            }

            boolean copy = false;
            if (idTmp.length == ids.length)
                copy = checkId(ids, idTmp);

            //compareID(id, idTmp);
            if (copy) {
                System.out.println("frame " + n + " copy geometry");
                System.arraycopy(posTmp, 0, pos, n * nPartsMax * 3, posTmp.length);
                for (int i = 0; i < idTmp.length; i++) {
                    valid.setBoolean(n * nPartsMax + i, true);
                }
            } else {
                //different particle configuration readdress
                for (int i = 0; i < idTmp.length; i++) {

                    //----------------------------------
                    //int id = findId(idTmp[i], ids);
                    int id = -1;
                    for (int k = 0; k < ids.length; k++) {
                        if (ids[k] == idTmp[i]) {
                            id = k;
                            break;
                        }
                    }

                    //------------
                    if (id == -1) {
                        System.out.println("WARNING id from file " + headers[i].getFilePath() + " not found in file " + headers[iPartsMax].getFilePath());
                        continue;
                    }
                    for (int m = 0; m < 3; m++) {
                        pos[n * nPartsMax * 3 + 3 * id + m] = posTmp[3 * i + m];
                    }
                    valid.setBoolean(n * nPartsMax + id, true);
                }
            }

            progress = (c + 1) * 0.9f / (float) nFrames;
            fireStateChanged();
        }

        //System.err.println("TEST: maxID="+maxID);
        //System.err.println("TEST: minID="+minID);
        //TODO copy positions into invalid steps
        for (int n = 1, l = nPartsMax; n < nFrames; n++) {
            for (int i = 0; i < nPartsMax; i++, l++) {
                if (valid.getBoolean(n * nPartsMax + i) && !valid.getBoolean((n - 1) * nPartsMax + i)) {
                    //validated celll - copy position to previous invalid
                    for (int j = n - 1; j >= 0; j--) {
                        if (valid.getBoolean(j * nPartsMax + i))
                            break;
                        for (int m = 0; m < 3; m++) {
                            pos[3 * j * nPartsMax + 3 * i + m] = pos[3 * n * nPartsMax + 3 * i + m];
                        }
                    }
                }

                if (!valid.getBoolean(n * nPartsMax + i) && valid.getBoolean((n - 1) * nPartsMax + i)) {
                    //invalidated cell - copy last position to following invalid
                    for (int j = n; j < nFrames; j++) {
                        if (valid.getBoolean(j * nPartsMax + i))
                            break;
                        for (int m = 0; m < 3; m++) {
                            pos[3 * j * nPartsMax + 3 * i + m] = pos[3 * (n - 1) * nPartsMax + 3 * i + m];
                        }
                    }
                }

            }
        }

        //build out field and cellsets
        System.out.print("creating field...");
        IrregularField outField = new IrregularField(nPartsMax);

        outField.setCurrentCoords(new FloatLargeArray(pos));
        outField.setCurrentMask(valid);
        if (params.isReadVelocity())
            outField.addComponent(DataArray.create(vel, 3, "velocity"));
        outField.addComponent(DataArray.create(ids, 1, "ID"));
        if (params.isReadType())
            outField.addComponent(DataArray.create(type, 1, "type"));
        if (params.isReadMass())
            outField.addComponent(DataArray.create(mass, 1, "mass"));

        int pc_new = 0;
        int[] header_npart = headers[iPartsMax].getNpart();
        for (int k = 0; k < 6; k++) {
            if (header_npart[k] == 0)
                continue;
            int[] cells = new int[header_npart[k]];
            byte[] orient = new byte[header_npart[k]];
            for (int i = 0; i < cells.length; i++, pc_new++) {
                cells[i] = pc_new;
                orient[i] = 1;
            }
            CellArray ca = new CellArray(CellType.POINT, cells, orient, null);
            String csName = "cs_";
            switch (k) {
                case 0:
                    csName += "Gas";
                    break;
                case 1:
                    csName += "Halo";
                    break;
                case 2:
                    csName += "Disk";
                    break;
                case 3:
                    csName += "Bulge";
                    break;
                case 4:
                    csName += "Stars";
                    break;
                case 5:
                    csName += "Bndry";
                    break;
            }
            CellSet cs = new CellSet(csName);
            cs.setCellArray(ca);
            if (k == 0 && nPartsMaxGas > 0) {
                if (params.isReadEnergy())
                    cs.addComponent(DataArray.create(u, 1, "energy"));
                if (params.isReadDensity())
                    cs.addComponent(DataArray.create(rho, 1, "density"));
                if (params.isReadTemperature())
                    cs.addComponent(DataArray.create(temp, 1, "temperature"));
            }
            outField.addCellSet(cs);
        }

        progress = 1.0f;
        fireStateChanged();

        System.out.print("done.");
        return outField;
    }

    private boolean readGadgetFullPositionsIds(GadgetFileHeader header, float[] pos, int[] id)
    {
        if (pos == null || id == null || header == null) {
            return false;
        }

        File f = new File(header.getFilePath());

        FileImageInputStream in;
        try {
            int[] header_npart = header.getNpart();
            int NumPart = 0;
            for (int k = 0; k < 6; k++) {
                NumPart += header_npart[k];
            }

            in = new FileImageInputStream(f);
            in.setByteOrder(header.getEndian());

            skipInt(in, 1);
            in.skipBytes(256); //skip header
            skipInt(in, 1);

            skipInt(in, 1);
            in.readFully(pos, 0, 3 * NumPart); //read positions
            skipInt(in, 1);

            skipInt(in, 1);
            in.skipBytes(4 * 3 * NumPart); //skip velocities
            skipInt(in, 1);

            skipInt(in, 1);
            in.readFully(id, 0, NumPart); //read ID
            skipInt(in, 1);

            in.close();
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    private boolean checkId(int[] id1, int[] id2)
    {
        if (id1 == null || id2 == null) {
            return false;
        }
        if (id1.length != id2.length) {
            return false;
        }

        for (int i = 0; i < id2.length; i++) {
            if (id1[i] != id2[i]) {
                return false;
            }
        }
        return true;
    }

    private boolean checkIdContinous(int[] id)
    {
        boolean isIdContinous = true;
        for (int i = 1; i < id.length; i++) {
            if (id[i] != id[i - 1] + 1) {
                isIdContinous = false;
                break;
            }
        }
        return isIdContinous;
    }

    //    private int findId(int id, int[] ids) {
    //        for (int i = 0; i < ids.length; i++) {
    //            if(ids[i] == id)
    //                return i;
    //        }        
    //        return -1;
    //    }
    int maxID = 0;
    int minID = 0;

    private void compareID(int[] id, int[] idTmp)
    {
        if (id == null || idTmp == null) {
            System.err.println("TEST: null");
            return;
        }

        for (int i = 0; i < idTmp.length; i++) {
            int c = 0;
            for (int j = 0; j < id.length; j++) {
                if (id[j] == idTmp[i]) {
                    c++;
                }
            }
            if (c > 1 && idTmp[i] != 0) {
                System.err.println("TEST: id=" + idTmp[i] + " found " + c + " times");
            }

            if (idTmp[i] > maxID) {
                maxID = idTmp[i];
            }
            if (idTmp[i] < minID) {
                minID = idTmp[i];
            }
        }

        //System.err.println("TEST: OK");
    }

    public IrregularField readGadgetFullDataTimeSequenceThr(String[] filePaths)
    {

        if (filePaths == null) {
            return null;
        }

        if (filePaths.length == 1) {
            return readGadgetFullData(filePaths[0]);
        }

        for (int i = 0; i < filePaths.length; i++) {
            File f = new File(filePaths[i]);
            if (!f.exists() || !f.canRead()) {
                System.err.println("ERROR: cannot read file " + filePaths[i]);
                return null;
            }
        }

        progress = 0.0f;
        fireStateChanged();

        int nFrames = filePaths.length;

        GadgetFileHeader[] headers = new GadgetFileHeader[nFrames];
        int[] nParts = new int[nFrames];
        int[] tmp;
        int nPartsMax = 0;
        int nPartsMaxGas = 0;
        int iPartsMax = 0;

        for (int i = 0; i < nFrames; i++) {
            headers[i] = GadgetFileHeader.read(filePaths[i]);
            if (headers[i] == null) {
                System.err.println("ERROR: error reading file " + filePaths[i]);
                return null;
            }
            nParts[i] = 0;
            tmp = headers[i].getNpart();
            for (int j = 0; j < tmp.length; j++) {
                nParts[i] += tmp[j];
            }
            if (nParts[i] > nPartsMax) {
                nPartsMax = nParts[i];
                nPartsMaxGas = tmp[0];
                iPartsMax = i;
            }
        }

        System.out.println("Maximum of " + nPartsMax + " particles found");
        System.out.println("estimating memory needs...");

        //positions
        long mem = nFrames * nPartsMax * 3 * 4;
        mem += nPartsMax * 3 * 4; //for tmp

        if (params.isReadVelocity()) {
            mem += nPartsMax * 3 * 4;
        }

        //ID
        if (!params.isReadId())
            params.setReadId(true);
        mem += 3 * nPartsMax * 4; //for store, tmp and addressing

        if (params.isReadType()) {
            mem += nPartsMax * 4;
        }

        if (params.isReadMass()) {
            mem += nPartsMax * 4;
        }

        if (params.isReadEnergy()) {
            mem += nPartsMaxGas * 4;
        }
        if (params.isReadDensity()) {
            mem += nPartsMaxGas * 4;
        }
        if (params.isReadTemperature()) {
            mem += nPartsMaxGas * 4;
        }

        //memory for cellsets
        mem += nPartsMax * 4;
        mem += nPartsMax;

        //memory fo validity mask
        mem += nFrames * nPartsMax;
        System.out.println("estimated size: " + (mem / (1024 * 1024)) + " MB");

        System.out.print("allocating memory...");

        float[] pos = null;
        float[] vel = null;
        int[] ids = null;
        int[] type = null;
        float[] mass = null;
        float[] u = null;
        float[] rho = null;
        float[] temp = null;
        LogicLargeArray valid = null;

        pos = new float[nFrames * nPartsMax * 3];
        for (int i = 0; i < pos.length; i++) {
            pos[i] = 0;
        }

        valid = new LogicLargeArray(nFrames * nPartsMax, false);

        System.out.println(" done.");

        IrregularField tmpField = readGadgetFullData(filePaths[iPartsMax], true);
        if (params.isReadVelocity()) {
            vel = (float[])tmpField.getComponent("velocity").getRawArray().getData();
        }

        ids = (int[])tmpField.getComponent("ID").getRawArray().getData();

        if (params.isReadType()) {
            type = (int[])tmpField.getComponent("type").getRawArray().getData();
        }
        if (params.isReadMass()) {
            mass = (float[])tmpField.getComponent("mass").getRawArray().getData();
        }

        if (params.isReadEnergy()) {
            u = (float[])tmpField.getCellSet(0).getComponent(0).getRawArray().getData(); //energy
        }
        if (params.isReadDensity()) {
            rho = (float[])tmpField.getCellSet(0).getComponent(1).getRawArray().getData(); //density
        }
        if (params.isReadTemperature()) {
            temp = (float[])tmpField.getCellSet(0).getComponent(2).getRawArray().getData(); //tmperature
        }

        float[] posTmp;
        posTmp = tmpField.getCurrentCoords() == null ? null : tmpField.getCurrentCoords().getData();
        System.arraycopy(posTmp, 0, pos, iPartsMax * nPartsMax * 3, posTmp.length);
        for (int i = 0; i < nPartsMax; i++) {
            valid.setBoolean(iPartsMax * nPartsMax + i, true);
        }
        tmpField = null;

        if (params.isReadTemperature() && params.isReadEnergy()) {
            double BOLTZMANN, PROTONMASS;
            double UnitLength_in_cm, UnitMass_in_g, UnitVelocity_in_cm_per_s;
            double UnitTime_in_s, UnitEnergy_in_cgs;
            double Xh;

            double MeanWeight, gamma, uu;

            /*
             * physical constants in cgs units
             */
            BOLTZMANN = 1.3806e-16;
            PROTONMASS = 1.6726e-24;

            /*
             * internal unit system of the code
             */
            UnitLength_in_cm = 3.085678e21; /*
             * code length unit in cm/h
             */

            UnitMass_in_g = 1.989e43; /*
             * code mass unit in g/h
             */

            UnitVelocity_in_cm_per_s = 1.0e5;
            UnitTime_in_s = UnitLength_in_cm / UnitVelocity_in_cm_per_s;
            UnitEnergy_in_cgs = UnitMass_in_g * pow(UnitLength_in_cm, 2) / pow(UnitTime_in_s, 2);
            Xh = 0.76; /*
             * mass fraction of hydrogen
             */

            gamma = 5.0 / 3;

            for (int i = 0; i < nPartsMaxGas; i++) {
                MeanWeight = 4.0 / (3 * Xh + 1 + 4 * Xh) * PROTONMASS; //Ne=1.0
                uu = u[i] * UnitEnergy_in_cgs / UnitMass_in_g;
                /*
                 * get temperature in Kelvin
                 */
                temp[i] = (float) (MeanWeight / BOLTZMANN * (gamma - 1) * uu);
            }
        }
        //done data

        progress = 0.9f / (float) nFrames;
        fireStateChanged();

        //read position timesequence       
        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        FrameReaderThread[] threads = new FrameReaderThread[nThreads];
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new FrameReaderThread(i, nThreads, nFrames, iPartsMax, nPartsMax, nParts, headers, ids, valid, pos);
            threads[i].start();
        }

        try {
            for (int i = 0; i < threads.length; i++) {
                threads[i].join();
            }
        } catch (InterruptedException ex) {
            return null;
        }

        for (int n = 1, l = nPartsMax; n < nFrames; n++) {
            for (int i = 0; i < nPartsMax; i++, l++) {
                if (valid.getBoolean(n * nPartsMax + i) && !valid.getBoolean((n - 1) * nPartsMax + i)) {
                    //validated celll - copy position to previous invalid
                    for (int j = n - 1; j >= 0; j--) {
                        if (valid.getBoolean(j * nPartsMax + i)) {
                            break;
                        }
                        for (int m = 0; m < 3; m++) {
                            pos[3 * j * nPartsMax + 3 * i + m] = pos[3 * n * nPartsMax + 3 * i + m];
                        }
                    }
                }

                if (!valid.getBoolean(n * nPartsMax + i) && valid.getBoolean((n - 1) * nPartsMax + i)) {
                    //invalidated cell - copy last position to following invalid
                    for (int j = n; j < nFrames; j++) {
                        if (valid.getBoolean(j * nPartsMax + i)) {
                            break;
                        }
                        for (int m = 0; m < 3; m++) {
                            pos[3 * j * nPartsMax + 3 * i + m] = pos[3 * (n - 1) * nPartsMax + 3 * i + m];
                        }
                    }
                }

            }
        }

        //build out field and cellsets
        System.out.print("creating field...");
        IrregularField outField = new IrregularField(nPartsMax);

        outField.setCurrentCoords(new FloatLargeArray(pos));
        outField.setCurrentMask(valid);
        if (params.isReadVelocity()) {
            outField.addComponent(DataArray.create(vel, 3, "velocity"));
        }
        outField.addComponent(DataArray.create(ids, 1, "ID"));
        if (params.isReadType()) {
            outField.addComponent(DataArray.create(type, 1, "type"));
        }
        if (params.isReadMass()) {
            outField.addComponent(DataArray.create(mass, 1, "mass"));
        }

        int pc_new = 0;
        int[] header_npart = headers[iPartsMax].getNpart();
        for (int k = 0; k < 6; k++) {
            if (header_npart[k] == 0) {
                continue;
            }
            int[] cells = new int[header_npart[k]];
            byte[] orient = new byte[header_npart[k]];
            for (int i = 0; i < cells.length; i++, pc_new++) {
                cells[i] = pc_new;
                orient[i] = 1;
            }
            CellArray ca = new CellArray(CellType.POINT, cells, orient, null);
            String csName = "cs_";
            switch (k) {
                case 0:
                    csName += "Gas";
                    break;
                case 1:
                    csName += "Halo";
                    break;
                case 2:
                    csName += "Disk";
                    break;
                case 3:
                    csName += "Bulge";
                    break;
                case 4:
                    csName += "Stars";
                    break;
                case 5:
                    csName += "Bndry";
                    break;
            }
            CellSet cs = new CellSet(csName);
            cs.setCellArray(ca);
            if (k == 0 && nPartsMaxGas > 0) {
                if (params.isReadEnergy()) {
                    cs.addComponent(DataArray.create(u, 1, "energy"));
                }
                if (params.isReadDensity()) {
                    cs.addComponent(DataArray.create(rho, 1, "density"));
                }
                if (params.isReadTemperature()) {
                    cs.addComponent(DataArray.create(temp, 1, "temperature"));
                }
            }
            outField.addCellSet(cs);
        }

        progress = 1.0f;
        fireStateChanged();

        System.out.print("done.");
        return outField;
    }

    private class FrameReaderThread extends Thread
    {

        int iThread;
        int nThreads;

        int nFrames;
        int iPartsMax;
        int nPartsMax;
        int[] nParts;
        GadgetFileHeader[] headers;
        int[] ids;
        LogicLargeArray valid;
        float[] pos;

        public FrameReaderThread(int iThread, int nThreads, int nFrames, int iPartsMax, int nPartsMax, int[] nParts, GadgetFileHeader[] headers, int[] ids, LogicLargeArray valid, float[] pos)
        {
            this.iThread = iThread;
            this.nThreads = nThreads;
            this.nFrames = nFrames;
            this.iPartsMax = iPartsMax;
            this.nPartsMax = nPartsMax;
            this.nParts = nParts;
            this.headers = headers;
            this.ids = ids;
            this.valid = valid;
            this.pos = pos;
        }

        @Override
        public void run()
        {
            int[] idTmp;
            float[] posTmp;
            for (int n = iThread, c = 0; n < nFrames; n += nThreads) {
                c++;
                if (n == iPartsMax) {
                    //System.out.println("thread "+iThread+" skipping file "+(n+1)+" of "+nFrames);
                    continue;
                }
                //System.out.println("thread "+iThread+" reading file "+(n+1)+" of "+nFrames);

                posTmp = new float[3 * nParts[n]];
                idTmp = new int[nParts[n]];
                if (!readGadgetFullPositionsIds(headers[n], posTmp, idTmp)) {
                    System.err.println("ERROR: error reading data from file " + headers[n].getFilePath());
                    return;
                }

                boolean copy = false;
                if (idTmp.length == ids.length) {
                    copy = checkId(ids, idTmp);
                }

                //compareID(id, idTmp);
                if (copy) {
                    System.out.println("frame " + n + " copy geometry");
                    System.arraycopy(posTmp, 0, pos, n * nPartsMax * 3, posTmp.length);
                    for (int i = 0; i < idTmp.length; i++) {
                        valid.setBoolean(n * nPartsMax + i, true);
                    }
                } else {
                    //different particle configuration readdress
                    for (int i = 0; i < idTmp.length; i++) {

                        //----------------------------------
                        //int id = findId(idTmp[i], ids);
                        int id = -1;
                        for (int k = 0; k < ids.length; k++) {
                            if (ids[k] == idTmp[i]) {
                                id = k;
                                break;
                            }
                        }

                        //------------
                        if (id == -1) {
                            System.out.println("WARNING id from file " + headers[i].getFilePath() + " not found in file " + headers[iPartsMax].getFilePath());
                            continue;
                        }
                        for (int m = 0; m < 3; m++) {
                            pos[n * nPartsMax * 3 + 3 * id + m] = posTmp[3 * i + m];
                        }
                        valid.setBoolean(n * nPartsMax + id, true);
                    }
                }

                if (iThread == 0) {
                    progress = (c + 1) * 0.9f / (float) (nFrames / nThreads);
                    fireStateChanged();
                }
            }

        }
    }
}
