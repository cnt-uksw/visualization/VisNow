/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.ReadLAS;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.system.main.VisNow;

/**
 * GUI for LAS reader.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class GUI extends javax.swing.JPanel
{

    private String lastPath = null;
    private Parameters parameters;

    /**
     * Creates new form GUI
     */
    public GUI()
    {
        initComponents();
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        pathTF = new javax.swing.JTextField();
        browseObjButton = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        setMinimumSize(new java.awt.Dimension(200, 400));
        setPreferredSize(new java.awt.Dimension(200, 400));
        setRequestFocusEnabled(false);
        setLayout(new java.awt.GridBagLayout());

        pathTF.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                pathTFActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 0, 0);
        add(pathTF, gridBagConstraints);

        browseObjButton.setText("Browse...");
        browseObjButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                browseObjButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        add(browseObjButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.weighty = 1.0;
        add(filler1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void browseObjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseObjButtonActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(lastPath != null && !lastPath.isEmpty() ?
                                        new File(lastPath) :
                                        new File(VisNow.get().getMainConfig().getUsableDataPath(ReadLAS.class)));
        FileNameExtensionFilter allObiectFilter = new FileNameExtensionFilter("All object files", "las", "LAS", "laz", "LAZ");
        fileChooser.addChoosableFileFilter(allObiectFilter);
        fileChooser.setFileFilter(allObiectFilter);
        int returnVal = fileChooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            String path = fileChooser.getSelectedFile().getAbsolutePath();
            lastPath = path.substring(0, path.lastIndexOf(File.separator));
            VisNow.get().getMainConfig().setLastDataPath(lastPath, ReadLAS.class);
            pathTF.setText(path);
            parameters.set(ReadLASShared.FILENAME, path);
        }
    }//GEN-LAST:event_browseObjButtonActionPerformed

    private void pathTFActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_pathTFActionPerformed
    {//GEN-HEADEREND:event_pathTFActionPerformed
        String path = pathTF.getText();
        parameters.set(ReadLASShared.FILENAME, path);
        lastPath = path.substring(0, path.lastIndexOf(File.separator));
    }//GEN-LAST:event_pathTFActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton browseObjButton;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JTextField pathTF;
    // End of variables declaration//GEN-END:variables

    void activateOpenDialog()
    {
        browseObjButtonActionPerformed(null);
    }

    void setParameters(Parameters parameters)
    {
        this.parameters = parameters;
    }

    void updateGUI(ParameterProxy p)
    {
        lastPath = p.get(ReadLASShared.FILENAME);
        pathTF.setText(lastPath);
    }

}
