/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.readRaw;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import javax.imageio.stream.FileImageInputStream;

/**
 * Utility class for serving multiple files as single stream with additional options:
 * - auto skip head/tail for each file
 * - reading only particular number of bytes from each file (slice mode).
 * <p>
 * @author Marcin Szpak, University of Warsaw, ICM
 */
class FilePipe
{
    final private String[] filenames;
    final private ByteOrder byteOrder;
    final private long inFileAlignment;
    final private boolean asSlices; //as slices or as stream
    final private long singleSliceByteLength;

    private int currentFileNameIndex;
    private FileImageInputStream currentInputStream;
    private long currentFileBytesRead;
    private long currentFileEndAfterAlignmentAndSlicing;
    private byte[] buffer = new byte[0];

    /**
     * Assuming that every file is big enough to fit inFileAlignment (and singleSliceByteLength - in slice mode)
     * <p>
     * @param asSlices              read only singleSliceByteLength from each file
     * @param singleSliceByteLength number of bytes to read from each file (only when asSlices == true)
     */
    public FilePipe(String[] filenames, ByteOrder byteOrder, long inFileAlignment, boolean asSlices, long singleSliceByteLength)
    {
        this.filenames = filenames;
        this.byteOrder = byteOrder;
        this.inFileAlignment = inFileAlignment;
        this.asSlices = asSlices;
        this.singleSliceByteLength = singleSliceByteLength;
    }

    private void nextFile() throws IOException
    {
        File file = new File(filenames[currentFileNameIndex++]);
        currentInputStream = new FileImageInputStream(file);
        long currentFileLength = file.length();
        if (inFileAlignment >= 0) {
            currentFileBytesRead = inFileAlignment;
            currentInputStream.skipBytes(currentFileBytesRead);
            if (asSlices) currentFileEndAfterAlignmentAndSlicing = singleSliceByteLength + inFileAlignment;
            else currentFileEndAfterAlignmentAndSlicing = currentFileLength;
        } else {
            if (asSlices) {
                currentFileBytesRead = currentFileLength - (-1 - inFileAlignment) - singleSliceByteLength;
                currentInputStream.skipBytes(currentFileBytesRead);
                currentFileEndAfterAlignmentAndSlicing = currentFileLength - (-1 - inFileAlignment);
            } else {
                currentFileEndAfterAlignmentAndSlicing = currentFileLength - (-1 - inFileAlignment);
                currentFileBytesRead = 0;
            }
        }
    }

    private void readBytes(int len) throws IOException
    {
        if (buffer.length < len) buffer = new byte[len];
        if (currentInputStream == null) nextFile();
        int read = 0;
        while (read < len) {
            long bytesLeft = currentFileEndAfterAlignmentAndSlicing - currentFileBytesRead;
            if (bytesLeft == 0) nextFile();
            int toRead = (int) Math.min(len - read, bytesLeft);
            currentInputStream.readFully(buffer, read, toRead);
            read += toRead;
            currentFileBytesRead += toRead;
        }
    }

    public void skipBytes(long len) throws IOException
    {
        if (currentInputStream == null) nextFile();
        long skipped = 0;
        while (skipped < len) {
            long bytesLeft = currentFileEndAfterAlignmentAndSlicing - currentFileBytesRead;
            if (bytesLeft == 0) nextFile();
            long toSkip = Math.min(len - skipped, bytesLeft);
            currentInputStream.skipBytes(toSkip);
            skipped += toSkip;
            currentFileBytesRead += toSkip;
        }
    }

    public void readFully(byte[] array, int off, int len) throws IOException
    {
        readBytes(len);
        System.arraycopy(buffer, 0, array, off, len);
    }

    public void readFully(short[] array, int off, int len) throws IOException
    {
        readBytes(len * 2);
        ByteBuffer.wrap(buffer).order(byteOrder).asShortBuffer().get(array, off, len);
    }

    public void readFully(int[] array, int off, int len) throws IOException
    {
        readBytes(len * 4);
        ByteBuffer.wrap(buffer).order(byteOrder).asIntBuffer().get(array, off, len);
    }

    public void readFully(float[] array, int off, int len) throws IOException
    {
        readBytes(len * 4);
        ByteBuffer.wrap(buffer).order(byteOrder).asFloatBuffer().get(array, off, len);
    }

    public void readFully(double[] array, int off, int len) throws IOException
    {
        readBytes(len * 8);
        ByteBuffer.wrap(buffer).order(byteOrder).asDoubleBuffer().get(array, off, len);
    }
}
