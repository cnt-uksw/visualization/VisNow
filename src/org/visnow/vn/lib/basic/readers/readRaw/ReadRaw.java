/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.readRaw;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import static java.lang.Math.max;
import static java.lang.Math.min;
import java.nio.ByteOrder;
import java.util.Arrays;
import javax.swing.SwingUtilities;
import org.apache.log4j.Logger;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.ShortLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.readers.readRaw.ReadRawShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;

public class ReadRaw extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(ReadRaw.class);

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    private ReadRawComputeUI computeUI;
    private int runQueue = 0;

//    private String previousFileName = "";
    public ReadRaw()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
//                    runQueue += startIfNotInQueue() ? 1 : 0;
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    //                    startIfNotInQueue();
                    startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new ReadRawComputeUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILE_NAMES, new String[0]),
            new Parameter<>(NUMBER_OF_DIMENSIONS, 3),
            new Parameter<>(DATA_DIMS, new long[]{2, 2, 2}),
            new Parameter<>(FIELD_DIMS, new long[]{2, 2, 2}),
            new Parameter<>(FIELD_OFFSET, new long[]{0, 0, 0}),
            new Parameter<>(IN_FILE_ALIGNMENT, -1l),
            new Parameter<>(NUMBER_TYPE, NumberType.BYTE),
            new Parameter<>(ENDIANNESS, Endianness.BIG),
            new Parameter<>(META_FILE_SIZE_INFO, "0"),
            new Parameter<>(RUNNING_MESSAGE, RUN_DYNAMICALLY)
        };
    }

    /**
     * Generate out field based on input data dims, output field dims and output
     * field offset. Steps are: 1. Calculate intersection dims 2. Calculate
     * inner skips (empty space between intersection field and data/output
     * field) 3. Calculate start positions (in field and data)
     *
     */
    private RegularField generateOutField(Parameters p, ProgressAgent progressAgent)
    {
        String[] filenames = p.get(FILE_NAMES);
        int numberOfDimensions = p.get(NUMBER_OF_DIMENSIONS);
        long[] dataDims = p.get(DATA_DIMS);
        long[] fieldDims = p.get(FIELD_DIMS);
        long[] fieldOffset = p.get(FIELD_OFFSET);
        NumberType numberType = p.get(NUMBER_TYPE);

        //calculate size of one number
        int typeWidth = getTypeWidth(p.get(NUMBER_TYPE));

        long singleSliceByteLength = typeWidth * boxVolume(dataDims, filenames.length == 1 ? numberOfDimensions : numberOfDimensions - 1);

        FilePipe filePipe = new FilePipe(filenames,
                                         p.get(ENDIANNESS).equals(Endianness.BIG) ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN,
                                         p.get(IN_FILE_ALIGNMENT), numberOfDimensions != 1,
                                         singleSliceByteLength);

        try {

            //calculate intersection of field and data
            //(size and start at every dimension)
            long[] intersectionDims = new long[numberOfDimensions];
            long[] dataStart = new long[numberOfDimensions];
            long[] fieldStart = new long[numberOfDimensions];

            for (int i = 0; i < numberOfDimensions; i++) {
                long data = dataDims[i];
                long field = fieldDims[i];
                long offset = fieldOffset[i];
                //if offset is negative then start from 0
                dataStart[i] = max(offset, 0);
                //if offset is negative then start in the middle of the field
                fieldStart[i] = max(0, -offset);
                //calculate intersection
                intersectionDims[i] = max(0, min(offset + field, data) - max(0, offset));
            }

            LOGGER.debug("Data dims: " + Arrays.toString(dataDims));
            LOGGER.debug("Field dims: " + Arrays.toString(fieldDims));
            LOGGER.debug("Offset: " + Arrays.toString(fieldOffset));
            LOGGER.debug("Intersection dims: " + Arrays.toString(intersectionDims));
            LOGGER.debug("Data start: " + Arrays.toString(dataStart));
            LOGGER.debug("Field start: " + Arrays.toString(fieldStart));

            long fieldSize = boxVolume(fieldDims, numberOfDimensions);
            long intersectionSize = boxVolume(intersectionDims, numberOfDimensions);

            //full dimIndex (length = numberOfDimensions) is not needed, here is set to avoid NPE on skip line bytes
            long[] dimIndex = new long[numberOfDimensions];//new long[numberOfDimensions - 1];

            long fieldPosition = 0;
            LargeArray fieldData;

            //set start positions (input data, output field)
            long dataSkipSize = 1;
            long fieldSkipSize = 1;
            for (int i = 0; i < numberOfDimensions; i++) {
                filePipe.skipBytes(typeWidth * dataStart[i] * dataSkipSize);
                dataSkipSize *= dataDims[i];

                fieldPosition += fieldStart[i] * fieldSkipSize;
                fieldSkipSize *= fieldDims[i];
            }

            //init data array
            fieldData = initDataArray(numberType, fieldSize);
            int bufferLength = 1000000;
            Object buffer = initBuffer(numberType, bufferLength);

            long lineLength = intersectionDims[0];
            long interSectionPosition = 0;

            while (interSectionPosition < intersectionSize) {
                progressAgent.setProgressStep((int) (interSectionPosition * 95 / intersectionSize));

                //read data (whole line at once)
                int read = 0;
                while (read < intersectionDims[0]) {
                    int toRead = (int) min(lineLength - read, bufferLength);
                    if (numberType.equals(NumberType.BYTE)) {
                        filePipe.readFully((byte[]) buffer, 0, toRead);
                        LargeArrayUtils.arraycopy(new UnsignedByteLargeArray((byte[]) buffer), 0, (UnsignedByteLargeArray) fieldData, fieldPosition, toRead);
                    } else if (numberType.equals(NumberType.SHORT)) {
                        filePipe.readFully((short[]) buffer, 0, toRead);
                        LargeArrayUtils.arraycopy(new ShortLargeArray((short[]) buffer), 0, (ShortLargeArray) fieldData, fieldPosition, toRead);
                    } else if (numberType.equals(NumberType.INT)) {
                        filePipe.readFully((int[]) buffer, 0, toRead);
                        LargeArrayUtils.arraycopy(new IntLargeArray((int[]) buffer), 0, (IntLargeArray) fieldData, fieldPosition, toRead);
                    } else if (numberType.equals(NumberType.FLOAT)) {
                        filePipe.readFully((float[]) buffer, 0, toRead);
                        LargeArrayUtils.arraycopy(new FloatLargeArray((float[]) buffer), 0, (FloatLargeArray) fieldData, fieldPosition, toRead);
                    } else if (numberType.equals(NumberType.DOUBLE)) {
                        filePipe.readFully((double[]) buffer, 0, toRead);
                        LargeArrayUtils.arraycopy(new DoubleLargeArray((double[]) buffer), 0, (DoubleLargeArray) fieldData, fieldPosition, toRead);
                    } else throw new IllegalArgumentException("Type: " + numberType + " not supported!");

                    read += toRead;
                }

                fieldPosition += lineLength;
                interSectionPosition += lineLength;
                dimIndex[0] += lineLength - 1;

                dataSkipSize = 1;
                fieldSkipSize = 1;

                //skip empty space (between intersection and field/data)
                for (int dim = 0; dim < numberOfDimensions - 1; dim++) {
                    if (++dimIndex[dim] < intersectionDims[dim]) {
                        break;
                    } else //if reached end of line (for current dimension) then skip
                    {
                        dimIndex[dim] = 0;
                        filePipe.skipBytes(typeWidth * ((dataDims[dim] - intersectionDims[dim]) * dataSkipSize));
                        dataSkipSize *= dataDims[dim];

                        fieldPosition += (fieldDims[dim] - intersectionDims[dim]) * fieldSkipSize;
                        fieldSkipSize *= fieldDims[dim];
                    }
                }
            }

            int[] dims = new int[numberOfDimensions];

            for (int i = 0; i < numberOfDimensions; i++) {
                dims[i] = (int) fieldDims[i];
            }

            RegularField regularField = new RegularField(dims);
            regularField.setName(new File(filenames[0]).getName() + (filenames.length > 1 ? "+" : ""));
            regularField.addComponent(DataArray.create(fieldData, 1, "Raw data"));
            float[] origin = new float[3];
            for (int i = 0; i < 3 && i < fieldOffset.length; i++) {
                origin[i] = fieldOffset[i];
            }
            regularField.setOrigin(origin);

            return regularField;
        } catch (FileNotFoundException ex) {
            LOGGER.error("File not found. This should not be the case - validator didn't work or sth happend in the middle...", ex);
            return null;
        } catch (IOException ex) {
            LOGGER.error("Error while reading file", ex);
            return null;
        }
    }

    private LargeArray initDataArray(NumberType numberType, long length)
    {
        if (numberType.equals(NumberType.BYTE)) return new UnsignedByteLargeArray(length);
        else if (numberType.equals(NumberType.SHORT)) return new ShortLargeArray(length);
        else if (numberType.equals(NumberType.INT)) return new IntLargeArray(length);
        else if (numberType.equals(NumberType.FLOAT)) return new FloatLargeArray(length);
        else if (numberType.equals(NumberType.DOUBLE)) return new DoubleLargeArray(length);
        else throw new IllegalArgumentException("Type: " + numberType + " not supported!");
    }

    private Object initBuffer(NumberType numberType, int length)
    {
        if (numberType.equals(NumberType.BYTE)) return new byte[length];
        else if (numberType.equals(NumberType.SHORT)) return new short[length];
        else if (numberType.equals(NumberType.INT)) return new int[length];
        else if (numberType.equals(NumberType.FLOAT)) return new float[length];
        else if (numberType.equals(NumberType.DOUBLE)) return new double[length];
        else throw new IllegalArgumentException("Type: " + numberType + " not supported!");
    }

    private boolean validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);

        boolean isValid = true;

        String[] fileNames = parameters.get(FILE_NAMES);
        int numberOfDimensions = parameters.get(NUMBER_OF_DIMENSIONS);
        int typeWidth = getTypeWidth(parameters.get(NUMBER_TYPE));
        long minimumFieldFileSize = (2 * typeWidth) << numberOfDimensions;
//        if (numberOfDimensions == 1 || fileNames.length == 1) parameters.set(MULTIPLE_FILES_MODE, MultipleFilesMode.AS_STREAM);

        if (fileNames.length == 0) isValid = false;
        else {
            boolean emptyOrNotExists = false;
            for (String fileName : fileNames)
                if (!new File(fileName).exists() || new File(fileName).isDirectory() || fileName.isEmpty()) {
                    VisNow.get().userMessageSend(this, "File " + fileNames + " is empty or does not exists", "", Level.ERROR);
                    emptyOrNotExists = true;
                    break;
                }

            if (emptyOrNotExists) isValid = false;
            else {
                long minFileSize = Long.MAX_VALUE;
                long maxFileSize = Long.MIN_VALUE;
                long sumFileSize = 0;
                for (String fileName : fileNames) {
                    long fileSize = new File(fileName).length();
                    minFileSize = Math.min(minFileSize, fileSize);
                    maxFileSize = Math.max(maxFileSize, fileSize);
                    sumFileSize += fileSize;
                }

                parameters.set(META_FILE_SIZE_INFO, minFileSize + (minFileSize != maxFileSize ? "+" : ""));
                //minimum size: double * 2^3
                if (minFileSize < minimumFieldFileSize) {
                    VisNow.get().userMessageSend(this, "File too small", "Minimum file size needs to be (2*NumberTypeWidth)^NumberOfDimensions == " + minimumFieldFileSize, Level.ERROR);
                    isValid = false;
                } else {
                    //1. validate alignment
                    long alignment = parameters.get(IN_FILE_ALIGNMENT);
                    long absoluteAlignment = alignment < 0 ? -1 - alignment : alignment;

                    if (minFileSize < absoluteAlignment) absoluteAlignment = minFileSize;
                    if (minFileSize - absoluteAlignment < minimumFieldFileSize)
                        absoluteAlignment = minFileSize - minimumFieldFileSize;

                    alignment = alignment < 0 ? -1 - absoluteAlignment : absoluteAlignment;
                    parameters.set(IN_FILE_ALIGNMENT, alignment);

                    //2. validate data dims
                    long[] dataDims = parameters.get(DATA_DIMS);
                    if (fileNames.length == 1 || numberOfDimensions == 1) {
                        long freeSpace = (sumFileSize - fileNames.length * absoluteAlignment) / typeWidth;
                        dataDims = cropDimsToSpace(freeSpace, dataDims, numberOfDimensions, 2);
                    } else {
                        long freeSpace = (minFileSize - absoluteAlignment) / typeWidth;
                        dataDims = cropDimsToSpace(freeSpace, dataDims, numberOfDimensions - 1, 2);
                        dataDims[numberOfDimensions - 1] = Math.min(dataDims[numberOfDimensions - 1], fileNames.length);
                    }
                    parameters.set(DATA_DIMS, dataDims);
                }
            }
        }

        parameters.set(FIELD_DIMS, parameters.get(DATA_DIMS));

        //validate parameters
        parameters.setParameterActive(true);
        return isValid;
    }

    /**
     * @return width of type in bytes.
     */
    private int getTypeWidth(NumberType type)
    {
        if (type.equals(NumberType.DOUBLE)) return 8;
        else if (type.equals(NumberType.INT) || type.equals(NumberType.FLOAT)) return 4;
        else if (type.equals(NumberType.SHORT)) return 2;
        else if (type.equals(NumberType.BYTE)) return 1;
        else throw new IllegalArgumentException("Type: " + type + " not supported!");
    }

    /**
     * Crops dimsTable to fit freeSpace. Only first numDims are considered.
     * Minimum dim size is set to minValueDim.
     *
     * @param freeSpace space to fit dimsTable[0] * dimsTable[1] * ... *
     *                  dimsTable[numDims]
     * @param dimsTable table with lengths of dimensions to crop
     * @param numDims   number of calculated/cropped dimensions (next dimensions
     *                  are just left unchanged)
     * @param minDim    minValueimum dimension length if cropped
     * <p>
     * @return table with cropped dims
     */
    private long[] cropDimsToSpace(long freeSpace, long[] dimsTable, int numDims, long minDim)
    {
        long[] dims = Arrays.copyOf(dimsTable, dimsTable.length);
        long dataSize = boxVolume(dimsTable, numDims);

        //crop 3rd dimension
        if (numDims == 3) {
            if (dataSize > freeSpace) {
                dims[2] = Math.max(minDim, freeSpace / dims[0] / dims[1]);
                dataSize = dims[0] * dims[1];
                freeSpace /= dims[2];
            }
        }

        // 3. crop 2nd dimension
        if (numDims >= 2) {
            if (dataSize > freeSpace) {
                dims[1] = Math.max(minDim, freeSpace / dims[0]);
                freeSpace /= dims[1];
                dataSize = dims[0];
            }
        }

        // 4. crop 1st dimension
        if (dataSize > freeSpace) {
            dims[0] = freeSpace;
        }

        return dims;
    }

    private long boxVolume(long[] boxDimensions, int numberOfDimensions)
    {
        long volume = 1;
        for (int i = 0; i < numberOfDimensions; i++) volume *= boxDimensions[i];
        return volume;
    }

    private void compute()
    {
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }
    
    @Override
    public void onInitFinishedLocal() {
        if (isForceFlag()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    computeUI.activateOpenDialog();
                }
            });
        }
    }    

    @Override
    public void onActive()
    {
        boolean isValid;
        Parameters p;
        synchronized (parameters) {
            //2. validate params             
            isValid = validateParamsAndSetSmart(false);
            //2b. clone param (local read-only copy)
            p = parameters.getReadOnlyClone();
        }

        //3. update gui (GUI doesn't change parameters !!!!!!!!!!!!! - assuming correct set of parameters)
        notifyGUIs(p, isFromVNA(), isFromVNA());

        //4. run computation and propagate
        if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
            runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data

            //10% for prepare output geometry
            ProgressAgent progressAgent = getProgressAgent(110);
            if (isValid) outRegularField = generateOutField(p, progressAgent);
            else outRegularField = null;

            outField = outRegularField;
            prepareOutputGeometry();
            show();
            progressAgent.setProgress(1.0);
            if (outRegularField == null) setOutputValue("outField", null);
            else setOutputValue("outField", new VNRegularField(outRegularField));
        }
    }
}
