/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.readRaw;

import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton;

public class ReadRawShared
{
    //Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    //not-null, may be empty
    static final ParameterName<String[]> FILE_NAMES = new ParameterName("File names");
    //1, 2 or 3
    static final ParameterName<Integer> NUMBER_OF_DIMENSIONS = new ParameterName("Number of dimensions");
    //length == 3, each element >= 2
    static final ParameterName<long[]> DATA_DIMS = new ParameterName("Data dims");
    //length == 3, each element >= 2
    static final ParameterName<long[]> FIELD_DIMS = new ParameterName("Field dims");
    //length == 3
    static final ParameterName<long[]> FIELD_OFFSET = new ParameterName("Field offset");
    //DESCRIPTION:
    //Alignment within single file. 
    //if n >= 0 then skip n bytes head and read data aligned to start of file
    //if n < 0 then skip -n-1 bytes tail and read data aligned to end of file
    static final ParameterName<Long> IN_FILE_ALIGNMENT = new ParameterName("In-file alignment");
    static final ParameterName<NumberType> NUMBER_TYPE = new ParameterName("Number type");
    static final ParameterName<Endianness> ENDIANNESS = new ParameterName("Endianness");

    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");
    
    
    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic)
    //"file_size_string" (>=0) or "file_size_string+" (>=0) 
    static final ParameterName<String> META_FILE_SIZE_INFO = new ParameterName("META File size or shortest file size");
}
