//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package org.visnow.vn.lib.basic.readers.ReadCDM4;

import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.apache.log4j.Logger;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import static org.visnow.vn.lib.basic.readers.ReadCDM4.ReadCDM4Shared.*;

/**
 * Reader for HDF5 and CDM4 files.
 * 
 * It uses two libraries:
 * 
 * NetCDF-Java library {@link http://www.unidata.ucar.edu/software/thredds/current/netcdf-java}
 * for reading files compatible with the Common Data Model version 4 (abbreviated CDM4 here).
 * 
 * JHDF5 {@link https://wiki-bsse.ethz.ch/display/JHDF5}
 * for reading VisNow fields stored in HDF5 files.
 * 
 *
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl), Warsaw University, ICM
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class ReadCDM4 extends OutFieldVisualizationModule
{

    private static final Logger LOGGER = Logger.getLogger(ReadCDM4.class);

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI computeUI;

    public ReadCDM4()
    {

        parameters.addParameterChangelistener((String name) -> {
            startAction();
        });
        SwingInstancer.swingRunAndWait(() -> {
            computeUI = new GUI();
            ui.addComputeGUI(computeUI);
            setPanel(ui);
            computeUI.setParameters(parameters);
        });
    }

    /**
     * Reads data from a file and returns a Field.
     *
     * @param params        parameters
     * @param progressAgent progress monitor
     *
     * @return a new Field or null if an error occurred  
     */
    public static Field createField(Parameters params, ProgressAgent progressAgent)
    {
        String filename = params.get(FILENAME);
        String visNowFieldName = params.get(VISNOW_FIELD_NAME);
        String[] visNowComponentNames = params.get(VISNOW_COMPONENT_NAMES);
        String[] variablesNames = params.get(VARIABLE_NAMES);
        Boolean firstDimensionIsTime = params.get(FIRST_DIMENSION_IS_TIME);
        Boolean lastDimensionIsVector = params.get(LAST_DIMENSION_IS_VECTOR);

        if (visNowFieldName.equals("")) {
            return ReadCDM4Core.createFieldFromVariables(filename, variablesNames, firstDimensionIsTime, lastDimensionIsVector, progressAgent);
        } else {
            return ReadHDF5Core.readVisNowField(filename, visNowFieldName, visNowComponentNames, progressAgent);
        }
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return ReadCDM4Shared.getDefaultParameters();
    }

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        ProgressAgent progressAgent = getProgressAgent(100);
        Parameters p;
        synchronized (parameters) {
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, false, false);

        progressAgent.setProgress(0.0f);

        if (p.get(FILENAME).isEmpty()) {
            outField = null;
            progressAgent.setProgress(1.0);
            show();
            setOutputValue("outCDM4Datasets", null);
            return;
        } else {
            outField = createField(p, progressAgent);
            if (outField != null) {
                setOutputValue("outCDM4Datasets", new VNRegularField((RegularField) outField));
            } else {
                setOutputValue("outCDM4Datasets", null);
            }
            progressAgent.setProgress(0.9);
            prepareOutputGeometry();
            progressAgent.setProgress(1.0);
            show();
        }
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag()) {
            computeUI.activateOpenDialog();
        }
    }
}
