/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.material_science.ReadVASP;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University
 * Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class AtomGeometryTemplates
{

    public static final VisualAtomTemplate[] stdAtomTemplates = {
        new VisualAtomTemplate(0, 1., "DF", "Default     ", 2.00, 1.00, 1.50, 0.31, 1.00, 0.00),
        new VisualAtomTemplate(1, 1.008, "H", "Hydrogen    ", 1.20, 1.00, 1.50, 1.00, 1.00, 1.00),
        new VisualAtomTemplate(2, 4.003, "He", "Helium      ", 1.40, 1.00, 1.50, 1.00, 1.00, 1.00),
        new VisualAtomTemplate(3, 6.940, "Li", "Lithium     ", 1.82, 0.90, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(4, 9.010, "Be", "Berylium    ", 2.00, 0.59, 1.50, 0.40, 1.00, 0.33),
        new VisualAtomTemplate(5, 10.81, "B", "Boron       ", 2.00, 0.25, 1.50, 0.36, 0.63, 1.00),
        new VisualAtomTemplate(6, 12.01, "C", "Carbon      ", 1.70, 0.29, 1.50, 0.50, 0.50, 0.50),
        new VisualAtomTemplate(7, 14.01, "N", "Nitrogen    ", 1.55, 0.32, 1.50, 0.00, 0.00, 1.00),
        new VisualAtomTemplate(8, 16.00, "O", "Oxygen      ", 1.52, 1.24, 1.50, 1.00, 0.00, 0.00),
        new VisualAtomTemplate(9, 19.00, "F", "Fluorin     ", 1.47, 1.19, 1.50, 0.70, 0.30, 0.90),
        new VisualAtomTemplate(10, 20.18, "Ne", "Neon        ", 1.54, 1.00, 1.50, 1.00, 1.00, 1.00),
        new VisualAtomTemplate(11, 22.99, "Na", "Sodium      ", 2.27, 1.16, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(12, 24.31, "Mg", "Magnesium   ", 1.73, 0.86, 1.50, 0.40, 1.00, 0.33),
        new VisualAtomTemplate(13, 26.98, "Al", "Aluminium   ", 2.00, 0.68, 1.50, 0.36, 0.63, 1.00),
        new VisualAtomTemplate(14, 28.09, "Si", "Slicon      ", 2.10, 0.54, 1.50, 0.00, 0.20, 1.00),
        new VisualAtomTemplate(15, 30.97, "P", "Phosphorus  ", 1.80, 0.58, 1.50, 0.00, 1.00, 0.00),
        new VisualAtomTemplate(16, 32.06, "S", "Sulfur      ", 1.80, 0.51, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(17, 35.45, "Cl", "Chlorine    ", 1.75, 1.67, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(18, 39.95, "Ar", "Argon       ", 1.88, 1.00, 1.50, 1.00, 1.00, 1.00),
        new VisualAtomTemplate(19, 39.10, "K", "Potassium   ", 2.75, 1.52, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(20, 40.08, "Ca", "Calcium     ", 2.00, 1.14, 1.50, 0.40, 1.00, 0.33),
        new VisualAtomTemplate(21, 44.96, "Sc", "Scandium    ", 2.00, 0.88, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(22, 47.90, "Ti", "Titanium    ", 2.00, 0.81, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(23, 50.94, "V", "Vanadium    ", 2.00, 0.80, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(24, 52.00, "Cr", "Chromium    ", 2.00, 0.46, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(25, 54.94, "Mn", "Manganese   ", 2.00, 0.81, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(26, 55.85, "Fe", "Iron        ", 2.00, 0.69, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(27, 58.93, "Co", "Cobalt      ", 2.00, 0.69, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(28, 58.71, "Ni", "Nickel      ", 1.63, 0.70, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(29, 63.55, "Cu", "Copper      ", 1.40, 0.87, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(30, 65.37, "Zn", "Zinc        ", 1.39, 0.88, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(31, 69.72, "Ga", "Gallium     ", 1.87, 0.76, 1.50, 0.36, 0.63, 1.00),
        new VisualAtomTemplate(32, 72.59, "Ge", "Germanium   ", 2.00, 0.67, 1.50, 0.00, 0.20, 1.00),
        new VisualAtomTemplate(33, 74.92, "As", "Arsenic     ", 1.85, 0.60, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(34, 78.96, "Se", "Selenium    ", 1.90, 0.64, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(35, 79.90, "Br", "Bromine     ", 1.85, 1.82, 1.50, 0.70, 0.30, 0.90),
        new VisualAtomTemplate(36, 83.80, "Kr", "Krypton     ", 2.02, 1.00, 1.50, 1.00, 1.00, 1.00),
        new VisualAtomTemplate(37, 85.47, "Rb", "Rubidium    ", 2.00, 1.66, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(38, 87.62, "Sr", "Strontium   ", 2.00, 1.32, 1.50, 0.40, 1.00, 0.33),
        new VisualAtomTemplate(39, 88.90, "Y", "Yttorium    ", 2.00, 1.04, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(40, 91.22, "Zr", "Zirconium   ", 2.00, 0.86, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(41, 92.90, "Nb", "Niobium     ", 2.00, 0.86, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(42, 95.94, "Mo", "Molydbenum  ", 2.00, 0.83, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(43, 97.00, "Tc", "Technetium  ", 2.00, 0.79, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(44, 101.07, "Ru", "Ruthenuim   ", 2.00, 0.81, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(45, 102.90, "Rh", "Rhodium     ", 2.00, 0.69, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(46, 106.40, "Pd", "Palladium   ", 1.63, 1.00, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(47, 107.86, "Ag", "Silver      ", 1.72, 1.27, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(48, 112.41, "Cd", "Cadmium     ", 1.58, 1.09, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(49, 114.82, "In", "Indium      ", 1.93, 0.94, 1.50, 0.36, 0.63, 1.00),
        new VisualAtomTemplate(50, 118.69, "Sn", "Tin         ", 2.17, 0.83, 1.50, 0.20, 0.20, 0.20),
        new VisualAtomTemplate(51, 121.75, "Sb", "Antimony    ", 2.10, 0.74, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(52, 127.60, "Te", "Tellurium   ", 2.06, 0.66, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(53, 126.90, "I", "Iodine      ", 1.98, 2.06, 1.50, 0.70, 0.30, 0.90),
        new VisualAtomTemplate(54, 131.30, "Xe", "Xenon       ", 2.16, 1.00, 1.50, 1.00, 1.00, 1.00),
        new VisualAtomTemplate(55, 132.90, "Cs", "Caesium     ", 2.00, 1.81, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(56, 137.33, "Ba", "Barium      ", 2.00, 1.49, 1.50, 0.40, 1.00, 0.33),
        new VisualAtomTemplate(57, 138.90, "La", "Lanthanum   ", 2.00, 1.17, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(58, 140.12, "Ce", "Cerium      ", 2.00, 1.15, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(59, 140.90, "Pr", "Praseodymium", 2.00, 1.00, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(60, 144.24, "Nd", "Neodymium   ", 2.00, 1.12, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(61, 145.00, "Pm", "Promethium  ", 2.00, 1.00, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(62, 150.40, "Sm", "Samarium    ", 2.00, 1.00, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(63, 151.96, "Eu", "Europium    ", 2.00, 1.31, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(64, 157.25, "Gd", "Gadolinium  ", 2.00, 1.08, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(65, 158.92, "Tb", "Terbium     ", 2.00, 1.00, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(66, 162.50, "Dy", "Dysprosium  ", 2.00, 1.05, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(67, 164.93, "Ho", "Holmium     ", 2.00, 1.00, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(68, 167.26, "Er", "Erbium      ", 2.00, 1.00, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(69, 168.93, "Tm", "Thulium     ", 2.00, 1.00, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(70, 173.04, "Yb", "Ytterbium   ", 2.00, 1.00, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(71, 174.97, "Lu", "Luteium     ", 2.00, 1.00, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(72, 178.49, "Hf", "Hfnium      ", 2.00, 0.85, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(73, 180.94, "Ta", "Tantalum    ", 2.00, 0.78, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(74, 183.85, "W", "Tungsten    ", 2.00, 0.80, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(75, 186.20, "Re", "Rhenium     ", 2.00, 0.69, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(76, 190.20, "Os", "Osmium      ", 2.00, 0.77, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(77, 192.22, "Ir", "Iridium     ", 2.00, 0.82, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(78, 195.09, "Pt", "Platinum    ", 1.72, 0.94, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(79, 196.96, "Au", "Gold        ", 1.66, 1.51, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(80, 200.59, "Hg", "Mercury     ", 1.55, 1.16, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(81, 204.37, "Tl", "Thallium    ", 1.96, 1.00, 1.50, 0.36, 0.63, 1.00),
        new VisualAtomTemplate(82, 207.20, "Pb", "Lead        ", 2.02, 1.33, 1.50, 0.20, 0.20, 0.20),
        new VisualAtomTemplate(83, 208.98, "Bi", "Bismuth     ", 2.00, 1.71, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(84, 209.00, "Po", "Polonium    ", 2.00, 1.00, 1.50, 0.00, 0.60, 0.00),
        new VisualAtomTemplate(85, 210.00, "At", "Astatine    ", 2.00, 1.00, 1.50, 0.70, 0.30, 0.90),
        new VisualAtomTemplate(86, 222.00, "Rn", "Radon       ", 2.00, 1.00, 1.50, 1.00, 1.00, 1.00),
        new VisualAtomTemplate(87, 223.00, "Fr", "Francium    ", 2.00, 1.00, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(88, 226.02, "Ra", "Radium      ", 2.00, 1.62, 1.50, 0.40, 1.00, 0.33),
        new VisualAtomTemplate(89, 227.02, "Ac", "Actinium    ", 2.00, 1.26, 1.50, 0.90, 0.90, 0.00),
        new VisualAtomTemplate(90, 232.03, "Th", "Thorium     ", 2.00, 1.08, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(91, 231.03, "Pa", "Protactinium", 2.00, 1.00, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(92, 238.02, "U", "Uranium     ", 1.86, 0.87, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(93, 237.04, "Np", "Neptunium   ", 2.00, 1.00, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(94, 244.00, "Pu", "Plutonium   ", 2.00, 1.14, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(95, 243.00, "Am", "Americium   ", 2.00, 0.68, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(96, 247.00, "Cm", "Curium      ", 2.00, 1.00, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(97, 247.00, "Bk", "Berkelium   ", 2.00, 1.00, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(98, 251.00, "Cf", "Californium ", 2.00, 1.00, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(99, 254.00, "Es", "Einsteinium ", 2.00, 1.00, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(100, 257.00, "Fm", "Fermium     ", 2.00, 1.00, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(101, 258.00, "Md", "Mendelevium ", 2.00, 1.00, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(102, 259.00, "No", "Nobelium    ", 2.00, 1.00, 1.50, 0.70, 0.30, 0.00),
        new VisualAtomTemplate(103, 260.00, "Lr", "Lawrencium  ", 2.00, 1.00, 1.50, 0.70, 0.30, 0.00)
    };
    public static VisualAtomTemplate[] atomTemplates = stdAtomTemplates;


    /**
     * Creates a new instance of AtomGeometryTemplates
     */
    public AtomGeometryTemplates()
    {
    }
    
    public static VisualAtomTemplate getTemplate(String symbol)
    {
        for (int i = 0; i < atomTemplates.length; i++) 
            if (atomTemplates[i].getSymbol().equals(symbol))
                return atomTemplates[i];
        return atomTemplates[0];
    }
    
    public static int getTemplateIndex(String symbol)
    {
        for (int i = 0; i < atomTemplates.length; i++) 
            if (atomTemplates[i].getSymbol().equals(symbol))
                return i;
        return 0;
    }

    public void updateAtomTemplates(String name)
    {
        String line = "";
        int i, n;
        boolean found = false;
        VisualAtomTemplate t = null;
        try {
            BufferedReader r = new BufferedReader(
                new InputStreamReader(
                    new FileInputStream(name)));
            n = atomTemplates.length;
            while ((line = r.readLine()) != null) {
                String[] tokens = line.split(" +");
                found = false;
                for (i = 0; i < n; i++)
                    if (tokens[0].equals(atomTemplates[i].getSymbol())) {
                        found = true;
                        break;
                    }
                if (!found)
                    n += 1;
            }
            r.close();
            VisualAtomTemplate[] newAtomTemplates = new VisualAtomTemplate[n];
            for (i = 0; i < atomTemplates.length; i++)
                newAtomTemplates[i] = atomTemplates[i];
            r = new BufferedReader(
                new InputStreamReader(
                    new FileInputStream(name)));
            n = atomTemplates.length;
            while ((line = r.readLine()) != null) {
                String[] tokens = line.split(" +");
                found = false;
                for (i = 0; i < n; i++)
                    if (tokens[0].equals(atomTemplates[i].getSymbol())) {
                        found = true;
                        t = newAtomTemplates[i];
                        t.setMass(Float.parseFloat(tokens[2]));
                        t.setVdWRadius(Float.parseFloat(tokens[4]));
                        t.setIonRadius(Float.parseFloat(tokens[5]));
                        t.setUserRadius(Float.parseFloat(tokens[6]));
                        t.setColor(Float.parseFloat(tokens[7]),
                                   Float.parseFloat(tokens[8]),
                                   Float.parseFloat(tokens[9]));
                        break;
                    }
                if (!found) {
                    t = new VisualAtomTemplate(Integer.parseInt(tokens[1]),
                                               Float.parseFloat(tokens[2]),
                                               tokens[0], tokens[3],
                                               Float.parseFloat(tokens[4]),
                                               Float.parseFloat(tokens[5]),
                                               Float.parseFloat(tokens[6]),
                                               Float.parseFloat(tokens[7]),
                                               Float.parseFloat(tokens[8]),
                                               Float.parseFloat(tokens[9]));
                    newAtomTemplates[n] = t;
                    n += 1;
                }
            }
            atomTemplates = newAtomTemplates;
            r.close();
        } catch (Exception e) {
            System.out.println("Bad template file " + line + "  " + e);
            return;
        }
    }
}
