/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.material_science.ReadVASP;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.swingwrappers.TextField;
import static org.visnow.vn.lib.basic.readers.material_science.ReadVASP.ReadVASPShared.*;
import org.visnow.vn.lib.utils.io.InputSource;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class GUI extends JPanel
{

    private JFileChooser fileChooser = new JFileChooser();
    private FileNameExtensionFilter dataFilter;
    private Parameters parameters;
    private String lastPath = null;
    protected String fileName = null;
    //   private Browser browser = new Browser();
//    private GridFrame gridFrame = new GridFrame();

    /**
     * Creates new form GUI
     */
    public GUI()
    {
        initComponents();
        fileChooser.setLocation(0, 0);

        //      browser.setVisible(false);
        //      browser.addChangeListener(new ChangeListener()
        //      {
        //         @Override
        //         public void stateChanged(ChangeEvent e)
        //         {
        //            params.setSource(InputSource.URL);
        //            params.setFileName(browser.getCurrentURL());
        //            fileNameField.setText(params.getFileName());
        //            fireStateChanged();
        //         }
        //      });
//        gridFrame.setVisible(false);
//        gridFrame.setSingleFile(true);
//        gridFrame.addChangeListener(new ChangeListener()
//        {
//            @Override
//            public void stateChanged(ChangeEvent e)
//            {
//                params.setSource(InputSource.GRID);
//                params.setFileName(VisNow.getTmpDirPath() + File.separator + gridFrame.getTransferredFileNames()[0]);
//                fileNameField.setText(params.getFileName());
//                fireStateChanged();
//            }
//        });
    }

    public GUI(String title, String dataFileDesc, String ext0, String ext1)
    {
        initComponents();
//        moduleLabel.setText(title);
        dataFilter = new FileNameExtensionFilter(dataFileDesc, ext0, ext1);
        fileChooser.setFileFilter(dataFilter);
        //      browser.setVisible(false);
        //      browser.addChangeListener(new ChangeListener()
        //      {
        //         public void stateChanged(ChangeEvent e)
        //         {
        //            params.setSource(InputSource.URL);
        //            params.setFileName(browser.getCurrentURL());
        //            fileNameField.setText(params.getFileName());
        //            fireStateChanged();
        //         }
        //      });
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT
     * modify this code. The content of this method is always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        sourceGroup = new javax.swing.ButtonGroup();
        geometryGroup = new javax.swing.ButtonGroup();
        browsePanel = new javax.swing.JPanel();
        filePathTF = new org.visnow.vn.gui.swingwrappers.TextField();
        browseButton = new javax.swing.JButton();
        additionalPanel = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        xSpin = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        ySpin = new javax.swing.JSpinner();
        jLabel1 = new javax.swing.JLabel();
        zSpin = new javax.swing.JSpinner();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        setBorder(javax.swing.BorderFactory.createEmptyBorder(4, 4, 4, 4));
        setLayout(new java.awt.GridBagLayout());

        browsePanel.setLayout(new java.awt.GridBagLayout());

        filePathTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                filePathTFUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                filePathTFUserAction(evt);
            }
        });
        filePathTF.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusGained(java.awt.event.FocusEvent evt)
            {
                filePathTFselectAllOnFocus(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        browsePanel.add(filePathTF, gridBagConstraints);

        browseButton.setText("Browse...");
        browseButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                browseButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        browsePanel.add(browseButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(browsePanel, gridBagConstraints);

        additionalPanel.setLayout(new java.awt.GridLayout(4, 3));

        jLabel4.setText("Axis:");
        additionalPanel.add(jLabel4);

        jLabel5.setText("Multiply by:");
        additionalPanel.add(jLabel5);

        jLabel2.setText("X:");
        additionalPanel.add(jLabel2);

        xSpin.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(1), Integer.valueOf(1), null, Integer.valueOf(1)));
        additionalPanel.add(xSpin);

        jLabel3.setText("Y:");
        additionalPanel.add(jLabel3);

        ySpin.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(1), Integer.valueOf(1), null, Integer.valueOf(1)));
        additionalPanel.add(ySpin);

        jLabel1.setText("Z:");
        additionalPanel.add(jLabel1);

        zSpin.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(1), Integer.valueOf(1), null, Integer.valueOf(1)));
        additionalPanel.add(zSpin);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(8, 0, 0, 0);
        add(additionalPanel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weighty = 1.0;
        add(filler1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    public void activateOpenDialog()
    {
        browseButtonActionPerformed(null);
    }

    private void filePathTFUserAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_filePathTFUserAction
    {//GEN-HEADEREND:event_filePathTFUserAction
        if (evt.getEventType() == TextField.EVENT_CHANGE_VALUE || evt.getEventType() == TextField.EVENT_NO_CHANGE_ENTER_KEY) {
            parameters.set(TYPE, InputSource.FILE, FILENAME, filePathTF.getText(), MULT, new int[]{(Integer) xSpin.getValue(),
                                         (Integer) ySpin.getValue(),
                                         (Integer) zSpin.getValue()});
        }
    }//GEN-LAST:event_filePathTFUserAction

    private void filePathTFUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_filePathTFUserChangeAction
    {//GEN-HEADEREND:event_filePathTFUserChangeAction

    }//GEN-LAST:event_filePathTFUserChangeAction

    private void filePathTFselectAllOnFocus(java.awt.event.FocusEvent evt)//GEN-FIRST:event_filePathTFselectAllOnFocus
    {//GEN-HEADEREND:event_filePathTFselectAllOnFocus
        ((TextField) evt.getSource()).selectAll();
    }//GEN-LAST:event_filePathTFselectAllOnFocus

    private void browseButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_browseButtonActionPerformed
    {//GEN-HEADEREND:event_browseButtonActionPerformed
        fileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(this.getClass())));

        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            String fileName = fileChooser.getSelectedFile().getAbsolutePath();
            VisNow.get().getMainConfig().setLastDataPath(fileName.substring(0, fileName.lastIndexOf(File.separator)), this.getClass());
            parameters.set(FILENAME, fileName, MULT, new int[]{(Integer) xSpin.getValue(),
                                         (Integer) ySpin.getValue(),
                                         (Integer) zSpin.getValue()});
        }
    }//GEN-LAST:event_browseButtonActionPerformed


    /**
     * @param params the params to set
     */
    public void setParameters(Parameters parameters)
    {
        this.parameters = parameters;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JPanel additionalPanel;
    private javax.swing.JButton browseButton;
    private javax.swing.JPanel browsePanel;
    private org.visnow.vn.gui.swingwrappers.TextField filePathTF;
    private javax.swing.Box.Filler filler1;
    private javax.swing.ButtonGroup geometryGroup;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.ButtonGroup sourceGroup;
    private javax.swing.JSpinner xSpin;
    private javax.swing.JSpinner ySpin;
    private javax.swing.JSpinner zSpin;
    // End of variables declaration//GEN-END:variables

    void updateGUI(ParameterProxy p) {
        filePathTF.setText(p.get(FILENAME));
        int[] mult = p.get(MULT);
        xSpin.setValue((Integer) mult[0]);
        ySpin.setValue((Integer) mult[1]);
        zSpin.setValue((Integer) mult[2]);
    }
}
