/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.VolumeReader;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.readers.VolumeReader.VolumeReaderShared.*;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class GUI extends JPanel
{

    private final JFileChooser dataFileChooser = new JFileChooser();
    private final FileNameExtensionFilter dataFilter;
    private String lastPath = null;
    protected String fileName = null;
    protected String[] extensions = new String[]{"dat", "DAT", "vol", "VOL"};
    private Parameters parameters;
    /**
     * Creates new form GUI
     */
    public GUI()
    {
        initComponents();
        dataFileChooser.setLocation(0, 0);
        dataFilter = new FileNameExtensionFilter("volume data file",
                                                 extensions[0], extensions[1], extensions[2], extensions[3],
                                                 "dat_gz", "DAT_GZ", "vol_gz", "VOL_GZ");
        dataFileChooser.setFileFilter(dataFilter);
    }
    
    void setParameters(Parameters parameters)
    {
        this.parameters = parameters;
    }
    
    protected void updateGUI(final ParameterProxy p)
    {
        fileNameField2.setText(p.get(FILENAME));
        xScaleField.setText(String.format("%5f", p.get(SCALE)[0]));
        xOrigField.setText(String.format("%5f", p.get(ORIG)[0]));
        yScaleField.setText(String.format("%5f", p.get(SCALE)[1]));
        yOrigField.setText(String.format("%5f", p.get(ORIG)[1]));
        zScaleField.setText(String.format("%5f", p.get(SCALE)[2]));
        zOrigField.setText(String.format("%5f", p.get(ORIG)[2]));
        xMinField.setText(String.format("%5f", p.get(MIN)[0])); 
        xMaxField.setText(String.format("%5f", p.get(MAX)[0])); 
        yMinField.setText(String.format("%5f", p.get(MIN)[1])); 
        yMaxField.setText(String.format("%5f", p.get(MAX)[1]));
        zMinField.setText(String.format("%5f", p.get(MIN)[2])); 
        zMaxField.setText(String.format("%5f", p.get(MAX)[2]));
        if(p.get(TYPE) == VolumeReaderShared.FROM_FILE) {
            extFromFile.setSelected(true);
        }
        else if(p.get(TYPE) == VolumeReaderShared.FROM_INDICES) {
            extFromIndices.setSelected(true);
        }
        else if(p.get(TYPE) == VolumeReaderShared.NORMALIZED) {
            extNormalize.setSelected(true);
        }
        else if(p.get(TYPE) == VolumeReaderShared.USER_EXTENTS) {
            extUser.setSelected(true);
            jTabbedPane1.setSelectedIndex(0);
        }
        else {
            extUser.setSelected(true);
            jTabbedPane1.setSelectedIndex(1);
        }
    }


    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT
     * modify this code. The content of this method is always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        sourceGroup = new javax.swing.ButtonGroup();
        geometryGroup = new javax.swing.ButtonGroup();
        jLabel14 = new javax.swing.JLabel();
        selectButton = new javax.swing.JButton();
        fileNameField2 = new org.visnow.vn.gui.swingwrappers.TextField();
        jLabel7 = new javax.swing.JLabel();
        extFromIndices = new javax.swing.JRadioButton();
        extNormalize = new javax.swing.JRadioButton();
        extUser = new javax.swing.JRadioButton();
        extFromFile = new javax.swing.JRadioButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        additionalPanel1 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        xMinField = new org.visnow.vn.gui.components.FloatFormattedTextField();
        xMaxField = new org.visnow.vn.gui.components.FloatFormattedTextField();
        jLabel12 = new javax.swing.JLabel();
        yMinField = new org.visnow.vn.gui.components.FloatFormattedTextField();
        yMaxField = new org.visnow.vn.gui.components.FloatFormattedTextField();
        jLabel13 = new javax.swing.JLabel();
        zMinField = new org.visnow.vn.gui.components.FloatFormattedTextField();
        zMaxField = new org.visnow.vn.gui.components.FloatFormattedTextField();
        additionalPanel = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        xOrigField = new javax.swing.JTextField();
        xScaleField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        yOrigField = new javax.swing.JTextField();
        yScaleField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        zOrigField = new javax.swing.JTextField();
        zScaleField = new javax.swing.JTextField();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 32767));

        setBorder(javax.swing.BorderFactory.createEmptyBorder(4, 4, 4, 4));
        setLayout(new java.awt.GridBagLayout());

        jLabel14.setText("Read volume file");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        add(jLabel14, gridBagConstraints);

        selectButton.setText("browse");
        selectButton.setMaximumSize(new java.awt.Dimension(90, 20));
        selectButton.setMinimumSize(new java.awt.Dimension(90, 20));
        selectButton.setPreferredSize(new java.awt.Dimension(90, 20));
        selectButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                selectButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        add(selectButton, gridBagConstraints);

        fileNameField2.setToolTipText("");
        fileNameField2.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                fileNameField2UserChangeAction(evt);
            }
        });
        fileNameField2.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusGained(java.awt.event.FocusEvent evt)
            {
                fileNameField2FocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(fileNameField2, gridBagConstraints);

        jLabel7.setText("Geometric dimensions");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(jLabel7, gridBagConstraints);

        geometryGroup.add(extFromIndices);
        extFromIndices.setText("from indices");
        extFromIndices.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                extFromIndicesActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(extFromIndices, gridBagConstraints);

        geometryGroup.add(extNormalize);
        extNormalize.setText("normalize to fit in unit box");
        extNormalize.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                extNormalizeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(extNormalize, gridBagConstraints);

        geometryGroup.add(extUser);
        extUser.setText("user");
        extUser.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                extUserActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(extUser, gridBagConstraints);

        geometryGroup.add(extFromFile);
        extFromFile.setSelected(true);
        extFromFile.setText("from file");
        extFromFile.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                extFromFileActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(extFromFile, gridBagConstraints);

        jTabbedPane1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N

        additionalPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        additionalPanel1.setMinimumSize(new java.awt.Dimension(180, 80));
        additionalPanel1.setPreferredSize(new java.awt.Dimension(200, 81));
        additionalPanel1.setLayout(new java.awt.GridLayout(4, 3));

        jLabel8.setText("axis");
        additionalPanel1.add(jLabel8);

        jLabel9.setText("min");
        additionalPanel1.add(jLabel9);

        jLabel10.setText("max");
        additionalPanel1.add(jLabel10);

        jLabel11.setText("x");
        additionalPanel1.add(jLabel11);

        xMinField.setText("-1");
        additionalPanel1.add(xMinField);
        additionalPanel1.add(xMaxField);

        jLabel12.setText("y");
        additionalPanel1.add(jLabel12);

        yMinField.setText("-1");
        additionalPanel1.add(yMinField);
        additionalPanel1.add(yMaxField);

        jLabel13.setText("z");
        additionalPanel1.add(jLabel13);

        zMinField.setText("-1");
        additionalPanel1.add(zMinField);
        additionalPanel1.add(zMaxField);

        jTabbedPane1.addTab("extents", additionalPanel1);

        additionalPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        additionalPanel.setMinimumSize(new java.awt.Dimension(180, 80));
        additionalPanel.setPreferredSize(new java.awt.Dimension(200, 81));
        additionalPanel.setLayout(new java.awt.GridLayout(4, 3));

        jLabel4.setText("axis");
        additionalPanel.add(jLabel4);

        jLabel5.setText("origin");
        additionalPanel.add(jLabel5);

        jLabel6.setText("cell dim");
        additionalPanel.add(jLabel6);

        jLabel2.setText("x");
        additionalPanel.add(jLabel2);

        xOrigField.setText("0");
        additionalPanel.add(xOrigField);

        xScaleField.setText("1");
        additionalPanel.add(xScaleField);

        jLabel3.setText("y");
        additionalPanel.add(jLabel3);

        yOrigField.setText("0");
        additionalPanel.add(yOrigField);

        yScaleField.setText("1");
        additionalPanel.add(yScaleField);

        jLabel1.setText("z");
        additionalPanel.add(jLabel1);

        zOrigField.setText("0");
        additionalPanel.add(zOrigField);

        zScaleField.setText("1");
        additionalPanel.add(zScaleField);

        jTabbedPane1.addTab("origin/cell size", additionalPanel);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(jTabbedPane1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weighty = 1.0;
        add(filler1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    public void activateOpenDialog()
    {
        selectButtonActionPerformed(null);
    }

    private void selectButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_selectButtonActionPerformed
    {//GEN-HEADEREND:event_selectButtonActionPerformed
            parameters.setParameterActive(false);
            if (lastPath == null)
                dataFileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(VolumeReader.class)));
            else
                dataFileChooser.setCurrentDirectory(new File(lastPath));

            int returnVal = dataFileChooser.showOpenDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                fileName = dataFileChooser.getSelectedFile().getAbsolutePath();
                parameters.set(FILENAME, fileName);
                lastPath = fileName.substring(0, fileName.lastIndexOf(File.separator));
                VisNow.get().getMainConfig().setLastDataPath(lastPath, VolumeReader.class);
            }
            fileNameField2.setText(parameters.get(FILENAME));
            parameters.setParameterActive(true);
            updateParameters();
    }//GEN-LAST:event_selectButtonActionPerformed

    private void fileNameField2UserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_fileNameField2UserChangeAction
    {//GEN-HEADEREND:event_fileNameField2UserChangeAction
        String filename = fileNameField2.getText();
        parameters.set(FILENAME, filename);

        if (fileNameField2.getText() != null && fileNameField2.getText().length() >= 1) {
            parameters.set(FILENAME, fileNameField2.getText());
        } else {
            parameters.set(FILENAME, "");
        }
    }//GEN-LAST:event_fileNameField2UserChangeAction

    private void fileNameField2FocusGained(java.awt.event.FocusEvent evt)//GEN-FIRST:event_fileNameField2FocusGained
    {//GEN-HEADEREND:event_fileNameField2FocusGained
        fileNameField2.selectAll();
    }//GEN-LAST:event_fileNameField2FocusGained

    private void extFromIndicesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_extFromIndicesActionPerformed
    {//GEN-HEADEREND:event_extFromIndicesActionPerformed
        updateParameters();
    }//GEN-LAST:event_extFromIndicesActionPerformed

    private void extNormalizeActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_extNormalizeActionPerformed
    {//GEN-HEADEREND:event_extNormalizeActionPerformed
        updateParameters();
    }//GEN-LAST:event_extNormalizeActionPerformed

    private void extUserActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_extUserActionPerformed
    {//GEN-HEADEREND:event_extUserActionPerformed
        updateParameters();
    }//GEN-LAST:event_extUserActionPerformed

    private void extFromFileActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_extFromFileActionPerformed
    {//GEN-HEADEREND:event_extFromFileActionPerformed
        updateParameters();
    }//GEN-LAST:event_extFromFileActionPerformed

    protected void updateParameters()
    {
        parameters.setParameterActive(false);
        float x = parameters.get(SCALE)[0];
        float y = parameters.get(SCALE)[1];
        float z = parameters.get(SCALE)[2];
        float x0 = parameters.get(ORIG)[0];
        float y0 = parameters.get(ORIG)[1];
        float z0 = parameters.get(ORIG)[2];
        try {
            x = Float.parseFloat(xScaleField.getText());
            x0 = Float.parseFloat(xOrigField.getText());
        } catch (NumberFormatException e) {
            xScaleField.setText(String.format("%5f", parameters.get(SCALE)[0]));
            xOrigField.setText(String.format("%5f", parameters.get(ORIG)[0]));
        }
        try {
            y = Float.parseFloat(yScaleField.getText());
            y0 = Float.parseFloat(yOrigField.getText());
        } catch (NumberFormatException e) {
            yScaleField.setText(String.format("%5f", parameters.get(SCALE)[1]));
            yOrigField.setText(String.format("%5f", parameters.get(ORIG)[1]));
        }
        try {
            z = Float.parseFloat(zScaleField.getText());
            z0 = Float.parseFloat(zOrigField.getText());
        } catch (NumberFormatException e) {
            zScaleField.setText(String.format("%5f", parameters.get(SCALE)[2]));
            zOrigField.setText(String.format("%5f", parameters.get(ORIG)[2]));
        }
        parameters.set(SCALE, new float[] {x, y, z});
        parameters.set(ORIG, new float[] {x0, y0, z0});
        parameters.set(MIN, new float[] {xMinField.getValue(), yMinField.getValue(), zMinField.getValue()});
        parameters.set(MAX, new float[] {xMaxField.getValue(), yMaxField.getValue(), zMaxField.getValue()});
        if (extFromFile.isSelected())
            parameters.set(TYPE, VolumeReaderShared.FROM_FILE);
        else if (extFromIndices.isSelected())
            parameters.set(TYPE, VolumeReaderShared.FROM_INDICES);
        else if (extNormalize.isSelected())
            parameters.set(TYPE, VolumeReaderShared.NORMALIZED);
        else if (extUser.isSelected() && jTabbedPane1.getSelectedIndex() == 0)
            parameters.set(TYPE, VolumeReaderShared.USER_EXTENTS);
        else if (extUser.isSelected() && jTabbedPane1.getSelectedIndex() == 1)
            parameters.set(TYPE, VolumeReaderShared.USER_AFFINE);
        parameters.setParameterActive(true);
        parameters.fireParameterChanged(null);
    }

    public void setAVSCompatible(boolean c)
    {
        extFromFile.setEnabled(!c);
        if (c)
            extNormalize.setSelected(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JPanel additionalPanel;
    protected javax.swing.JPanel additionalPanel1;
    private javax.swing.JRadioButton extFromFile;
    private javax.swing.JRadioButton extFromIndices;
    private javax.swing.JRadioButton extNormalize;
    private javax.swing.JRadioButton extUser;
    private org.visnow.vn.gui.swingwrappers.TextField fileNameField2;
    private javax.swing.Box.Filler filler1;
    private javax.swing.ButtonGroup geometryGroup;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTabbedPane jTabbedPane1;
    protected javax.swing.JButton selectButton;
    private javax.swing.ButtonGroup sourceGroup;
    private org.visnow.vn.gui.components.FloatFormattedTextField xMaxField;
    private org.visnow.vn.gui.components.FloatFormattedTextField xMinField;
    private javax.swing.JTextField xOrigField;
    private javax.swing.JTextField xScaleField;
    private org.visnow.vn.gui.components.FloatFormattedTextField yMaxField;
    private org.visnow.vn.gui.components.FloatFormattedTextField yMinField;
    private javax.swing.JTextField yOrigField;
    private javax.swing.JTextField yScaleField;
    private org.visnow.vn.gui.components.FloatFormattedTextField zMaxField;
    private org.visnow.vn.gui.components.FloatFormattedTextField zMinField;
    private javax.swing.JTextField zOrigField;
    private javax.swing.JTextField zScaleField;
    // End of variables declaration//GEN-END:variables
}
