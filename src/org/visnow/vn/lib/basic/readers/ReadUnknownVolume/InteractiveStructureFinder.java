/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.ReadUnknownVolume;

import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ComponentColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Hashtable;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class InteractiveStructureFinder extends javax.swing.JPanel {

    private Hashtable<Integer, JLabel> labelsTable = new Hashtable<>();
    private JFileChooser dataFileChooser = new JFileChooser();
    private String fileName = null;
    private float[] scale = new float[3];
    private int size = -1;
    private float minV, maxV;
    private int nx = 256, ny = 256, nz = 2, skip = 0, tailLen = 0, sliceSkip = 0;
    private BufferedImage im0 = null;
    private BufferedImage im1 = null;
    private BufferedImage im2 = null;
    private DataArrayType dataType = DataArrayType.FIELD_DATA_BYTE;
    private byte[] bData = null;
    private short[] sData = null;
    private int[] iData = null;
    private float[] fData = null;
    private String lastPath = null;
    private boolean inVisNow = true;

    public void setInVisNow(boolean inVisNow)
    {
        this.inVisNow = inVisNow;
    }

    public void setLastPath(String lastPath)
    {
        this.lastPath = lastPath;
    }

    public DataArrayType getDataType() {
        return dataType;
    }

    public byte[] getBData() {
        return bData;
    }

    public int[] getIData() {
        return iData;
    }

    public float[] getFData() {
        return fData;
    }

    public short[] getSData() {
        return sData;
    }

    public int getNz() {
        return nz;
    }

    public int getSkip() {
        return skip;
    }

    public int getSliceSkip() {
        return sliceSkip;
    }

    public String getFileName() {
        return fileName;
    }

    public float[] getScale() {
        return scale;
    }

    public int getNx() {
        return nx;
    }

    public int getNy() {
        return ny;
    }

    private JPanel map0 = new JPanel() {

        public void paint(Graphics g) {
            Graphics2D gr = (Graphics2D) g;
            gr.setColor(Color.DARK_GRAY);
            gr.fillRect(0, 0, getWidth(), getHeight());
            if (im0 != null) {
                gr.drawImage(im0, 0, 0, getWidth(), getHeight(), null);
            }

        }
    };
    private JPanel map1 = new JPanel() {

        public void paint(Graphics g) {
            Graphics2D gr = (Graphics2D) g;
            gr.setColor(Color.DARK_GRAY);
            gr.fillRect(0, 0, getWidth(), getHeight());
            if (im1 != null) {
                gr.drawImage(im1, 0, 0, getWidth(), getHeight(), null);
            }

        }
    };
    private JPanel map2 = new JPanel() {

        public void paint(Graphics g) {
            Graphics2D gr = (Graphics2D) g;
            gr.setColor(Color.DARK_GRAY);
            gr.fillRect(0, 0, getWidth(), getHeight());
            if (im2 != null) {
                gr.drawImage(im2, 0, 0, getWidth(), getHeight(), null);
            }

        }
    };

    /**
     * Creates new form VolumeReaderUI
     */
    public InteractiveStructureFinder() {
        initComponents();
        yRoller.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                skipPixelsSlider.setMaximum(xRoller.getValue());
                refresh();
            }
        });
        yRoller.setLabel("y size");
        yRoller.setValue(256);
        yRoller.setSensitivity(2);
        xRoller.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                skipLinesSlider.setMaximum(xRoller.getValue());
                refresh();
            }
        });
        xRoller.setLabel("x size");
        xRoller.setValue(256);
        xRoller.setSensitivity(2);
        zRoller.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (size <= 0) 
                    return;
                nz = zRoller.getValue();
                im0 = makeXYImage(skip);
                map0.repaint();
                im1 = makeXZImage(skip);
                map1.repaint();
                im2 = makeYZImage(skip);
                map2.repaint();
            }
        });
        zRoller.setLabel("z size");
        zRoller.setValue(100);
        zRoller.setSensitivity(2);
        datamapRangeSlider.setParams(0, 256, 0, 256);
        leftSlicePanel.add(map0, BorderLayout.CENTER);
        centerSlicePanel.add(map1, BorderLayout.CENTER);
        rightSlicePanel.add(map2, BorderLayout.CENTER);
        dataGroup.add(byteButton);
        dataGroup.add(shortButton);
        dataGroup.add(intButton);
    }

    private void refresh() {
        if (size <= 0) {
            return;
        }
        sliceSkip = sliceSkipSlider.getValue();
        ny = yRoller.getValue();
        nx = xRoller.getValue();
        nz = ((int) size - tailLen) / (nx * ny + sliceSkip);
        skip = nx * skipLinesSlider.getValue() + skipPixelsSlider.getValue();
        zRoller.setValue(nz);
        im0 = makeXYImage(skip);
        map0.repaint();
        im1 = makeXZImage(skip);
        map1.repaint();
        im2 = makeYZImage(skip);
        map2.repaint();
    }

    BufferedImage makeXYImage(int st) {
        int[] rgba = new int[4];
        ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_sRGB);
        int[] nBits
                = {
                    8, 8, 8, 8
                };
        ComponentColorModel colorModel = new ComponentColorModel(cs, nBits,
                true, true, Transparency.TRANSLUCENT, 0);
        WritableRaster raster
                = colorModel.createCompatibleWritableRaster(nx, ny);
        BufferedImage bImage
                = new BufferedImage(colorModel, raster, false, null);
        int sz = nz / 2;
        switch (dataType) {
            case FIELD_DATA_BYTE:
                for (int i1 = 0, j = st + sz * nx * ny; i1 < ny; i1++) {
                    for (int i0 = 0; i0 < nx; i0++, j++) {
                        if (j >= bData.length) {
                            break;
                        }
                        int c = bData[j];
                        for (int k = 0; k < 3; k++) {
                            rgba[k] = c;
                        }
                        rgba[3] = 0xFF;
                        raster.setPixel(i0, i1, rgba);
                    }
                }
                break;
            case FIELD_DATA_SHORT:
                for (int i1 = 0, j = st + sz * nx * ny; i1 < ny; i1++) {
                    for (int i0 = 0; i0 < nx; i0++, j++) {
                        if (j >= sData.length) {
                            break;
                        }
                        int c = (int) (255 * (sData[j] - minV) / (maxV - minV));
                        for (int k = 0; k < 3; k++) {
                            rgba[k] = c;
                        }
                        rgba[3] = 0xFF;
                        raster.setPixel(i0, i1, rgba);
                    }
                }
                break;
            case FIELD_DATA_INT:
                for (int i1 = 0, j = st + sz * nx * ny; i1 < ny; i1++) {
                    for (int i0 = 0; i0 < nx; i0++, j++) {
                        if (j >= iData.length) {
                            break;
                        }
                        int c = (int) (255 * (iData[j] - minV) / (maxV - minV));
                        for (int k = 0; k < 3; k++) {
                            rgba[k] = c;
                        }
                        rgba[3] = 0xFF;
                        raster.setPixel(i0, i1, rgba);
                    }
                }
                break;
            case FIELD_DATA_FLOAT:
                for (int i1 = 0, j = st + sz * nx * ny; i1 < ny; i1++) {
                    for (int i0 = 0; i0 < nx; i0++, j++) {
                        if (j >= fData.length) {
                            break;
                        }
                        int c = (int) (255.0f * (fData[j] - minV) / (maxV - minV));
                        for (int k = 0; k < 3; k++) {
                            rgba[k] = c;
                        }
                        rgba[3] = 0xFF;
                        raster.setPixel(i0, i1, rgba);
                    }
                }
                break;
        }
        return bImage;
    }

    BufferedImage makeXZImage(int st) {
        int[] rgba = new int[4];
        ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_sRGB);
        int[] nBits
                = {
                    8, 8, 8, 8
                };
        ComponentColorModel colorModel = new ComponentColorModel(cs, nBits,
                true, true, Transparency.TRANSLUCENT, 0);
        WritableRaster raster
                = colorModel.createCompatibleWritableRaster(nx, nz);
        BufferedImage bImage
                = new BufferedImage(colorModel, raster, false, null);
        int sy = ny / 2;
        switch (dataType) {
            case FIELD_DATA_BYTE:
                for (int i1 = 0; i1 < nz; i1++) {
                    for (int i0 = 0, j = st + sy * nx + i1 * (nx * ny + sliceSkip); i0 < nx; i0++, j++) {
                        if (j >= bData.length) {
                            break;
                        }
                        int c = bData[j];
                        for (int k = 0; k < 3; k++) {
                            rgba[k] = c;
                        }
                        rgba[3] = 0xFF;
                        raster.setPixel(i0, i1, rgba);
                    }
                }
                break;
            case FIELD_DATA_SHORT:
                for (int i1 = 0; i1 < nz; i1++) {
                    for (int i0 = 0, j = st + sy * nx + i1 * (nx * ny + sliceSkip); i0 < nx; i0++, j++) {
                        if (j >= sData.length) {
                            break;
                        }
                        int c = (int) (255 * (sData[j] - minV) / (maxV - minV));
                        for (int k = 0; k < 3; k++) {
                            rgba[k] = c;
                        }
                        rgba[3] = 0xFF;
                        raster.setPixel(i0, i1, rgba);
                    }
                }
                break;
            case FIELD_DATA_INT:
                for (int i1 = 0; i1 < nz; i1++) {
                    for (int i0 = 0, j = st + sy * nx + i1 * (nx * ny + sliceSkip); i0 < nx; i0++, j++) {
                        if (j >= iData.length) {
                            break;
                        }
                        int c = (int) (255 * (iData[j] - minV) / (maxV - minV));
                        for (int k = 0; k < 3; k++) {
                            rgba[k] = c;
                        }
                        rgba[3] = 0xFF;
                        raster.setPixel(i0, i1, rgba);
                    }
                }
                break;
            case FIELD_DATA_FLOAT:
                for (int i1 = 0; i1 < nz; i1++) {
                    for (int i0 = 0, j = st + sy * nx + i1 * (nx * ny + sliceSkip); i0 < nx; i0++, j++) {
                        if (j >= fData.length) {
                            break;
                        }
                        int c = (int) (255.0f * (fData[j] - minV) / (maxV - minV));
                        for (int k = 0; k < 3; k++) {
                            rgba[k] = c;
                        }
                        rgba[3] = 0xFF;
                        raster.setPixel(i0, i1, rgba);
                    }
                }
                break;
        }
        return bImage;
    }

    BufferedImage makeYZImage(int st) {
        int[] rgba = new int[4];
        ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_sRGB);
        int[] nBits
                = {
                    8, 8, 8, 8
                };
        ComponentColorModel colorModel = new ComponentColorModel(cs, nBits,
                true, true, Transparency.TRANSLUCENT, 0);
        WritableRaster raster
                = colorModel.createCompatibleWritableRaster(ny, nz);
        BufferedImage bImage
                = new BufferedImage(colorModel, raster, false, null);
        switch (dataType) {
            case FIELD_DATA_BYTE:
                for (int i1 = 0; i1 < nz; i1++) {
                    for (int i0 = 0, j = st + nx / 2 + i1 * (nx * ny + sliceSkip); i0 < ny; i0++, j += nx) {
                        if (j >= bData.length) {
                            break;
                        }
                        int c = bData[j];
                        for (int k = 0; k < 3; k++) {
                            rgba[k] = c;
                        }
                        rgba[3] = 0xFF;
                        raster.setPixel(i0, i1, rgba);
                    }
                }
                break;
            case FIELD_DATA_SHORT:
                for (int i1 = 0; i1 < nz; i1++) {
                    for (int i0 = 0, j = st + nx / 2 + i1 * (nx * ny + sliceSkip); i0 < ny; i0++, j += nx) {
                        if (j >= sData.length) {
                            break;
                        }
                        int c = (int) (255 * (sData[j] - minV) / (maxV - minV));
                        for (int k = 0; k < 3; k++) {
                            rgba[k] = c;
                        }
                        rgba[3] = 0xFF;
                        raster.setPixel(i0, i1, rgba);
                    }
                }
                break;
            case FIELD_DATA_INT:
                for (int i1 = 0; i1 < nz; i1++) {
                    for (int i0 = 0, j = st + nx / 2 + i1 * (nx * ny + sliceSkip); i0 < ny; i0++, j += nx) {
                        if (j >= iData.length) {
                            break;
                        }
                        int c = (int) (255 * (iData[j] - minV) / (maxV - minV));
                        for (int k = 0; k < 3; k++) {
                            rgba[k] = c;
                        }
                        rgba[3] = 0xFF;
                        raster.setPixel(i0, i1, rgba);
                    }
                }
                break;
            case FIELD_DATA_FLOAT:
                for (int i1 = 0; i1 < nz; i1++) {
                    for (int i0 = 0, j = st + nx / 2 + i1 * (nx * ny + sliceSkip); i0 < ny; i0++, j += nx) {
                        if (j >= fData.length) {
                            break;
                        }
                        int c = (int) (255.0f * (fData[j] - minV) / (maxV - minV));
                        for (int k = 0; k < 3; k++) {
                            rgba[k] = c;
                        }
                        rgba[3] = 0xFF;
                        raster.setPixel(i0, i1, rgba);
                    }
                }
                break;
        }
        return bImage;
    }
    private float autoCorrelation(int start, int end, int offset, byte[] bData)
    {
        float a0 = 0, a1 = 0, sp = 0, n0 = 0, n1 = 0;
        for (int i = start; i < end - offset; i += offset) {
            a0 += bData[i] & 0xff;
            a1 += bData[i + 2] & 0xff;
        }
        if (a0 == 0 || a1 == 0)
            return .8f;
        else {
            a0 /= (end - start - offset) / offset;
            a1 /= (end - start - offset) / offset;
            for (int i = start; i < end - offset; i += offset) {
                float t = (bData[i] & 0xff) - a0;
                float u = (bData[i + offset] & 0xff) - a1;
                sp += t * u;
                n0 += t * t;
                n1 += u * u;
            }
            return (float) (sp / Math.sqrt(n0 * n1));
        }
    }
    private float autoCorrelation(int start, int end, int offset, short[] data)
    {
        float a0 = 0, a1 = 0, sp = 0, n0 = 0, n1 = 0;
        for (int i = start; i < end - offset; i += offset) {
            a0 += data[i];
            a1 += data[i + 2];
        }
        if (a0 == 0 || a1 == 0)
            return .8f;
        else {
            a0 /= (end - start - offset) / offset;
            a1 /= (end - start - offset) / offset;
            for (int i = start; i < end - offset; i += offset) {
                float t = data[i] - a0;
                float u = data[i + offset] - a1;
                sp += t * u;
                n0 += t * t;
                n1 += u * u;
            }
            return (float) (sp / Math.sqrt(n0 * n1));
        }
    }
    private float autoCorrelation(int start, int end, int offset, int[] data)
    {
        float a0 = 0, a1 = 0, sp = 0, n0 = 0, n1 = 0;
        for (int i = start; i < end - offset; i += offset) {
            a0 += data[i];
            a1 += data[i + 2];
        }
        if (a0 == 0 || a1 == 0)
            return .8f;
        else {
            a0 /= (end - start - offset) / offset;
            a1 /= (end - start - offset) / offset;
            for (int i = start; i < end - offset; i += offset) {
                float t = data[i] - a0;
                float u = data[i + offset] - a1;
                sp += t * u;
                n0 += t * t;
                n1 += u * u;
            }
            return (float) (sp / Math.sqrt(n0 * n1));
        }
    }
    private float autoCorrelation(int start, int end, int offset, float[] data)
    {
        float a0 = 0, a1 = 0, sp = 0, n0 = 0, n1 = 0;
        for (int i = start; i < end - offset; i += offset) {
            a0 += data[i];
            a1 += data[i + 2];
        }
        if (a0 == 0 || a1 == 0)
            return .8f;
        else {
            a0 /= (end - start - offset) / offset;
            a1 /= (end - start - offset) / offset;
            for (int i = start; i < end - offset; i += offset) {
                float t = data[i] - a0;
                float u = data[i + offset] - a1;
                sp += t * u;
                n0 += t * t;
                n1 += u * u;
            }
            return (float) (sp / Math.sqrt(n0 * n1));
        }
    }

    private void guessDataStructure()
    {
        int size = (int) dataFileChooser.getSelectedFile().length() - (Integer)offsetSpinner.getValue();
        if (size > (2<<25))
            size = 2 << 25;
        minV = Float.MAX_VALUE;
        maxV = Float.MIN_VALUE;
        try {
            ImageInputStream in = new FileImageInputStream(dataFileChooser.getSelectedFile());
            bData = new byte[(int) size];
            in.readFully(bData);
            in.close();
            double c1 = 0, c20 = 0, c21 = 0, c40 = 0, c41 = 0, c42 = 0, c43 = 0;
            float sp = 0, n0 = 0, n1 = 0;
            int start = 1000;
            int end = (int) (.75 * size);
            float a0 = 0, a1 = 0;
            // estimating byte autocorrelations
            c1  = autoCorrelation(start,      end,     1,  bData);
            
            c20 = autoCorrelation(start,      end,     2,  bData);
            c21 = autoCorrelation(start + 1,  end + 1, 2,  bData);

            c40 = autoCorrelation(start,      end,     4,  bData);
            c41 = autoCorrelation(start + 1,  end + 1, 4,  bData);
            c42 = autoCorrelation(start + 2,  end + 2, 4,  bData);
            c43 = autoCorrelation(start + 3,  end + 3, 4,  bData);
            
            System.out.printf("%10.7f %n %10.7f %10.7f %n %10.7f %10.7f %10.7f %10.7f %n", 
                              c1, c20, c21, c40, c41, c42, c43);
            
            double[][] correlations = new double[][] {{c1}, {c20, c21}, {c40, c41, c42, c43}};
            double cmin = c1;
            int besti = 0, bestj = 0;
            for (int i = 0; i < correlations.length; i++) {
                double[] correlation = correlations[i];
                for (int j = 0; j < correlation.length; j++) {
                    double d = correlation[j];
                    if (d > cmin){
                        besti = i;
                        bestj = j;
                        cmin = d;
                    }
                }
            }
            switch (besti) {
                case 0:
                    byteButton.setSelected(true);
                        guessLabel.setText("<html>Data are probably of type byte.<p>Try to reread and proceed to the second step</html>");
                    break;
                case 1:
                    shortButton.setSelected(true);
                    if (bestj == 0) {
                        bigEndianButton.setSelected(true);
                        guessLabel.setText("<html>Data are probably of type short<p>stored in big endian order.<p>Try to reread and proceed to the second step</html>");
                    }
                else{
                        littleEndianButton.setSelected(true);
                        guessLabel.setText("<html>Data are probably of type short<p>stored in little endian order.<p>Try to reread and proceed to the second step</html>");
                    }
                break;
                case 2:
                    if (bestj == 0) {
                        bigEndianButton.setSelected(true);
                        guessLabel.setText("<html>Data are probably of type int or float<p>stored in big endian order.<p>Try to reread and proceed to the second step</html>");
                    }
                else{
                        littleEndianButton.setSelected(true);
                        guessLabel.setText("<html>Data are probably of type int or float<p>stored in little endian order.<p>Try to reread and proceed to the second step</html>");
                    }
                    
                    in = new FileImageInputStream(dataFileChooser.getSelectedFile());
                    in.setByteOrder(bigEndianButton.isSelected() ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
                    int iSize = size / 4;
                    iData = new int[iSize];
                    in.readFully(iData, 0, iSize);
                    in.close();
                    start = 1000;
                    end = (int) (.75 * iSize);
                    int min = Integer.MAX_VALUE;
                    int max = Integer.MIN_VALUE;
                    for (int i = start; i < end; i++) {
                        if (iData[i] < min) {
                            min = iData[i];
                        }
                        if (iData[i] > max) {
                            max = iData[i];
                        }
                    }
                    float d = (float)max - (float)min;
                    if (d > Integer.MAX_VALUE / 4)
                        System.out.println("probably not int data: " + min + " " + max);
                    
                    in = new FileImageInputStream(dataFileChooser.getSelectedFile());
                    in.setByteOrder(bigEndianButton.isSelected() ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
                    fData = new float[iSize];
                    in.readFully(fData, 0, iSize);
                    in.close();
                    float fmin = Float.MAX_VALUE;
                    float fmax = -fmin;
                    for (int i = start; i < end; i++) {
                        if (fData[i] < fmin) {
                            fmin = fData[i];
                        }
                        if (fData[i] > fmax) {
                            fmax = fData[i];
                        }
                    }
                    double dd = fmax - fmin;
                    if (dd > Float.MAX_VALUE / 4)
                        System.out.println("probably not float data: " + fmin + " " + fmax);
                break;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private int guessXSize(ByteOrder endian) {
        int size = (int) dataFileChooser.getSelectedFile().length() - (Integer) offsetSpinner.getValue();
        if (size > (2 << 25)) {
            size = 2 << 25;
        }
        minV = Float.MAX_VALUE;
        maxV = Float.MIN_VALUE;
        try {
            ImageInputStream in;
            int minLen = 32;
            int maxLen = 1025;
            if (size < 10 * maxLen) {
                maxLen = size / 10;
            }
            int start, end, bestXLen;
            float maxCorel = 0;
            switch (dataType) {
                case FIELD_DATA_BYTE:
                    bData = new byte[(int) size];
                    in = new FileImageInputStream(dataFileChooser.getSelectedFile());
                    in.readFully(bData);
                    in.close();
                    start = 1000;
                    end = (int) (.75 * size);
                    maxCorel = 0;
                    bestXLen = -1;
                    for (int len = minLen; len < maxLen; len++) {
                        float corel = autoCorrelation(start, end, len, bData);
                        System.out.printf("%4d %7.4f%n", len, corel);
                        if (corel > maxCorel) {
                            maxCorel = corel;
                            bestXLen = len;
                        }
                    }
                    System.out.println("best x length = " + bestXLen + " correlation = " + maxCorel);
                    return bestXLen;
                case FIELD_DATA_SHORT:
                    size /= 2;
                    sData = new short[(int) size];
                    in = new FileImageInputStream(dataFileChooser.getSelectedFile());
                    in.setByteOrder(endian);
                    in.readFully(sData, 0, size);
                    in.close();
                    start = 1000;
                    end = (int) (.75 * size);
                    maxCorel = 0;
                    bestXLen = -1;
                    for (int len = minLen; len < maxLen; len++) {
                        float corel = autoCorrelation(start, end, len, sData);
                        if (corel > maxCorel) {
                            maxCorel = corel;
                            bestXLen = len;
                        }
                    }
                    System.out.println("best x length = " + bestXLen + " correlation = " + maxCorel);
                    return bestXLen;
                case FIELD_DATA_INT:
                    size /= 4;
                    iData = new int[(int) size];
                    in = new FileImageInputStream(dataFileChooser.getSelectedFile());
                    in.setByteOrder(endian);
                    in.readFully(iData, 0, size);
                    in.close();
                    start = 1000;
                    end = (int) (.75 * size);
                    maxCorel = 0;
                    bestXLen = -1;
                    for (int len = minLen; len < maxLen; len++) {
                        float corel = autoCorrelation(start, end, len, iData);
                        if (corel > maxCorel) {
                            maxCorel = corel;
                            bestXLen = len;
                        }
                    }
                    System.out.println("best x length = " + bestXLen + " correlation = " + maxCorel);
                    return bestXLen;
                case FIELD_DATA_FLOAT:
                    size /= 4;
                    fData = new float[(int) size];
                    in = new FileImageInputStream(dataFileChooser.getSelectedFile());
                    in.setByteOrder(endian);
                    in.readFully(fData, 0, size);
                    in.close();
                    start = 1000;
                    end = (int) (.75 * size);
                    maxCorel = 0;
                    bestXLen = -1;
                    for (int len = minLen; len < maxLen; len++) {
                        float corel = autoCorrelation(start, end, len, fData);
                        if (corel > maxCorel) {
                            maxCorel = corel;
                            bestXLen = len;
                        }
                    }
                    System.out.println("best x length = " + bestXLen + " correlation = " + maxCorel);
                    return bestXLen;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }
    private int guessYSize(int rowLength, ByteOrder endian) {
        int size = (int) dataFileChooser.getSelectedFile().length() - (Integer) offsetSpinner.getValue();
        if (size > (2 << 25)) {
            size = 2 << 25;
        }
        minV = Float.MAX_VALUE;
        maxV = Float.MIN_VALUE;
        try {
            ImageInputStream in;
            int minLen = 32;
            int maxLen = 1025;
            if (size < 10 * rowLength * maxLen) {
                maxLen = size / (10 * rowLength);
            }
            int start, end, bestYLen;
            float maxCorel = 0;
            switch (dataType) {
                case FIELD_DATA_BYTE:
                    bData = new byte[(int) size];
                    in = new FileImageInputStream(dataFileChooser.getSelectedFile());
                    in.readFully(bData);
                    in.close();
                    start = 1000;
                    end = (int) (.75 * size);
                    maxCorel = 0;
                    bestYLen = -1;
                    for (int len = minLen; len < maxLen; len++) {
                        float corel = autoCorrelation(start, end, len * rowLength, bData);
                        System.out.printf("%4d %7.4f%n", len, corel);
                        if (corel > maxCorel) {
                            maxCorel = corel;
                            bestYLen = len;
                        }
                    }
                    System.out.println("best y length = " + bestYLen + " correlation = " + maxCorel);
                    return bestYLen;
                case FIELD_DATA_SHORT:
                    size /= 2;
                    sData = new short[(int) size];
                    in = new FileImageInputStream(dataFileChooser.getSelectedFile());
                    in.setByteOrder(endian);
                    in.readFully(sData, 0, size);
                    in.close();
                    start = 1000;
                    end = (int) (.75 * size);
                    maxCorel = 0;
                    bestYLen = -1;
                    for (int len = minLen; len < maxLen; len++) {
                        float corel = autoCorrelation(start, end, len * rowLength, sData);
                        System.out.printf("%4d %7.4f%n", len, corel);
                        if (corel > maxCorel) {
                            maxCorel = corel;
                            bestYLen = len;
                        }
                    }
                    System.out.println("best y length = " + bestYLen + " correlation = " + maxCorel);
                    return bestYLen;
                case FIELD_DATA_INT:
                    size /= 4;
                    iData = new int[(int) size];
                    in = new FileImageInputStream(dataFileChooser.getSelectedFile());
                    in.setByteOrder(endian);
                    in.readFully(iData, 0, size);
                    in.close();
                    start = 1000;
                    end = (int) (.75 * size);
                    maxCorel = 0;
                    bestYLen = -1;
                    for (int len = minLen; len < maxLen; len++) {
                        float corel = autoCorrelation(start, end, len * rowLength, iData);
                        if (corel > maxCorel) {
                            maxCorel = corel;
                            bestYLen = len;
                        }
                    }
                    System.out.println("best y length = " + bestYLen + " correlation = " + maxCorel);
                    return bestYLen;
                case FIELD_DATA_FLOAT:
                    size /= 4;
                    fData = new float[(int) size];
                    in = new FileImageInputStream(dataFileChooser.getSelectedFile());
                    in.setByteOrder(endian);
                    in.readFully(fData, 0, size);
                    in.close();
                    start = (int) (.25 * size);
                    end = (int) (.75 * size);
                    maxCorel = 0;
                    bestYLen = -1;
                    for (int len = minLen; len < maxLen; len++) {
                        float corel = autoCorrelation(start, end, len * rowLength, fData);
                        if (corel > maxCorel) {
                            maxCorel = corel;
                            bestYLen = len;
                        }
                    }
                    System.out.println("best y length = " + bestYLen + " correlation = " + maxCorel);
                    return bestYLen;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    private void readData(boolean reduce) {
        if(dataFileChooser.getSelectedFile() == null)
            return;
        
        size = (int) dataFileChooser.getSelectedFile().length() - (Integer)offsetSpinner.getValue();
        if (reduce && size > (2<<25))
            size = 2<<25;
        minV = Float.MAX_VALUE;
        maxV = Float.MIN_VALUE;
        try {
            ImageInputStream in = new FileImageInputStream(dataFileChooser.getSelectedFile());
            in.skipBytes((Integer)offsetSpinner.getValue());
            in.setByteOrder(bigEndianButton.isSelected() ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
            if (byteButton.isSelected()) {
                dataType = DataArrayType.FIELD_DATA_BYTE;
                bData = new byte[(int) size];
                in.readFully(bData);
                minV = 0;
                maxV = 255;
            } else if (shortButton.isSelected()) {
                dataType = DataArrayType.FIELD_DATA_SHORT;
                size /= 2;
                sData = new short[size];
                in.readFully(sData, 0, size);
                for (int i = 0; i < size; i++) {
                    if (sData[i] < minV) {
                        minV = sData[i];
                    }
                    if (sData[i] > maxV) {
                        maxV = sData[i];
                    }
                }
                System.out.println(minV + " " + maxV);
            } else if (intButton.isSelected()) {
                dataType = DataArrayType.FIELD_DATA_INT;
                size /= 4;
                iData = new int[size];
                in.readFully(iData, 0, size);
                for (int i = 0; i < size; i++) {
                    if (iData[i] < minV) {
                        minV = iData[i];
                    }
                    if (iData[i] > maxV) {
                        maxV = iData[i];
                    }
                }
                System.out.println(minV + " " + maxV);
            } else if (floatButton.isSelected()) {
                dataType = DataArrayType.FIELD_DATA_FLOAT;
                size /= 4;
                fData = new float[size];
                in.readFully(fData, 0, size);
                for (int i = 0; i < size; i++) {
                    if (fData[i] < minV) {
                        minV = fData[i];
                    }
                    if (fData[i] > maxV) {
                        maxV = fData[i];
                    }
                }
                System.out.println(minV + " " + maxV);
            }
            datamapRangeSlider.setParams(minV, maxV, minV, maxV);
            refresh();
            in.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        dataGroup = new javax.swing.ButtonGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        leftSlicePanel = new javax.swing.JPanel();
        centerSlicePanel = new javax.swing.JPanel();
        rightSlicePanel = new javax.swing.JPanel();
        controlPanel = new javax.swing.JPanel();
        operationsPanel = new javax.swing.JPanel();
        structPanel = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        selectButton = new javax.swing.JButton();
        rereadButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        datamapRangeSlider = new org.visnow.vn.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider();
        offsetSpinner = new javax.swing.JSpinner();
        jPanel8 = new javax.swing.JPanel();
        bigEndianButton = new javax.swing.JRadioButton();
        littleEndianButton = new javax.swing.JRadioButton();
        jPanel11 = new javax.swing.JPanel();
        shortButton = new javax.swing.JRadioButton();
        intButton = new javax.swing.JRadioButton();
        floatButton = new javax.swing.JRadioButton();
        byteButton = new javax.swing.JRadioButton();
        guessButton = new javax.swing.JButton();
        guessLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        dimsPanel = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        zRoller = new org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedIntRoller();
        skipPixelsSlider = new javax.swing.JSlider();
        skipLinesSlider = new javax.swing.JSlider();
        sliceSkipSlider = new javax.swing.JSlider();
        fullReadButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        guessXButton = new javax.swing.JButton();
        xRoller = new org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedIntRoller();
        jPanel2 = new javax.swing.JPanel();
        guessYButton = new javax.swing.JButton();
        yRoller = new org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedIntRoller();
        jLabel3 = new javax.swing.JLabel();
        geomPanel = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        xSlider = new javax.swing.JSlider();
        ySlider = new javax.swing.JSlider();
        zSlider = new javax.swing.JSlider();
        jPanel10 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        fileButton = new javax.swing.JRadioButton();
        dimButton = new javax.swing.JRadioButton();
        geomButton = new javax.swing.JRadioButton();
        jPanel6 = new javax.swing.JPanel();
        readButton = new javax.swing.JButton();
        fileNameField = new javax.swing.JTextField();

        setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        setMinimumSize(new java.awt.Dimension(800, 800));
        setPreferredSize(new java.awt.Dimension(1050, 1050));
        setLayout(new java.awt.GridLayout(2, 2));

        leftSlicePanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        leftSlicePanel.setMinimumSize(new java.awt.Dimension(200, 300));
        leftSlicePanel.setPreferredSize(new java.awt.Dimension(300, 500));
        leftSlicePanel.setLayout(new java.awt.BorderLayout());
        add(leftSlicePanel);

        centerSlicePanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        centerSlicePanel.setMinimumSize(new java.awt.Dimension(200, 300));
        centerSlicePanel.setPreferredSize(new java.awt.Dimension(300, 500));
        centerSlicePanel.setLayout(new java.awt.BorderLayout());
        add(centerSlicePanel);

        rightSlicePanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        rightSlicePanel.setMinimumSize(new java.awt.Dimension(200, 300));
        rightSlicePanel.setPreferredSize(new java.awt.Dimension(300, 500));
        rightSlicePanel.setLayout(new java.awt.BorderLayout());
        add(rightSlicePanel);

        controlPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        controlPanel.setLayout(new java.awt.BorderLayout());

        operationsPanel.setLayout(new java.awt.CardLayout());

        structPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("file properties and data mapping"));
        structPanel.setLayout(new java.awt.GridLayout(1, 0, 5, 0));

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel7.setLayout(new java.awt.GridBagLayout());

        selectButton.setText("select file");
        selectButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        selectButton.setPreferredSize(new java.awt.Dimension(60, 25));
        selectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel7.add(selectButton, gridBagConstraints);

        rereadButton.setText("re-read");
        rereadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rereadButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 0);
        jPanel7.add(rereadButton, gridBagConstraints);

        jLabel1.setText("byte offset");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel7.add(jLabel1, gridBagConstraints);

        datamapRangeSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "datamap range", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        datamapRangeSlider.setMinimumSize(new java.awt.Dimension(96, 85));
        datamapRangeSlider.setPreferredSize(new java.awt.Dimension(150, 87));
        datamapRangeSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                datamapRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel7.add(datamapRangeSlider, gridBagConstraints);

        offsetSpinner.setModel(new javax.swing.SpinnerNumberModel(0, 0, 3, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 4);
        jPanel7.add(offsetSpinner, gridBagConstraints);

        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.setLayout(new java.awt.GridLayout(1, 0));

        buttonGroup1.add(bigEndianButton);
        bigEndianButton.setSelected(true);
        bigEndianButton.setText("big endian");
        jPanel8.add(bigEndianButton);

        buttonGroup1.add(littleEndianButton);
        littleEndianButton.setText("little endian");
        littleEndianButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                littleEndianButtonActionPerformed(evt);
            }
        });
        jPanel8.add(littleEndianButton);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
        jPanel7.add(jPanel8, gridBagConstraints);

        jPanel11.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel11.setLayout(new java.awt.GridLayout(2, 2));

        dataGroup.add(shortButton);
        shortButton.setText("short");
        shortButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                shortButtonActionPerformed(evt);
            }
        });
        jPanel11.add(shortButton);

        dataGroup.add(intButton);
        intButton.setText("int");
        intButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                intButtonActionPerformed(evt);
            }
        });
        jPanel11.add(intButton);

        dataGroup.add(floatButton);
        floatButton.setText("float");
        floatButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                floatButtonActionPerformed(evt);
            }
        });
        jPanel11.add(floatButton);

        dataGroup.add(byteButton);
        byteButton.setSelected(true);
        byteButton.setText("byte");
        byteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                byteButtonActionPerformed(evt);
            }
        });
        jPanel11.add(byteButton);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
        jPanel7.add(jPanel11, gridBagConstraints);

        guessButton.setText("<html>guess data type <p>and byte order</html>");
        guessButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guessButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel7.add(guessButton, gridBagConstraints);

        guessLabel.setText("<html><p><p><p></html>");
        guessLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weighty = 1.0;
        jPanel7.add(guessLabel, gridBagConstraints);

        structPanel.add(jPanel7);

        jLabel2.setText("<html>Select data file and look at the upper left window.<p><p>\nTry to guess data type (byte/short/int/float), <p>\nfile endianness (byte order) and initial byte offset<p>\ntrying to obtain relatively smooth horizontal lines in this window.<p>\nNote: re-read file after selecting any new combination of buttons.<p><p>\nSet datamap range slider to obtain a reasonable grayscale map.</html>");
        jLabel2.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel2.setMaximumSize(new java.awt.Dimension(500, 15));
        jLabel2.setMinimumSize(new java.awt.Dimension(200, 15));
        jLabel2.setPreferredSize(new java.awt.Dimension(300, 15));
        structPanel.add(jLabel2);

        operationsPanel.add(structPanel, "file");

        dimsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("volume dimensions"));
        dimsPanel.setLayout(new java.awt.GridLayout(1, 0, 5, 0));

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setLayout(new java.awt.GridBagLayout());

        zRoller.setSensitivity(3.0F);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
        jPanel4.add(zRoller, gridBagConstraints);

        skipPixelsSlider.setFont(new java.awt.Font("Dialog", 0, 8)); // NOI18N
        skipPixelsSlider.setMajorTickSpacing(100);
        skipPixelsSlider.setMaximum(500);
        skipPixelsSlider.setMinorTickSpacing(5);
        skipPixelsSlider.setPaintLabels(true);
        skipPixelsSlider.setPaintTicks(true);
        skipPixelsSlider.setValue(0);
        skipPixelsSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "skip pixels", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        skipPixelsSlider.setMinimumSize(new java.awt.Dimension(36, 50));
        skipPixelsSlider.setOpaque(false);
        skipPixelsSlider.setPreferredSize(new java.awt.Dimension(200, 50));
        skipPixelsSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                skipPixelsSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 3);
        jPanel4.add(skipPixelsSlider, gridBagConstraints);

        skipLinesSlider.setFont(new java.awt.Font("Dialog", 0, 8)); // NOI18N
        skipLinesSlider.setMajorTickSpacing(100);
        skipLinesSlider.setMaximum(500);
        skipLinesSlider.setMinorTickSpacing(5);
        skipLinesSlider.setPaintLabels(true);
        skipLinesSlider.setPaintTicks(true);
        skipLinesSlider.setValue(0);
        skipLinesSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "skip rows", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        skipLinesSlider.setMinimumSize(new java.awt.Dimension(36, 50));
        skipLinesSlider.setOpaque(false);
        skipLinesSlider.setPreferredSize(new java.awt.Dimension(200, 50));
        skipLinesSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                skipLinesSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 3);
        jPanel4.add(skipLinesSlider, gridBagConstraints);

        sliceSkipSlider.setFont(new java.awt.Font("Dialog", 0, 8)); // NOI18N
        sliceSkipSlider.setMajorTickSpacing(10);
        sliceSkipSlider.setMaximum(32);
        sliceSkipSlider.setMinorTickSpacing(1);
        sliceSkipSlider.setPaintLabels(true);
        sliceSkipSlider.setPaintTicks(true);
        sliceSkipSlider.setValue(0);
        sliceSkipSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "skip inter slices", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        sliceSkipSlider.setMinimumSize(new java.awt.Dimension(36, 50));
        sliceSkipSlider.setOpaque(false);
        sliceSkipSlider.setPreferredSize(new java.awt.Dimension(60, 50));
        sliceSkipSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sliceSkipSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel4.add(sliceSkipSlider, gridBagConstraints);

        fullReadButton.setText("re-read full dataset");
        fullReadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fullReadButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 0);
        jPanel4.add(fullReadButton, gridBagConstraints);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("row length setting"));
        jPanel1.setMinimumSize(new java.awt.Dimension(400, 76));
        jPanel1.setPreferredSize(new java.awt.Dimension(100, 80));
        jPanel1.setLayout(new java.awt.GridLayout(2, 0));

        guessXButton.setText("guess x dimension");
        guessXButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guessXButtonActionPerformed(evt);
            }
        });
        jPanel1.add(guessXButton);

        xRoller.setMaximumSize(new java.awt.Dimension(500, 2147483647));
        xRoller.setMinimumSize(new java.awt.Dimension(200, 22));
        xRoller.setPreferredSize(new java.awt.Dimension(300, 25));
        xRoller.setSensitivity(3.0F);
        jPanel1.add(xRoller);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel4.add(jPanel1, gridBagConstraints);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("row length setting"));
        jPanel2.setMinimumSize(new java.awt.Dimension(400, 76));
        jPanel2.setPreferredSize(new java.awt.Dimension(100, 80));
        jPanel2.setLayout(new java.awt.GridLayout(2, 0));

        guessYButton.setText("guess y dimension");
        guessYButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guessYButtonActionPerformed(evt);
            }
        });
        jPanel2.add(guessYButton);

        yRoller.setSensitivity(3.0F);
        jPanel2.add(yRoller);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel4.add(jPanel2, gridBagConstraints);

        dimsPanel.add(jPanel4);

        jLabel3.setText("<html>\n1. Try to guess x dimension of the box and obtain a reasonably looking slice\nin the upper left window. <p>Use <b>x size</b> roller and spinner box for fine tuning.<p>\nHint: when the image appears to be slanted to the left, increase the value, \nif slanted right, decrease value. If multiple copies of an image seem to appear, \ndivide the x dimension by the number of copies.<p><p>\n2. Guess the y resolution with the <b>y size</b> roller and spinner looking at the bottom left image<p><p>\n3. Click the <b>re-read full dataset</b> button to view all data and adjust the z resolution<p><p>\n4. When thrash looking pixels appear in the top left window, try to use <b>skip rows</b> and \n<b>skip pixels</b> sliders.");
        jLabel3.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel3.setMaximumSize(new java.awt.Dimension(500, 15));
        jLabel3.setMinimumSize(new java.awt.Dimension(200, 15));
        jLabel3.setPreferredSize(new java.awt.Dimension(300, 15));
        dimsPanel.add(jLabel3);

        operationsPanel.add(dimsPanel, "dims");

        geomPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("volume geometry"));
        geomPanel.setMinimumSize(new java.awt.Dimension(100, 160));
        geomPanel.setPreferredSize(new java.awt.Dimension(120, 165));
        geomPanel.setLayout(new java.awt.GridLayout(1, 2, 5, 0));

        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel9.setLayout(new java.awt.GridBagLayout());

        xSlider.setFont(new java.awt.Font("Dialog", 0, 8)); // NOI18N
        xSlider.setMajorTickSpacing(100);
        xSlider.setMaximum(500);
        xSlider.setMinorTickSpacing(5);
        xSlider.setPaintLabels(true);
        xSlider.setPaintTicks(true);
        xSlider.setValue(100);
        xSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(null, "x scale (%)", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10)), "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        xSlider.setMaximumSize(new java.awt.Dimension(32767, 100));
        xSlider.setMinimumSize(new java.awt.Dimension(100, 60));
        xSlider.setPreferredSize(new java.awt.Dimension(200, 80));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanel9.add(xSlider, gridBagConstraints);

        ySlider.setFont(new java.awt.Font("Dialog", 0, 8)); // NOI18N
        ySlider.setMajorTickSpacing(100);
        ySlider.setMaximum(500);
        ySlider.setMinorTickSpacing(5);
        ySlider.setPaintLabels(true);
        ySlider.setPaintTicks(true);
        ySlider.setValue(100);
        ySlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "y scale (%)", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        ySlider.setMaximumSize(new java.awt.Dimension(32767, 100));
        ySlider.setMinimumSize(new java.awt.Dimension(36, 60));
        ySlider.setPreferredSize(new java.awt.Dimension(200, 80));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel9.add(ySlider, gridBagConstraints);

        zSlider.setFont(new java.awt.Font("Dialog", 0, 8)); // NOI18N
        zSlider.setMajorTickSpacing(100);
        zSlider.setMaximum(500);
        zSlider.setMinorTickSpacing(5);
        zSlider.setPaintLabels(true);
        zSlider.setPaintTicks(true);
        zSlider.setValue(100);
        zSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "z scale (%)", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        zSlider.setMaximumSize(new java.awt.Dimension(32767, 100));
        zSlider.setMinimumSize(new java.awt.Dimension(100, 60));
        zSlider.setPreferredSize(new java.awt.Dimension(200, 80));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel9.add(zSlider, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        jPanel9.add(jPanel10, gridBagConstraints);

        geomPanel.add(jPanel9);

        jLabel4.setText("<html> Use scale sliders to obtain desired proportions of the data box.<p><p>\nWhen finished, use <b>output data</b> button below to create the output field.</html>");
        jLabel4.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        geomPanel.add(jLabel4);

        operationsPanel.add(geomPanel, "geometry");

        controlPanel.add(operationsPanel, java.awt.BorderLayout.CENTER);

        jPanel5.setMinimumSize(new java.awt.Dimension(305, 100));
        jPanel5.setPreferredSize(new java.awt.Dimension(305, 100));
        jPanel5.setLayout(new java.awt.GridLayout(3, 0));

        buttonGroup2.add(fileButton);
        fileButton.setSelected(true);
        fileButton.setText("<html>step 1: select file and byte order, <p>set data type and displayed values</html>");
        fileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileButtonActionPerformed(evt);
            }
        });
        jPanel5.add(fileButton);

        buttonGroup2.add(dimButton);
        dimButton.setText("step 2: interactively guess data dimensions");
        dimButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dimButtonActionPerformed(evt);
            }
        });
        jPanel5.add(dimButton);

        buttonGroup2.add(geomButton);
        geomButton.setText("step 3: set geometry and create output field");
        geomButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                geomButtonActionPerformed(evt);
            }
        });
        jPanel5.add(geomButton);

        controlPanel.add(jPanel5, java.awt.BorderLayout.NORTH);

        jPanel6.setLayout(new java.awt.BorderLayout());

        readButton.setText("output final data");
        readButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        readButton.setMaximumSize(new java.awt.Dimension(200, 25));
        readButton.setMinimumSize(new java.awt.Dimension(200, 25));
        readButton.setPreferredSize(new java.awt.Dimension(200, 25));
        readButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                readButtonActionPerformed(evt);
            }
        });
        jPanel6.add(readButton, java.awt.BorderLayout.EAST);

        fileNameField.setMinimumSize(new java.awt.Dimension(4, 24));
        fileNameField.setPreferredSize(new java.awt.Dimension(4, 24));
        jPanel6.add(fileNameField, java.awt.BorderLayout.CENTER);

        controlPanel.add(jPanel6, java.awt.BorderLayout.SOUTH);

        add(controlPanel);
    }// </editor-fold>//GEN-END:initComponents

    private void skipPixelsSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_skipPixelsSliderStateChanged
    {//GEN-HEADEREND:event_skipPixelsSliderStateChanged
        refresh();
    }//GEN-LAST:event_skipPixelsSliderStateChanged

    private void skipLinesSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_skipLinesSliderStateChanged
    {//GEN-HEADEREND:event_skipLinesSliderStateChanged
        refresh();
    }//GEN-LAST:event_skipLinesSliderStateChanged

    private void sliceSkipSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_sliceSkipSliderStateChanged
    {//GEN-HEADEREND:event_sliceSkipSliderStateChanged
        refresh();
    }//GEN-LAST:event_sliceSkipSliderStateChanged

    private void readButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_readButtonActionPerformed
    {//GEN-HEADEREND:event_readButtonActionPerformed
        scale[0] = xSlider.getValue() / 100.f;
        scale[1] = ySlider.getValue() / 100.f;
        scale[2] = zSlider.getValue() / 100.f;
        fireStateChanged();
    }//GEN-LAST:event_readButtonActionPerformed

    private void selectButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_selectButtonActionPerformed
    {//GEN-HEADEREND:event_selectButtonActionPerformed
        if (lastPath == null) {
            dataFileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(ReadUnknownVolume.class)));
        } else {
            dataFileChooser.setCurrentDirectory(new File(lastPath));
        }

        int returnVal = dataFileChooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            fileName = dataFileChooser.getSelectedFile().getAbsolutePath();
            lastPath = fileName.substring(0, fileName.lastIndexOf(File.separator));
            if (inVisNow)
                VisNow.get().getMainConfig().setLastDataPath(lastPath, ReadUnknownVolume.class);
        }
        fileNameField.setText(fileName);
        readData(true);
    }//GEN-LAST:event_selectButtonActionPerformed

    private void updateMapRange()
    {
        if (byteButton.isSelected())
            datamapRangeSlider.setParams(0, 256, 0, 256);
        else 
            datamapRangeSlider.setParams(-1000, 1000, -1000, 1000);    
    }
    
    private void shortButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_shortButtonActionPerformed
    {//GEN-HEADEREND:event_shortButtonActionPerformed
        updateMapRange(); 
    }//GEN-LAST:event_shortButtonActionPerformed

    private void littleEndianButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_littleEndianButtonActionPerformed
    {//GEN-HEADEREND:event_littleEndianButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_littleEndianButtonActionPerformed

    private void datamapRangeSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_datamapRangeSliderStateChanged
    {//GEN-HEADEREND:event_datamapRangeSliderStateChanged
        minV = datamapRangeSlider.getLow();
        maxV = datamapRangeSlider.getUp();
        refresh();
    }//GEN-LAST:event_datamapRangeSliderStateChanged

    private void rereadButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_rereadButtonActionPerformed
    {//GEN-HEADEREND:event_rereadButtonActionPerformed
        if (fileName == null) 
            selectButtonActionPerformed(evt);
        else
            readData(true);
    }//GEN-LAST:event_rereadButtonActionPerformed

    
    protected void cardSelect(String card)
    {
        CardLayout cl = (CardLayout)operationsPanel.getLayout();
        cl.show(operationsPanel, card);
    }
    
    private void fileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileButtonActionPerformed
        CardLayout cl = (CardLayout)operationsPanel.getLayout();
        cl.show(operationsPanel, "file");
    }//GEN-LAST:event_fileButtonActionPerformed

    private void dimButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dimButtonActionPerformed
        CardLayout cl = (CardLayout)operationsPanel.getLayout();
        cl.show(operationsPanel, "dims");
        setAutoRowLength();
    }//GEN-LAST:event_dimButtonActionPerformed

    private void geomButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_geomButtonActionPerformed
        CardLayout cl = (CardLayout)operationsPanel.getLayout();
        cl.show(operationsPanel, "geometry");// TODO add your handling code here:
    }//GEN-LAST:event_geomButtonActionPerformed

    private void byteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_byteButtonActionPerformed
        updateMapRange(); 
    }//GEN-LAST:event_byteButtonActionPerformed

    private void intButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_intButtonActionPerformed
        updateMapRange(); 
    }//GEN-LAST:event_intButtonActionPerformed

    private void floatButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_floatButtonActionPerformed
        updateMapRange(); 
    }//GEN-LAST:event_floatButtonActionPerformed

    private void fullReadButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_fullReadButtonActionPerformed
    {//GEN-HEADEREND:event_fullReadButtonActionPerformed
                if (fileName == null) 
            selectButtonActionPerformed(evt);
        else
            readData(false);
    }//GEN-LAST:event_fullReadButtonActionPerformed

    private void guessButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_guessButtonActionPerformed
    {//GEN-HEADEREND:event_guessButtonActionPerformed
        guessDataStructure();
    }//GEN-LAST:event_guessButtonActionPerformed

    private void guessXButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guessXButtonActionPerformed
        setAutoRowLength();
    }//GEN-LAST:event_guessXButtonActionPerformed

    private void guessYButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guessYButtonActionPerformed
        setAutoColLength();
    }//GEN-LAST:event_guessYButtonActionPerformed

    private void setAutoRowLength()
    {
        nx = guessXSize(bigEndianButton.isSelected() ? 
                        ByteOrder.BIG_ENDIAN : 
                        ByteOrder.LITTLE_ENDIAN);
        xRoller.setEnabled(false);
        xRoller.setValue(nx);
        refresh();
        xRoller.setEnabled(true);
        readData(true);
    }
    private void setAutoColLength()
    {
        ny = guessYSize(xRoller.getValue(),
                        bigEndianButton.isSelected() ? 
                        ByteOrder.BIG_ENDIAN : 
                        ByteOrder.LITTLE_ENDIAN);
        yRoller.setEnabled(false);
        yRoller.setValue(ny);
        refresh();
        yRoller.setEnabled(true);
        readData(true);
    }
    
    /**
     * Utility field holding list of ChangeListeners.
     */
    private transient ArrayList<ChangeListener> changeListenerList
            = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     *
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener) {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     *
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener) {
        changeListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param object Parameter #1 of the      <CODE>ChangeEvent<CODE> constructor.
     */
    private void fireStateChanged() {
        ChangeEvent e = new ChangeEvent(this);
        for (ChangeListener listener : changeListenerList) {
            listener.stateChanged(e);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton bigEndianButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.JRadioButton byteButton;
    private javax.swing.JPanel centerSlicePanel;
    private javax.swing.JPanel controlPanel;
    private javax.swing.ButtonGroup dataGroup;
    private org.visnow.vn.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider datamapRangeSlider;
    private javax.swing.JRadioButton dimButton;
    private javax.swing.JPanel dimsPanel;
    private javax.swing.JRadioButton fileButton;
    private javax.swing.JTextField fileNameField;
    private javax.swing.JRadioButton floatButton;
    private javax.swing.JButton fullReadButton;
    private javax.swing.JRadioButton geomButton;
    private javax.swing.JPanel geomPanel;
    private javax.swing.JButton guessButton;
    private javax.swing.JLabel guessLabel;
    private javax.swing.JButton guessXButton;
    private javax.swing.JButton guessYButton;
    private javax.swing.JRadioButton intButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel leftSlicePanel;
    private javax.swing.JRadioButton littleEndianButton;
    private javax.swing.JSpinner offsetSpinner;
    private javax.swing.JPanel operationsPanel;
    private javax.swing.JButton readButton;
    private javax.swing.JButton rereadButton;
    private javax.swing.JPanel rightSlicePanel;
    private javax.swing.JButton selectButton;
    private javax.swing.JRadioButton shortButton;
    private javax.swing.JSlider skipLinesSlider;
    private javax.swing.JSlider skipPixelsSlider;
    private javax.swing.JSlider sliceSkipSlider;
    private javax.swing.JPanel structPanel;
    private org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedIntRoller xRoller;
    private javax.swing.JSlider xSlider;
    private org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedIntRoller yRoller;
    private javax.swing.JSlider ySlider;
    private org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedIntRoller zRoller;
    private javax.swing.JSlider zSlider;
    // End of variables declaration//GEN-END:variables

    void activateOpenDialog() {
        selectButtonActionPerformed(null);
    }
    
    
}
