/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
package org.visnow.vn.lib.basic.readers.ReadCSV;

import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.swingwrappers.TextField;
import org.visnow.vn.gui.widgets.RunButton;
import org.visnow.vn.gui.widgets.SteppedComboBox;
import static org.visnow.vn.lib.basic.readers.ReadCSV.ReadCSVShared.*;
import org.visnow.vn.system.main.VisNow;

/**
 * GUI for CSV reader.
 *
 * @author Bartosz Borucki (babor@icm.edu.pl) Warsaw University, Interdisciplinary Centre for Mathematical and Computational Modelling
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class ReadCSVGUI extends javax.swing.JPanel
{

    private static final long serialVersionUID = 9213869686282169136L;

    private int nColumns = 0;
    private static final String[] ACTION_TABLE_HEADER = new String[]{"headers", "first row", "action", "import", "timedata"};
    private static final Class[] ACTION_TABLE_TYPES = new Class[]{String.class, String.class, Object.class, Boolean.class, Boolean.class};
    private static final int[] ACTION_TABLE_COLUMN_WIDTH = new int[]{80, 80, 50, 20, 20};
    private JPopupMenu importColumnPopup;
    private boolean importColumnSelectionInProgress = false;
    private Parameters parameters;
    private final FileNameExtensionFilter nfdFilter = new FileNameExtensionFilter("CSV and TSV files (*.csv, *.tsv, *.txt)", "csv", "CSV", "tsv", "TSV", "txt", "TXT");
    private final JFileChooser fileChooser = new JFileChooser();

    /**
     * Creates new form GUI
     */
    public ReadCSVGUI()
    {
        initComponents();
        fileChooser.setFileFilter(nfdFilter);
        actionTable.setRowHeight(22);
        for (int i = 1; i < ACTION_TABLE_COLUMN_WIDTH.length; i++)
            actionTable.getColumnModel().getColumn(i).setWidth(ACTION_TABLE_COLUMN_WIDTH[i]);
        actionTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        actionTable.getTableHeader().setReorderingAllowed(false);

        //create import column popup menu        
        JMenuItem importColumnPopupSelectAll = new JMenuItem("select all");
        importColumnPopupSelectAll.addActionListener((java.awt.event.ActionEvent evt) -> {
            importColumnSelectAll();
        });
        JMenuItem importColumnPopupUnselectAll = new JMenuItem("unselect all");
        importColumnPopupUnselectAll.addActionListener((java.awt.event.ActionEvent evt) -> {
            importColumnUnselectAll();
        });
        JMenuItem importColumnPopupInvert = new JMenuItem("invert selection");
        importColumnPopupInvert.addActionListener((java.awt.event.ActionEvent evt) -> {
            importColumnInvert();
        });

        importColumnPopup = new JPopupMenu();
        importColumnPopup.add(importColumnPopupSelectAll);
        importColumnPopup.add(importColumnPopupUnselectAll);
        importColumnPopup.add(importColumnPopupInvert);
        actionTable.getTableHeader().addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseReleased(MouseEvent e)
            {
                int c = actionTable.getTableHeader().columnAtPoint(e.getPoint());
                if (e.getButton() == MouseEvent.BUTTON3 && (e.getComponent() instanceof JTableHeader) && c == 3) {
                    importColumnPopup.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        });
        actionTable.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseReleased(MouseEvent e)
            {
                int c = actionTable.columnAtPoint(e.getPoint());
                if (e.getButton() == MouseEvent.BUTTON3 && c == 3) {
                    importColumnPopup.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        });
        action_2DCB.setModel(new DefaultComboBoxModel(ReadCSVShared.ACTION_NAMES));
        action_2DCB.setSelectedIndex(DEFAULT_ACTION_INDEX);
    }

    public void setParameters(Parameters parameters)
    {
        this.parameters = parameters;
    }

    void updateGUI(final ParameterProxy p, boolean resetFully, boolean setRunButtonPending)
    {
        if (p.get(FIELD_DELIMITER).equals("\t")) {
            delimiterTF.setText("\\t");
        } else {
            delimiterTF.setText(p.get(FIELD_DELIMITER));
        }
        headerCB.setSelected(p.get(HAS_HEADER_LINE));
        filePathTF.setText(p.get(FILENAME));

        String[] headerNames = p.get(HEADER_NAMES);
        String[] firstRow = p.get(FIRST_ROW);
        nColumns = headerNames.length;

        Object[][] actionTableContent = new Object[nColumns][5];
        for (int i = 0; i < nColumns; i++) {
            actionTableContent[i][0] = headerNames[i];
            actionTableContent[i][1] = firstRow[i];
            for (int j = 0; j < ReadCSVShared.ACTION_CODES.length; j++)
                if (ReadCSVShared.ACTION_CODES[j] == p.get(ACTIONS_1D)[i]) {
                    actionTableContent[i][2] = ReadCSVShared.ACTION_NAMES[j];
                    break;
                }
            actionTableContent[i][3] = p.get(COLUMN_SELECTION)[i];
            actionTableContent[i][4] = p.get(TIMEDATA_COLUMN_INDEX) == i;
        }

        DefaultTableModel tm = new DefaultTableModel(actionTableContent, ACTION_TABLE_HEADER)
        {
            private static final long serialVersionUID = -8548704006864930491L;

            @Override
            public Class getColumnClass(int columnIndex)
            {
                return ACTION_TABLE_TYPES[columnIndex];
            }

        };
        tm.addTableModelListener((TableModelEvent e) -> {
            if (importColumnSelectionInProgress)
                return;
            timeDataColumnChangeAction();
            runButton1.setPendingIfNoAuto();
        });
        actionTable.setModel(tm);
        for (int i = 1; i < 5; i++)
            actionTable.getColumnModel().getColumn(i).setWidth(ACTION_TABLE_COLUMN_WIDTH[i]);
        SteppedComboBox comboBox = new SteppedComboBox();
        DefaultComboBoxModel model = new DefaultComboBoxModel(ReadCSVShared.ACTION_NAMES);
        comboBox.setModel(model);
        setUpActionsColumn(comboBox, actionTable.getColumnModel().getColumn(2));

        runButton1.updateAutoState(p.get(RUNNING_MESSAGE));
        runButton1.updatePendingState(setRunButtonPending);
    }

    private void updateParameters()
    {
        parameters.setParameterActive(false);
        int[] actions = new int[nColumns];
        boolean[] columnSelection = new boolean[nColumns];
        int timedataColumnIndex = -1;
        for (int i = 0; i < nColumns; i++) {
            String s = (String) (actionTable.getValueAt(i, 2));
            for (int j = 0; j < ReadCSVShared.ACTION_NAMES.length; j++)
                if (s.equalsIgnoreCase(ReadCSVShared.ACTION_NAMES[j])) {
                    actions[i] = ReadCSVShared.ACTION_CODES[j];
                    break;
                }
            columnSelection[i] = (Boolean) (actionTable.getValueAt(i, 3));
            if (((Boolean) actionTable.getValueAt(i, 4)) == true) {
                timedataColumnIndex = i;
            }
        }

        parameters.set(FILENAME, filePathTF.getText());
        parameters.set(FIELD_DELIMITER, delimiterTF.getText());
        parameters.set(NUMBER_OF_ROWS_TO_SKIP, parseNonnegativeInteger(rowsToSkipTF));
        parameters.set(NUMBER_OF_COLUMNS_TO_SKIP, parseNonnegativeInteger(columnsToSkipTF));
         parameters.set(HAS_HEADER_LINE, headerCB.isSelected());
        // HEADER_NAMES and FIRST_ROW are set in readFirstTwoRowsAndUpdateGUI()
        parameters.set(MODE_1D, jTabbedPane1.getSelectedIndex() == 0);
        parameters.set(ACTIONS_1D, actions);
        parameters.set(ACTION_2D, ReadCSVShared.ACTION_CODES[action_2DCB.getSelectedIndex()]);
        parameters.set(ROW_LABELS_INDEX, parseNonnegativeInteger(rowLabelsIndexTF));
        parameters.set(COLUMN_LABELS_INDEX, parseNonnegativeInteger(columnLabelsIndexTF));
        parameters.set(COLUMN_SELECTION, columnSelection);
        parameters.set(TIMEDATA_COLUMN_INDEX, timedataColumnIndex);
        parameters.setParameterActive(true);
    }

    public void setUpActionsColumn(SteppedComboBox comboBox,
                                   TableColumn actionsColumn)
    {
        actionsColumn.setCellEditor(new DefaultCellEditor(comboBox));
        DefaultTableCellRenderer renderer
            = new DefaultTableCellRenderer();
        actionsColumn.setCellRenderer(renderer);
    }

    private void importColumnSelectAll()
    {
        importColumnSelectionInProgress = true;
        boolean change = false;
        for (int i = 0; i < actionTable.getModel().getRowCount(); i++) {
            if (!(Boolean) actionTable.getModel().getValueAt(i, 3))
                change = true;
            actionTable.getModel().setValueAt(true, i, 3);
        }
        importColumnSelectionInProgress = false;
        if (change)
            ((DefaultTableModel) actionTable.getModel()).fireTableChanged(null);
    }

    private void importColumnUnselectAll()
    {
        importColumnSelectionInProgress = true;
        boolean change = false;
        for (int i = 0; i < actionTable.getModel().getRowCount(); i++) {
            if ((Boolean) actionTable.getModel().getValueAt(i, 3))
                change = true;
            actionTable.getModel().setValueAt(false, i, 3);
        }
        importColumnSelectionInProgress = false;
        if (change)
            ((DefaultTableModel) actionTable.getModel()).fireTableChanged(null);
    }

    private void importColumnInvert()
    {
        importColumnSelectionInProgress = true;
        for (int i = 0; i < actionTable.getModel().getRowCount(); i++) {
            actionTable.getModel().setValueAt(!(Boolean) actionTable.getModel().getValueAt(i, 3), i, 3);
        }
        importColumnSelectionInProgress = false;
        ((DefaultTableModel) actionTable.getModel()).fireTableChanged(null);
    }

    private void timeDataColumnChangeAction()
    {
        importColumnSelectionInProgress = true;
        if (parameters.get(TIMEDATA_COLUMN_INDEX) >= 0) {
            for (int i = 0; i < actionTable.getModel().getRowCount(); i++) {
                if (i != parameters.get(TIMEDATA_COLUMN_INDEX) && ((Boolean) actionTable.getModel().getValueAt(i, 4)) == true) {
                    actionTable.getModel().setValueAt(false, parameters.get(TIMEDATA_COLUMN_INDEX), 4);
                    importColumnSelectionInProgress = false;
                    return;
                }
            }
        }
        importColumnSelectionInProgress = false;

    }

    private void readFirstTwoRowsAndUpdateGUI()
    {
        List<String[]> firstTwoRows = CsvFieldReader.readFirstTwoRows(filePathTF.getText(), autoCB.isSelected() ? null : delimiterTF.getText(), parseNonnegativeInteger(rowsToSkipTF), headerCB.isSelected());
        if (firstTwoRows != null) {
            parameters.setParameterActive(false);
            parameters.set(FILENAME, filePathTF.getText());
            parameters.set(HEADER_NAMES, firstTwoRows.get(0));
            parameters.set(FIRST_ROW, firstTwoRows.get(1));
            int length = firstTwoRows.get(0).length;
            int[] actions = null;
            if (parameters.get(ACTIONS_1D).length != length) {
                actions = new int[length];
            }
            else {
                actions = parameters.get(ACTIONS_1D);
            }
            Arrays.fill(actions, ReadCSVShared.FLOAT_WITH_NAN);
            parameters.set(ACTIONS_1D, actions);
            if (parameters.get(COLUMN_SELECTION).length != length) {
                boolean[] columnSelection = new boolean[length];
                Arrays.fill(columnSelection, true);
                parameters.set(COLUMN_SELECTION, columnSelection);
            }
            if (firstTwoRows.size() == 3) {
                parameters.set(FIELD_DELIMITER, firstTwoRows.get(2)[0]);
            }
            parameters.setParameterActive(true);
            updateGUI(parameters.getReadOnlyClone(), true, true);
        } else {
            rowsToSkipTF.setText("0");
        }
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        browsePanel = new javax.swing.JPanel();
        filePathTF = new org.visnow.vn.gui.swingwrappers.TextField();
        browseButton = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        customSizePanel1 = new org.visnow.vn.system.swing.CustomSizePanel();
        delimiterTF = new org.visnow.vn.gui.swingwrappers.TextField();
        autoCB = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        customSizePanel2 = new org.visnow.vn.system.swing.CustomSizePanel();
        rowsToSkipTF = new org.visnow.vn.gui.swingwrappers.TextField();
        runButton1 = new org.visnow.vn.gui.widgets.RunButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        headerCB = new javax.swing.JCheckBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        actionTable = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        customSizePanel3 = new org.visnow.vn.system.swing.CustomSizePanel();
        columnsToSkipTF = new org.visnow.vn.gui.swingwrappers.TextField();
        jPanel6 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        action_2DCB = new org.visnow.vn.gui.widgets.SteppedComboBox();
        jPanel7 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        customSizePanel4 = new org.visnow.vn.system.swing.CustomSizePanel();
        rowLabelsIndexTF = new org.visnow.vn.gui.swingwrappers.TextField();
        jPanel8 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        customSizePanel5 = new org.visnow.vn.system.swing.CustomSizePanel();
        columnLabelsIndexTF = new org.visnow.vn.gui.swingwrappers.TextField();
        filler3 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        setBorder(javax.swing.BorderFactory.createEmptyBorder(4, 4, 4, 4));
        setLayout(new java.awt.GridBagLayout());

        browsePanel.setLayout(new java.awt.GridBagLayout());

        filePathTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                filePathTFUserAction(evt);
            }
        });
        filePathTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectAllOnFocus(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        browsePanel.add(filePathTF, gridBagConstraints);

        browseButton.setText("Browse...");
        browseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        browsePanel.add(browseButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(browsePanel, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel2.setText("Field separator:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        jPanel2.add(jLabel2, gridBagConstraints);

        customSizePanel1.setCustomWidth(40);
        customSizePanel1.setOverrideMinSize(true);
        customSizePanel1.setLayout(new java.awt.GridBagLayout());

        delimiterTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        delimiterTF.setEnabled(false);
        delimiterTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                delimiterTFUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        delimiterTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectAllOnFocus(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        customSizePanel1.add(delimiterTF, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        jPanel2.add(customSizePanel1, gridBagConstraints);

        autoCB.setSelected(true);
        autoCB.setText("auto");
        autoCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                autoCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel2.add(autoCB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(jPanel2, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel1.setText("Number of rows ro skip:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel1.add(jLabel1, gridBagConstraints);

        customSizePanel2.setCustomWidth(40);
        customSizePanel2.setOverrideMinSize(true);
        customSizePanel2.setLayout(new java.awt.GridBagLayout());

        rowsToSkipTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        rowsToSkipTF.setText("0");
        rowsToSkipTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                rowsToSkipTFUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        rowsToSkipTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                rowsToSkipTFselectAllOnFocus(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        customSizePanel2.add(rowsToSkipTF, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel1.add(customSizePanel2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(jPanel1, gridBagConstraints);

        runButton1.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                runButton1UserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(runButton1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.weighty = 1.0;
        add(filler1, gridBagConstraints);

        jPanel3.setLayout(new java.awt.GridBagLayout());

        headerCB.setText("Has header line");
        headerCB.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        headerCB.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        headerCB.setMargin(new java.awt.Insets(0, 0, 0, 0));
        headerCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                headerCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanel3.add(headerCB, gridBagConstraints);

        jScrollPane1.setMinimumSize(new java.awt.Dimension(300, 400));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(300, 400));

        actionTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null}
            },
            new String [] {
                "header", "first row", "action", "import", "timedata"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Boolean.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(actionTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanel3.add(jScrollPane1, gridBagConstraints);

        jTabbedPane1.addTab("1D", jPanel3);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        jPanel5.setLayout(new java.awt.GridBagLayout());

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel3.setText("Number of columns to skip:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel5.add(jLabel3, gridBagConstraints);

        customSizePanel3.setCustomWidth(40);
        customSizePanel3.setOverrideMinSize(true);
        customSizePanel3.setLayout(new java.awt.GridBagLayout());

        columnsToSkipTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        columnsToSkipTF.setText("0");
        columnsToSkipTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                columnsToSkipTFUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        columnsToSkipTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                columnsToSkipTFselectAllOnFocus(evt);
            }
        });
        columnsToSkipTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                columnsToSkipTFActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        customSizePanel3.add(columnsToSkipTF, gridBagConstraints);

        jPanel5.add(customSizePanel3, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanel4.add(jPanel5, gridBagConstraints);

        jPanel6.setLayout(new java.awt.GridBagLayout());

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel4.setText("Action:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanel6.add(jLabel4, gridBagConstraints);

        action_2DCB.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                action_2DCBItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel6.add(action_2DCB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanel4.add(jPanel6, gridBagConstraints);

        jPanel7.setLayout(new java.awt.GridBagLayout());

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel5.setText("Index of row labels:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel7.add(jLabel5, gridBagConstraints);

        customSizePanel4.setCustomWidth(40);
        customSizePanel4.setOverrideMinSize(true);
        customSizePanel4.setLayout(new java.awt.GridBagLayout());

        rowLabelsIndexTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        rowLabelsIndexTF.setText("0");
        rowLabelsIndexTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                rowLabelsIndexTFUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        rowLabelsIndexTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                rowLabelsIndexTFselectAllOnFocus(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        customSizePanel4.add(rowLabelsIndexTF, gridBagConstraints);

        jPanel7.add(customSizePanel4, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanel4.add(jPanel7, gridBagConstraints);

        jPanel8.setLayout(new java.awt.GridBagLayout());

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel6.setText("Index of column labels:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel8.add(jLabel6, gridBagConstraints);

        customSizePanel5.setCustomWidth(40);
        customSizePanel5.setOverrideMinSize(true);
        customSizePanel5.setLayout(new java.awt.GridBagLayout());

        columnLabelsIndexTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        columnLabelsIndexTF.setText("0");
        columnLabelsIndexTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                columnLabelsIndexTFUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        columnLabelsIndexTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                columnLabelsIndexTFselectAllOnFocus(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        customSizePanel5.add(columnLabelsIndexTF, gridBagConstraints);

        jPanel8.add(customSizePanel5, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanel4.add(jPanel8, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 4;
        gridBagConstraints.weighty = 1.0;
        jPanel4.add(filler3, gridBagConstraints);

        jTabbedPane1.addTab("2D", jPanel4);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(jTabbedPane1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void browseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseButtonActionPerformed
        fileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(this.getClass())));

        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            String fileName = fileChooser.getSelectedFile().getAbsolutePath();
            filePathTF.setText(fileName);
            VisNow.get().getMainConfig().setLastDataPath(fileName.substring(0, fileName.lastIndexOf(File.separator)), this.getClass());
            readFirstTwoRowsAndUpdateGUI();
        }
    }//GEN-LAST:event_browseButtonActionPerformed

    private void headerCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_headerCBActionPerformed
        parameters.setParameterActive(false);
        parameters.set(HAS_HEADER_LINE, headerCB.isSelected());
        parameters.setParameterActive(true);
        readFirstTwoRowsAndUpdateGUI();
    }//GEN-LAST:event_headerCBActionPerformed

    private void delimiterTFUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_delimiterTFUserChangeAction
    {//GEN-HEADEREND:event_delimiterTFUserChangeAction
        parameters.setParameterActive(false);
        parameters.set(FIELD_DELIMITER, delimiterTF.getText());
        parameters.setParameterActive(true);
        readFirstTwoRowsAndUpdateGUI();
    }//GEN-LAST:event_delimiterTFUserChangeAction

    private void selectAllOnFocus(java.awt.event.FocusEvent evt)//GEN-FIRST:event_selectAllOnFocus
    {//GEN-HEADEREND:event_selectAllOnFocus
        ((TextField) evt.getSource()).selectAll();
    }//GEN-LAST:event_selectAllOnFocus

    private void filePathTFUserAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_filePathTFUserAction
    {//GEN-HEADEREND:event_filePathTFUserAction
        if (evt.getEventType() == TextField.EVENT_CHANGE_VALUE || evt.getEventType() == TextField.EVENT_NO_CHANGE_ENTER_KEY) {
            String fileName = filePathTF.getText();
            VisNow.get().getMainConfig().setLastDataPath(fileName.substring(0, fileName.lastIndexOf(File.separator)), this.getClass());
            readFirstTwoRowsAndUpdateGUI();
        }
    }//GEN-LAST:event_filePathTFUserAction

    private void runButton1UserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_runButton1UserChangeAction
    {//GEN-HEADEREND:event_runButton1UserChangeAction
        updateParameters();
        parameters.set(RUNNING_MESSAGE, (RunButton.RunState) evt.getEventData());
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_runButton1UserChangeAction

    private void autoCBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_autoCBActionPerformed
    {//GEN-HEADEREND:event_autoCBActionPerformed
        if (autoCB.isSelected()) {
            delimiterTF.setEnabled(false);
            parameters.setParameterActive(false);
            parameters.set(FIELD_DELIMITER, null);
            parameters.setParameterActive(true);
            readFirstTwoRowsAndUpdateGUI();
        } else {
            delimiterTF.setEnabled(true);
        }

    }//GEN-LAST:event_autoCBActionPerformed

    private int parseNonnegativeInteger(TextField tf)
    {
        int n;
        try {
            n = Integer.parseInt(tf.getText());
        } catch (NumberFormatException ex) {
            tf.setText("0");
            n = 0;
        }
        if (n < 0) {
            tf.setText("0");
            n = 0;
        }
        return n;
    }

    private void rowsToSkipTFUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_rowsToSkipTFUserChangeAction
    {//GEN-HEADEREND:event_rowsToSkipTFUserChangeAction
        parameters.setParameterActive(false);
        parameters.set(NUMBER_OF_ROWS_TO_SKIP, parseNonnegativeInteger(rowsToSkipTF));
        parameters.setParameterActive(true);
        readFirstTwoRowsAndUpdateGUI();
    }//GEN-LAST:event_rowsToSkipTFUserChangeAction

    private void rowsToSkipTFselectAllOnFocus(java.awt.event.FocusEvent evt)//GEN-FIRST:event_rowsToSkipTFselectAllOnFocus
    {//GEN-HEADEREND:event_rowsToSkipTFselectAllOnFocus
        ((TextField) evt.getSource()).selectAll();
    }//GEN-LAST:event_rowsToSkipTFselectAllOnFocus

    private void columnsToSkipTFUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_columnsToSkipTFUserChangeAction
    {//GEN-HEADEREND:event_columnsToSkipTFUserChangeAction

        parameters.setParameterActive(false);
        parameters.set(NUMBER_OF_COLUMNS_TO_SKIP, parseNonnegativeInteger(columnsToSkipTF));
        parameters.setParameterActive(true);
    }//GEN-LAST:event_columnsToSkipTFUserChangeAction

    private void columnsToSkipTFselectAllOnFocus(java.awt.event.FocusEvent evt)//GEN-FIRST:event_columnsToSkipTFselectAllOnFocus
    {//GEN-HEADEREND:event_columnsToSkipTFselectAllOnFocus
        ((TextField) evt.getSource()).selectAll();
    }//GEN-LAST:event_columnsToSkipTFselectAllOnFocus

    private void rowLabelsIndexTFUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_rowLabelsIndexTFUserChangeAction
    {//GEN-HEADEREND:event_rowLabelsIndexTFUserChangeAction
        
        parameters.setParameterActive(false);
        parameters.set(ROW_LABELS_INDEX, parseNonnegativeInteger(rowLabelsIndexTF));
        parameters.setParameterActive(true);
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_rowLabelsIndexTFUserChangeAction

    private void rowLabelsIndexTFselectAllOnFocus(java.awt.event.FocusEvent evt)//GEN-FIRST:event_rowLabelsIndexTFselectAllOnFocus
    {//GEN-HEADEREND:event_rowLabelsIndexTFselectAllOnFocus
        ((TextField) evt.getSource()).selectAll();
    }//GEN-LAST:event_rowLabelsIndexTFselectAllOnFocus

    private void columnLabelsIndexTFUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_columnLabelsIndexTFUserChangeAction
    {//GEN-HEADEREND:event_columnLabelsIndexTFUserChangeAction

        parameters.setParameterActive(false);
        parameters.set(COLUMN_LABELS_INDEX, parseNonnegativeInteger(columnLabelsIndexTF));
        parameters.setParameterActive(true);
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_columnLabelsIndexTFUserChangeAction

    private void columnLabelsIndexTFselectAllOnFocus(java.awt.event.FocusEvent evt)//GEN-FIRST:event_columnLabelsIndexTFselectAllOnFocus
    {//GEN-HEADEREND:event_columnLabelsIndexTFselectAllOnFocus
        ((TextField) evt.getSource()).selectAll();
    }//GEN-LAST:event_columnLabelsIndexTFselectAllOnFocus

    private void action_2DCBItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_action_2DCBItemStateChanged
    {//GEN-HEADEREND:event_action_2DCBItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED && parameters != null) {
            parameters.setParameterActive(false);
            parameters.set(ACTION_2D, ReadCSVShared.ACTION_CODES[action_2DCB.getSelectedIndex()]);
            parameters.setParameterActive(true);
            runButton1.setPendingIfNoAuto();
        }
    }//GEN-LAST:event_action_2DCBItemStateChanged

    private void columnsToSkipTFActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_columnsToSkipTFActionPerformed
    {//GEN-HEADEREND:event_columnsToSkipTFActionPerformed
        parameters.setParameterActive(false);
        parameters.set(ACTION_2D, ReadCSVShared.ACTION_CODES[action_2DCB.getSelectedIndex()]);
        parameters.setParameterActive(true);
        runButton1.setPendingIfNoAuto();
    }//GEN-LAST:event_columnsToSkipTFActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable actionTable;
    private org.visnow.vn.gui.widgets.SteppedComboBox action_2DCB;
    private javax.swing.JCheckBox autoCB;
    private javax.swing.JButton browseButton;
    private javax.swing.JPanel browsePanel;
    private org.visnow.vn.gui.swingwrappers.TextField columnLabelsIndexTF;
    private org.visnow.vn.gui.swingwrappers.TextField columnsToSkipTF;
    private org.visnow.vn.system.swing.CustomSizePanel customSizePanel1;
    private org.visnow.vn.system.swing.CustomSizePanel customSizePanel2;
    private org.visnow.vn.system.swing.CustomSizePanel customSizePanel3;
    private org.visnow.vn.system.swing.CustomSizePanel customSizePanel4;
    private org.visnow.vn.system.swing.CustomSizePanel customSizePanel5;
    private org.visnow.vn.gui.swingwrappers.TextField delimiterTF;
    private org.visnow.vn.gui.swingwrappers.TextField filePathTF;
    private javax.swing.Box.Filler filler1;
    private javax.swing.Box.Filler filler3;
    private javax.swing.JCheckBox headerCB;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private org.visnow.vn.gui.swingwrappers.TextField rowLabelsIndexTF;
    private org.visnow.vn.gui.swingwrappers.TextField rowsToSkipTF;
    private org.visnow.vn.gui.widgets.RunButton runButton1;
    // End of variables declaration//GEN-END:variables
    void activateOpenDialog()
    {
        browseButtonActionPerformed(null);
    }
}
