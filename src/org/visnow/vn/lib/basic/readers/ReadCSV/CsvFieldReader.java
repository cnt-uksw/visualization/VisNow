/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
package org.visnow.vn.lib.basic.readers.ReadCSV;

import com.univocity.parsers.common.processor.ObjectColumnProcessor;
import com.univocity.parsers.conversions.ByteConversion;
import com.univocity.parsers.conversions.DoubleConversion;
import com.univocity.parsers.conversions.FloatConversion;
import com.univocity.parsers.conversions.IntegerConversion;
import com.univocity.parsers.conversions.LongConversion;
import com.univocity.parsers.conversions.ObjectConversion;
import com.univocity.parsers.conversions.RegexConversion;
import com.univocity.parsers.conversions.ShortConversion;
import com.univocity.parsers.conversions.ToStringConversion;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import com.univocity.parsers.tsv.TsvParser;
import com.univocity.parsers.tsv.TsvParserSettings;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.complex.ComplexFormat;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.LongLargeArray;
import org.visnow.jlargearrays.ShortLargeArray;
import org.visnow.jlargearrays.StringLargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.utils.FloatingPointUtils;
import org.visnow.jscic.utils.NaNAction;
import org.visnow.vn.lib.utils.StringUtils;

/**
 * Reads field components from CSV and TSV columns.
 *
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class CsvFieldReader
{

    private static final String NULL_STRING_VALUE = "_NULL_";
    private static final String EMPTY_STRING_VALUE = "_EMPTY_";

    private static CsvParserSettings generateCsvSettings(String fieldDelimiter, int numberOfRowsToSkip, boolean headersLine, String emptyValue)
    {
        CsvParserSettings parserSettings = new CsvParserSettings();
        parserSettings.setLineSeparatorDetectionEnabled(true);
        parserSettings.setQuoteDetectionEnabled(true);
        if (fieldDelimiter == null || fieldDelimiter.isEmpty()) {
            parserSettings.setDelimiterDetectionEnabled(true, new char[] {',', ';'});
        } else {
            parserSettings.getFormat().setDelimiter(fieldDelimiter);
        }
        if (numberOfRowsToSkip > 0) {
            parserSettings.setNumberOfRowsToSkip(numberOfRowsToSkip);
        }
        parserSettings.setHeaderExtractionEnabled(headersLine);
        if (emptyValue != null && !emptyValue.isEmpty()) {
            parserSettings.setEmptyValue(emptyValue);
        }
        return parserSettings;
    }

    private static TsvParserSettings generateTsvSettings(int numberOfRowsToSkip, boolean headersLine)
    {
        TsvParserSettings parserSettings = new TsvParserSettings();
        parserSettings.setLineSeparatorDetectionEnabled(true);
        if (numberOfRowsToSkip > 0) {
            parserSettings.setNumberOfRowsToSkip(numberOfRowsToSkip);
        }
        parserSettings.setHeaderExtractionEnabled(headersLine);
        return parserSettings;
    }

    private static String[] replaceNullEmptyHeaders(String[] headers, String nullValue, String emptyValue)
    {
        if (headers != null && nullValue != null && emptyValue != null) {
            for (int j = 0; j < headers.length; j++) {
                if (headers[j] == null) {
                    headers[j] = nullValue;
                } else if (headers[j].isEmpty()) {
                    headers[j] = emptyValue;
                }
            }
        }
        return headers;
    }

    /**
     * Reads the first two rows from CSV and TSV files.
     *
     * @param filepath           file path, cannot be null or empty
     * @param fieldDelimiter     field delimeter, if null or empty, then it is determined automatically and returned as the third element of the result
     * @param numberOfRowsToSkip number of rows to skip
     * @param headersLine        if true, then the first non-commented row in a file is used as a header line, otherwise the header line is generated using
     *                           <code>generateHeaderNames</code>
     *
     *
     * @return {header line, first row} or {header line, first row, field delimeter}
     */
    public static List<String[]> readFirstTwoRows(String filepath, String fieldDelimiter, int numberOfRowsToSkip, boolean headersLine)
    {
        if (filepath == null || filepath.isEmpty())
            return null;

        List<String[]> result;

        if (fieldDelimiter != null && fieldDelimiter.equals("\t")) {
            result = readTsvFirstTwoRows(filepath, fieldDelimiter, numberOfRowsToSkip, headersLine);
        } else if (fieldDelimiter != null && !fieldDelimiter.isEmpty()) {
            result = readCsvFirstTwoRows(filepath, fieldDelimiter, numberOfRowsToSkip, headersLine);
        } else {
            //Try reading as TSV first
            result = readTsvFirstTwoRows(filepath, fieldDelimiter, numberOfRowsToSkip, headersLine);
            // Try reading as CSV
            if (result == null || result.get(0).length <= 1) {
                result = readCsvFirstTwoRows(filepath, fieldDelimiter, numberOfRowsToSkip, headersLine);
            }
        }
        return result;
    }

    private static List<String[]> readTsvFirstTwoRows(String filepath, String fieldDelimiter, int numberOfRowsToSkip, boolean headersLine)
    {
        try {
            List<String[]> result = new ArrayList<>(3);

            TsvParserSettings tsvParserSettings = generateTsvSettings(numberOfRowsToSkip, false);
            TsvParser tsvParser = new TsvParser(tsvParserSettings);
            tsvParser.beginParsing(new File(filepath));
            String[] row = tsvParser.parseNext();
            if (row == null) {
                return null;
            }
            if (headersLine == false) {
                result.add(StringUtils.generateExcelColumnNames(row.length, false, 0));
                result.add(replaceNullEmptyHeaders(row, NULL_STRING_VALUE, EMPTY_STRING_VALUE));
            } else {
                result.add(row);
                row = tsvParser.parseNext();
                if (row == null) {
                    return null;
                }
                result.add(replaceNullEmptyHeaders(row, NULL_STRING_VALUE, EMPTY_STRING_VALUE));
            }
            tsvParser.stopParsing();
            if (fieldDelimiter == null || fieldDelimiter.isEmpty()) {
                result.add(new String[]{"\t"});
            }
            return result;
        } catch (Exception ex) {
            return null;
        }
    }

    private static List<String[]> readCsvFirstTwoRows(String filepath, String fieldDelimiter, int numberOfRowsToSkip, boolean headersLine)
    {
        try {

            List<String[]> result = new ArrayList<>(3);
            CsvParserSettings csvParserSettings = generateCsvSettings(fieldDelimiter, numberOfRowsToSkip, false, EMPTY_STRING_VALUE);
            CsvParser csvParser = new CsvParser(csvParserSettings);
            csvParser.beginParsing(new File(filepath));
            String[] row = csvParser.parseNext();
            if (row == null) {
                return null;
            }
            if (headersLine == false) {
                result.add(StringUtils.generateExcelColumnNames(row.length, false, 0));
                result.add(replaceNullEmptyHeaders(row, NULL_STRING_VALUE, EMPTY_STRING_VALUE));
            } else {
                result.add(row);
                row = csvParser.parseNext();
                if (row == null) {
                    return null;
                }
                result.add(replaceNullEmptyHeaders(row, NULL_STRING_VALUE, EMPTY_STRING_VALUE));
            }
            csvParser.stopParsing();
            if (fieldDelimiter == null || fieldDelimiter.isEmpty()) {
                result.add(new String[]{csvParser.getDetectedFormat().getDelimiterString()});
            }
            return result;
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Reads selected row from CSV and TSV files.
     *
     * @param filepath       file path, cannot be null or empty
     * @param fieldDelimiter field delimeter, if null or empty, then it is determined automatically
     * @param rowIndex       index of the row to read
     *
     *
     * @return selected row
     */
    public static String[] readSelectedRow(String filepath, String fieldDelimiter, int rowIndex)
    {
        if (filepath == null || filepath.isEmpty())
            return null;

        String[] result;

        if (fieldDelimiter != null && fieldDelimiter.equals("\t")) {
            result = readTsvSelectedRow(filepath, rowIndex);
        } else if (fieldDelimiter != null && !fieldDelimiter.isEmpty()) {
            result = readCsvSelectedRow(filepath, fieldDelimiter, rowIndex);
        } else {
            //Try reading as TSV first
            result = readTsvSelectedRow(filepath, rowIndex);
            // Try reading as CSV
            if (result == null || result.length <= 1) {
                result = readCsvSelectedRow(filepath, fieldDelimiter, rowIndex);
            }
        }
        return result;
    }

    private static String[] readTsvSelectedRow(String filepath, int rowIndex)
    {
        try {
            TsvParserSettings tsvParserSettings = generateTsvSettings(Math.max(0, rowIndex), false);
            TsvParser tsvParser = new TsvParser(tsvParserSettings);
            tsvParser.beginParsing(new File(filepath));
            String[] row = tsvParser.parseNext();
            tsvParser.stopParsing();
            if (row == null) {
                return null;
            }
            return replaceNullEmptyHeaders(row, NULL_STRING_VALUE, EMPTY_STRING_VALUE);
        } catch (Exception ex) {
            return null;
        }
    }

    private static String[] readCsvSelectedRow(String filepath, String fieldDelimiter, int rowIndex)
    {
        try {
            CsvParserSettings csvParserSettings = generateCsvSettings(fieldDelimiter, Math.max(0, rowIndex), false, EMPTY_STRING_VALUE);
            CsvParser csvParser = new CsvParser(csvParserSettings);
            csvParser.beginParsing(new File(filepath));
            String[] row = csvParser.parseNext();
            csvParser.stopParsing();
            if (row == null) {
                return null;
            }
            return replaceNullEmptyHeaders(row, NULL_STRING_VALUE, EMPTY_STRING_VALUE);
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Reads selected column from CSV and TSV files.
     *
     * @param filepath       file path, cannot be null or empty
     * @param fieldDelimiter field delimeter, if null or empty, then it is determined automatically
     * @param columnIndex    index of the column to read
     *
     *
     * @return selected column
     */
    public static String[] readSelectedColumn(String filepath, String fieldDelimiter, int columnIndex)
    {
        if (filepath == null || filepath.isEmpty())
            return null;

        String[] result;

        if (fieldDelimiter != null && fieldDelimiter.equals("\t")) {
            result = readTsvSelectedColumn(filepath, columnIndex);
        } else if (fieldDelimiter != null && !fieldDelimiter.isEmpty()) {
            result = readCsvSelectedColumn(filepath, fieldDelimiter, columnIndex);
        } else {
            //Try reading as TSV first
            result = readTsvSelectedColumn(filepath, columnIndex);
            // Try reading as CSV
            if (result == null || result.length <= 1) {
                result = readCsvSelectedColumn(filepath, fieldDelimiter, columnIndex);
            }
        }
        return result;
    }

    private static String[] readTsvSelectedColumn(String filepath, int columnIndex)
    {
        try {
            TsvParserSettings tsvParserSettings = generateTsvSettings(0, false);
            ObjectColumnProcessor columnProcessor = new ObjectColumnProcessor();
            tsvParserSettings.selectIndexes(new Integer[]{columnIndex});
            TsvParser tsvParser = new TsvParser(tsvParserSettings);
            tsvParser.parse(new File(filepath));
            return replaceNullEmptyHeaders(columnProcessor.getColumn(0, String.class).toArray(new String[0]), NULL_STRING_VALUE, EMPTY_STRING_VALUE);
        } catch (Exception ex) {
            return null;
        }
    }

    private static String[] readCsvSelectedColumn(String filepath, String fieldDelimiter, int columnIndex)
    {
        try {
            CsvParserSettings csvParserSettings = generateCsvSettings(fieldDelimiter, 0, false, EMPTY_STRING_VALUE);
            ObjectColumnProcessor columnProcessor = new ObjectColumnProcessor();
            csvParserSettings.selectIndexes(new Integer[]{columnIndex});
            csvParserSettings.setProcessor(columnProcessor);
            CsvParser csvParser = new CsvParser(csvParserSettings);
            csvParser.parse(new File(filepath));
            return replaceNullEmptyHeaders(columnProcessor.getColumn(0, String.class).toArray(new String[0]), NULL_STRING_VALUE, EMPTY_STRING_VALUE);
        } catch (Exception ex) {
            return null;
        }
    }

    private static LargeArray padWithEmpty(StringLargeArray la, long paddedLength, String emptyValue)
    {
        if (la == null) {
            return null;
        }
        if (la.length() == paddedLength) {
            return la;
        } else if (la.length() < paddedLength) {
            LargeArray paddedLa = new StringLargeArray(paddedLength, LargeArray.DEFAULT_MAX_STRING_LENGTH, false);
            LargeArrayUtils.arraycopy(la, 0, paddedLa, 0, la.length());
            for (long i = la.length(); i < paddedLength; i++) {
                paddedLa.set(i, emptyValue);
            }
            return paddedLa;
        } else {
            LargeArray paddedLa = new StringLargeArray(paddedLength, LargeArray.DEFAULT_MAX_STRING_LENGTH, false);
            LargeArrayUtils.arraycopy(la, 0, paddedLa, 0, paddedLa.length());
            return paddedLa;
        }
    }

    private static LargeArray padWithZero(LargeArray la, long paddedLength)
    {
        if (la == null) {
            return null;
        }
        if (!la.isNumeric()) {
            throw new IllegalArgumentException("Expected LargeArray of numberic type.");
        }
        if (la.length() == paddedLength) {
            return la;
        } else if (la.length() < paddedLength) {
            LargeArray paddedLa = LargeArrayUtils.create(la.getType(), paddedLength, false);
            LargeArrayUtils.arraycopy(la, 0, paddedLa, 0, la.length());
            for (long i = la.length(); i < paddedLength; i++) {
                paddedLa.set(i, 0);
            }
            return paddedLa;
        } else {
            LargeArray paddedLa = LargeArrayUtils.create(la.getType(), paddedLength, false);
            LargeArrayUtils.arraycopy(la, 0, paddedLa, 0, paddedLa.length());
            return paddedLa;
        }
    }

    /**
     * Reads and parses columns from CSV and TSV files and converts them to components of 1D RegularField.
     *
     * @param filepath            file path, cannot be null or empty
     * @param fieldDelimiter      field delimeter, cannot be null or empty
     * @param numberOfRowsToSkip  number of rows to skip
     * @param headersLine         if true, then the first non-commented row in a file is used as a header line, otherwise the header line is generated using
     *                            <code>generateHeaderNames</code>
     * @param actions             list of actions performed on each column during parsing, cannot be null
     * @param columnSelection     list of columns to be parsed, cannot be null
     * @param timedataColumnIndex index of a column that is used to generate time data
     *
     * @return 1D RegularField containing columns of TSV and CSV files as components
     */
    public static RegularField readCsvField1D(String filepath, String fieldDelimiter, int numberOfRowsToSkip, boolean headersLine, int[] actions, boolean[] columnSelection, int timedataColumnIndex)
    {
        if (filepath == null || filepath.isEmpty() || fieldDelimiter == null || fieldDelimiter.isEmpty() || actions == null || columnSelection == null || actions.length != columnSelection.length)
            return null;

        RegularField field = null;
        long[] dims = new long[1];
        int nColumns = actions.length;
        int nSelectedColumns = 0;
        List<Integer> columnSelectionList = new ArrayList<>();
        List<Integer> actionsList = new ArrayList<>();
        int selectedTimedataColumnIndex = -1;
        boolean importTimedataColumn = true;
        CsvParserSettings csvParserSettings = null;
        TsvParserSettings tsvParserSettings = null;
        String noDigitsRegex = "^(?!(\\-|\\+)?\\d+(\\.|\\,)?\\d*).*$";

        if (fieldDelimiter.equals("\t")) {
            tsvParserSettings = generateTsvSettings(numberOfRowsToSkip, headersLine);
        } else {
            csvParserSettings = generateCsvSettings(fieldDelimiter, numberOfRowsToSkip, headersLine, EMPTY_STRING_VALUE);
        }
        ObjectColumnProcessor columnProcessor = new ObjectColumnProcessor();
        for (int i = 0; i < nColumns; i++) {
            if (columnSelection[i] == true || i == timedataColumnIndex) {
                columnSelectionList.add(i);
                actionsList.add(actions[i]);
                if (actions[i] == ReadCSVShared.NOOP) {
                    columnProcessor.convertIndexes(new ToStringConversion(NULL_STRING_VALUE, NULL_STRING_VALUE)).set(i);
                } else if (actions[i] == ReadCSVShared.LOGIC) {
                    columnProcessor.convertIndexes(new ByteConversion((byte) 0, "0")).set(i);
                } else if (actions[i] == ReadCSVShared.BYTE || actions[i] == ReadCSVShared.SHORT) {
                    columnProcessor.convertIndexes(new ShortConversion((short) 0, "0")).set(i);
                } else if (actions[i] == ReadCSVShared.INT) {
                    columnProcessor.convertIndexes(new IntegerConversion(0, "0")).set(i);
                } else if (actions[i] == ReadCSVShared.LONG) {
                    columnProcessor.convertIndexes(new LongConversion(0l, "0")).set(i);
                } else if (actions[i] == ReadCSVShared.FLOAT) {
                    columnProcessor.convertIndexes(new FloatConversion(0f, "0.0")).set(i);
                } else if (actions[i] == ReadCSVShared.DOUBLE) {
                    columnProcessor.convertIndexes(new DoubleConversion(0.0, "0.0")).set(i);
                } else if (actions[i] == ReadCSVShared.COMPLEX) {
                    columnProcessor.convertIndexes(new ComplexConversion(Complex.ZERO, Complex.ZERO.toString())).set(i);
                } else if (actions[i] == ReadCSVShared.FLOAT_WITH_NAN) {
                    if (FloatingPointUtils.defaultNanAction == NaNAction.EXCEPTION_AT_NAN) {
                        columnProcessor.convertIndexes(new FloatConversion(0f, "0.0")).set(i);
                    } else {
                        columnProcessor.convertIndexes(new RegexConversion(noDigitsRegex, Float.toString(Float.NaN)), new FloatConversion(0f, "0.0")).set(i);
                    }
                } else if (actions[i] == ReadCSVShared.DOUBLE_WITH_NAN) {
                    if (FloatingPointUtils.defaultNanAction == NaNAction.EXCEPTION_AT_NAN) {
                        columnProcessor.convertIndexes(new DoubleConversion(0.0, "0.0")).set(i);
                    } else {
                        columnProcessor.convertIndexes(new RegexConversion(noDigitsRegex, Double.toString(Double.NaN)), new DoubleConversion(0.0, "0.0")).set(i);
                    }
                } else if (actions[i] == ReadCSVShared.COMPLEX_WITH_NAN) {
                    if (FloatingPointUtils.defaultNanAction == NaNAction.EXCEPTION_AT_NAN) {
                        columnProcessor.convertIndexes(new ComplexConversion(Complex.ZERO, Complex.ZERO.toString())).set(i);
                    } else {
                        columnProcessor.convertIndexes(new RegexConversion(noDigitsRegex, Complex.NaN.toString()), new ComplexConversion(Complex.ZERO, Complex.ZERO.toString())).set(i);
                    }
                } else {
                    throw new IllegalArgumentException("Invalid action " + actions[i]);
                }
                if (i == timedataColumnIndex && columnSelection[i] == false) {
                    importTimedataColumn = false;
                }
            }
        }
        if (timedataColumnIndex >= 0) {
            selectedTimedataColumnIndex = columnSelectionList.indexOf(timedataColumnIndex);
        }
        try {
            if (csvParserSettings != null) {
                csvParserSettings.selectIndexes(columnSelectionList.toArray(new Integer[0]));
                csvParserSettings.setProcessor(columnProcessor);
                CsvParser parser = new CsvParser(csvParserSettings);
                parser.parse(new File(filepath));
            } else {
                tsvParserSettings.selectIndexes(columnSelectionList.toArray(new Integer[0]));
                tsvParserSettings.setProcessor(columnProcessor);
                TsvParser parser = new TsvParser(tsvParserSettings);
                parser.parse(new File(filepath));
            }
        } catch (Exception ex) {
            throw new IllegalArgumentException("Parsing error: " + ex.getMessage());
        }

        List<String> headerList;
        if (headersLine == true) {
            headerList = new ArrayList<>(Arrays.asList(replaceNullEmptyHeaders(columnProcessor.getHeaders(), NULL_STRING_VALUE, EMPTY_STRING_VALUE)));
        } else {
            String[] headers = StringUtils.generateExcelColumnNames(nColumns, false, 0);
            if (columnSelectionList.size() != headers.length) {
                headerList = new ArrayList<>(columnSelectionList.size());
                for (int i = 0; i < nColumns; i++) {
                    if (columnSelectionList.contains(i)) {
                        headerList.add(headers[i]);
                    }
                }
            } else {
                headerList = new ArrayList<>(Arrays.asList(headers));
            }
        }
        nSelectedColumns = columnSelectionList.size();
        if (selectedTimedataColumnIndex < 0) {
            dims[0] = columnProcessor.getColumn(0).size();
            field = new RegularField(dims);
            for (int i = 0; i < nSelectedColumns; i++) {
                if (actionsList.get(i) == ReadCSVShared.NOOP) {
                    field.addComponent(DataArray.create(columnProcessor.getColumn(i, String.class).toArray(new String[0]), 1, headerList.get(i)));
                } else if (actionsList.get(i) == ReadCSVShared.LOGIC) {
                    field.addComponent(DataArray.create(ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Byte.class).toArray(new Byte[0])), 1, headerList.get(i)));
                } else if (actionsList.get(i) == ReadCSVShared.BYTE) {
                    field.addComponent(DataArray.create(new UnsignedByteLargeArray(ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Short.class).toArray(new Short[0]))), 1, headerList.get(i)));
                } else if (actionsList.get(i) == ReadCSVShared.SHORT) {
                    field.addComponent(DataArray.create(ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Short.class).toArray(new Short[0])), 1, headerList.get(i)));
                } else if (actionsList.get(i) == ReadCSVShared.INT) {
                    field.addComponent(DataArray.create(ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Integer.class).toArray(new Integer[0])), 1, headerList.get(i)));
                } else if (actionsList.get(i) == ReadCSVShared.LONG) {
                    field.addComponent(DataArray.create(ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Long.class).toArray(new Long[0])), 1, headerList.get(i)));
                } else if (actionsList.get(i) == ReadCSVShared.FLOAT || actionsList.get(i) == ReadCSVShared.FLOAT_WITH_NAN) {
                    field.addComponent(DataArray.create(ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Float.class).toArray(new Float[0])), 1, headerList.get(i)));
                } else if (actionsList.get(i) == ReadCSVShared.DOUBLE || actionsList.get(i) == ReadCSVShared.DOUBLE_WITH_NAN) {
                    field.addComponent(DataArray.create(ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Double.class).toArray(new Double[0])), 1, headerList.get(i)));
                } else if (actionsList.get(i) == ReadCSVShared.COMPLEX || actionsList.get(i) == ReadCSVShared.COMPLEX_WITH_NAN) {
                    List<Complex> list = columnProcessor.getColumn(i, Complex.class);
                    ComplexFloatLargeArray la = new ComplexFloatLargeArray(list.size(), false);
                    complexListToLargeArray(list, la, 0);
                    field.addComponent(DataArray.create(la, 1, headerList.get(i)));
                } else {
                    throw new IllegalArgumentException("Invalid action " + actionsList.get(i));
                }
            }
        } else {
            ArrayList<LargeArray> columns = new ArrayList<>(nSelectedColumns);
            LargeArray timedataColumn = null;
            Map<Object, List[]> dataSeriesMap = new HashMap<>();
            for (int i = 0; i < nSelectedColumns; i++) {
                if (actionsList.get(i) == ReadCSVShared.NOOP) {
                    columns.add(new StringLargeArray(columnProcessor.getColumn(i, String.class).toArray(new String[0])));
                } else if (actionsList.get(i) == ReadCSVShared.LOGIC) {
                    columns.add(new LogicLargeArray(ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Byte.class).toArray(new Byte[0]))));
                } else if (actionsList.get(i) == ReadCSVShared.BYTE) {
                    columns.add(new UnsignedByteLargeArray(ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Short.class).toArray(new Short[0]))));
                } else if (actionsList.get(i) == ReadCSVShared.SHORT) {
                    columns.add(new ShortLargeArray(ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Short.class).toArray(new Short[0]))));
                } else if (actionsList.get(i) == ReadCSVShared.INT) {
                    columns.add(new IntLargeArray(ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Integer.class).toArray(new Integer[0]))));
                } else if (actionsList.get(i) == ReadCSVShared.LONG) {
                    columns.add(new LongLargeArray(ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Long.class).toArray(new Long[0]))));
                } else if (actionsList.get(i) == ReadCSVShared.FLOAT || actionsList.get(i) == ReadCSVShared.FLOAT_WITH_NAN) {
                    columns.add(new FloatLargeArray(ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Float.class).toArray(new Float[0]))));
                } else if (actionsList.get(i) == ReadCSVShared.DOUBLE || actionsList.get(i) == ReadCSVShared.DOUBLE_WITH_NAN) {
                    columns.add(new DoubleLargeArray(ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Double.class).toArray(new Double[0]))));
                } else if (actionsList.get(i) == ReadCSVShared.COMPLEX || actionsList.get(i) == ReadCSVShared.COMPLEX_WITH_NAN) {
                    List<Complex> list = columnProcessor.getColumn(i, Complex.class);
                    ComplexFloatLargeArray la = new ComplexFloatLargeArray(list.size(), false);
                    complexListToLargeArray(list, la, 0);
                    columns.add(la);
                } else {
                    throw new IllegalArgumentException("Invalid action " + actionsList.get(i));
                }
                if (i == selectedTimedataColumnIndex) {
                    timedataColumn = columns.get(i);
                    if (importTimedataColumn == false) {
                        columns.remove(i);
                    }
                }
            }
            if (importTimedataColumn == false) {
                actionsList.remove(selectedTimedataColumnIndex);
                headerList.remove(selectedTimedataColumnIndex);
                nSelectedColumns--;
            }
            for (long i = 0; i < timedataColumn.length(); i++) {
                Object elem = timedataColumn.get(i);
                if (dataSeriesMap.containsKey(elem)) {
                    List[] values = dataSeriesMap.get(elem);
                    for (int j = 0; j < nSelectedColumns; j++) {
                        List value = values[j];
                        value.add(columns.get(j).get(i));
                    }
                } else {
                    List[] values = new ArrayList[nSelectedColumns];
                    for (int j = 0; j < nSelectedColumns; j++) {
                        List value = new ArrayList<>();
                        value.add(columns.get(j).get(i));
                        values[j] = value;
                    }
                    dataSeriesMap.put(elem, values);
                }
            }
            int nSteps = dataSeriesMap.size();
            Collection<List[]> values = dataSeriesMap.values();
            ArrayList<Float>[] timeSeries = new ArrayList[nSelectedColumns];
            ArrayList<LargeArray>[] dataSeries = new ArrayList[nSelectedColumns];
            for (int i = 0; i < nSelectedColumns; i++) {
                timeSeries[i] = new ArrayList<>(nSteps);
                for (int j = 0; j < nSteps; j++) {
                    timeSeries[i].add((float) j);
                }
                dataSeries[i] = new ArrayList<>(nSteps);
            }
            long minLength = Long.MAX_VALUE;
            long maxLength = 0;
            for (List[] next : values) {
                for (int i = 0; i < nSelectedColumns; i++) {
                    int length = next[i].size();
                    if (length < minLength) {
                        minLength = length;
                    }
                    if (length > maxLength) {
                        maxLength = length;
                    }
                }
            }
            dims[0] = maxLength;
            for (List[] next : values) {
                for (int i = 0; i < nSelectedColumns; i++) {
                    LargeArray la = null;
                    if (actionsList.get(i) == ReadCSVShared.NOOP) {
                        la = new StringLargeArray((String[]) (next[i].toArray(new String[0])));
                    } else if (actionsList.get(i) == ReadCSVShared.LOGIC) {
                        la = new LogicLargeArray(ArrayUtils.toPrimitive((Byte[]) (next[i].toArray(new Byte[0]))));
                    } else if (actionsList.get(i) == ReadCSVShared.BYTE) {
                        la = new UnsignedByteLargeArray(ArrayUtils.toPrimitive((Short[]) (next[i].toArray(new Short[0]))));
                    } else if (actionsList.get(i) == ReadCSVShared.SHORT) {
                        la = new ShortLargeArray(ArrayUtils.toPrimitive((Short[]) (next[i].toArray(new Short[0]))));
                    } else if (actionsList.get(i) == ReadCSVShared.INT) {
                        la = new IntLargeArray(ArrayUtils.toPrimitive((Integer[]) (next[i].toArray(new Integer[0]))));
                    } else if (actionsList.get(i) == ReadCSVShared.LONG) {
                        la = new LongLargeArray(ArrayUtils.toPrimitive((Long[]) (next[i].toArray(new Long[0]))));
                    } else if (actionsList.get(i) == ReadCSVShared.FLOAT || actionsList.get(i) == ReadCSVShared.FLOAT_WITH_NAN) {
                        la = new FloatLargeArray(ArrayUtils.toPrimitive((Float[]) (next[i].toArray(new Float[0]))));
                    } else if (actionsList.get(i) == ReadCSVShared.DOUBLE || actionsList.get(i) == ReadCSVShared.DOUBLE_WITH_NAN) {
                        la = new DoubleLargeArray(ArrayUtils.toPrimitive((Double[]) (next[i].toArray(new Double[0]))));
                    } else if (actionsList.get(i) == ReadCSVShared.COMPLEX || actionsList.get(i) == ReadCSVShared.COMPLEX_WITH_NAN) {
                        la = new ComplexFloatLargeArray(next[i].size(), false);
                        complexListToLargeArray(next[i], (ComplexFloatLargeArray) la, 0);
                    } else {
                        throw new IllegalArgumentException("Invalid action " + actionsList.get(i));
                    }
                    if (minLength != maxLength && la.length() != maxLength) {
                        if (la.getType() == LargeArrayType.STRING) {
                            la = padWithEmpty((StringLargeArray) la, maxLength, EMPTY_STRING_VALUE);
                        } else {
                            la = padWithZero(la, maxLength);
                        }
                    }
                    dataSeries[i].add(la);
                }
            }
            field = new RegularField(dims);
            for (int i = 0; i < nSelectedColumns; i++) {
                TimeData td = new TimeData(timeSeries[i], dataSeries[i], 0);
                DataArray da = DataArray.create(td, 1, headerList.get(i));
                field.addComponent(da);
            }
        }
        return field;
    }

    /**
     * Reads and parses columns from CSV and TSV files and converts them to a 2D RegularField.
     *
     * @param filepath              file path, cannot be null or empty
     * @param fieldDelimiter        field delimeter, cannot be null or empty
     * @param nColumns              number of columns in a file
     * @param numberOfRowsToSkip    number of rows to skip
     * @param numberOfColumnsToSkip number of rows to skip
     * @param action                actions performed on each column during parsing
     *
     * @return 2D RegularField
     */
    public static RegularField readCsvField2D(String filepath, String fieldDelimiter, int nColumns, int numberOfRowsToSkip, int numberOfColumnsToSkip, int action)
    {
        if (filepath == null || filepath.isEmpty() || fieldDelimiter == null || fieldDelimiter.isEmpty())
            return null;

        RegularField field = null;
        long[] dims = new long[2];
        Integer[] excludedIndices;
        CsvParserSettings csvParserSettings = null;
        TsvParserSettings tsvParserSettings = null;
        String noDigitsRegex = "^(?!(\\-|\\+)?\\d+(\\.|\\,)?\\d*).*$";

        if (fieldDelimiter.equals("\t")) {
            tsvParserSettings = generateTsvSettings(numberOfRowsToSkip, false);
        } else {
            csvParserSettings = generateCsvSettings(fieldDelimiter, numberOfRowsToSkip, false, EMPTY_STRING_VALUE);
        }
        ObjectColumnProcessor columnProcessor = new ObjectColumnProcessor();
        if (action == ReadCSVShared.NOOP) {
            columnProcessor.convertAll(new ToStringConversion(NULL_STRING_VALUE, NULL_STRING_VALUE));
        } else if (action == ReadCSVShared.LOGIC) {
            columnProcessor.convertAll(new ByteConversion((byte) 0, "0"));
        } else if (action == ReadCSVShared.BYTE || action == ReadCSVShared.SHORT) {
            columnProcessor.convertAll(new ShortConversion((short) 0, "0"));
        } else if (action == ReadCSVShared.INT) {
            columnProcessor.convertAll(new IntegerConversion(0, "0"));
        } else if (action == ReadCSVShared.LONG) {
            columnProcessor.convertAll(new LongConversion(0l, "0"));
        } else if (action == ReadCSVShared.FLOAT) {
            columnProcessor.convertAll(new FloatConversion(0f, "0.0"));
        } else if (action == ReadCSVShared.DOUBLE) {
            columnProcessor.convertAll(new DoubleConversion(0.0, "0.0"));
        } else if (action == ReadCSVShared.COMPLEX) {
            columnProcessor.convertAll(new ComplexConversion(Complex.ZERO, Complex.ZERO.toString()));
        } else if (action == ReadCSVShared.FLOAT_WITH_NAN) {
            if (FloatingPointUtils.defaultNanAction == NaNAction.EXCEPTION_AT_NAN) {
                columnProcessor.convertAll(new FloatConversion(0f, "0.0"));
            } else {
                columnProcessor.convertAll(new RegexConversion(noDigitsRegex, Float.toString(Float.NaN)), new FloatConversion(0f, "0.0"));
            }
        } else if (action == ReadCSVShared.DOUBLE_WITH_NAN) {
            if (FloatingPointUtils.defaultNanAction == NaNAction.EXCEPTION_AT_NAN) {
                columnProcessor.convertAll(new DoubleConversion(0.0, "0.0"));
            } else {
                columnProcessor.convertAll(new RegexConversion(noDigitsRegex, Double.toString(Double.NaN)), new DoubleConversion(0.0, "0.0"));
            }
        } else if (action == ReadCSVShared.COMPLEX_WITH_NAN) {
            if (FloatingPointUtils.defaultNanAction == NaNAction.EXCEPTION_AT_NAN) {
                columnProcessor.convertAll(new ComplexConversion(Complex.ZERO, Complex.ZERO.toString()));
            } else {
                columnProcessor.convertAll(new RegexConversion(noDigitsRegex, Complex.NaN.toString()), new ComplexConversion(Complex.ZERO, Complex.ZERO.toString()));
            }
        } else {
            throw new IllegalArgumentException("Invalid action " + action);
        }

        excludedIndices = new Integer[numberOfColumnsToSkip];
        for (int i = 0; i < numberOfColumnsToSkip; i++) {
            excludedIndices[i] = i;
        }

        try {
            if (csvParserSettings != null) {
                csvParserSettings.excludeIndexes(excludedIndices);
                csvParserSettings.setProcessor(columnProcessor);
                CsvParser parser = new CsvParser(csvParserSettings);
                parser.parse(new File(filepath));
            } else {
                tsvParserSettings.excludeIndexes(excludedIndices);
                tsvParserSettings.setProcessor(columnProcessor);
                TsvParser parser = new TsvParser(tsvParserSettings);
                parser.parse(new File(filepath));
            }
        } catch (Exception ex) {
            throw new IllegalArgumentException("Parsing error: " + ex.getMessage());
        }

        nColumns = nColumns - numberOfColumnsToSkip;
        dims[0] = nColumns;
        dims[1] = columnProcessor.getColumn(0).size();
        field = new RegularField(dims);
        LargeArray la;
        if (action == ReadCSVShared.NOOP) {
            la = new StringLargeArray(dims[0] * dims[1], LargeArray.DEFAULT_MAX_STRING_LENGTH, false);
            for (int i = 0; i < nColumns; i++) {
                String[] column = columnProcessor.getColumn(i, String.class).toArray(new String[0]);
                for (int j = 0; j < column.length; j++) {
                    la.set(j * dims[0] + i, column[j]);
                }
            }
        } else if (action == ReadCSVShared.LOGIC) {
            la = new LogicLargeArray(dims[0] * dims[1], false);
            for (int i = 0; i < nColumns; i++) {
                byte[] column = ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Byte.class).toArray(new Byte[0]));
                for (int j = 0; j < column.length; j++) {
                    la.setByte(j * dims[0] + i, column[j]);
                }
            }
        } else if (action == ReadCSVShared.BYTE) {
            la = new UnsignedByteLargeArray(dims[0] * dims[1], false);
            for (int i = 0; i < nColumns; i++) {
                short[] column = ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Short.class).toArray(new Short[0]));
                for (int j = 0; j < column.length; j++) {
                    la.setUnsignedByte(j * dims[0] + i, column[j]);
                }
            }
        } else if (action == ReadCSVShared.SHORT) {
            la = new ShortLargeArray(dims[0] * dims[1], false);
            for (int i = 0; i < nColumns; i++) {
                short[] column = ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Short.class).toArray(new Short[0]));
                for (int j = 0; j < column.length; j++) {
                    la.setShort(j * dims[0] + i, column[j]);
                }
            }
        } else if (action == ReadCSVShared.INT) {
            la = new IntLargeArray(dims[0] * dims[1], false);
            for (int i = 0; i < nColumns; i++) {
                int[] column = ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Integer.class).toArray(new Integer[0]));
                for (int j = 0; j < column.length; j++) {
                    la.setInt(j * dims[0] + i, column[j]);
                }
            }
        } else if (action == ReadCSVShared.LONG) {
            la = new LongLargeArray(dims[0] * dims[1], false);
            for (int i = 0; i < nColumns; i++) {
                long[] column = ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Long.class).toArray(new Long[0]));
                for (int j = 0; j < column.length; j++) {
                    la.setLong(j * dims[0] + i, column[j]);
                }
            }
        } else if (action == ReadCSVShared.FLOAT || action == ReadCSVShared.FLOAT_WITH_NAN) {
            la = new FloatLargeArray(dims[0] * dims[1], false);
            for (int i = 0; i < nColumns; i++) {
                float[] column = ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Float.class).toArray(new Float[0]));
                for (int j = 0; j < column.length; j++) {
                    la.setFloat(j * dims[0] + i, column[j]);
                }
            }
        } else if (action == ReadCSVShared.DOUBLE || action == ReadCSVShared.DOUBLE_WITH_NAN) {
            la = new DoubleLargeArray(dims[0] * dims[1], false);
            for (int i = 0; i < nColumns; i++) {
                double[] column = ArrayUtils.toPrimitive(columnProcessor.getColumn(i, Double.class).toArray(new Double[0]));
                for (int j = 0; j < column.length; j++) {
                    la.setDouble(j * dims[0] + i, column[j]);
                }
            }
        } else if (action == ReadCSVShared.COMPLEX || action == ReadCSVShared.COMPLEX_WITH_NAN) {
            la = new ComplexFloatLargeArray(dims[0] * dims[1], false);
            for (int i = 0; i < nColumns; i++) {
                List<Complex> column = columnProcessor.getColumn(i, Complex.class);
                complexListToLargeArray(column, (ComplexFloatLargeArray) la, i * column.size());
            }
        } else {
            throw new IllegalArgumentException("Invalid action " + action);
        }
        field.addComponent(DataArray.create(la, 1, "data"));
        return field;
    }

    private static class ComplexConversion extends ObjectConversion<Complex>
    {

        private static final ComplexFormat format_i = new ComplexFormat("i");
        private static final ComplexFormat format_I = new ComplexFormat("I");
         private static final ComplexFormat format_j = new ComplexFormat("j");
        private static final ComplexFormat format_J = new ComplexFormat("J");

        public ComplexConversion()
        {
            super();
        }

        public ComplexConversion(Complex valueIfStringIsNull, String valueIfObjectIsNull)
        {
            super(valueIfStringIsNull, valueIfObjectIsNull);
        }

        @Override
        protected Complex fromString(String input)
        {
            Complex res = format_i.parse(input);
            if (res == null) {
                res = format_I.parse(input);
                if (res == null) {
                    res = format_j.parse(input);
                    if (res == null) {
                        res = format_J.parse(input);
                    }
                }
            }
            return res;
        }
    }

    private static void complexListToLargeArray(List<Complex> list, ComplexFloatLargeArray la, long offset)
    {
        int size = list.size();
        float[] elem = new float[2];
        for (int i = 0; i < size; i++) {
            Complex c = list.get(i);
            if(c == null) {
                throw new IllegalArgumentException("List element cannot be null");
            }
            elem[0] = (float) c.getReal();
            elem[1] = (float) c.getImaginary();
            la.setComplexFloat(offset + i, elem);
        }
    }

    private CsvFieldReader()
    {
    }
}
