/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
package org.visnow.vn.lib.basic.readers.ReadCSV;

import java.util.Arrays;
import javax.swing.SwingUtilities;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import static org.visnow.vn.gui.widgets.RunButton.RunState.NO_RUN;
import static org.visnow.vn.gui.widgets.RunButton.RunState.RUN_DYNAMICALLY;
import static org.visnow.vn.gui.widgets.RunButton.RunState.RUN_ONCE;
import static org.visnow.vn.lib.basic.readers.ReadCSV.ReadCSVShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;

/**
 * CSV reader - allows to read columns from CSV and TSV files into components of 1D RegularField.
 *
 * @author Bartosz Borucki (babor@icm.edu.pl) Warsaw University, Interdisciplinary Centre for Mathematical and Computational Modelling
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 *
 */
public class ReadCSV extends OutFieldVisualizationModule
{

    public static OutputEgg[] outputEggs = null;
    private ReadCSVGUI computeUI = null;
    private int runQueue = 0;

    public ReadCSV()
    {
        parameters.addParameterChangelistener((String name) -> {
            if (name != null && name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                runQueue++;
                startAction();
            } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                startAction();
        });
        SwingInstancer.swingRunAndWait(() -> {
            computeUI = new ReadCSVGUI();
            ui.addComputeGUI(computeUI);
            setPanel(ui);
            computeUI.setParameters(parameters);
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILENAME, ""),
            new Parameter<>(FIELD_DELIMITER, ","),
            new Parameter<>(NUMBER_OF_ROWS_TO_SKIP, 0),
            new Parameter<>(NUMBER_OF_COLUMNS_TO_SKIP, 0),
            new Parameter<>(HAS_HEADER_LINE, true),
            new Parameter<>(HEADER_NAMES, new String[]{}),
            new Parameter<>(FIRST_ROW, new String[]{}),
            new Parameter<>(MODE_1D, true),
            new Parameter<>(ACTIONS_1D, new int[]{DEFAULT_ACTION_INDEX}),
            new Parameter<>(ACTION_2D, DEFAULT_ACTION_INDEX),
            new Parameter<>(ROW_LABELS_INDEX, -1),
            new Parameter<>(COLUMN_LABELS_INDEX, -1),
            new Parameter<>(COLUMN_SELECTION, new boolean[0]),
            new Parameter<>(TIMEDATA_COLUMN_INDEX, -1),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN)
        };
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive()
    {
        RegularField rowLabels = null;
        RegularField columnLabels = null;

        Parameters p;
        synchronized (parameters) {
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, isFromVNA(), false);
        ProgressAgent progressAgent = getProgressAgent(120); //100 for read, 20 for geometry
        if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
            runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data
            if (p.get(FILENAME).isEmpty()) {
                outRegularField = null;
                rowLabels = null;
                columnLabels = null;
            } else {
                try {
                    if (p.get(MODE_1D)) {
                        outRegularField = CsvFieldReader.readCsvField1D(p.get(ReadCSVShared.FILENAME), p.get(FIELD_DELIMITER), p.get(NUMBER_OF_ROWS_TO_SKIP), p.get(HAS_HEADER_LINE), p.get(ACTIONS_1D), p.get(COLUMN_SELECTION), p.get(TIMEDATA_COLUMN_INDEX));
                    } else {
                        outRegularField = CsvFieldReader.readCsvField2D(p.get(ReadCSVShared.FILENAME), p.get(FIELD_DELIMITER), p.get(ACTIONS_1D).length, p.get(NUMBER_OF_ROWS_TO_SKIP), p.get(NUMBER_OF_COLUMNS_TO_SKIP), p.get(ACTION_2D));
                        String[] selectedRow = CsvFieldReader.readSelectedRow(p.get(FILENAME), p.get(FIELD_DELIMITER), p.get(COLUMN_LABELS_INDEX));
                        if (selectedRow != null) {
                            long[] dims = new long[]{selectedRow.length - p.get(NUMBER_OF_COLUMNS_TO_SKIP)};
                            columnLabels = new RegularField(dims);
                            DataArray da = DataArray.create(Arrays.copyOfRange(selectedRow, p.get(NUMBER_OF_COLUMNS_TO_SKIP), selectedRow.length), 1, "columnLabels");
                            columnLabels.addComponent(da);
                        }
                        String[] selectedColumn = CsvFieldReader.readSelectedColumn(p.get(FILENAME), p.get(FIELD_DELIMITER), p.get(ROW_LABELS_INDEX));
                        if (selectedColumn != null) {
                            long[] dims = new long[]{selectedColumn.length - p.get(NUMBER_OF_ROWS_TO_SKIP)};
                            rowLabels = new RegularField(dims);
                            DataArray da = DataArray.create(Arrays.copyOfRange(selectedColumn, p.get(NUMBER_OF_ROWS_TO_SKIP), selectedColumn.length), 1, "rowLabels");
                            rowLabels.addComponent(da);
                            float[][] affine = rowLabels.getAffine();
                            affine[0][0] = 0;
                            affine[0][1] = 1;
                            affine[0][2] = 0;
                            rowLabels.setAffine(affine);
                        }
                    }
                } catch (Exception ex) {
                    outRegularField = null;
                    rowLabels = null;
                    columnLabels = null;
                    VisNow.get().userMessageSend(null, "Cannot create output field.", ex.getMessage(), Level.ERROR);
                }
            }
            outField = outRegularField;
            if (outRegularField == null) {
                setOutputValue("outField", null);
            } else {
                setOutputValue("outField", new VNRegularField(outRegularField));
            }
            if (rowLabels == null) {
                setOutputValue("rowLabels", null);
            } else {
                setOutputValue("rowLabels", new VNRegularField(rowLabels));
            }
            if (columnLabels == null) {
                setOutputValue("columnLabels", null);
            } else {
                setOutputValue("columnLabels", new VNRegularField(columnLabels));
            }
            progressAgent.setProgressStep(100);
            prepareOutputGeometry();
            show();
            progressAgent.setProgress(1.0);
        }
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag()) {
            SwingUtilities.invokeLater(() -> {
                computeUI.activateOpenDialog();
            });
        }
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

}
