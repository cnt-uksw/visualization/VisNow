/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.ReadCSV;

import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton;

/**
 * Parameters for CSV reader.
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class ReadCSVShared
{

    public static final int NOOP = -1;
    public static final int LOGIC = DataArrayType.FIELD_DATA_LOGIC.getValue();
    public static final int BYTE = DataArrayType.FIELD_DATA_BYTE.getValue();
    public static final int SHORT = DataArrayType.FIELD_DATA_SHORT.getValue();
    public static final int INT = DataArrayType.FIELD_DATA_INT.getValue();
    public static final int LONG = DataArrayType.FIELD_DATA_LONG.getValue();
    public static final int FLOAT = DataArrayType.FIELD_DATA_FLOAT.getValue();
    public static final int DOUBLE = DataArrayType.FIELD_DATA_DOUBLE.getValue();
    public static final int COMPLEX = DataArrayType.FIELD_DATA_COMPLEX.getValue();
    public static final int FLOAT_WITH_NAN = 2 * DataArrayType.getMaxValue() + 1;
    public static final int DOUBLE_WITH_NAN = 2 * DataArrayType.getMaxValue() + 2;
    public static final int COMPLEX_WITH_NAN = 2 * DataArrayType.getMaxValue() + 3;

    public static final int[] ACTION_CODES = new int[]{NOOP, LOGIC, BYTE, SHORT, INT, LONG, FLOAT, DOUBLE, COMPLEX, FLOAT_WITH_NAN, DOUBLE_WITH_NAN, COMPLEX_WITH_NAN};

    public static final String[] ACTION_NAMES = new String[]{"to string", "to logic", "to byte", "to short", "to int", "to long", "to float", "to double", "to complex", "to float with nan", "to double with nan", "to complex with nan"};

    public static final int DEFAULT_ACTION_INDEX = 9; // "to float with nan"

    static final ParameterName<String> FILENAME = new ParameterName("File name");

    static final ParameterName<String> FIELD_DELIMITER = new ParameterName("Field delimiter");

    static final ParameterName<Integer> NUMBER_OF_ROWS_TO_SKIP = new ParameterName("Number of rows to skip");

    static final ParameterName<Integer> NUMBER_OF_COLUMNS_TO_SKIP = new ParameterName("Number of columns to skip");

    static final ParameterName<Boolean> HAS_HEADER_LINE = new ParameterName("Has header line");

    static final ParameterName<String[]> HEADER_NAMES = new ParameterName("Header names");

    static final ParameterName<String[]> FIRST_ROW = new ParameterName("First row");

    static final ParameterName<Boolean> MODE_1D = new ParameterName("Mode 1D");

    static final ParameterName<int[]> ACTIONS_1D = new ParameterName("Actions 1D");

    static final ParameterName<Integer> ACTION_2D = new ParameterName("Action 2D");

    static final ParameterName<Integer> ROW_LABELS_INDEX = new ParameterName("Row labels index");

    static final ParameterName<Integer> COLUMN_LABELS_INDEX = new ParameterName("Column labels index");

    static final ParameterName<boolean[]> COLUMN_SELECTION = new ParameterName("Column selection");

    static final ParameterName<Integer> TIMEDATA_COLUMN_INDEX = new ParameterName("Timedata column index");

    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");
}
