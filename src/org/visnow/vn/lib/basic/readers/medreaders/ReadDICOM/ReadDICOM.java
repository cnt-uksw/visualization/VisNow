/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.medreaders.ReadDICOM;

import org.visnow.vn.datamaps.ColorMapManager;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import static org.visnow.vn.lib.basic.readers.medreaders.ReadDICOM.ReadDICOMShared.*;
import org.visnow.vn.system.main.VisNow;

/**
 * @author bartosz Borucki (babor@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ReadDICOM extends OutFieldVisualizationModule
{

    private GUI computeUI;
    private ReadDICOMCore core = new ReadDICOMCore();
    public static OutputEgg[] outputEggs = null;

    /**
     * Creates a new instance of ReadImage
     */
    public ReadDICOM()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });

        core.setParameters(parameters);
        core.addFloatValueModificationListener(new FloatValueModificationListener()
        {

            @Override
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                setProgress(e.getVal());
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                core.setHistoArea(computeUI.getHistoArea());
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
        
        presentationParams.getDataMappingParams().getColorMap0().setMapIndex(ColorMapManager.COLORMAP1D_GRAY);
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return ReadDICOMShared.getDefaultParameters();
    }

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        
        Parameters p;
        synchronized (parameters) {
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, false, false);
        
        if (p.get(FILE_LIST) == null || p.get(FILE_LIST).length < 1) {
            outRegularField = null;
            outField = null;
            setOutputValue("outField", null);
            return;
        }

        try {
            core.update();
            outRegularField = core.getOutField();
            if(outRegularField == null)
                throw new ReadDICOMException("");
            setOutputValue("outField", new VNRegularField(outRegularField));
        } catch (ReadDICOMException ex) {
            //ex.printStackTrace();
            System.err.println("ERROR reding DICOM: " + ex.getMessage());
            outRegularField = null;
            setOutputValue("outField", null);
        }
        outField = outRegularField;
        prepareOutputGeometry();
        show();
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag())
            computeUI.activateOpenDialog();
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

}
