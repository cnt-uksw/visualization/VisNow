/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */

package org.visnow.vn.lib.basic.readers.ReadEnSightGoldCase;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;
import java.util.Vector;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.parameters.PresentationParams;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.gui.widgets.FileErrorFrame;
import static org.visnow.vn.lib.basic.readers.ReadEnSightGoldCase.ReadEnSightGoldCaseShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.io.InputSource;
import static org.visnow.vn.lib.utils.field.CellToNode.*;
import org.visnow.vn.lib.utils.field.GeometricOrientation;
import org.visnow.vn.lib.utils.field.VectorComponentCombiner;
import static org.visnow.vn.lib.utils.field.MergeIrregularField.mergeCellSet;
import org.visnow.vn.system.main.VisNow;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ReadEnSightGoldCase extends OutFieldVisualizationModule
{

    protected GUI computeUI = null;
    protected String lastFileName = " ";
    protected FileErrorFrame errorFrame = null;
    protected Cell[] stdCells = new Cell[Cell.getNProperCellTypes()];
    protected boolean ignoreUI = false;
    protected Scanner scanner = null;
    protected Vector<int[]> partCounts = new Vector<>();
    protected Vector<String> partNames = new Vector<>();

    protected String nextLine()
    {
        String line;
        try {
            do {
                line = scanner.nextLine().trim();
                System.out.println(line);
            } while (line.isEmpty());
            return line;
        } catch (Exception e) {
            return null;
        }
    }

    protected String nextLine(String[] begin)
    {
        String[] lBegin = new String[begin.length];
        for (int i = 0; i < begin.length; i++)
            lBegin[i] = begin[i].toLowerCase();
        String line;
        try {
            while ((line = scanner.nextLine().trim()) != null) {
                System.out.println(line);
                for (String lBegin1 : lBegin)
                    if (line.toLowerCase().startsWith(lBegin1))
                        return line;
            }
        } catch (Exception e) {
        }
        return null;
    }

    /**
     * Creates a new instance of CreateGrid
     */
    public ReadEnSightGoldCase()
    {
        for (int i = 0; i < stdCells.length; i++)
            stdCells[i] = Cell.createCell(CellType.getType(i), 3, new int[CellType.getType(i).getNVertices()], (byte)1);
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI("EnSight Gold reader", "EnSight Gold case file",
                                    new String[]{"case", "CASE", "encas", "ENCAS"});
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
        computeUI.setParameters(parameters);

    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILENAME, ""),
            new Parameter<>(MATERIALS_AS_SETS, true),
            new Parameter<>(CELL_TO_NODE, true),
            new Parameter<>(DROP_CELL_DATA, true),
            new Parameter<>(MERGE_CELL_SETS, true),
            new Parameter<>(DROP_CONSTANT_DATA, true),
            new Parameter<>(SHOW, true),
            new Parameter<>(INPUT_SOURCE, InputSource.FILE)
        };
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    private File caseFile = null;

    private Scanner getScanner(Parameters p)
    {
        URL url = null;
        Scanner sc = null;
        try {
            if (p.get(INPUT_SOURCE) == InputSource.URL) {
                sc = new Scanner(new InputStreamReader(new URL(p.get(FILENAME)).openConnection().getInputStream()));
            } else {
                caseFile = new File(p.get(FILENAME));
                sc = new Scanner(new FileReader(caseFile));
            }
        } catch (IOException iOException) {
        }
        return sc;
    }

    class TimeSet
    {

        int index;
        String description;
        Vector<Float> timeSteps = new Vector<>();
        Vector<String> timeFileSuffixes = new Vector<>();

        public TimeSet(int index, String description)
        {
            this.index = index;
            this.description = description;
        }

        public void add(float time, String suffix)
        {
            timeSteps.add(time);
            timeFileSuffixes.add(suffix);
        }

        public float getTime(int i)
        {
            return timeSteps.get(i);
        }

        public String getSuffix(int i)
        {
            return timeFileSuffixes.get(i);
        }
    }

    private IrregularField readEnsightGoldCase(Parameters p)
    {

        URL url = null;
        caseFile = null;
        boolean binary = false;
        String line;
        Vector<TimeSet> timeSets = new Vector<>();

        scanner = getScanner(p);
        if (scanner == null)
            return null;
        if (nextLine(new String[]{"# EnSight Gold", "#EnSight Gold"}) == null)
            return null;

        while ((line = nextLine(new String[]{"TIME", "FILE"})) != null) {
            if (line.trim().toUpperCase().startsWith("TIME")) {
                int n;
                int nSteps;
                line = scanner.nextLine();
                String[] items = nextLine().split("\\s+");
                StringBuilder desc = new StringBuilder();
                try {
                    n = Integer.parseInt(items[2]);
                    if (items.length > 3)
                        for (int i = 3; i < items.length; i++)
                            desc.append(items[i]).append(" ");
                } catch (Exception e) {
                    continue;
                }
                TimeSet timeSet = new TimeSet(n, desc.toString());
                try {
                    nSteps = Integer.parseInt(scanner.nextLine().split("\\s+")[3]);
                } catch (Exception e) {
                }

            }
        }
        scanner.close();

        scanner = getScanner(p);

        if (nextLine(new String[]{"GEOMETRY"}) == null)
            return null;
        String[] items = nextLine().split("\\s+");
        if (!"model:".equalsIgnoreCase(items[0]))
            return null;
        Reader reader = binary ? new BinaryReader() : new ASCIIReader();
        outIrregularField = reader.readEnSightGoldGeometry(p, caseFile.getParent() + File.separator + items[items.length - 1]);

        partCounts = reader.getPartCounts();
        partNames = reader.getPartNames();

        while (nextLine(new String[]{"VARIABLES"}) != null) {
            items = nextLine().split("\\s+");
            if (!items[2].startsWith("node"))
                continue;
            int veclen = 0;
            if (items[0].equalsIgnoreCase("scalar"))
                veclen = 1;
            else if (items[0].equalsIgnoreCase("vector"))
                veclen = 3;
            else if (items[0].equalsIgnoreCase("tensor"))
                veclen = 6;
            reader = binary ? new BinaryReader() : new ASCIIReader();
            reader.setPartCounts(partCounts);
            reader.setPartNames(partNames);
            DataArray da = reader.readEnSightGoldVariable(p, (int) outIrregularField.getNNodes(), veclen, items[5],
                                                caseFile.getParent() + File.separator + items[items.length - 1]);
            if (da != null)
                outIrregularField.addComponent(da);
        }
        return outIrregularField;
    }


    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    public static OutputEgg[] outputEggs = null;
    IrregularField field;

    @Override
    public void onActive() {
        Parameters p = parameters.getReadOnlyClone();
        notifyGUIs(p, isFromVNA(), false);

        if (p.get(FILENAME) != null && !p.get(FILENAME).isEmpty() && !p.get(FILENAME).equals(lastFileName)) {
            lastFileName = p.get(FILENAME);
            field = readEnsightGoldCase(p);
            if ((outIrregularField = field) == null)
                return;
            int[] nodeIdx = new int[(int) outIrregularField.getNNodes()];
            for (int i = 0; i < nodeIdx.length; i++)
                nodeIdx[i] = i;
            outIrregularField.addComponent(DataArray.create(nodeIdx, 1, "node indices"));

            GeometricOrientation.recomputeOrientations(outIrregularField);

            VectorComponentCombiner.combineVectors(outIrregularField);

            if (parameters.get(DROP_CONSTANT_DATA)) {
                for (int i = outIrregularField.getNComponents() - 1; i >= 0; i--) {
                    DataArray component = outIrregularField.getComponent(i);
                    if (component.getMinValue() == component.getMaxValue()) {
                        outIrregularField.removeComponent(i);
                    }
                }
                for (int c = 0; c < outIrregularField.getNCellSets(); c++) {
                    for (int i = outIrregularField.getCellSet(c).getNComponents() - 1; i >= 0; i--) {
                        DataArray component = outIrregularField.getCellSet(c).getComponent(i);
                        if (component.getMinValue() == component.getMaxValue()) {
                            outIrregularField.getCellSet(c).removeComponent(i);
                        }
                    }
                }
            }

            if (parameters.get(CELL_TO_NODE))
                outIrregularField = convertCellDataToNodeData(parameters.get(DROP_CELL_DATA), outIrregularField);

            if (parameters.get(MERGE_CELL_SETS))
                outIrregularField = mergeCellSet(outIrregularField);

            computeUI.setFieldDescription(outIrregularField.description());
            setOutputValue("EnSight object", new VNIrregularField(outIrregularField));
        }

        if (p.get(SHOW)) {
            outField = field;
            prepareOutputGeometry();
            renderingParams.setShadingMode(RenderingParams.FLAT_SHADED);
            for (PresentationParams csParams : presentationParams.getChildrenParams())
                csParams.getRenderingParams().setShadingMode(RenderingParams.FLAT_SHADED);
            if (VisNow.get().getMainConfig().isDefaultOutline() && outField.getTrueNSpace() == 3) {
                ui.getPresentationGUI().getRenderingGUI().setOutlineMode(true);
                renderingParams.setMinEdgeDihedral(10);
                renderingParams.setDisplayMode(RenderingParams.EDGES);
            }
            show();
        } else {
            outField = null;
            prepareOutputGeometry();
            show();
        }
    }

    @Override
    public void onInitFinishedLocal() {
        if (isForceFlag())
            computeUI.activateOpenDialog();
    }

}
