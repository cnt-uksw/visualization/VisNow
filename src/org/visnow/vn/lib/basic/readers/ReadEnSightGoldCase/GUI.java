/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.ReadEnSightGoldCase;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.swingwrappers.TextField;
import static org.visnow.vn.lib.basic.readers.ReadEnSightGoldCase.ReadEnSightGoldCaseShared.*;
import org.visnow.vn.lib.gui.Browser;
import org.visnow.vn.lib.utils.io.InputSource;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class GUI extends javax.swing.JPanel
{

    private JFileChooser dataFileChooser = new JFileChooser();
    private FileNameExtensionFilter dataFilter;
    private Parameters parameters;
    private String lastPath = null;
    private String[] extensions = new String[]{
        "case", "CASE", "encas", "ENCAS"
    };
    private Browser browser = new Browser(extensions);

    /**
     * Creates new form GUI
     */
    public GUI()
    {
        initComponents();
        dataFileChooser.setLocation(0, 0);
        browser.setVisible(false);
        browser.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                parameters.set(INPUT_SOURCE, InputSource.URL, FILENAME, browser.getCurrentURL());
            }
        });
    }

    public GUI(String title, String dataFileDesc, String[] extensions)
    {
        initComponents();
        moduleLabel.setText(title);
        this.extensions = extensions;
        switch (extensions.length) {
            case 1:
                dataFilter = new FileNameExtensionFilter(dataFileDesc, extensions[0]);
                break;
            case 2:
                dataFilter = new FileNameExtensionFilter(dataFileDesc, extensions[0], extensions[1]);
                break;
            case 3:
                dataFilter = new FileNameExtensionFilter(dataFileDesc, extensions[0], extensions[1],
                                                         extensions[2]);
                break;
            case 4:
                dataFilter = new FileNameExtensionFilter(dataFileDesc, extensions[0], extensions[1],
                                                         extensions[2], extensions[3]);
                break;
        }
        dataFileChooser.setFileFilter(dataFilter);
        browser.setVisible(false);
        browser.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                parameters.set(INPUT_SOURCE, InputSource.URL, FILENAME, browser.getCurrentURL());
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        selectButton = new javax.swing.JButton();
        moduleLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        fieldDescription = new javax.swing.JLabel();
        fileButton = new javax.swing.JRadioButton();
        urlButton = new javax.swing.JRadioButton();
        separateCheckBox = new javax.swing.JCheckBox();
        fileNameField = new org.visnow.vn.gui.swingwrappers.TextField();
        dropConstantCB = new javax.swing.JCheckBox();
        cellToNodeCB = new javax.swing.JCheckBox();
        dropCellDataCB = new javax.swing.JCheckBox();
        displayCB = new javax.swing.JCheckBox();
        mergeCellSetsCB = new javax.swing.JCheckBox();

        setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        setMinimumSize(new java.awt.Dimension(180, 500));
        setPreferredSize(new java.awt.Dimension(200, 600));
        setRequestFocusEnabled(false);
        setLayout(new java.awt.GridBagLayout());

        selectButton.setText("browse");
        selectButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        selectButton.setMaximumSize(new java.awt.Dimension(90, 20));
        selectButton.setMinimumSize(new java.awt.Dimension(90, 20));
        selectButton.setPreferredSize(new java.awt.Dimension(90, 20));
        selectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        add(selectButton, gridBagConstraints);

        moduleLabel.setText("module");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(moduleLabel, gridBagConstraints);

        fieldDescription.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        fieldDescription.setText("null");
        fieldDescription.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        fieldDescription.setMaximumSize(new java.awt.Dimension(700, 250));
        fieldDescription.setMinimumSize(new java.awt.Dimension(400, 170));
        fieldDescription.setPreferredSize(new java.awt.Dimension(500, 200));
        jScrollPane1.setViewportView(fieldDescription);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(jScrollPane1, gridBagConstraints);

        buttonGroup1.add(fileButton);
        fileButton.setSelected(true);
        fileButton.setText("file");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(fileButton, gridBagConstraints);

        buttonGroup1.add(urlButton);
        urlButton.setText("URL");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(urlButton, gridBagConstraints);

        separateCheckBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        separateCheckBox.setSelected(true);
        separateCheckBox.setText("parts as cell sets");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        add(separateCheckBox, gridBagConstraints);

        fileNameField.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                fileNameFieldUserAction(evt);
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        fileNameField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                fileNameFieldFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        add(fileNameField, gridBagConstraints);

        dropConstantCB.setSelected(true);
        dropConstantCB.setText("drop constant components");
        dropConstantCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dropConstantCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(dropConstantCB, gridBagConstraints);

        cellToNodeCB.setSelected(true);
        cellToNodeCB.setText("convert cell data to node data");
        cellToNodeCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cellToNodeCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(cellToNodeCB, gridBagConstraints);

        dropCellDataCB.setSelected(true);
        dropCellDataCB.setText("drop cell data");
        dropCellDataCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dropCellDataCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(dropCellDataCB, gridBagConstraints);

        displayCB.setSelected(true);
        displayCB.setText("show");
        displayCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                displayCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(displayCB, gridBagConstraints);

        mergeCellSetsCB.setSelected(true);
        mergeCellSetsCB.setText("merge cell sets");
        mergeCellSetsCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mergeCellSetsCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(mergeCellSetsCB, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void selectButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_selectButtonActionPerformed
    {//GEN-HEADEREND:event_selectButtonActionPerformed
        String fileName = null;
        if (fileButton.isSelected()) {
            if (lastPath == null)
                dataFileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(ReadEnSightGoldCase.class)));
            else
                dataFileChooser.setCurrentDirectory(new File(lastPath));

            int returnVal = dataFileChooser.showOpenDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                fileName = dataFileChooser.getSelectedFile().getAbsolutePath();
                lastPath = fileName.substring(0, fileName.lastIndexOf(File.separator));
                VisNow.get().getMainConfig().setLastDataPath(lastPath, ReadEnSightGoldCase.class);

                parameters.set(FILENAME, fileName, INPUT_SOURCE, InputSource.FILE, MATERIALS_AS_SETS, separateCheckBox.isSelected());
            }
        } else if (urlButton.isSelected()) {
            browser.setVisible(true);
        }
    }//GEN-LAST:event_selectButtonActionPerformed

    private void fileNameFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fileNameFieldFocusGained
        ((TextField) evt.getSource()).selectAll();        
    }//GEN-LAST:event_fileNameFieldFocusGained

    private void fileNameFieldUserAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_fileNameFieldUserAction
        if (evt.getEventType() == TextField.EVENT_CHANGE_VALUE || evt.getEventType() == TextField.EVENT_NO_CHANGE_ENTER_KEY)
            parameters.set(FILENAME, fileNameField.getText());
    }//GEN-LAST:event_fileNameFieldUserAction

    private void dropConstantCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dropConstantCBActionPerformed
        parameters.setParameterActive(false);
        parameters.set(DROP_CONSTANT_DATA, dropConstantCB.isSelected());
        parameters.setParameterActive(true);
    }//GEN-LAST:event_dropConstantCBActionPerformed

    private void cellToNodeCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cellToNodeCBActionPerformed
        parameters.setParameterActive(false);
        parameters.set(CELL_TO_NODE, cellToNodeCB.isSelected());
        parameters.setParameterActive(true);
    }//GEN-LAST:event_cellToNodeCBActionPerformed

    private void dropCellDataCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dropCellDataCBActionPerformed
        parameters.setParameterActive(false);
        parameters.set(DROP_CELL_DATA, dropCellDataCB.isSelected());
        parameters.setParameterActive(true);
    }//GEN-LAST:event_dropCellDataCBActionPerformed

    private void mergeCellSetsCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mergeCellSetsCBActionPerformed
        parameters.setParameterActive(false);
        parameters.set(MERGE_CELL_SETS, mergeCellSetsCB.isSelected());
        parameters.setParameterActive(true);
    }//GEN-LAST:event_mergeCellSetsCBActionPerformed

    private void displayCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_displayCBActionPerformed
        parameters.setParameterActive(false);
        parameters.set(SHOW, displayCB.isSelected());
        parameters.setParameterActive(true);
    }//GEN-LAST:event_displayCBActionPerformed


    public void setFieldDescription(String s)
    {
        fieldDescription.setText(s);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JCheckBox cellToNodeCB;
    private javax.swing.JCheckBox displayCB;
    private javax.swing.JCheckBox dropCellDataCB;
    private javax.swing.JCheckBox dropConstantCB;
    private javax.swing.JLabel fieldDescription;
    private javax.swing.JRadioButton fileButton;
    private org.visnow.vn.gui.swingwrappers.TextField fileNameField;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JCheckBox mergeCellSetsCB;
    private javax.swing.JLabel moduleLabel;
    private javax.swing.JButton selectButton;
    private javax.swing.JCheckBox separateCheckBox;
    private javax.swing.JRadioButton urlButton;
    // End of variables declaration//GEN-END:variables
    /**
     * @param params the params to set
     */

    public String getLastPath()
    {
        return lastPath;
    }

    void activateOpenDialog()
    {
        selectButtonActionPerformed(null);
    }

    void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }

    void updateGUI(ParameterProxy p) {
        fileNameField.setText(p.get(FILENAME));
        separateCheckBox.setSelected(p.get(MATERIALS_AS_SETS));
        Integer input_type = p.get(INPUT_SOURCE);
        
        fileButton.setSelected(input_type == 0);
        urlButton.setSelected(input_type == 1);
    }
}
