/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.readers.ReadVTK;

import java.io.File;
import java.nio.ByteOrder;
import javax.swing.SwingUtilities;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import static org.visnow.vn.lib.basic.readers.ReadVTK.ReadVTKShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.vtk.VTKCore;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;

/**
 * @author creed Interdisciplinary Centre for Mathematical and Computational
 * Modelling
 */
public class ReadVTK extends OutFieldVisualizationModule
{

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ReadVTK.class);

    private GUI computeUI = null;
    private VTKCore core;

    public ReadVTK()
    {
        core = VTKCore.loadVTKLibrary();
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (!VisNow.isNativeLibraryLoaded("vtk")) {
            VisNow.get().userMessageSend(this, "Native VTK library is not loaded.", "Only legacy VTK files can be imported", Level.WARNING);
        }
        if (isForceFlag()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    computeUI.activateOpenDialog();
                }
            });
        }
    }
    
    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILENAME, ""),
            new Parameter<>(BIG_ENDIAN, false),};
    }

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    public static OutputEgg[] outputEggs = null;

    @Override
    public void onActive()
    {
        Parameters p = parameters.getReadOnlyClone();

        notifyGUIs(p, false, false);
        if (p.get(FILENAME).equals("")) {
            outField = null;
            show();
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", null);
        } else {
            ProgressAgent progressAgent = getProgressAgent(120); //100 for read, 20 for geometry
            File f;
            f = new File(p.get(FILENAME));
            outField = core.readVTK(f, p.get(BIG_ENDIAN) == true ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
            if (outField == null) {
                VisNow.get().userMessageSend(this, "Error loading file.", "Cannot read file " + p.get(FILENAME), Level.ERROR);
            }

            if (outField != null && outField instanceof RegularField) {
                outRegularField = (RegularField) outField;
                outIrregularField = null;
                setOutputValue("outRegularField", new VNRegularField(outRegularField));
                setOutputValue("outIrregularField", null);
                VisNow.get().userMessageSend(this, "<html>File successfully loaded", outRegularField.toMultilineString(), Level.INFO);
            } else if (outField != null && outField instanceof IrregularField) {
                outRegularField = null;
                outIrregularField = (IrregularField) outField;
                setOutputValue("outRegularField", null);
                setOutputValue("outIrregularField", new VNIrregularField(outIrregularField));
                VisNow.get().userMessageSend(this, "<html>File successfully loaded.", outIrregularField.toMultilineString(), Level.INFO);
            } else {
                outRegularField = null;
                outIrregularField = null;
                setOutputValue("outRegularField", null);
                setOutputValue("outIrregularField", null);
            }

            progressAgent.setProgressStep(100);
            prepareOutputGeometry();
            show();
        }
    }
}
