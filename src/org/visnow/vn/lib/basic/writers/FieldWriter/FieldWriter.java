/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */

package org.visnow.vn.lib.basic.writers.FieldWriter;

import java.nio.file.FileSystemException;
import org.visnow.jscic.Field;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.writers.FieldWriter.FieldWriterShared.*;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.io.VisNowFieldWriter;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.system.utils.usermessage.UserMessage;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class FieldWriter extends ModuleCore
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI ui = null;
    protected Field inField;
    protected boolean do_write = false;

    public FieldWriter()
    {
        parameters.addParameterChangelistener((String name) -> {
            if (name.equals("Do write")) {
                do_write = true;
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(() -> {
            ui = new GUI();
            setPanel(ui);
            ui.setParameters(parameters);
        });

    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(DO_WRITE, false),
            new Parameter<>(FILENAME, ""),
            new Parameter<>(FIELDFORMAT, FieldWriterFileFormat.SERIALIZED)
        };
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        ui.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            Field newField = ((VNField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA() && (inField == null || inField.getTimestamp() != newField.getTimestamp());
            inField = newField;
            Parameters p = parameters.getReadOnlyClone();
            notifyGUIs(p, isFromVNA() || isDifferentField, true);
            String outFileName = p.get(FILENAME);
            if (isDifferentField)
                VisNow.get().userMessageSend(new UserMessage("", "", "", "", Level.INFO));
            if (do_write && outFileName != null && !outFileName.isEmpty()) {

                FieldWriterFileFormat format = p.get(FIELDFORMAT);
                if (VisNowFieldWriter.canWriteToOutputFile(outFileName, format)) {
                    try {
                        VisNowFieldWriter.writeField(inField, p.get(FILENAME), p.get(FIELDFORMAT), true);
                        VisNow.get().userMessageSend(new UserMessage("VisNow", "", "Field successfully written", "", Level.INFO));
                    } catch (FileSystemException ex) {
                        VisNow.get().userMessageSend(new UserMessage("VisNow", "", "Error writing field", "", Level.ERROR));
                    }
                } else {
                    VisNow.get().userMessageSend(new UserMessage("VisNow", "", "Error writing field", "", Level.ERROR));
                }
                do_write = false;
            }
        }
    }

}
