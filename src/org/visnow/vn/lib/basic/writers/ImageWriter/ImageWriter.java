/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.writers.ImageWriter;

import java.awt.Color;
import java.util.Arrays;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.LinkFace;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.parameters.RegularField3DParams;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.geometries.parameters.TransparencyParams;
import org.visnow.vn.geometries.utils.ColorMapper;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.ImageUtils;
import org.visnow.vn.lib.utils.SwingInstancer;
import static org.visnow.vn.lib.basic.writers.ImageWriter.ImageWriterShared.*;
import org.visnow.vn.lib.utils.field.SliceRegularField;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 *
 */
public class ImageWriter extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    private static final Logger LOGGER = Logger.getLogger(ImageWriter.class);
    private ImageWriterGUI computeUI = null;
    private RegularField inField = null;

    public ImageWriter()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (!parameters.get(FILENAME).isEmpty() && parameters.get(WRITE)) {
                    onActive();
                    parameters.set(WRITE, false);
                }
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new ImageWriterGUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
                ui.showPresentation();
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FORMAT, "JPEG"),
            new Parameter<>(FILENAME, ""),
            new Parameter<>(FLIP_I, false),
            new Parameter<>(FLIP_J, false),
            new Parameter<>(FLIP_K, false),
            new Parameter<>(SLICE_AXIS, 2),
            new Parameter<>(WRITE, false),
            new Parameter<>(META_FIELD_DIMENSION_LENGTH, 3)
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        int dims = inField.getDimNum();
        if (dims == 2) {
            renderingParams.setShadingMode(RenderingParams.UNSHADED);
            presentationParams.getContent3DParams().setGridType(RegularField3DParams.GRID_TYPE_NONE);
        } else {
            renderingParams.setShadingMode(RenderingParams.GOURAUD_SHADED);
            presentationParams.getContent3DParams().setGridType(RegularField3DParams.RENDER_VOLUME);
        }
        parameters.set(META_FIELD_DIMENSION_LENGTH, dims);
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {

        if (getInputFirstValue("inField") != null) {
            Parameters p;
            RegularField newInField = ((VNRegularField) getInputFirstValue("inField")).getField();
            if (newInField == null) {
                outField = null;
                prepareOutputGeometry();
                show();
                return;
            }
            boolean isDifferentField = !isFromVNA() && (inField == null || !Arrays.equals(inField.getDims(), newInField.getDims()));
            inField = newInField;
            outField = outRegularField = inField;

            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentField);
                p = parameters.getReadOnlyClone();
            }
            notifyGUIs(p, isFromVNA() || isDifferentField, false);

            if (p.get(FILENAME).isEmpty() || p.get(WRITE) == false) {
                prepareOutputGeometry();
                show();
            } else {
                BufferedImage[][] outImages = mapFieldToImages(inField, presentationParams.getDataMappingParams(), p.get(FORMAT), p.get(SLICE_AXIS), outObj.getGeometryObj().getCurrentViewer().getBackgroundColor());
                if (outImages == null) {
                    prepareOutputGeometry();
                    show();
                    return;
                }
                int ts = outImages.length;
                int n = outImages[0].length;
                if (p.get(FLIP_I)) {
                    for (int t = 0; t < ts; t++) {
                        for (int s = 0; s < n; s++) {
                            outImages[t][s] = ImageUtils.flipImageHorizontal(outImages[t][s]);
                        }
                    }
                }
                if (p.get(FLIP_J)) {
                    for (int t = 0; t < ts; t++) {
                        for (int s = 0; s < n; s++) {
                            outImages[t][s] = ImageUtils.flipImageVertical(outImages[t][s]);
                        }
                    }
                }
                if (p.get(FLIP_K)) {
                    for (int t = 0; t < ts; t++) {
                        org.apache.commons.lang3.ArrayUtils.reverse(outImages[t]);
                    }
                }

                try {
                    DecimalFormat timeFormat = new DecimalFormat("000000");
                    DecimalFormat sliceFormat = new DecimalFormat("000000");
                    String name = FilenameUtils.removeExtension(p.get(FILENAME));
                    String ext = FilenameUtils.getExtension(p.get(FILENAME));
                    String fname;
                    if (p.get(FORMAT).equals("JPEG")) {
                        for (int t = 0; t < ts; t++) {
                            for (int s = 0; s < n; s++) {
                                if (ts == 1 && n == 1) {
                                    fname = p.get(FILENAME);
                                } else if (ts > 1 && n == 1) {
                                    fname = name + "_time_" + timeFormat.format((long) t) + "." + ext;
                                } else if (ts == 1 && n > 1) {
                                    fname = name + "_slice_" + sliceFormat.format((long) s) + "." + ext;
                                } else {
                                    fname = name + "_time_" + timeFormat.format((long) t) + "_slice_" + sliceFormat.format((long) s) + "." + ext;
                                }
                                ImageUtils.writeImageJPEG(outImages[t][s], new File(fname), 0.9f);
                            }
                        }
                    } else if (p.get(FORMAT).equals("PNG")) {
                        for (int t = 0; t < ts; t++) {
                            for (int s = 0; s < n; s++) {
                                if (ts == 1 && n == 1) {
                                    fname = p.get(FILENAME);
                                } else if (ts > 1 && n == 1) {
                                    fname = name + "_time_" + timeFormat.format((long) t) + "." + ext;
                                } else if (ts == 1 && n > 1) {
                                    fname = name + "_slice_" + sliceFormat.format((long) s) + "." + ext;
                                } else {
                                    fname = name + "_time_" + timeFormat.format((long) t) + "_slice_" + sliceFormat.format((long) s) + "." + ext;
                                }
                                ImageUtils.writeImagePNG(outImages[t][s], new File(fname));
                            }
                        }
                    } else if (p.get(FORMAT).equals("BMP")) {
                        for (int t = 0; t < ts; t++) {
                            for (int s = 0; s < n; s++) {
                                if (ts == 1 && n == 1) {
                                    fname = p.get(FILENAME);
                                } else if (ts > 1 && n == 1) {
                                    fname = name + "_time_" + timeFormat.format((long) t) + "." + ext;
                                } else if (ts == 1 && n > 1) {
                                    fname = name + "_slice_" + sliceFormat.format((long) s) + "." + ext;
                                } else {
                                    fname = name + "_time_" + timeFormat.format((long) t) + "_slice_" + sliceFormat.format((long) s) + "." + ext;
                                }
                                ImageUtils.writeImageBMP(outImages[t][s], new File(fname));
                            }
                        }
                    } else if (p.get(FORMAT).equals("GIF")) {
                        for (int t = 0; t < ts; t++) {
                            for (int s = 0; s < n; s++) {
                                if (ts == 1 && n == 1) {
                                    fname = p.get(FILENAME);
                                } else if (ts > 1 && n == 1) {
                                    fname = name + "_time_" + timeFormat.format((long) t) + "." + ext;
                                } else if (ts == 1 && n > 1) {
                                    fname = name + "_slice_" + sliceFormat.format((long) s) + "." + ext;
                                } else {
                                    fname = name + "_time_" + timeFormat.format((long) t) + "_slice_" + sliceFormat.format((long) s) + "." + ext;
                                }
                                ImageUtils.writeImageGIF(outImages[t][s], new File(fname));
                            }
                        }
                    } else if (p.get(FORMAT).equals("Animated GIF")) {
                        if (ts == 1 && n == 1) {
                            fname = p.get(FILENAME);
                            ImageUtils.writeImageGIF(outImages[0][0], new File(fname));
                        } else if (ts > 1 && n == 1) {
                            fname = p.get(FILENAME);
                            BufferedImage[] tempList = new BufferedImage[ts];
                            for (int t = 0; t < ts; t++) {
                                tempList[t] = outImages[t][0];
                            }
                            ImageUtils.writeImageListGIF(tempList, new File(fname));
                        } else if (ts == 1 && n > 1) {
                            fname = p.get(FILENAME);
                            ImageUtils.writeImageListGIF(outImages[0], new File(fname));
                        } else {
                            for (int t = 0; t < ts; t++) {
                                fname = name + "_time_" + timeFormat.format((long) t) + "." + ext;
                                ImageUtils.writeImageListGIF(outImages[t], new File(fname));
                            }
                        }
                    } else if (p.get(FORMAT).equals("TIFF")) {
                        for (int t = 0; t < ts; t++) {
                            for (int s = 0; s < n; s++) {
                                if (ts == 1 && n == 1) {
                                    fname = p.get(FILENAME);
                                } else if (ts > 1 && n == 1) {
                                    fname = name + "_time_" + timeFormat.format((long) t) + "." + ext;
                                } else if (ts == 1 && n > 1) {
                                    fname = name + "_slice_" + sliceFormat.format((long) s) + "." + ext;
                                } else {
                                    fname = name + "_time_" + timeFormat.format((long) t) + "_slice_" + sliceFormat.format((long) s) + "." + ext;
                                }
                                ImageUtils.writeImageTIFF(outImages[t][s], new File(fname), "LZW");
                            }
                        }
                    } else if (p.get(FORMAT).equals("3D TIFF")) {
                        if (ts == 1 && n == 1) {
                            fname = p.get(FILENAME);
                            ImageUtils.writeImageTIFF(outImages[0][0], new File(fname), "LZW");
                        } else if (ts > 1 && n == 1) {
                            fname = p.get(FILENAME);
                            BufferedImage[] tempList = new BufferedImage[ts];
                            for (int t = 0; t < ts; t++) {
                                tempList[t] = outImages[t][0];
                            }
                            ImageUtils.writeImageListTIFF(tempList, new File(fname), "LZW");
                        } else if (ts == 1 && n > 1) {
                            fname = p.get(FILENAME);
                            ImageUtils.writeImageListTIFF(outImages[0], new File(fname), "LZW");
                        } else {
                            for (int t = 0; t < ts; t++) {
                                fname = name + "_time_" + timeFormat.format((long) t) + "." + ext;
                                ImageUtils.writeImageListTIFF(outImages[t], new File(fname), "LZW");
                            }
                        }
                    }
                } catch (IOException ex) {
                    LOGGER.error("Cannot write image to file " + p.get(FILENAME));
                    show();
                    setOutputValue("outRegularField", null);
                    return;

                }
                prepareOutputGeometry();
                show();
            }
        }
    }

    private static BufferedImage[][] mapFieldToImages(RegularField field, DataMappingParams mappingParams, String format, int sliceAxis, Color bgcolor)
    {
        if (field == null) {
            return null;
        }

        BufferedImage[][] images = null;
        byte[] colors = null;
        int[] dims = field.getDims();
        int w = 0, h = 0, n = 1, ts = field.getNFrames();
        float[] timeMoments = field.getTimesteps();
        float currentTime = field.getCurrentTime();
        int imageType;

        switch (dims.length) {
            case 1:
                w = dims[0];
                h = 1;
                break;
            case 2:
                w = dims[0];
                h = dims[1];
                break;
            default:
                switch (sliceAxis) {
                    case 0:
                        w = dims[1];
                        h = dims[2];
                        n = dims[0];
                        break;
                    case 1:
                        w = dims[0];
                        h = dims[2];
                        n = dims[1];
                        break;
                    default:
                        w = dims[0];
                        h = dims[1];
                        n = dims[2];
                }
        }

        TransparencyParams transparencyParams = mappingParams.getTransparencyParams();
        if (field.getComponent(transparencyParams.getComponentRange().getComponentName()) != null || field.hasMask()) {
            imageType = BufferedImage.TYPE_INT_ARGB;
        } else {
            imageType = BufferedImage.TYPE_INT_RGB;
        }

        images = new BufferedImage[ts][n];
        if (dims.length == 3 && sliceAxis != 2) {
            RegularField[] rf = new RegularField[n];
            for (int s = 0; s < n; s++) {
                rf[s] = SliceRegularField.sliceField(field, sliceAxis, s, true);
                for (int t = 0; t < ts; t++) {
                    rf[s].setCurrentTime(timeMoments[t]);
                    colors = ColorMapper.map(rf[s], mappingParams, colors);
                    if (imageType == BufferedImage.TYPE_INT_ARGB && field.getComponent(transparencyParams.getComponentRange().getComponentName()) != null && !field.hasMask()) {
                        colors = ColorMapper.mapTransparency(rf[s], mappingParams.getTransparencyParams(), colors);
                        images[t][s] = new BufferedImage(w, h, imageType);
                        WritableRaster raster = images[t][s].getRaster();
                        for (int j = 0, l = 0; j < h; j++) {
                            for (int i = 0; i < w; i++, l++) {
                                raster.setSample(i, h - j - 1, 0, (int) (colors[4 * l] & 0xff));
                                raster.setSample(i, h - j - 1, 1, (int) (colors[4 * l + 1] & 0xff));
                                raster.setSample(i, h - j - 1, 2, (int) (colors[4 * l + 2] & 0xff));
                                raster.setSample(i, h - j - 1, 3, (int) (colors[4 * l + 3] & 0xff));
                            }
                        }
                    } else if (imageType == BufferedImage.TYPE_INT_ARGB && field.getComponent(transparencyParams.getComponentRange().getComponentName()) == null && field.hasMask()) {
                        LogicLargeArray mask = rf[s].getCurrentMask();
                        images[t][s] = new BufferedImage(w, h, imageType);
                        WritableRaster raster = images[t][s].getRaster();
                        for (int j = 0, l = 0; j < h; j++) {
                            for (int i = 0; i < w; i++, l++) {
                                raster.setSample(i, h - j - 1, 0, (int) (colors[4 * l] & 0xff));
                                raster.setSample(i, h - j - 1, 1, (int) (colors[4 * l + 1] & 0xff));
                                raster.setSample(i, h - j - 1, 2, (int) (colors[4 * l + 2] & 0xff));
                                raster.setSample(i, h - j - 1, 3, mask.getByte(j * w + i) == 0 ? 0 : 255);
                            }
                        }
                    } else if (imageType == BufferedImage.TYPE_INT_ARGB && field.getComponent(transparencyParams.getComponentRange().getComponentName()) != null && field.hasMask()) {
                        colors = ColorMapper.mapTransparency(rf[s], mappingParams.getTransparencyParams(), colors);
                        LogicLargeArray mask = rf[s].getCurrentMask();
                        images[t][s] = new BufferedImage(w, h, imageType);
                        WritableRaster raster = images[t][s].getRaster();
                        for (int j = 0, l = 0; j < h; j++) {
                            for (int i = 0; i < w; i++, l++) {
                                raster.setSample(i, h - j - 1, 0, (int) (colors[4 * l] & 0xff));
                                raster.setSample(i, h - j - 1, 1, (int) (colors[4 * l + 1] & 0xff));
                                raster.setSample(i, h - j - 1, 2, (int) (colors[4 * l + 2] & 0xff));
                                raster.setSample(i, h - j - 1, 3, mask.getByte(j * w + i) == 0 ? 0 : (int) (colors[4 * l + 3] & 0xff));
                            }
                        }
                    } else {
                        images[t][s] = new BufferedImage(w, h, imageType);
                        WritableRaster raster = images[t][s].getRaster();
                        for (int j = 0, l = 0; j < h; j++) {
                            for (int i = 0; i < w; i++, l++) {
                                raster.setSample(i, h - j - 1, 0, (int) (colors[4 * l] & 0xff));
                                raster.setSample(i, h - j - 1, 1, (int) (colors[4 * l + 1] & 0xff));
                                raster.setSample(i, h - j - 1, 2, (int) (colors[4 * l + 2] & 0xff));
                            }
                        }
                    }
                    if (!(format.equals("PNG") || format.equals("TIFF") || format.equals("3D TIFF")) && imageType == BufferedImage.TYPE_INT_ARGB) {
                        images[t][s] = ImageUtils.removeAlpha(images[t][s], bgcolor);
                    }
                }
            }
        } else {
            colors = ColorMapper.map(field, mappingParams, colors);
            for (int t = 0; t < ts; t++) {
                field.setCurrentTime(timeMoments[t]);
                if (imageType == BufferedImage.TYPE_INT_ARGB && field.getComponent(transparencyParams.getComponentRange().getComponentName()) != null && !field.hasMask()) {
                    colors = ColorMapper.mapTransparency(field, mappingParams.getTransparencyParams(), colors);
                    for (int s = 0; s < n; s++) {
                        images[t][s] = new BufferedImage(w, h, imageType);
                        WritableRaster raster = images[t][s].getRaster();
                        for (int j = 0, l = 0; j < h; j++) {
                            for (int i = 0; i < w; i++, l++) {
                                raster.setSample(i, h - j - 1, 0, (int) (colors[s * 4 * w * h + 4 * l] & 0xff));
                                raster.setSample(i, h - j - 1, 1, (int) (colors[s * 4 * w * h + 4 * l + 1] & 0xff));
                                raster.setSample(i, h - j - 1, 2, (int) (colors[s * 4 * w * h + 4 * l + 2] & 0xff));
                                raster.setSample(i, h - j - 1, 3, (int) (colors[s * 4 * w * h + 4 * l + 3] & 0xff));
                            }
                        }
                    }
                } else if (imageType == BufferedImage.TYPE_INT_ARGB && field.getComponent(transparencyParams.getComponentRange().getComponentName()) == null && field.hasMask()) {
                    LogicLargeArray mask = field.getCurrentMask();
                    for (int s = 0; s < n; s++) {
                        images[t][s] = new BufferedImage(w, h, imageType);
                        WritableRaster raster = images[t][s].getRaster();
                        for (int j = 0, l = 0; j < h; j++) {
                            for (int i = 0; i < w; i++, l++) {
                                raster.setSample(i, h - j - 1, 0, (int) (colors[s * 4 * w * h + 4 * l] & 0xff));
                                raster.setSample(i, h - j - 1, 1, (int) (colors[s * 4 * w * h + 4 * l + 1] & 0xff));
                                raster.setSample(i, h - j - 1, 2, (int) (colors[s * 4 * w * h + 4 * l + 2] & 0xff));
                                raster.setSample(i, h - j - 1, 3, mask.getByte(j * w + i) == 0 ? 0 : 255);
                            }
                        }
                    }
                } else if (imageType == BufferedImage.TYPE_INT_ARGB && field.getComponent(transparencyParams.getComponentRange().getComponentName()) != null && field.hasMask()) {
                    colors = ColorMapper.mapTransparency(field, mappingParams.getTransparencyParams(), colors);
                    LogicLargeArray mask = field.getCurrentMask();
                    for (int s = 0; s < n; s++) {
                        images[t][s] = new BufferedImage(w, h, imageType);
                        WritableRaster raster = images[t][s].getRaster();
                        for (int j = 0, l = 0; j < h; j++) {
                            for (int i = 0; i < w; i++, l++) {
                                raster.setSample(i, h - j - 1, 0, (int) (colors[s * 4 * w * h + 4 * l] & 0xff));
                                raster.setSample(i, h - j - 1, 1, (int) (colors[s * 4 * w * h + 4 * l + 1] & 0xff));
                                raster.setSample(i, h - j - 1, 2, (int) (colors[s * 4 * w * h + 4 * l + 2] & 0xff));
                                raster.setSample(i, h - j - 1, 3, mask.getByte(j * w + i) == 0 ? 0 : (int) (colors[s * 4 * w * h + 4 * l + 3] & 0xff));
                            }
                        }
                    }
                } else {
                    for (int s = 0; s < n; s++) {
                        images[t][s] = new BufferedImage(w, h, imageType);
                        WritableRaster raster = images[t][s].getRaster();
                        for (int j = 0, l = 0; j < h; j++) {
                            for (int i = 0; i < w; i++, l++) {
                                raster.setSample(i, h - j - 1, 0, (int) (colors[s * 4 * w * h + 4 * l] & 0xff));
                                raster.setSample(i, h - j - 1, 1, (int) (colors[s * 4 * w * h + 4 * l + 1] & 0xff));
                                raster.setSample(i, h - j - 1, 2, (int) (colors[s * 4 * w * h + 4 * l + 2] & 0xff));
                            }
                        }
                    }
                }
                if (!(format.equals("PNG") || format.equals("TIFF") || format.equals("3D TIFF")) && imageType == BufferedImage.TYPE_INT_ARGB) {
                    for (int s = 0; s < n; s++) {
                        images[t][s] = ImageUtils.removeAlpha(images[t][s], bgcolor);
                    }
                }
            }
            field.setCurrentTime(currentTime);
        }
        return images;
    }

}
