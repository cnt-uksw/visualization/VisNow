/* VisNow
   Copyright (C) 2006-2019 University of Warsaw, ICM
   Copyright (C) 2020 onward visnow.org
   All rights reserved. 

This file is part of GNU Classpath.

GNU Classpath is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

GNU Classpath is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU Classpath; see the file COPYING.  If not, write to the 
University of Warsaw, Interdisciplinary Centre for Mathematical and 
Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

Linking this library statically or dynamically with other modules is
making a combined work based on this library.  Thus, the terms and
conditions of the GNU General Public License cover the whole
combination.

As a special exception, the copyright holders of this library give you
permission to link this library with independent modules to produce an
executable, regardless of the license terms of these independent
modules, and to copy and distribute the resulting executable under
terms of your choice, provided that you also meet, for each linked
independent module, the terms and conditions of the license of that
module.  An independent module is a module which is not derived from
or based on this library.  If you modify this library, you may extend
this exception to your version of the library, but you are not
obligated to do so.  If you do not wish to do so, delete this
exception statement from your version. */
package org.visnow.vn.lib.basic.writers.CSVWriter;

import java.io.File;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.system.utils.usermessage.UserMessage;
import javax.swing.JOptionPane;
import org.visnow.jscic.RegularField;
import static org.visnow.vn.lib.basic.writers.CSVWriter.CSVWriterShared.*;

/**
 * Writer for CSV files.
 * Only VisNow RegularFields are supported.
 * ObjectDataArray components are not supported.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class CSVWriter extends ModuleCore
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI ui = null;
    protected RegularField inField;
    private boolean write = false;

    public CSVWriter()
    {
        parameters.addParameterChangelistener((String name) -> {
            if (name.equals("Write")) {
                write = true;
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(() -> {
            ui = new GUI();
            setPanel(ui);
            ui.setParameters(parameters);
        });

    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILENAME, ""),
            new Parameter<>(SELECTED_COMPONENT_INDEX, 0),
            new Parameter<>(FIELD_DELIMITER, ","),
            new Parameter<>(META_COMPONENT_NAMES, new String[0]),
            new Parameter<>(WRITE, false)
        };
    }
    
     private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        parameters.set(META_COMPONENT_NAMES, inField != null ? inField.getComponentNames() : new String[0]);
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        ui.updateGUI(clonedParameterProxy);
    }

    private static boolean canWriteToOutputFile(String path)
    {
        boolean result = true;
        File outFile = new File(path);
        if (!outFile.getParentFile().canWrite()) {
            JOptionPane.showMessageDialog(null, "Cannot write file (check write permissions)");
            return false;
        }
        if (outFile.exists()) {
            Object ans = JOptionPane.showConfirmDialog(null, "File " + outFile.getName() + " exists.\nOwerwrite?", "", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            result = ans instanceof Integer && (Integer) ans == JOptionPane.YES_OPTION;
        }
        return result;
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            Parameters p;
            RegularField newField = (RegularField)((VNField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA() && (inField == null || inField.getTimestamp() != newField.getTimestamp());
            inField = newField;
            RegularField columnLabels = null;
            RegularField rowLabels = null;
                
            if (getInputFirstValue("columnLabels") != null) {
                columnLabels = (RegularField)((VNField) getInputFirstValue("columnLabels")).getField();
            }
            if (getInputFirstValue("rowLabels") != null) {
                rowLabels = (RegularField)((VNField) getInputFirstValue("rowLabels")).getField();
            }
            
            synchronized (parameters) {
                validateParamsAndSetSmart(true);
                p = parameters.getReadOnlyClone();
            }
            notifyGUIs(p, isFromVNA() || isDifferentField, true);

            String outFileName = p.get(FILENAME);
            if (write && outFileName != null && !outFileName.isEmpty()) {
                if (canWriteToOutputFile(outFileName)) {
                    if (CSVWriterCore.writeField(inField, columnLabels, rowLabels, p.get(SELECTED_COMPONENT_INDEX), p.get(FILENAME), p.get(FIELD_DELIMITER))) {
                        VisNow.get().userMessageSend(new UserMessage("VisNow", "", "CSV file written.", "", Level.INFO));
                    } else {
                        VisNow.get().userMessageSend(new UserMessage("VisNow", "", "Error writing CSV file.", "Only 1D and 2D regular fields with scalar components are supported.", Level.ERROR));
                    }
                }
                write = false;
            }
        }
    }
}
