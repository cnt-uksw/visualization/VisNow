/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.testdata.TestLineMesh;

import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.testdata.TestLineMesh.TestLineMeshShared.MESH_SIZE;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof Nowinski (know@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class TestLineMesh extends OutFieldVisualizationModule
{

    protected GUI computeUI = null;
    protected boolean fromUI = false;
    public static OutputEgg[] outputEggs = null;

    /**
     * Creates a new instance of TestGeometryObject
     */
    public TestLineMesh()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(MESH_SIZE, 300),};
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    public void createTestPoints(Parameters p)
    {
        int level = (int) (sqrt((int)p.get(MESH_SIZE)));
        if (level < 2)
            level = 2;
        int nPoints = 2 + 2 * (level - 1) * level;
        outIrregularField = new IrregularField(nPoints);
        FloatLargeArray coords = new FloatLargeArray((long) nPoints * 3, false);
        int nSegments = 2 * (2 * (level - 1) + 1) * level;
        FloatLargeArray data = new FloatLargeArray(nPoints, false);
        FloatLargeArray data1 = new FloatLargeArray(nPoints, false);
        int[] cells = new int[2 * nSegments];
        int[] cellDataIndices = new int[nSegments];
        float[] cellData = new float[nSegments];
        float[] cellData1 = new float[nSegments];
        double d = PI / level;
        coords.setFloat(0, 0);
        coords.setFloat(1, 0);
        coords.setFloat(2, 1);
        data.setFloat(0, 1);
        data1.setFloat(0, 0);
        long k = 3;
        int l = 0;
        int pointNumber = 1;
        for (int i = 1; i < level; i++) {
            float cosPsi = (float) cos(i * d);
            float sinPsi = (float) sin(i * d);
            for (int j = 0; j < 2 * level; j++, k += 3, pointNumber += 1, l += 4) {
                data.setFloat(pointNumber, cosPsi);
                data1.setFloat(pointNumber, sinPsi * (float) cos(4 * j * d));
                coords.setFloat(k, (float) (sinPsi * cos(j * d)));
                coords.setFloat(k + 1, (float) (sinPsi * sin(j * d)));
                coords.setFloat(k + 2, cosPsi);
                if (i == 1)
                    cells[l] = 0;
                else
                    cells[l] = pointNumber - 2 * level;
                cells[l + 1] = cells[l + 2] = pointNumber;
                if (j < 2 * level - 1)
                    cells[l + 3] = pointNumber + 1;
                else
                    cells[l + 3] = pointNumber - (2 * level - 1);
            }
        }
        data.setFloat(pointNumber, -1);
        data1.setFloat(pointNumber, 0);
        coords.setFloat(k, 0);
        coords.setFloat(k + 1, 0);
        coords.setFloat(k + 2, -1);

        pointNumber = 1 + 2 * (level - 2) * level;
        for (int j = 0; j < 2 * level; j++, pointNumber += 1, l += 2) {
            cells[l] = pointNumber;
            cells[l + 1] = nPoints - 1;
        }
        for (int i = 0; i < cellData.length; i++) {
            cellData[i] = cellDataIndices[i] = i;
            cellData1[i] = coords.getFloat(3 * cells[2 * i]);
        }
        CellArray ca = new CellArray(CellType.SEGMENT, cells, null, cellDataIndices);
        CellSet cs = new CellSet();
        cs.setCellArray(ca);
        cs.addComponent(DataArray.create(cellData, 1, "c"));
        cs.addComponent(DataArray.create(cellData1, 1, "c1"));
        cs.generateDisplayData(coords);
        FloatLargeArray v = new FloatLargeArray(coords.length(), false);
        for (long i = 0; i < v.length(); i++)
            v.setFloat(i, coords.getFloat(i) / (2 + coords.getFloat(i)));
        outIrregularField.addCellSet(cs);
        outIrregularField.setCurrentCoords(coords);
        outIrregularField.addComponent(DataArray.create(data, 1, "z"));
        outIrregularField.addComponent(DataArray.create(data1, 1, "z2"));
        outIrregularField.addComponent(DataArray.create(v, 3, "v"));
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        Parameters p;
        synchronized (parameters) {
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, false, false);

        createTestPoints(p);
        setOutputValue("outField", new VNIrregularField(outIrregularField));
        outField = outIrregularField;
        outField.setName("Test line mesh");
        prepareOutputGeometry();
        show();
    }
}
