/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.testdata.TestPoints;

import java.util.ArrayList;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jscic.PointField;
import org.visnow.jscic.TimeData;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.testdata.TestPoints.TestPointsShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNPointField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class TestPoints extends OutFieldVisualizationModule
{

    protected GUI computeUI = null;
    protected boolean fromUI = false;
    public static OutputEgg[] outputEggs = null;

    /**
     * Creates a new instance of TestGeometryObject
     */
    public TestPoints()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(POINT_COUNT, 100000L),
            new Parameter<>(SUBSETS, true),};
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    public void createTestPoints(Parameters p)
    {
        long nPoints = p.get(POINT_COUNT);
        outField = new PointField(nPoints);
        outPointField = (PointField) outField;
        FloatLargeArray coords = new FloatLargeArray((long) nPoints * 3, false);
        FloatLargeArray data   = new FloatLargeArray(nPoints, false);
        UnsignedByteLargeArray rData  = new UnsignedByteLargeArray(nPoints, false);
        FloatLargeArray vData  = new FloatLargeArray((long) nPoints * 3, false);
        float[] c = {1, 1, -1};
        if (p.get(SUBSETS)) {
            UnsignedByteLargeArray sArray = new UnsignedByteLargeArray(nPoints);
            for (long i = 0; i < nPoints; i++) {
                float r = 0, s = 0;
                for (int j = 0; j < 3; j++) {
                    float t = (float) random() - .5f;
                    coords.setFloat(3 * i + j, t);
                    r += t * t;
                    s += c[j] * t * t;
                }
                rData.setFloat(i, 40 * Math.abs(s));
                vData.setFloat(3 * i, -coords.getFloat(3 * i + 1) + (float) random() / 5 - .1f);
                vData.setFloat(3 * i + 1, -coords.getFloat(3 * i) + (float) random() / 5 - .1f);
                vData.setFloat(3 * i + 2, -coords.getFloat(3 * i + 2) + (float) random() / 5 - .1f);
                sArray.setInt(i, Math.min(4, (int)(30 * r)));
        }
        outPointField.addComponent(DataArray.create(sArray, 1, "subsets", "", 
                                   new String[] {"core", "layer 1", "layer 2", "layer 3", "extern"}).
                                   preferredRange(0, 4));
        }
        ArrayList<Float> timeMoments = new ArrayList<>();
        ArrayList<LargeArray> timeCrds = new ArrayList<>();
        timeMoments.add(0f);
        timeCrds.add(coords);
        for (int i = 1; i < 50; i++) {
            FloatLargeArray ncrds = new FloatLargeArray((long) nPoints * 3, false);
            for (long j = 0; j < nPoints; j++) {
                ncrds.setFloat(3 * j, .98f * coords.getFloat(3 * j));
                ncrds.setFloat(3 * j + 1, .9848f * coords.getFloat(3 * j + 1) - .1736f * coords.getFloat(3 * j + 2));
                ncrds.setFloat(3 * j + 2, .9848f * coords.getFloat(3 * j + 2) + .1736f * coords.getFloat(3 * j + 1));
            }
            timeMoments.add((float)i);
            timeCrds.add(ncrds);
            coords = ncrds;
        }
        TimeData crds = new TimeData(timeMoments, timeCrds, 0);
        outPointField.setCoords(crds);
        outPointField.addComponent(DataArray.create(data, 1, "points"));
        outPointField.addComponent(DataArray.create(rData, 1, "r"));
        outPointField.addComponent(DataArray.create(vData, 3, "vectors"));
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        Parameters p;
        synchronized (parameters) {
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, false, false);
        
        createTestPoints(p);
        prepareOutputGeometry();
        show();
        setOutputValue("outField", new VNPointField(outPointField));
    }
}
