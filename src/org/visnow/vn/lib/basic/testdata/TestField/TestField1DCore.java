/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.testdata.TestField;

import java.util.ArrayList;
import java.util.List;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.engine.core.ProgressAgent;
import static org.visnow.vn.lib.basic.testdata.TestField.TestFieldShared.*;

/**
 * 1D data components computational core.
 * <p>
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class TestField1DCore
{

    static List<DataArray> createDataArrays(long resolution, int[] components, double[][] randoms, ProgressAgent progressAgent)
    {
        FloatLargeArray dataGaussians = null;
        FloatLargeArray dataTrig = null;
        FloatLargeArray dataVectorTrig = null;
        FloatLargeArray dataTime0 = null, dataTime1 = null, dataTime2 = null;
        boolean gaussiansQ = false;
        boolean trigFunctionQ = false;
        boolean vectorTrigQ = false;
        boolean timeFieldQ = false;

        // 10/150
        progressAgent.increase((int) resolution / 10);

        for (int i = 0; i < components.length; i++) {
            String componentName = FIELD_NAMES_1D[components[i]];
            if (componentName.equals(GAUSSIANS)) gaussiansQ = true;
            else if (componentName.equals(TRIG_FUNCTION)) trigFunctionQ = true;
            else if (componentName.equals(VECTOR_TRIG)) vectorTrigQ = true;
            else if (componentName.equals(TIME_FIELD)) timeFieldQ = true;
            else throw new IllegalStateException("Incorrect field component: " + componentName);
        }
        if (gaussiansQ) dataGaussians = new FloatLargeArray(resolution, false);
        if (trigFunctionQ) dataTrig = new FloatLargeArray(resolution, false);
        if (vectorTrigQ) dataVectorTrig = new FloatLargeArray(3 * resolution, false);
        if (timeFieldQ) {
            dataTime0 = new FloatLargeArray(resolution, false);
            dataTime1 = new FloatLargeArray(resolution, false);
            dataTime2 = new FloatLargeArray(resolution, false);
        }

        // 20/150
        progressAgent.increase((int) resolution / 10);
        FloatLargeArray c = new FloatLargeArray((int)Math.sqrt(resolution));
        if (trigFunctionQ) 
            for (long i = 0; i < c.length(); i++) 
                c.set(i,(float)Math.random() - .5f);
        for (long i = 0; i < resolution; i++) {
            double u = (2. * i - resolution) / resolution;
            double s = 0;
            for (int k = 0; k < randoms.length; k++) {
                double r = randoms[k][4] * Math.exp(-(u - randoms[k][0]) * (u - randoms[k][0]) / randoms[k][2]);
                s += r;
            }

            if (gaussiansQ) dataGaussians.setFloat(i, (float) s);

            if (trigFunctionQ) {
                double d = 0;
                for (int j = 0; j < c.length(); j++) 
                    d += c.get(j) * Math.cos((Math.PI * i * j) / resolution);
                dataTrig.setFloat(i, (float) d);
            }

            if (vectorTrigQ)
                for (long v = 0; v < 3; v++)
                    dataVectorTrig.setFloat(3 * i + v, (float) (cos(6 * u) + u / (v + 1)));

            if (timeFieldQ) {
                double r0 = .1234567;
                double r000 = sqrt((u - r0) * (u - r0));
                double r110 = sqrt((u + r0) * (u + r0));
                dataTime0.setFloat(i, (float) (1 / (.5 + u * u)));
                dataTime1.setFloat(i, (float) (1 / (.5 + r000)));
                dataTime2.setFloat(i, (float) (1 / (.5 + r000) + 1 / (.5 + r110)));
            }

        }

        List<DataArray> dataArrays = new ArrayList<>();
        if (gaussiansQ) dataArrays.add(DataArray.create(dataGaussians, 1, "gaussians"));
        if (trigFunctionQ) dataArrays.add(DataArray.create(dataTrig, 1, "trig_function").
            preferredRanges(-4, 4, -40, 40));
        if (vectorTrigQ) dataArrays.add(DataArray.create(dataVectorTrig, 3, "vector_trig"));
        if (timeFieldQ) {
            DataArray da = DataArray.create(dataTime2, 1, TIME_FIELD);
            da.addRawArray(dataTime1, 1);
            da.addRawArray(dataTime0, 2);
            dataArrays.add(da);
        }

        return dataArrays;
    }

}
