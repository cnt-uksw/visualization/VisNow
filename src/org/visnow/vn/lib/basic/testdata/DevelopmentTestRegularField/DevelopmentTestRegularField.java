/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.testdata.DevelopmentTestRegularField;

import org.apache.log4j.Logger;

import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.*;

import org.visnow.vn.lib.basic.testdata.DevelopmentTestRegularField.Geometries.*;
import org.visnow.vn.lib.basic.testdata.DevelopmentTestRegularField.Components.*;

/**
 * The module provides a filed (1D, 2D or 3D) with somehow weird components.
 * Used as test input field for other modules.
 *
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl), Warsaw University, ICM
 */
public final class DevelopmentTestRegularField extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(DevelopmentTestRegularField.class);

    public static OutputEgg[] outputEggs = null;

    protected GUI computeUI;
    protected Params params;

    /* Must be changed manually, if the change of components' package name did happen. */
    private static final String PACKAGE_DATA_COMPONENTS = "org.visnow.vn.lib.basic.testdata.DevelopmentTestRegularField.Components";
    private static final String PACKAGE_GEOMETRIES = "org.visnow.vn.lib.basic.testdata.DevelopmentTestRegularField.Geometries";

    private static final ArrayList<Class<? extends AbstractComponent>> components;
    private static final ArrayList<Class<? extends AbstractGeometry>> geometries;

    static {
        components = new ArrayList<>();
        geometries = new ArrayList<>();
    }

    /* TODO User must manually add new component and geometry classes in the
     * following static block. */
    static {
        components.add(BigOscillationsInteger.class);
        components.add(BigOscillationsDouble.class);
        components.add(BigOscillationsFloat.class);
        components.add(OscillationsBit.class);
        components.add(OscillationsByte.class);
        components.add(OscillationsShort.class);
        components.add(SmallOscillationsHighValueDouble.class);
        components.add(SmallOscillationsHighValueFloat.class);
        components.add(SmallOscillationsDouble.class);
        components.add(SmallOscillationsFloat.class);
        components.add(ConstantBitTrue.class);
        components.add(ConstantBitFalse.class);
        components.add(ConstantByte.class);
        components.add(ConstantShort.class);
        components.add(ConstantInteger.class);
        components.add(ConstantDouble.class);
        components.add(ConstantFloat.class);
        components.add(ConstantComplex.class);
        components.add(InfinityDouble.class);
        components.add(InfinityFloat.class);
        components.add(NaNDouble.class);
        components.add(NaNFloat.class);
        components.add(Vector2Byte.class);
        components.add(Vector2Short.class);
        components.add(Vector2Integer.class);
        components.add(Vector2Double.class);
        components.add(Vector2Float.class);
        components.add(Vector3Byte.class);
        components.add(Vector3Short.class);
        components.add(Vector3Integer.class);
        components.add(Vector3Double.class);
        components.add(Vector3Float.class);
        components.add(ComplexComponent.class);
        components.add(StringComponent.class);

        geometries.add(AffineOrthogonal.class);
        geometries.add(AffineSkew.class);
        geometries.add(ExplicitToroidal.class);
    }

    private static final Map<String, String> componentsNamesToClassNames;
    private static final Map<String, String> geometriesNamesToClassNames;

    static {
        componentsNamesToClassNames = new HashMap<>();
        geometriesNamesToClassNames = new HashMap<>();
    }

    static {
        for (int i = 0; i < components.size(); ++i) {
            componentsNamesToClassNames.put(getComponentName(components.get(i)), components.get(i).getSimpleName());
        }
        for (int i = 0; i < geometries.size(); ++i) {
            geometriesNamesToClassNames.put(getGeometryName(geometries.get(i)), geometries.get(i).getSimpleName());
        }
    }

    public DevelopmentTestRegularField()
    {
        parameters = params = new Params();
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI(params);
            }
        });
        ui.addComputeGUI(computeUI);
        params.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                startAction();
            }
        });
        setPanel(ui);
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    /**
     * Extracts class simple names (names without package path) from given ArrayList.
     * 
     * @param classArray ArrayList to extract class names from.
     * @return Array of String containing names of classes. Order is preserved.
     */
    private static String[] getClassNames(ArrayList<?> classArray)
    {
        final int NUMBER_OF_RECORDS = classArray.size();
        String[] classNames = new String[NUMBER_OF_RECORDS];

        for (int j = 0; j < NUMBER_OF_RECORDS; ++j) {
            classNames[j] = ((Class) (classArray.get(j))).getSimpleName();
        }
        return classNames;
    }

    private static String getComponentName(Class<? extends AbstractComponent> componentClass)
    {
        Method getNameMethod = null;
        String name = null;
        try {
            getNameMethod = componentClass.getMethod("getName", new Class[]{});
        } catch (NoSuchMethodException ex) {
            LOGGER.info("getName() method not declared. Using default component name.", ex);
            name = componentClass.getSimpleName();
        } catch (SecurityException ex) {
            LOGGER.fatal(null, ex);
            throw new RuntimeException();
        }

        final String methodInvokationErrorMessage = "Check declaration of getName() method.";
        if (getNameMethod != null) {
            try {
                name = (String) getNameMethod.invoke(null, (Object[]) null);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOGGER.fatal(methodInvokationErrorMessage, ex);
                throw new RuntimeException();
            }
        }
        return name;
    }

    static final String[] getComponentsNames()
    {
        final int NUMBER_OF_COMPONENTS = components.size();
        String[] componentsNames = new String[NUMBER_OF_COMPONENTS];

        for (int i = 0; i < components.size(); i++) {
            componentsNames[i] = getComponentName(components.get(i));
        }
        return componentsNames;
    }

    public static final String getComponentClassName(String componentName)
    {
        return componentsNamesToClassNames.get(componentName);
    }

    public static final String[] getComponentsClassNames()
    {
        return getClassNames(components);
    }

    public static final String getComponentsPackageName()
    {
        return PACKAGE_GEOMETRIES;
    }

    static final String[] getGeometriesNames()
    {
        final int NUMBER_OF_COMPONENTS = geometries.size();
        String[] geometriesNames = new String[NUMBER_OF_COMPONENTS];

        for (int i = 0; i < geometries.size(); i++) {
            geometriesNames[i] = getGeometryName(geometries.get(i));
        }
        return geometriesNames;
    }

    private static String getGeometryName(Class<? extends AbstractGeometry> geometryClass)
    {
        Method getNameMethod = null;
        String name = null;
        try {
            getNameMethod = geometryClass.getMethod("getName", new Class[]{});
        } catch (NoSuchMethodException ex) {
            LOGGER.info("getName() method not declared. Using default component name.", ex);
            name = geometryClass.getSimpleName();
        } catch (SecurityException ex) {
            LOGGER.fatal(null, ex);
            throw new RuntimeException();
        }

        final String methodInvokationErrorMessage = "Check declaration of getName() method.";
        if (getNameMethod != null) {
            try {
                name = (String) getNameMethod.invoke(null, (Object[]) null);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOGGER.fatal(methodInvokationErrorMessage, ex);
                throw new RuntimeException();
            }
        }
        return name;
    }

    public static final String getGeometryClassName(String geometryName)
    {
        return geometriesNamesToClassNames.get(geometryName);
    }

    public static final String[] getGeometriesClassNames()
    {
        return getClassNames(geometries);
    }

    public static final String[] getGeometriesClassNames(int n)
    {
        ArrayList<String> geometriesClassNamesList = new ArrayList();
        for (Class<? extends AbstractGeometry> geometry : geometries) {
            switch (n) {
                case 3:
                    if (isGeometry3D(geometry)) {
                        geometriesClassNamesList.add(geometry.getSimpleName());
                    }
                    break;
                case 2:
                    if (isGeometry2D(geometry)) {
                        geometriesClassNamesList.add(((Class) geometry).getSimpleName());
                    }
                    break;
                case 1:
                    if (isGeometry1D(geometry)) {
                        geometriesClassNamesList.add(geometry.getSimpleName());
                    }
                    break;
                default:
                    throw new RuntimeException("Only 1, 2 or 3 dimensional geometries are supported.");
            }
        }
        String[] geometriesClassNames = new String[geometriesClassNamesList.size()];
        geometriesClassNamesList.toArray(geometriesClassNames);
        return geometriesClassNames;
    }

    public static final String[] getGeometries1DClassNames()
    {
        return getGeometriesClassNames(1);
    }

    public static final String[] getGeometries2DClassNames()
    {
        return getGeometriesClassNames(2);
    }

    public static final String[] getGeometries3DClassNames()
    {
        return getGeometriesClassNames(3);
    }

    public static final String[] getGeometriesAffineClassNames()
    {
        ArrayList<String> geometriesClassNamesList = new ArrayList();
        for (Class<? extends AbstractGeometry> geometry : geometries) {
            if (isGeometryAffine(geometry)) {
                geometriesClassNamesList.add(geometry.getSimpleName());
            }
        }
        String[] geometriesClassNames = new String[geometriesClassNamesList.size()];
        geometriesClassNamesList.toArray(geometriesClassNames);
        return geometriesClassNames;
    }

    public static final String[] getGeometriesExplicitClassNames()
    {
        ArrayList<String> geometriesClassNamesList = new ArrayList();
        for (Class<? extends AbstractGeometry> geometry : geometries) {
            if (isGeometryExplicit(geometry)) {
                geometriesClassNamesList.add(geometry.getSimpleName());
            }
        }
        String[] geometriesClassNames = new String[geometriesClassNamesList.size()];
        geometriesClassNamesList.toArray(geometriesClassNames);
        return geometriesClassNames;
    }

    public static final String getGeometriesPackageName()
    {
        return PACKAGE_DATA_COMPONENTS;
    }

    static final String[] getGeometriesNames(int n)
    {
        ArrayList<String> geometriesNamesList = new ArrayList();
        for (Class<? extends AbstractGeometry> geometry : geometries) {
            switch (n) {
                case 3:
                    if (isGeometry3D(geometry)) {
                        geometriesNamesList.add(getGeometryName(geometry));
                    }
                    break;
                case 2:
                    if (isGeometry2D(geometry)) {
                        geometriesNamesList.add(getGeometryName(geometry));
                    }
                    break;
                case 1:
                    if (isGeometry1D(geometry)) {
                        geometriesNamesList.add(getGeometryName(geometry));
                    }
                    break;
                default:
                    throw new RuntimeException("Only 1, 2 or 3 dimensional geometries are supported.");
            }
        }
        String[] geometriesNames = new String[geometriesNamesList.size()];
        geometriesNamesList.toArray(geometriesNames);
        return geometriesNames;
    }

    static final String[] getGeometries1DNames()
    {
        return getGeometriesNames(1);
    }

    static final String[] getGeometries2DNames()
    {
        return getGeometriesNames(2);
    }

    static final String[] getGeometries3DNames()
    {
        return getGeometriesNames(3);
    }

    private static boolean isGeometry1D(Class<? extends AbstractGeometry> geometryClass)
    {
        Method is1DMethod = null;
        boolean is1D = false;
        try {
            is1DMethod = geometryClass.getMethod("is1D", new Class[]{});
        } catch (NoSuchMethodException ex) {
            LOGGER.fatal("is1D() method not declared.", ex);
        } catch (SecurityException ex) {
            LOGGER.fatal(null, ex);
            throw new RuntimeException();
        }

        final String methodInvokationErrorMessage = "Check declaration of is1D() method.";
        if (is1DMethod != null) {
            try {
                is1D = ((Boolean) is1DMethod.invoke(null, (Object[]) null));
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOGGER.fatal(methodInvokationErrorMessage, ex);
                throw new RuntimeException();
            }
        }
        return is1D;
    }

    private static boolean isGeometry2D(Class<? extends AbstractGeometry> geometryClass)
    {
        Method is2DMethod = null;
        boolean is2D = false;
        try {
            is2DMethod = geometryClass.getMethod("is2D", new Class[]{});
        } catch (NoSuchMethodException ex) {
            LOGGER.fatal("is2D() method not declared.", ex);
            throw new RuntimeException();
        } catch (SecurityException ex) {
            LOGGER.fatal(null, ex);
            throw new RuntimeException();
        }

        final String methodInvokationErrorMessage = "Check declaration of is2D() method.";
        if (is2DMethod != null) {
            try {
                is2D = ((Boolean) is2DMethod.invoke(null, (Object[]) null));
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOGGER.fatal(methodInvokationErrorMessage, ex);
                throw new RuntimeException();
            }
        }
        return is2D;
    }

    private static boolean isGeometry3D(Class<? extends AbstractGeometry> geometryClass)
    {
        Method is3DMethod = null;
        boolean is3D = false;
        try {
            is3DMethod = geometryClass.getMethod("is3D", new Class[]{});
        } catch (NoSuchMethodException ex) {
            LOGGER.fatal("is3D() method not declared.", ex);
            throw new RuntimeException();
        } catch (SecurityException ex) {
            LOGGER.fatal(null, ex);
            throw new RuntimeException();
        }

        final String methodInvokationErrorMessage = "Check declaration of is3D() method.";
        if (is3DMethod != null) {
            try {
                is3D = ((Boolean) is3DMethod.invoke(null, (Object[]) null));
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOGGER.fatal(methodInvokationErrorMessage, ex);
                throw new RuntimeException();
            }
        }
        return is3D;
    }

    private static boolean isGeometryAffine(Class<? extends AbstractGeometry> geometryClass)
    {
        return !isGeometryExplicit(geometryClass);
    }

    private static boolean isGeometryExplicit(Class<? extends AbstractGeometry> geometryClass)
    {
        Method isExplicitMethod = null;
        boolean explicit = false;
        try {
            isExplicitMethod = geometryClass.getDeclaredMethod("isExplicit", new Class[]{});
        } catch (NoSuchMethodException ex) {
            LOGGER.fatal("isExplicit() method not declared.", ex);
            throw new RuntimeException();
        } catch (SecurityException ex) {
            LOGGER.fatal(null, ex);
            throw new RuntimeException();
        }

        final String methodInvokationErrorMessage = "Check declaration of isExplicit() method.";
        if (isExplicitMethod != null) {
            try {
                explicit = ((Boolean) isExplicitMethod.invoke(null, (Object[]) null));
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOGGER.fatal(methodInvokationErrorMessage, ex);
                throw new RuntimeException();
            }
        }
        return explicit;
    }

    /**
     * Static method creating the output field with certain geometry based on
     * given parameters.
     * Core method of this module.
     * 
     * @param params parameters.
     * @return Field constructed.
     * @throws java.lang.ClassNotFoundException
     */
    public static final Field create(Params params) throws ClassNotFoundException
    {
        final int[] dims = params.getDims();

        final String[] componentNames = params.getComponentsClassNames();
        final String geometryName = params.getGeometryClassName();

        Field returnField = new RegularField(dims);

        for (String componentName : componentNames) {
            LOGGER.info("Creating component " + componentName + "...");
            /* Only full class name (with package) is supported by the reflection mechanism. */
            Class<? extends AbstractComponent> componentClass = (Class<? extends AbstractComponent>) Class.forName(PACKAGE_DATA_COMPONENTS + '.' + componentName);

            String name = getComponentName(componentClass); // No need for an instance, to have human-readable name.
            AbstractComponent componentInstance = createComponent(componentClass);
            componentInstance.compute(dims); // Compute values also!

            Object data = componentInstance.getData();
            int veclen = componentInstance.getVeclen();

            returnField.addComponent(DataArray.create(data, veclen, name));
        }
        /* Only full class name (with package) is supported by the reflect mechanism. */
        Class<? extends AbstractGeometry> geometryClass = (Class<? extends AbstractGeometry>) Class.forName(PACKAGE_GEOMETRIES + '.' + geometryName);

        boolean explicit = isGeometryExplicit(geometryClass); // No need for an instance..
        AbstractGeometry geometryInstance = createGeometry(geometryClass);
        geometryInstance.compute(dims); // Compute values also!

        if (explicit) {
            float[] coords = geometryInstance.getCoords();
            returnField.setCurrentCoords(new FloatLargeArray(coords));
        } else {
            float[][] affine = geometryInstance.getAffine();
            ((RegularField) returnField).setAffine(affine);
        }

        if (params.getMaskSetting()) {
            returnField.setCurrentMask(getConstantMask(dims));
        }

        return returnField;
    }

    /**
     * Instantiates component class. Does not compute it's values.
     * 
     * @param componentClass A class to be instantiated. Must extend <tt>AbstractComponent</tt> abstract class.
     * @return Pointer to newly created instance of <tt>componentClass</tt>.
     */
    private static AbstractComponent createComponent(Class<? extends AbstractComponent> componentClass)
    {
        final String errorMessage = "Check declaration of no-argument constructor in Your \"AbstractComponent\" extent...";

        Constructor<?> constructor = null;
        try {
            constructor = componentClass.getConstructor();
        } catch (NoSuchMethodException ex) {
            LOGGER.fatal(errorMessage, ex);
        } catch (SecurityException ex) {
            LOGGER.fatal(null, ex);
        }

        Object componentInstance = null;
        try {
            if (constructor != null) {
                componentInstance = constructor.newInstance();
            }
        } catch (InstantiationException | IllegalArgumentException ex) {
            LOGGER.fatal(errorMessage, ex);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            LOGGER.fatal(null, ex);
        }
        return (AbstractComponent) componentInstance;
    }

    /**
     * Instantiates geometry class. Does not compute it's values.
     * 
     * @param geometryClass A class to be instantiated. Must extend <tt>AbstractGeometry</tt> abstract class.
     * @return Pointer to newly created instance of <tt>componentClass</tt>.
     */
    private static AbstractGeometry createGeometry(Class<? extends AbstractGeometry> geometryClass)
    {
        final String errorMessage = "Check declaration of no-argument constructor in Your \"AbstractGeometry\" extent...";

        Constructor<?> constructor = null;
        try {
            constructor = geometryClass.getConstructor();
        } catch (NoSuchMethodException ex) {
            LOGGER.fatal(errorMessage, ex);
        } catch (SecurityException ex) {
            LOGGER.fatal(null, ex);
        }

        Object geometryInstance = null;
        try {
            if (constructor != null) {
                geometryInstance = constructor.newInstance();
            }
        } catch (InstantiationException | IllegalArgumentException ex) {
            LOGGER.fatal(errorMessage, ex);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            LOGGER.fatal(null, ex);
        }
        return (AbstractGeometry) geometryInstance;
    }

    public static final LogicLargeArray getConstantMask(int[] dims)
    {
        long length = 1;
        for (int i = 0; i < dims.length; ++i) {
            length *= dims[i];
        }

        LogicLargeArray mask = new LogicLargeArray(length, true);

        switch (dims.length) {
            case 3:
                for (long i = 0; i < dims[0]; ++i) {
                    for (long j = 0; j < dims[1]; ++j) {
                        for (long k = 0; k < dims[2]; ++k) {
                            mask.setBoolean(i + dims[0] * (j + k * dims[1]), false);
                        }
                    }
                }
                break;
            case 2:
                for (long i = 0; i < dims[0] / 2; ++i) {
                    for (long j = 0; j < dims[1] / 2; ++j) {
                        mask.setBoolean(i + dims[0] * j, false);
                    }
                }
                break;
            case 1:
                for (long i = 0; i < dims[0] / 3; ++i) {
                    mask.setBoolean(i, false);
                }
                break;
            default:
                LOGGER.fatal("Unrecognized structure of \"dims\" array... dims.length = " + dims.length);
                throw new RuntimeException("Unrecognized structure of \"dims\" array... dims.length = " + dims.length);
        }
        return mask;
    }

    @Override
    public void onActive()
    {
        /* "setProgress" is not available from the inside of "create" method, as "create" is static. */
        setProgress(0.0f);
        
        LargeArray.setMaxSizeOf32bitArray(536870912);
        
        try {
                /* Basically onActive() method consist of one-to-one copy of */
            outField = create(this.params);
            
        } catch (ClassNotFoundException | IllegalArgumentException ex) {
            LOGGER.fatal(null, ex);
        }

        setProgress(1.0f);

        prepareOutputGeometry();
        show();

        setOutputValue("outField", new VNRegularField((RegularField) outField));
    }
}