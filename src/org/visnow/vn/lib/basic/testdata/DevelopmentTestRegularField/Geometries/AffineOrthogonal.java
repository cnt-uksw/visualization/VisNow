/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.testdata.DevelopmentTestRegularField.Geometries;

/**
 * Represents general affine geometry, with orthogonal (canonical) basis.
 * Class cannot be extended, as static methods (e.g. <tt>getName</tt>) are not inherited.
 *
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl), Warsaw University, ICM
 * @see AbstractGeometry
 */
public final class AffineOrthogonal extends AbstractGeometry
{
    static final boolean explicit = false;

    public AffineOrthogonal()
    {
        affine = new float[4][];
    }

    /**
     * Checks, whether one can put <tt>1</tt> as <tt>nSpace</tt> argument in <tt>compute</tt> method.
     *
     * All methods, that inherit from <tt>AbstractGeometry</tt> class should have
     * this method implemented; not doing so can result in runtime errors.
     *
     * @return The answer (boolean).
     * @see AbstractGeometry
     * @see compute
     */
    public static boolean is1D()
    {
        return false;
    }

    /**
     * Checks, whether one can put <tt>2</tt> as <tt>nSpace</tt> argument in <tt>compute</tt> method.
     *
     * All methods, that inherit from <tt>AbstractGeometry</tt> class should have
     * this method implemented; not doing so can result in runtime errors.
     *
     * @return The answer (boolean).
     * @see AbstractGeometry
     * @see compute
     */
    public static boolean is2D()
    {
        return false;
    }

    /**
     * Checks, whether one can put <tt>3</tt> as <tt>nSpace</tt> argument in <tt>compute</tt> method.
     *
     * All methods, that inherit from <tt>AbstractGeometry</tt> class should have
     * this method implemented; not doing so can result in runtime errors.
     *
     * @return The answer (boolean).
     * @see AbstractGeometry
     * @see compute
     */
    public static boolean is3D()
    {
        return true;
    }

    /**
     * Checks, whether geometry is affine.
     * Note for classes providing affine geometries: The <tt>compute</tt> method
     * should provide a valid <tt>affine</tt> array (member of <tt>AbstractGeometry</tt> class).
     *
     * All methods, that inherit from <tt>AbstractGeometry</tt> class should have
     * this method (i.e. <tt>public static boolean isAffine()</tt>) implemented;
     * not doing so can result in runtime errors.
     *
     * @return The answer (boolean).
     */
    public static boolean isAffine()
    {
        return !explicit;
    }

    /**
     * Checks, whether geometry is explicit.
     * Note for classes providing explicit geometries: The <tt>compute</tt> method
     * should provide a valid <tt>coords</tt> array (member of <tt>AbstractGeometry</tt> class).
     *
     * All methods, that inherit from <tt>AbstractGeometry</tt> class should have
     * this method (i.e. <tt>public static boolean isExplicit()</tt>) implemented;
     * not doing so can result in runtime errors.
     *
     * @return The answer (boolean).
     * @see AbstractGeometry
     */
    public static boolean isExplicit()
    {
        return explicit;
    }

    /**
     * Method providing a human-readable name of geometry presented in this class.
     *
     * All methods, that inherit from <tt>AbstractGeometry</tt> class should have
     * this method implemented. Lack of this method will result in appearance
     * of (not always informative) bare class names in user interfaces.
     *
     * @return The human-readable name of the geometry.
     * @see AbstractGeometry
     */
    public static String getName()
    {
        return "Affine, orthogonal (canonical)";
    }

    /**
     * Method providing an information, whether geometry produced by the class
     * can coexist with <tt>JLargeArray</tt> (for example <tt>FloatLargeArray</tt>)
     * data.
     * All methods, that inherit from <tt>AbstractGeometry</tt> class should have
     * this method implemented. Lack of this method will most likely result in
     * runtime errors (when user will provide parameters resulting in
     * an array of length beyond maximum value of <tt>int</tt>, or
     * <tt>JLargeArrays</tt> threshold; see <tt>setMaxSizeOf32bitArray</tt> method).
     *
     * @return <tt>true</tt>, if geometry can be attached to a field with
     *         <tt>*LargeArray</tt> components.
     *         <tt>false</tt> otherwise (i.e. field can consist of components in
     *          a regular Java arrays, like <tt>float[]</tt>).
     * @see AbstractGeometry
     */
    public static final boolean hasJLargeArraysSupport()
    {
        return true;
    }
    
    /**
     * Method, that computes actual coordinates of every point in the grid.
     *
     * @param dims   An array containing the dimensions of the grid. Can be ignored
     *               (i.e. set <tt>null</tt> for affine geometries.
     *
     * @see org.visnow.jscic.RegularField
     */
    @Override
    public void compute(int[] dims)
    {
        affine[3] = new float[]{1.4f, 11.2f, 2.97f};

       
        affine[0] = new float[]{6.12f, 0.0f, 0.0f};
        affine[1] = new float[]{0.0f, 4.89f, 0.0f};
        affine[2] = new float[]{0.0f, 0.0f, 5.43f};
    }
}