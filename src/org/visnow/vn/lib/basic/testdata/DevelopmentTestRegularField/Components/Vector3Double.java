/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.testdata.DevelopmentTestRegularField.Components;

import org.visnow.jlargearrays.DoubleLargeArray;

import org.apache.log4j.Logger;

import static org.apache.commons.math3.util.FastMath.*;

/**
 * Class creating data array consisting of 3-dimensional <tt>double</tt> vectors.
 * Class cannot be extended, as static methods (e.g. <tt>name</tt>) are not inherited.
 *
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl), Warsaw University, ICM
 * @see AbstractComponent
 */
public final class Vector3Double extends AbstractComponent
{
    private static final Logger LOGGER = Logger.getLogger(Vector3Double.class);

    private final int nThreads = Runtime.getRuntime().availableProcessors();

    public Vector3Double()
    {
        veclen = 3;
        data = null;
    }

    /**
     * Method providing a human-readable name of component presented in
     * this class.
     * All methods, that inherit from <tt>AbstractComponent</tt> class should have
     * this method implemented. Lack of this method will result in appearance
     * of (not always informative) bare class names in user interfaces.
     *
     * @return The human-readable name of the data component.
     * @see AbstractComponent
     */
    public static final String getName()
    {
        return "Three-dimensional vector (double)";
    }

    /**
     * Method, that computes actual values of component on every point
     * on the grid.
     * Core calculations should be carried out in this method. A constructor
     * should be kept as light as possible.
     *
     * @param dims An array containing the dimensions of the grid. It must have
     *             one to three elements - by design all of these values (i.e. 1, 2 and 3)
     *             are supported by every data component class.
     * @see AbstractComponent
     * @see org.visnow.jscic.RegularField
     */
    @Override
    public void compute(int[] dims)
    {
        long length = veclen;

        for (int i = 0; i < dims.length; i++) {
            length *= dims[i];
        }

        data = new DoubleLargeArray(length, false);

        Thread[] workThreads = new Thread[nThreads];
        for (int i = 0; i < workThreads.length; i++) {
            workThreads[i] = new Thread(new ComputeThreaded(nThreads, i, dims));
            workThreads[i].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
    }

    class ComputeThreaded implements Runnable
    {
        int nThreads;
        int iThread;

        int[] dims;

        public ComputeThreaded(int nThreads, int iThread, int[] dims)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;

            this.dims = dims;
        }

        @Override
        public void run()
        {
            double x, y, z;

            final double x0 = -1.0, y0 = -1.0, z0 = -1.0;
            final double x1 = 6.0, y1 = 6.0, z1 = 6.0;
            double dist0, dist1;

            switch (dims.length) {
                case 3:
                    for (long k = iThread; k < dims[2]; k += nThreads) {
                        for (long j = 0; j < dims[1]; ++j) {
                            for (long i = 0; i < dims[0]; ++i) {
                                x = 5.0 / ((double) dims[0] - 1.0) * (double) i;
                                y = 5.0 / ((double) dims[1] - 1.0) * (double) j;
                                z = 5.0 / ((double) dims[2] - 1.0) * (double) k;
                                dist0 = sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0) + (z - z0) * (z - z0));
                                dist1 = sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1) + (z - z1) * (z - z1));

                                ((DoubleLargeArray) data).setDouble(veclen * (i + dims[0] * (j + k * dims[1])) + 0, -75.0 * (x - x0) / (dist0 * dist0 * dist0) - 150.0 * (x - x1) / (dist1 * dist1 * dist1));
                                ((DoubleLargeArray) data).setDouble(veclen * (i + dims[0] * (j + k * dims[1])) + 1, -75.0 * (y - y0) / (dist0 * dist0 * dist0) - 150.0 * (y - y1) / (dist1 * dist1 * dist1));
                                ((DoubleLargeArray) data).setDouble(veclen * (i + dims[0] * (j + k * dims[1])) + 2, -75.0 * (z - z0) / (dist0 * dist0 * dist0) - 150.0 * (z - z1) / (dist1 * dist1 * dist1));
                            }
                        }
                    }
                    break;
                case 2:
                    for (long j = iThread; j < dims[1]; j += nThreads) {
                        for (long i = 0; i < dims[0]; ++i) {
                            x = 5.0 / ((double) dims[0] - 1.0) * (double) i;
                            y = 5.0 / ((double) dims[1] - 1.0) * (double) j;
                            z = 2.5;
                            dist0 = sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0) + (z - z0) * (z - z0));
                            dist1 = sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1) + (z - z1) * (z - z1));

                            ((DoubleLargeArray) data).setDouble(veclen * (i + dims[0] * j) + 0, -75.0 * (x - x0) / (dist0 * dist0 * dist0) - 150.0 * (x - x1) / (dist1 * dist1 * dist1));
                            ((DoubleLargeArray) data).setDouble(veclen * (i + dims[0] * j) + 1, -75.0 * (y - y0) / (dist0 * dist0 * dist0) - 150.0 * (y - y1) / (dist1 * dist1 * dist1));
                            ((DoubleLargeArray) data).setDouble(veclen * (i + dims[0] * j) + 2, -75.0 * (z - z0) / (dist0 * dist0 * dist0) - 150.0 * (z - z1) / (dist1 * dist1 * dist1));
                        }
                    }
                    break;
                case 1:
                    for (long i = iThread; i < dims[0]; i += nThreads) {
                        x = 5.0 / ((double) dims[0] - 1.0) * (double) i;
                        y = 1.5;
                        z = 2.5;
                        dist0 = sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0) + (z - z0) * (z - z0));
                        dist1 = sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1) + (z - z1) * (z - z1));

                        ((DoubleLargeArray) data).setDouble(veclen * i + 0, -75.0 * (x - x0) / (dist0 * dist0 * dist0) - 150.0 * (x - x1) / (dist1 * dist1 * dist1));
                        ((DoubleLargeArray) data).setDouble(veclen * i + 1, -75.0 * (y - y0) / (dist0 * dist0 * dist0) - 150.0 * (y - y1) / (dist1 * dist1 * dist1));
                        ((DoubleLargeArray) data).setDouble(veclen * i + 2, -75.0 * (z - z0) / (dist0 * dist0 * dist0) - 150.0 * (z - z1) / (dist1 * dist1 * dist1));
                    }
                    break;
                default:
                    LOGGER.fatal("Unrecognized structure of \"dims\" array... dims.length = " + dims.length);
                    throw new RuntimeException("Unrecognized structure of \"dims\" array... dims.length = " + dims.length);
            }
        }
    }
}