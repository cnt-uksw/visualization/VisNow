/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.mask;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.types.VNRegularField;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Mask extends ModuleCore
{

    private GUI ui = new GUI();
    protected Params params;
    RegularField inField = null;

    public Mask()
    {
        parameters = params = new Params();
        ui.setParams(params);
        params.addChangeListener(new ChangeListener()
        {

            public void stateChanged(ChangeEvent evt)
            {
                startAction();
            }
        });
        setPanel(ui);
        //WTF-MUI:addModuleUI(ui);
    }

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    protected RegularField calculateMask(RegularField inFld)
    {
        RegularField outFld = new RegularField(inFld.getDims());
        for (int i = 0; i < inField.getNComponents(); i++) {
            float[] data = inFld.getComponent(i).getRawFloatArray().getData();
            float[] data2 = new float[data.length];
            System.arraycopy(data, 0, data2, 0, data.length);
            int[] dims = inFld.getDims();
            for (int i1 = 0; i1 < dims[0]; i1++) {
                for (int i2 = 0; i2 < dims[1]; i2++) {
                    float min = data[i1 + i2 * dims[0]];
                    for (int i3 = 0; i3 < dims[2]; i3++) {
                        data2[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] = 0;
                        if (data[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] > min) {
                            break;
                        }
                    }
                }
            }
            //
            for (int i3 = 0; i3 < dims[2]; i3++) {
                for (int i2 = 0; i2 < dims[1]; i2++) {
                    float min = data[i2 * dims[0] + i3 * dims[0] * dims[1]];
                    for (int i1 = 0; i1 < dims[0]; i1++) {
                        data2[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] = 0;
                        if (data[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] > min) {
                            break;
                        }
                    }
                }
            }

            for (int i1 = 0; i1 < dims[0]; i1++) {
                for (int i3 = 0; i3 < dims[2]; i3++) {
                    float min = data[i1 + i3 * dims[0] * dims[1]];
                    for (int i2 = 0; i2 < dims[1]; i2++) {
                        data2[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] = 0;
                        if (data[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] > min) {
                            break;
                        }
                    }
                }
            }

            // ----------
            for (int i1 = 0; i1 < dims[0]; i1++) {
                for (int i2 = 0; i2 < dims[1]; i2++) {
                    float min = data[i1 + i2 * dims[0] + (dims[2] - 1) * dims[0] * dims[1]];
                    for (int i3 = dims[2] - 1; i3 > 0; i3--) {
                        data2[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] = 0;
                        if (data[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] > min) {
                            break;
                        }
                    }
                }
            }
            //
            for (int i3 = 0; i3 < dims[2]; i3++) {
                for (int i2 = 0; i2 < dims[1]; i2++) {
                    float min = data[dims[0] - 1 + i2 * dims[0] + i3 * dims[0] * dims[1]];
                    for (int i1 = dims[0] - 1; i1 > 0; i1--) {
                        data2[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] = 0;
                        if (data[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] > min) {
                            break;
                        }
                    }
                }
            }

            for (int i1 = 0; i1 < dims[0]; i1++) {
                for (int i3 = 0; i3 < dims[2]; i3++) {
                    float min = data[i1 + (dims[1] - 1) * dims[0] + i3 * dims[0] * dims[1]];
                    for (int i2 = dims[1] - 1; i2 > 0; i2--) {
                        data2[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] = 0;
                        if (data[i1 + i2 * dims[0] + i3 * dims[0] * dims[1]] > min) {
                            break;
                        }
                    }
                }
            }

            outFld.addComponent(DataArray.create(data2, inField.getComponent(i).getVectorLength(), "mask_" + inField.getComponent(i).getName()));
        }
        if (inField.getCurrentCoords() != null) {
            outFld.setCurrentCoords(inField.getCurrentCoords());
        } else {
            outFld.setAffine(inField.getAffine());
        }
        return outFld;
    }

    @Override
    public void onActive()
    {

        if (getInputFirstValue("inField") == null) {
            return;
        }
        RegularField inFld = ((VNRegularField) getInputFirstValue("inField")).getField();
        if (inFld == null) {
            return;
        }
        if (inFld != inField) {
            inField = inFld;
            ui.setInField(inField);
        }
        ui.update();
        RegularField outField = calculateMask(inFld);

        setOutputValue("outField", new VNRegularField(outField));
    }
}
