/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.LocalOperations;

import java.util.Arrays;
import org.apache.log4j.Logger;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.filters.LocalOperations.LocalOperationsShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class LocalOperations extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(LocalOperations.class);

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected GUI computeUI = null;
    protected RegularField inField = null;
    protected LocalOps ops = null;
    private int runQueue = 0;

    public LocalOperations()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startAction();

            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(RADIUS, 1),
            new Parameter<>(SEQUENCE, "de"),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN)
        };
    }

    public void update(Parameters p)
    {
        ProgressAgent progressAgent;
        if (inField.getDims().length == 3) {
            ops = new LocalOps3D();
            progressAgent = getProgressAgent(p.get(SEQUENCE).length() * (int) Math.round(inField.getDims()[2] * 1.1)); //10% for prepare geometry
        } else {
            ops = new LocalOps2D();
            progressAgent = getProgressAgent(p.get(SEQUENCE).length() * (int) Math.round(inField.getDims()[1] * 1.1)); //10% for prepare geometry
        }

        outRegularField = ops.compute(inField, p.get(RADIUS), p.get(SEQUENCE), progressAgent);
        setOutputValue("outField", new VNRegularField(outRegularField));
        outField = outRegularField;
        prepareOutputGeometry();
        show();
        progressAgent.setProgress(1);

    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive()
    {
        LOGGER.debug(getInputFirstValue("inField") != null ? Arrays.toString(((VNRegularField) getInputFirstValue("inField")).getField().getDims()) : null);
        
        if (getInputFirstValue("inField") != null) {
            RegularField newField = ((VNRegularField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA() && (inField == null || inField.getTimestamp() != newField.getTimestamp());
            boolean isNewField = !isFromVNA() && newField != inField;
            inField = newField;

            Parameters p = parameters.getReadOnlyClone();

            notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data
                update(p);
            }
        }
    }
}
