/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.IntensityEqualizer;

import java.util.Arrays;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.filters.IntensityEqualizer.IntensityEqualizerShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.jscic.utils.ScalarMath;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class IntensityEqualizer extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private int runQueue = 0;
    private GUI computeUI = null;
    private Core core = new Core();
    int[] dims;
    RegularField inField = null;

    public IntensityEqualizer()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name != null && name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startIfNotInQueue();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(GAIN, 0.3f),
            new Parameter<>(WEIGHTS, new float[]{1.f}),
            new Parameter<>(FULL_PADDING, false),
            new Parameter<>(RUNNING_MESSAGE, RUN_DYNAMICALLY)
        };
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(parameters, resetFully, setRunButtonPending);
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        if (resetParameters) {
            float[] weights;
            if (inField == null || inField.getDims() == null || inField.getDims().length < 2 || inField.getDims().length > 3) {
                weights = new float[]{1.f};
            } else {
                dims = inField.getDims();
                int[] outDimsL = new int[dims.length];
                for (int i = 0; i < outDimsL.length; i++) {
                    outDimsL[i] = ScalarMath.nextPower2(dims[i]);
                }

                int min = min(outDimsL[0], outDimsL[1]);
                if (outDimsL.length == 3)
                    min = min(min, outDimsL[2]);
                int n = (31 - Integer.numberOfLeadingZeros(min));//ScalarMath.nextPower2(min);

                if (n < 1)
                    n = 1;
                weights = new float[n];
                for (int i = 0; i < weights.length; i++) {
                    weights[i] = 1.0f;
                }
            }

            parameters.set(WEIGHTS, weights);
        }
        parameters.setParameterActive(true);
    }

    @Override
    public void onActive()
    {

        if (getInputFirstValue("inField") != null) {
            RegularField newField = ((VNRegularField) getInputFirstValue("inField")).getField();
            boolean isDifferentDims = !isFromVNA() && (inField == null || inField.getDimNum() != newField.getDimNum() || !Arrays.equals(inField.getDims(), newField.getDims()));

            boolean isNewField = !isFromVNA() && newField != inField;

            inField = newField;

            Parameters p;

            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentDims);
                p = parameters.getReadOnlyClone();
            }

            notifyGUIs(p, isFromVNA() || isDifferentDims, isFromVNA() || isNewField);

            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                int totalSteps = (int) inField.getNNodes();
                ProgressAgent progressAgent = getProgressAgent(totalSteps);
                runQueue = Math.max(runQueue - 1, 0);
                core.setInField(inField, p.get(FULL_PADDING), progressAgent);
                core.getPreprocessedInField();
                core.updatePyramid(p.get(WEIGHTS));
            }

            core.updateOutput(p.get(WEIGHTS), p.get(GAIN));
            outRegularField = core.getOutField();
            setOutputValue("outField", new VNRegularField(outRegularField));
            outField = outRegularField;
            prepareOutputGeometry();
            show();

        }
    }
}
