/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.hough;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.RegularField;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Core
{

    private float progress;
    private Params params = new Params();
    private RegularField inField = null;
    private RegularField outField = null;

    public Core()
    {
    }

    public float getProgress()
    {
        return progress;
    }

    public void setParams(Params params)
    {
        this.params = params;
    }

    public void update()
    {
        if (inField == null) {
            return;
        }

        if (inField.getDims().length != 2) {
            return;
        }

        int[] dims = {255, 255, 255};
        outField = new RegularField(dims);

        for (int i = 0; i < inField.getNComponents(); i++) {
            if (inField.getComponent(i).getVectorLength() != 1) {
                continue;
            }

            outField.addComponent(DataArray.create(calculateHough(i), 1, inField.getComponent(i).getName()));
        }
    }

    public void setInField(RegularField inField)
    {
        this.inField = inField;
    }

    public RegularField getOutField()
    {
        return outField;
    }

    private void bresenhamEllipse(int inputOffset, int zOffset, int xP, int yP, int xQ, int yQ, byte[] input, float[] tab)
    {

        int v = 0xFF & input[inputOffset];
        int x = xP, y = yP, y2 = 255 * y, D = 0, HX = xQ - xP, HY = yQ - yP, c, M, xInc = 1, yInc = 1, yInc2 = 255;
        if (HX < 0) {
            xInc = -1;
            HX = -HX;
        }
        if (HY < 0) {
            yInc = -1;
            yInc2 = -255;
            HY = -HY;
        }
        if (HY <= HX) {
            c = 2 * HX;
            M = 2 * HY;
            for (;;) {
                if (x >= 0 && x < 255 && y >= 0 && y < 255) {
                    tab[zOffset + y2 + x] += v;
                }
                if (x == xQ) {
                    break;
                }
                x += xInc;
                D += M;
                if (D > HX) {
                    y += yInc;
                    y2 += yInc2;
                    D -= c;
                }
            }
        } else {
            c = 2 * HY;
            M = 2 * HX;
            for (;;) {
                if (x >= 0 && x < 255 && y >= 0 && y < 255) {
                    tab[zOffset + y2 + x] += v;
                }
                if (y == yQ) {
                    break;
                }
                y += yInc;
                y2 += yInc2;
                D += M;
                if (D > HY) {
                    x += xInc;
                    D -= c;
                }
            }
        }
    }

    private float[] calculateHough(int i)
    {
        byte[] in = inField.getComponent(i).getRawByteArray().getData();            
        float[] out = new float[255 * 255 * 255];

        progress = 0;

        for (int zz = 0; zz < 255; zz++) {
            int zSqr = zz * zz;
            int zOffset = 255 * 255 * zz;
            int inputOffset = 0;
            for (int yy = 0; yy < 255; yy++) {
                inputOffset = 256 * yy;
                double cc = ((double) (zSqr - yy * yy) / (4 * zSqr));
                if (cc > 0) {
                    double sqrt_c = 255 * sqrt(cc);
                    for (int xx = 0; xx < 255; xx++) {
                        bresenhamEllipse(inputOffset, zOffset, xx, 0, (int) (xx - sqrt_c), 255, in, out);
                        bresenhamEllipse(inputOffset, zOffset, xx, 0, (int) (xx + sqrt_c), 255, in, out);
                        inputOffset++;
                    }
                }
            }

            progress = 0.8f * zz / 255;
            fireStateChanged();
        }

        int j = 0;
        for (int zz = 0; zz < 255; zz++) {
            for (int yy = 0; yy < 255; yy++) {
                double v = PI * (3 / 2 * (zz + (254 - yy) / 2) - sqrt(zz * (254 - yy) / 2));
                if (v > 1) {
                    for (int xx = 0; xx < 255; xx++) {

                        out[j] /= v;
                    }
                }
            }
            progress = 0.8f + 0.2f * zz / 255;
            fireStateChanged();
        }
        return out;
    }

    /**
     * Utility field holding list of ChangeListeners.
     */
    private transient ArrayList<ChangeListener> changeListenerList
        = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     * <p>
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param object Parameter #1 of the <CODE>ChangeEvent<CODE> constructor.
     */
    private void fireStateChanged()
    {
        ChangeEvent e = new ChangeEvent(this);
        for (ChangeListener listener : changeListenerList) {
            listener.stateChanged(e);
        }
    }
}
