/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.Convolution;

import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.dataarrays.FloatDataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jvia.spatialops.Padding.PaddingType;
import org.visnow.vn.lib.utils.convolution.ConvolutionCore;

/**
 *
 * @author Piotr Wendykier(piotrw@icm.edu.pl)
 *
 */
public class Core
{
    
    private RegularField inFieldData = null;
    private RegularField inFieldKernel = null;
    private RegularField outField = null;
    private ConvolutionCore conv;


    public void update(int[] components, PaddingType padding, boolean normalizeKernel)
    {

        if (inFieldData == null || inFieldKernel == null) {
            outField = null;
            return;
        }

        if (components == null) {
            return;
        }

        outField = new RegularField(inFieldData.getDims());
        if (inFieldData.getCurrentCoords() == null) {
            outField.setAffine(inFieldData.getAffine());
        } else {
            outField.setCurrentCoords(inFieldData.getCurrentCoords());
        }
        
        conv = ConvolutionCore.loadConvolutionLibrary();
        
        DataArray kernel = inFieldKernel.getComponent(0);

        if (normalizeKernel) {
            FloatLargeArray kdata = kernel.getRawFloatArray();
            double sum = 0;
            for (long i = 0; i < kdata.length(); i++) {
                sum += kdata.getFloat(i);
            }
            for (long i = 0; i < kdata.length(); i++) {
                kdata.setFloat(i, (float)(kdata.getFloat(i) / sum));
            }
            kernel = new FloatDataArray(kdata, new DataArraySchema(kernel.getName(), DataArrayType.FIELD_DATA_FLOAT, kdata.length(), 1, false));
        }

        for (int n = 0; n < components.length; n++) {
            DataArray data = inFieldData.getComponent(components[n]);
            conv.setInput(data, inFieldData.getLDims(), kernel, inFieldKernel.getLDims(), padding);
            conv.calculateConvolution();
            outField.addComponent(conv.getOutput());
        }

    }

    public void setInFieldData(RegularField field)
    {
        this.inFieldData = field;
    }

    public void setInFieldKernel(RegularField field)
    {
        this.inFieldKernel = field;
    }

    /**
     * @return the outField
     */
    public RegularField getOutField()
    {
        return outField;
    }
}
