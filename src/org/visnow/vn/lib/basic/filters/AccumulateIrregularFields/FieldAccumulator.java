/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.AccumulateIrregularFields;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.PointField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNPointField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.field.MergeIrregularField;
import org.visnow.vn.lib.utils.field.PointFldToIrregularFld;
import org.visnow.vn.lib.utils.field.RegularFldToIrregularFld;

/**
 *
 * @author Krzysztof S. Nowinski
 * Warsaw University, ICM
 */
public class FieldAccumulator extends OutFieldVisualizationModule

{

    private GUI computeUI = null;
    protected IrregularField inIrregularField = null;
    protected Params params;
    protected boolean fromGUI = false;
    protected int current = 0;
    protected boolean separate = false;
    protected int[][] outSNDS;
    protected VNField input = null;

    public FieldAccumulator()
    {
        parameters = params = new Params();
        params.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                fromGUI = true;
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                computeUI = new GUI();
            }
        });
        computeUI.setParams(params);
        ui.addComputeGUI(computeUI);
        setPanel(ui);
        outObj.setName("accumulated");
    }

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    
    private static int[][] getNumericDataSchema(Field field)
    {
        if(field == null || field.getNComponents() == 0) return null;
        int size = field.getNComponents();
        int nSNData = 0;
        for (int i = 0; i < size; i++)
            if (field.getComponent(i).isNumeric())
                nSNData += 1;
        int[][] sNDS = new int[nSNData][2];
        for (int i = 0, j = 0; i < size; i++)
            if (field.getComponent(i).isNumeric()) {
                sNDS[j][0] = field.getComponent(i).getType().getValue();
                sNDS[j][1] = field.getComponent(i).getVectorLength();
            }
        return sNDS;
    }
    
    private IrregularField convertToIrregular(VNField input)
    {
        if (input instanceof VNIrregularField)
            return(IrregularField) input.getField();
        else if (input instanceof VNPointField)
            return PointFldToIrregularFld.convert((PointField)input.getField());
        else if (input instanceof VNRegularField)
            return RegularFldToIrregularFld.convert((RegularField)input.getField());
        else
            return null;
    }
   
    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null || !params.accumulating())
            return;
        inIrregularField = convertToIrregular((VNField) getInputFirstValue("inField"));
        if (outIrregularField == null || params.resetting()) {
            params.setReset(false);
            if (inIrregularField != null) {
                outIrregularField = inIrregularField.cloneDeep();
                outIrregularField.removeComponents();
                for (int i = 0; i < inIrregularField.getNComponents(); i++)
                    if (inIrregularField.getComponent(i).isNumeric())
                        outIrregularField.addComponent(inIrregularField.getComponent(i).cloneShallow());
                outSNDS = getNumericDataSchema(outIrregularField);
            }
            current = 0;
        } else if (input != ((VNField) getInputFirstValue("inField"))) {
            inIrregularField = convertToIrregular(input);
            if (inIrregularField == null || inIrregularField.getNNodes() < 1)
                return;
            int[][] inSNDS = getNumericDataSchema(inIrregularField);
            if (outSNDS != null) {
                if (outSNDS.length != inSNDS.length)
                    return;
                for (int i = 0; i < inSNDS.length; i++)
                    if (inSNDS[i][0] != outSNDS[i][0] || inSNDS[i][1] != outSNDS[i][1])
                        return;
            }
            current += 1;
            outIrregularField = MergeIrregularField.merge(outIrregularField, inIrregularField, current, params.separateCellSets());
        }
        else
            return;
        input = ((VNField) getInputFirstValue("inField"));
        outField = outIrregularField;
        outPointField = null;
        setOutputValue("outIrregularField", new VNIrregularField(outIrregularField));
        prepareOutputGeometry();
        try {
            Thread.sleep(500);
        } catch (Exception e) {
        }
        show();
        fromGUI = false;
    }
}
