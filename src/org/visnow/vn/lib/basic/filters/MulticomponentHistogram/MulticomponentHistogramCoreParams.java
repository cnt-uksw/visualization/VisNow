/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.MulticomponentHistogram;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class MulticomponentHistogramCoreParams
{
    static final int BINNING_BY_COMPONENTS = 0;
    static final int BINNING_BY_COORDINATES = 1;

    private final int nDims;
    private final int binning;
    private final int[] dims;
    private final int[] selectedComponents;
    private final int[] selectedCoords;
    private final boolean countLogScale;
    private final boolean countDropBackground;
    private final float logConstant;
    private final boolean outGeometryToData;
    private final boolean roundByteDimsTo32;
    private final HistogramOperation[] histogramOperations;

    public MulticomponentHistogramCoreParams(int nDims, int binning, int[] dims, int[] selectedComponents, int[] selectedCoords, boolean countLogScale, boolean countDropBackground, float logConstant, boolean outGeometryToData, boolean roundByteDimsTo32, HistogramOperation[] histogramOperations)
    {
        this.nDims = nDims;
        this.binning = binning;
        this.dims = dims;
        this.selectedComponents = selectedComponents;
        this.selectedCoords = selectedCoords;
        this.countLogScale = countLogScale;
        this.countDropBackground = countDropBackground;
        this.logConstant = logConstant;
        this.outGeometryToData = outGeometryToData;
        this.roundByteDimsTo32 = roundByteDimsTo32;
        this.histogramOperations = histogramOperations;
    }

    public int getNDims()
    {
        return nDims;
    }

    public int getBinning()
    {
        return binning;
    }

    public int[] getDims()
    {
        return dims;
    }

    public int[] getSelectedComponents()
    {
        return selectedComponents;
    }

    public int[] getSelectedCoords()
    {
        return selectedCoords;
    }

    public boolean isCountLogScale()
    {
        return countLogScale;
    }

    public boolean isCountDropBackground()
    {
        return countDropBackground;
    }

    public float getLogConstant()
    {
        return logConstant;
    }

    public boolean isOutGeometryToData()
    {
        return outGeometryToData;
    }

    public boolean isRoundByteDimsTo32()
    {
        return roundByteDimsTo32;
    }
    
    public Condition[] getFilterConditions()
    {
        return null;
    }

    public Condition.Logic[] getFilterConditionsLogic()
    {
        return null;
    }

    public HistogramOperation[] getHistogramOperations()
    {
        return histogramOperations;
    }
}
