/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.MulticomponentHistogram;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.filters.MulticomponentHistogram.MulticomponentHistogramShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 * @author szpak
 */
public class MulticomponentHistogram extends OutFieldVisualizationModule
{
    private MulticomponentHistogramGUI computeUI;
    private MulticomponentHistogramCore core;
    private Field inField = null;

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private int runQueue = 0;

    public MulticomponentHistogram()
    {
        core = new MulticomponentHistogramCore();
        core.addFloatValueModificationListener(new FloatValueModificationListener()
        {
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                setProgress(e.getVal());
            }
        });

        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name != null && name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startAction();
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                computeUI = new MulticomponentHistogramGUI();
                computeUI.setParameterProxy(parameters);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        BinOperationState[] emptyRow = new BinOperationState[BinOperationType.values().length];
        Arrays.fill(emptyRow, BinOperationState.OFF);

        return new Parameter[]{
            new Parameter<>(DIMENSION_LENGTHS, new int[]{64, 64}),
            new Parameter<>(BINNING_SOURCES, new int[]{0, 0}),
            new Parameter<>(ROUND_BYTE_DIMS_TO_32, false),
            new Parameter<>(LOG_SCALE, true),
            new Parameter<>(LOG_SCALE_BASE, 1.0f),
            new Parameter<>(INHERIT_EXTENTS, false),
            new Parameter<>(RESET_FIRST_BIN, false),
            new Parameter<>(MASK_OUT_EMPTY_BINS, false),
            new Parameter<>(BIN_OPERATIONS, new BinOperationState[][]{emptyRow}),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN),
            new Parameter<>(META_SCALAR_COMPONENT_NAMES, new String[]{"------"}),
            new Parameter<>(META_ALL_COMPONENT_NAMES, new String[]{"------"})
        };
    }

    private void validateParamsAndSetSmart(boolean resetFully)//, boolean differentRegularDims)
    {
        parameters.setParameterActive(false);
        if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE) parameters.set(RUNNING_MESSAGE, NO_RUN);

        String[] previousScalarComponentNames = parameters.get(META_SCALAR_COMPONENT_NAMES);
        String[] previousAllComponentNames = parameters.get(META_ALL_COMPONENT_NAMES);
        List<String> currentScalarComponentNames = Arrays.asList(inField.getScalarComponentNames());
        List<String> currentAllComponentNames = Arrays.asList(inField.getComponentNames());
        parameters.set(META_SCALAR_COMPONENT_NAMES, inField.getScalarComponentNames());
        parameters.set(META_ALL_COMPONENT_NAMES, inField.getComponentNames());

        //validate binning sources
        int[] sources = parameters.get(BINNING_SOURCES);
        boolean namesCorrect = true; //flag for binning
        //validate spacial binning
        if (sources[0] < 0) //clip to [-3 ... -1]
            for (int i = 0; i < sources.length; i++)
                sources[i] = Math.max(-3, sources[i]);
        else { //validate (+smart) component data based binning
            List<String> currentComponentNames = Arrays.asList(parameters.get(META_SCALAR_COMPONENT_NAMES));
            for (int j = 0; j < sources.length; j++) {
                int currentIndex = currentComponentNames.indexOf(previousScalarComponentNames[sources[j]]);
                if (currentIndex == -1) namesCorrect = false;
                else sources[j] = currentIndex;
            }
        }

        //smart binning sources (/validation)
        if (resetFully || !namesCorrect) {
            if (inField instanceof RegularField) //for regular fields take components as default interval source
                for (int i = 0; i < sources.length; i++)
                    sources[i] = Math.min(i, parameters.get(META_SCALAR_COMPONENT_NAMES).length - 1);
            else //for irregular fields take spacial data
                for (int i = 0; i < sources.length; i++)
                    sources[i] = -1 - i % 3;
        }

        parameters.set(BINNING_SOURCES, sources);

        //validate (+smart) binning operations
        boolean binningOperationNamesCorrect = true;
        BinOperationState[][] binOperationStateses = parameters.get(BIN_OPERATIONS);
        BinOperationState[][] validatedBinOperationStateses = new BinOperationState[currentAllComponentNames.size()][];
        //reset bin operations
        for (int i = 0; i < validatedBinOperationStateses.length; i++) {
            validatedBinOperationStateses[i] = new BinOperationState[BinOperationType.values().length];
            Arrays.fill(validatedBinOperationStateses[i], BinOperationState.OFF);
            //clear VSTD operation for scalar components
            if (currentScalarComponentNames.contains(currentAllComponentNames.get(i)))
                validatedBinOperationStateses[i][BinOperationType.VSTD.ordinal()] = null;
        }
        //set empty bin operation stateses
        parameters.set(BIN_OPERATIONS, validatedBinOperationStateses);

        //test if they can be copied from previous ones
        for (int i = 0; i < binOperationStateses.length; i++)
            for (int j = 0; j < binOperationStateses[i].length; j++)
                if (binOperationStateses[i][j] != BinOperationState.OFF && binOperationStateses[i][j] != null) {
                    int componentIndex = currentAllComponentNames.indexOf(previousAllComponentNames[i]);
                    if (componentIndex == -1) binningOperationNamesCorrect = false;
                    else
                        validatedBinOperationStateses[componentIndex] = binOperationStateses[i];
                }

        //clear VSTD operation for scalar components
        for (int i = 0; i < validatedBinOperationStateses.length; i++)
            if (currentScalarComponentNames.contains(currentAllComponentNames.get(i)))
                validatedBinOperationStateses[i][BinOperationType.VSTD.ordinal()] = null;

        //set binning operation to smart one (or keep reset otherwise)
        if (!resetFully && binningOperationNamesCorrect)
            parameters.set(BIN_OPERATIONS, validatedBinOperationStateses);

        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive()
    {

        if (getInputFirstValue("inField") != null) {
            //1. get new field
            Field newInField = ((VNField) getInputFirstValue("inField")).getField();
            //1a. set distinction flags
            boolean isNewField = !isFromVNA() && newInField != inField;
            boolean isNonCompatibleField = !isFromVNA() &&
                    (inField == null ||
                    inField instanceof IrregularField && newInField instanceof RegularField ||
                    inField instanceof RegularField && newInField instanceof IrregularField);
//            boolean isNewIrregularField = !isFromVNA() &&
//                    inField instanceof IrregularField && newInField instanceof IrregularField && newInField != inField;
//            boolean isDifferentRegularDims = !isFromVNA() &&
//                    inField != null && inField instanceof RegularField && newInField instanceof RegularField &&
//                    !Arrays.toString(((RegularField) inField).getDims()).equals(Arrays.toString(((RegularField) newInField).getDims()));
            inField = newInField;

            //2. validate params
            Parameters p;
            synchronized (parameters) {
                validateParamsAndSetSmart(isNonCompatibleField);// || isNewField, isDifferentRegularDims);
                //2b. clone param (local read-only copy)
                p = parameters.getReadOnlyClone();
            }
            //3. update gui
            //TODO: switch to ParameterProxy
            notifyGUIs(p, isFromVNA() || isNewField, isFromVNA() || isNewField);

            //4. run computation and propagate
            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data
                core.setInField(MulticomponentHistogramCore.IN_FIELD_MAIN, inField);
                //translate GUI parameter binning sources into MulticomponentHistogramCoreParams format
                int[] binningSources = p.get(BINNING_SOURCES);
                boolean spacialBinning = binningSources[0] < 0;
                if (spacialBinning)
                    for (int i = 0; i < binningSources.length; i++) binningSources[i] = -1 - binningSources[i];
                else {//translate component indexes (numeric, scalar components) into whole field indexes (all components)
                    for (int i = 0; i < binningSources.length; i++) {
                        String[] names = p.get(META_SCALAR_COMPONENT_NAMES);
                        binningSources[i] = Arrays.asList(inField.getComponentNames()).indexOf(names[binningSources[i]]);
                    }
                }
                //translate GUI parameter histogram operations into MulticomponentHistogramCoreParams format
                BinOperationState[][] binOperations = p.get(BIN_OPERATIONS);
                List<HistogramOperation> histogramOperations = new ArrayList<>();
                for (int i = 0; i < binOperations.length; i++)
                    for (int j = 0; j < binOperations[i].length; j++) {
                        BinOperationState state = binOperations[i][j];
                        if (state != null && state != BinOperationState.OFF) {
                            histogramOperations.add(
                                    new HistogramOperation(HistogramOperation.Operation.values()[1 + j], i, inField, state == BinOperationState.ON_LOG, p.get(LOG_SCALE_BASE), p.get(RESET_FIRST_BIN)));
                        }
                    }

                core.update(new MulticomponentHistogramCoreParams(
                        p.get(DIMENSION_LENGTHS).length,
                        p.get(BINNING_SOURCES)[0] >= 0 ? MulticomponentHistogramCoreParams.BINNING_BY_COMPONENTS : MulticomponentHistogramCoreParams.BINNING_BY_COORDINATES,
                        p.get(DIMENSION_LENGTHS),
                        spacialBinning ? null : binningSources,
                        spacialBinning ? binningSources : null,
                        p.get(LOG_SCALE),
                        p.get(RESET_FIRST_BIN),
                        p.get(LOG_SCALE_BASE),
                        p.get(INHERIT_EXTENTS),
                        p.get(ROUND_BYTE_DIMS_TO_32),
                        histogramOperations.toArray(new HistogramOperation[]{})));
                outRegularField = (RegularField) core.getOutField(MulticomponentHistogramCore.OUT_RFIELD_MAIN);
                if (p.get(MASK_OUT_EMPTY_BINS) && outRegularField.getComponent("histogram") != null) {
                    LogicLargeArray mask = new LogicLargeArray(outRegularField.getNNodes());
                    FloatLargeArray histogram = outRegularField.getComponent("histogram").getRawFloatArray();
                    for (long i = 0; i < histogram.length(); i++)
                        mask.setBoolean(i, histogram.getFloat(i) != 0);
                    outRegularField.setCurrentMask(mask);
                }
                setOutputValue("outField", new VNRegularField(outRegularField));
                outField = outRegularField;
                prepareOutputGeometry();
                show();
            }
        }
    }
}
