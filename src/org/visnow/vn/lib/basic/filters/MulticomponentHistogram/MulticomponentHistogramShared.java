/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.MulticomponentHistogram;

import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class MulticomponentHistogramShared
{
    static enum BinOperationType
    {
        SUM, MIN, MAX, AVG, STD, VSTD;
    }

    static enum BinOperationState
    {
         OFF("-"), ON("On"), ON_LOG("Log");

        private String label;

        private BinOperationState(String label)
        {
            this.label = label;
        }

        @Override
        public String toString()
        {
            return label;
        }
    }

    //Specification:
    //array of length 1, 2 or 3. All elements >=2
    static final ParameterName<int[]> DIMENSION_LENGTHS = new ParameterName("Dimension lengths");
    //XXX: this should be removed anyway
    static final ParameterName<Boolean> ROUND_BYTE_DIMS_TO_32 = new ParameterName("Round byte dims to multiple of 32");
    static final ParameterName<Boolean> INHERIT_EXTENTS = new ParameterName("Inherit extents from data (works with component data as interval source)");

    //non empty array of length == DIMENSION_LENGTHS.length
    //with all elements constraint 0 <= element <META_COMPONENT_NAMES.length
    //or all elements constraint -DIMENSION_LENGTHS.length <= element <= -1 (means spacial data)
    static final ParameterName<int[]> BINNING_SOURCES = new ParameterName("Data sources for bins");
    static final ParameterName<Boolean> RESET_FIRST_BIN = new ParameterName("Reset first bin");
    static final ParameterName<Boolean> LOG_SCALE = new ParameterName("Log scale");
    //XXX: add constraint / remove this??
    static final ParameterName<Float> LOG_SCALE_BASE = new ParameterName("Log scale base");
    static final ParameterName<Boolean> MASK_OUT_EMPTY_BINS = new ParameterName("Mask out empty bins");

    //XXX: fix this - replace with some reliable solution
//    static final ParameterName<Condition[]> FILTER_CONDITIONS = new ParameterName("Filter conditions");
    //XXX: fix this - replace with some reliable solution
//    static final ParameterName<Condition.Logic[]> FILTER_CONDITION_LOGICS = new ParameterName("Filter condition logic operators");
    //array of length META_ALL_COMPONENT_NAMES.length with each element of length BinOperations.values().length
    //can be null (means: bin operation not possible - like vstd for scalar components)
    static final ParameterName<BinOperationState[][]> BIN_OPERATIONS = new ParameterName("Per bin operations");

    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");

    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic)
    //not null array of Strings
    static final ParameterName<String[]> META_SCALAR_COMPONENT_NAMES = new ParameterName("META scalar numeric component names");
    //not null array of Strings
    static final ParameterName<String[]> META_ALL_COMPONENT_NAMES = new ParameterName("META all component names");

}
