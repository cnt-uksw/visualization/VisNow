/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.ConvexHull;


import java.util.Arrays;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.PointField;
import org.visnow.jscic.RegularField;
import static org.visnow.jscic.cells.CellType.*;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.lib.utils.QuickHull2D;
import org.visnow.vn.lib.utils.quickhull3d.QuickHull3D;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author know
 */
public class ConvexHullCore
{
    private ConvexHullCore()
    {
    }
    public static final float[] convexHullCoords(float[] crds)
    {
        boolean is2D = true;
        for (int i = 2; i < crds.length; i += 3)
            if (crds[i] != 0) {
                is2D = false;
                break;
            }
        if (is2D)
            return QuickHull2D.quickHull(crds);
        int nInNodes = crds.length / 3;
        double[] coords = new double[3 * nInNodes];
        for (int i = 0; i < crds.length; i++) 
            coords[i] = crds[i];
        QuickHull3D hull = new QuickHull3D();
        hull.build(coords);
        int nOutNodes = hull.getNumVertices();
        int[] outIndices = hull.getVertexPointIndices();
        float[] outCrds = new float[3 * nOutNodes];
        for (int i = 0; i < nOutNodes; i++) {
            int k = outIndices[i];
            System.arraycopy(crds, 3 * k, outCrds, 3 * i, 3);
        }
        return outCrds;
    }
    
    /**
     * creates convex hull of a set of points
     * @param crds  flat array of 3d point coordinates 
     * @return surface of convex hull of the input points; the only node component contains node indices 0:n output nodes - 1
     */
    public static final IrregularField convexHull(float[] crds)
    {
        boolean is2D = true;
        for (int i = 2; i < crds.length; i += 3)
            if (crds[i] != 0) {
                is2D = false;
                break;
            }
        int nOutNodes;
        float[] outCrds;
        IrregularField convexHull;
        if (is2D) {
             outCrds = QuickHull2D.quickHull(crds);
             nOutNodes = outCrds.length / 3;
             convexHull = new IrregularField(nOutNodes);
             convexHull.setCoords(new FloatLargeArray(outCrds), 0);
             int[] outSeg = new int[2 * nOutNodes];
             for (int i = 0; i < outSeg.length - 2; i += 2) {
                outSeg[i] = i / 2;
                outSeg[i + 1] = i / 2 + 1;
             }
             outSeg[2 * nOutNodes - 2] = nOutNodes - 1;
             outSeg[2 * nOutNodes - 1] = 0;
             CellArray seg = new CellArray(SEGMENT, outSeg, null, null);
             CellSet cs = new CellSet("perimeter");
             cs.setCellArray(seg);
             convexHull.addCellSet(cs);
             int[] ind = new int[nOutNodes];
             for (int i = 0; i < ind.length; i++) 
                 ind[i] = i;
             convexHull.addComponent(DataArray.create(ind, 1, "indices"));
             return convexHull;
        }
        int nInNodes = crds.length / 3;
        double[] coords = new double[3 * nInNodes];
        for (int i = 0; i < crds.length; i++) 
            coords[i] = crds[i];
        QuickHull3D hull = new QuickHull3D();
        hull.build(coords);
        nOutNodes = hull.getNumVertices();
        int[] outIndices = hull.getVertexPointIndices();
        outCrds = new float[3 * nOutNodes];
        for (int i = 0; i < nOutNodes; i++) {
            int k = outIndices[i];
            System.arraycopy(crds, 3 * k, outCrds, 3 * i, 3);
        }
        convexHull = new IrregularField(nOutNodes);
        convexHull.setCoords(new FloatLargeArray(outCrds), 0);
        convexHull.addComponent(DataArray.create(outIndices, 1, "indices"));
        int[][] faces = hull.getFaces();
        int[] nFaces = new int[5];
        Arrays.fill(nFaces, 0);
        for (int[] face : faces) {
            int faceLength = face.length;
            if (faceLength < 5 && faceLength > 0)
                nFaces[faceLength] += 1;
        }
        int[][] nodes = new int[5][];
        for (int i = 1; i < nodes.length; i++) 
            if (nFaces[i] > 0)
                nodes[i] = new int[i * nFaces[i]];
        Arrays.fill(nFaces, 0);
        for (int[] face : faces) {
            int faceLength = face.length;
            if (faceLength < 5 && faceLength > 0) {
                System.arraycopy(face, 0, nodes[faceLength], nFaces[faceLength] * faceLength, faceLength);
                nFaces[faceLength] += 1;
            }
        }
        CellSet cs= new CellSet();
        if (nodes[1] != null)
            cs.addCells(new CellArray(POINT, nodes[1], null, null));
        if (nodes[2] != null)
            cs.addCells(new CellArray(SEGMENT, nodes[2], null, null));
        if (nodes[3] != null)
            cs.addCells(new CellArray(TRIANGLE, nodes[3], null, null));
        if (nodes[4] != null)
            cs.addCells(new CellArray(QUAD, nodes[4], null, null));
        convexHull.addCellSet(cs);
        return convexHull;
    }
    
    private static float[] getBoundaryCoords(RegularField in)
    {
        int[] dims = in.getDims();
        int n, l, k;
        float[] crds;
        if (in.hasCoords()) {
            float[] crd = in.getCurrentCoords().getData();
            switch (in.getTrueNSpace()) {
            case 3:
                crds = new float[6 * (dims[0] * dims[1] + 
                                     (dims[0] *                 (dims[2] - 2) + 
                                                (dims[1] - 2) * (dims[2] - 2)))];
                n = 0;
                k = dims[0] * dims[1];
                l = (dims[2] - 1) * k;
                System.arraycopy(crd,     0, crds, 3 * n, 3 * k);   // bottom face coords copied to crds
                n += k;
                System.arraycopy(crd, 3 * l, crds, 3 * n, 3 * k);   // top face coords copied to crds
                n += k;
                k = dims[0];
                l = (dims[1] - 1) * k;
                for (int i = 1; i < dims[2] - 1; i++) {
                    System.arraycopy(crd, 3 * i * dims[1] * k,       crds, 3 * n, 3 * k); // i-th row of front face  coords copied to crds
                    n += k;
                    System.arraycopy(crd, 3 * (i * dims[1] * k + l), crds, 3 * n, 3 * k); // i-th row of back face  coords copied to crds
                    n += k;
                }
                k = 1;
                l = (dims[0] - 1) * k;
                for (int i = 1; i < dims[2] - 1; i++) 
                    for (int j = 1; j < dims[1] - 1; j++) {
                        System.arraycopy(crd, 3 *  (i * dims[1] + j) * dims[0] * k,      crds, 3 * n, 3 * k); // i,j-th element of left face  coords copied to crds
                        n += k;
                        System.arraycopy(crd, 3 * ((i * dims[1] + j) * dims[0] * k + l), crds, 3 * n, 3 * k); // i,j-th element of right face  coords copied to crds
                        n += k;
                    }
                return crds;
            case 2:
                crds = new float[6 * (dims[0] + dims[1] - 2)];
                n = 0;
                k = dims[0];
                l = (dims[1] - 1) * k;
                System.arraycopy(crd,     0, crds, 3 * n, 3 * k);   // bottom edge coords copied to crds
                n += k;
                System.arraycopy(crd, 3 * l, crds, 3 * n, 3 * k);   // top edge coords copied to crds
                n += k;
                k = 1;
                l = (dims[0] - 1) * k;
                for (int j = 1; j < dims[1] - 1; j++) {
                    System.arraycopy(crd, 3 *  j * dims[0] * k,     crds, 3 * n, 3 * k); // j-th element of left edge  coords copied to crds
                    n += k;
                    System.arraycopy(crd, 3 *  (j * dims[0] * k + l), crds, 3 * n, 3 * k); // j-th element of right edge  coords copied to crds
                    n += k;
                }
                return crds;
            default:
                return crd;
            }
        }
        else {
            int nBoxVerts = in.getTrueNSpace() == 2 ? 4 : 8;
            int[] inDims = in.getDims();
            float[][] inAffine = in.getAffine();
            crds = new float[3 * nBoxVerts];
            for (int i = 0; i < nBoxVerts; i++) 
                for (int j = 0; j < 3; j++) {
                    crds[3 * i + j] = inAffine[3][j];
                    for (k = 0; k < in.getTrueNSpace(); k++)
                        crds[3 * i + j] += ((i >> k) & 1) * (inDims[k] - 1) * inAffine[k][j];
                }
            return crds;
        }
    }
    
    public static final IrregularField convexHull(Field in)
    {
        if (in instanceof IrregularField || in instanceof PointField)
            return convexHull(in.getCurrentCoords().getData());
        else 
            return convexHull(getBoundaryCoords((RegularField)in));
    }

    public static final float[] convexHullCoords(Field in)
    {
        if (in instanceof IrregularField)
            return convexHullCoords(in.getCurrentCoords().getData());
        else 
            return convexHullCoords(getBoundaryCoords((RegularField)in));
    }

}

