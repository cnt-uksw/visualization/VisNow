/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.Canny;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.ModuleCore;

/**
 *
 * @author Andrzej Rutkowski (rudy@mat.uni.torun.pl)
 * <p>
 */
public class CannyMaskedGradients extends ModuleCore
{

    private CannyMaskedGradGUI panel = new CannyMaskedGradGUI();
    private CannyMaskedGradParams params = new CannyMaskedGradParams();

    public CannyMaskedGradients()
    {
        panel.setParams(params);
        panel.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                startAction();
            }
        });
        /*addModuleUI(panel);*/
        setPanel(panel);
    }

    public String getStandardName()
    {
        return "Canny Masked Grad.";
    }

    public static CannyMaskedGradients getInstance()
    {
        return new CannyMaskedGradients();
    }

    /*public static InputDesc[] getInputTable() {
     return new InputDesc[] {
     new InputDesc("inField", VNRegularField.class, null, true, true, InputVisibility.VISIBLE),
     new InputDesc("Edges mask", VNRegularField.class, null, true, true, InputVisibility.VISIBLE),
     };
     }

     public static OutputDesc[] getOutputTable() {
     return new OutputDesc[] {
     new OutputDesc("Gradients", VNRegularField.class),
     };
     }*/
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;


    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null)
            return;
        RegularField inField = ((VNRegularField) getInputFirstValue("inField")).getField();
        if (inField == null)
            return;
        int[] dims = inField.getDims();
        if (dims == null)
            return;

        if (getInputFirstValue("Edges mask") == null)
            return;
        RegularField inMaskField = ((VNRegularField) getInputFirstValue("Edges mask")).getField();
        if (inMaskField == null)
            return;
        int[] maskDims = inMaskField.getDims();
        if (maskDims == null)
            return;

        if (maskDims.length != dims.length)
            return;

        if (dims.length < 2 || dims.length > 3) {
            return;
            // TODO: wiecej wymiarow - ogolna metoda
        }

        for (int i = 0; i < dims.length; ++i) {
            if (dims[i] != maskDims[i])
                return;
        }

        /*RegularField outField = new RegularFld(dims, inField.getPts());*/
        /*RegularField outField= inField.clone();*/
        RegularField outField = new RegularField(dims);
        outField.setAffine(inField.getAffine());
        outField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
        for (int i = 0; i < inField.getNComponents(); i++) {
            CoreBase cannyCore = null;
            DataArray datain = inField.getComponent(i);

            switch (datain.getType()) {
                case FIELD_DATA_BYTE:
                    cannyCore = new CoreB(datain, dims);
                    break;
                case FIELD_DATA_SHORT:
                    cannyCore = new CoreS(datain, dims);
                    break;
                case FIELD_DATA_INT:
                    cannyCore = new CoreI(datain, dims);
                    break;
                case FIELD_DATA_FLOAT:
                    cannyCore = new CoreF(datain, dims);
                    break;
                case FIELD_DATA_DOUBLE:
                    cannyCore = new CoreD(datain, dims);
                    break;
                default:
                    cannyCore = new CoreF(datain, dims);
                    break;
            }
            if (cannyCore != null) {
                cannyCore.calculateCannyGradients(inMaskField.getComponent(0), maskDims,
                                                  dims, true, params, outField, "CannyGrad" + i);
            }
        }

        setOutputValue("Gradients", new VNRegularField(outField));
    }
}
