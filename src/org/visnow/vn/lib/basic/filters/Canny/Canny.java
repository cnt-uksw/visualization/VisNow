/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.Canny;

import org.apache.log4j.Logger;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.jscic.RegularField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.utils.SwingInstancer;
import static org.visnow.vn.lib.basic.filters.Canny.CannyShared.*;

/**
 *
 * @author Andrzej Rutkowski (rudy@mat.uni.torun.pl)
 * @author modified by pregulski, ICM, UW
 */
public class Canny extends OutFieldVisualizationModule
{

    private static final Logger LOGGER = Logger.getLogger(Canny.class);

    private int runQueue = 0;

    private GUI cannyPanel;
    RegularField inField;

    public Canny()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name != null && name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                cannyPanel = new GUI();
                ui.addComputeGUI(cannyPanel);
                setPanel(ui);
                cannyPanel.setParameters(parameters);
            }
        });
    }

    public String getStandardName()
    {
        return "Canny";
    }

    public static Canny getInstance()
    {
        return new Canny();
    }

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(THRESHOLDLOW, 0.3f),
            new Parameter<>(THRESHOLDHIGH, 0.45f),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN)
        };
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        cannyPanel.updateGUI(parameters, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive()
    {
        LOGGER.debug("");

        if (getInputFirstValue("inField") != null) {
            //1. get new field
            RegularField newField = ((VNRegularField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA() && (inField == null || inField.getTimestamp() != newField.getTimestamp());
            boolean isNewField = !isFromVNA() && newField != inField;
            inField = newField;
            int[] dims = inField.getDims();

            //2. validate parameters
            Parameters p = parameters.getReadOnlyClone();

            //3. update GUI
            notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

            //4. computation
            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data
                if (dims.length < 2 || dims.length > 3) {
                    outRegularField = inField.cloneShallow();
                } else {
                    outRegularField = new RegularField(dims);
                    outRegularField.setAffine(inField.getAffine());
                    outRegularField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
                    for (int i = 0; i < inField.getNComponents(); i++) {
                        CoreBase cannyCore = null;
                        DataArray datain = inField.getComponent(i);
                        switch (datain.getType()) {
                            case FIELD_DATA_BYTE:
                                cannyCore = new CoreB(datain, dims);
                                break;
                            case FIELD_DATA_SHORT:
                                cannyCore = new CoreS(datain, dims);
                                break;
                            case FIELD_DATA_INT:
                                cannyCore = new CoreI(datain, dims);
                                break;
                            case FIELD_DATA_FLOAT:
                                cannyCore = new CoreF(datain, dims);
                                break;
                            case FIELD_DATA_DOUBLE:
                                cannyCore = new CoreD(datain, dims);
                                break;
                            default:
                                cannyCore = new CoreF(datain, dims);
                                break;
                        }
                        if (cannyCore != null) {
                            byte[] out = cannyCore.calculate(p.get(THRESHOLDHIGH), p.get(THRESHOLDLOW));
                            outRegularField.addComponent(DataArray.create(out, 1, "canny_" + inField.getComponent(i).getName()));
                        }
                    }
                }

                //5. propagate
                setOutputValue("outField", new VNRegularField(outRegularField));
                outField = outRegularField;
                prepareOutputGeometry();
                show();
            }
        }
    }
}
