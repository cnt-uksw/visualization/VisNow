/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.TransformGeometry;

import java.util.ArrayList;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.geometries.objects.SignalingTransform3D;
import org.visnow.vn.geometries.events.TransformEvent;
import org.visnow.vn.geometries.events.TransformListener;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNGeometryTransform;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class TransformGeometry extends ModuleCore
{

    protected Field inField = null;
    protected IrregularField inIrregularField = null;
    protected RegularField inRegularField = null;
    protected SignalingTransform3D transform = null;
    protected IrregularField outIrregularField = null;
    protected RegularField outRegularField = null;
    private GUI ui = null;
    protected Params params;

    public TransformGeometry()
    {
        parameters = params = new Params();
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                ui = new GUI();
            }
        });
        ui.setParams(params);
        setPanel(ui);
    }

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null || ((VNField) getInputFirstValue("inField")).getField() == null) {
            return;
        }

        inField = ((VNField) getInputFirstValue("inField")).getField();
        if (inField instanceof RegularField) {
            inRegularField = (RegularField) inField;
            inIrregularField = null;
        } else {
            inRegularField = null;
            inIrregularField = (IrregularField) inField;
        }

        if (getInputFirstValue("transform") == null || ((VNGeometryTransform) getInputFirstValue("transform")).getCurrentTransform() == null) {
            return;
        }
        transform = ((VNGeometryTransform) getInputFirstValue("transform")).getCurrentTransform();
        transform.addTransformListener(new TransformListener()
        {
            @Override
            public void transformChanged(TransformEvent e)
            {
                startAction();
            }
        });
        float[][] trMatrix = transform.getMatrix();
        if (inRegularField != null) {
            outRegularField = inRegularField.cloneShallow();
            float[][] inAffine = inRegularField.getAffine();
            float[][] outAffine = new float[4][3];
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 3; j++) {
                    outAffine[i][j] = 0;
                    for (int k = 0; k < 3; k++) {
                        outAffine[i][j] += trMatrix[j][k] * inAffine[i][k];
                    }

                    if (i == 3) {
                        outAffine[i][j] += trMatrix[j][3];
                    }
                }
            }
            if (inRegularField.getCurrentCoords() == null && params.generateCoords()) {
                int[] dims = inRegularField.getDims();
                FloatLargeArray outCoords = new FloatLargeArray(3 * (long)dims[0] * (long)dims[1] * (long)dims[2], false);
                for (long i = 0, l = 0; i < dims[2]; i++) {
                    for (long j = 0; j < dims[1]; j++) {
                        for (long k = 0; k < dims[0]; k++) {
                            for (long m = 0; m < 3; m++, l++) {
                                outCoords.setFloat(l, outAffine[3][(int)m]);
                                for (int n = 0; n < 3; n++) {
                                    outCoords.setFloat(l, outCoords.getFloat(l) + i * outAffine[2][(int)m] + j * outAffine[1][(int)m] + k * outAffine[0][(int)m]);
                                }
                            }
                        }
                    }
                }
            }
            if (inRegularField.getCoords() != null && !inRegularField.getCoords().isEmpty()) {
                int nframes = inRegularField.getCoords().getNSteps();
                ArrayList<LargeArray> series = inRegularField.getCoords().getValues();
                for (int frame = 0; frame < nframes; frame++) {
                    FloatLargeArray inCoords = (FloatLargeArray)series.get(frame);
                    FloatLargeArray outCoords = new FloatLargeArray(inCoords.length(), false);
                    for (long i = 0; i < outCoords.length(); i += 3) {
                        for (int j = 0; j < 3; j++) {
                            outCoords.setFloat(i + j, trMatrix[j][3]);
                            for (int k = 0; k < 3; k++) {
                                outCoords.setFloat(i + j, outCoords.getFloat(i + j) + trMatrix[j][k] * inCoords.getFloat(i + k));
                            }
                        }
                    }
                    outRegularField.addCoords(outCoords);
                }
            }
            outRegularField.setAffine(outAffine);
            setOutputValue("outRegularField", new VNRegularField(outRegularField));
            setOutputValue("outIrregularField", null);
        } else {
            outIrregularField = inIrregularField.cloneShallow();
            outIrregularField.setCoords(new TimeData(DataArrayType.FIELD_DATA_FLOAT));
            int nframes = inField.getCoords().getNSteps();
            ArrayList<LargeArray> series = inField.getCoords().getValues();
            for (int frame = 0; frame < nframes; frame++) {
                FloatLargeArray inCoords = (FloatLargeArray)series.get(frame);
                FloatLargeArray outCoords = new FloatLargeArray(inCoords.length(), false);
                for (long i = 0; i < outCoords.length(); i += 3) {
                    for (long j = 0; j < 3; j++) {
                        outCoords.setFloat(i + j, trMatrix[(int)j][3]);
                        for (int k = 0; k < 3; k++) {
                            outCoords.setFloat(i + j, outCoords.getFloat(i + j) + trMatrix[(int)j][k] * inCoords.getFloat(i + k));
                        }
                    }
                }
                outIrregularField.addCoords(outCoords);
            }
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", new VNIrregularField(outIrregularField));
        }
    }
}
