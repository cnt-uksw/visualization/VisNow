/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */

package org.visnow.vn.lib.basic.filters.ComponentCalculator;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;
import org.visnow.jscic.utils.PhysicalConstants;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.gui.swingwrappers.RadioButton;
import org.visnow.vn.gui.widgets.RunButton;
import org.visnow.vn.gui.widgets.SectionHeader;
import static org.visnow.vn.lib.basic.filters.ComponentCalculator.ComponentCalculatorShared.*;
import org.visnow.vn.lib.utils.StringUtils;
import org.visnow.vn.lib.utils.expressions.Operator;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.swing.UIStyle;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl), University of Warsaw, ICM
 * modified by:
 * @author Krzysztof Nowinski (know@icm.edu.pl, knowpl@gmail.com), University of Warsaw, ICM
 *
 */
public class GUI extends javax.swing.JPanel
{
    private static final Logger LOGGER = Logger.getLogger(GUI.class);

    private ParameterProxy parameterProxy;

    //unfortunatelly this flag is necessary to distinguish user action on jTable from setter actions.
    private boolean tableActive = true;
    //this is necessary to avoid firing textArea event after a delay (when parameters are active again)
    private boolean textAreaActive = true;

    //arrays for restore user values (in generator mode, when dim num changed)
    int[] generatorFullDims = new int[]{2, 2, 2};
    float[][] generatorFullExtents = new float[][]{{-0.5f, -0.5f, -0.5f}, {0.5f, 0.5f, 0.5f}};

    private final Operator[] opers = Operator.values();
    private final ArrayList<JButton> operatorButtons = new ArrayList<>();
    private final String[] constantDescriptions;
    private final String[] constantSymbols;
    private String preamble = "";

    private FileNameExtensionFilter vncFilter = new FileNameExtensionFilter("Component calculator program files (*.vnc)", "vnc", "VNC");

    public enum FoldablePanel
    {
        Variables, Constants, Operators
    }

    private String[] aliases;
    /**
     * Creates new form GUI
     */
    public GUI()
    {
        initComponents();

        generatorTablePanel.add(generatorTable.getTableHeader(), BorderLayout.NORTH);
        generatorTable.getTableHeader().setReorderingAllowed(false);

        initControlListeners();
        for (Operator oper : opers) {
            String name = oper.toString();
            String label = oper.getLabel();
            JButton b = new JButton(label);
            b.setToolTipText(oper.getTooltip());
            b.addActionListener(new java.awt.event.ActionListener()
            {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt)
                {
                    int c = expressionsTA.getCaretPosition();
                    if (name.matches("^[A-Za-z].*$")) {
                        expressionsTA.insert(name + "()", c);
                        expressionsTA.setCaretPosition(c + name.length() + 1);
                    }
                    else {
                        expressionsTA.insert(name , c);
                        expressionsTA.setCaretPosition(c + name.length());
                    }
                }
            });
            b.setMargin(new java.awt.Insets(1, 2, 1, 2));
            b.setPreferredSize(new java.awt.Dimension(40, 14));
            operatorButtons.add(b);
            operatorsPanel.add(b);
        }
        JButton b = new JButton("( )");
        b.addActionListener(new java.awt.event.ActionListener()
        {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                int c = expressionsTA.getCaretPosition();
                expressionsTA.insert("( )" , c);
                expressionsTA.setCaretPosition(c + 1);
            }
        });
        b.setMargin(new java.awt.Insets(1, 2, 1, 2));
        b.setPreferredSize(new java.awt.Dimension(40, 16));
        operatorButtons.add(b);
        operatorsPanel.add(b);

        constantDescriptions = new String[PhysicalConstants.values().length + 4];
        constantSymbols = new String[PhysicalConstants.values().length + 4];
        constantDescriptions[0] = "";
        constantDescriptions[1] = "";
        constantDescriptions[2] = "";
        constantDescriptions[3] = ""; // table header, e, pi, i do not require description
        constantSymbols[0] = "";
        constantSymbols[1] = "PI";
        constantSymbols[2] = "E";
        constantSymbols[3] = "I"; // table header, e, pi, i do not require description
        DefaultTableModel constantsTableModel = (DefaultTableModel) constantsTable.getModel();
        constantsTableModel.setRowCount(0);
        constantsTableModel.addRow(new Object[]{"<html>&pi</html>","", ""});
        constantsTableModel.addRow(new Object[]{"e", "", ""});
        constantsTableModel.addRow(new Object[]{"i", "", ""});
        for (int i = 0; i < PhysicalConstants.values().length; i++) {
            PhysicalConstants value = PhysicalConstants.values()[i];
            constantsTableModel.addRow(new Object[]{value.getHtmlSymbol(), value.getDescription(), value.getUnit()});
            constantSymbols[i + 4] = "_" + value.getSymbol();
            constantDescriptions[i + 4] = value.getDescription();
        }
        constantsTable.getColumnModel().getColumn(0).setPreferredWidth(20);
        constantsTable.getColumnModel().getColumn(1).setPreferredWidth(150);
        constantsTable.getColumnModel().getColumn(2).setPreferredWidth(60);
        constantsTable.getTableHeader().setReorderingAllowed(false);

        componentTable.getColumnModel().getColumn(0).setPreferredWidth(180);
        componentTable.getColumnModel().getColumn(1).setPreferredWidth(20);
        componentTable.getColumnModel().getColumn(2).setPreferredWidth(40);
        componentTable.getTableHeader().setReorderingAllowed(false);

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
        for (int i = 1; i < 4; i++) {
            pseudoVarsTable.getColumnModel().getColumn(i).setCellRenderer(rightRenderer);
            pseudoVarsTable.getColumnModel().getColumn(i).setPreferredWidth(30);
        }
        panelsUpdateExpandedState();
        loadFileChooser.setFileFilter(vncFilter);
        loadFileChooser.setDialogTitle("Load VisNow calculator program");
        loadFileChooser.setApproveButtonText("load");
        saveFileChooser.setFileFilter(vncFilter);
        saveFileChooser.setDialogTitle("Save current program");
        saveFileChooser.setApproveButtonText("save");
//        preamble = "// regular field 50 50 50 //\n";
//        updatePreamble(preamble);
    }

    private void initControlListeners()
    {
        generatorTable.getModel().addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                if (tableActive) {
                    int dimNum = generatorTable.getRowCount();
                    int dims[] = new int[dimNum];
                    float[][] extents = new float[2][dimNum];
                    for (int i = 0; i < dimNum; i++) {
                        if ((Integer) generatorTable.getValueAt(i, 1) < 2) generatorTable.setValueAt(2, i, 1);
                        dims[i] = (Integer) generatorTable.getValueAt(i, 1);
                        extents[0][i] = (Float) generatorTable.getValueAt(i, 2);
                        extents[1][i] = (Float) generatorTable.getValueAt(i, 3);
                        if (extents[1][i] <= extents[0][i]) extents[1][i] = extents[0][i] + Math.ulp(extents[0][i]);
                    }

                    updateGeneratorInternalArrays(dims, extents);

//                    StringBuilder preambleBuilder = new StringBuilder();
//                    preambleBuilder.append("// regular field ");
//                    for (int i = 0; i < dims.length; i++)
//                        preambleBuilder.append(" " + dims[i]);
//                    preambleBuilder.append(" //\n");
//                    preamble = preambleBuilder.toString();
//                    updatePreamble(preamble);
                    parameterProxy.set(GENERATOR_DIMENSION_LENGTHS, dims,
                                       GENERATOR_EXTENTS, extents);
                }
            }
        });

        componentTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        componentTable.getModel().addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                if (tableActive) {
                    aliases = new String[componentTable.getRowCount()];
                    for (int i = 0; i < componentTable.getRowCount(); i++)
                        aliases[i] = (String) componentTable.getValueAt(i, 2);
                    parameterProxy.set(INPUT_ALIASES, aliases);
                }
            }
        });

        constantsTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        pseudoVarsTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        expressionsTA.getDocument().addDocumentListener(new DocumentListener()
        {
            @Override
            public void insertUpdate(DocumentEvent e)
            {
                processEvent();
            }

            @Override
            public void removeUpdate(DocumentEvent e)
            {
                processEvent();
            }

            @Override
            public void changedUpdate(DocumentEvent e)
            {
                processEvent();
            }

            private long lastModifyTime = 0;
            private SwingWorker worker = null;

            synchronized private void processEvent()
            {
                if (textAreaActive) {
                    lastModifyTime = System.currentTimeMillis();

                    if (worker == null) {
                        worker = new SwingWorker()
                        {
                            @Override
                            protected Object doInBackground()
                            {
                                try {
                                    while (System.currentTimeMillis() - lastModifyTime < 1000) Thread.sleep(10);
                                } catch (InterruptedException ex) {
                                    LOGGER.warn(ex);
                                }
                                return null;
                            }

                            @Override
                            protected void done()
                            {
                                worker = null;
                                parameterProxy.set(EXPRESSION_LINES, expressionsTA.getText().split("\\n", -1));
                            }

                        };
                        worker.execute();
                    }
                }
            }
        });
    }

    public void setParameterProxy(ParameterProxy targetParameterProxy)
    {
        //putting run button between logic and gui
        parameterProxy = runButton.getPlainProxy(targetParameterProxy, RUNNING_MESSAGE);
    }

    public void updateGUI(ParameterProxy p, boolean resetFully, boolean setRunButtonPending, String preamble)
    {

        generatorPanel.setVisible(p.get(META_IS_GENERATOR));
        variablesPane.setVisible(!p.get(META_IS_GENERATOR));

        String expressionLines = StringUtils.join(p.get(EXPRESSION_LINES), System.lineSeparator());

        textAreaActive = false;
        if (!expressionsTA.getText().equals(expressionLines)) expressionsTA.setText(expressionLines);
        textAreaActive = true;

        singlePrecisionRB.setSelected(p.get(PRECISION) == Precision.SINGLE);
        doublePrecisionRB.setSelected(p.get(PRECISION) == Precision.DOUBLE);

        RadioButton[] dimButtons = new RadioButton[] {dim1RB, dim2RB, dim3RB};
        dimButtons[p.get(GENERATOR_DIMENSION_LENGTHS).length - 1].setSelected(true);

        retainCB.setSelected(p.get(RETAIN));

        updateGeneratorInternalArrays(p.get(GENERATOR_DIMENSION_LENGTHS), p.get(GENERATOR_EXTENTS));
        updateGeneratorTable(p.get(GENERATOR_DIMENSION_LENGTHS), p.get(GENERATOR_EXTENTS));
        updateComponentTable(p.get(META_COMPONENT_NAMES), p.get(META_COMPONENT_VECLEN), p.get(INPUT_ALIASES));
        if(p.get(META_IS_GENERATOR))
            updatePseudoVarsTable(p.get(GENERATOR_DIMENSION_LENGTHS).length);
        else
            updatePseudoVarsTable(p.get(META_FIELD_DIMENSION));
//        updatePreamble(preamble);
        runButton.updateAutoState(p.get(RUNNING_MESSAGE));
        runButton.updatePendingState(setRunButtonPending);
    }

    private void updateGeneratorInternalArrays(int[] generatorDims, float[][] generatorExtents)
    {
        System.arraycopy(generatorDims,       0, generatorFullDims, 0, generatorDims.length);
        System.arraycopy(generatorExtents[0], 0, generatorFullExtents[0], 0, generatorExtents[0].length);
        System.arraycopy(generatorExtents[1], 0, generatorFullExtents[1], 0, generatorExtents[1].length);
        updatePseudoVarsTable(generatorDims.length);
    }

    private void updateGeneratorTable(int[] generatorDims, float[][] generatorExtents)
    {
        tableActive = false;
        DefaultTableModel generatorTableModel = (DefaultTableModel) generatorTable.getModel();

        while (generatorTableModel.getRowCount() > 0) generatorTableModel.removeRow(0);

        for (int i = 0; i < generatorDims.length; i++)
            generatorTableModel.addRow(new Object[]{new String[]{"x", "y", "z"}[i],
                                                    generatorDims[i],
                                                    generatorExtents[0][i],
                                                    generatorExtents[1][i]});
        tableActive = true;
    }

    private boolean isRowEquivalent(DefaultTableModel componentTableModel, int row, String[] componentNames, int[] componentVecLens)
    {
        if (!componentNames[row].equals(componentTableModel.getValueAt(row, 0)))
            return false;
        return componentVecLens[row] == ((Integer)componentTableModel.getValueAt(row, 1)).intValue();
    }

    private boolean checkComponentTableValidity(DefaultTableModel componentTableModel, String[] componentNames, int[] componentVecLens)
    {
        if (componentTableModel.getRowCount() != componentNames.length)
            return false;
        for (int i = 0; i < componentNames.length; i++)
            if (!isRowEquivalent(componentTableModel, i, componentNames, componentVecLens))
                return false;
        return true;
    }

    private void updateComponentTable(String[] componentNames, int[] componentVecLens, String[] componentAliases)
    {
        tableActive = false;
        DefaultTableModel componentTableModel = (DefaultTableModel) componentTable.getModel();
        if (!checkComponentTableValidity(componentTableModel, componentNames, componentVecLens)) {
            while (componentTableModel.getRowCount() > 0) componentTableModel.removeRow(0);
            for (int i = 0; i < componentNames.length; i++)
                componentTableModel.addRow(new Object[]{componentNames[i], componentVecLens[i], componentAliases[i]});
            Dimension preferred = new Dimension(200, Math.min(400, componentTable.getRowHeight() * (1 + componentTable.getRowCount()) + 70));
            componentTable.setMinimumSize(preferred);
            componentTable.setPreferredSize(preferred);
            componentTable.setSize(preferred);
            componentTable.repaint();
        }
        componentTable.setVisible(true);
        tableActive = true;
    }

    private void updatePseudoVarsTable(int dim)
    {
        String[] ind = {"_i","_j","_k"};
        if (dim < 1)
            dim = 1;
        DefaultTableModel pseudoVarsTableModel = (DefaultTableModel)pseudoVarsTable.getModel();
        for (int i = 0; i < ind.length; i++)
            pseudoVarsTableModel.setValueAt(i >= dim ? "" : ind[i], 0, i + 1);
        pseudoVarsTable.repaint();
    }

    public void panelFold(FoldablePanel panel, boolean expanded)
    {
        SectionHeader sectionHeader = null;

        switch (panel) {
            case Variables:
                sectionHeader = varHeader;
                break;
            case Constants:
                sectionHeader = constHeader;
                break;
            case Operators:
                sectionHeader = operatorHeader;
                break;
        }

        sectionHeader.setExpanded(expanded);
        panelsUpdateExpandedState();
    }

    private void panelsUpdateExpandedState()
    {
        varPanel.setVisible(varHeader.isExpanded() && varHeader.isVisible());
        constPanel.setVisible(constHeader.isExpanded() && constHeader.isVisible());
        operatorsPanel.setVisible(operatorHeader.isExpanded() && operatorHeader.isVisible());
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        precisionRBG = new javax.swing.ButtonGroup();
        generatorRBG = new javax.swing.ButtonGroup();
        CalculatorFrame = new javax.swing.JFrame();
        loadFileChooser = new javax.swing.JFileChooser();
        saveFileChooser = new javax.swing.JFileChooser();
        calculatorPanel = new javax.swing.JPanel();
        generatorPanel = new javax.swing.JPanel();
        generatorLabel = new javax.swing.JLabel();
        dimPanel = new javax.swing.JPanel();
        dim1RB = new org.visnow.vn.gui.swingwrappers.RadioButton();
        dim2RB = new org.visnow.vn.gui.swingwrappers.RadioButton();
        dim3RB = new org.visnow.vn.gui.swingwrappers.RadioButton();
        generatorTablePanel = new javax.swing.JPanel();
        generatorTable = new javax.swing.JTable();
        varHeader = new org.visnow.vn.gui.widgets.SectionHeader();
        varPanel = new javax.swing.JPanel();
        variablesPane = new javax.swing.JScrollPane();
        componentTable = new javax.swing.JTable();
        pseudoVarsTable = new javax.swing.JTable();
        constHeader = new org.visnow.vn.gui.widgets.SectionHeader();
        constPanel = new javax.swing.JPanel();
        constPane = new javax.swing.JScrollPane();
        constantsTable = new JTable()
        {
            public String getToolTipText(MouseEvent e)
            {
                String tip = null;
                java.awt.Point p = e.getPoint();
                int rowIndex = rowAtPoint(p);
                try {
                    if(rowIndex != 0)
                    tip = constantDescriptions[rowIndex + 1];
                } catch (RuntimeException e1) {
                }
                return tip;
            }
        };
        operatorHeader = new org.visnow.vn.gui.widgets.SectionHeader();
        operatorsPanel = new javax.swing.JPanel();
        exprPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        expressionsTA = new javax.swing.JTextArea();
        ignoreUnitsCB = new javax.swing.JCheckBox();
        retainCB = new javax.swing.JCheckBox();
        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        singlePrecisionRB = new org.visnow.vn.gui.swingwrappers.RadioButton();
        doublePrecisionRB = new org.visnow.vn.gui.swingwrappers.RadioButton();
        errorMessage = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        saveButton = new javax.swing.JButton();
        loadButton = new javax.swing.JButton();
        runButton = new org.visnow.vn.gui.widgets.RunButton();
        jPanel3 = new javax.swing.JPanel();
        detachButton = new javax.swing.JToggleButton();

        CalculatorFrame.setSize(new java.awt.Dimension(300, 800));
        CalculatorFrame.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                CalculatorFrameWindowClosing(evt);
            }
        });

        setBorder(javax.swing.BorderFactory.createEmptyBorder(4, 4, 4, 4));
        setLayout(new java.awt.GridBagLayout());

        calculatorPanel.setLayout(new java.awt.GridBagLayout());

        generatorPanel.setLayout(new java.awt.GridBagLayout());

        generatorLabel.setText("Generate field:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 6, 0, 26);
        generatorPanel.add(generatorLabel, gridBagConstraints);

        dimPanel.setLayout(new java.awt.GridLayout(1, 0));

        generatorRBG.add(dim1RB);
        dim1RB.setText("1D");
        dim1RB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                generatorDimRBUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        dimPanel.add(dim1RB);

        generatorRBG.add(dim2RB);
        dim2RB.setText("2D");
        dim2RB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                generatorDimRBUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        dimPanel.add(dim2RB);

        generatorRBG.add(dim3RB);
        dim3RB.setText("3D");
        dim3RB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                generatorDimRBUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        dimPanel.add(dim3RB);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        generatorPanel.add(dimPanel, gridBagConstraints);

        generatorTablePanel.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 0, 0, javax.swing.UIManager.getDefaults().getColor("ScrollBar.darkShadow")));
        generatorTablePanel.setLayout(new java.awt.BorderLayout());

        generatorTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null,  new Integer(50),  new Float(-0.5),  new Float(0.5)}
            },
            new String [] {
                "dim", "size", "min", "max"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        generatorTable.setMaximumSize(new java.awt.Dimension(6016, 60));
        generatorTable.setMinimumSize(new java.awt.Dimension(160, 55));
        generatorTable.setPreferredSize(new java.awt.Dimension(200, 60));
        generatorTable.setRowSelectionAllowed(false);
        generatorTablePanel.add(generatorTable, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        generatorPanel.add(generatorTablePanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        calculatorPanel.add(generatorPanel, gridBagConstraints);

        varHeader.setShowCheckBox(false);
        varHeader.setText("Variables and coordinates");
        varHeader.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                varHeaderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                varHeaderUserAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        calculatorPanel.add(varHeader, gridBagConstraints);

        varPanel.setMinimumSize(new java.awt.Dimension(150, 100));
        varPanel.setPreferredSize(new java.awt.Dimension(220, 170));
        varPanel.setLayout(new java.awt.GridBagLayout());

        componentTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null}
            },
            new String [] {
                "Component name", "Vl", "Var "
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        componentTable.setToolTipText("<html>click first or second column<p>\nto insertvariable name in formula<p>\nclick third column to edit variable name<p>\n(alias to the component name)</html>\n");
        componentTable.setMaximumSize(new java.awt.Dimension(400, 200));
        componentTable.setMinimumSize(new java.awt.Dimension(145, 50));
        componentTable.setOpaque(false);
        componentTable.setPreferredSize(new java.awt.Dimension(200, 120));
        componentTable.setRowSelectionAllowed(false);
        componentTable.setSelectionBackground(new java.awt.Color(153, 204, 255));
        componentTable.setSelectionForeground(new java.awt.Color(51, 51, 51));
        componentTable.getTableHeader().setResizingAllowed(false);
        componentTable.getTableHeader().setReorderingAllowed(false);
        componentTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                componentTableMouseClicked(evt);
            }
        });
        variablesPane.setViewportView(componentTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        varPanel.add(variablesPane, gridBagConstraints);

        pseudoVarsTable.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pseudoVarsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"index", "_i", "_j", "_k"},
                {"coords", "_x", "_y", "_z"}
            },
            new String [] {
                "vector", "", "", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        pseudoVarsTable.setMaximumSize(new java.awt.Dimension(400, 45));
        pseudoVarsTable.setMinimumSize(new java.awt.Dimension(145, 41));
        pseudoVarsTable.setName(""); // NOI18N
        pseudoVarsTable.setPreferredSize(new java.awt.Dimension(225, 44));
        pseudoVarsTable.setRowHeight(20);
        pseudoVarsTable.setRowSelectionAllowed(false);
        pseudoVarsTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pseudoVarsTableMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(6, 0, 6, 0);
        varPanel.add(pseudoVarsTable, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        calculatorPanel.add(varPanel, gridBagConstraints);

        constHeader.setExpanded(false);
        constHeader.setShowCheckBox(false);
        constHeader.setText("Mathematical and physical constants");
        constHeader.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                constHeaderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                constHeaderUserAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        calculatorPanel.add(constHeader, gridBagConstraints);

        constPanel.setLayout(new java.awt.BorderLayout());

        constPane.setMinimumSize(new java.awt.Dimension(150, 60));
        constPane.setOpaque(false);
        constPane.setPreferredSize(new java.awt.Dimension(200, 160));

        constantsTable.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        constantsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "  ", "description", "unit"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        constantsTable.setMaximumSize(new java.awt.Dimension(400, 1000));
        constantsTable.setMinimumSize(new java.awt.Dimension(150, 350));
        constantsTable.setPreferredSize(new java.awt.Dimension(220, 350));
        constantsTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                constantsTableMouseClicked(evt);
            }
        });
        constPane.setViewportView(constantsTable);

        constPanel.add(constPane, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        calculatorPanel.add(constPanel, gridBagConstraints);

        operatorHeader.setShowCheckBox(false);
        operatorHeader.setText("Operators and functions");
        operatorHeader.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                operatorHeaderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                operatorHeaderUserAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        calculatorPanel.add(operatorHeader, gridBagConstraints);

        operatorsPanel.setMinimumSize(new java.awt.Dimension(200, 150));
        operatorsPanel.setOpaque(false);
        operatorsPanel.setPreferredSize(new java.awt.Dimension(200, 150));
        operatorsPanel.setLayout(new java.awt.GridLayout(6, 6));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        calculatorPanel.add(operatorsPanel, gridBagConstraints);

        exprPanel.setFocusTraversalPolicyProvider(true);
        exprPanel.setMaximumSize(new java.awt.Dimension(2147483647, 500));
        exprPanel.setMinimumSize(new java.awt.Dimension(150, 250));
        exprPanel.setName(""); // NOI18N
        exprPanel.setPreferredSize(new java.awt.Dimension(200, 350));
        exprPanel.setRequestFocusEnabled(false);
        exprPanel.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("Expressions:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 4, 0);
        exprPanel.add(jLabel1, gridBagConstraints);

        jScrollPane3.setAutoscrolls(true);
        jScrollPane3.setMinimumSize(new java.awt.Dimension(150, 50));
        jScrollPane3.setPreferredSize(new java.awt.Dimension(203, 203));

        expressionsTA.setText("result = expr");
        expressionsTA.setMaximumSize(new java.awt.Dimension(1000, 460));
        expressionsTA.setMinimumSize(new java.awt.Dimension(300, 300));
        expressionsTA.setPreferredSize(new java.awt.Dimension(600, 400));
        jScrollPane3.setViewportView(expressionsTA);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        exprPanel.add(jScrollPane3, gridBagConstraints);

        ignoreUnitsCB.setSelected(true);
        ignoreUnitsCB.setText("ignore units");
        ignoreUnitsCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ignoreUnitsCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 14);
        exprPanel.add(ignoreUnitsCB, gridBagConstraints);

        retainCB.setText("retain input components");
        retainCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                retainCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        exprPanel.add(retainCB, gridBagConstraints);

        jLabel3.setText("Precision:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        exprPanel.add(jLabel3, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridLayout(1, 0));

        precisionRBG.add(singlePrecisionRB);
        singlePrecisionRB.setSelected(true);
        singlePrecisionRB.setText("single");
        singlePrecisionRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                singlePrecisionRBActionPerformed(evt);
            }
        });
        jPanel1.add(singlePrecisionRB);

        precisionRBG.add(doublePrecisionRB);
        doublePrecisionRB.setText("double");
        doublePrecisionRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                doublePrecisionRBActionPerformed(evt);
            }
        });
        jPanel1.add(doublePrecisionRB);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        exprPanel.add(jPanel1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(8, 0, 0, 0);
        calculatorPanel.add(exprPanel, gridBagConstraints);

        errorMessage.setForeground(new java.awt.Color(204, 0, 51));
        errorMessage.setInheritsPopupMenu(false);
        errorMessage.setMaximumSize(new java.awt.Dimension(300, 70));
        errorMessage.setMinimumSize(new java.awt.Dimension(200, 70));
        errorMessage.setPreferredSize(new java.awt.Dimension(200, 70));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        calculatorPanel.add(errorMessage, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        saveButton.setText("save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        jPanel2.add(saveButton, new java.awt.GridBagConstraints());

        loadButton.setText("load");
        loadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 16, 0, 16);
        jPanel2.add(loadButton, gridBagConstraints);

        runButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                runButtonUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 0, 0);
        jPanel2.add(runButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        calculatorPanel.add(jPanel2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(calculatorPanel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        add(jPanel3, gridBagConstraints);

        detachButton.setText("detach calculator to new window");
        detachButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                detachButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(detachButton, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents


    private void singlePrecisionRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_singlePrecisionRBActionPerformed
        parameterProxy.set(PRECISION, Precision.SINGLE);
    }//GEN-LAST:event_singlePrecisionRBActionPerformed

    private void doublePrecisionRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doublePrecisionRBActionPerformed
        parameterProxy.set(PRECISION, Precision.DOUBLE);
    }//GEN-LAST:event_doublePrecisionRBActionPerformed

    private void retainCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_retainCBActionPerformed
        parameterProxy.set(RETAIN, retainCB.isSelected());
    }//GEN-LAST:event_retainCBActionPerformed

    private void runButtonUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_runButtonUserChangeAction
    {//GEN-HEADEREND:event_runButtonUserChangeAction

        String[] aliases = new String[componentTable.getRowCount()];
        for (int i = 0; i < aliases.length; i++)
            aliases[i] = (String)componentTable.getValueAt(i, 2);
        setErrorText("");
        parameterProxy.set(INPUT_ALIASES, aliases);
        parameterProxy.set(RUNNING_MESSAGE, (RunButton.RunState) evt.getEventData());
    }//GEN-LAST:event_runButtonUserChangeAction

    private void generatorDimRBUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_generatorDimRBUserChangeAction
    {//GEN-HEADEREND:event_generatorDimRBUserChangeAction
        int dimNum = dim1RB.isSelected() ? 1 : dim2RB.isSelected() ? 2 : 3;
        int[] dims = Arrays.copyOf(generatorFullDims, dimNum);
        float[][] extents = new float[][]{Arrays.copyOf(generatorFullExtents[0], dimNum), Arrays.copyOf(generatorFullExtents[1], dimNum)};

        updateGeneratorTable(dims, extents);
        updatePseudoVarsTable(dims.length);
        parameterProxy.set(GENERATOR_DIMENSION_LENGTHS, dims,
                       GENERATOR_EXTENTS, extents);
//        StringBuilder preambleBuilder = new StringBuilder();
//        preambleBuilder.append("// regular field ");
//        for (int i = 0; i < dims.length; i++)
//            preambleBuilder.append(" " + dims[i]);
//        preambleBuilder.append(" //\n");
//        preamble = preambleBuilder.toString();
//        updatePreamble(preamble);
    }//GEN-LAST:event_generatorDimRBUserChangeAction

    private void ignoreUnitsCBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_ignoreUnitsCBActionPerformed
    {//GEN-HEADEREND:event_ignoreUnitsCBActionPerformed
        parameterProxy.set(IGNORE_UNITS, ignoreUnitsCB.isSelected());
    }//GEN-LAST:event_ignoreUnitsCBActionPerformed

    private void componentTableMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_componentTableMouseClicked
    {//GEN-HEADEREND:event_componentTableMouseClicked
        int row = componentTable.rowAtPoint(evt.getPoint());
        if (componentTable.getModel().getRowCount() < 1 || row < 0 ||
                row > componentTable.getModel().getRowCount())
            return;
        int col = componentTable.columnAtPoint(evt.getPoint());
        if (col < 2) {
            int c = expressionsTA.getCaretPosition();
            String name = (String) componentTable.getModel().getValueAt(row, 2);
            expressionsTA.insert(name, c);
            expressionsTA.setCaretPosition(c + name.length());
        }
    }//GEN-LAST:event_componentTableMouseClicked

    private void constantsTableMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_constantsTableMouseClicked
    {//GEN-HEADEREND:event_constantsTableMouseClicked
        int c = expressionsTA.getCaretPosition();
        String name = constantSymbols[constantsTable.getSelectedRow() + 1];
        expressionsTA.insert(name, c);
        expressionsTA.setCaretPosition(c + name.length());
    }//GEN-LAST:event_constantsTableMouseClicked

    private void pseudoVarsTableMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_pseudoVarsTableMouseClicked
    {//GEN-HEADEREND:event_pseudoVarsTableMouseClicked
        int col = pseudoVarsTable.columnAtPoint(evt.getPoint());
        int row = pseudoVarsTable.rowAtPoint(evt.getPoint());
        int c = expressionsTA.getCaretPosition();
        String name =  (String)pseudoVarsTable.getModel().getValueAt(row, col);
        expressionsTA.insert(name, c);
        expressionsTA.setCaretPosition(c + name.length());
    }//GEN-LAST:event_pseudoVarsTableMouseClicked

    private void varHeaderUserAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_varHeaderUserAction
        varPanel.setVisible(varHeader.isExpanded());
    }//GEN-LAST:event_varHeaderUserAction

    private void constHeaderUserAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_constHeaderUserAction
        constPanel.setVisible(constHeader.isExpanded());
    }//GEN-LAST:event_constHeaderUserAction

    private void constHeaderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_constHeaderUserChangeAction
        constPanel.setVisible(constHeader.isExpanded());
    }//GEN-LAST:event_constHeaderUserChangeAction

    private void varHeaderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_varHeaderUserChangeAction
        varPanel.setVisible(varHeader.isExpanded());
    }//GEN-LAST:event_varHeaderUserChangeAction

    private void operatorHeaderUserAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_operatorHeaderUserAction
        operatorsPanel.setVisible(operatorHeader.isExpanded());
    }//GEN-LAST:event_operatorHeaderUserAction

    private void operatorHeaderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_operatorHeaderUserChangeAction
        operatorsPanel.setVisible(operatorHeader.isExpanded());
    }//GEN-LAST:event_operatorHeaderUserChangeAction

    private void moveCalcPanelToPanel()
    {
        CalculatorFrame.remove(calculatorPanel);
        CalculatorFrame.setVisible(false);
        java.awt.GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(calculatorPanel, gridBagConstraints);
        revalidate();
    }

    private void detachButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_detachButtonActionPerformed
        if (detachButton.isSelected()) {
            remove(calculatorPanel);
            CalculatorFrame.add(calculatorPanel, BorderLayout.CENTER);
            CalculatorFrame.setVisible(true);
            revalidate();
        }
        else
            moveCalcPanelToPanel();
    }//GEN-LAST:event_detachButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed

        saveFileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(this.getClass())));

        if (saveFileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            String fileName = saveFileChooser.getSelectedFile().getAbsolutePath();
            if (!fileName.endsWith(".vnc") && !fileName.endsWith(".VNC"))
                fileName = fileName + ".vnc";
            VisNow.get().getMainConfig().setLastDataPath(fileName.substring(0, fileName.lastIndexOf(File.separator)), this.getClass());
            try (BufferedWriter br = new BufferedWriter(new FileWriter(fileName))) {
                br.write(expressionsTA.getText());
            }
            catch(IOException e) {}
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void loadButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadButtonActionPerformed

        loadFileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(this.getClass())));

        if (loadFileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            String fileName = loadFileChooser.getSelectedFile().getAbsolutePath();
            if (!fileName.endsWith(".vnc") && !fileName.endsWith(".VNC"))
                fileName = fileName + ".vnc";
            VisNow.get().getMainConfig().setLastDataPath(fileName.substring(0, fileName.lastIndexOf(File.separator)), this.getClass());
            expressionsTA.setText("");
            try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
                String line;
                while ((line = br.readLine()) != null)
                    expressionsTA.append(line + "\n");
            }
            catch(IOException e) {}
        }
    }//GEN-LAST:event_loadButtonActionPerformed

    private void CalculatorFrameWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_CalculatorFrameWindowClosing
        detachButton.setSelected(false);
        moveCalcPanelToPanel();// TODO add your handling code here:
    }//GEN-LAST:event_CalculatorFrameWindowClosing

    public void setErrorText(String txt)
    {
        errorMessage.setText("<html>" + txt + "</html>");
        errorMessage.setVisible(txt != null && !txt.isEmpty());
        revalidate();
    }

    public void closeFrame()
    {
        CalculatorFrame.dispose();
    }

//    public void updatePreamble(String preamble)
//    {
//        String program = expressionsTA.getText();
//        if (program.startsWith("// regular") || program.startsWith("// irrregular")) {
//            int n = program.indexOf(" //\n");
//            if (n > 0)
//                program = program.substring(n + 7);
//        }
//        expressionsTA.setText(preamble + program);
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFrame CalculatorFrame;
    private javax.swing.JPanel calculatorPanel;
    private javax.swing.JTable componentTable;
    private org.visnow.vn.gui.widgets.SectionHeader constHeader;
    private javax.swing.JScrollPane constPane;
    private javax.swing.JPanel constPanel;
    private javax.swing.JTable constantsTable;
    private javax.swing.JToggleButton detachButton;
    private org.visnow.vn.gui.swingwrappers.RadioButton dim1RB;
    private org.visnow.vn.gui.swingwrappers.RadioButton dim2RB;
    private org.visnow.vn.gui.swingwrappers.RadioButton dim3RB;
    private javax.swing.JPanel dimPanel;
    private org.visnow.vn.gui.swingwrappers.RadioButton doublePrecisionRB;
    private javax.swing.JLabel errorMessage;
    private javax.swing.JPanel exprPanel;
    private javax.swing.JTextArea expressionsTA;
    private javax.swing.JLabel generatorLabel;
    private javax.swing.JPanel generatorPanel;
    private javax.swing.ButtonGroup generatorRBG;
    private javax.swing.JTable generatorTable;
    private javax.swing.JPanel generatorTablePanel;
    private javax.swing.JCheckBox ignoreUnitsCB;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JButton loadButton;
    private javax.swing.JFileChooser loadFileChooser;
    private org.visnow.vn.gui.widgets.SectionHeader operatorHeader;
    private javax.swing.JPanel operatorsPanel;
    private javax.swing.ButtonGroup precisionRBG;
    private javax.swing.JTable pseudoVarsTable;
    private javax.swing.JCheckBox retainCB;
    private org.visnow.vn.gui.widgets.RunButton runButton;
    private javax.swing.JButton saveButton;
    private javax.swing.JFileChooser saveFileChooser;
    private org.visnow.vn.gui.swingwrappers.RadioButton singlePrecisionRB;
    private org.visnow.vn.gui.widgets.SectionHeader varHeader;
    private javax.swing.JPanel varPanel;
    private javax.swing.JScrollPane variablesPane;
    // End of variables declaration//GEN-END:variables

    public static void main(String[] args)
    {
        VisNow.initLogging(true);
        UIStyle.initStyle();

        SwingUtilities.invokeLater(new Runnable()
        {

            @Override
            public void run()
            {
                JFrame f = new JFrame();
                GUI gui = new GUI();
                f.add(gui);
                f.setSize(260, 900);
                f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                f.setLocationRelativeTo(null);
                f.setVisible(true);
                gui.setErrorText("<html>line 1: some error<p>test error</html>");

            }
        });
    }
}
