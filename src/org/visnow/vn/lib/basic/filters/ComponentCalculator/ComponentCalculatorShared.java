/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.ComponentCalculator;

import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class ComponentCalculatorShared
{
    static enum Precision {
        SINGLE, DOUBLE
    }
    
//Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    //not-null, each element not-null (may be empty)
    static final ParameterName<String[]> EXPRESSION_LINES = new ParameterName("Expression lines");
    static final ParameterName<Precision> PRECISION = new ParameterName("Precision");

    static final ParameterName<Boolean> META_IS_GENERATOR = new ParameterName("META is generator");


    static final ParameterName<Boolean> IGNORE_UNITS = new ParameterName("Ignore units");

    static final ParameterName<Boolean> RETAIN = new ParameterName("Retain");
    
    static final ParameterName<Integer> META_FIELD_DIMENSION = new ParameterName("META dimensions");            // regular input field dimensions length = 1, 2 or 3
    static final ParameterName<String[]> META_COMPONENT_NAMES = new ParameterName("META component names");      // component names not-null, length >= 0, (0 means that module works in "Generator" mode)
    static final ParameterName<int[]> META_COMPONENT_VECLEN = new ParameterName("META component vecLenS");      // component vector lengths not-null, length = META_COMPONENT_NAMES.length, each element >=1
    static final ParameterName<String[]> INPUT_ALIASES = new ParameterName("Input field component aliases");    // component name aliases not-null, length = META_COMPONENT_NAMES.length each element not empty
    

    //**** generator parameters
    
    static final ParameterName<int[]> GENERATOR_DIMENSION_LENGTHS = new ParameterName("Generator dimensions");  //generated regular field dimensions length =  1, 2 or 3; each element >= 2
    static final ParameterName<float[][]> GENERATOR_EXTENTS = new ParameterName("Generator extents");           //generated regular field geometric extents 
                                                                                                                //length = 2, each element length = GENERATOR_DIMENSION_LENGTHS.length,  with constraint [0][i] < [1][i]

    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");
}
