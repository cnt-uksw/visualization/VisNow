/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.GeometricCrop;

import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType;
import org.visnow.vn.gui.widgets.RunButton;
import org.visnow.vn.lib.utils.field.subset.GeometricSubsetParams.Depth;
import org.visnow.vn.lib.utils.field.subset.GeometricSubsetParams.Position;
/**
 *
 * @author know
 */

public class GeometricCropShared {
    
    public static final String GEOMETRIC_STRING = "geometric crop";
    public static final String TYPE_STRING = "glyph type";
    public static final String POSITION_STRING = "position";
    public static final String DEPTH_STRING = "depth";

    static final ParameterName<Boolean> GEOMETRIC = new ParameterName(GEOMETRIC_STRING);
    static final ParameterName<GlyphType> TYPE = new ParameterName(TYPE_STRING);
    static final ParameterName<Position> POSITION = new ParameterName(POSITION_STRING);
    static final ParameterName<Depth> DEPTH = new ParameterName(DEPTH_STRING);
    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");
}

