/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.ComponentOperations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;
import org.visnow.jscic.Field;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.utils.LargeArrayMath;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.filters.ComponentOperations.ComponentOperationsShared.*;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class VectorOperationsCore
{

    private Parameters params = null;
    private Field inField = null;
    protected Field outField = null;
    
    private ArrayList<Float> combineTimesteps(TimeData[] inTimeData)
    {
        ArrayList<Float> tmpTimeSeries = new ArrayList<>();
        for (TimeData timeData : inTimeData) {
            float[] t = timeData.getTimesAsArray();
            for (float timeStep : t) 
                tmpTimeSeries.add(timeStep);
        }
        if (tmpTimeSeries.isEmpty())
            return tmpTimeSeries;
        Collections.sort(tmpTimeSeries);
        ArrayList<Float> timeSeries = new ArrayList<>();
        timeSeries.add(tmpTimeSeries.get(0)); 
        for (int i = 1, j = 0; i < tmpTimeSeries.size(); i++) 
            if (tmpTimeSeries.get(i) != timeSeries.get(j)) {
                timeSeries.add(tmpTimeSeries.get(i));
                j += 1;
            }
        return timeSeries;
    }
    
    private TimeData combineVectorData(TimeData[] inTimeData)
    {
        if (inTimeData == null || inTimeData.length == 0)
            return null;
        long n = inTimeData[0].length();
        ArrayList<Float> timeSeries = combineTimesteps(inTimeData);
        if (n == 0 || timeSeries.isEmpty())
            return null;
        int vLen = inTimeData.length;
        if (params.get(FIX3D))
            vLen = Math.max(vLen, 3);
        DataArrayType type = inTimeData[0].getType();
        for (int i = 1; i < inTimeData.length; i++) 
            if (inTimeData[i].getType() != type) {
                type = DataArrayType.FIELD_DATA_FLOAT;
                break;
            }
        TimeData outTimeData = new TimeData(type);
        for (int i = 0; i < timeSeries.size(); i++) {
            float t = timeSeries.get(i);
            LargeArray out = LargeArrayUtils.create(type.toLargeArrayType(), vLen * n, true);
            for (int iData = 0; iData < inTimeData.length; iData++) {
                LargeArray inTData = inTimeData[iData].getValue(t);
                for (long j = 0; j < n; j++) 
                    out.setFloat(vLen * j + iData, inTData.getFloat(j));
            }
            outTimeData.setValue(out, t);
        }
        return outTimeData;
    }

    public void setData(Field inField, Field outField, Parameters p)
    {
        this.inField = inField;
        this.outField = outField;
        this.params = p;
    }

    void update()
    {
        if (inField == null || outField == null)
            return;
        int n = (int) outField.getNNodes();
        Vector<VectorComponent> components = params.get(VECTOR_COMPONENTS);
        if (components != null) {
            for (VectorComponent component : components) {
                int vlen = 0;
                for (int i = 0; i < component.getScalarComponentNames().length; i++) 
                    if (component.getScalarComponentNames()[i] != null && 
                        inField.getComponent(component.getScalarComponentNames()[i]) != null)
                        vlen += 1;
                TimeData[] inTimeData = new TimeData[vlen];
                String[] inUnits = new String[vlen];
                for (int i = 0; i < component.getScalarComponentNames().length; i++) 
                    if (component.getScalarComponentNames()[i] != null && 
                        inField.getComponent(component.getScalarComponentNames()[i]) != null) {
                        inTimeData[i] = inField.getComponent(component.getScalarComponentNames()[i]).getTimeData(); 
                        inUnits[i] = inField.getComponent(component.getScalarComponentNames()[i]).getUnit();
                    }
                TimeData data = combineVectorData(inTimeData);
                String outUnit = "1";
                boolean inUnitsEqual = true;
                for (int i = 1; i < inUnits.length; i++) {
                    if(!inUnits[i].equals(inUnits[0]))
                        inUnitsEqual = false;
                }
                if(inUnitsEqual)
                    outUnit = inUnits[0];
                outField.addComponent(DataArray.create(data, vlen, component.getName(), outUnit, null));
                if (component.isComputeNorm()) {
                    TimeData norms = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
                    if (params.get(FIX3D))
                        vlen = Math.max(vlen, 3);
                    for (int i = 0; i < data.getNSteps(); i++)
                        norms.setValue(LargeArrayMath.vectorNorms(data.getValues().get(i), vlen), data.getTime(i));
                    outField.addComponent(DataArray.create(norms, 1, component.getName() + "_norm", inUnitsEqual?outUnit:"1", null));
                }
            }
        }
        boolean[] vCN = params.get(VCNORMS);
        if (vCN != null)
            for (int i = 0, l = 0; i < inField.getNComponents(); i++)
                if (inField.getComponent(i).isNumeric() && inField.getComponent(i).getVectorLength() > 1) {
                    if (vCN[l]) {
                        TimeData data = inField.getComponent(i).getTimeData();
                        int vLen = inField.getComponent(i).getVectorLength();
                        TimeData norms = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
                        for (int j = 0; j < data.getNSteps(); j++)
                            norms.setValue(LargeArrayMath.vectorNorms(data.getValues().get(j), vLen), data.getTime(i));
                        outField.addComponent(DataArray.create(norms, 1, inField.getComponent(i).getName() + "_norm", inField.getComponent(i).getUnit(), null));
                    }
                    l += 1;
                }
        boolean[] vCNormalize = params.get(VCNORMALIZE);
        if (vCNormalize != null)
            for (int i = 0, l = 0; i < inField.getNComponents(); i++)
                if (inField.getComponent(i).isNumeric() && inField.getComponent(i).getVectorLength() > 1) {
                    if (vCNormalize[l]) {
                        TimeData data = inField.getComponent(i).getTimeData();
                        int vLen = inField.getComponent(i).getVectorLength();
                        TimeData normalizeds = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
                        int vlen = inField.getComponent(i).getVectorLength();
                        for (int j = 0; j < data.getNSteps(); j++) {
                            LargeArray raw = data.getValues().get(j);
                            FloatLargeArray norm = LargeArrayMath.vectorNorms(raw, vLen);
                            FloatLargeArray normalized = new FloatLargeArray(raw.length());
                            for (long k = 0, m = 0; k < norm.length(); k++)
                                for (int c = 0; c < vLen; c++, m++)
                                    normalized.setFloat(m, raw.getFloat(m) / norm.getFloat(k));
                            normalizeds.setValue(normalized, data.getStep(j));
                        }
                        outField.addComponent(DataArray.create(normalizeds, vlen, inField.getComponent(i).getName() + "_normalized"));
                    }
                    l += 1;
                }
        boolean[] vCS = params.get(VCSPLIT);
        if (vCS != null)
            for (int i = 0, iv = 0; i < inField.getNComponents(); i++)
                if (inField.getComponent(i).isNumeric() && inField.getComponent(i).getVectorLength() > 1) {
                    if (vCS[iv]) {
                        float[] data = inField.getComponent(i).getRawFloatArray().getData();
                        int vlen = inField.getComponent(i).getVectorLength();
                        for (int j = 0; j < vlen; j++) {
                            float[] cmp = new float[(int) inField.getNNodes()];
                            for (int k = 0, l = j; k < cmp.length; k++, l += vlen)
                                cmp[k] = data[l];
                            outField.addComponent(DataArray.create(cmp, 1, inField.getComponent(i).getName() + "_" + j, inField.getComponent(i).getUnit(), null));
                        }
                    }
                    iv += 1;
                }
    }

    Field getOutField()
    {
        return outField;
    }

}
