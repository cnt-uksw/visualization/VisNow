/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.ComponentOperations;

import java.util.Arrays;
import java.util.Vector;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.PointField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;

import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.filters.ComponentOperations.ComponentOperationsShared.*;

import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNPointField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ComponentOperations extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected Field inField = null;
    protected Field lastInField = null;
    protected GUI computeUI = null;
    protected boolean fromUI = false;
    protected ComponentSelectorCore coreComponent = new ComponentSelectorCore();
    protected CoordsFromDataCore coreCoords = new CoordsFromDataCore();
    protected VectorOperationsCore coreVectors = new VectorOperationsCore();
    protected MaskCore coreMask = new MaskCore();
    protected ComplexCore coreComplex = new ComplexCore();
    protected boolean dataUnchanged = true;
    private int runQueue = 0;

    public ComponentOperations()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name != null && name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {

        return new Parameter[]{
            new Parameter<>(ACTIONS, new int[]{0}),
            new Parameter<>(RETAIN, new boolean[0]),
            new Parameter<>(USE_COORDS, false),
            new Parameter<>(NDIMS, 3),
            new Parameter<>(XCOORD_COMPONENT, 0),
            new Parameter<>(YCOORD_COMPONENT, 0),
            new Parameter<>(ZCOORD_COMPONENT, 0),
            new Parameter<>(ADD_INDEX_COMPONENT, false),
            new Parameter<>(XCOORD_SCALE_VALUE, 1f),
            new Parameter<>(YCOORD_SCALE_VALUE, 1f),
            new Parameter<>(ZCOORD_SCALE_VALUE, 1f),
            new Parameter<>(XVAR_SHIFT, 0f),
            new Parameter<>(YVAR_SHIFT, 0f),
            new Parameter<>(ZVAR_SHIFT, 0f),
            new Parameter<>(XCOORD_SHIFT, 0f),
            new Parameter<>(YCOORD_SHIFT, 0f),
            new Parameter<>(ZCOORD_SHIFT, 0f),
            new Parameter<>(MIN, new double[0]),
            new Parameter<>(MAX, new double[0]),
            new Parameter<>(VECTOR_COMPONENTS, new Vector()),
            new Parameter<>(VCNORMS, new boolean[0]),
            new Parameter<>(VCNORMALIZE, new boolean[0]),
            new Parameter<>(VCSPLIT, new boolean[0]),
            new Parameter<>(FIX3D, false),
            new Parameter<>(MASK_COMPONENT, -1),
            new Parameter<>(RECOMPUTE_MIN_MAX, false),
            new Parameter<>(MASK_MIN_MAX, new float[] {0,1}),
            new Parameter<>(MASK_LOW_UP, new float[] {0,1}),
            new Parameter<>(ADD_TO_MASK, true),
            new Parameter<>(COMPLEX_COMBINE_COMPONENTS, new Vector()),
            new Parameter<>(COMPLEX_SPLIT_RE, new boolean[0]),
            new Parameter<>(COMPLEX_SPLIT_IM, new boolean[0]),
            new Parameter<>(COMPLEX_SPLIT_ABS, new boolean[0]),
            new Parameter<>(COMPLEX_SPLIT_ARG, new boolean[0]),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN),
            new Parameter<>(META_SCHEMA, null),
            new Parameter<>(META_EXTENDS, new float[0][0]),
            new Parameter<>(META_IS_REGULAR_FIELD, false),
            new Parameter<>(META_COMPONENT_NAMES, new String[]{}),
            new Parameter<>(META_COMPONENT_VECLEN, new int[]{}),
            new Parameter<>(META_COMPONENT_TYPES, new DataArrayType[]{})
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE) parameters.set(RUNNING_MESSAGE, NO_RUN);

        String[] newComponentNames = inField.getComponentNames();
        int nComponents = newComponentNames.length;
        int nVectorComps = 0;
        if (resetParameters && newComponentNames.length > 0) {
            parameters.set(META_COMPONENT_NAMES, newComponentNames);
            int[] veclens = new int[nComponents];
            double[] min = new double[nComponents];
            double[] max = new double[nComponents];
            DataArrayType[] types = new DataArrayType[nComponents];
            int k = 0;

            for (int i = 0; i < inField.getNComponents(); i++) {
                DataArray da = inField.getComponent(i);
                if (inField.getComponent(i).isNumeric()) {
                    veclens[k] = da.getVectorLength();
                    if(veclens[k] > 1) {
                        nVectorComps++;
                    }
                    min[k] = da.getPreferredPhysMinValue();
                    max[k] = da.getPreferredPhysMaxValue();
                    types[k++] = da.getType();
                }
                else {
                    types[k++] = DataArrayType.FIELD_DATA_UNKNOWN;
                }
            }
            parameters.set(META_COMPONENT_VECLEN, veclens);
            parameters.set(META_COMPONENT_TYPES, types);
            parameters.set(MIN, min);
            parameters.set(MAX, max);
            parameters.set(META_EXTENDS, inField.getPreferredExtents());
            //inField.createStatistics();
            parameters.set(META_SCHEMA, inField.getSchema());
            parameters.set(META_IS_REGULAR_FIELD, inField instanceof RegularField);
            int[] actions = new int[newComponentNames.length];
            boolean[] retain = new boolean[newComponentNames.length];
            for (int i = 0; i < actions.length; i++) {
                actions[i] = ComponentOperationsShared.NOOP;
                retain[i] = true;
            }
            parameters.set(ACTIONS, actions);
            parameters.set(RETAIN, retain);
            if(nVectorComps > 0) {
                boolean[] vCN = new boolean[nVectorComps];
                boolean[] vCNormalize = new boolean[nVectorComps];
                boolean[] vCS = new boolean[nVectorComps];
                parameters.set(VCNORMS, vCN);
                parameters.set(VCNORMALIZE, vCNormalize);
                parameters.set(VCSPLIT, vCS);
            }
        }
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive()
    {

        if (getInputFirstValue("inField") != null) {
            Field newField = ((VNField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA() && (inField == null || !Arrays.equals(inField.getComponentNames(), newField.getComponentNames()));
            boolean isNewField = !isFromVNA() && newField != inField;
            inField = newField;

            Parameters p;

            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentField);
                p = parameters.getReadOnlyClone();
            }

            notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data

                if (inField instanceof RegularField) {
                    outField = ((RegularField) inField).cloneShallow();
                    outField.removeComponents();
                    outRegularField   = (RegularField) outField;
                    outPointField     = null;
                    outIrregularField = null;
                } else if (inField instanceof PointField) {
                    outField = ((PointField) inField).cloneShallow();
                    outField.removeComponents();
                    outRegularField   = null;
                    outPointField     = (PointField) outField;
                    outIrregularField = null;
                } else {
                    outField = ((IrregularField) inField).cloneShallow();
                    outField.removeComponents();
                    outRegularField = null;
                    outPointField     = null;
                    outIrregularField = (IrregularField) outField;
                }
                if (p.get(USE_COORDS)) {
                    coreCoords.setData(inField, p, outField);
                    coreCoords.update();
                }
                coreComponent.setData(inField, outField, p);
                coreComponent.update();
                coreVectors.setData(inField, coreComponent.getOutField(), p);
                coreVectors.update();
                coreMask.setData(inField, coreComponent.getOutField(), p);
                coreMask.update();
                coreComplex.setData(inField, coreMask.getOutField(), p);
                coreComplex.update();

                if (inField instanceof RegularField) {
                    setOutputValue("pointOutField", null);
                    setOutputValue("irregularOutField", null);
                    if(outRegularField.getNComponents() > 0) {
                        setOutputValue("regularOutField", new VNRegularField(outRegularField));
                    }
                    else {
                        outRegularField = null;
                        outField = null;
                        setOutputValue("regularOutField", null);
                    }
                } else if (inField instanceof PointField) {
                    setOutputValue("regularOutField", null);
                    setOutputValue("irregularOutField", null);
                    if(outPointField.getNComponents() > 0) {
                        setOutputValue("pointOutField", new VNPointField(outPointField));
                    }
                    else {
                        outPointField = null;
                        outField = null;
                        setOutputValue("pointOutField", null);
                    }
                } else if (inField instanceof IrregularField) {
                    setOutputValue("regularOutField", null);
                    setOutputValue("pointOutField", null);
                    if(outIrregularField.getNComponents() == 0) {
                        boolean componentExists = false;
                        for (CellSet cellSet : ((IrregularField) outIrregularField).getCellSets())
                            if (cellSet.getNComponents() != 0) componentExists = true;
                        if (!componentExists) {
                            outIrregularField = null;
                            outField = null;
                            setOutputValue("irregularOutField", null);
                        }
                    }
                    else {
                         setOutputValue("irregularOutField", new VNIrregularField((outIrregularField)));
                    }
                }

                if (getOutputs().getOutput("outObj").isLinked()) {
                    prepareOutputGeometry();
                    show();
                }
            }
        }
    }
}
