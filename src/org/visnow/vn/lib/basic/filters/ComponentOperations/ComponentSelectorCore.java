/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.ComponentOperations;

import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.ShortLargeArray;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.filters.ComponentOperations.ComponentOperationsShared.*;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class ComponentSelectorCore
{

    private Parameters params = null;
    private Field inField = null;
    protected RegularField outRegularField = null;
    protected Field outField = null;

    public ComponentSelectorCore()
    {
    }

    public void setData(Field inField, Field outField, Parameters p)
    {
        this.inField = inField;
        this.outField = outField;
        this.params = p;
    }

    void update()
    {
        if (inField == null || params == null) {
            outField = null;
            return;
        }
        int[] actions = params.get(ACTIONS);
        double[] clampPhysMin = params.get(MIN); //physical min
        double[] clampPhysMax = params.get(MAX); //physical max

        //TODO: looks like "add index component" functionality is implemented in CoordsFromDataCore 
        //but maybe it should be additionally processed somehow in here? - Double check it with know
        for (int iData = 0; iData < inField.getNumericComponentNames().length; iData++) {
            if (params.get(RETAIN)[iData]) {
                outField.addComponent(inField.getComponent(inField.getNumericComponentNames()[iData]).cloneShallow());
            }
        }

        for (int iData = 0; iData < actions.length; iData++) {
            DataArray da = inField.getComponent(iData);
            //this is not tested against null time data - assuming that time data cannot be null! (double check with babor on DataArray concept)
            //TODO: resolve problem with time data and complex data array
            if (!da.isNumeric())// || da.getTimeData() == null)
                continue;
            if (actions[iData] == ComponentOperationsShared.NOOP &&
                clampPhysMin[iData] <= da.getPreferredPhysMinValue() && clampPhysMax[iData] >= da.getPreferredPhysMaxValue()) {
                //only change the preffered min/max if not byte component
                if(outField.getComponent(da.getName()) != null && outField.getComponent(da.getName()).getType() != DataArrayType.FIELD_DATA_BYTE) {
                    DataArray outDa = outField.getComponent(da.getName());
                    outDa.setPreferredRanges(da.getSchema().dataPhysToRaw(clampPhysMin[iData]), da.getSchema().dataPhysToRaw(clampPhysMax[iData]), clampPhysMin[iData], clampPhysMax[iData]);
                }
                continue;
            }
            //data type changed, update the preffered min/max 
            if (clampPhysMin[iData] <= da.getPreferredPhysMinValue() && clampPhysMax[iData] >= da.getPreferredPhysMaxValue()) {
                DataArray outDa = null;
                for (int timeStep = 0; timeStep < da.getNFrames(); timeStep++) {
                    float time = da.getTime(timeStep);
                    da.setCurrentTime(time);
                    double minv = da.getSchema().dataPhysToRaw(clampPhysMin[iData]);
                    double maxv = da.getSchema().dataPhysToRaw(clampPhysMax[iData]);
                    double d = 255 / (maxv - minv);
                    if (actions[iData] == ComponentOperationsShared.BYTE) {
                        byte[] outb = da.getRawByteArray().getData();
                        if (timeStep == 0) {
                            outDa = DataArray.create(outb, da.getVectorLength(), da.getName() + "_B", da.getUnit(), da.getUserData());
                            outDa.setPreferredRanges(0, 255, da.getSchema().dataRawToPhys(0), da.getSchema().dataRawToPhys(255));
                            outField.addComponent(outDa);
                        } else 
                            outDa.addRawArray(new UnsignedByteLargeArray(outb), time);
                    } else if (actions[iData] == ComponentOperationsShared.BYTE_NORMALIZED) {
                        byte[] outbd = null;
                        switch (da.getType()) {
                            case FIELD_DATA_LOGIC:
                                byte[] bd2 = da.getRawByteArray().getData();
                                outbd = new byte[bd2.length];
                                for (int j = 0; j < outbd.length; j++) {
                                    outbd[j] = bd2[j] == 0 ? Byte.MIN_VALUE : Byte.MAX_VALUE;
                                }
                                break;
                            case FIELD_DATA_BYTE:
                            case FIELD_DATA_SHORT:
                            case FIELD_DATA_INT:
                            case FIELD_DATA_FLOAT:
                            case FIELD_DATA_DOUBLE:
                                LargeArray dd = da.getRawArray();
                                outbd = new byte[(int)dd.length()];
                                for (int j = 0; j < outbd.length; j++) {
                                    outbd[j] = (byte) (0xff & (int) ((dd.getDouble(j) - minv) * d));
                                }
                                break;
                            case FIELD_DATA_COMPLEX:
                                double[] cd = da.getRawDoubleArray().getData();
                                outbd = new byte[cd.length];
                                for (int j = 0; j < outbd.length; j++) {
                                    outbd[j] = (byte) (0xff & (int) ((cd[j] - minv) * d));
                                }
                                break;
                            default:
                                throw new IllegalArgumentException("Unsuported array type.");
                            //TODOL: 0...255 should be used or UnsignedByte type should be created
                        }
                        if (timeStep == 0) {
                            outDa = DataArray.create(outbd, da.getVectorLength(), da.getName() + "_B", da.getUnit(), da.getUserData()).
                                    preferredRanges(0, 255, minv, maxv);
                            outField.addComponent(outDa);
                        } else {
                            outDa.addRawArray(new UnsignedByteLargeArray(outbd), time);
                        }
                    } else if (actions[iData] == ComponentOperationsShared.SHORT) {
                        short[] outs = da.getRawShortArray().getData();
                        if (timeStep == 0) {
                            outDa = DataArray.create(outs, da.getVectorLength(), da.getName() + "_S", da.getUnit(), da.getUserData());
                            outDa.setPreferredRanges(minv, maxv, clampPhysMin[iData], clampPhysMax[iData]);
                            outField.addComponent(outDa);
                        } else {
                            outDa.addRawArray(new ShortLargeArray(outs), time);
                        }
                    } else if (actions[iData] == ComponentOperationsShared.SHORT_NORMALIZED) {
                        d = ((double) Short.MAX_VALUE - Short.MIN_VALUE) / (maxv - minv);
                        short[] outsd = null;
                        switch (da.getType()) {
                            case FIELD_DATA_LOGIC:
                                byte[] bd2 = da.getRawByteArray().getData();
                                outsd = new short[bd2.length];
                                for (int j = 0; j < outsd.length; j++) {
                                    outsd[j] = bd2[j] == 0 ? Short.MIN_VALUE : Short.MAX_VALUE;
                                }
                                break;
                            case FIELD_DATA_BYTE:
                                byte[] bd = (byte[])da.getRawArray().getData();
                                outsd = new short[bd.length];
                                for (int j = 0; j < outsd.length; j++) {
                                    outsd[j] = (short) (((int) bd[j] - minv) * d);
                                }
                                break;
                            case FIELD_DATA_SHORT:
                            case FIELD_DATA_INT:
                            case FIELD_DATA_FLOAT:
                            case FIELD_DATA_DOUBLE:
                                LargeArray dd = da.getRawArray();
                                outsd = new short[(int)dd.length()];
                                for (int j = 0; j < outsd.length; j++) {
                                    outsd[j] = (short) ((dd.getDouble(j) - minv) * d + Short.MIN_VALUE);
                                }
                                break;
                            case FIELD_DATA_COMPLEX:
                                double[] cd = da.getRawDoubleArray().getData();
                                outsd = new short[cd.length];
                                for (int j = 0; j < outsd.length; j++) {
                                    outsd[j] = (short) ((cd[j] - minv) * d + Short.MIN_VALUE);
                                }
                                break;
                        }
                        if (timeStep == 0) {
                            outDa = DataArray.create(outsd, da.getVectorLength(), da.getName() + "_S", da.getUnit(), da.getUserData());
                            outDa.setPreferredRanges(minv, maxv, clampPhysMin[iData], clampPhysMax[iData]);
                            outField.addComponent(outDa);
                        } else {
                            outDa.addRawArray(new ShortLargeArray(outsd), time);
                        }
                    } else if (actions[iData] == ComponentOperationsShared.INT) {
                        int[] outi = da.getRawIntArray().getData();
                        if (timeStep == 0) {
                            outDa = DataArray.create(outi, da.getVectorLength(), da.getName() + "_I", da.getUnit(), da.getUserData());
                            outDa.setPreferredRanges(minv, maxv, clampPhysMin[iData], clampPhysMax[iData]);
                            outField.addComponent(outDa);
                        } else {
                            outDa.addRawArray(new IntLargeArray(outi), time);
                        }
                    } else if (actions[iData] == ComponentOperationsShared.FLOAT) {
                        float[] outf = da.getRawFloatArray().getData();
                        if (timeStep == 0) {
                            outDa = DataArray.create(outf, da.getVectorLength(), da.getName() + "_F", da.getUnit(), da.getUserData());
                            outDa.setPreferredRanges(minv, maxv, clampPhysMin[iData], clampPhysMax[iData]);
                            outField.addComponent(outDa);
                        } else {
                            outDa.addRawArray(new FloatLargeArray(outf), time);
                        }
                    } else if (actions[iData] == ComponentOperationsShared.DOUBLE) {
                        double[] outdo = da.getRawDoubleArray().getData();
                        if (timeStep == 0) {
                            outDa = DataArray.create(outdo, da.getVectorLength(), da.getName() + "_D", da.getUnit(), da.getUserData());
                            outDa.setPreferredRanges(minv, maxv, clampPhysMin[iData], clampPhysMax[iData]);
                            outField.addComponent(outDa);
                        } else {
                            outDa.addRawArray(new DoubleLargeArray(outdo), time);
                        }
                    } else if (actions[iData] == ComponentOperationsShared.LOG) {
                        if (da.getVectorLength() == 1) {
                            float[] outd = da.getRawFloatArray().getData();
                            if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                                outd = outd.clone();
                            }
                            for (int j = 0; j < outd.length; j++) {
                                outd[j] = (float) log(outd[j]);
                            }
                            outField.addComponent(DataArray.create(outd, 1, "log_" + da.getName()));
                        }
                    } else if (actions[iData] == ComponentOperationsShared.ATAN) {
                        if (da.getVectorLength() == 1) {
                            float[] outd = da.getRawFloatArray().getData();
                            double md = max(abs(da.getMinValue()), abs(da.getMaxValue())) / 10;
                            if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                                outd = outd.clone();
                            }
                            for (int j = 0; j < outd.length; j++) {
                                outd[j] = (float) atan(outd[j] / md);
                            }
                            outField.addComponent(DataArray.create(outd, 1, "atan_" + da.getName()));
                        }
                    }
                }
            }
            
            if (actions[iData] == ComponentOperationsShared.NOOP &&
                (clampPhysMin[iData] > da.getPreferredPhysMinValue() || clampPhysMax[iData] < da.getPreferredPhysMaxValue())) {
                DataArray outDa = null;

                for (int timeStep = 0; timeStep < da.getNFrames(); timeStep++) {
                    float time = da.getTime(timeStep);
                    da.setCurrentTime(time);
                    int vlen = da.getVectorLength();
                    int n = (int) da.getNElements();
                    double cm = da.getSchema().dataPhysToRaw(clampPhysMin[iData]);
                    double cx = da.getSchema().dataPhysToRaw(clampPhysMax[iData]);
                    // clamping data         
                    switch (da.getType()) {
                        case FIELD_DATA_BYTE:
                            byte[] outbd = da.getRawByteArray().getData();
                            if (da.getType().getValue() == actions[iData]) {
                                outbd = outbd.clone();
                            }
                            for (int j = 0; j < outbd.length; j++) {
                                int k = outbd[j] & 0xff;
                                k = (k > cx ? (int) cx : (k < cm ? (int) cm : k));
                                outbd[j] = (byte) (k & 0xff);
                            }
                            if (timeStep == 0) {
                                outDa = DataArray.create(outbd, da.getVectorLength(), da.getName() + "_B", da.getUnit(), da.getUserData());
                                outDa.setPreferredRanges(0, 255, da.getSchema().dataRawToPhys(0), da.getSchema().dataRawToPhys(255));
                                outField.addComponent(outDa);
                            } else {
                                outDa.addRawArray(new UnsignedByteLargeArray(outbd), time);
                            }
                            break;
                        case FIELD_DATA_SHORT:
                            short[] outsd = da.getRawShortArray().getData();
                            if (da.getType().getValue() == actions[iData]) {
                                outsd = outsd.clone();
                            }
                            for (int j = 0; j < outsd.length; j++) {
                                outsd[j] = (outsd[j] > cx ? (short) cx : (outsd[j] < cm ? (short) cm : outsd[j]));
                            }
                            if (timeStep == 0) {
                                outDa = DataArray.create(outsd, da.getVectorLength(), da.getName() + "_S", da.getUnit(), da.getUserData());
                                outDa.setPreferredRanges(cm, cx, clampPhysMin[iData], clampPhysMax[iData]);
                                outField.addComponent(outDa);
                            } else {
                                outDa.addRawArray(new ShortLargeArray(outsd), time);
                            }
                            break;
                        case FIELD_DATA_INT:
                            int[] outid = da.getRawIntArray().getData();
                            if (da.getType().getValue() == actions[iData]) {
                                outid = outid.clone();
                            }
                            if (vlen == 1) {
                                for (int j = 0; j < outid.length; j++) {
                                    outid[j] = (outid[j] > cx ? (int) cx : (outid[j] < cm ? (int) cm : outid[j]));
                                }
                            } else {
                                for (int j = 0; j < n; j++) {
                                    double t = 0;
                                    for (int k = 0, l = vlen * k; k < vlen; k++, l++) {
                                        t += outid[l] * outid[l];
                                    }
                                    if (t <= cx * cx) {
                                        continue;
                                    }
                                    t = cx / sqrt(t);
                                    for (int k = 0, l = vlen * k; k < vlen; k++, l++) {
                                        outid[l] *= t;
                                    }
                                }
                            }
                            if (timeStep == 0) {
                                outDa = DataArray.create(outid, da.getVectorLength(), da.getName() + "_I", da.getUnit(), da.getUserData());
                                outDa.setPreferredRanges(cm, cx, clampPhysMin[iData], clampPhysMax[iData]);
                                outField.addComponent(outDa);
                            } else {
                                outDa.addRawArray(new IntLargeArray(outid), time);
                            }
                            break;
                        case FIELD_DATA_FLOAT:
                            float[] outfd = da.getRawFloatArray().getData();
                            if (da.getType().getValue() == actions[iData]) {
                                outfd = outfd.clone();
                            }
                            if (vlen == 1) {
                                for (int j = 0; j < outfd.length; j++) {
                                    outfd[j] = (outfd[j] > (float)cx ? (float)cx : (outfd[j] < (float)cm ? (float)cm : outfd[j]));
                                }
                            } else {
                                for (int j = 0; j < n; j++) {
                                    double t = 0;
                                    for (int k = 0, l = vlen * j; k < vlen; k++, l++) {
                                        t += outfd[l] * outfd[l];
                                    }
                                    if (t <= cx * cx) {
                                        continue;
                                    }
                                    t = cx / sqrt(t);
                                    for (int k = 0, l = vlen * j; k < vlen; k++, l++) {
                                        outfd[l] *= t;
                                    }
                                }
                            }
                            if (timeStep == 0) {
                                outDa = DataArray.create(outfd, da.getVectorLength(), da.getName() + "_F", da.getUnit(), da.getUserData());
                                outDa.setPreferredRanges(cm, cx, clampPhysMin[iData], clampPhysMax[iData]);
                                outField.addComponent(outDa);
                            } else {
                                outDa.addRawArray(new FloatLargeArray(outfd), time);
                            }
                            break;
                        case FIELD_DATA_DOUBLE:
                            double[] outdd = da.getRawDoubleArray().getData();
                            if (da.getType().getValue() == actions[iData]) {
                                outdd = outdd.clone();
                            }
                            if (vlen == 1) {
                                for (int j = 0; j < outdd.length; j++) {
                                    outdd[j] = (outdd[j] > cx ? cx : (outdd[j] < cm ? cm : outdd[j]));
                                }
                            } else {
                                for (int j = 0; j < n; j++) {
                                    double t = 0;
                                    for (int k = 0, l = vlen * k; k < vlen; k++, l++) {
                                        t += outdd[l] * outdd[l];
                                    }
                                    if (t <= cx * cx) {
                                        continue;
                                    }
                                    t = cx / sqrt(t);
                                    for (int k = 0, l = vlen * k; k < vlen; k++, l++) {
                                        outdd[l] *= t;
                                    }
                                }
                            }
                            if (timeStep == 0) {
                                outDa = DataArray.create(outdd, da.getVectorLength(), da.getName() + "_D", da.getUnit(), da.getUserData());
                                outDa.setPreferredRanges(cm, cx, clampPhysMin[iData], clampPhysMax[iData]);
                                outField.addComponent(outDa);
                            } else {
                                outDa.addRawArray(new DoubleLargeArray(outdd), time);
                            }
                            break;
                    }
                }
            }
        }
    }

    Field getOutField()
    {
        return outField;
    }
}
