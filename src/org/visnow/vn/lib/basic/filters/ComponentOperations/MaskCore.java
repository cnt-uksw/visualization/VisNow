/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.ComponentOperations;

import java.util.ArrayList;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.filters.ComponentOperations.ComponentOperationsShared.*;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class MaskCore
{

    private Parameters params = null;
    private Field inField = null;
    protected RegularField outRegularField = null;
    protected Field outField = null;

    public MaskCore()
    {
    }

    public void setData(Field inField, Field outField, Parameters p)
    {
        this.inField = inField;
        this.outField = outField;
        this.params = p;
    }

    void update()
    {
        LogicLargeArray mask;
        if (outField == null)
            return;
        int n = (int) outField.getNNodes();

        if (params.get(MASK_COMPONENT) < 0) {
            if (params.get(ADD_TO_MASK) == false) {
                outField.setMask(null);
            }
            else {
                outField.setCurrentMask(inField.getCurrentMask());
            }
            return;
        }
        
        if (inField.getCurrentMask() == null) {
            mask = new LogicLargeArray(n);
            for (int j = 0; j < n; j++)
                mask.setByte(j, (byte) 1);
        } else {
            if (params.get(ADD_TO_MASK) == true) {
                mask = inField.getCurrentMask().clone();
            } else {
                mask = new LogicLargeArray(n);
                for (int j = 0; j < n; j++)
                    mask.setByte(j, (byte) 1);
            }
        }

        int i = params.get(MASK_COMPONENT);
        float low = params.get(MASK_LOW_UP)[0];
        float up = params.get(MASK_LOW_UP)[1];

        if (i < inField.getNComponents()) {
            switch (inField.getComponent(i).getType()) {
                case FIELD_DATA_BYTE:
                    byte[] inB = (byte[])inField.getComponent(i).getRawArray().getData();
                    for (int j = 0; j < n; j++)
                        if ((0xFF & inB[j]) < low || (0xFF & inB[j]) > up)
                            mask.setByte(j, (byte) 0);
                    break;
                case FIELD_DATA_SHORT:
                case FIELD_DATA_INT:
                case FIELD_DATA_FLOAT:
                case FIELD_DATA_DOUBLE:
                    LargeArray inD = inField.getComponent(i).getRawArray();
                    for (int j = 0; j < n; j++)
                        if (inD.getDouble(j) < low || inD.getDouble(j) > up)
                            mask.setByte(j, (byte) 0);
                    break;
            }
        } else if (i >= inField.getNComponents()) {
            //mask by coords
            int dim = i - inField.getNComponents();
            if (inField instanceof RegularField && ((RegularField) inField).getCurrentCoords() == null) {
                //by affine
                int[] dims = ((RegularField) inField).getDims();
                float[] p;
                switch (dims.length) {
                    case 3:
                        for (int z = 0, j = 0; z < dims[2]; z++) {
                            for (int y = 0; y < dims[1]; y++) {
                                for (int x = 0; x < dims[0]; x++, j++) {
                                    p = ((RegularField) inField).getGridCoords(x, y, z);
                                    if (p[dim] < low || p[dim] > up) {
                                        mask.setByte(j, (byte) 0);
                                    }
                                }
                            }
                        }
                        break;
                    case 2:
                        for (int y = 0, j = 0; y < dims[1]; y++) {
                            for (int x = 0; x < dims[0]; x++, j++) {
                                p = ((RegularField) inField).getGridCoords(x, y);
                                if (p[dim] < low || p[dim] > up) {
                                    mask.setByte(j, (byte) 0);
                                }
                            }
                        }
                        break;
                    case 1:
                        for (int x = 0; x < dims[0]; x++) {
                            p = ((RegularField) inField).getGridCoords(x);
                            if (p[dim] < low || p[dim] > up) {
                                mask.setByte(x, (byte) 0);
                            }
                        }
                        break;
                }
            } else {
                float[] coords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords().getData();
                for (int j = 0; j < n; j++) {
                    if (coords[3 * j + dim] < low || coords[3 * j + dim] > up) {
                        mask.setByte(j, (byte) 0);
                    }
                }
            }
        }
        outField.setCurrentMask(mask);
        if (params.get(RECOMPUTE_MIN_MAX))
            for (DataArray da : outField.getComponents()) {
                ArrayList<Float> times = new ArrayList<>(1);
                times.add(0.0f);
                ArrayList<LargeArray> values = new ArrayList<>(1);
                values.add(mask);
                da.recomputeStatistics(new TimeData(times, values, 0), true);
            }
    }

    Field getOutField()
    {
        return outField;
    }
}
