/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.RadialCoordinates;

import org.visnow.vn.engine.core.ParameterName;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class RadialCoordinatesShared
{
    static final int CONSTANT_VALUE = -1;

    static final String MAP_SPHERICAL = "Spherical";
    static final String MAP_CYLINDRICAL = "Cylindrical";

    static final String[] MAP_TYPES = new String[]{
        MAP_SPHERICAL,
        MAP_CYLINDRICAL
    };

    //Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    //
    //Specification:
    
    //string from MAP_TYPES
    static final ParameterName<String> MAP_TYPE = new ParameterName("Map type");

    //CONSTANT_VALUE or [0 ... META_NUMBER_OF_DIMENSIONS-1] 
    static final ParameterName<Integer> RADIUS_AXIS = new ParameterName("Radius axis");
    static final ParameterName<Float> RADIUS_MIN = new ParameterName("Radius minimum");
    // >= RADIUS_MIN
    static final ParameterName<Float> RADIUS_MAX = new ParameterName("Radius maximum");
    //CONSTANT_VALUE or [0 ... META_NUMBER_OF_DIMENSIONS-1] 
    static final ParameterName<Integer> PHI_AXIS = new ParameterName("Phi axis");
    static final ParameterName<Float> PHI_MIN = new ParameterName("Phi minimum");
    // >= PHI_MIN
    static final ParameterName<Float> PHI_MAX = new ParameterName("Phi maximum");
    //CONSTANT_VALUE or [0 ... META_NUMBER_OF_DIMENSIONS-1] 
    static final ParameterName<Integer> PSI_AXIS = new ParameterName("Psi axis");
    static final ParameterName<Float> PSI_MIN = new ParameterName("Psi minimum");
    // >= PSI_MIN
    static final ParameterName<Float> PSI_MAX = new ParameterName("Psi maximum");
    //CONSTANT_VALUE or [0 ... META_NUMBER_OF_DIMENSIONS-1] 
    static final ParameterName<Integer> HEIGHT_AXIS = new ParameterName("Height axis");
    static final ParameterName<Float> HEIGHT_MIN = new ParameterName("Height minimum");
    // >= HEIGHT_MIN
    static final ParameterName<Float> HEIGHT_MAX = new ParameterName("Height maximum");
    static final ParameterName<Boolean> ADJUSTING = new ParameterName("Adjusting");
    static final ParameterName<Boolean> TRANSFORM_VECTORS = new ParameterName("Transform vectors");

    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic)    
    //integer: 1,2 or 3
    static final ParameterName<Integer> META_NUMBER_OF_DIMENSIONS = new ParameterName("META Number of dimensions");
}
