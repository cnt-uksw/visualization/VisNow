/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.RadialCoordinates;

import java.awt.CardLayout;
import org.apache.log4j.Logger;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.widgets.FloatSlider;
import org.visnow.vn.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider;
import static org.visnow.vn.lib.basic.filters.RadialCoordinates.RadialCoordinatesShared.*;

public class GUI extends javax.swing.JPanel
{
    private static final Logger LOGGER = Logger.getLogger(GUI.class);

    private Parameters parameters;
    private static final float radianRatio = (float) (PI / 180);

    /**
     * Creates new form EmptyVisnowModuleGUI
     */
    public GUI()
    {
        initComponents();
        mapCombo.setListData(MAP_TYPES);
        update3rdDimPanels(true);
        updateIJKPanels();
    }

    public void setParameters(Parameters parameters)
    {
        this.parameters = parameters;
    }

    public void updateGUI(ParameterProxy p, boolean resetGUIControls)
    {
        int dimNum = p.get(META_NUMBER_OF_DIMENSIONS);

        yRRadioButton.setVisible(dimNum > 1);
        yPhiRadioButton.setVisible(dimNum > 1);
        yPsiRadioButton.setVisible(dimNum > 1);
        yHeightRadioButton.setVisible(dimNum > 1);
        zRRadioButton.setVisible(dimNum > 2);
        zPhiRadioButton.setVisible(dimNum > 2);
        zPsiRadioButton.setVisible(dimNum > 2);
        zHeightRadioButton.setVisible(dimNum > 2);

        xRRadioButton.setSelected(p.get(RADIUS_AXIS) == 0);
        yRRadioButton.setSelected(p.get(RADIUS_AXIS) == 1);
        zRRadioButton.setSelected(p.get(RADIUS_AXIS) == 2);
        xPhiRadioButton.setSelected(p.get(PHI_AXIS) == 0);
        yPhiRadioButton.setSelected(p.get(PHI_AXIS) == 1);
        zPhiRadioButton.setSelected(p.get(PHI_AXIS) == 2);
        xPsiRadioButton.setSelected(p.get(PSI_AXIS) == 0);
        yPsiRadioButton.setSelected(p.get(PSI_AXIS) == 1);
        zPsiRadioButton.setSelected(p.get(PSI_AXIS) == 2);
        xHeightRadioButton.setSelected(p.get(HEIGHT_AXIS) == 0);
        yHeightRadioButton.setSelected(p.get(HEIGHT_AXIS) == 1);
        zHeightRadioButton.setSelected(p.get(HEIGHT_AXIS) == 2);

        rConstantRadioButton.setSelected(p.get(RADIUS_AXIS) == CONSTANT_VALUE);
        rVariableRadioButton.setSelected(p.get(RADIUS_AXIS) != CONSTANT_VALUE);
        phiConstantRadioButton.setSelected(p.get(PHI_AXIS) == CONSTANT_VALUE);
        phiVariableRadioButton.setSelected(p.get(PHI_AXIS) != CONSTANT_VALUE);
        psiConstantRadioButton.setSelected(p.get(PSI_AXIS) == CONSTANT_VALUE);
        psiVariableRadioButton.setSelected(p.get(PSI_AXIS) != CONSTANT_VALUE);
        heightConstantRadioButton.setSelected(p.get(HEIGHT_AXIS) == CONSTANT_VALUE);
        heightVariableRadioButton.setSelected(p.get(HEIGHT_AXIS) != CONSTANT_VALUE);

        if (resetGUIControls) {
            rRangeSlider.setMinMax(0, 1);
            phiSlider.setMinMax(-90, 90);
            psiSlider.setMinMax(0, 360);
            heightSlider.setMinMax(0, 1);
        }

        if (p.get(RADIUS_AXIS) == CONSTANT_VALUE) {
            validateSlider(rSlider, p.get(RADIUS_MIN));
            rSlider.setVal(p.get(RADIUS_MIN));
        } else {
            validateSlider(rRangeSlider, p.get(RADIUS_MIN), p.get(RADIUS_MAX));
            rRangeSlider.setLowUp(p.get(RADIUS_MIN), p.get(RADIUS_MAX));
        }
        if (p.get(PHI_AXIS) == CONSTANT_VALUE) {
            validateSlider(phiSlider, p.get(PHI_MIN) / radianRatio);
            phiSlider.setVal(p.get(PHI_MIN) / radianRatio);
        } else {
            validateSlider(phiRangeSlider, p.get(PHI_MIN) / radianRatio, p.get(PHI_MAX) / radianRatio);
            phiRangeSlider.setLowUp(p.get(PHI_MIN) / radianRatio, p.get(PHI_MAX) / radianRatio);
        }
        if (p.get(PSI_AXIS) == CONSTANT_VALUE) {
            validateSlider(psiSlider, p.get(PSI_MIN) / radianRatio);
            psiSlider.setVal(p.get(PSI_MIN) / radianRatio);
        } else {
            validateSlider(psiRangeSlider, p.get(PSI_MIN) / radianRatio, p.get(PSI_MAX) / radianRatio);
            psiRangeSlider.setLowUp(p.get(PSI_MIN) / radianRatio, p.get(PSI_MAX) / radianRatio);
        }
        if (p.get(HEIGHT_AXIS) == CONSTANT_VALUE) {
            validateSlider(heightSlider, p.get(HEIGHT_MIN));
            heightSlider.setVal(p.get(HEIGHT_MIN));
        } else {
            validateSlider(heightRangeSlider, p.get(HEIGHT_MIN), p.get(HEIGHT_MAX));
            heightRangeSlider.setLowUp(p.get(HEIGHT_MIN), p.get(HEIGHT_MAX));
        }

        update3rdDimPanels(p.get(MAP_TYPE).equals(MAP_SPHERICAL));
        updateIJKPanels();
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        rAxisButtonGroup = new javax.swing.ButtonGroup();
        phiAxisButtonGroup = new javax.swing.ButtonGroup();
        psiAxisButtonGroup = new javax.swing.ButtonGroup();
        radiusButtonGroup = new javax.swing.ButtonGroup();
        phiButtonGroup = new javax.swing.ButtonGroup();
        psiButtonGroup = new javax.swing.ButtonGroup();
        heightButtonGroup = new javax.swing.ButtonGroup();
        heightAxisButtonGroup = new javax.swing.ButtonGroup();
        jLabel4 = new javax.swing.JLabel();
        mapCombo = new org.visnow.vn.gui.swingwrappers.ComboBox();
        radiusModePanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        rConstantRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        rVariableRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        radiusPanel = new javax.swing.JPanel();
        varRPanel = new javax.swing.JPanel();
        rAxisPanel = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        xRRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        yRRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        zRRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        rRangeSlider = new org.visnow.vn.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider();
        constRPanel = new javax.swing.JPanel();
        rSlider = new org.visnow.vn.gui.widgets.FloatSlider();
        phiModePanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        phiConstantRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        phiVariableRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        phiPanel = new javax.swing.JPanel();
        varPhiPanel = new javax.swing.JPanel();
        phiAxisPanel = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        xPhiRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        yPhiRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        zPhiRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        phiRangeSlider = new org.visnow.vn.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider();
        constPhiPanel = new javax.swing.JPanel();
        phiSlider = new org.visnow.vn.gui.widgets.FloatSlider();
        psiModePanel = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        psiConstantRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        psiVariableRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        psiPanel = new javax.swing.JPanel();
        varPsiPanel = new javax.swing.JPanel();
        psiAxisPanel = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        xPsiRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        yPsiRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        zPsiRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        psiRangeSlider = new org.visnow.vn.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider();
        constPsiPanel = new javax.swing.JPanel();
        psiSlider = new org.visnow.vn.gui.widgets.FloatSlider();
        heightModePanel = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        heightConstantRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        heightVariableRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        heightPanel = new javax.swing.JPanel();
        varHeightPanel = new javax.swing.JPanel();
        heightAxisPanel = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        xHeightRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        yHeightRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        zHeightRadioButton = new org.visnow.vn.gui.swingwrappers.RadioButton();
        heightRangeSlider = new org.visnow.vn.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider();
        constHeightPanel = new javax.swing.JPanel();
        heightSlider = new org.visnow.vn.gui.widgets.FloatSlider();
        transformVectorsBox = new javax.swing.JCheckBox();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        setLayout(new java.awt.GridBagLayout());

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/visnow/vn/lib/basic/filters/RadialCoordinates/Bundle"); // NOI18N
        jLabel4.setText(bundle.getString("GUI.jLabel4.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 4, 0);
        add(jLabel4, gridBagConstraints);

        mapCombo.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                mapComboUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 12, 0, 4);
        add(mapCombo, gridBagConstraints);

        radiusModePanel.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText(bundle.getString("GUI.jLabel1.text")); // NOI18N
        radiusModePanel.add(jLabel1, new java.awt.GridBagConstraints());

        radiusButtonGroup.add(rConstantRadioButton);
        rConstantRadioButton.setText(bundle.getString("GUI.rConstantRadioButton.text")); // NOI18N
        rConstantRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                rTypeRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        radiusModePanel.add(rConstantRadioButton, gridBagConstraints);

        radiusButtonGroup.add(rVariableRadioButton);
        rVariableRadioButton.setSelected(true);
        rVariableRadioButton.setText(bundle.getString("GUI.rVariableRadioButton.text")); // NOI18N
        rVariableRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                rTypeRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        radiusModePanel.add(rVariableRadioButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(8, 4, 0, 4);
        add(radiusModePanel, gridBagConstraints);

        radiusPanel.setLayout(new java.awt.CardLayout());

        varRPanel.setLayout(new java.awt.GridBagLayout());

        rAxisPanel.setLayout(new java.awt.GridBagLayout());

        jLabel2.setText(bundle.getString("GUI.jLabel2.text")); // NOI18N
        rAxisPanel.add(jLabel2, new java.awt.GridBagConstraints());

        rAxisButtonGroup.add(xRRadioButton);
        xRRadioButton.setSelected(true);
        xRRadioButton.setText(bundle.getString("GUI.xRRadioButton.text")); // NOI18N
        xRRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                rRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        rAxisPanel.add(xRRadioButton, new java.awt.GridBagConstraints());

        rAxisButtonGroup.add(yRRadioButton);
        yRRadioButton.setText(bundle.getString("GUI.yRRadioButton.text")); // NOI18N
        yRRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                rRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        rAxisPanel.add(yRRadioButton, new java.awt.GridBagConstraints());

        rAxisButtonGroup.add(zRRadioButton);
        zRRadioButton.setText(bundle.getString("GUI.zRRadioButton.text")); // NOI18N
        zRRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                rRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        rAxisPanel.add(zRRadioButton, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        varRPanel.add(rAxisPanel, gridBagConstraints);

        rRangeSlider.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        rRangeSlider.setMax(1.0F);
        rRangeSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                rRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        varRPanel.add(rRangeSlider, gridBagConstraints);

        radiusPanel.add(varRPanel, "variable");

        constRPanel.setLayout(new java.awt.BorderLayout());

        rSlider.setVal(1.0F);
        rSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                rSliderStateChanged(evt);
            }
        });
        constRPanel.add(rSlider, java.awt.BorderLayout.CENTER);

        radiusPanel.add(constRPanel, "constant");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 12, 0, 4);
        add(radiusPanel, gridBagConstraints);

        phiModePanel.setLayout(new java.awt.GridBagLayout());

        jLabel3.setText(bundle.getString("GUI.jLabel3.text")); // NOI18N
        phiModePanel.add(jLabel3, new java.awt.GridBagConstraints());

        phiButtonGroup.add(phiConstantRadioButton);
        phiConstantRadioButton.setText(bundle.getString("GUI.phiConstantRadioButton.text")); // NOI18N
        phiConstantRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                phiTypeRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        phiModePanel.add(phiConstantRadioButton, gridBagConstraints);

        phiButtonGroup.add(phiVariableRadioButton);
        phiVariableRadioButton.setSelected(true);
        phiVariableRadioButton.setText(bundle.getString("GUI.phiVariableRadioButton.text")); // NOI18N
        phiVariableRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                phiTypeRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        phiModePanel.add(phiVariableRadioButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(8, 4, 0, 4);
        add(phiModePanel, gridBagConstraints);

        phiPanel.setLayout(new java.awt.CardLayout());

        varPhiPanel.setLayout(new java.awt.GridBagLayout());

        phiAxisPanel.setLayout(new java.awt.GridBagLayout());

        jLabel7.setText(bundle.getString("GUI.jLabel7.text")); // NOI18N
        phiAxisPanel.add(jLabel7, new java.awt.GridBagConstraints());

        phiAxisButtonGroup.add(xPhiRadioButton);
        xPhiRadioButton.setSelected(true);
        xPhiRadioButton.setText(bundle.getString("GUI.xPhiRadioButton.text")); // NOI18N
        xPhiRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                phiRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        phiAxisPanel.add(xPhiRadioButton, new java.awt.GridBagConstraints());

        phiAxisButtonGroup.add(yPhiRadioButton);
        yPhiRadioButton.setText(bundle.getString("GUI.yPhiRadioButton.text")); // NOI18N
        yPhiRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                phiRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        phiAxisPanel.add(yPhiRadioButton, new java.awt.GridBagConstraints());

        phiAxisButtonGroup.add(zPhiRadioButton);
        zPhiRadioButton.setText(bundle.getString("GUI.zPhiRadioButton.text")); // NOI18N
        zPhiRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                phiRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        phiAxisPanel.add(zPhiRadioButton, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        varPhiPanel.add(phiAxisPanel, gridBagConstraints);

        phiRangeSlider.setLow(-90.0F);
        phiRangeSlider.setMax(90.0F);
        phiRangeSlider.setMin(-90.0F);
        phiRangeSlider.setOpaque(false);
        phiRangeSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                phiRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        varPhiPanel.add(phiRangeSlider, gridBagConstraints);

        phiPanel.add(varPhiPanel, "variable");

        constPhiPanel.setLayout(new java.awt.BorderLayout());

        phiSlider.setMax(90.0F);
        phiSlider.setMin(-90.0F);
        phiSlider.setVal(0.0F);
        phiSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                phiSliderStateChanged(evt);
            }
        });
        constPhiPanel.add(phiSlider, java.awt.BorderLayout.CENTER);

        phiPanel.add(constPhiPanel, "constant");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 12, 0, 4);
        add(phiPanel, gridBagConstraints);

        psiModePanel.setLayout(new java.awt.GridBagLayout());

        jLabel5.setText(bundle.getString("GUI.jLabel5.text")); // NOI18N
        psiModePanel.add(jLabel5, new java.awt.GridBagConstraints());

        psiButtonGroup.add(psiConstantRadioButton);
        psiConstantRadioButton.setText(bundle.getString("GUI.psiConstantRadioButton.text")); // NOI18N
        psiConstantRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                psiTypeRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        psiModePanel.add(psiConstantRadioButton, gridBagConstraints);

        psiButtonGroup.add(psiVariableRadioButton);
        psiVariableRadioButton.setText(bundle.getString("GUI.psiVariableRadioButton.text")); // NOI18N
        psiVariableRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                psiTypeRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        psiModePanel.add(psiVariableRadioButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(8, 4, 0, 4);
        add(psiModePanel, gridBagConstraints);

        psiPanel.setLayout(new java.awt.CardLayout());

        varPsiPanel.setLayout(new java.awt.GridBagLayout());

        psiAxisPanel.setLayout(new java.awt.GridBagLayout());

        jLabel8.setText(bundle.getString("GUI.jLabel8.text")); // NOI18N
        psiAxisPanel.add(jLabel8, new java.awt.GridBagConstraints());

        psiAxisButtonGroup.add(xPsiRadioButton);
        xPsiRadioButton.setSelected(true);
        xPsiRadioButton.setText(bundle.getString("GUI.xPsiRadioButton.text")); // NOI18N
        xPsiRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                psiRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        psiAxisPanel.add(xPsiRadioButton, new java.awt.GridBagConstraints());

        psiAxisButtonGroup.add(yPsiRadioButton);
        yPsiRadioButton.setText(bundle.getString("GUI.yPsiRadioButton.text")); // NOI18N
        yPsiRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                psiRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        psiAxisPanel.add(yPsiRadioButton, new java.awt.GridBagConstraints());

        psiAxisButtonGroup.add(zPsiRadioButton);
        zPsiRadioButton.setText(bundle.getString("GUI.zPsiRadioButton.text")); // NOI18N
        zPsiRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                psiRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        psiAxisPanel.add(zPsiRadioButton, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        varPsiPanel.add(psiAxisPanel, gridBagConstraints);

        psiRangeSlider.setLow(-180.0F);
        psiRangeSlider.setMax(360.0F);
        psiRangeSlider.setUp(180.0F);
        psiRangeSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                psiRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        varPsiPanel.add(psiRangeSlider, gridBagConstraints);

        psiPanel.add(varPsiPanel, "variable");

        psiSlider.setMax(360.0F);
        psiSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                psiSliderStateChanged(evt);
            }
        });

        javax.swing.GroupLayout constPsiPanelLayout = new javax.swing.GroupLayout(constPsiPanel);
        constPsiPanel.setLayout(constPsiPanelLayout);
        constPsiPanelLayout.setHorizontalGroup(
            constPsiPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(psiSlider, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
        );
        constPsiPanelLayout.setVerticalGroup(
            constPsiPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(constPsiPanelLayout.createSequentialGroup()
                .addComponent(psiSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        psiPanel.add(constPsiPanel, "constant");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 12, 0, 4);
        add(psiPanel, gridBagConstraints);

        heightModePanel.setLayout(new java.awt.GridBagLayout());

        jLabel6.setText(bundle.getString("GUI.jLabel6.text")); // NOI18N
        heightModePanel.add(jLabel6, new java.awt.GridBagConstraints());

        heightButtonGroup.add(heightConstantRadioButton);
        heightConstantRadioButton.setText(bundle.getString("GUI.heightConstantRadioButton.text")); // NOI18N
        heightConstantRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                heightTypeRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        heightModePanel.add(heightConstantRadioButton, gridBagConstraints);

        heightButtonGroup.add(heightVariableRadioButton);
        heightVariableRadioButton.setSelected(true);
        heightVariableRadioButton.setText(bundle.getString("GUI.heightVariableRadioButton.text")); // NOI18N
        heightVariableRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                heightTypeRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        heightModePanel.add(heightVariableRadioButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(8, 4, 0, 4);
        add(heightModePanel, gridBagConstraints);

        heightPanel.setLayout(new java.awt.CardLayout());

        varHeightPanel.setLayout(new java.awt.GridBagLayout());

        heightAxisPanel.setLayout(new java.awt.GridBagLayout());

        jLabel9.setText(bundle.getString("GUI.jLabel9.text")); // NOI18N
        heightAxisPanel.add(jLabel9, new java.awt.GridBagConstraints());

        heightAxisButtonGroup.add(xHeightRadioButton);
        xHeightRadioButton.setSelected(true);
        xHeightRadioButton.setText(bundle.getString("GUI.xHeightRadioButton.text")); // NOI18N
        xHeightRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                heightRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        heightAxisPanel.add(xHeightRadioButton, new java.awt.GridBagConstraints());

        heightAxisButtonGroup.add(yHeightRadioButton);
        yHeightRadioButton.setText(bundle.getString("GUI.yHeightRadioButton.text")); // NOI18N
        yHeightRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                heightRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        heightAxisPanel.add(yHeightRadioButton, new java.awt.GridBagConstraints());

        heightAxisButtonGroup.add(zHeightRadioButton);
        zHeightRadioButton.setText(bundle.getString("GUI.zHeightRadioButton.text")); // NOI18N
        zHeightRadioButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                heightRBAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        heightAxisPanel.add(zHeightRadioButton, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        varHeightPanel.add(heightAxisPanel, gridBagConstraints);

        heightRangeSlider.setMax(1.0F);
        heightRangeSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                heightRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        varHeightPanel.add(heightRangeSlider, gridBagConstraints);

        heightPanel.add(varHeightPanel, "variable");

        heightSlider.setVal(1.0F);
        heightSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                heightSliderStateChanged(evt);
            }
        });

        javax.swing.GroupLayout constHeightPanelLayout = new javax.swing.GroupLayout(constHeightPanel);
        constHeightPanel.setLayout(constHeightPanelLayout);
        constHeightPanelLayout.setHorizontalGroup(
            constHeightPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(heightSlider, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
        );
        constHeightPanelLayout.setVerticalGroup(
            constHeightPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(constHeightPanelLayout.createSequentialGroup()
                .addComponent(heightSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        heightPanel.add(constHeightPanel, "constant");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 12, 0, 4);
        add(heightPanel, gridBagConstraints);

        transformVectorsBox.setSelected(true);
        transformVectorsBox.setText(bundle.getString("GUI.transformVectorsBox.text")); // NOI18N
        transformVectorsBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                transformVectorsBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(12, 0, 0, 0);
        add(transformVectorsBox, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weighty = 1.0;
        add(filler1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void validateSlider(FloatSlider slider, float value)
    {
        if (slider.getMin() > value) slider.setMin(value);
        if (slider.getMax() < value) slider.setMax(value);
    }

    private void validateSlider(ExtendedFloatSubRangeSlider slider, float min, float max)
    {
        if (slider.getMin() > min) slider.setMin(min);
        if (slider.getMax() < max) slider.setMax(max);
    }


    private void transformVectorsBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_transformVectorsBoxActionPerformed
    {//GEN-HEADEREND:event_transformVectorsBoxActionPerformed
        parameters.set(TRANSFORM_VECTORS, transformVectorsBox.isSelected());
    }//GEN-LAST:event_transformVectorsBoxActionPerformed

    private void mapComboUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_mapComboUserChangeAction
    {//GEN-HEADEREND:event_mapComboUserChangeAction
        boolean spherical = mapCombo.getSelectedItem().equals(MAP_SPHERICAL);
        update3rdDimPanels(spherical);
        parameters.set(MAP_TYPE, spherical ? MAP_SPHERICAL : MAP_CYLINDRICAL);
    }//GEN-LAST:event_mapComboUserChangeAction

    private void phiRBAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_phiRBAction
    {//GEN-HEADEREND:event_phiRBAction
        parameters.set(PHI_AXIS, xPhiRadioButton.isSelected() ? 0 : yPhiRadioButton.isSelected() ? 1 : 2);
    }//GEN-LAST:event_phiRBAction

    private void rRBAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_rRBAction
    {//GEN-HEADEREND:event_rRBAction
        parameters.set(RADIUS_AXIS, xRRadioButton.isSelected() ? 0 : yRRadioButton.isSelected() ? 1 : 2);
    }//GEN-LAST:event_rRBAction

    private void psiRBAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_psiRBAction
    {//GEN-HEADEREND:event_psiRBAction
        parameters.set(PSI_AXIS, xPsiRadioButton.isSelected() ? 0 : yPsiRadioButton.isSelected() ? 1 : 2);
    }//GEN-LAST:event_psiRBAction

    private void heightRBAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_heightRBAction
    {//GEN-HEADEREND:event_heightRBAction
        parameters.set(HEIGHT_AXIS, xHeightRadioButton.isSelected() ? 0 : yHeightRadioButton.isSelected() ? 1 : 2);
    }//GEN-LAST:event_heightRBAction

    private void rTypeRBAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_rTypeRBAction
    {//GEN-HEADEREND:event_rTypeRBAction
        updateIJKPanels();
        if (rConstantRadioButton.isSelected()) parameters.set(RADIUS_AXIS, CONSTANT_VALUE);
        else rRBAction(null);
    }//GEN-LAST:event_rTypeRBAction

    private void phiTypeRBAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_phiTypeRBAction
    {//GEN-HEADEREND:event_phiTypeRBAction
        updateIJKPanels();
        if (phiConstantRadioButton.isSelected()) parameters.set(PHI_AXIS, CONSTANT_VALUE);
        else phiRBAction(null);
    }//GEN-LAST:event_phiTypeRBAction

    private void psiTypeRBAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_psiTypeRBAction
    {//GEN-HEADEREND:event_psiTypeRBAction
        updateIJKPanels();
        if (psiConstantRadioButton.isSelected()) parameters.set(PSI_AXIS, CONSTANT_VALUE);
        else psiRBAction(null);
    }//GEN-LAST:event_psiTypeRBAction

    private void heightTypeRBAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_heightTypeRBAction
    {//GEN-HEADEREND:event_heightTypeRBAction
        updateIJKPanels();
        if (heightConstantRadioButton.isSelected()) parameters.set(HEIGHT_AXIS, CONSTANT_VALUE);
        else heightRBAction(null);
    }//GEN-LAST:event_heightTypeRBAction

    private void rRangeSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_rRangeSliderStateChanged
    {//GEN-HEADEREND:event_rRangeSliderStateChanged
        parameters.set(RADIUS_MIN, rRangeSlider.getLow(),
                       RADIUS_MAX, rRangeSlider.getUp());
    }//GEN-LAST:event_rRangeSliderStateChanged

    private void phiRangeSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_phiRangeSliderStateChanged
    {//GEN-HEADEREND:event_phiRangeSliderStateChanged
        parameters.set(PHI_MIN, phiRangeSlider.getLow() * radianRatio,
                       PHI_MAX, phiRangeSlider.getUp() * radianRatio);
    }//GEN-LAST:event_phiRangeSliderStateChanged

    private void psiRangeSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_psiRangeSliderStateChanged
    {//GEN-HEADEREND:event_psiRangeSliderStateChanged
        parameters.set(PSI_MIN, psiRangeSlider.getLow() * radianRatio,
                       PSI_MAX, psiRangeSlider.getUp() * radianRatio);
    }//GEN-LAST:event_psiRangeSliderStateChanged

    private void heightRangeSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_heightRangeSliderStateChanged
    {//GEN-HEADEREND:event_heightRangeSliderStateChanged
        parameters.set(HEIGHT_MIN, heightRangeSlider.getLow(),
                       HEIGHT_MAX, heightRangeSlider.getUp());
    }//GEN-LAST:event_heightRangeSliderStateChanged

    private void rSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_rSliderStateChanged
    {//GEN-HEADEREND:event_rSliderStateChanged
        parameters.set(RADIUS_MIN, rSlider.getVal());
    }//GEN-LAST:event_rSliderStateChanged

    private void phiSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_phiSliderStateChanged
    {//GEN-HEADEREND:event_phiSliderStateChanged
        parameters.set(PHI_MIN, phiSlider.getVal() * radianRatio);
    }//GEN-LAST:event_phiSliderStateChanged

    private void psiSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_psiSliderStateChanged
    {//GEN-HEADEREND:event_psiSliderStateChanged
        parameters.set(PSI_MIN, psiSlider.getVal() * radianRatio);
    }//GEN-LAST:event_psiSliderStateChanged

    private void heightSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_heightSliderStateChanged
    {//GEN-HEADEREND:event_heightSliderStateChanged
        parameters.set(HEIGHT_MIN, heightSlider.getVal());
    }//GEN-LAST:event_heightSliderStateChanged

    private void updateIJKPanels()
    {
        ((CardLayout) radiusPanel.getLayout()).show(radiusPanel, rConstantRadioButton.isSelected() ? "constant" : "variable");
        ((CardLayout) psiPanel.getLayout()).show(psiPanel, psiConstantRadioButton.isSelected() ? "constant" : "variable");
        ((CardLayout) phiPanel.getLayout()).show(phiPanel, phiConstantRadioButton.isSelected() ? "constant" : "variable");
        ((CardLayout) heightPanel.getLayout()).show(heightPanel, heightConstantRadioButton.isSelected() ? "constant" : "variable");
    }

    private void update3rdDimPanels(boolean spherical)
    {
        heightModePanel.setVisible(!spherical);
        heightPanel.setVisible(!spherical);
        psiModePanel.setVisible(spherical);
        psiPanel.setVisible(spherical);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel constHeightPanel;
    private javax.swing.JPanel constPhiPanel;
    private javax.swing.JPanel constPsiPanel;
    private javax.swing.JPanel constRPanel;
    private javax.swing.Box.Filler filler1;
    private javax.swing.ButtonGroup heightAxisButtonGroup;
    private javax.swing.JPanel heightAxisPanel;
    private javax.swing.ButtonGroup heightButtonGroup;
    private org.visnow.vn.gui.swingwrappers.RadioButton heightConstantRadioButton;
    private javax.swing.JPanel heightModePanel;
    private javax.swing.JPanel heightPanel;
    private org.visnow.vn.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider heightRangeSlider;
    private org.visnow.vn.gui.widgets.FloatSlider heightSlider;
    private org.visnow.vn.gui.swingwrappers.RadioButton heightVariableRadioButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private org.visnow.vn.gui.swingwrappers.ComboBox mapCombo;
    private javax.swing.ButtonGroup phiAxisButtonGroup;
    private javax.swing.JPanel phiAxisPanel;
    private javax.swing.ButtonGroup phiButtonGroup;
    private org.visnow.vn.gui.swingwrappers.RadioButton phiConstantRadioButton;
    private javax.swing.JPanel phiModePanel;
    private javax.swing.JPanel phiPanel;
    private org.visnow.vn.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider phiRangeSlider;
    private org.visnow.vn.gui.widgets.FloatSlider phiSlider;
    private org.visnow.vn.gui.swingwrappers.RadioButton phiVariableRadioButton;
    private javax.swing.ButtonGroup psiAxisButtonGroup;
    private javax.swing.JPanel psiAxisPanel;
    private javax.swing.ButtonGroup psiButtonGroup;
    private org.visnow.vn.gui.swingwrappers.RadioButton psiConstantRadioButton;
    private javax.swing.JPanel psiModePanel;
    private javax.swing.JPanel psiPanel;
    private org.visnow.vn.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider psiRangeSlider;
    private org.visnow.vn.gui.widgets.FloatSlider psiSlider;
    private org.visnow.vn.gui.swingwrappers.RadioButton psiVariableRadioButton;
    private javax.swing.ButtonGroup rAxisButtonGroup;
    private javax.swing.JPanel rAxisPanel;
    private org.visnow.vn.gui.swingwrappers.RadioButton rConstantRadioButton;
    private org.visnow.vn.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider rRangeSlider;
    private org.visnow.vn.gui.widgets.FloatSlider rSlider;
    private org.visnow.vn.gui.swingwrappers.RadioButton rVariableRadioButton;
    private javax.swing.ButtonGroup radiusButtonGroup;
    private javax.swing.JPanel radiusModePanel;
    private javax.swing.JPanel radiusPanel;
    private javax.swing.JCheckBox transformVectorsBox;
    private javax.swing.JPanel varHeightPanel;
    private javax.swing.JPanel varPhiPanel;
    private javax.swing.JPanel varPsiPanel;
    private javax.swing.JPanel varRPanel;
    private org.visnow.vn.gui.swingwrappers.RadioButton xHeightRadioButton;
    private org.visnow.vn.gui.swingwrappers.RadioButton xPhiRadioButton;
    private org.visnow.vn.gui.swingwrappers.RadioButton xPsiRadioButton;
    private org.visnow.vn.gui.swingwrappers.RadioButton xRRadioButton;
    private org.visnow.vn.gui.swingwrappers.RadioButton yHeightRadioButton;
    private org.visnow.vn.gui.swingwrappers.RadioButton yPhiRadioButton;
    private org.visnow.vn.gui.swingwrappers.RadioButton yPsiRadioButton;
    private org.visnow.vn.gui.swingwrappers.RadioButton yRRadioButton;
    private org.visnow.vn.gui.swingwrappers.RadioButton zHeightRadioButton;
    private org.visnow.vn.gui.swingwrappers.RadioButton zPhiRadioButton;
    private org.visnow.vn.gui.swingwrappers.RadioButton zPsiRadioButton;
    private org.visnow.vn.gui.swingwrappers.RadioButton zRRadioButton;
    // End of variables declaration//GEN-END:variables
}
