/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.PeronaMalikDiffusion;

import java.util.Arrays;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.filters.PeronaMalikDiffusion.PeronaMalikDiffusionShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.vn.engine.core.ProgressAgent;

public class PeronaMalikDiffusion extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI computeUI;
    private RegularField inField;
    private Core core = new Core();

    public PeronaMalikDiffusion()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(ITERATION_NUMBER, 2),
            new Parameter<>(LAMBDA_FOR_ITERATION_SCHEME, 0.15f),
            new Parameter<>(PARAMETER_K, 35.0f),
            new Parameter<>(NABLA_WEIGHT_INDEX_I, 1.0f),
            new Parameter<>(NABLA_WEIGHT_INDEX_J, 1.0f),
            new Parameter<>(NABLA_WEIGHT_INDEX_K, 1.0f),
            new Parameter<>(META_NUMBER_OF_DIMENSIONS, 3)};
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        float nablaWeightIndexI = parameters.get(NABLA_WEIGHT_INDEX_I);
        float nablaWeightIndexJ = parameters.get(NABLA_WEIGHT_INDEX_J);
        float nablaWeightIndexK = parameters.get(NABLA_WEIGHT_INDEX_K);
        float lambda0 = parameters.get(LAMBDA_FOR_ITERATION_SCHEME);

        parameters.setParameterActive(false);

        // 1.  validate parameters
        int dimNum = inField.getDimNum();
        parameters.set(META_NUMBER_OF_DIMENSIONS, dimNum);
        if (lambda0 < 0) {
            lambda0 = abs(lambda0);
            parameters.set(LAMBDA_FOR_ITERATION_SCHEME, lambda0);
        }
        // 2. reset parameters / set smart values
        if (resetParameters) {

            parameters.set(ITERATION_NUMBER, 2);
            parameters.set(PARAMETER_K, 35.0f);

            lambda0 = 0.15f;
            float lambda0AuxiliaryCoeff = 0.9f; // has to be < 1.0; used for calculation of lambda0
            nablaWeightIndexI = 1.0f;
            nablaWeightIndexJ = 1.0f;
            nablaWeightIndexK = 1.0f;
            if (inField.hasCoords() == false) {
                float[][] affine = inField.getAffine();
                if ((inField.getDimNum() == 2) && (inField.getTrueNSpace() == 2)) { //dims.length == 2 and contained in xy-plane
                    if (abs(affine[0][1]) <= 1.0e-10 && abs(affine[1][0]) <= 1.0e-10) {
                        nablaWeightIndexI = 1 / abs(affine[0][0]);
                        nablaWeightIndexJ = 1 / abs(affine[1][1]);                        
                        lambda0 = lambda0AuxiliaryCoeff / (2*nablaWeightIndexI*nablaWeightIndexI + 2*nablaWeightIndexJ*nablaWeightIndexJ);                        
                    }

                } else if (inField.getDimNum() == 3) {
                    if (abs(affine[0][1]) <= 1.0e-10 &&
                            abs(affine[0][2]) <= 1.0e-10 &&
                            abs(affine[1][0]) <= 1.0e-10 &&
                            abs(affine[1][2]) <= 1.0e-10 &&
                            abs(affine[2][0]) <= 1.0e-10 &&
                            abs(affine[2][1]) <= 1.0e-10) {
                        nablaWeightIndexI = 1 / abs(affine[0][0]);
                        nablaWeightIndexJ = 1 / abs(affine[1][1]);
                        nablaWeightIndexK = 1 / abs(affine[2][2]);
                        lambda0 = lambda0AuxiliaryCoeff / (2*nablaWeightIndexI*nablaWeightIndexI + 2*nablaWeightIndexJ*nablaWeightIndexJ + 2*nablaWeightIndexK*nablaWeightIndexK);
                    }
                }
            }

            parameters.set(NABLA_WEIGHT_INDEX_I, nablaWeightIndexI);
            parameters.set(NABLA_WEIGHT_INDEX_J, nablaWeightIndexJ);
            parameters.set(NABLA_WEIGHT_INDEX_K, nablaWeightIndexK);
            parameters.set(LAMBDA_FOR_ITERATION_SCHEME, lambda0);

        }

        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully);
    }

    @Override
    public void onActive()
    {

        if (getInputFirstValue("inField") != null) {
            //1. get new field
            RegularField newInField = (RegularField) ((VNField) getInputFirstValue("inField")).getField();
            //1a. set "different Field" flag
            boolean isDifferentField = !isFromVNA() && (inField == null || !Arrays.deepEquals(newInField.getAffine(), inField.getAffine()) || (newInField.getDimNum() != inField.getDimNum()));
            inField = newInField;

            Parameters p;
            synchronized (parameters) {
                //2. validate params             
                validateParamsAndSetSmart(isDifferentField);
                //2b. clone param (local read-only copy)
                p = parameters.getReadOnlyClone();
            }

            //3. update gui (GUI doesn't change parameters !!!!!!!!!!!!! - assuming correct set of parameters)
            notifyGUIs(p, isDifferentField || isFromVNA(), false);

            //4. run computation and propagate
            DataArray outDataArray = null;
            if (inField != null) {
                outRegularField = new RegularField(inField.getDims());
                outRegularField.setAffine(inField.getAffine());
                outRegularField.setCoords(inField.getCoords());
                core.setInField(inField);
                ProgressAgent progressAgent = getProgressAgent(inField.getNComponents() * p.get(ITERATION_NUMBER));
                progressAgent.setProgress(0.0f);
                for (int componentIndex = 0; componentIndex < inField.getNComponents(); componentIndex++) {

                    core.setInOutDataArrays(componentIndex);
                    if (!core.getIncompatibleVeclenFlag()) {

                        core.calculateAnisotropicDiffusion(p.get(ITERATION_NUMBER), p.get(LAMBDA_FOR_ITERATION_SCHEME), p.get(PARAMETER_K), p.get(NABLA_WEIGHT_INDEX_I), p.get(NABLA_WEIGHT_INDEX_J), p.get(NABLA_WEIGHT_INDEX_K), progressAgent);

                        outDataArray = core.getOutDataArray();
                        outRegularField.addComponent(outDataArray);
                    } else {
                        VisNow.get().userMessageSend(this, "Components with veclen != 1 are ignored", "", Level.WARNING);
                    }

                }
                outField = outRegularField;
                setOutputValue("outField", new VNRegularField(outRegularField));
                progressAgent.setProgress(1.0f);
            } else {
                outField = null;
                outRegularField = null;
                setOutputValue("outField", null);
            }

            prepareOutputGeometry();

            //create default presentation of outField
            show(); //and send it to output 

        }

    }
}
