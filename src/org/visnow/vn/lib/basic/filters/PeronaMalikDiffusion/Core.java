/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.PeronaMalikDiffusion;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.apache.log4j.Logger;
import org.visnow.jscic.dataarrays.ByteDataArray;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.dataarrays.FloatDataArray;
import org.visnow.jscic.utils.ArrayUtils;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.engine.core.ProgressAgent;

/**
 * @author Jedrzej M. Nowosielski (jnow@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class Core
{

    private static final Logger LOGGER = Logger.getLogger(Core.class);

    private RegularField inField = null;

    private DataArray inDataArray;
    private DataArray outDataArray;
    int componentIndex;
    boolean incompatibleVeclenFlag = false;

    public Core()
    {
    }

    public void setInField(RegularField field)
    {
        this.inField = field;

    }

    /**
     * set/reset the working inDataArray and outDataArray for computation
     */
    public void setInOutDataArrays(int componentIndex)
    {
        incompatibleVeclenFlag = false;
        if (inField != null) {
            this.componentIndex = componentIndex;
            DataArray tmpDataArray = inField.getComponent(componentIndex);
            if (tmpDataArray.getVectorLength() != 1) {
                LOGGER.error("Only veclen == 1 is supported");
                incompatibleVeclenFlag = true;
            }

            switch (tmpDataArray.getType()) {
                case FIELD_DATA_FLOAT:
                    inDataArray = tmpDataArray.cloneDeep();
                    //inDataArray = new FloatDataArray(tmpDataArray.getFData().getData().clone(), 1, tmpDataArray.getName());
                    break;
                default:
                    inDataArray = new FloatDataArray(tmpDataArray.getRawFloatArray(), new DataArraySchema(tmpDataArray.getName(), DataArrayType.FIELD_DATA_FLOAT, tmpDataArray.getNElements(), tmpDataArray.getVectorLength(), false));
                    break;
            }

            float[] outArray = new float[(int) inDataArray.getNElements()];

            outDataArray = DataArray.create(outArray, 1, tmpDataArray.getName() + "_diffusionResult");
        } else {
            LOGGER.warn("inField is null.");
        }
    }

    /**
     * @return the outDataArray
     */
    public DataArray getOutDataArray()
    {
        return outDataArray;
    }

    /**
     * @return the incompatibleVeclenFlag
     */
    public boolean getIncompatibleVeclenFlag()
    {
        return incompatibleVeclenFlag;
    }

    public void incrementTIndex(final float lambda0, final float K, final float nablaWeightIndexI, final float nablaWeightIndexJ, final float nablaWeightIndexK)
    {

        final float[] inArray = (float[]) inDataArray.getRawArray().getData();
        final float[] outArray = (float[]) outDataArray.getRawArray().getData();
        
        final float nablaWeightSquaredIndexI = nablaWeightIndexI*nablaWeightIndexI;
        final float nablaWeightSquaredIndexJ = nablaWeightIndexJ*nablaWeightIndexJ;
        final float nablaWeightSquaredIndexK = nablaWeightIndexK*nablaWeightIndexK;

        final int[] arraySize = inField.getDims();
        int flatArrayLength = (int) inDataArray.getRawArray().length();

        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), flatArrayLength);
        int k = flatArrayLength / nthreads;
        Future<?>[] futures = new Future[nthreads];

        if ((inField.getDimNum()) == 1) {

            for (int j = 0; j < nthreads; j++) {
                final int firstIdx = j * k;
                final int lastIdx = (j == nthreads - 1) ? flatArrayLength : firstIdx + k;
                futures[j] = ConcurrencyUtils.submit(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        for (int ii = firstIdx; ii < lastIdx; ii++) {
                            
                            if ((ii == 0) || (ii == (arraySize[0] - 1))) {
                                outArray[ii] = inArray[ii];
                            } else {

                                float inArrayNablaE = inArray[ii + 1] - inArray[ii];
                                float inArrayNablaW = inArray[ii - 1] - inArray[ii];

                                float cCoeffE = g1Function((float) abs(inArrayNablaE), K);
                                float cCoeffW = g1Function((float) abs(inArrayNablaW), K);

                                outArray[ii] = inArray[ii] + lambda0 * (cCoeffE * inArrayNablaE + cCoeffW * inArrayNablaW);
                            }

                        }
                    }
                });
            }

//            for (int ii = 0; ii < (arraySize[0]); ii++) {
//
//            }
            try {
                ConcurrencyUtils.waitForCompletion(futures);
            } catch (InterruptedException | ExecutionException ex) {
                throw new IllegalStateException(ex);
            }

        } else if ((inField.getDimNum()) == 2) {

            for (int j = 0; j < nthreads; j++) {
                final int firstIdx = j * k;
                final int lastIdx = (j == nthreads - 1) ? flatArrayLength : firstIdx + k;
                futures[j] = ConcurrencyUtils.submit(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        for (int i = firstIdx; i < lastIdx; i++) {
                            int ii = i % arraySize[0];
                            int jj = i / arraySize[0];

                            if ((jj != 0) && (ii != 0) && (jj != (arraySize[1] - 1)) && (ii != (arraySize[0] - 1))) {
                                float inArrayNablaS = inArray[(jj - 1) * arraySize[0] + ii] - inArray[jj * arraySize[0] + ii];
                                float inArrayNablaN = inArray[(jj + 1) * arraySize[0] + ii] - inArray[jj * arraySize[0] + ii];
                                float inArrayNablaE = inArray[jj * arraySize[0] + ii + 1] - inArray[jj * arraySize[0] + ii];
                                float inArrayNablaW = inArray[jj * arraySize[0] + ii - 1] - inArray[jj * arraySize[0] + ii];

                                float cCoeffN = g1Function((float) abs(nablaWeightIndexJ*inArrayNablaN), K);
                                float cCoeffS = g1Function((float) abs(nablaWeightIndexJ*inArrayNablaS), K);
                                float cCoeffE = g1Function((float) abs(nablaWeightIndexI*inArrayNablaE), K);
                                float cCoeffW = g1Function((float) abs(nablaWeightIndexI*inArrayNablaW), K);

                                outArray[jj * arraySize[0] + ii] = inArray[jj * arraySize[0] + ii] + 
                                        lambda0 * (cCoeffN * nablaWeightSquaredIndexJ*inArrayNablaN + cCoeffS * nablaWeightSquaredIndexJ*inArrayNablaS +
                                                   cCoeffE * nablaWeightSquaredIndexI*inArrayNablaE + cCoeffW * nablaWeightSquaredIndexI*inArrayNablaW);
                            } else if (((jj == 0) || (jj == (arraySize[1] - 1))) && (ii != 0) && (ii != (arraySize[0] - 1))) {
                                float inArrayNablaE = inArray[jj * arraySize[0] + ii + 1] - inArray[jj * arraySize[0] + ii];
                                float inArrayNablaW = inArray[jj * arraySize[0] + ii - 1] - inArray[jj * arraySize[0] + ii];

                                float cCoeffE = g1Function((float) abs(nablaWeightIndexI*inArrayNablaE), K);
                                float cCoeffW = g1Function((float) abs(nablaWeightIndexI*inArrayNablaW), K);

                                outArray[jj * arraySize[0] + ii] = inArray[jj * arraySize[0] + ii] + 
                                        lambda0 * (cCoeffE * nablaWeightSquaredIndexI*inArrayNablaE + cCoeffW * nablaWeightSquaredIndexI*inArrayNablaW);

                            } else if (((ii == 0) || (ii == (arraySize[0] - 1))) && (jj != 0) && (jj != (arraySize[1] - 1))) {
                                float inArrayNablaS = inArray[(jj - 1) * arraySize[0] + ii] - inArray[jj * arraySize[0] + ii];
                                float inArrayNablaN = inArray[(jj + 1) * arraySize[0] + ii] - inArray[jj * arraySize[0] + ii];

                                float cCoeffN = g1Function((float) abs(nablaWeightIndexJ*inArrayNablaN), K);
                                float cCoeffS = g1Function((float) abs(nablaWeightIndexJ*inArrayNablaS), K);

                                outArray[jj * arraySize[0] + ii] = inArray[jj * arraySize[0] + ii] + 
                                        lambda0 * (cCoeffN * nablaWeightSquaredIndexJ*inArrayNablaN + cCoeffS * nablaWeightSquaredIndexJ*inArrayNablaS);

                            } else {
                                outArray[jj * arraySize[0] + ii] = inArray[jj * arraySize[0] + ii];
                            }

                        }
                    }
                });
            }

            try {
                ConcurrencyUtils.waitForCompletion(futures);
            } catch (InterruptedException | ExecutionException ex) {
                throw new IllegalStateException(ex);
            }

//            for (int jj = 0; jj < (arraySize[1]); jj++) {
//                for (int ii = 0; ii < (arraySize[0]); ii++) {
//
//                }
//
//            }
        } else if ((inField.getDimNum()) == 3) {

            final int sStride = arraySize[1] * arraySize[0];
            final int rStride = arraySize[0];
            for (int j = 0; j < nthreads; j++) {
                final int firstIdx = j * k;
                final int lastIdx = (j == nthreads - 1) ? flatArrayLength : firstIdx + k;
                futures[j] = ConcurrencyUtils.submit(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        for (int i = firstIdx; i < lastIdx; i++) {
                            int kk = i / sStride;
                            int temp = i % sStride;
                            int jj = temp / rStride;
                            int ii = temp % rStride;

                            if ((kk != 0) && (jj != 0) && (ii != 0) && (kk != (arraySize[2] - 1)) && (jj != (arraySize[1] - 1)) && (ii != (arraySize[0] - 1))) {
                                float inArrayNablaUp = inArray[(kk + 1) * arraySize[1] * arraySize[0] + jj * arraySize[0] + ii] - inArray[kk * arraySize[1] * arraySize[0] + jj * arraySize[0] + ii];
                                float inArrayNablaDown = inArray[(kk - 1) * arraySize[1] * arraySize[0] + jj * arraySize[0] + ii] - inArray[kk * arraySize[1] * arraySize[0] + jj * arraySize[0] + ii];
                                float inArrayNablaS = inArray[kk * arraySize[1] * arraySize[0] + (jj - 1) * arraySize[0] + ii] - inArray[kk * arraySize[1] * arraySize[0] + jj * arraySize[0] + ii];
                                float inArrayNablaN = inArray[kk * arraySize[1] * arraySize[0] + (jj + 1) * arraySize[0] + ii] - inArray[kk * arraySize[1] * arraySize[0] + jj * arraySize[0] + ii];
                                float inArrayNablaE = inArray[kk * arraySize[1] * arraySize[0] + jj * arraySize[0] + ii + 1] - inArray[kk * arraySize[1] * arraySize[0] + jj * arraySize[0] + ii];
                                float inArrayNablaW = inArray[kk * arraySize[1] * arraySize[0] + jj * arraySize[0] + ii - 1] - inArray[kk * arraySize[1] * arraySize[0] + jj * arraySize[0] + ii];

                                float cCoeffUp = g1Function((float) abs(nablaWeightIndexK*inArrayNablaUp), K);
                                float cCoeffDown = g1Function((float) abs(nablaWeightIndexK*inArrayNablaDown), K);
                                float cCoeffN = g1Function((float) abs(nablaWeightIndexJ*inArrayNablaN), K);
                                float cCoeffS = g1Function((float) abs(nablaWeightIndexJ*inArrayNablaS), K);
                                float cCoeffE = g1Function((float) abs(nablaWeightIndexI*inArrayNablaE), K);
                                float cCoeffW = g1Function((float) abs(nablaWeightIndexI*inArrayNablaW), K);

                                outArray[kk * arraySize[1] * arraySize[0] + jj * arraySize[0] + ii] = inArray[kk * arraySize[1] * arraySize[0] + jj * arraySize[0] + ii] + 
                                        lambda0 * (cCoeffUp * nablaWeightSquaredIndexK*inArrayNablaUp + cCoeffDown * nablaWeightSquaredIndexK*inArrayNablaDown + 
                                                   cCoeffN * nablaWeightSquaredIndexJ*inArrayNablaN + cCoeffS * nablaWeightSquaredIndexJ*inArrayNablaS + 
                                                   cCoeffE * nablaWeightSquaredIndexI*inArrayNablaE + cCoeffW * nablaWeightSquaredIndexI*inArrayNablaW);
                            } else {
                                outArray[kk * arraySize[1] * arraySize[0] + jj * arraySize[0] + ii] = inArray[kk * arraySize[1] * arraySize[0] + jj * arraySize[0] + ii];
                            }

                        }
                    }
                });
            }

            try {
                ConcurrencyUtils.waitForCompletion(futures);
            } catch (InterruptedException | ExecutionException ex) {
                throw new IllegalStateException(ex);
            }

//            for (int kk = 0; kk < arraySize[2]; kk++) {
//                for (int jj = 0; jj < arraySize[1]; jj++) {
//                    for (int ii = 0; ii < arraySize[0]; ii++) {
//                        
//                    }
//                }
//            }
        } else {
            LOGGER.error("something is wrong with DimNum.");
        }

        if (outDataArray != null) {
            outDataArray.recomputeStatistics();
        }
    }

    public float g1Function(float positiveNumber, float K)
    {
//        float K = params.getParameterK();
        float gFunctionValue = 0;
        if (positiveNumber < 0) {
            LOGGER.error("Negative argument of g() !!!");
        } else {
            gFunctionValue = (float) (exp(-pow(positiveNumber / K, 2.0)));

        }
        return gFunctionValue;
    }

    public void calculateAnisotropicDiffusion(int iterationNumber, float lambda0, float K, float nablaWeightIndexI, float nablaWeightIndexJ, float nablaWeightIndexK, ProgressAgent progressAgent)
    {
        LOGGER.info("calculateAnisotropicDiffusion() is being executed");
        DataArray tmpDataArray = null;
        String inComponentName = inDataArray.getName();
        String outComponentName = outDataArray.getName();
        //    int iterationNumber = params.getIterationNumber();
        int iterationNumberThreshold = 200;
        int progressInterval = iterationNumber / 100; // used when iterationNumber >= iterationNumberThreshold 
        for (int i = 0; i < iterationNumber; i++) {
            incrementTIndex(lambda0, K, nablaWeightIndexI, nablaWeightIndexJ, nablaWeightIndexK);

            if (iterationNumber < iterationNumberThreshold) {
                progressAgent.increase(); // 100% corresponds to "all components processed"
                LOGGER.info(" percentage of completed iterations for the current component: " + ((i + 1) * 100 / iterationNumber) + "%"); // of the current component
            } else {
                if ((i + 1) % progressInterval == 0) {
                    progressAgent.increase(progressInterval); // 100% corresponds to "all components processed"
                    LOGGER.info(" percentage of completed iterations for the current component: " + ((i + 1) * 100 / iterationNumber) + "%"); // of the current component
                }

            }

            if (i < (iterationNumber - 1)) {

                /* references exchange
                 inDataArray -> outDataArray 
                 outDataArray -> inDataArray*/
                tmpDataArray = inDataArray;
                inDataArray = outDataArray;
                outDataArray = tmpDataArray;
            }

        }
        inDataArray.setName(inComponentName + "_duringLastIteration");
        outDataArray.setName(outComponentName);

    }

}
