/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.AnisotropicDenoiser;

import org.apache.log4j.Logger;

import java.awt.BorderLayout;
import java.util.Arrays;

import javax.swing.JPanel;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import org.visnow.vn.engine.core.ParameterProxy;

import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.gui.widgets.RunButton;
import org.visnow.vn.gui.widgets.SteppedComboBox;

import static org.visnow.vn.lib.basic.filters.AnisotropicDenoiser.AnisotropicDenoiserShared.*;
import static org.visnow.vn.lib.basic.filters.AnisotropicDenoiser.AnisotropicDenoiserShared.Method.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 *
 * Modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl), University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling,
 * revisions above 245.
 */
public class GUI extends JPanel
{
    private static final Logger LOGGER = Logger.getLogger(AnisotropicDenoiser.class);

    boolean componentChooserTableActive = false;

    private ParameterProxy parameterProxy;

    String[] scalarComponentNamesForComboBox = new String[]{""};
    String[] scalarAnisotropyNamesForComboBox = new String[]{""};

    boolean slopeSliderActive = true;
    boolean weightsOnlyActive = true;
    private boolean isGradientChangeByUser = false;

    /*
     * Creates new form InterpolateFieldUI
     */
    public GUI()
    {
        initComponents();

        final JTableHeader header = componentChooserTable.getTableHeader();
        componentChooserPanel.add(header, BorderLayout.NORTH);

        final DefaultTableModel dtm = (DefaultTableModel) componentChooserTable.getModel();

        dtm.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                if (componentChooserTableActive) {
                    componentChooserTableActive = false;

                    if (!dtm.getValueAt(dtm.getRowCount() - 1, 1).equals("")) {
                        dtm.setValueAt(scalarComponentNamesForComboBox[1], dtm.getRowCount() - 1, 0);
                    } else if (!dtm.getValueAt(dtm.getRowCount() - 1, 0).equals("")) {
                        dtm.setValueAt(scalarAnisotropyNamesForComboBox[1], dtm.getRowCount() - 1, 1);
                    }

                    for (int i = dtm.getRowCount() - 1; i >= 0; i--) {
                        if (dtm.getValueAt(i, 0).equals("") || dtm.getValueAt(i, 1).equals("")) {
                            dtm.removeRow(i);
                        }
                    }

                    if (dtm.getRowCount() == 0) {
                        dtm.addRow(new Object[]{scalarComponentNamesForComboBox[1], scalarAnisotropyNamesForComboBox[1]});
                        dtm.addRow(new Object[]{"", ""});
                    }

                    if (!dtm.getValueAt(dtm.getRowCount() - 1, 0).equals("")) {
                        dtm.addRow(new Object[]{"", ""});
                    }

                    // Set parameters.
                    int[][] components = new int[dtm.getRowCount() - 1][2];
                    for (int i = 0; i < dtm.getRowCount() - 1; ++i) {
                        components[i][0] = Arrays.asList(parameterProxy.get(META_INPUT_FIELD_COMPONENT_NAMES)).indexOf(dtm.getValueAt(i, 0));
                        components[i][1] = Arrays.asList(parameterProxy.get(META_ANISOTROPY_FIELD_COMPONENT_NAMES)).indexOf(dtm.getValueAt(i, 1));
                    }
                    //parameters.set(COMPONENTS, components);

                    double[][] componentExtents = parameterProxy.get(META_COMPONENT_EXTENTS);
                    if (!isGradientChangeByUser)
                        gradientSlopeSlider.setValue(componentExtents[components[0][0]][0] - componentExtents[components[0][0]][1]);
                    parameterProxy.set(COMPONENTS, components, SLOPE1, (float) gradientSlopeSlider.getValue());

                    componentChooserTableActive = true;
                }
            }
        });

        componentChooserTable.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(new JComboBox(new DefaultComboBoxModel(scalarComponentNamesForComboBox))));
        componentChooserTable.getColumnModel().getColumn(1).setCellEditor(new DefaultCellEditor(new JComboBox(new DefaultComboBoxModel(scalarAnisotropyNamesForComboBox))));

        dtm.addRow(new Object[]{"", ""});

        componentChooserTable.getTableHeader().setResizingAllowed(true);
        componentChooserTable.getTableHeader().setReorderingAllowed(false);

        componentChooserTableActive = true;

        resourceSelector.setEnabled(false);
        if (!VisNow.hasCudaDevice()) {
            resourceSelector.setEnabled(false);
        } else {
            resourceSelector.setEnabled(true);
            resourceSelector.setSelectedItem(Resource.GPU_CUDA);
        }
    }

    public void setParameterProxy(ParameterProxy targetParameterProxy)
    {
        //putting run button between logic and gui
        parameterProxy = runButton.getPlainProxy(targetParameterProxy, RUNNING_MESSAGE);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        anisotropyPanel = new javax.swing.JPanel();
        presmoothIndicator = new javax.swing.JLabel();
        presmoothDiameterSpinner = new javax.swing.JSpinner();
        componentChooserPanel = new javax.swing.JPanel();
        componentChooserTable = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        averagePresmoothButton = new javax.swing.JRadioButton();
        medianPresmoothButton = new javax.swing.JRadioButton();
        algorithmPanel = new javax.swing.JPanel();
        radiusSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        slopeSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        gradientSlopeSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        methodSelector = new javax.swing.JComboBox();
        iterationsPanel = new javax.swing.JPanel();
        iterationsLabel = new javax.swing.JLabel();
        iterationsField = new org.visnow.vn.gui.components.NumericTextField();
        weightsPanel = new javax.swing.JPanel();
        weightsOnlyCheckBox = new javax.swing.JCheckBox();
        weightsCheckBox = new javax.swing.JCheckBox();
        resourceSelector = new org.visnow.vn.gui.widgets.WidePopupComboBox();
        runButton = new org.visnow.vn.gui.widgets.RunButton();
        filler = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        smoothBySlicesBox = new javax.swing.JCheckBox();

        setLayout(new java.awt.GridBagLayout());

        anisotropyPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Anisotropy field"));
        anisotropyPanel.setLayout(new java.awt.GridBagLayout());

        presmoothIndicator.setText("presmoothing: radius");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.8;
        anisotropyPanel.add(presmoothIndicator, gridBagConstraints);

        presmoothDiameterSpinner.setModel(new javax.swing.SpinnerNumberModel(5, 1, 50, 1));
        presmoothDiameterSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                presmoothDiameterSpinnerStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        anisotropyPanel.add(presmoothDiameterSpinner, gridBagConstraints);

        componentChooserPanel.setLayout(new java.awt.BorderLayout());

        componentChooserTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "components", "anisotropy"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        componentChooserTable.getTableHeader().setResizingAllowed(false);
        componentChooserTable.getTableHeader().setReorderingAllowed(false);
        componentChooserPanel.add(componentChooserTable, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 4, 0);
        anisotropyPanel.add(componentChooserPanel, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridLayout(2, 1));

        buttonGroup1.add(averagePresmoothButton);
        averagePresmoothButton.setSelected(true);
        averagePresmoothButton.setText("gauss weighted average");
        averagePresmoothButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                averagePresmoothButtonActionPerformed(evt);
            }
        });
        jPanel1.add(averagePresmoothButton);

        buttonGroup1.add(medianPresmoothButton);
        medianPresmoothButton.setText("gauss weighted median");
        medianPresmoothButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                medianPresmoothButtonActionPerformed(evt);
            }
        });
        jPanel1.add(medianPresmoothButton);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        anisotropyPanel.add(jPanel1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        add(anisotropyPanel, gridBagConstraints);

        algorithmPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Algorithm properties"));
        algorithmPanel.setLayout(new java.awt.GridBagLayout());

        radiusSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        radiusSlider.setGlobalMin(1);
        radiusSlider.setMax(30);
        radiusSlider.setMin(1);
        radiusSlider.setSubmitOnAdjusting(false);
        radiusSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("radius"));
        radiusSlider.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                radiusSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        algorithmPanel.add(radiusSlider, gridBagConstraints);

        slopeSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        slopeSlider.setMax(500);
        slopeSlider.setMin(0.1);
        slopeSlider.setScaleType(org.visnow.vn.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        slopeSlider.setShowingFields(true);
        slopeSlider.setSubmitOnAdjusting(false);
        slopeSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("width"));
        slopeSlider.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                slopeSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        algorithmPanel.add(slopeSlider, gridBagConstraints);

        gradientSlopeSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        gradientSlopeSlider.setMax(50000);
        gradientSlopeSlider.setMin(0.001);
        gradientSlopeSlider.setScaleType(org.visnow.vn.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        gradientSlopeSlider.setShowingFields(true);
        gradientSlopeSlider.setSubmitOnAdjusting(false);
        gradientSlopeSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("gradient direction width"));
        gradientSlopeSlider.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                gradientSlopeSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        algorithmPanel.add(gradientSlopeSlider, gridBagConstraints);

        methodSelector.setModel(new DefaultComboBoxModel(Method.values()));
        methodSelector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                methodSelectorActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 0);
        algorithmPanel.add(methodSelector, gridBagConstraints);

        iterationsPanel.setLayout(new java.awt.GridBagLayout());

        iterationsLabel.setText("number of iterations");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.6;
        iterationsPanel.add(iterationsLabel, gridBagConstraints);

        iterationsField.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        iterationsField.setMin(1);
        iterationsField.setText("1");
        iterationsField.setHorizontalAlignment(11);
        iterationsField.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                iterationsFieldUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 0.4;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        iterationsPanel.add(iterationsField, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        algorithmPanel.add(iterationsPanel, gridBagConstraints);

        weightsPanel.setLayout(new java.awt.GridBagLayout());

        weightsOnlyCheckBox.setText("only weights");
        weightsOnlyCheckBox.setEnabled(false);
        weightsOnlyCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                weightsOnlyCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        weightsPanel.add(weightsOnlyCheckBox, gridBagConstraints);

        weightsCheckBox.setText("output weights");
        weightsCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                weightsCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        weightsPanel.add(weightsCheckBox, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        algorithmPanel.add(weightsPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        add(algorithmPanel, gridBagConstraints);

        resourceSelector.setListData(Resource.values());
        resourceSelector.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                resourceSelectorUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 12, 0, 7);
        add(resourceSelector, gridBagConstraints);

        runButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                runButtonUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 8);
        add(runButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.weighty = 1.0;
        add(filler, gridBagConstraints);

        smoothBySlicesBox.setText("smooth by slices");
        smoothBySlicesBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                smoothBySlicesBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(smoothBySlicesBox, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void presmoothDiameterSpinnerStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_presmoothDiameterSpinnerStateChanged
    {//GEN-HEADEREND:event_presmoothDiameterSpinnerStateChanged
        parameterProxy.set(PRESMOOTH_RADIUS, (Integer) presmoothDiameterSpinner.getValue());
    }//GEN-LAST:event_presmoothDiameterSpinnerStateChanged

    private void methodSelectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_methodSelectorActionPerformed
        parameterProxy.set(METHOD, (Method) methodSelector.getSelectedItem());
    }//GEN-LAST:event_methodSelectorActionPerformed

    private void slopeSliderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_slopeSliderUserChangeAction
        if (slopeSliderActive)
            parameterProxy.set(SLOPE, (Float) slopeSlider.getValue());
    }//GEN-LAST:event_slopeSliderUserChangeAction

    private void gradientSlopeSliderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_gradientSlopeSliderUserChangeAction
        parameterProxy.set(SLOPE1, (Float) gradientSlopeSlider.getValue());
        isGradientChangeByUser = true;
    }//GEN-LAST:event_gradientSlopeSliderUserChangeAction

    private void radiusSliderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_radiusSliderUserChangeAction
        parameterProxy.set(RADIUS, (Integer) radiusSlider.getValue(), SLOPE, radiusSlider.getValue().floatValue());
        slopeSliderActive = false;
        slopeSlider.setValue(radiusSlider.getValue().floatValue());
        slopeSliderActive = true;
    }//GEN-LAST:event_radiusSliderUserChangeAction

    private void runButtonUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_runButtonUserChangeAction
        parameterProxy.set(RUNNING_MESSAGE, (RunButton.RunState) evt.getEventData());
    }//GEN-LAST:event_runButtonUserChangeAction

    private void iterationsFieldUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_iterationsFieldUserChangeAction
        parameterProxy.set(ITERATIONS, (Integer) iterationsField.getValue());
    }//GEN-LAST:event_iterationsFieldUserChangeAction

    private void resourceSelectorUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_resourceSelectorUserChangeAction
        parameterProxy.set(RESOURCE, (Resource) resourceSelector.getSelectedItem());
    }//GEN-LAST:event_resourceSelectorUserChangeAction

    private void weightsCheckBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_weightsCheckBoxActionPerformed
    {//GEN-HEADEREND:event_weightsCheckBoxActionPerformed
        parameterProxy.set(COMPUTE_WEIGHTS, weightsCheckBox.isSelected(), COMPUTE_ONLY_WEIGHTS, false);
        weightsOnlyActive = false;
        if (!weightsCheckBox.isSelected()) {
            weightsOnlyCheckBox.setSelected(false);
        }
        weightsOnlyCheckBox.setEnabled(weightsCheckBox.isSelected());
        weightsOnlyActive = true;
    }//GEN-LAST:event_weightsCheckBoxActionPerformed

    private void weightsOnlyCheckBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_weightsOnlyCheckBoxActionPerformed
    {//GEN-HEADEREND:event_weightsOnlyCheckBoxActionPerformed
        if (weightsOnlyActive) {
            parameterProxy.set(COMPUTE_ONLY_WEIGHTS, weightsOnlyCheckBox.isSelected());
        }
    }//GEN-LAST:event_weightsOnlyCheckBoxActionPerformed

    private void smoothBySlicesBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_smoothBySlicesBoxActionPerformed
    {//GEN-HEADEREND:event_smoothBySlicesBoxActionPerformed
        parameterProxy.set(BY_SLICE, smoothBySlicesBox.isSelected());
    }//GEN-LAST:event_smoothBySlicesBoxActionPerformed

    private void averagePresmoothButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_averagePresmoothButtonActionPerformed
    {//GEN-HEADEREND:event_averagePresmoothButtonActionPerformed
        presmoothMethod();
    }//GEN-LAST:event_averagePresmoothButtonActionPerformed

    private void medianPresmoothButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_medianPresmoothButtonActionPerformed
    {//GEN-HEADEREND:event_medianPresmoothButtonActionPerformed
        presmoothMethod();
    }//GEN-LAST:event_medianPresmoothButtonActionPerformed

    private void presmoothMethod()
    {
        parameterProxy.set(PRESMOOTH_METHOD, averagePresmoothButton.isSelected() ? AVERAGE : MEDIAN);
    }

    void update(ParameterProxy p, boolean resetFully, boolean setRunButtonPending)
    {
        // No full resets should be needed.
        // Maybe only for presmoothing.
        resourceSelector.setSelectedItem(p.get(RESOURCE));
        methodSelector.setSelectedItem(p.get(METHOD));

        // One won't mind a little clean-up here.
        presmoothIndicator.setEnabled(true);
        presmoothDiameterSpinner.setEnabled(true);
        if (p.get(PRESMOOTH)) {
            presmoothIndicator.setEnabled(true);
            presmoothDiameterSpinner.setValue(p.get(PRESMOOTH_RADIUS));
        } else {
            presmoothIndicator.setEnabled(false);
            presmoothDiameterSpinner.setEnabled(false);
        }
        if(resetFully)
            isGradientChangeByUser = false;

        radiusSlider.setValue(p.get(RADIUS));
        slopeSlider.setValue(p.get(SLOPE));
        gradientSlopeSlider.setValue(p.get(SLOPE1));

        iterationsField.setValue(p.get(ITERATIONS));

        weightsCheckBox.setSelected(p.get(COMPUTE_WEIGHTS));

        // Table update.
        updateComponentTable(p.get(COMPONENTS),
                             p.get(META_INPUT_FIELD_SCALAR_COMPONENT_NAMES),
                             p.get(META_ANISOTROPY_FIELD_SCALAR_COMPONENT_NAMES));

        runButton.updateAutoState(p.get(RUNNING_MESSAGE));
        runButton.updatePendingState(setRunButtonPending);
    }

    private void updateComponentTable(int[][] components, String[] inputComponentNames, String[] anisotropyComponentNames)
    {
        scalarComponentNamesForComboBox = new String[1 + inputComponentNames.length];
        scalarComponentNamesForComboBox[0] = "";
        for (int i = 0; i < inputComponentNames.length; ++i) {
            scalarComponentNamesForComboBox[i + 1] = inputComponentNames[i];
        }
        setupComponentColumn(scalarComponentNamesForComboBox, componentChooserTable.getColumnModel().getColumn(0));

        scalarAnisotropyNamesForComboBox = new String[1 + anisotropyComponentNames.length];
        scalarAnisotropyNamesForComboBox[0] = "";
        for (int i = 0; i < anisotropyComponentNames.length; ++i) {
            scalarAnisotropyNamesForComboBox[i + 1] = anisotropyComponentNames[i];
        }
        setupComponentColumn(scalarAnisotropyNamesForComboBox, componentChooserTable.getColumnModel().getColumn(1));

        // Clean the model, update with new data.
        final DefaultTableModel dtm = (DefaultTableModel) componentChooserTable.getModel();

        componentChooserTableActive = false;
        String[] componentNames = parameterProxy.get(META_INPUT_FIELD_COMPONENT_NAMES);
        String[] anisotropyNames = parameterProxy.get(META_ANISOTROPY_FIELD_COMPONENT_NAMES);
        dtm.setRowCount(0);
        for (int i = 0; i < components.length; ++i) {
            dtm.addRow(new Object[]{componentNames[components[i][0]], anisotropyNames[components[i][1]]});
        }
        dtm.addRow(new Object[]{"", ""});
        componentChooserTableActive = true;
    }

    private void setupComponentColumn(String[] names, TableColumn componentColumn)
    {
        SteppedComboBox comboBox = new SteppedComboBox();
        comboBox.setModel(new DefaultComboBoxModel(names));

        componentColumn.setCellEditor(new DefaultCellEditor(comboBox));
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();

        renderer.setToolTipText("Click for components");

        componentColumn.setCellRenderer(renderer);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel algorithmPanel;
    private javax.swing.JPanel anisotropyPanel;
    private javax.swing.JRadioButton averagePresmoothButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JPanel componentChooserPanel;
    private javax.swing.JTable componentChooserTable;
    private javax.swing.Box.Filler filler;
    private org.visnow.vn.gui.widgets.ExtendedSlider gradientSlopeSlider;
    private org.visnow.vn.gui.components.NumericTextField iterationsField;
    private javax.swing.JLabel iterationsLabel;
    private javax.swing.JPanel iterationsPanel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton medianPresmoothButton;
    private javax.swing.JComboBox methodSelector;
    private javax.swing.JSpinner presmoothDiameterSpinner;
    private javax.swing.JLabel presmoothIndicator;
    private org.visnow.vn.gui.widgets.ExtendedSlider radiusSlider;
    private org.visnow.vn.gui.widgets.WidePopupComboBox resourceSelector;
    private org.visnow.vn.gui.widgets.RunButton runButton;
    private org.visnow.vn.gui.widgets.ExtendedSlider slopeSlider;
    private javax.swing.JCheckBox smoothBySlicesBox;
    private javax.swing.JCheckBox weightsCheckBox;
    private javax.swing.JCheckBox weightsOnlyCheckBox;
    private javax.swing.JPanel weightsPanel;
    // End of variables declaration//GEN-END:variables
}
