/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.AnisotropicDenoiser;

import org.visnow.vn.engine.core.ParameterName;

import org.visnow.vn.gui.widgets.RunButton;

/**
 * 
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl), University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */

public class AnisotropicDenoiserShared
{
    enum Resource
    {
        CPU("CPU"),
        GPU_CUDA("GPU (Nvidia CUDA)");

        private String name;

        private Resource(String name)
        {
            this.name = name;
        }

        @Override
        public String toString()
        {
            return name;
        }
    };
    
    enum Method {
        AVERAGE("weighted average"),
        MEDIAN("weighted median");
        
        private String name;

        private Method(String name)
        {
            this.name = name;
        }

        @Override
        public String toString()
        {
            return name;
        }
    };

    /* Old "Params" class featured following parameters: */
    
    //new ParameterEgg<Integer>(METHOD, ParameterType.dependent, AVERAGE)
    //new ParameterEgg<CoreType>(CORE, ParameterType.independent, CoreType.CPU)
    //new ParameterEgg<Integer>(NTHREADS, ParameterType.independent, null)           // Depracated.
    //new ParameterEgg<int[][]>(COMPONENTS, ParameterType.dependent, new int[][]{{0, 0}})
    //new ParameterEgg<Integer>(RADIUS, ParameterType.dependent, 2)
    //new ParameterEgg<Float>(SLOPE, ParameterType.dependent, 2.f)
    //new ParameterEgg<Float>(SLOPE1, ParameterType.dependent, 4.f)
    //new ParameterEgg<Boolean>(NATIVE, ParameterType.independent, false)            // Not used.
    //new ParameterEgg<Integer>(ITERATIONS, ParameterType.dependent, 1)
    //new ParameterEgg<Integer>(PRESMOOTH_RADIUS, ParameterType.dependent, 5)    
    //new ParameterEgg<Boolean>(COMPUTE_SIGMA, ParameterType.independent, false)     // Not used.
    //new ParameterEgg<Boolean>(NORMALIZE_SIGMA, ParameterType.independent, false)   // Not used.
    //new ParameterEgg<Boolean>(PRESMOOTH, ParameterType.independent, false)
    //new ParameterEgg<Boolean>(COMPUTE_WEIGHTS, ParameterType.independent, false)
    //new ParameterEgg<Boolean>(COMPUTE_BY_SLICE, ParameterType.independent, false)  // Not used.
    //new ParameterEgg<Boolean>(COMPUTE, ParameterType.independent, false)           // Depracated, changed to RUNNING_MESSAGE.

    // New "Parameters":
    
    // Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    // Specification of a parameter above declaration.
    static final ParameterName<Boolean>  BY_SLICE = new ParameterName<>("By slice");
    
    static final ParameterName<Resource> RESOURCE = new ParameterName<>("Resource");
    static final ParameterName<Method>   METHOD = new ParameterName<>("Method");

    static final ParameterName<Boolean>  PRESMOOTH = new ParameterName<>("Presmooth");
    static final ParameterName<Method>   PRESMOOTH_METHOD = new ParameterName<>("Presmooth method");
    static final ParameterName<Integer>  PRESMOOTH_RADIUS = new ParameterName<>("Radius (presmooth)");

    static final ParameterName<Integer>  RADIUS = new ParameterName<>("Radius (actual denoise)"); // TODO. Change to float.
    static final ParameterName<Float>    SLOPE = new ParameterName<>("Slope (alpha)");
    static final ParameterName<Float>    SLOPE1 = new ParameterName<>("Slope1 (beta)");
    static final ParameterName<Integer>  ITERATIONS = new ParameterName<>("Iterations");
   
    static final ParameterName<int[][]>  COMPONENTS = new ParameterName<>("Components (input and anisotropy)");

    static final ParameterName<Boolean>  COMPUTE_WEIGHTS = new ParameterName<>("Output weights");
    static final ParameterName<Boolean>  COMPUTE_ONLY_WEIGHTS = new ParameterName<>("Output weights only");
    
    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");
    
    // Meta-parameters, needed by GUI to set up.
    static final ParameterName<String[]> META_INPUT_FIELD_COMPONENT_NAMES = new ParameterName<>("(META) Input component names (all)");
    static final ParameterName<String[]> META_ANISOTROPY_FIELD_COMPONENT_NAMES = new ParameterName<>("(META) Anisotropy component names (all)");
    
    static final ParameterName<String[]> META_INPUT_FIELD_SCALAR_COMPONENT_NAMES = new ParameterName<>("(META) Input scalar component names");
    static final ParameterName<String[]> META_ANISOTROPY_FIELD_SCALAR_COMPONENT_NAMES = new ParameterName<>("(META) Anisotropy scalar component names");
    
    static final ParameterName<double[][]> META_COMPONENT_EXTENTS = new ParameterName<>("(META) Components extents");
}
