/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.SplineInterpolation;

import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton;

/**
 * 
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 */

public class SplineInterpolationShared
{   
    static enum Type {
        DENSITY, NEW_DIMS, NEW_CELL_SIZE;
    }
    
    // Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    // Specification of a parameter above declaration.
   
    // ...
    static final ParameterName<Type> TYPE = new ParameterName("Type of interpolation");
    
    static final ParameterName<Integer> DENSITY_TYPE_PARAMETER = new ParameterName("Density parameter");
 
    static final ParameterName<int[]> NEW_DIMS_TYPE_PARAMETER = new ParameterName("New dims");
    
    // A floating point number, greater than zero.
    static final ParameterName<Float> NEW_CELL_SIZE_TYPE_PARAMETER = new ParameterName("New cell size");
    // Table of ints of length 0...<number of components of input field>; each element in range 0...<number of components of input field>,
    // elements of array must be distinct.
    static final ParameterName<int[]> COMPONENTS = new ParameterName("Components (indices)");    
   
    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName("Running message");
    
    // Meta-parameters, mostly needed by the UI.
    static final ParameterName<Float> META_MAX_CELL_SIZE = new ParameterName("Max cell size, so that output field does not become trivial (eg. 1x1x1)");
    static final ParameterName<int[]> META_INPUT_FIELD_DIMS = new ParameterName("InField dims");
    static final ParameterName<Double> META_INPUT_FIELD_CELL_VOLUME = new ParameterName("InField cell volume");
    static final ParameterName<double[]> META_INPUT_FIELD_AFFINE_NORM = new ParameterName("InField affine norm");
    static final ParameterName<Boolean> META_INPUT_FIELD_IS_GEOMETRY_AFFINE = new ParameterName("InField affine/coords flag");
    static final ParameterName<String[]> META_COMPONENTS_NAMES = new ParameterName("InField components (names)");
}