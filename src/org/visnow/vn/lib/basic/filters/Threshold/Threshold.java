/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.Threshold;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Threshold extends ModuleCore
{

    private GUI ui = null;
    private Core core = new Core();
    protected Params params;
    Field inField = null;
    private boolean fromGUI = false;

    public Threshold()
    {
        parameters = params = new Params();
        core.setParams(params);
        params.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                fromGUI = true;
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                ui = new GUI();
            }
        });
        ui.setParams(params);
        setPanel(ui);
    }

    @Override
    public void onActive()
    {
        if(!fromGUI) {
            if (getInputFirstValue("inField") == null)
                return;
            Field inFld = ((VNField) getInputFirstValue("inField")).getField();
            if (inFld == null)
                return;
            if (inFld != inField) {
                inField = inFld;
                ui.setInField(inField);
                core.setInField(inField);
            }
        } else {
            core.update();
            Field outField = core.getOutField();
            if (outField != null && outField instanceof RegularField) {
                setOutputValue("outRegularField", new VNRegularField((RegularField) outField));
                setOutputValue("outIrregularField", null);
            } else if (outField != null && outField instanceof IrregularField) {
                setOutputValue("outRegularField", null);
                setOutputValue("outIrregularField", new VNIrregularField((IrregularField) outField));
            } else {
                setOutputValue("outRegularField", null);
                setOutputValue("outIrregularField", null);
            }
        }
        fromGUI = false;

    }

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

}
