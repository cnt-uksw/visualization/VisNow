/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.Threshold;

import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Params extends Parameters
{

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Float>("lowerThreshold", ParameterType.dependent, 0.f),
        new ParameterEgg<Float>("upperThreshold", ParameterType.dependent, 1.f),
        new ParameterEgg<Integer>("component", ParameterType.dependent, 0),
        new ParameterEgg<Boolean>("setLowToMin", ParameterType.dependent, false),
        new ParameterEgg<Boolean>("invalidate", ParameterType.dependent, false),
        new ParameterEgg<Boolean>("setUpToMax", ParameterType.dependent, false)
    };

    public Params()
    {
        super(eggs);
    }

    public int getComponent()
    {
        return (int) getValue("component");
    }

    public void setComponent(int component)
    {
        setValue("component", component);
    }

    public float getLowerThreshold()
    {
        return (Float) getValue("lowerThreshold");
    }

    public void setLowerThreshold(float lowerThreshold)
    {
        setValue("lowerThreshold", lowerThreshold);
    }

    public float getUpperThreshold()
    {
        return (Float) getValue("upperThreshold");
    }

    public void setUpperThreshold(float upperThreshold)
    {
        setValue("upperThreshold", upperThreshold);
    }

    public boolean isLowToMin()
    {
        return (Boolean) getValue("setLowToMin");
    }

    public void setLowToMin(boolean setLowToMin)
    {
        setValue("setLowToMin", setLowToMin);
    }

    public boolean isUpToMax()
    {
        return (Boolean) getValue("setUpToMax");
    }

    public void setUpToMax(boolean setUpToMax)
    {
        setValue("setUpToMax", setUpToMax);
    }

    public boolean isInvalidate()
    {
        return (Boolean) getValue("invalidate");
    }

    public void setInvalidate(boolean invalidate)
    {
        setValue("invalidate", invalidate);
    }

}
