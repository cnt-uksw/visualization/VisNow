/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.DropCoords;

import org.apache.log4j.Logger;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.filters.DropCoords.DropCoordsShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class DropCoords extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(DropCoords.class);
    
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    private GUI computeUI = null;
    protected RegularField inField = null;

    public DropCoords()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(ORIGIN, new float[]{0.f, 0.f, 0.f}),
            new Parameter<>(CELL_SIZES, new float[]{1.f, 1.f, 1.f}),
            new Parameter<>(META_FIELD_RANK, 1)
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        parameters.set(META_FIELD_RANK, inField.getDimNum());
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        LOGGER.debug("fromVNA: " + isFromVNA());
        
        if (getInputFirstValue("inField") != null) {
            inField = ((VNRegularField) getInputFirstValue("inField")).getField();
            Parameters p;
            synchronized (parameters) {
                validateParamsAndSetSmart(false);
                p = parameters.getReadOnlyClone();
            }
            notifyGUIs(p, false, false);

            outRegularField = inField.cloneShallow();
            outRegularField.removeCoords();
            outField = outRegularField;

            float[][] affine = new float[4][3];
            for (int i = 0; i < affine.length; i++)
                for (int j = 0; j < affine[0].length; j++)
                    affine[i][j] = 0;
            float[] cell_extends = parameters.get(CELL_SIZES);
            float[] origin = parameters.get(ORIGIN);
            for (int i = 0; i < cell_extends.length; i++) {
                affine[i][i] = cell_extends[i];
                affine[3][i] = origin[i];
            }
            outRegularField.setAffine(affine);
            setOutputValue("outField", new VNRegularField(outRegularField));
            outField = outRegularField;
            prepareOutputGeometry();
            show();
        }
    }
}
