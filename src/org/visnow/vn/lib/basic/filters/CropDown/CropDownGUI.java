/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.CropDown;

import java.util.Arrays;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.apache.log4j.Logger;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.widgets.RunButton;
import static org.visnow.vn.lib.basic.filters.CropDown.CropDownShared.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class CropDownGUI extends JPanel
{

    private static final Logger LOGGER = Logger.getLogger(CropDownGUI.class);
    private Parameters parameters;

    //unfortunatelly this flag is necessary to distinguish user action on cropUI and downsizeUI from setter actions.
    private boolean subUIActive = false;

    /**
     * Creates new form CropDownUI
     */
    public CropDownGUI()
    {
        initComponents();
        cropUI.setDynamic(true);
        cropUI.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                LOGGER.debug(Arrays.toString(cropUI.getLow()) + "   " + Arrays.toString(cropUI.getUp()));
                int[] low = cropUI.getLow();
                int[] up = cropUI.getUp();
                for (int i = 0; i < up.length; i++)
                    if (up[i] - low[i] == 1) {
                        if (up[i] == parameters.get(META_FIELD_DIMENSIONS)[i]) low[i]--;
                        else up[i]++;
                    }

                parameters.set(LOW, low,
                               UP, up,
                               ADJUSTING, cropUI.isAdjusting());
                if (subUIActive) runButton.setPendingIfNoAuto();
            }
        });
        downsizeUI.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                parameters.set(DOWNSIZES, downsizeUI.getDownsize());
                if (subUIActive) runButton.setPendingIfNoAuto();
            }
        });
    }

    public void setParameters(Parameters parameters)
    {
        this.parameters = parameters;
    }

    void updateGUI(final ParameterProxy p, boolean resetFully, boolean setRunButtonPending)
    {
        subUIActive = false;

        final int[] downsizes = p.get(DOWNSIZES);
        final int[] dims = p.get(META_FIELD_DIMENSIONS);
        final int[] low = p.get(LOW);
        final int[] up = p.get(UP);
        int[] down = new int[dims.length];
        System.arraycopy(downsizes, 0, down, 0, down.length);

        if (dims.length > 0) {
            cropUI.setNewExtents(dims, low, up, resetFully);
            downsizeUI.setDownsize(down);
        }
        subUIActive = true;

        runButton.updateAutoState(p.get(RUNNING_MESSAGE));
        runButton.updatePendingState(setRunButtonPending);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        cropUI = new org.visnow.vn.lib.gui.cropUI.CropUI();
        downsizeUI = new org.visnow.vn.lib.gui.DownsizeUI();
        runButton = new org.visnow.vn.gui.widgets.RunButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(cropUI, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(downsizeUI, gridBagConstraints);

        runButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                runButtonUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        add(runButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weighty = 1.0;
        add(filler1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void runButtonUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_runButtonUserChangeAction
    {//GEN-HEADEREND:event_runButtonUserChangeAction
        parameters.set(RUNNING_MESSAGE, (RunButton.RunState) evt.getEventData());
    }//GEN-LAST:event_runButtonUserChangeAction


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public org.visnow.vn.lib.gui.cropUI.CropUI cropUI;
    public org.visnow.vn.lib.gui.DownsizeUI downsizeUI;
    public javax.swing.Box.Filler filler1;
    public org.visnow.vn.gui.widgets.RunButton runButton;
    // End of variables declaration//GEN-END:variables
    public static void main(String[] args)
    {
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.add(new CropDownGUI());
        f.pack();
        f.setVisible(true);

    }
}
