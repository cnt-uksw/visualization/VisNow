/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.CropDown;

import java.util.Arrays;
import org.apache.log4j.Logger;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.objects.CroppedRegularFieldOutline;
import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.filters.CropDown.CropDownShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.field.subset.FieldSample;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class CropDown extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(CropDown.class);
    
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    protected CropDownGUI computeUI = null;
    protected RegularField inField = null;
    protected final CroppedRegularFieldOutline outline = new CroppedRegularFieldOutline();
    private int runQueue = 0;

    public CropDown()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (parameters.get(ADJUSTING) && inField.getDims().length > 1) {
                    final int[] low = parameters.get(LOW);
                    final int[] up = parameters.get(UP);
                    new Thread(new Runnable()
                    {

                        @Override
                        public void run()
                        {
                            synchronized (outline) {
                                if (outline.getParent() == null)
                                    outObj.addNode(outline);
                                outline.setCrop(low, up);
                            }
                        }
                    }).start();
                } else {
                    if (name != null && name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                        runQueue++;
                        startAction();
                    } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY && !parameters.get(ADJUSTING))
                        startAction();
                }
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new CropDownGUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(LOW, new int[]{0, 0, 0}),
            new Parameter<>(UP, new int[]{2, 2, 2}),
            new Parameter<>(DOWNSIZES, new int[]{1, 1, 1}),
            new Parameter<>(ADJUSTING, false),
            new Parameter<>(META_FIELD_DIMENSIONS, new int[]{2, 2, 2}),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN)
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE) parameters.set(RUNNING_MESSAGE, NO_RUN);
        int[] dims = inField.getDims();
        parameters.set(META_FIELD_DIMENSIONS, dims);
        boolean invalidDims = false;
        switch (inField.getDims().length) {
            case 3:
                invalidDims = parameters.get(UP)[0] > dims[0] || parameters.get(UP)[1] > dims[1] || parameters.get(UP)[2] > dims[2];
                break;
            case 2:
                invalidDims = parameters.get(UP)[0] > dims[0] || parameters.get(UP)[1] > dims[1];
                break;
            case 1:
                invalidDims = parameters.get(UP)[0] > dims[0];
                break;
        }
        if (resetParameters || invalidDims) {
            int[] low = new int[3];
            int[] up = new int[3];
            for (int i = 0; i < inField.getDimNum(); i++) {
                up[i] = inField.getDims()[i];
            }
            parameters.set(LOW, low);
            parameters.set(UP, up);
        }

        int[] downsizes = parameters.get(DOWNSIZES);
        int[] low = parameters.get(LOW);
        int[] up = parameters.get(UP);

        if (!resetParameters) { //validate
            for (int i = 0; i < inField.getDimNum(); i++) {
                int maxDownsize = (up[i] - low[i]) / 2;
                if (downsizes[i] > maxDownsize)
                    if (downsizes[i] < maxDownsize + 2) //"smart" :(
                        downsizes[i] = maxDownsize;
                    else
                        downsizes[i] = Math.min(3, maxDownsize); //default (3) :(
            }
        } else { //set smart
            switch (inField.getDims().length) {
                case 3:
                    for (int i = 0; i < inField.getDimNum(); i++) {
                        downsizes[i] = (up[i] - low[i] + 99) / 100;
                    }
                    break;
                case 2:
                    for (int i = 0; i < inField.getDims().length; i++) {
                        downsizes[i] = (up[i] - low[i] + 999) / 1000;
                    }
                    break;
                case 1:
                    for (int i = 0; i < inField.getDims().length; i++) {
                        downsizes[i] = (up[i] - low[i] + 999999) / 1000000;
                    }
                    break;
            }
        }
        parameters.set(DOWNSIZES, downsizes);

        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive()
    {
        LOGGER.debug("isFromVNA = " + isFromVNA());
        
        if (getInputFirstValue("inField") != null) {
            RegularField newField = ((VNRegularField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA() && (inField == null || !Arrays.equals(inField.getDims(), newField.getDims()));
            boolean isNewField = !isFromVNA() && newField != inField;
            inField = newField;
            Parameters p;
            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentField);
                p = parameters.getReadOnlyClone();
            }

            notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

            synchronized (outline) {
                if (inField.getDims().length > 1) outline.setField(inField);
                outObj.removeNode(outline);
            }
            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data
                outField = outRegularField = FieldSample.regularSample(inField, p.get(DOWNSIZES), p.get(LOW), p.get(UP), true);
                setOutputValue("outField", new VNRegularField(outRegularField));
                outField = outRegularField;
                prepareOutputGeometry();
                show();
            }
        }
    }
}
