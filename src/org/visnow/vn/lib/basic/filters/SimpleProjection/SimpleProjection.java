/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.SimpleProjection;

import java.util.Arrays;
import org.apache.log4j.Logger;

import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;

import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;

import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

import org.visnow.vn.lib.basic.filters.SimpleProjection.SimpleProjectionShared.Axis;
import static org.visnow.vn.lib.basic.filters.SimpleProjection.SimpleProjectionShared.*;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 *
 * Modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl; University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling) since
 * revision 25.
 */
public class SimpleProjection extends OutFieldVisualizationModule {

    private static final Logger LOGGER = Logger.getLogger(SimpleProjection.class);

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    private GUI gui;

    protected RegularField inField = null;

    public SimpleProjection() {
        parameters.addParameterChangelistener(new ParameterChangeListener() {
            @Override
            public void parameterChanged(String name) {
                startIfNotInQueue();
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable() {
            @Override
            public void run() {
                gui = new GUI();
                gui.setParameters(parameters);
                ui.addComputeGUI(gui);
                setPanel(ui);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters() {
        return new Parameter[]{
            new Parameter<>(METHOD, "Maximum"),
            new Parameter<>(AXIS_L0, Axis.I),
            new Parameter<>(AXIS_L1, Axis.J),
            new Parameter<>(LEVELS, 1),
            new Parameter<>(META_NUMBER_OF_DIMENSIONS, 3)
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters) {
        parameters.setParameterActive(false);

        if (resetParameters) {
            parameters.set(METHOD, MAX);
            parameters.set(AXIS_L0, Axis.I);
            parameters.set(AXIS_L1, Axis.J);
            parameters.set(LEVELS, 1);
            parameters.set(META_NUMBER_OF_DIMENSIONS, 3);
        } else {
            int dimNum = inField.getDimNum();
            if (dimNum == 2) {
                parameters.set(META_NUMBER_OF_DIMENSIONS, 2);
                if (parameters.get(LEVELS) != 1) {
                    parameters.set(LEVELS, 1);
                }
            } else { // Assuming (dimNum == 3).
                parameters.set(META_NUMBER_OF_DIMENSIONS, 3);
                if (parameters.get(LEVELS) == 2) {
                    // i.e. (levels == 2)
                    if (parameters.get(AXIS_L0) == parameters.get(AXIS_L1)) {
                        // Change second level axis to something else.
                        switch (parameters.get(AXIS_L0)) {
                            case I:
                                parameters.set(AXIS_L1, Axis.J);
                                break;
                            case J:
                                parameters.set(AXIS_L1, Axis.K);
                                break;
                            default: // case K:
                                parameters.set(AXIS_L1, Axis.I);
                        }
                    }
                }
            }
        }
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending) {
        gui.updateGUI(clonedParameterProxy, resetFully);
    }

    private void compute(Parameters p) {
        // Read parameters (additional clone).
        int axis0 = p.get(AXIS_L0) == Axis.I ? 0 : p.get(AXIS_L0) == Axis.J ? 1 : 2;
        int axis1 = p.get(AXIS_L1) == Axis.I ? 0 : p.get(AXIS_L1) == Axis.J ? 1 : 2;
        int method = Arrays.asList(METHODS_NAMES).indexOf(p.get(METHOD));
        int levels = p.get(LEVELS);

        if (inField.getDims().length == 3) {
            if (levels == 1) {
                outRegularField = SimpleProjection3D.compute(inField, method, axis0);
            } else {
                outRegularField = SimpleProjection3D.compute(inField, method, axis0, axis1);
            }
        } else if (inField.getDims().length == 2) {
            outRegularField = SimpleProjection2D.compute(inField, method, axis0);
        }
    }

    @Override
    public void onActive() {
        LOGGER.debug("FromVNA: " + isFromVNA());

        if (getInputFirstValue("inField") != null) {
            // 1. Get new field.
            RegularField newInField = ((VNRegularField) getInputFirstValue("inField")).getField();
            // 1a. Set "Different Field" flag.
            boolean isDifferentField = !isFromVNA() && (inField == null || inField.getDimNum() != newInField.getDimNum());
            inField = newInField;

            // 2. Validate parameters.             
            Parameters p;
            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentField);
                // 2b. Clone parameters (local read-only copy).
                p = parameters.getReadOnlyClone();
            }
            // 3. Update GUI (GUI doesn't change parameters! Assuming correct set of parameters).
            notifyGUIs(p, isFromVNA() || isDifferentField, false);

            // 4. Run computation and propagate.
            compute(p);

            setOutputValue("outField", new VNRegularField(outRegularField));
            outField = outRegularField;

            prepareOutputGeometry();

            show();
        }
    }
}
