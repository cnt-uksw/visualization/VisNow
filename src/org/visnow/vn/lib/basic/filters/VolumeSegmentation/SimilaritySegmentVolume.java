/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.basic.filters.VolumeSegmentation;

import java.util.ArrayList;
import javax.swing.ProgressMonitor;
import org.visnow.jscic.RegularField;
import org.visnow.vn.lib.utils.FastIntQueue;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class SimilaritySegmentVolume extends SegmentVolume
{

    protected ArrayList<Integer> indices = null;
    protected short[][] data = null;
    protected float[] weights = null;
    /*
     * mask[i] = 1 for background,
     * mask[i] = 0 for voxels with unknown status
     * mask[i] = k>1 for k-throws segmented set
     */
    protected short tollerance = 300;
    protected boolean inside;
    protected int[][] framePoints = null;
    protected int segIndex = 2;

    /**
     * Holds value of property backgroundThreshold.
     */
    /**
     * Creates a new instance of SegmentVolume
     * <p>
     * @param inField   data to be segmented
     * @param distField for dbug purposes: map of similarity distances from the start points set
     * @param outField  output including byte indices of seg
     */
    public SimilaritySegmentVolume(RegularField inField, RegularField distField, RegularField outField)
    {
        dims = inField.getDims();
        off = inField.getPartNeighbOffsets();
        ndata = (int) inField.getNNodes();
        this.inField = inField;
        outd = (int[])distField.getComponent(0).getRawArray().getData();
        mask = (byte[])outField.getComponent(0).getRawArray().getData();
    }

    @Override
    public void computeDistance(ArrayList<int[]> initPoints, boolean[] allowed)
    {
        if (tollerance < 0 || allowed == null || initPoints.isEmpty())
            return;
        boolean isSomethingAllowed = false;
        for (int i = 0; i < allowed.length; i++)
            if (allowed[i]) {
                isSomethingAllowed = true;
                break;
            }
        if (!isSomethingAllowed)
            return;
        
        selectedPoints = new int[initPoints.size()];
        for (int i = 0; i < selectedPoints.length; i++) {
            int[] p = initPoints.get(i);
            selectedPoints[i] = (dims[1] * p[2] + p[1]) * dims[0] + p[0];
        }
        this.allowed = allowed;

        data = new short[indices.size()][];
        for (int i = 0; i < indices.size(); i++)
            data[i] = inField.getComponent(indices.get(i)).getRawShortArray().getData();

        status = 0;
        new Thread(new ComputeSimilarity()).start();
    }


    private class ComputeSimilarity implements Runnable
    {

        @SuppressWarnings("unchecked")
        @Override
        public synchronized void run()
        {
            System.out.println("segmenting in range "+low+":"+up);
            ProgressMonitor monitor = new ProgressMonitor(null, "Computing similarity field...", " ", 0, 100);
            monitor.setMillisToDecideToPopup(100);
            queue = new FastIntQueue[tollerance];
            monitor.setMillisToPopup(100);
            queue[0] = new FastIntQueue();
            for (int i = 0; i < ndata; i++)
                outd[i] = tollerance;
            //set outd to 0 on boundary 
            extendMargins((short) 0);
            //initializing queue to selected points and setting seed values at selected points
            for (int k : selectedPoints) {
                outd[k] = 0;
                queue[0].push(k);
            }
            for (int i = 0; i < tollerance; i++) {
                if (queue[i] == null)
                    continue;
                FastIntQueue q = queue[i];
                monitor.setProgress((i * 100) / (tollerance - 1));
                while (!q.isEmpty()) {
                    int n = q.get();
                    if (!allowed[mask[n]])
                        continue;
                    int cd = outd[n];
                    for (int j = 0; j < off.length; j++) {
                        int of = off[j];
                        if (!allowed[mask[n + of]] || 
                             data[0][n + of] < low || data[0][n + of] > up)
                            continue;
                        int d = outd[n + of];
                        if (d < cd)
                            continue;
                        float dd = 0;
                        for (int id = 0; id < data.length; id++) {
                            if (weights[id] == 0)
                                continue;
                            int ll = Math.abs(data[id][n + of] - data[id][n]);
                            ll = Math.max(0, ll - noiseLevel);
                            if (ll < contourLevel)
                                dd += weights[id] * ll * ll;
                            else 
                                dd = Integer.MAX_VALUE / 2;
                        }
                        int l = cd + (int)dd;
                        if (l < d) {
                            outd[n + of] = l;
                            if (queue[l] == null)
                                queue[l] = new FastIntQueue();
                            queue[l].push(n + of);
                        }
                    }
                }
            }
            status = SIMILARITY_COMPUTED;
            fireStateChanged();
        }
    }

    @Override
    public void setIndices(ArrayList<Integer> indices)
    {
        this.indices = indices;
    }

    @Override
    public void setWeights(float[] weights)
    {
        this.weights = weights;
    }

    @Override
    public void setTollerance(short tollerance)
    {
        this.tollerance = tollerance;
    }

    @Override
    public void setComponent(int component)
    {

    }

}
