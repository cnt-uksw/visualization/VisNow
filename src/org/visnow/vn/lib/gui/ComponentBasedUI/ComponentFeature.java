/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.gui.ComponentBasedUI;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArraySchema;
import static org.visnow.vn.lib.gui.ComponentBasedUI.PreferredExtentChangeBehavior.ALWAYS_RESET;
import org.visnow.vn.lib.utils.SwingInstancer;

public class ComponentFeature implements Cloneable
{

    public enum PreferredRangesChangePolicy
    {
        ALWAYS_RESET,
        KEEP_IF_INSIDE,
        KEEP_IF_BRACES,
        EXTEND_IF_INTERSECTS,
        ALWAYS_KEEP
    }

    protected DataContainerSchema containerSchema = null;
    protected DataArraySchema componentSchema = null;
    protected ComponentFeatureUI ui = null;
    protected String[] acceptableComponentNames = {""};
    protected float componentMin, componentMax;
    protected float componentPhysMin, componentPhysMax;
    protected boolean active = true;
    protected boolean lastActive = true;

    /**
     * indicates how the values are set when component preferredMinValue/preferredMaxValue change
     */
    protected PreferredExtentChangeBehavior preferredExtentChangeBehavior = ALWAYS_RESET;
    /**
     * indicates if field pseudo-components (coordinates, indices etc.) are allowed
     */
    protected boolean pseudoComponentsAllowed = false;
    /**
     * indicates if only simple numeric components are allowed
     */
    protected boolean numericsOnly = false;
    /**
     * indicates if only scalar components are allowed
     */
    protected boolean scalarsOnly = false;
    /**
     * indicates if only vector (non-scalar) components are allowed
     */
    protected boolean vectorsOnly = false;
    /**
     * indicates if null component choice is added
     */
    protected boolean addNull = false;
    /**
     * when new container is set, this array is matched against component names and first matching index is set
     */
    protected String[] preferedItemNames = {};
    /**
     * indicates if null component is preferred when new container is set (valid only if addNull is true)
     */
    protected boolean prefereNull = false;
    /**
     * indicates if state change action is fired when an atomic update is
     * performed
     */
    protected boolean fireOnUpdate = true;
    /**
     * indicates if state change action is fired when a slider type widget is
     * moving
     */
    protected boolean continuousUpdate = false;
    /**
     * indicates if component name has been changed when new container has been
     * set
     */
    protected boolean oldComponentNameInvalid = true;

    public ComponentFeature()
    {
        this(false, false, false, false, false, false);
    }

    public ComponentFeature(boolean pseudoComponentsAllowed, String[] preferedItemNames,
                            boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate,
                            PreferredExtentChangeBehavior preferredExtentChangeBehavior)
    {
        this.preferedItemNames = preferedItemNames;
        this.pseudoComponentsAllowed = pseudoComponentsAllowed;
        this.numericsOnly = numericsOnly;
        this.scalarsOnly = scalarsOnly;
        this.vectorsOnly = vectorsOnly;
        this.addNull = addNull;
        this.prefereNull = prefereNull;
        this.continuousUpdate = continuousUpdate;
        this.preferredExtentChangeBehavior = preferredExtentChangeBehavior;
    }

    public ComponentFeature(boolean pseudoComponentsAllowed,
                            boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate,
                            PreferredExtentChangeBehavior preferredExtentChangeBehavior)
    {
        this(pseudoComponentsAllowed, new String[]{}, numericsOnly, scalarsOnly, vectorsOnly,
             addNull, prefereNull, continuousUpdate, preferredExtentChangeBehavior);
    }

    public ComponentFeature(boolean pseudoComponentsAllowed,
                            boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate)
    {
        this(pseudoComponentsAllowed, new String[]{}, numericsOnly, scalarsOnly, vectorsOnly,
             addNull, prefereNull, continuousUpdate, ALWAYS_RESET);
    }

    public ComponentFeature(boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate)
    {
        this(false, numericsOnly, scalarsOnly, vectorsOnly,
             addNull, prefereNull, continuousUpdate);
    }

    public ComponentFeature(boolean pseudoComponentsAllowed, String[] preferedItemNames,
                            boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate)
    {
        this(pseudoComponentsAllowed, preferedItemNames, numericsOnly, scalarsOnly, vectorsOnly,
             addNull, prefereNull, continuousUpdate, ALWAYS_RESET);
    }

    public ComponentFeature clone()
    {
        ComponentFeature cloned = new ComponentFeature(pseudoComponentsAllowed, preferedItemNames,
                                                       numericsOnly, scalarsOnly, vectorsOnly,
                                                       addNull, prefereNull, continuousUpdate,
                                                       preferredExtentChangeBehavior);
        cloned.setContainerSchema(containerSchema);
        cloned.setComponentSchema(componentSchema);
        return cloned;
    }

    public void copyValuesFrom(ComponentFeature src)
    {
        setActive(false);
        numericsOnly = src.numericsOnly;
        scalarsOnly = src.scalarsOnly;
        vectorsOnly = src.vectorsOnly;
        addNull = src.addNull;
        prefereNull = src.prefereNull;
        continuousUpdate = src.continuousUpdate;
        pseudoComponentsAllowed = src.pseudoComponentsAllowed;
        containerSchema = src.containerSchema;
        acceptableComponentNames = src.acceptableComponentNames;
        if (ui != null) {
            SwingInstancer.swingRunLater(() -> {
                ui.setComponentNames(acceptableComponentNames);
                ui.setSelectedName(src.getComponentName());
            });
        }
        setComponentSchema(src.componentSchema);
        restoreActive();
    }

    public boolean isPseudoComponentsAllowed()
    {
        return pseudoComponentsAllowed;
    }

    public void setPseudoComponentsAllowed(boolean pseudoComponentsAllowed)
    {
        this.pseudoComponentsAllowed = pseudoComponentsAllowed;
    }

    public void setPreferedItemNames(String[] preferedItemNames)
    {
        this.preferedItemNames = preferedItemNames;
    }

    public boolean isNumericsOnly()
    {
        return numericsOnly;
    }

    public boolean isScalarsOnly()
    {
        return scalarsOnly;
    }

    public boolean isVectorsOnly()
    {
        return vectorsOnly;
    }

    public boolean isAddNull()
    {
        return addNull;
    }

    public void setNumericsOnly(boolean numericsOnly)
    {
        this.numericsOnly = numericsOnly;
    }

    public ComponentFeature numericsOnly(boolean numericsOnly)
    {
        this.numericsOnly = numericsOnly;
        return this;
    }

    public void setScalarsOnly(boolean scalarsOnly)
    {
        this.scalarsOnly = scalarsOnly;
    }

    public ComponentFeature scalarsOnly(boolean scalarsOnly)
    {
        this.scalarsOnly = scalarsOnly;
        return this;
    }

    public void setVectorsOnly(boolean vectorsOnly)
    {
        this.vectorsOnly = vectorsOnly;
    }

    public ComponentFeature vectorsOnly(boolean vectorsOnly)
    {
        this.vectorsOnly = vectorsOnly;
        return this;
    }

    public void setAddNull()
    {
        addNull = true;
    }

    public ComponentFeature addNull()
    {
        addNull = true;
        return this;
    }

    public void setAddNull(boolean addNull)
    {
        this.addNull = prefereNull = addNull;
    }

    public ComponentFeature addNull(boolean addNull)
    {
        this.addNull = prefereNull = addNull;
        return this;
    }

    public void setPrefereNull(boolean prefereNull)
    {
        if (addNull)
            this.prefereNull = prefereNull;
    }

    public ComponentFeature prefereNull(boolean prefereNull)
    {
        this.prefereNull = prefereNull;
        return this;
    }

    public void setUI(ComponentFeatureUI ui)
    {
        this.ui = ui;
        localUpdateUI();
    }

    public void setContinuousUpdate(boolean continuousUpdate)
    {
        this.continuousUpdate = continuousUpdate;
    }

    public ComponentFeature continuousUpdate(boolean continuousUpdate)
    {
        this.continuousUpdate = continuousUpdate;
        return this;
    }

    public void setFireOnUpdate(boolean fireOnUpdate)
    {
        this.fireOnUpdate = fireOnUpdate;
    }

    public ComponentFeature fireOnUpdate(boolean fireOnUpdate)
    {
        this.fireOnUpdate = fireOnUpdate;
        return this;
    }

    public void setPolicy(PreferredExtentChangeBehavior policy)
    {
       this.preferredExtentChangeBehavior = policy;
    }

    public ComponentFeature policy(PreferredExtentChangeBehavior policy)
    {
       this.preferredExtentChangeBehavior = policy;
       return this;
    }

    public boolean isAcceptable(DataArray da)
    {
        return (addNull && da == null) ||
            da != null &&
            (!numericsOnly || da.isNumeric()) &&
            (!scalarsOnly || da.getVectorLength() == 1) &&
            (!vectorsOnly || da.getVectorLength() != 1);
    }

    public boolean isSchemaAcceptable(DataArraySchema da)
    {
        return (addNull && da == null) ||
            isComponentSchemaAcceptable(da);
    }

    public boolean isComponentSchemaAcceptable(DataArraySchema da)
    {
        return da != null &&
            (!numericsOnly || da.isNumeric()) &&
            (!scalarsOnly || da.getVectorLength() == 1) &&
            (!vectorsOnly || da.getVectorLength() != 1);
    }

    public DataContainerSchema getContainerSchema()
    {
        return containerSchema;
    }

    public void setContainerSchema(DataContainerSchema containerSchema)
    {
        if (containerSchema == null)
            return;
        this.containerSchema = containerSchema;
        int nNewAcceptableComponents = 0;

// generating new list of acceptable components and pseudocomponents names (see isSchemaAcceptable() condition
        String[] newAcceptableComponentNames
            = new String[containerSchema.getNComponents() +
            containerSchema.getNPseudoComponents() + 1];
        for (DataArraySchema cSch : containerSchema.getComponentSchemas())
            if (isSchemaAcceptable(cSch)) {
                newAcceptableComponentNames[nNewAcceptableComponents]
                    = cSch.getName();
                nNewAcceptableComponents += 1;
            }
        if (pseudoComponentsAllowed)
            for (DataArraySchema cSch : containerSchema.getPseudoComponentSchemas())
                if (isSchemaAcceptable(cSch)) {
                    newAcceptableComponentNames[nNewAcceptableComponents]
                        = cSch.getName();
                    nNewAcceptableComponents += 1;
                }
        if (addNull) {
            newAcceptableComponentNames[nNewAcceptableComponents] = "null";
            nNewAcceptableComponents += 1;
        }
        boolean acceptableComponentsChanged = acceptableComponentNames == null ||
            nNewAcceptableComponents != acceptableComponentNames.length;
        if (!acceptableComponentsChanged)
            for (int i = 0; i < acceptableComponentNames.length; i++)
                if (!newAcceptableComponentNames[i].equals(acceptableComponentNames[i])) {
                    acceptableComponentsChanged = true;
                    break;
                }

//  finding new component name: old name when old name points to an acceptable component,
//  otherwise "null" when preferred, first acceptable component name otherwise
        String newName = "null";
        if (acceptableComponentsChanged) {
            acceptableComponentNames = new String[nNewAcceptableComponents];
            System.arraycopy(newAcceptableComponentNames, 0,
                             acceptableComponentNames, 0, nNewAcceptableComponents);
            if (ui != null)
                SwingInstancer.swingRunLater(() -> {
                    ui.setComponentNames(acceptableComponentNames);
            });
        }
        oldComponentNameInvalid = true;
        if (componentSchema != null)
            for (String acceptableComponentName : acceptableComponentNames)
                if (componentSchema.getName().equals(acceptableComponentName)) {
                    newName = acceptableComponentName;
                    oldComponentNameInvalid = false;
                    break;
                }
        if (newName.equals("null") && (!prefereNull || !addNull)) {
            componentSchema = null;
            acceptableNames:
            for (String string : preferedItemNames)
                for (String name : acceptableComponentNames)
                    if (name.equals(string)) {
                        newName = string;
                        break acceptableNames;
                    }
            if (newName.equals("null") &&
                acceptableComponentNames != null &&
                acceptableComponentNames.length > 0)
                newName = acceptableComponentNames[0];
        }
        if (newName.equalsIgnoreCase("null"))
            setComponentSchema((DataArraySchema) null);
        else
            setComponentSchema(newName);

        localUpdateComponent();
        if (ui != null) {
            SwingInstancer.swingRunLater(() -> {
                if (componentSchema != null)
                    ui.componentSelector.setSelectedItem(componentSchema.getName());
                else
                    ui.componentSelector.setSelectedItem("null");
            });
        }
    }

    public void setContainer(DataContainer container)
    {
        setContainerSchema(container.getSchema());
    }

    public void setComponentSchema(DataArraySchema daSchema)
    {

        if (containerSchema == null || daSchema == null ||
            !(containerSchema.getComponentSchemas().contains(daSchema) ||
            pseudoComponentsAllowed &&
            containerSchema.getPseudoComponentSchemas().contains(daSchema))) {
            if (ui != null)
                SwingInstancer.swingRunLater(() -> {
                    ui.setSelectedName("null");
            });
            componentSchema = null;
            componentMin = componentPhysMin = 0;
            componentMax = componentPhysMax = 1;
        } else {
            if (ui != null) {
                SwingInstancer.swingRunLater(() -> {
                    ui.setSelectedName(daSchema.getName());
                });
            }
            componentSchema = daSchema;
            componentMin = (float) daSchema.getPreferredMinValue();
            componentMax = (float) daSchema.getPreferredMaxValue();
            componentPhysMin = (float) daSchema.getPreferredPhysMinValue();
            componentPhysMax = (float) daSchema.getPreferredPhysMaxValue();
        }
        if (componentSchema != null)
            componentSchema = daSchema;
        localUpdateComponent();
    }

    public void setComponentSchema(int k)
    {
        if (containerSchema == null || k < 0 || k >= containerSchema.getNComponents())
            componentSchema = null;
        else {
            DataArraySchema da = containerSchema.getComponentSchema(k);
            if (isSchemaAcceptable(da))
                setComponentSchema(da);
        }
        if (fireOnUpdate) {
        }
    }

    public void setComponentSchema(String componentName) //used from external sources
    {
        fireOnUpdate = false;
        if (containerSchema == null) {
            oldComponentNameInvalid = componentSchema != null;
            componentSchema = null;
        } else {
            oldComponentNameInvalid = (componentSchema == null || !componentSchema.getName().equals(componentName));
            DataArraySchema da = containerSchema.getComponentSchema(componentName);
            if (da != null && isSchemaAcceptable(da)) {
                setComponentSchema(da);
            }
        }
        fireOnUpdate = true;
    }

    public void uiSetComponentSchema(String componentName) //used from UI
    {
        fireOnUpdate = false;
        if (containerSchema == null || addNull && componentName.equalsIgnoreCase("null")) {
            oldComponentNameInvalid = componentSchema != null;
            setComponentSchema((DataArraySchema) null);
        } else {
            oldComponentNameInvalid = (componentSchema == null || !(componentSchema.getName().equals(componentName)));
            DataArraySchema dataArraySchema = containerSchema.getComponentSchema(componentName);
            if (dataArraySchema == null)
                dataArraySchema = containerSchema.getPseudoComponentSchema(componentName);
            if (isSchemaAcceptable(dataArraySchema))
                setComponentSchema(dataArraySchema);
        }
        fireOnUpdate = true;
        fireStateChanged();
    }

    public DataArraySchema getComponentSchema()
    {
        return componentSchema;
    }

    public int getComponentIndex()
    {
        if (componentSchema == null)
            return -1;
        else
            return (containerSchema.getComponentSchemas().indexOf(componentSchema));
    }

    public String[] getComponentNames()
    {
        return acceptableComponentNames;
    }

    public String getComponentName()
    {
        if (componentSchema != null)
            return componentSchema.getName();
        return null;
    }

    public float getComponentMin()
    {
        return componentMin;
    }

    public float getComponentMax()
    {
        return componentMax;
    }

    public float getComponentPhysMin()
    {
        return componentPhysMin;
    }

    public float getComponentPhysMax()
    {
        return componentPhysMax;
    }

    protected void localUpdateUI()
    {
    }

    protected void localUpdateComponent(){
    }

    protected void reset()
    {

    }

    /**
     * Utility field holding list of ChangeListeners.
     */
    protected transient ArrayList<ChangeListener> changeListenerList = new ArrayList<>();

    /**
     * Registers ChangeListener to receive events.
     *
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     *
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     * <p>
     */
    protected void fireStateChanged()
    {
        if (fireOnUpdate && active) {
            ChangeEvent e = new ChangeEvent(this);
            for (ChangeListener listener : changeListenerList)
                listener.stateChanged(e);
        }
    }

    /**
     * Utility field holding list of ChangeListeners.
     */
    protected transient ArrayList<ChangeListener> uiListenerList = new ArrayList<>();

    /**
     * Registers ChangeListener to receive events.
     *
     * @param listener The listener to register.
     */
    public synchronized void addUIChangeListener(ChangeListener listener)
    {
        uiListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     *
     * @param listener The listener to remove.
     */
    public synchronized void removeUIChangeListener(ChangeListener listener)
    {
        uiListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     * <p>
     */
    protected void updateUIs()
    {
        ChangeEvent e = new ChangeEvent(this);
        for (ChangeListener listener : uiListenerList)
            listener.stateChanged(e);
    }

    protected ChangeListener localListener = null;

    public void setActive(boolean active)
    {
        lastActive = this.active;
        this.active = active;
    }

    public void restoreActive()
    {
        active = lastActive;
    }

    public boolean isComponentDefault()
    {
        return componentSchema != null && acceptableComponentNames.length > 0 &&
            componentSchema.getName().equals(acceptableComponentNames[0]);
    }

    @Override
    public String toString()
    {
        if (componentSchema == null)
            return "[component][/component]";
        return "[component]" + componentSchema.getName() + "[/component]";
    }

    public void updateFromString(String s)
    {
        if (s == null || s.isEmpty() || s.length() < 24 ||
            !s.startsWith("[component]") || !s.endsWith("[/component]"))
            return;
        setComponentSchema(s.substring(11, s.length() - 12).trim());
    }
}
