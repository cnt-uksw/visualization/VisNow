/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.gui.ComponentBasedUI;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.gui.widgets.MultistateButton;
import org.visnow.vn.lib.gui.ComponentBasedUI.array.ComponentValuesArray;
import org.visnow.vn.lib.gui.ComponentBasedUI.array.ComponentValuesArrayUI;
import org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrange;
import org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrangeUI;
import org.visnow.vn.lib.gui.ComponentBasedUI.scale.ComponentScale;
import org.visnow.vn.lib.gui.ComponentBasedUI.scale.ComponentScaleUI;
import org.visnow.vn.lib.gui.ComponentBasedUI.value.ComponentValue;
import org.visnow.vn.lib.gui.ComponentBasedUI.value.ComponentValueUI;

import javax.swing.JLabel;
import javax.swing.JTextField;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.RegularField;

/**
 *
 * @author Krzysztof S. Nowinski (University of Warsaw, ICM)
 */
public class TestFrame extends javax.swing.JFrame
{

    ComponentValue cv = new ComponentValue();
    ComponentSubrange cr = new ComponentSubrange();
    ComponentValuesArray ca = new ComponentValuesArray();
    ComponentScale cs = new ComponentScale();
    RegularField outField0, outField1, outField2;
    ComponentFeature[] params = {cv, cr, cs, ca};
    RegularField[] flds;
    String[] names;
    int selCom = 0;

    /**
     * Creates new form TestFrame
     */
    public TestFrame()
    {
        initComponents();
        this.setBounds(500,200,270,720);
        createTestPoints0();
        createTestPoints1();
        createTestPoints3();
        names = new String[outField0.getNComponents()];
        for (int i = 0; i < names.length; i++)
            names[i] = outField0.getComponent(i).getName();
        flds = new RegularField[]{outField0, outField1, outField2};
        cv.addNull();
        cv.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                System.out.println(cv.getComponentName() + " value: " + cv.getValue());
            }
        });
        cr.addNull();
        cr.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                System.out.println(cr.getComponentName() + " range: " + cr.getLow() + " " + cr.getUp());
            }
        });
        ca.addNull();
        ca.setPreferredCount(10);
        ca.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                float[] vs = ca.getValues();
                System.out.print(ca.getComponentName() + " vals: ");
                for (float f : vs)
                    System.out.printf("%6.3f ", f);
                System.out.println("");
            }
        });
        cs.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                System.out.println(cs.getComponentName() + " scale: " + cs.getValue());
            }
        });
        valueUI.setComponentValue(cv);
        cv.setContainerSchema(outField0.getSchema());
        cs.setContainerSchema(outField0.getSchema());
        scaleUI.setComponentScale(cs);
        cr.setPseudoComponentsAllowed(true);
        cr.setContainerSchema(outField0.getSchema());
        rangeUI.setComponentValue(cr);
        cr.setFireOnUpdate(true);
        ca.setPreferredCount(10);
        arrayUI.setComponentValuesArray(ca);
        ca.setContainerSchema(outField0.getSchema());
        cr.setFireOnUpdate(true);
    }

    /**
     * This method is cellArraylled from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        GridBagConstraints gridBagConstraints;

        switchButton = new MultistateButton();
        jButton1 = new JButton();
        jPanel1 = new JPanel();
        dynamicBox = new JCheckBox();
        valueUI = new ComponentValueUI();
        rangeUI = new ComponentSubrangeUI();
        jPanel2 = new JPanel();
        jLabel1 = new JLabel();
        jTextField1 = new JTextField();
        scaleUI = new ComponentScaleUI();
        arrayUI = new ComponentValuesArrayUI();
        jPanel3 = new JPanel();
        saveButton = new JButton();
        loadButton = new JButton();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(180, 770));
        setPreferredSize(new Dimension(166, 775));
        getContentPane().setLayout(new GridBagLayout());

        switchButton.setText("multistateButton1");
        switchButton.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                switchButtonStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        getContentPane().add(switchButton, gridBagConstraints);

        jButton1.setText("next component");
        jButton1.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                jButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        getContentPane().add(jButton1, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(jPanel1, gridBagConstraints);

        dynamicBox.setText("dynamic update");
        dynamicBox.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                dynamicBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        getContentPane().add(dynamicBox, gridBagConstraints);

        valueUI.setBorder(BorderFactory.createEtchedBorder());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        getContentPane().add(valueUI, gridBagConstraints);

        rangeUI.setBorder(BorderFactory.createEtchedBorder());
        rangeUI.setMinimumSize(new Dimension(180, 95));
        rangeUI.setPreferredSize(new Dimension(100, 100));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        getContentPane().add(rangeUI, gridBagConstraints);

        jPanel2.setLayout(new GridBagLayout());

        jLabel1.setText("force value");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel2.add(jLabel1, gridBagConstraints);

        jTextField1.setText("5");
        jTextField1.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                jTextField1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel2.add(jTextField1, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        getContentPane().add(jPanel2, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        getContentPane().add(scaleUI, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        getContentPane().add(arrayUI, gridBagConstraints);

        saveButton.setText("save");
        saveButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                saveButtonActionPerformed(evt);
            }
        });
        jPanel3.add(saveButton);

        loadButton.setText("load");
        loadButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                loadButtonActionPerformed(evt);
            }
        });
        jPanel3.add(loadButton);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        getContentPane().add(jPanel3, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void switchButtonStateChanged(ChangeEvent evt)//GEN-FIRST:event_switchButtonStateChanged
    {//GEN-HEADEREND:event_switchButtonStateChanged
        System.out.println("switch " + switchButton.getState());
        for (ComponentFeature param : params)
            param.setContainer(flds[switchButton.getState()]);
        names = new String[flds[switchButton.getState()].getNComponents()];
        for (int i = 0; i < names.length; i++)
            names[i] = flds[switchButton.getState()].getComponent(i).getName();
    }//GEN-LAST:event_switchButtonStateChanged

    private void jButton1ActionPerformed(ActionEvent evt)//GEN-FIRST:event_jButton1ActionPerformed
    {//GEN-HEADEREND:event_jButton1ActionPerformed
        for (ComponentFeature param : params) {
            int k = param.getComponentIndex();
            k = (k + 1) % names.length;
            param.setComponentSchema(names[k]);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

   private void dynamicBoxActionPerformed(ActionEvent evt)//GEN-FIRST:event_dynamicBoxActionPerformed
   {//GEN-HEADEREND:event_dynamicBoxActionPerformed
      for (ComponentFeature componentFeature : params)
         componentFeature.setContinuousUpdate(dynamicBox.isSelected());
   }//GEN-LAST:event_dynamicBoxActionPerformed

   private void jTextField1ActionPerformed(ActionEvent evt)//GEN-FIRST:event_jTextField1ActionPerformed
   {//GEN-HEADEREND:event_jTextField1ActionPerformed
      try
      {
         cv.setPhysicalValue(Float.parseFloat(jTextField1.getText()));
      } catch (Exception e)
      {
      }
   }//GEN-LAST:event_jTextField1ActionPerformed

    private void saveButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_saveButtonActionPerformed
    {//GEN-HEADEREND:event_saveButtonActionPerformed
        System.out.println(cv.toString());
        System.out.println(cr.toString());
        try {
            PrintWriter saveWriter = new PrintWriter(new File("/tmp/cmp.save"));
            saveWriter.println(cv.toString());
            saveWriter.println(cr.toString());
            saveWriter.println(ca.toString());
            saveWriter.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void loadButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_loadButtonActionPerformed
    {//GEN-HEADEREND:event_loadButtonActionPerformed
        try {
            LineNumberReader reader = new LineNumberReader(new FileReader(new File("/tmp/cmp.save")));
            
            String line;
            do {
                line = reader.readLine();
                if (line == null || line.isEmpty())
                    break;
                if (line.startsWith("[componentValue]"))
                    cv.updateFromString(line);
                if (line.startsWith("[componentSubrange]"))
                    cr.updateFromString(line);
                if (line.startsWith("[componentArray]"))
                    ca.updateFromString(line);
            } while (true);
            reader.close();
        } catch (Exception ex) {
            Logger.getLogger(TestFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_loadButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                new TestFrame().setVisible(true);
            }
        });
    }

    private void createTestPoints0()
    {
        int nPoints = 100;
        int[] dims = {10,10};
        outField0 = new RegularField(dims);
        float[] coords = new float[nPoints * 3];
        int[] data = new int[nPoints];
        float[] vData = new float[nPoints * 3];
        String[] texts = new String[nPoints];
        int[] cells = new int[nPoints];
        boolean[] orient = new boolean[nPoints];
        for (int i = 0; i < nPoints; i++) {
            data[i] = i + 1;
            texts[i] = "point " + i;
            cells[i] = i;
            orient[i] = true;
        }
        for (int i = 0; i < nPoints; i++) {
            for (int j = 0; j < 3; j++)
                coords[3 * i + j] = ((float) random() - .5f) / 10;
            vData[3 * i] = -coords[3 * i + 1] + (float) random() / 5 - .1f;
            vData[3 * i + 1] = -coords[3 * i] + (float) random() / 5 - .1f;
            vData[3 * i + 2] = -coords[3 * i + 2] + (float) random() / 5 - .1f;
        }
        outField0.setCurrentCoords(new FloatLargeArray(coords));
        outField0.addComponent(DataArray.create(data, 1, "points"));
        outField0.addComponent(DataArray.create(texts, 1, "texts"));
        outField0.addComponent(DataArray.create(vData, 3, "vectors"));
    }

    private void createTestPoints1()
    {
        int nPoints = 90;
        int[] dims = {9,10};
        outField1 = new RegularField(dims);
        float[] coords = new float[nPoints * 3];
        int[] data = new int[nPoints];
        float[] vData = new float[nPoints * 3];
        String[] texts = new String[nPoints];
        int[] cells = new int[nPoints];
        boolean[] orient = new boolean[nPoints];
        for (int i = 0; i < nPoints; i++) {
            data[i] = i + 1;
            texts[i] = "point " + i;
            cells[i] = i;
            orient[i] = true;
        }
        for (int i = 0; i < nPoints; i++) {
            for (int j = 0; j < 3; j++)
                coords[3 * i + j] = 10 * ((float) random() - .5f);
            vData[3 * i] = -coords[3 * i + 1] + (float) random() / 5 - .1f;
            vData[3 * i + 1] = -coords[3 * i] + (float) random() / 5 - .1f;
            vData[3 * i + 2] = -coords[3 * i + 2] + (float) random() / 5 - .1f;
        }
        outField1.setCurrentCoords(new FloatLargeArray(coords));
        outField1.addComponent(DataArray.create(data, 1, "points"));
        outField1.addComponent(DataArray.create(texts, 1, "texts"));
        outField1.addComponent(DataArray.create(data, 1, "punkty"));
        outField1.addComponent(DataArray.create(texts, 1, "teksty"));
        outField1.addComponent(DataArray.create(vData, 3, "vectors"));
    }

    private void createTestPoints3()
    {
        int nPoints = 99;
        int[] dims = {9,11};
        outField2 = new RegularField(dims);
        float[] coords = new float[nPoints * 3];
        int[] data = new int[nPoints];
        float[] vData = new float[nPoints * 3];
        String[] texts = new String[nPoints];
        int[] cells = new int[nPoints];
        boolean[] orient = new boolean[nPoints];
        for (int i = 0; i < nPoints; i++) {
            data[i] = i + 1;
            texts[i] = "point " + i;
            cells[i] = i;
            orient[i] = true;
        }
        for (int i = 0; i < nPoints; i++) {
            for (int j = 0; j < 3; j++)
                coords[3 * i + j] = (float) random() - .5f;
            vData[3 * i] = -coords[3 * i + 1] + (float) random() / 5 - .1f;
            vData[3 * i + 1] = -coords[3 * i] + (float) random() / 5 - .1f;
            vData[3 * i + 2] = -coords[3 * i + 2] + (float) random() / 5 - .1f;
        }
        
        outField2.setCurrentCoords(new FloatLargeArray(coords));
        outField2.addComponent(DataArray.create(data, 1, "punkty"));
        outField2.addComponent(DataArray.create(texts, 1, "teksty"));
        outField2.addComponent(DataArray.create(vData, 3, "vectors"));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected ComponentValuesArrayUI arrayUI;
    protected JCheckBox dynamicBox;
    protected JButton jButton1;
    protected JLabel jLabel1;
    protected JPanel jPanel1;
    protected JPanel jPanel2;
    protected JPanel jPanel3;
    protected JTextField jTextField1;
    protected JButton loadButton;
    protected ComponentSubrangeUI rangeUI;
    protected JButton saveButton;
    protected ComponentScaleUI scaleUI;
    protected MultistateButton switchButton;
    protected ComponentValueUI valueUI;
    // End of variables declaration//GEN-END:variables
}
