/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.gui.ComponentBasedUI.array;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;
import org.visnow.vn.lib.utils.Range;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ComponentValuesArray extends ComponentFeature
{
    public static final int ALWAYS_RESET = 0;
    public static final int KEEP_IF_INSIDE = 1;
    public static final int ALWAYS_KEEP = 2;

    protected float[] values = {0};
    protected float[] physicalValues = {0};
    protected int preferredCount = 1;
    protected ComponentValuesArrayUI valUI = null;
    
    protected ChangeListener uiChangeListener = 
        new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                if (componentSchema == null || valUI == null || valUI.isAdjusting() && !continuousUpdate)
                    return;
                values = valUI.getValues();
                physicalValues = new float[values.length];
                double delta = (componentSchema.getPreferredPhysMaxValue() - componentSchema.getPreferredPhysMinValue()) /
                               (componentSchema.getPreferredMaxValue() - componentSchema.getPreferredMinValue());
                for (int i = 0; i < values.length; i++)
                    physicalValues[i] = (float)(componentSchema.getPreferredPhysMinValue() +
                                         delta  * (values[i] - componentSchema.getPreferredMinValue()));
                fireStateChanged();
            }
        };

    public int getPreferredCount()
    {
        return preferredCount;
    }

    public void setPreferredCount(int preferredCount)
    {
        this.preferredCount = preferredCount;
        if (valUI != null)
            valUI.setStartSingle(preferredCount <= 1);
    }
    
    public ComponentValuesArray preferredCount(int preferredCount)
    {
        this.preferredCount = preferredCount;
        if (valUI != null)
            valUI.setStartSingle(preferredCount <= 1);
        return this;
    }
    
    public void setValues(float[] values)
    {
        fireOnUpdate = false;
        this.values = values;
        physicalValues = new float[values.length];
        for (int i = 0; i < values.length; i++)
            physicalValues[i] = (float)(componentSchema.getPreferredPhysMinValue() +
                (componentSchema.getPreferredPhysMaxValue() - componentSchema.getPreferredPhysMinValue()) *
                (values[i] - componentSchema.getPreferredMinValue()) / (componentSchema.getPreferredMaxValue() - componentSchema.getPreferredMinValue()));
        if (valUI != null)
            valUI.updateValue();
        fireOnUpdate = true;
        fireStateChanged();
    }

    public void setPhysicalValues(float[] physicalValues)
    {
        if (componentSchema == null)
            return;
        this.physicalValues = physicalValues;
        values = new float[physicalValues.length];
        for (int i = 0; i < values.length; i++)
            values[i] = (float)(componentSchema.getPreferredMinValue() +
                (componentSchema.getPreferredMaxValue() - componentSchema.getPreferredMinValue()) *
                (physicalValues[i] - componentSchema.getPreferredPhysMinValue()) /
                (componentSchema.getPreferredPhysMaxValue() - componentSchema.getPreferredPhysMinValue()));
        if (valUI != null)
            valUI.updateValue();
        fireStateChanged();
    }

    void setValuesFromUI(float[] values)
    {
        if (componentSchema == null)
            return;
        this.values = values;
        values = new float[physicalValues.length];
        physicalValues = new float[values.length];
        for (int i = 0; i < values.length; i++)
            physicalValues[i] = (float)(componentSchema.getPreferredPhysMinValue() +
                (componentSchema.getPreferredPhysMaxValue() - componentSchema.getPreferredPhysMinValue()) *
                (values[i] - componentSchema.getPreferredMinValue()) / (componentSchema.getPreferredMaxValue() - componentSchema.getPreferredMinValue()));
        fireStateChanged();
    }

    public float[] getValues()
    {
        return values;
    }

    public float[] getPhysicalValues()
    {
        return physicalValues;
    }

    @Override
    public void setContainerSchema(DataContainerSchema containerSchema)
    {
        fireOnUpdate = false;
        super.setContainerSchema(containerSchema);
        fireOnUpdate = true;
        if (componentSchema != null)
            fireStateChanged();
    }
    
    @Override
    public void setContainer(DataContainer container)
    {
        setContainerSchema(container.getSchema());
    }

    @Override
    protected void localUpdateComponent()
    {
        if (componentSchema == null)
            return;
        if (oldComponentNameInvalid)
            reset();
    }

    @Override
    protected void localUpdateUI()
    {
       if (ui != null && ui instanceof ComponentValuesArrayUI)
       {
          valUI = (ComponentValuesArrayUI)ui;
          valUI.setListener(uiChangeListener);
          valUI.setStartSingle(preferredCount <= 1);
       }
    }
    
    @Override
    public void setComponentSchema(String componentName)
    {
        if (fireOnUpdate) {
            super.setComponentSchema(componentName);
            updateComponentValue();
        }
    }
    
    public void updateComponentValue()
    {
        if (componentSchema == null)
            return;
        if (oldComponentNameInvalid)
            reset();
        switch (preferredExtentChangeBehavior)
        {
        case ALWAYS_RESET:
            reset();
            break;
        case KEEP_IF_INSIDE: 
//            if (physicalValue < componentSchema.getPhysMin() ||
//                physicalValue > componentSchema.getPhysMax())
//                physicalValue = (componentSchema.getPhysMax() + componentSchema.getPhysMin()) / 2;
            break;
        case ALWAYS_KEEP:
            break;
        }
        fireStateChanged();
    }

    
    @Override
    public void reset()
    {
        if (componentSchema != null)
        {
            if (preferredCount == 1)
                values = new float[]{(float)(componentSchema.getPreferredPhysMaxValue() + componentSchema.getPreferredPhysMinValue()) / 2};
            else
                values = Range.createLinearRange(preferredCount, (float)componentSchema.getPreferredPhysMinValue(), (float)componentSchema.getPreferredPhysMaxValue());
            if (valUI != null)
            {
                valUI.setMinMax(componentPhysMin, componentPhysMax);
                valUI.updateValue();
            }
        }
    }
    
    @Override
    public String toString()
    {
        if (componentSchema == null)
            return "[componentArray][/componentArray]";
        StringBuilder strB = new StringBuilder("[componentArray] "+componentSchema.getName() + ":");
        for (int i = 0; i < values.length; i++) 
            strB.append(String.format(" %7.3f", values[i]));
        return strB.toString() +
               " [/componentArray]";
    }
    
    @Override
    public void updateFromString(String s)
    {
        s = s.trim();
        if (s == null || s.isEmpty() ||
           !s.startsWith("[componentArray]") || !s.endsWith("[/componentArray]"))
            return;
        String[] c = s.substring("[componentArray]".length(), s.length() - "[/componentArray]".length()).trim().split(" *:* +");
        setComponentSchema(c[0]);
        float[] vals = new float[c.length - 1];
        for (int i = 0; i < vals.length; i++) 
            vals[i] = Float.parseFloat(c[i + 1]);
        setValues(vals);
    }

}
