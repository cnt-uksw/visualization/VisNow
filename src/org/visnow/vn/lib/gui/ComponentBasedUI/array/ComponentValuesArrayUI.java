/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.gui.ComponentBasedUI.array;


import java.awt.BorderLayout;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeatureUI;
import org.visnow.vn.lib.gui.FloatArrayEditor;

/**
 *
 * @author know
 */
public class ComponentValuesArrayUI extends ComponentFeatureUI
{

    protected FloatArrayEditor valuesArrayEditor = new FloatArrayEditor();
    protected ComponentValuesArray param;

    public ComponentValuesArrayUI()
    {
        super();
        add(valuesArrayEditor, BorderLayout.CENTER);
        valuesArrayEditor.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                valChanged();
            }
        });
    }

    public void setComponentValuesArray(ComponentValuesArray param)
    {
        userUpdate = false;
        this.param = param;
        super.setComponentFeature(param);
        param.setUI(this);
        valuesArrayEditor.setValuePreferred(param.preferredCount <= 1);
        valuesArrayEditor.setMinMax(param.getComponentMin(), param.getComponentMax(), 
                                    param.getComponentPhysMin(), param.getComponentPhysMax());
        userUpdate = true;
    }

    public boolean isAdjusting()
    {
        return valuesArrayEditor.isAdjusting();
    }

    public float[] getValues()
    {
        return valuesArrayEditor.getThresholds();
    }

    void updateValue()
    {
        userUpdate = false;
        if (param != null)
            valuesArrayEditor.setThresholds(param.getPhysicalValues());
        userUpdate = true;
    }
    
    void setMinMax(float min, float max)
    {
        valuesArrayEditor.setMinMax(min, max, min, max);
    }
    
    void setStartSingle(boolean single)
    {
        valuesArrayEditor.setStartSingle(single);
    }

    ChangeListener listener = null;

    public void setListener(ChangeListener listener)
    {
        this.listener = listener;
    }

    public void clearListener()
    {
        listener = null;
    }

    private void valChanged()
    {
        if (listener != null && userUpdate)
            listener.stateChanged(new ChangeEvent(this));
    }
    
    
    @Override
    protected void updateUIToNewComponent(boolean isNull)
    {
        valuesArrayEditor.setEnabled(!isNull);
    }

    
}
