/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.gui.ComponentBasedUI.range;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.gui.components.NumericTextField;
import org.visnow.vn.gui.events.FloatPairModificationEvent;
import org.visnow.vn.gui.events.FloatPairModificationListener;
import org.visnow.vn.gui.swingwrappers.UserActionAdapter;
import org.visnow.vn.gui.swingwrappers.UserEvent;
import org.visnow.vn.gui.widgets.ExtendedSlider;
import org.visnow.vn.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeatureUI;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ComponentSubrangeUI extends ComponentFeatureUI
{

    protected static final String RANGE = "range";
    protected static final String SYM_RANGE = "symmetric range";
    protected ExtendedFloatSubRangeSlider rangeSlider = new ExtendedFloatSubRangeSlider();
    protected ExtendedSlider symmetricRangeSlider = new ExtendedSlider();
    protected JPanel switchPanel = new JPanel();
    protected JPanel sliderPanel = new JPanel(new CardLayout());
    protected JCheckBox symmetricBox = new JCheckBox();
    protected JCheckBox logBox = new JCheckBox();
    protected ComponentSubrange param;

    public ComponentSubrangeUI()
    {
        super();
        symmetricRangeSlider.setFieldType(NumericTextField.FieldType.FLOAT);
        symmetricRangeSlider.addUserActionListener(new UserActionAdapter()
        {
            @Override
            public void userChangeAction(UserEvent event)
            {
                rangeChanged();
            }
        });
        symmetricRangeSlider.setVisible(false);
        rangeSlider.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                rangeChanged();
            }
        });
        sliderPanel.setSize(200, 60);
        sliderPanel.add(rangeSlider, RANGE);
        sliderPanel.add(symmetricRangeSlider, SYM_RANGE);
        add(sliderPanel, BorderLayout.CENTER);
        symmetricBox.setText("Symmetric");
        symmetricBox.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                updateSymmetricUI();
            }
        });
        logBox.setText("Logarithmic");
        logBox.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                updateLogUI();
            }
        });
        switchPanel.setLayout(new java.awt.GridLayout(1, 0));
        switchPanel.add(symmetricBox);
        switchPanel.add(logBox);
        add(switchPanel, BorderLayout.SOUTH);
        setSize(200, 100);
    }
    
    private void updateLogUI()
    {
        if (logBox.isSelected()) {
            float max = (Float) symmetricRangeSlider.getMax();
            float val = (Float) symmetricRangeSlider.getValue();
            symmetricRangeSlider.setScaleType(ExtendedSlider.ScaleType.LOGARITHMIC);
            symmetricRangeSlider.setAll(max/1000, max, val);
        }
        else {
            float max = (Float) symmetricRangeSlider.getMax();
            float val = (Float) symmetricRangeSlider.getValue();
            symmetricRangeSlider.setScaleType(ExtendedSlider.ScaleType.LINEAR);
            symmetricRangeSlider.setAll(0, max, val);
        }
    }

    private void updateSymmetricUI()
    {
        if (symmetricBox.isSelected()) {
            logBox.setSelected(false);
            logBox.setEnabled(true);
            float maxV = Math.max(Math.abs(rangeSlider.getMin()), Math.abs(rangeSlider.getMax()));
            symmetricRangeSlider.setAll(0, maxV, maxV);
        } else {
            logBox.setEnabled(false);
            logBox.setSelected(false);
            float bdr = (Float) symmetricRangeSlider.getMax();
            float val = (Float) symmetricRangeSlider.getValue();
            rangeSlider.setMinMax(-bdr, bdr);
            rangeSlider.setLow(-val);
            rangeSlider.setUp(val);
        }
        CardLayout cl = (CardLayout) (sliderPanel.getLayout());
        cl.show(sliderPanel, symmetricBox.isSelected() ? SYM_RANGE : RANGE);
        rangeChanged();
    }

    public void setComponentValue(ComponentSubrange param)
    {
        this.param = param;
        super.setComponentFeature(param);
        userUpdate = false;
        param.setUI(this);
        float min = param.getComponentPhysMin();
        float max = param.getComponentPhysMax();
        float absMax = Math.max(Math.abs(min), Math.abs(max));
        rangeSlider.setMinMax(min, max);
        symmetricRangeSlider.setAll(absMax, absMax / 1000, absMax);
        userUpdate = true;
    }
    
    @Override
    protected void updateUIToNewComponent(boolean isNull)
    {
        rangeSlider.setEnabled(!isNull);
        symmetricRangeSlider.setEnabled(!isNull);
        symmetricBox.setEnabled(!isNull);
        logBox.setEnabled(!isNull);
    }

    public boolean isAdjusting()
    {
        return rangeSlider.isAdjusting() || symmetricRangeSlider.isAdjusting();
    }

    void setRange(float min, float max)
    {
        userUpdate = false;
        rangeSlider.setMinMax(min, max);
        float maxV = Math.max(Math.abs(min), Math.abs(max));
        symmetricRangeSlider.setMax(maxV);
        if (logBox.isSelected())
            symmetricRangeSlider.setMin(maxV / 1000);
        else
            symmetricRangeSlider.setMin(0);
        userUpdate = true;
    }

    public void updateRange()
    {
        userUpdate = false;
        if (param != null) {
            symmetricBox.setSelected(false);
            updateSymmetricUI();
            rangeSlider.setMinMax(param.getComponentPhysMin(), param.getComponentPhysMax());
            float maxV = Math.max(Math.abs(rangeSlider.getMin()), Math.abs(rangeSlider.getMin()));
            symmetricRangeSlider.setMax(maxV);
            if (logBox.isSelected())
                symmetricRangeSlider.setMin(maxV / 1e-4f);
            else
                symmetricRangeSlider.setMin(0);

        }
        userUpdate = true;
    }

    public void updateSubrange()
    {
        userUpdate = false;
        {
            if (symmetricBox.isSelected()) {
                float maxV = Math.max(Math.abs(param.getPhysicalLow()), Math.abs(param.getPhysicalUp())); 
                symmetricRangeSlider.setValue(maxV);
                rangeSlider.setLow(Math.max(-maxV, rangeSlider.getMin()));
                rangeSlider.setUp(Math.min(maxV, rangeSlider.getMax()));
            } else {
                rangeSlider.setLow(param.getPhysicalLow());
                rangeSlider.setUp(param.getPhysicalUp());
            }
        }
        userUpdate = true;
    }

    FloatPairModificationListener listener = null;

    public void setListener(FloatPairModificationListener listener)
    {
        this.listener = listener;
    }

    public void clearListener()
    {
        listener = null;
    }

    private void rangeChanged()
    {
        if (listener != null && userUpdate) {
            if (symmetricBox.isSelected())
                listener.floatPairValueChanged(
                        new FloatPairModificationEvent(this, 
                                                      -(Float) symmetricRangeSlider.getValue(), 
                                                       (Float) symmetricRangeSlider.getValue(), 
                                                       symmetricRangeSlider.isAdjusting()));
            else
                listener.floatPairValueChanged(
                        new FloatPairModificationEvent(this, 
                                                       rangeSlider.getLow(), 
                                                       rangeSlider.getUp(), 
                                                       rangeSlider.isAdjusting()));
        }
    }
}
