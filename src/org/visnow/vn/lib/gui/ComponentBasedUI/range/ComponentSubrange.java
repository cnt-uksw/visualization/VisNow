/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.gui.ComponentBasedUI.range;

import org.visnow.vn.gui.events.FloatPairModificationEvent;
import org.visnow.vn.gui.events.FloatPairModificationListener;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;
import org.visnow.vn.lib.gui.ComponentBasedUI.PreferredExtentChangeBehavior;
import org.visnow.vn.lib.gui.ComponentBasedUI.PreferredSubrangeBehavior;
import static org.visnow.vn.lib.gui.ComponentBasedUI.PreferredSubrangeBehavior.RESET_LOW_UP;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ComponentSubrange extends ComponentFeature implements Cloneable
{
    
    private float low = 0, up = 255;
    private float physLow = 0, physUp = 1;
    private double physRange = 1;
    private double compRange = 1;
    protected ComponentSubrangeUI rangeUI = null;
    /**
     * indicates how the low / up values are set when component preferredMinValue/preferredMaxValue change
     */
    protected PreferredSubrangeBehavior preferredSubrangeBehavior = RESET_LOW_UP;
    
    protected FloatPairModificationListener uiValueChangedListener = 
           new FloatPairModificationListener()
           {

               @Override
               public void floatPairValueChanged(FloatPairModificationEvent e)
               {
                   if (componentSchema == null || e.isAdjusting() && !continuousUpdate)
                       return;
                   physLow = (float) e.getValX();
                   physUp = (float) e.getValY();
                   low = (float) (componentSchema.getPreferredMinValue()
                                + compRange * (physLow - componentSchema.getPreferredPhysMinValue()) / physRange);
                   up = (float) (componentSchema.getPreferredMinValue()
                                + compRange * (physUp - componentSchema.getPreferredPhysMinValue()) / physRange);
                   fireStateChanged();
               }
           };
    
    public ComponentSubrange(boolean pseudoComponentsAllowed, 
                             boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                             boolean addNull, boolean prefereNull, boolean continuousUpdate,
                             PreferredExtentChangeBehavior preferredExtentChangeBehavior,
                             PreferredSubrangeBehavior preferredSubrangeBehavior)
    {
       super(pseudoComponentsAllowed,  numericsOnly, scalarsOnly, vectorsOnly, addNull, prefereNull, continuousUpdate, preferredExtentChangeBehavior);
       this.preferredSubrangeBehavior = preferredSubrangeBehavior;
    }

    public ComponentSubrange(boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly, boolean addNull, boolean prefereNull, boolean continuousUpdate)
    {
       super(numericsOnly, scalarsOnly, vectorsOnly, addNull, prefereNull, continuousUpdate);
    }
    
    public ComponentSubrange()
    {
       super();
    }

    @Override
    public ComponentSubrange clone()
    {
        ComponentSubrange cloned = new ComponentSubrange(numericsOnly, scalarsOnly, vectorsOnly,
                                                         addNull, prefereNull, continuousUpdate);
        cloned.preferredExtentChangeBehavior = preferredExtentChangeBehavior;
        cloned.low = low;
        cloned.up = up;
        cloned.setContainerSchema(containerSchema);
        cloned.setComponentSchema(componentSchema);
        cloned.physLow = physLow;
        cloned.physUp = physUp;
        return cloned;
    }

    public void copyValuesFrom(ComponentSubrange src)
    {
        super.copyValuesFrom(src);
        updateComponentValue(false);
        setPhysicalLowUp(src.physLow, src.physUp);
        if (ui != null && ui instanceof ComponentSubrangeUI) {
            SwingInstancer.swingRunLater(new Runnable() {
                @Override
                public void run() {
                    ((ComponentSubrangeUI) ui).updateRange();
                    ((ComponentSubrangeUI) ui).updateSubrange();
                }
            });
        }
    }

    @Override
    protected void localUpdateUI()
    {
        if (ui != null && ui instanceof ComponentSubrangeUI)
            SwingInstancer.swingRunLater(new Runnable() {
                @Override
                public void run() {
                    rangeUI = (ComponentSubrangeUI) ui;
                    rangeUI.setListener(uiValueChangedListener);                    
                }
            });        
    }

    public void setLowUp(float low, float up)
    {
        if (componentSchema == null)
            return;
        this.low = low;
        this.up = up;
        physLow = (float) componentSchema.dataRawToPhys(low);
        physUp = (float) componentSchema.dataRawToPhys(up);

        if (rangeUI != null)
            SwingInstancer.swingRunLater(new Runnable() {
                @Override
                public void run() {
                    rangeUI.updateSubrange();
                }
            });
        fireStateChanged();
    }

    public void setPhysicalLowUp(float physLow, float physUp)
    {
        if (componentSchema == null)
            return;
        this.physLow = physLow;
        this.physUp = physUp;
        low = (float) componentSchema.dataPhysToRaw(physLow);
        up = (float) componentSchema.dataPhysToRaw(physUp);

        if (rangeUI != null)
            SwingInstancer.swingRunLater(new Runnable() {
                @Override
                public void run() {
                    rangeUI.updateSubrange();
                }
            });
        fireStateChanged();
    }

    public float getLow()
    {
        return low;
    }

    public float getUp()
    {
        return up;
    }

    public float getPhysicalLow()
    {
        return physLow;
    }

    public float getPhysicalUp()
    {
        return physUp;
    }

    public void updateComponentValue()
    {
        updateComponentValue(true);
    }

    public void updateComponentValue(boolean fire)
    {

        if (oldComponentNameInvalid) {
            physLow = componentPhysMin;
            physUp = componentPhysMax;
        }
//        switch (policy)
//        {
//        case ALWAYS_RESET:
//            physLow = componentPhysMin;
//            physUp = componentPhysMax;
//            break;
//        case KEEP_IF_INSIDE: 
//            if (physLow < componentPhysMin)
//                physLow = componentPhysMin;
//            if (physUp > componentPhysMax)
//                physUp = componentPhysMax;
//            break;
//        case KEEP_IF_BRACES: 
//            if (physLow > componentPhysMin)
//                physLow = componentPhysMin;
//            if (physUp < componentPhysMax)
//                physUp = componentPhysMax;
//            break;
//        case EXTEND_IF_INTERSECTS: 
//            if (physLow > componentPhysMin ||
//                physUp < componentPhysMax){
//                physLow = componentPhysMin;
//                physUp = componentPhysMax;
//            }
//            break;
//        case ALWAYS_KEEP:
//            break;
//        }
        if (componentSchema != null) {
            physRange = componentSchema.getPreferredPhysMaxValue() - componentSchema.getPreferredPhysMinValue();
            compRange = componentSchema.getPreferredMaxValue() - componentSchema.getPreferredMinValue();
            if (physRange <= 0) physRange = 1;
            if (compRange <= 0) compRange = 1;
            if (componentSchema.isAutoResetMapRange() || oldComponentNameInvalid) {
//                if (lowUpPolicy == RESET_LOW_UP) 
                setPhysicalLowUp(componentPhysMin, componentPhysMax);
//                else 
//                    setPhysicalLowUp(Math.max(physLow, componentPhysMin), Math.min(physUp, componentPhysMax));
            }
        } else {
            low = 0;
            up = 1;
        }
        if (low >= up) {
            float m = .5f * (low + up);
            low = m - 1;
            up = m + 1;
        }
        if (fire && active)
            fireStateChanged();
    }

    @Override
    public void reset()
    {
        if (componentSchema != null) {
            setPhysicalLowUp(componentPhysMin, componentPhysMax);
            if (rangeUI != null) {
                SwingInstancer.swingRunLater(new Runnable() {
                    @Override
                    public void run() {
                        rangeUI.updateRange();
                        rangeUI.updateSubrange();
                    }
                });
            }
        }
    }

    @Override
    public void localUpdateComponent()
    {
        if (componentSchema != null) {
            updateComponentValue();
            if (rangeUI != null) {
                SwingInstancer.swingRunLater(new Runnable() {
                    @Override
                    public void run() {
                        rangeUI.updateRange();
                        rangeUI.updateSubrange();
                    }
                });
            }
        }
    }

    @Override
    public String toString()
    {
        if (componentSchema == null)
            return "";
        return componentSchema.getName() +
            String.format(": %7.3f %7.3f", physLow, physUp);
    }

    @Override
    public void updateFromString(String s)
    {
        s = s.trim();
        if (s == null || s.isEmpty()) {
            componentSchema = null;
            return;
        }
        String[] c = s.trim().split(" *:* +");
        setComponentSchema(c[0]);
        setPhysicalLowUp(Float.parseFloat(c[1]), Float.parseFloat(c[2]));
    }

    public String[] valuesToStringArray()
    {
        if (componentSchema == null)
            return new String[]{"null"};
        return new String[]{componentSchema.exportSchemaToString(),
                            String.format("selected range: %7.3f %7.3f", physLow, physUp)};
    }

    public void restoreFromStringArray(String[] s)
    {
        setComponentSchema((s[0].trim().split(" *:* +")[0]));
        if (s.length > 1) {
            String[] c = s[1].trim().split(" *:* +");
            setPhysicalLowUp(Float.parseFloat(c[2]), Float.parseFloat(c[3]));
        }
    }

    public void setLowUpPolicy(PreferredSubrangeBehavior lowUpPolicy)
    {
        this.preferredSubrangeBehavior = lowUpPolicy;
    }

    public boolean isAdjusting()
    {
        if (ui == null)
            return false;
        return ((ComponentSubrangeUI) ui).isAdjusting();
    }

    public boolean isUnmodified()
    {
        return isComponentDefault() && physLow == componentPhysMin && physUp == componentPhysMax;
    }
}
