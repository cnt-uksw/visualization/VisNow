/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */

package org.visnow.vn.lib.gui.ComponentBasedUI.value;

import org.visnow.jscic.DataContainer;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;
import org.visnow.vn.lib.gui.ComponentBasedUI.PreferredExtentChangeBehavior;
import static org.visnow.vn.lib.gui.ComponentBasedUI.PreferredExtentChangeBehavior.KEEP_IF_INSIDE;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ComponentValue extends ComponentFeature
{

    protected float value = 0;
    protected float physicalValue = 0;
    protected ComponentValueUI valUI = null;
    protected FloatValueModificationListener uiValueChangedListener =
           new FloatValueModificationListener()
           {
              @Override
              public void floatValueChanged(FloatValueModificationEvent e)
              {
                 if (componentSchema == null || e.isAdjusting() && !continuousUpdate)
                    return;
                 physicalValue = e.getVal();
                 value = (float)(componentSchema.getPreferredMinValue() +
                         (componentSchema.getPreferredMaxValue() - componentSchema.getPreferredMinValue()) *
                         (physicalValue - componentSchema.getPreferredPhysMinValue()) /
                         (componentSchema.getPreferredPhysMaxValue() - componentSchema.getPreferredPhysMinValue()));
                 fireStateChanged();
              }
           };

    public ComponentValue()
    {
       super();
       preferredExtentChangeBehavior = KEEP_IF_INSIDE;
    }

    public ComponentValue(boolean pseudoComponentsAllowed, String[] preferedItemNames,
                          boolean scalarsOnly, boolean vectorsOnly,
                          boolean addNull, boolean prefereNull,
                          boolean continuousUpdate,
                          PreferredExtentChangeBehavior preferredExtentChangeBehavior)
    {
        super(pseudoComponentsAllowed, preferedItemNames, true,
              scalarsOnly, vectorsOnly, addNull, prefereNull,
              continuousUpdate, preferredExtentChangeBehavior);
    }

    public ComponentValue(boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                          boolean addNull, boolean prefereNull, boolean continuousUpdate)
    {
        this(false, new String[]{}, scalarsOnly, vectorsOnly,
             addNull, prefereNull, continuousUpdate, KEEP_IF_INSIDE);
    }

    public void setValue(float value)
    {
        if (componentSchema == null)
            return;
        this.value = value;
        physicalValue = (float)(componentSchema.getPreferredPhysMinValue() +
            (componentSchema.getPreferredPhysMaxValue() - componentSchema.getPreferredPhysMinValue()) *
            (value - componentSchema.getPreferredMinValue()) / (componentSchema.getPreferredMaxValue() - componentSchema.getPreferredMinValue()));
        if (valUI != null)
            valUI.updateValue();
        fireStateChanged();
    }

    public void setPhysicalValue(float physicalValue)
    {
        if (componentSchema == null)
            return;
        this.physicalValue = physicalValue;
        value = (float)(componentSchema.getPreferredMinValue() +
            (componentSchema.getPreferredMaxValue() - componentSchema.getPreferredMinValue()) *
            (physicalValue - componentSchema.getPreferredPhysMinValue()) /
            (componentSchema.getPreferredPhysMaxValue() - componentSchema.getPreferredPhysMinValue()));
        if (valUI != null)
            valUI.updateValue();
        fireStateChanged();
    }

    void setPhysicalValueFromUI(float physicalValue)
    {
        if (componentSchema == null)
            return;
        this.physicalValue = physicalValue;
        value = (float)(componentSchema.getPreferredMinValue() +
            (componentSchema.getPreferredMaxValue() - componentSchema.getPreferredMinValue()) *
            (physicalValue - componentSchema.getPreferredPhysMinValue()) /
            (componentSchema.getPreferredPhysMaxValue() - componentSchema.getPreferredPhysMinValue()));
        if (continuousUpdate || valUI == null || !valUI.isAdjusting())
            fireStateChanged();
    }

    public float getValue()
    {
        return value;
    }

    public float getPhysicalValue()
    {
        return physicalValue;
    }

    public void updateComponentValue()
    {
        if (componentSchema == null)
            return;
        if (oldComponentNameInvalid)
            physicalValue = (float)(componentSchema.getPreferredPhysMaxValue() + componentSchema.getPreferredPhysMinValue()) / 2;
        switch (preferredExtentChangeBehavior)
        {
        case ALWAYS_RESET:
            physicalValue = (float)(componentSchema.getPreferredPhysMaxValue() + componentSchema.getPreferredPhysMinValue()) / 2;
            break;
        case KEEP_IF_INSIDE:
            if (physicalValue < componentSchema.getPreferredPhysMinValue() ||
                physicalValue > componentSchema.getPreferredPhysMaxValue())
                physicalValue = (float)(componentSchema.getPreferredPhysMaxValue() + componentSchema.getPreferredPhysMinValue()) / 2;
            break;
        case ALWAYS_KEEP:
            break;
        }
        value = (float)(componentSchema.getPreferredMinValue() +
            (componentSchema.getPreferredMaxValue() - componentSchema.getPreferredMinValue()) *
            (physicalValue - componentSchema.getPreferredPhysMinValue()) /
            (componentSchema.getPreferredPhysMaxValue() - componentSchema.getPreferredPhysMinValue()));
        fireStateChanged();
    }

    @Override
    public void setComponentSchema(String componentName)
    {
        if (fireOnUpdate) {
            super.setComponentSchema(componentName);
            updateComponentValue();
        }
    }

    @Override
    public void setContainer(DataContainer container)
    {
        setContainerSchema(container.getSchema());
    }

    @Override
    protected void localUpdateUI()
    {
       if (ui != null && ui instanceof ComponentValueUI)
          valUI = (ComponentValueUI)ui;
       valUI.setListener(uiValueChangedListener);
    }

    @Override
    public void reset()
    {
        if (componentSchema != null)
        {
            setPhysicalValue((componentPhysMin + componentPhysMax) / 2);
            if (valUI != null)
            {
                valUI.updateRange();
                valUI.updateValue();
            }
        }
    }

    @Override
    protected void localUpdateComponent()
    {
        if (componentSchema == null)
            return;
        if (oldComponentNameInvalid ||
            physicalValue < componentPhysMin ||
            physicalValue > componentPhysMax)
            setPhysicalValue((componentPhysMin + componentPhysMax) / 2);
        if (valUI == null)
           return;
        SwingInstancer.swingRunLater(() -> {
            valUI.updateRange();
            valUI.updateValue();
        });

    }

    @Override
    public String toString()
    {
        if (componentSchema == null)
            return "[componentValue][/componentValue]";
        return "[componentValue]"+componentSchema.getName()+
                String.format(": %7.3f", value) +
               "[/componentValue]";
    }

    @Override
    public void updateFromString(String s)
    {
        s = s.trim();
        if (s == null || s.isEmpty() ||
           !s.startsWith("[componentValue]") || !s.endsWith("[/componentValue]"))
            return;
        String[] c = s.substring("[componentValue]".length(), s.length() - "[/componentValue]".length()).trim().split(" *:* +");
        setComponentSchema(c[0]);
        setPhysicalValue(Float.parseFloat(c[1]));
    }
}
