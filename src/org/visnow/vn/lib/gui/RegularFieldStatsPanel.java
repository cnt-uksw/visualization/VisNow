/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;
import static org.apache.commons.math3.util.FastMath.*;
import org.apache.log4j.Logger;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.utils.EngineeringFormattingUtils;
import static org.visnow.vn.lib.basic.utilities.FieldStats.FieldStatsShared.*;
import org.visnow.vn.engine.core.Parameters;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class RegularFieldStatsPanel extends javax.swing.JPanel
{
    private static final Logger LOGGER = Logger.getLogger(RegularFieldStatsPanel.class);

    class GraphArea extends JPanel
    {

        double[] histo = null;
        double minValue, maxValue;
        boolean logScale = false;
        int valueLinePosition = -1;

        public GraphArea() {
            //mouse listener for tooltip and line
            addMouseMotionListener(new MouseAdapter()
            {
                @Override
                public void mouseMoved(MouseEvent e)
                {                    
                    valueLinePosition = e.getX();
                    repaint();
                    //setToolTipText("<html>count: " + histogramValue + "<br/>at: [" + labels[0] + ", " + labels[1] + "]");
                }
            });        
            
            addMouseListener(new MouseAdapter() {
                @Override
                public void mouseExited(MouseEvent e) {
                    valueLinePosition = -1;
                    repaint();
                }                                
            });
        }                

        @Override
        public void paint(Graphics g)
        {
            if (histo != null) {
                int width = getWidth();
                int height = getHeight();
                int margin = 2;
                int binCount = histo.length;

                g.setColor(Color.WHITE);
                g.fillRect(0, 0, width, height);
                double max = 0;
                for (int i = 0; i < histo.length; i++) {
                    if (max < histo[i])
                        max = histo[i];
                }
                double norm = max/1000; //for log scale normalisation                
                if (logScale) {
                    max = 0;
                    for (int i = 0; i < histo.length; i++)
                        if (max < log1p((double) histo[i]/norm))
                            max = log1p((double) histo[i]/norm);
                } 
                
                max = (float) (height - 2 * margin) / (max + .01);
                float d = (float) (width - 2 * margin) / histo.length;
                g.setColor(Color.BLUE);
                int[] xPoints = new int[histo.length];
                int[] yPoints = new int[histo.length];
                if (logScale) {
                    for (int i = 0; i < histo.length; i++) {
                        xPoints[i] = (int) (i * d + margin);
                        yPoints[i] = height - margin - (int) (max * log1p((double) histo[i]/norm));
                    }
                } else
                    for (int i = 0; i < histo.length; i++) {
                        xPoints[i] = (int) (i * d + margin);
                        yPoints[i] = height - margin - (int) (max * histo[i]);
                    }
                g.drawPolyline(xPoints, yPoints, histo.length);
                
                if(valueLinePosition > 0) {
                    int dst,mdst=histo.length,mi=0;
                    for (int i = 0; i < histo.length; i++) {
                        dst = abs(valueLinePosition-xPoints[i]);
                        if(dst < mdst) {
                            mdst = dst;
                            mi = i;
                        }
                    } 
                    g.setColor(Color.RED);
                    g.drawLine(xPoints[mi], height-margin, xPoints[mi], yPoints[mi]);
                    g.fillOval(xPoints[mi]-3, yPoints[mi]-3, 6, 6);
                    
                    float xv = (float) (minValue+mi*(maxValue-minValue)/(binCount-1));
                    setToolTipText("<html>at value: " + EngineeringFormattingUtils.format(xv) + "</html>");
                }
            }
        }

        public void setHisto(double[] histo, boolean logScale, double minValue, double maxValue)
        {
            this.histo = histo;
            this.logScale = logScale;
            this.minValue = minValue;
            this.maxValue = maxValue;
            repaint();
        }
    }

    GraphArea thrArea = new GraphArea();

    Parameters params;
    String name;
    int[] dims;
    float[][] fieldExtends;
    float[][] physExtends;
    String[] componentNames;
    DataContainerSchema schema;
    double[] avgGrad;
    double[] stdDevGrad;
    long[][] derivHistograms;
    double[][] thrHistograms;
    long[][] histograms;

    /**
     * Creates new form RegularFieldStatsPanel
     */
    public RegularFieldStatsPanel()
    {
        initComponents();
        thresholdPreferencesPanel.add(thrArea, BorderLayout.CENTER);
    }

    public void setProperties(Parameters params, int selectedComponentIndex, boolean logScale, String name, int[] dims, float[][] fieldExtends, float[][] physExtends, String[] componentNames, DataContainerSchema schema, long[][] histograms, double[] avgGrad, double[] stdDevGrad, long[][] derivHistograms, double[][] thrHistograms)
    {
        this.params = params;
        this.name = name;
        this.dims = dims;
        this.fieldExtends = fieldExtends;
        this.physExtends = physExtends;
        this.componentNames = componentNames;
        this.schema = schema;
        this.histograms = histograms;
        this.avgGrad = avgGrad;
        this.stdDevGrad = stdDevGrad;
        this.derivHistograms = derivHistograms;
        this.thrHistograms = thrHistograms;
        fieldNameLabel.setText(name);
        StringBuilder dBuf = new StringBuilder("" + dims[0]);
        for (int i = 1; i < dims.length; i++)
            dBuf.append("x").append(dims[i]);
        dimsLabel.setText(dBuf.toString());
        extLabel.setText(String.format("[%4.1f:%4.1f]x[%4.1f:%4.1f]x[%4.1f:%4.1f]",
                                       fieldExtends[0][0], fieldExtends[1][0],
                                       fieldExtends[0][1], fieldExtends[1][1],
                                       fieldExtends[0][2], fieldExtends[1][2]));
        physExtLabel.setText(String.format("[%4.1f:%4.1f]x[%4.1f:%4.1f]x[%4.1f:%4.1f]",
                                           physExtends[0][0], physExtends[1][0],
                                           physExtends[0][1], physExtends[1][1],
                                           physExtends[0][2], physExtends[1][2]));
        componentsBox.setModel(new javax.swing.DefaultComboBoxModel(componentNames));
        setSelectedComponentIndex(selectedComponentIndex);
        setLogScale(logScale);
        binCountCB.setSelectedItem(params.get(BIN_COUNT));
        show(selectedComponentIndex, params.get(BIN_COUNT));
    }

    private void show(int n, int binCount)
    {
        if (n >= 0 && n < componentNames.length) {

//            double[] minMaxMean
            String[] minMeanMax = EngineeringFormattingUtils.formatInContext(new double[]{schema.getComponentSchema(n).getMinValue(), schema.getComponentSchema(n).getMeanValue(), schema.getComponentSchema(n).getMaxValue()});
            vMinLabel.setText(minMeanMax[0]);
            vMeanLabel.setText(minMeanMax[1]);
            vMaxLabel.setText(minMeanMax[2]);
            vSDLabel.setText(EngineeringFormattingUtils.format(schema.getComponentSchema(n).getStandardDeviationValue()));
            if(avgGrad != null) {
                gradLabel.setVisible(true);
                gradALabel.setVisible(true);
                gradSDLabel.setVisible(true);
                dMeanLabel.setVisible(true);
                dSDLabel.setVisible(true);
                dMeanLabel.setText(EngineeringFormattingUtils.format(avgGrad[n]));
                dSDLabel.setText(EngineeringFormattingUtils.format(stdDevGrad[n]));
            }
            else {
                gradLabel.setVisible(false);
                gradALabel.setVisible(false);
                gradSDLabel.setVisible(false);
                dMeanLabel.setVisible(false);
                dSDLabel.setVisible(false);
            }
            //assuming histograms of length 256 
            long[] valueHistogram = histograms[n];
            int downsize = 256 / binCount;
            long[] valueHistogramDownsized = new long[binCount];
            int k = 0;
            for (int i = 0; i < binCount; i++) {
                for (int j = 0; j < downsize; j++) {
                    valueHistogramDownsized[i] += valueHistogram[k];
                    k++;
                }
            }
            valueHistogramPlot.setData(valueHistogramDownsized, logBox.isSelected(), schema.getComponentSchema(n).getMinValue(), schema.getComponentSchema(n).getMaxValue());
            if(derivHistograms != null) {
                long[] derivHistogram = derivHistograms[n];
                long[] derivHistogramDownsized = new long[binCount];
                k = 0;
                for (int i = 0; i < binCount; i++) {
                    for (int j = 0; j < downsize; j++) {
                        derivHistogramDownsized[i] += derivHistogram[k];
                        k++;
                    }
                }
                jLabel8.setVisible(true);
                derivHistoPanel.setVisible(true);
                derivativeHistogramPlot.setVisible(true);
                derivativeHistogramPlot.setData(derivHistogramDownsized, logBox.isSelected());
            }
            else {
                jLabel8.setVisible(false);
                derivHistoPanel.setVisible(true);
                derivativeHistogramPlot.setVisible(false);
            }
            if(thrHistograms != null) {
                jLabel9.setVisible(true);
                thresholdPreferencesPanel.setVisible(true);
                thrArea.setVisible(true);
                thrArea.setHisto(thrHistograms[n], logBox.isSelected(), schema.getComponentSchema(n).getMinValue(), schema.getComponentSchema(n).getMaxValue());
            }
            else {
                jLabel9.setVisible(false);
                thresholdPreferencesPanel.setVisible(false);
                thrArea.setVisible(false);                
            }
        }
    }

    public int getSelectedComponentIndex()
    {
        return componentsBox.getSelectedIndex();
    }

    public void setSelectedComponentIndex(int idx)
    {
        componentsBox.setSelectedIndex(idx);
    }

    public boolean isLogScale()
    {
        return logBox.isSelected();
    }

    public void setLogScale(boolean logScale)
    {
        logBox.setSelected(logScale);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        valLabel = new javax.swing.JLabel();
        valALabel = new javax.swing.JLabel();
        valSDLabel = new javax.swing.JLabel();
        gradLabel = new javax.swing.JLabel();
        gradALabel = new javax.swing.JLabel();
        gradSDLabel = new javax.swing.JLabel();
        valHistoPanel = new javax.swing.JPanel();
        valueHistogramPlot = new org.visnow.vn.lib.gui.HistogramPlot();
        jLabel7 = new javax.swing.JLabel();
        derivHistoPanel = new javax.swing.JPanel();
        derivativeHistogramPlot = new org.visnow.vn.lib.gui.HistogramPlot();
        jLabel8 = new javax.swing.JLabel();
        vMeanLabel = new javax.swing.JLabel();
        vSDLabel = new javax.swing.JLabel();
        binCountCB = new org.visnow.vn.gui.swingwrappers.ComboBox();
        dMeanLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        dSDLabel = new javax.swing.JLabel();
        componentsBox = new javax.swing.JComboBox();
        logBox = new javax.swing.JCheckBox();
        valMinLabel = new javax.swing.JLabel();
        valMaxLabel = new javax.swing.JLabel();
        vMinLabel = new javax.swing.JLabel();
        vMaxLabel = new javax.swing.JLabel();
        thresholdPreferencesPanel = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        fieldNameLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        dimsLabel = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        extLabel = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        physExtLabel = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createEmptyBorder(4, 4, 4, 4));
        setLayout(new java.awt.GridBagLayout());

        valLabel.setText("Values:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        add(valLabel, gridBagConstraints);

        valALabel.setText("Mean:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(valALabel, gridBagConstraints);

        valSDLabel.setText("Std. dev.:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(valSDLabel, gridBagConstraints);

        gradLabel.setText("Derivatives:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        add(gradLabel, gridBagConstraints);

        gradALabel.setText("Mean:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        add(gradALabel, gridBagConstraints);

        gradSDLabel.setText("Std. dev.:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(gradSDLabel, gridBagConstraints);

        valHistoPanel.setBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.disabledText")));
        valHistoPanel.setPreferredSize(new java.awt.Dimension(100, 100));
        valHistoPanel.setLayout(new java.awt.BorderLayout());
        valHistoPanel.add(valueHistogramPlot, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(valHistoPanel, gridBagConstraints);

        jLabel7.setText("Value histogram:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 1, 0);
        add(jLabel7, gridBagConstraints);

        derivHistoPanel.setBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.disabledText")));
        derivHistoPanel.setPreferredSize(new java.awt.Dimension(100, 100));
        derivHistoPanel.setLayout(new java.awt.BorderLayout());
        derivHistoPanel.add(derivativeHistogramPlot, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(derivHistoPanel, gridBagConstraints);

        jLabel8.setText("Derivatives histogram:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 1, 0);
        add(jLabel8, gridBagConstraints);

        vMeanLabel.setText(" ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(vMeanLabel, gridBagConstraints);

        vSDLabel.setText(" ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(vSDLabel, gridBagConstraints);

        binCountCB.setListData(new Object[]{8, 16, 32, 64, 128, 256});
        binCountCB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                binCountCBUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(binCountCB, gridBagConstraints);

        dMeanLabel.setText(" ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        add(dMeanLabel, gridBagConstraints);

        jLabel1.setText("Bin count:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 4);
        add(jLabel1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(dSDLabel, gridBagConstraints);

        componentsBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        componentsBox.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                componentsBoxItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 4, 0);
        add(componentsBox, gridBagConstraints);

        logBox.setText("Log scale");
        logBox.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        logBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                logBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        add(logBox, gridBagConstraints);

        valMinLabel.setText("Min:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(valMinLabel, gridBagConstraints);

        valMaxLabel.setText("Max:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(valMaxLabel, gridBagConstraints);

        vMinLabel.setText(" ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(vMinLabel, gridBagConstraints);

        vMaxLabel.setText(" ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(vMaxLabel, gridBagConstraints);

        thresholdPreferencesPanel.setBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.disabledText")));
        thresholdPreferencesPanel.setPreferredSize(new java.awt.Dimension(100, 100));
        thresholdPreferencesPanel.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 15;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(thresholdPreferencesPanel, gridBagConstraints);

        jLabel9.setText("Threshold preferences (smaller better)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 1, 0);
        add(jLabel9, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(fieldNameLabel, gridBagConstraints);

        jLabel2.setText("Dimensions:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(jLabel2, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(dimsLabel, gridBagConstraints);

        jLabel4.setText("Extents:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(jLabel4, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(extLabel, gridBagConstraints);

        jLabel6.setText("Physical extents:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(jLabel6, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(physExtLabel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        add(jPanel1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void logBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logBoxActionPerformed
        show(componentsBox.getSelectedIndex(), params.get(BIN_COUNT));
        params.setParameterActive(false);
        params.set(LOG_SCALE, logBox.isSelected());
        params.setParameterActive(true);
    }//GEN-LAST:event_logBoxActionPerformed

    private void componentsBoxItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_componentsBoxItemStateChanged
    {//GEN-HEADEREND:event_componentsBoxItemStateChanged
        show(componentsBox.getSelectedIndex(), params.get(BIN_COUNT));
        params.setParameterActive(false);
        params.set(SELECTED_COMPONENT, componentsBox.getSelectedIndex());
        params.setParameterActive(true);
    }//GEN-LAST:event_componentsBoxItemStateChanged

    private void binCountCBUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_binCountCBUserChangeAction
    {//GEN-HEADEREND:event_binCountCBUserChangeAction
        if (params != null) {
            params.set(BIN_COUNT, (int) binCountCB.getSelectedItem());
            show(componentsBox.getSelectedIndex(), params.get(BIN_COUNT));
        }
    }//GEN-LAST:event_binCountCBUserChangeAction

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.visnow.vn.gui.swingwrappers.ComboBox binCountCB;
    private javax.swing.JComboBox componentsBox;
    private javax.swing.JLabel dMeanLabel;
    private javax.swing.JLabel dSDLabel;
    private javax.swing.JPanel derivHistoPanel;
    private org.visnow.vn.lib.gui.HistogramPlot derivativeHistogramPlot;
    private javax.swing.JLabel dimsLabel;
    private javax.swing.JLabel extLabel;
    private javax.swing.JLabel fieldNameLabel;
    private javax.swing.JLabel gradALabel;
    private javax.swing.JLabel gradLabel;
    private javax.swing.JLabel gradSDLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JCheckBox logBox;
    private javax.swing.JLabel physExtLabel;
    private javax.swing.JPanel thresholdPreferencesPanel;
    private javax.swing.JLabel vMaxLabel;
    private javax.swing.JLabel vMeanLabel;
    private javax.swing.JLabel vMinLabel;
    private javax.swing.JLabel vSDLabel;
    private javax.swing.JLabel valALabel;
    private javax.swing.JPanel valHistoPanel;
    private javax.swing.JLabel valLabel;
    private javax.swing.JLabel valMaxLabel;
    private javax.swing.JLabel valMinLabel;
    private javax.swing.JLabel valSDLabel;
    private org.visnow.vn.lib.gui.HistogramPlot valueHistogramPlot;
    // End of variables declaration//GEN-END:variables

}
