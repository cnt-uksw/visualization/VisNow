/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.gui.FieldBasedUI.SubsetGUI;

import java.util.Arrays;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;

/**
 *
 * @author know
 */


public class SubsetParams extends Parameters
{
    public static final int ALWAYS_RESET = 0;
    public static final int KEEP_IF_INSIDE = 1;
    
    protected int policy = KEEP_IF_INSIDE;
    protected long preferredSize = 100;
    protected long minSize = 1;
    protected long maxSize = 10000;
    protected int[] dims;
    
    protected long nNodes;
    protected boolean regularAvailable = false;
    protected boolean adjusting = false;
    protected SubsetUI ui = null;
    
    public static final String RANDOM      = "random";
    public static final String SUBSET_SIZE = "subset size";
    public static final String DOWN        = "downsize";
    public static final String CENTERED    = "centered";
    
    protected static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Boolean>(RANDOM,      ParameterType.dependent, true),
        new ParameterEgg<Integer>(SUBSET_SIZE, ParameterType.dependent, 100),
        new ParameterEgg<int[]>(DOWN,          ParameterType.dependent, new int[]{10,10,10}),
        new ParameterEgg<Boolean>(CENTERED,    ParameterType.dependent, true),
    };

    public SubsetParams(long minSize, long  maxSize, long preferredSize, boolean random, boolean centered)
    {
        super(eggs);
        this.preferredSize = preferredSize;
        this.minSize = minSize;
        this.maxSize = maxSize;
    }
    
    public SubsetParams(long minSize, long  maxSize, long preferredSize, boolean random)
    {
        this(preferredSize, minSize, maxSize, random, true);
    }
    
    public SubsetParams(int minSize, int maxSize, boolean random)
    {
        this((int)Math.sqrt(maxSize * minSize), minSize, maxSize, random);
    }
    

    public SubsetUI getUI()
    {
        return ui;
    }

    public void setUI(SubsetUI ui)
    {
        this.ui = ui;
        ui.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                fireStateChanged();
            }
        });
    }

    public void setPolicy(int policy)
    {
        this.policy = policy;
    }

    public boolean isAdjusting()
    {
        return adjusting;
    }

    public void setAdjusting(boolean adjusting)
    {
        this.adjusting = adjusting;
    }
    
    public void setFieldData(long nNodes, int[] dims)
    {
        
        this.nNodes = nNodes;
        this.dims = dims;
        if (ui != null)
            ui.updateFromParams();
    }
    
    public boolean isRegularAvailable()
    {
        return regularAvailable;
    }
    
    public int[] getDown()
    {
        return (int[])getValue(DOWN);
    }

    public void setDown(int[] down)
    {
        setValue(DOWN, down);
        if (ui != null)
            ui.updateFromParams();
        fireParameterChanged(DOWN);
    }

    public long getPreferredSize()
    {
        return preferredSize;
    }

    public long getMinSize()
    {
        return minSize;
    }

    public long getMaxSize()
    {
        return maxSize;
    }

    public void setPreferredSize(long preferredSize)
    {
        this.preferredSize = preferredSize;
    }

    public void setSizes(long minSize, long maxSize, long preferredSize)
    {
        this.minSize = minSize;
        this.maxSize = maxSize;
        this.preferredSize = preferredSize;
    }

    public int getSubsetSize()
    {
        return (Integer)getValue(SUBSET_SIZE);
    }
    public void setSubsetSize(int subsetSize)
    {
        int subSize = subsetSize;
        if (dims != null) {
            long n = 1;
            int[] down = new int[dims.length];
            Arrays.fill(down, 1);
            for (int dim : dims)
                n *= dim;
            if (subsetSize < n / 2) {
                int d = (int)(Math.pow(n / (double)subsetSize, 1./dims.length) + .5);

                subSize = 1;
                for (int i = 0; i < down.length; i++) {
                    down[i] = d;
                    subSize *= Math.max(2, dims[i] / d);
                }
            }
            parameterActive = false;
            setDown(down);
            parameterActive = true;
        }
        setValue(SUBSET_SIZE, subSize);
    }

    public boolean isRandom()
    {
        return (Boolean)getValue(RANDOM) || !regularAvailable;
    }

    public void setRandom(boolean random)
    {
        setValue(RANDOM, random);
    }
    
    public boolean isCentered()
    {
        return (Boolean)getValue(CENTERED);
    }

    public void setCentered(boolean centered)
    {
        setValue(CENTERED, centered);
    }
    
    public String[] valuesToStringArray()
    {
        int[] subsetDims = (int[])getValue(DOWN);
        if (dims != null && subsetDims != null && !(Boolean)getValue(RANDOM)) {
            switch (subsetDims.length) {
                case 1:
                    return new String[] {"random: false", 
                                         String.format("dims: {%3d}", 
                                                       subsetDims[0]),
                                         String.format("centered: %b", (Boolean)getValue(CENTERED))};
                case 2:
                    return new String[] {"random: false", 
                                         String.format("dims: {%3d %3d}", 
                                                        subsetDims[0], subsetDims[1]),
                                         String.format("centered: %b", (Boolean)getValue(CENTERED))};
                case 3:
                    return new String[] {"random: false", 
                                         String.format("dims: {%3d %3d %3d}", 
                                                        subsetDims[0], subsetDims[1], subsetDims[2]),
                                         String.format("centered: %b", (Boolean)getValue(CENTERED))};
            }
        }
        return new String[] {String.format("random: %b",getValue(RANDOM) ), 
                             String.format("size: %6d", getValue(SUBSET_SIZE))};
    }
    
    public void restoreFromStringArray(String[] input)
    {
        setActive(false);
        if (input == null || input.length < 2)
            return;
        try {
            String[] tokens;
            for (String in : input) 
                if (in != null && !in.isEmpty()){
                    tokens = in.trim().toLowerCase().split(" *: +");
                    if (tokens[0].equalsIgnoreCase("random"))
                        setValue(RANDOM, tokens[1].charAt(0) == 't');
                    else if (tokens[0].equalsIgnoreCase("size"))
                         setValue(SUBSET_SIZE, Integer.parseInt(tokens[1]));
                    else if (tokens[0].equalsIgnoreCase("dims")){
                        String t = tokens[1].replaceAll("\\{|\\}", "");
                        tokens = t.trim().split(" +");
                        int[] subsetDims = new int[tokens.length];
                        for (int i = 0; i < subsetDims.length; i++) 
                            subsetDims[i] = Integer.parseInt(tokens[i]);
                        setValue(DOWN, subsetDims);
                    }
                    else if (tokens[0].equalsIgnoreCase("centered"))
                        setValue(CENTERED, tokens[1].charAt(0) == 't');
            }
            if (ui != null)
                ui.updateFromParams();
        } catch (Exception e) {
        }
        setActive(true);
    }
}
