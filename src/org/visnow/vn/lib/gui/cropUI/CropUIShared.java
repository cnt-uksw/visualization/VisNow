/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.gui.cropUI;

import java.util.ArrayList;
import java.util.List;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.engine.core.ParameterProxy;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class CropUIShared
{
    private static final String NAMESPACE = "CropUI.";
    //Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    //
    //Specification:
    //low.length == high.length == 3, 
    //0 <= LOW[i] < HIGH[i] < META_FIELD_DIMENSION_LENGTHS[i]
    //(when i < META_FIELD_DIMENSION_LENGTHS.length)    
    public static final ParameterName<int[]> LOW = new ParameterName(NAMESPACE + "Low values");
    public static final ParameterName<int[]> HIGH = new ParameterName(NAMESPACE + "High values");

    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic
    //not null array of integers, length = 1..3, values >=2
    public static final ParameterName<int[]> META_FIELD_DIMENSION_LENGTHS = new ParameterName(NAMESPACE + "META dimension lengths");

    public static List<Parameter> createDefaultParametersAsList()
    {
        return new ArrayList<Parameter>()
        {
            {
                add(new Parameter<>(META_FIELD_DIMENSION_LENGTHS, new int[]{100, 100, 100}));
                add(new Parameter<>(LOW, new int[]{0, 0, 0}));
                add(new Parameter<>(HIGH, new int[]{99, 99, 99}));
            }
        };
    }

    private static void validateLowHigh(int[] low, int[] high, int[] dimensionLengths)
    {
        for (int i = 0; i < low.length; i++) {
            low[i] = Math.min(Math.max(low[i], 0), dimensionLengths[i] - 2);
            high[i] = Math.min(Math.max(high[i], low[i] + 1), dimensionLengths[i] - 1);
        }
    }

    /**
     * Validates low and high ranges to meet:
     * 0 &le; CROP_UI_LOW[i] &le; CROP_UI_HIGH[i] &lt; META_CROP_UI_FIELD_DIMENSION_LENGTHS[i]
     */
    public static void validateLowHighRange(ParameterProxy p)
    {
        int[] dimensionLengths = p.get(META_FIELD_DIMENSION_LENGTHS);
        int[] low = p.get(LOW);
        int[] high = p.get(HIGH);
        validateLowHigh(low, high, dimensionLengths);
        p.set(LOW, low,
              HIGH, high);
    }

    /**
     * Resets low and high ranges to meet:
     * 0 == CROP_UI_LOW[i], CROP_UI_HIGH[i] = META_CROP_UI_FIELD_DIMENSION_LENGTHS[i]
     */
    public static void resetLowHighRange(ParameterProxy p)
    {
        int[] dimensionLengths = p.get(META_FIELD_DIMENSION_LENGTHS);

        int[] high = new int[3];
        for (int i = 0; i < high.length; i++)
            high[i] = dimensionLengths[i] - 1;

        p.set(LOW, new int[]{0, 0, 0},
              HIGH, high);
    }

    /**
     * Rescales low, high and dimension_lengths, by factor dimensionLengths/oldDimensionLengths
     */
    public static void rescaleAll(ParameterProxy p, int[] oldDimensionLengths)
    {
        int[] dimensionLengths = p.get(META_FIELD_DIMENSION_LENGTHS);
        int[] low = p.get(LOW);
        int[] high = p.get(HIGH);

        for (int i = 0; i < low.length; i++) {
            float scale = (float) dimensionLengths[i] / oldDimensionLengths[i];
            low[i] *= scale;
            high[i] *= scale;
        }

        validateLowHigh(low, high, dimensionLengths);

        p.set(LOW, low,
              HIGH, high);
    }
}
