/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.curves;

import org.jogamp.vecmath.Point3d;

/**
 * This class provides utilities to calculate cubic spline interpolation.
 * 
 * @author Norbert Kapiński (norkap@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 
 * 
 */


public class Cubic {
    
    /**
     * Creates an instance of NaturalCubic class
     */
    
    public Cubic(){

    }
    
    /**
     * This method calculates splines coefficient in 3D space.
     * @param n number of splines to be calculated
     * @param points interpolation key points
     * @return array of splines coefficients
     * [] - splines1D
     * [][] - spline1D
     * [][][] - spline coefficients
     */
    
    public static float[][][] calcCubic3D(int n, Point3d[] points){

        int nPoints = points.length;
     
        float[] X = new float[nPoints];
        float[] Y = new float[nPoints];
        float[] Z = new float[nPoints];
        
        for (int i = 0; i < nPoints; i++) {
            X[i] = (float)points[i].x;
            Y[i] = (float)points[i].y;
            Z[i] = (float)points[i].z;
        }

        return new float[][][]{
            calcCubic1D(n, X),
            calcCubic1D(n, Y),
            calcCubic1D(n, Z)
        };
    }
    
    
  /**
   * calculates the cubic spline that interpolates y[0], y[1], ... y[n]
   * The first spline coefficients are returned in the array C[][] where
   * C[0][0] + C[0][1]*u + C[0][2]*u^2 + C[0][3]*u^3 0<=u <1
   * the other splines coefficients are in C[1], C[2], ...  C[n-1]  
   * @param n number of splines
   * @param x points coordinates
   * @return splines 
   */

    public static float[][] calcCubic1D(int n, float[] x) {

        if(x.length<4){
            throw new IllegalArgumentException("Incorrect number of interpolation points = "+x.length+"; number of interpolation points must be greater than 3");
        }
        if(n<1||n>x.length-1){
            throw new IllegalArgumentException("Incorrect number of splines = "+n+"; number of splines must be greater than 0 and lower "+x.length);
        }
        float[] gamma = new float[n+1];
        float[] delta = new float[n+1];
        float[] D = new float[n+1];
        int i;
        /* Solve the equation
           [2 1       ] [D[0]]   [3(x[1] - x[0])  ]
           |1 4 1     | |D[1]|   |3(x[2] - x[0])  |
           |  1 4 1   | | .  | = |      .         |
           |    ..... | | .  |   |      .         |
           |     1 4 1| | .  |   |3(x[n] - x[n-2])|
           [       1 2] [D[n]]   [3(x[n] - x[n-1])]

           by using row operations to convert the matrix to upper triangular
           and then back sustitution.  The D[i] are the derivatives at the knots.
           */

        gamma[0] = 1.0f/2.0f;
        for ( i = 1; i < n; i++) {
          gamma[i] = 1/(4-gamma[i-1]);
        }
        gamma[n] = 1/(2-gamma[n-1]);

        delta[0] = 3*(x[1]-x[0])*gamma[0];
        for ( i = 1; i < n; i++) {
          delta[i] = (3*(x[i+1]-x[i-1])-delta[i-1])*gamma[i];
        }
        delta[n] = (3*(x[n]-x[n-1])-delta[n-1])*gamma[n];

        D[n] = delta[n];
        for ( i = n-1; i >= 0; i--) {
          D[i] = delta[i] - gamma[i]*D[i+1];
        }

        /* now compute the coefficients of the cubics */
        float[][] C = new float[n][];

        for ( i = 0; i < n; i++) {
          C[i] = new float[]{(float)x[i], D[i], 3*(x[i+1] - x[i]) - 2*D[i] - D[i+1],
                           2*(x[i] - x[i+1]) + D[i] + D[i+1]};
        }
        return C;
      }

/**
 * This method calculates f(u) value based on a spline coefficients
 * @param c spline coefficient 
 * @param u 0<=u <1
 * @return f(u)
 */    
    
  public static float eval(float[] c, float u) {
    return (((c[3]*u) + c[2])*u + c[1])*u + c[0];
  }
    
}
