/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.curves;

import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author babor
 */
public class Bezier
{

    private float[][] points;

    public Bezier(float[][] points)
    {
        this.points = points;
    }

    public float[] bezier(float t)
    {
        return bezier(points, t);
    }

    public float[] bezierDerivative(float t)
    {
        return bezierDerivative(points, t);
    }

    public static float[] bezier(float[][] points, float t)
    {
        if (points == null)
            return null;

        int n = points.length - 1;
        int nDims = points[0].length;
        float[] out = new float[nDims];
        for (int d = 0; d < nDims; d++) {
            out[d] = 0.0f;
            for (int i = 0; i < n + 1; i++) {
                out[d] += points[i][d] * bernstein(n, i, t);
            }
        }
        return out;
    }

    public static float[] bezierDerivative(float[][] points, float t)
    {
        if (points == null)
            return null;

        int n = points.length - 1;
        int nDims = points[0].length;

        float[][] dPoints = new float[n][nDims];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < nDims; j++) {
                dPoints[i][j] = n * (points[i + 1][j] - points[i][j]);
            }
        }

        float[] out = new float[nDims];
        for (int d = 0; d < nDims; d++) {
            out[d] = 0.0f;
            for (int i = 0; i < n; i++) {
                out[d] += dPoints[i][d] * bernstein(n - 1, i, t);
            }
        }
        return out;
    }

    public static double bernstein(int n, int i, float u)
    {
        return binomial(n, i) * pow(u, i) * pow(1.0f - u, n - i);
    }

    public static int factorial(int n)
    {
        int ret = 1;
        for (int i = 1; i <= n; ++i)
            ret *= i;
        return ret;
    }

    public static float binomial(int n, int i)
    {
        return (float) factorial(n) / (float) (factorial(i) * factorial(n - i));
    }

}
