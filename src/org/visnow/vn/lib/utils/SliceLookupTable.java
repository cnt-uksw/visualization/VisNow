/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils;

import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class SliceLookupTable
{
    /*
     * encode distribution of values of slice equation function as 3-base number 
     * with i-th digit 0 for zero value at i-th node, 1 for negative and 2 for positive value
     * the code will be used to look in the table of sections
     */

    public static int simplexCode(float[] v)
    {
        int code = 0;
        for (int i = 0, k = 1; i < v.length; i++, k *= 3)
            if (v[i] < 0)
                code += k;
            else if (v[i] > 0)
                code += 2 * k;
        return code;
    }

    /*
     * encode distribution of values of slice equation function as 3-base number 
     * if over,
     * i-th digit is 0 for zero value at i-th node, 1 for negative and 2 for positive value
     * else
     * i-th digit is 0 for zero value at i-th node, 1 for positive and 2 for negative value
     * the code will be used to look in the table of sections
     */
    public static int simplexCode(float[] v, boolean over)
    {
        int code = 0;
        if (over) {
            for (int i = 0, k = 1; i < v.length; i++, k *= 3)
                if (v[i] < 0)
                    code += k;
                else if (v[i] > 0)
                    code += 2 * k;
        } else {
            for (int i = 0, k = 1; i < v.length; i++, k *= 3)
                if (v[i] > 0)
                    code += k;
                else if (v[i] < 0)
                    code += 2 * k;
        }
        return code;
    }

    public static final int[][][] addNodes
        = {
            {},
            {
                {}
            },
            {
                {}, {}, {0, 1}
            },
            {
                {}, {}, {}, {0, 1}, {0, 2}, {1, 2}
            },
            {
                {}, {}, {}, {}, {0, 1}, {0, 2}, {0, 3}, {1, 2}, {1, 3}, {2, 3}
            },};

    public static final CellType[][][] slice
        = {
            {
                {CellType.POINT}, {}, {}
            },
            { // node codes (0:0, 1:-, 2:+
                {CellType.POINT, CellType.SEGMENT}, {CellType.SEGMENT}, {CellType.SEGMENT}, //x0
                {CellType.POINT}, {}, {CellType.TRIANGLE}, //x1
                {CellType.POINT}, {CellType.TRIANGLE}, {} //x2
            },
            {
                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE}, {CellType.SEGMENT, CellType.TRIANGLE}, {CellType.SEGMENT, CellType.TRIANGLE}, //x00
                {CellType.POINT, CellType.TRIANGLE}, {}, {CellType.TRIANGLE, CellType.QUAD}, //x10
                {CellType.POINT, CellType.TRIANGLE}, {CellType.TRIANGLE, CellType.QUAD}, {}, //x20
                {CellType.POINT, CellType.SEGMENT}, {}, {CellType.SEGMENT, CellType.TETRA}, //x01
                {}, {}, {CellType.QUAD, CellType.TETRA}, //x11
                {CellType.POINT, CellType.PYRAMID}, {CellType.QUAD, CellType.PYRAMID}, {CellType.TETRA, CellType.PYRAMID}, //x21
                {CellType.POINT, CellType.SEGMENT}, {CellType.SEGMENT, CellType.TETRA}, {}, //x02
                {CellType.POINT, CellType.PYRAMID}, {CellType.TETRA, CellType.PYRAMID}, {CellType.QUAD, CellType.PYRAMID}, //x12
                {}, {CellType.QUAD, CellType.TETRA}, {}, //x22
            },
            {},
            {
                {}, {CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, {CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, //x000
                {CellType.POINT, CellType.TRIANGLE, CellType.QUAD}, {}, {CellType.TRIANGLE, CellType.QUAD, CellType.TETRA}, //x100
                {CellType.POINT, CellType.TRIANGLE, CellType.QUAD}, {CellType.TRIANGLE, CellType.QUAD, CellType.TETRA}, {}, //x200

                {CellType.POINT, CellType.SEGMENT, CellType.QUAD}, {}, {CellType.SEGMENT, CellType.QUAD, CellType.PYRAMID}, //x010
                {}, {}, {CellType.QUAD, CellType.TETRA, CellType.PYRAMID}, //x110
                {CellType.POINT, CellType.QUAD, CellType.HEXAHEDRON}, {CellType.QUAD, CellType.TETRA, CellType.HEXAHEDRON}, {CellType.QUAD, CellType.PYRAMID, CellType.HEXAHEDRON}, //x210

                {CellType.POINT, CellType.SEGMENT, CellType.QUAD}, {CellType.SEGMENT, CellType.QUAD, CellType.PYRAMID}, {}, //x020
                {CellType.POINT, CellType.QUAD, CellType.HEXAHEDRON}, {CellType.QUAD, CellType.PYRAMID, CellType.HEXAHEDRON}, {CellType.QUAD, CellType.TETRA, CellType.HEXAHEDRON}, //x120
                {}, {CellType.QUAD, CellType.TETRA, CellType.PYRAMID}, {}, //x220

                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE}, {}, {CellType.SEGMENT, CellType.TRIANGLE, CellType.PRISM}, //x001
                {}, {}, {CellType.TRIANGLE, CellType.TETRA, CellType.PRISM}, //x101
                {CellType.POINT, CellType.TRIANGLE, CellType.EXPLICIT}, {CellType.TRIANGLE, CellType.TETRA, CellType.EXPLICIT}, {CellType.TRIANGLE, CellType.PRISM, CellType.EXPLICIT}, //x201

                {}, {}, {CellType.SEGMENT, CellType.PYRAMID, CellType.PRISM}, //x011
                {}, {}, {CellType.TETRA, CellType.PYRAMID, CellType.PRISM}, //x111
                {CellType.POINT, CellType.HEXAHEDRON, CellType.EXPLICIT}, {CellType.TETRA, CellType.HEXAHEDRON, CellType.EXPLICIT}, {CellType.PYRAMID, CellType.PRISM, CellType.EXPLICIT, CellType.HEXAHEDRON}, //x211

                {CellType.POINT, CellType.SEGMENT, CellType.CUSTOM}, {CellType.SEGMENT, CellType.PYRAMID, CellType.CUSTOM}, {CellType.POINT, CellType.PRISM, CellType.CUSTOM}, //x021
                {CellType.POINT, CellType.HEXAHEDRON, CellType.CUSTOM}, {CellType.PYRAMID, CellType.HEXAHEDRON, CellType.CUSTOM}, {CellType.TETRA, CellType.PRISM, CellType.CUSTOM, CellType.HEXAHEDRON}, //x121
                {CellType.POINT, CellType.EXPLICIT, CellType.CUSTOM}, {CellType.TETRA, CellType.PYRAMID, CellType.CUSTOM, CellType.EXPLICIT}, {CellType.PRISM, CellType.EXPLICIT, CellType.CUSTOM}, //x221

                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE}, {CellType.SEGMENT, CellType.TRIANGLE, CellType.PRISM}, {}, //x002
                {CellType.POINT, CellType.TRIANGLE, CellType.EXPLICIT}, {CellType.TRIANGLE, CellType.PRISM, CellType.EXPLICIT}, {CellType.TRIANGLE, CellType.TETRA, CellType.EXPLICIT}, //x102
                {}, {CellType.TRIANGLE, CellType.TETRA, CellType.PRISM}, {}, //x202

                {CellType.POINT, CellType.SEGMENT, CellType.CUSTOM}, {CellType.SEGMENT, CellType.PRISM, CellType.CUSTOM}, {CellType.SEGMENT, CellType.PYRAMID, CellType.CUSTOM}, //x012
                {CellType.POINT, CellType.EXPLICIT, CellType.CUSTOM}, {CellType.PRISM, CellType.EXPLICIT, CellType.CUSTOM}, {CellType.TETRA, CellType.PYRAMID, CellType.CUSTOM, CellType.EXPLICIT}, //x112
                {CellType.POINT, CellType.HEXAHEDRON, CellType.CUSTOM}, {CellType.TETRA, CellType.PRISM, CellType.CUSTOM, CellType.HEXAHEDRON}, {CellType.PYRAMID, CellType.HEXAHEDRON, CellType.CUSTOM}, //x212

                {}, {CellType.SEGMENT, CellType.PYRAMID, CellType.PRISM}, {}, //x022
                {CellType.POINT, CellType.HEXAHEDRON, CellType.EXPLICIT}, {CellType.PYRAMID, CellType.PRISM, CellType.EXPLICIT, CellType.HEXAHEDRON}, {CellType.TETRA, CellType.HEXAHEDRON, CellType.EXPLICIT}, //x122
                {}, {CellType.TETRA, CellType.PYRAMID, CellType.PRISM}, {} //x222
            }
        };

    public static final CellType[][] sliceType
        = {
            {
                CellType.POINT, CellType.EMPTY, CellType.EMPTY
            },
            { // node codes (0:0, 1:-, 2:+
                // node codes (0:0, 1:-, 2:+
                CellType.SEGMENT, CellType.POINT, CellType.POINT, //x0
                CellType.POINT, CellType.EMPTY, CellType.POINT, //x1
                CellType.POINT, CellType.POINT, CellType.EMPTY //x2
            },
            {
                CellType.TRIANGLE, CellType.SEGMENT, CellType.SEGMENT, //x00
                CellType.SEGMENT, CellType.EMPTY, CellType.SEGMENT, //x10
                CellType.SEGMENT, CellType.SEGMENT, CellType.EMPTY, //x20
                CellType.SEGMENT, CellType.EMPTY, CellType.SEGMENT, //x01
                CellType.EMPTY, CellType.EMPTY, CellType.SEGMENT, //x11
                CellType.SEGMENT, CellType.SEGMENT, CellType.SEGMENT, //x21
                CellType.SEGMENT, CellType.SEGMENT, CellType.EMPTY, //x02
                CellType.SEGMENT, CellType.SEGMENT, CellType.SEGMENT, //x12
                CellType.EMPTY, CellType.SEGMENT, CellType.EMPTY, //x22
            },
            {},
            {
                CellType.EMPTY, CellType.TRIANGLE, CellType.TRIANGLE,//x000
                CellType.TRIANGLE, CellType.EMPTY, CellType.TRIANGLE,//x100
                CellType.TRIANGLE, CellType.TRIANGLE, CellType.EMPTY, //x200

                CellType.TRIANGLE, CellType.EMPTY, CellType.TRIANGLE,//x010
                CellType.EMPTY, CellType.EMPTY, CellType.TRIANGLE,//x110
                CellType.TRIANGLE, CellType.TRIANGLE, CellType.TRIANGLE,//x210

                CellType.TRIANGLE, CellType.TRIANGLE, CellType.EMPTY, //x020
                CellType.TRIANGLE, CellType.TRIANGLE, CellType.TRIANGLE,//x120
                CellType.EMPTY, CellType.TRIANGLE, CellType.EMPTY, //x220

                CellType.TRIANGLE, CellType.EMPTY, CellType.TRIANGLE, //x001
                CellType.EMPTY, CellType.EMPTY, CellType.TRIANGLE, //x101
                CellType.TRIANGLE, CellType.TRIANGLE, CellType.TRIANGLE, //x201

                CellType.EMPTY, CellType.EMPTY, CellType.TRIANGLE, //x011
                CellType.EMPTY, CellType.EMPTY, CellType.TRIANGLE, //x111
                CellType.TRIANGLE, CellType.TRIANGLE, CellType.QUAD, //x211

                CellType.TRIANGLE, CellType.TRIANGLE, CellType.QUAD, //x021
                CellType.TRIANGLE, CellType.TRIANGLE, CellType.QUAD, //x121
                CellType.TRIANGLE, CellType.QUAD, CellType.TRIANGLE, //x221

                CellType.TRIANGLE, CellType.TRIANGLE, CellType.EMPTY, //x002
                CellType.TRIANGLE, CellType.TRIANGLE, CellType.TRIANGLE, //x102
                CellType.EMPTY, CellType.TRIANGLE, CellType.EMPTY, //x202

                CellType.TRIANGLE, CellType.TRIANGLE, CellType.TRIANGLE, //x012
                CellType.TRIANGLE, CellType.TRIANGLE, CellType.QUAD, //x112
                CellType.TRIANGLE, CellType.QUAD, CellType.TRIANGLE, //x212

                CellType.EMPTY, CellType.TRIANGLE, CellType.EMPTY, //x022
                CellType.TRIANGLE, CellType.QUAD, CellType.TRIANGLE, //x122
                CellType.EMPTY, CellType.TRIANGLE, CellType.EMPTY //x222
            }
        };

    
    public static final CellType[][][] subcell
        = {
            {
                {CellType.POINT}, {}, {CellType.POINT}
            },
            { // node codes (0:0, 1:-, 2:+
                {CellType.POINT, CellType.SEGMENT}, {}, {CellType.POINT, CellType.SEGMENT}, //x0
                {}, {}, {CellType.POINT, CellType.TRIANGLE}, //x1
                {CellType.POINT, CellType.SEGMENT}, {CellType.TRIANGLE, CellType.SEGMENT}, {CellType.POINT, CellType.SEGMENT} //x2
            },
            {
                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE}, {}, {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE}, //x00
                {}, {}, {CellType.POINT, CellType.TRIANGLE, CellType.QUAD}, //x10
                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE}, {CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE}, //x20
                {}, {}, {CellType.POINT, CellType.SEGMENT, CellType.TETRA}, //x01
                {}, {}, {CellType.POINT, CellType.QUAD, CellType.TETRA}, //x11
                {CellType.POINT, CellType.SEGMENT, CellType.PYRAMID}, {CellType.SEGMENT, CellType.QUAD, CellType.PYRAMID}, {CellType.POINT, CellType.SEGMENT, CellType.PYRAMID, CellType.TETRA}, //x21
                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE}, {CellType.SEGMENT, CellType.TRIANGLE, CellType.TETRA}, {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE}, //x02
                {CellType.POINT, CellType.TRIANGLE, CellType.PYRAMID}, {CellType.TRIANGLE, CellType.TETRA, CellType.PYRAMID}, {CellType.POINT, CellType.QUAD, CellType.PYRAMID, CellType.TRIANGLE}, //x12
                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE}, {CellType.SEGMENT, CellType.QUAD, CellType.TETRA, CellType.TRIANGLE}, {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE}, //x22
            },
            {},
            {
                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, {}, {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, //x000
                {}, {}, {CellType.POINT, CellType.TRIANGLE, CellType.QUAD, CellType.TETRA}, //x100
                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, {CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD, CellType.TETRA}, {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, //x200

                {}, {}, {CellType.POINT, CellType.SEGMENT, CellType.QUAD, CellType.PYRAMID}, //x010
                {}, {}, {CellType.POINT, CellType.QUAD, CellType.TETRA, CellType.PYRAMID}, //x110
                {CellType.POINT, CellType.SEGMENT, CellType.QUAD, CellType.HEXAHEDRON}, {CellType.SEGMENT, CellType.QUAD, CellType.TETRA, CellType.HEXAHEDRON}, {CellType.POINT, CellType.SEGMENT, CellType.HEXAHEDRON, CellType.PYRAMID, CellType.QUAD}, //x210

                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, {CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD, CellType.PYRAMID}, {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, //x020
                {CellType.POINT, CellType.TRIANGLE, CellType.QUAD, CellType.HEXAHEDRON}, {CellType.TRIANGLE, CellType.QUAD, CellType.PYRAMID, CellType.HEXAHEDRON}, {CellType.POINT, CellType.TRIANGLE, CellType.HEXAHEDRON, CellType.TETRA, CellType.QUAD}, //x120
                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, {CellType.SEGMENT, CellType.TRIANGLE, CellType.PYRAMID, CellType.TETRA, CellType.QUAD}, {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, //x220

                {}, {}, {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.PRISM}, //x001
                {}, {}, {CellType.POINT, CellType.TRIANGLE, CellType.TETRA, CellType.PRISM}, //x101
                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.EXPLICIT}, {CellType.SEGMENT, CellType.TRIANGLE, CellType.TETRA, CellType.EXPLICIT}, {CellType.POINT, CellType.SEGMENT, CellType.EXPLICIT, CellType.PRISM, CellType.TRIANGLE}, //x201

                {}, {}, {CellType.POINT, CellType.SEGMENT, CellType.PYRAMID, CellType.PRISM}, //x011
                {}, {}, {CellType.POINT, CellType.TETRA, CellType.PYRAMID, CellType.PRISM}, //x111
                {CellType.POINT, CellType.SEGMENT, CellType.HEXAHEDRON, CellType.EXPLICIT}, {CellType.SEGMENT, CellType.TETRA, CellType.HEXAHEDRON, CellType.EXPLICIT}, {CellType.POINT, CellType.PYRAMID, CellType.PRISM, CellType.SEGMENT, CellType.HEXAHEDRON, CellType.EXPLICIT}, //x211

                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.CUSTOM}, {CellType.SEGMENT, CellType.TRIANGLE, CellType.PYRAMID, CellType.CUSTOM}, {CellType.POINT, CellType.TRIANGLE, CellType.CUSTOM, CellType.PRISM, CellType.SEGMENT}, //x021
                {CellType.POINT, CellType.TRIANGLE, CellType.HEXAHEDRON, CellType.CUSTOM}, {CellType.TRIANGLE, CellType.PYRAMID, CellType.HEXAHEDRON, CellType.CUSTOM}, {CellType.POINT, CellType.TETRA, CellType.PRISM, CellType.TRIANGLE, CellType.HEXAHEDRON, CellType.CUSTOM}, //x121
                {CellType.SEGMENT, CellType.TRIANGLE, CellType.CUSTOM, CellType.EXPLICIT, CellType.POINT}, {CellType.SEGMENT, CellType.TETRA, CellType.EXPLICIT, CellType.TRIANGLE, CellType.PYRAMID, CellType.CUSTOM}, {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.PRISM, CellType.EXPLICIT, CellType.CUSTOM}, //x221

                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, {CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD, CellType.PRISM}, {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, //x002
                {CellType.POINT, CellType.TRIANGLE, CellType.QUAD, CellType.EXPLICIT}, {CellType.TRIANGLE, CellType.QUAD, CellType.PRISM, CellType.EXPLICIT}, {CellType.POINT, CellType.QUAD, CellType.EXPLICIT, CellType.TETRA, CellType.TRIANGLE}, //x102
                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, {CellType.SEGMENT, CellType.QUAD, CellType.PRISM, CellType.TETRA, CellType.TRIANGLE}, {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, //x202

                {CellType.POINT, CellType.SEGMENT, CellType.QUAD, CellType.CUSTOM}, {CellType.SEGMENT, CellType.QUAD, CellType.PRISM, CellType.CUSTOM}, {CellType.POINT, CellType.QUAD, CellType.CUSTOM, CellType.PYRAMID, CellType.SEGMENT}, //x012
                {CellType.POINT, CellType.QUAD, CellType.EXPLICIT, CellType.CUSTOM}, {CellType.QUAD, CellType.PRISM, CellType.EXPLICIT, CellType.CUSTOM}, {CellType.POINT, CellType.TETRA, CellType.PYRAMID, CellType.QUAD, CellType.EXPLICIT, CellType.CUSTOM}, //x112
                {CellType.SEGMENT, CellType.QUAD, CellType.CUSTOM, CellType.HEXAHEDRON, CellType.POINT}, {CellType.SEGMENT, CellType.TETRA, CellType.HEXAHEDRON, CellType.QUAD, CellType.PRISM, CellType.CUSTOM}, {CellType.POINT, CellType.SEGMENT, CellType.QUAD, CellType.PYRAMID, CellType.HEXAHEDRON, CellType.CUSTOM}, //x212

                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, {CellType.TRIANGLE, CellType.QUAD, CellType.PRISM, CellType.PYRAMID, CellType.SEGMENT}, {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, //x022
                {CellType.TRIANGLE, CellType.QUAD, CellType.EXPLICIT, CellType.HEXAHEDRON, CellType.POINT}, {CellType.TRIANGLE, CellType.PYRAMID, CellType.HEXAHEDRON, CellType.QUAD, CellType.PRISM, CellType.EXPLICIT}, {CellType.POINT, CellType.TRIANGLE, CellType.QUAD, CellType.TETRA, CellType.HEXAHEDRON, CellType.EXPLICIT}, //x122
                {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD}, {CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD, CellType.TETRA, CellType.PYRAMID, CellType.PRISM}, {CellType.POINT, CellType.SEGMENT, CellType.TRIANGLE, CellType.QUAD} //x222
            }
        };

    public static final CellType[][] subcellType
        = {
            {
                CellType.POINT, CellType.EMPTY, CellType.POINT
            },
            { // node codes (0:0, 1:-, 2:+
                // node codes (0:0, 1:-, 2:+
                CellType.SEGMENT, CellType.EMPTY, CellType.SEGMENT, //x0
                CellType.EMPTY, CellType.EMPTY, CellType.SEGMENT, //x1
                CellType.SEGMENT, CellType.SEGMENT, CellType.SEGMENT //x2
            },
            {
                CellType.TRIANGLE, CellType.EMPTY, CellType.TRIANGLE, //x00
                CellType.EMPTY, CellType.EMPTY, CellType.TRIANGLE, //x10
                CellType.TRIANGLE, CellType.TRIANGLE, CellType.TRIANGLE, //x20
                CellType.EMPTY, CellType.EMPTY, CellType.TRIANGLE, //x01
                CellType.EMPTY, CellType.EMPTY, CellType.TRIANGLE, //x11
                CellType.TRIANGLE, CellType.TRIANGLE, CellType.QUAD, //x21
                CellType.TRIANGLE, CellType.TRIANGLE, CellType.TRIANGLE, //x02
                CellType.TRIANGLE, CellType.TRIANGLE, CellType.QUAD, //x12
                CellType.TRIANGLE, CellType.QUAD, CellType.TRIANGLE, //x22
            },
            {},
            {
                CellType.TETRA, CellType.EMPTY, CellType.TETRA, //x000
                CellType.EMPTY, CellType.EMPTY, CellType.TETRA, //x100
                CellType.TETRA, CellType.TETRA, CellType.TETRA, //x200

                CellType.EMPTY, CellType.EMPTY, CellType.TETRA, //x010
                CellType.EMPTY, CellType.EMPTY, CellType.TETRA, //x110
                CellType.TETRA, CellType.TETRA, CellType.PYRAMID,//x210

                CellType.TETRA, CellType.TETRA, CellType.TETRA, //x020
                CellType.TETRA, CellType.TETRA, CellType.PYRAMID,//x120
                CellType.TETRA, CellType.PYRAMID, CellType.TETRA, //x220

                CellType.EMPTY, CellType.EMPTY, CellType.TETRA, //x001
                CellType.EMPTY, CellType.EMPTY, CellType.TETRA, //x101
                CellType.TETRA, CellType.TETRA, CellType.PYRAMID,//x201

                CellType.EMPTY, CellType.EMPTY, CellType.TETRA, //x011
                CellType.EMPTY, CellType.EMPTY, CellType.TETRA, //x111
                CellType.TETRA, CellType.TETRA, CellType.PRISM, //x211

                CellType.TETRA, CellType.TETRA, CellType.PYRAMID,//x021
                CellType.TETRA, CellType.TETRA, CellType.PRISM, //x121
                CellType.PYRAMID, CellType.PRISM, CellType.PRISM, //x221

                CellType.TETRA, CellType.TETRA, CellType.TETRA, //x002
                CellType.TETRA, CellType.TETRA, CellType.PYRAMID,//x102
                CellType.TETRA, CellType.PYRAMID, CellType.TETRA, //x202

                CellType.TETRA, CellType.TETRA, CellType.PYRAMID,//x012
                CellType.TETRA, CellType.TETRA, CellType.PRISM, //x112
                CellType.PYRAMID, CellType.PRISM, CellType.PRISM, //x212

                CellType.TETRA, CellType.PYRAMID, CellType.TETRA, //x022
                CellType.PYRAMID, CellType.PRISM, CellType.PRISM, //x122
                CellType.TETRA, CellType.PRISM, CellType.TETRA //x222
            }
        };

    public static CellType getSliceType(CellType cellType, float[] vals)
    {
        return sliceType[cellType.getValue()][simplexCode(vals)];
    }

    public static CellType[] getSliceNodes(CellType cellType, float[] vals)
    {
        return slice[cellType.getValue()][simplexCode(vals)];
    }

    public static CellType getSubcellType(CellType cellType, float[] vals, boolean above)
    {
        return subcellType[cellType.getValue()][simplexCode(vals, above)];
    }

    public static CellType[] getSubcellNodes(CellType cellType, float[] vals, boolean above)
    {
        return subcell[cellType.getValue()][simplexCode(vals, above)];
    }

    static int[] v = {0, 1, -1};

    public static void main(String[] args)
    {
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                for (int k = 0; k < 3; k++)
                    for (int l = 0; l < 3; l++)
                        System.out.printf("%d%d%d%d, in%n", l, k, j, i);
        for (int node = 0; node < 10; node++) {
            System.out.printf("%2d %2d ", node + 1, node);
            for (int i = 0, n = 1; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    for (int k = 0; k < 3; k++)
                        for (int l = 0; l < 3; l++)
                            switch (node) {
                                case 0:
                                    System.out.printf("%2d ", v[l]);
                                    break;
                                case 1:
                                    System.out.printf("%2d ", v[k]);
                                    break;
                                case 2:
                                    System.out.printf("%2d ", v[j]);
                                    break;
                                case 3:
                                    System.out.printf("%2d ", v[i]);
                                    break;
                                default:
                                    System.out.printf("%2d ", 0);
                            }
            System.out.println("");
        }
    }
}
