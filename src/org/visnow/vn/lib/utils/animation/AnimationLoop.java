/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.animation;

import org.visnow.vn.geometries.viewer3d.eventslisteners.render.FrameModificationEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.FrameModificationListener;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class AnimationLoop implements Runnable
{

    public static final int ONCE = 0;
    public static final int CYCLE = 1;
    public static final int BOUNCE = 2;
    private Params params = new Params();
    private int counter = 0;
    private float start = 0, incr = 1, stop = 10, value = 0;
    private int firstFrame = 0;
    private int currentFrame = 0;
    private float currentTime = 0;
    private int lastFrame = 100;
    private float tmin = 0, tmax = 1, dt = .01f;
    private float tlow = 0, tup = 1;
    private int delay = 10;
    private int step = 1;
    private int mode = params.STOP;
    private int loopMode = ONCE;
    private boolean renderDone = true;
    private FrameModificationListener frameModificationListener = new FrameModificationListener()
    {
        @Override
        public void frameChanged(FrameModificationEvent e)
        {
            counter = e.getFrame();
            value = start + counter * incr;
            params.setCounter(counter);
            params.setValue(value);
        }
    };

    @Override
    public synchronized void run()
    {
        if (mode == params.STOP)
            return;
        int frame;
        int dir = mode;
        frame = firstFrame;
        animation_loop:
        while (mode != params.STOP) {
            renderDone = false;
            frame += dir * step;
            if (dir == 1 && frame > lastFrame) {
                switch (loopMode) {
                    case ONCE:
                        mode = params.STOP;
                        break animation_loop;
                    case CYCLE:
                        frame = 0;
                        break;
                    case BOUNCE:
                        dir = -dir;
                        frame = lastFrame;
                }
            } else if (dir == -1 && frame < 0) {
                switch (loopMode) {
                    case ONCE:
                        mode = params.STOP;
                        break animation_loop;
                    case CYCLE:
                        frame = lastFrame;
                        break;
                    case BOUNCE:
                        dir = -dir;
                        frame = 0;
                }
            }

            //            setFrame(frame);
            try {
                while (!renderDone)
                    wait(10);
                if (mode == params.STOP)
                    //                  stopAnimation();
                    wait(delay);
            } catch (InterruptedException c) {
                //               stopAnimation();
            }
        }
        //         stopAnimation();
    }

    public AnimationLoop()
    {
    }
}
