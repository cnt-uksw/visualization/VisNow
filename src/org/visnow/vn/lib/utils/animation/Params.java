/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.animation;

import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Params extends Parameters
{

    public static final int FORWARD = 1;
    public static final int STOP = 0;
    public static final int BACK = -1;

    private float d = .1f;

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Integer>("counter", ParameterType.independent, 0),
        new ParameterEgg<Integer>("min", ParameterType.independent, 0),
        new ParameterEgg<Integer>("max", ParameterType.independent, 10),
        new ParameterEgg<Float>("value", ParameterType.independent, 0.f),
        new ParameterEgg<Float>("min value", ParameterType.independent, 0.f),
        new ParameterEgg<Float>("min value", ParameterType.independent, 0.f),
        new ParameterEgg<Float>("max value", ParameterType.independent, 1.f),};

    public Params()
    {
        super(eggs);
    }

    public int getCounter()
    {
        return (Integer) getValue("counter");
    }

    public void setCounter(int counter)
    {
        setValue("counter", counter);
        fireStateChanged();
    }

    public float getValue()
    {
        return (Float) getValue("value");
    }

    public void setValue(float value)
    {
        setValue("value", value);
        fireStateChanged();
    }
}
