/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */

package org.visnow.vn.lib.utils.field;

import java.util.Arrays;
import java.util.Collection;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class MergeIrregularField
{
    private static CellArray merge(CellArray ca0, CellArray ca1, int nodeOffset)
    {
        if (ca0 == null && ca1 == null)
            return null;
        else if (ca0 != null && ca1 == null)
            return ca0.cloneDeep();
        else if (ca0 == null && ca1 != null) {
            if (nodeOffset == 0)
                return ca1.cloneDeep();
            else {
                int[] nodes1 = ca1.getNodes();
                int[] nodes = Arrays.copyOf(nodes1, nodes1.length);
                for (int j = 0; j < nodes1.length; j++)
                    nodes[j] += nodeOffset;
                if (ca1.getDataIndices() == null)
                    return new CellArray(ca1.getType(), nodes,
                               Arrays.copyOf(ca1.getOrientations(), ca1.getNCells()),
                               null);
                else
                    return new CellArray(ca1.getType(), nodes,
                               Arrays.copyOf(ca1.getOrientations(), ca1.getNCells()),
                               Arrays.copyOf(ca1.getDataIndices(), ca1.getNCells()));
            }
        }
        int[] nodes;
        byte[] or;
        int[] nodes0 = ca0.getNodes();
        int[] nodes1 = ca1.getNodes();
        if (nodes0 == null)
            nodes = nodes1;
        else if (nodes1 == null)
            nodes = nodes0;
        else {
            nodes = new int[nodes0.length + nodes1.length];
            System.arraycopy(nodes0, 0, nodes, 0, nodes0.length);
            if (nodeOffset == 0)
                System.arraycopy(nodes1, 0, nodes, nodes0.length, nodes1.length);
            else
                for (int j = 0, k = nodes0.length; j < nodes1.length; j++, k++)
                    nodes[k] = nodes1[j] + nodeOffset;
        }
        byte[] or0 = ca0.getOrientations();
        byte[] or1 = ca1.getOrientations();
        if (or0 == null)
            or = or1;
        else if (or1 == null)
            or = or0;
        else {
            or = new byte[or0.length + or1.length];
            System.arraycopy(or0, 0, or, 0, or0.length);
            System.arraycopy(or1, 0, or, or0.length, or1.length);
        }
        int[] ind0 = ca0.getDataIndices();
        int[] ind1 = ca1.getDataIndices();
        if (ind0 == null && ind1 == null)
            return new CellArray(ca0.getType(), nodes, or, null);
        else {
            int[] dataIndices = new int[ca0.getNCells() + ca1.getNCells()];
            Arrays.fill(dataIndices, 0);
            int min = Integer.MAX_VALUE;
            int max = -Integer.MAX_VALUE;

            if (ind0 != null) {
                for (int i = 0; i < ind0.length; i++) {
                    if (ind0[i] < min) min = ind0[i];
                    if (ind0[i] > max) max = ind0[i];
                }
                max -= min;
                for (int i = 0; i < ind0.length; i++)
                    dataIndices[i] = ind0[i] - min;
            }
            if (ind1 != null) {
                min = Integer.MAX_VALUE;
                for (int i = 0; i < ind1.length; i++)
                    if (ind1[i] < min) min = ind1[i];
                max -= min;
                for (int i = 0, l = ca0.getNCells(); i < ind1.length; i++, l++)
                    dataIndices[l] = ind1[i] + max;
            }
            return new CellArray(ca0.getType(), nodes, or, dataIndices);
        }
    }

    private static TimeData merge(TimeData timeData0, TimeData timeData1)
    {
        if (timeData0 == null && timeData1 == null)
            return null;
        else if (timeData0 != null && timeData1 == null)
            return timeData0.cloneShallow();
        else if (timeData0 == null && timeData1 != null)
            return timeData1.cloneShallow();

        DataArrayType dType = timeData0.getType();
        if (dType != timeData1.getType())
            return null;
        LargeArrayType type = dType.toLargeArrayType();
        TimeData result = new TimeData(dType);
        long n0 = timeData0.length();
        long n1 = timeData1.length();
        long n = n0 + n1;
        float[] t0 = timeData0.getTimesAsArray();
        float[] t1 = timeData1.getTimesAsArray();
        for (int i = 0; i < t0.length; i++) {
            float t = t0[i];
            LargeArray val0 = timeData0.getValue(t);
            LargeArray val1 = timeData1.getValue(t);
            LargeArray val = LargeArrayUtils.create(type, n);
            LargeArrayUtils.arraycopy(val0, 0, val, 0, n0);
            LargeArrayUtils.arraycopy(val1, 0, val, n0, n1);
            result.setValue(val, t);
        }
        for (int i = 0; i < t0.length; i++) {
            float t = t1[i];
            if (timeData0.isTimestep(t))
                continue;
            LargeArray val0 = timeData0.getValue(t);
            LargeArray val1 = timeData1.getValue(t);
            LargeArray val = LargeArrayUtils.create(type, n);
            LargeArrayUtils.arraycopy(val0, 0, val, 0, n0);
            LargeArrayUtils.arraycopy(val1, 0, val, n0, n1);
            result.setValue(val, t);
        }
        return result;
    }

    private static DataArray merge(DataArray inDA0, DataArray inDA1)
    {
        int vlen = inDA0.getVectorLength();
        if (inDA1 == null || inDA1.getType() != inDA0.getType() || inDA1.getVectorLength() != vlen)
            return null;
        double min  = Math.min(inDA0.getPreferredMinValue(), inDA1.getPreferredMinValue());
        double pmin = Math.min(inDA0.getPreferredPhysMinValue(), inDA1.getPreferredPhysMinValue());
        double max  = Math.max(inDA0.getPreferredMaxValue(), inDA1.getPreferredMaxValue());
        double pmax = Math.max(inDA0.getPreferredPhysMaxValue(), inDA1.getPreferredPhysMaxValue());
        return DataArray.create(merge(inDA0.getTimeData(), inDA1.getTimeData()),
                                vlen, inDA0.getName(), inDA0.getUnit(), inDA0.getUserData()).
                                preferredRanges(min, max, pmin, pmax);
    }

    private static DataArray merge(DataArray inDA, long n1)
    {
        int vlen = inDA.getVectorLength();
        TimeData inTD = inDA.getTimeData();
        TimeData outTD = new TimeData(inDA.getType());
        for (int i = 0; i < inTD.getNSteps(); i++) {
            float t = inTD.getTime(i);
            LargeArray in = inTD.getValue(t);
            LargeArray out = LargeArrayUtils.create(inDA.getType().toLargeArrayType(), in.length() + n1, true);
            LargeArrayUtils.arraycopy(in, 0, out, 0, in.length());
            outTD.setValue(out, t);
        }
        return DataArray.create(outTD, vlen, inDA.getName(), inDA.getUnit(), inDA.getUserData()).
                                preferredRanges(inDA.getPreferredMinValue(), inDA.getPreferredMaxValue(),
                                                inDA.getPreferredPhysMinValue(), inDA.getPreferredPhysMaxValue());
    }

    private static DataArray merge(long n0, DataArray inDA)
    {
        int vlen = inDA.getVectorLength();
        TimeData inTD = inDA.getTimeData();
        TimeData outTD = new TimeData(inDA.getType());
        for (int i = 0; i < inTD.getNSteps(); i++) {
            float t = inTD.getTime(i);
            LargeArray in = inTD.getValue(t);
            LargeArray out = LargeArrayUtils.create(inDA.getType().toLargeArrayType(), in.length() + n0, true);
            LargeArrayUtils.arraycopy(in, 0, out, n0, in.length());
            outTD.setValue(out, t);
        }
        return DataArray.create(outTD, vlen, inDA.getName(), inDA.getUnit(), inDA.getUserData()).
                                preferredRanges(inDA.getPreferredMinValue(), inDA.getPreferredMaxValue(),
                                                inDA.getPreferredPhysMinValue(), inDA.getPreferredPhysMaxValue());
    }


    /**
     * Creates a cell set merged from cell sets given as arguments merging their respective cell arrays
     * @param cellSet0: first merged cell set
     * @param cellSet1: second merged cell set
     * @param nodeOffset: 0 if cs1 and cs2 share the same set of nodes (merging two cell sets from the same field)
     * <p>
     * if cellSet0 and cellSet1 come from different fields, nNodes must be set to the number of nodes of the field containing cs1;
     * Warning! there is no control on the nNodes parameter; There is no check for duplicate cells in the case of merging cell sets within a field.
     * @return merged cell set
     */
    private static CellSet add(CellSet cellSet0, CellSet cellSet1, int nodeOffset)
    {
        CellSet out = new CellSet();
        out.setName(cellSet0.getName());
        for (int i = 0; i < cellSet0.getCellArrays().length; i++)
            out.setCellArray(merge(cellSet0.getCellArrays()[i],
                                   cellSet1.getCellArray(CellType.getType(i)),
                                   nodeOffset));
//        for (int i = 0; i < cellSet0.getBoundaryCellArrays().length; i++) {
//            out.setBoundaryCellArray(merge(cellSet0.getBoundaryCellArrays()[i],
//                                           cellSet1.getBoundaryCellArray(CellType.getType(i)),
//                                           nodeOffset));
//        }
        long component0Length = cellSet0.getNComponents() == 0 ? 0 :
                                cellSet0.getComponent(0).getNElements();
        long component1Length = cellSet1.getNComponents() == 0 ? 0 :
                                cellSet1.getComponent(0).getNElements();

        String[] cellSet1ComponentNames = new String[cellSet1.getNComponents()];
        boolean[] mergedcellSet1Components = new boolean[cellSet1.getNComponents()];
        for (int i = 0; i < cellSet1.getNComponents(); i++) {
            cellSet1ComponentNames[i] = cellSet1.getComponent(i).getName();
            mergedcellSet1Components[i] = false;
        }
    cellSet0ComponentLoop:
        for (int i = 0; i < cellSet0.getNComponents(); i++) {
            DataArray component0 = cellSet0.getComponent(i);
            String cName = component0.getName();
            DataArrayType type = component0.getType();
            int vLen = component0.getVectorLength();
            for (int j = 0; j < cellSet1.getNComponents(); j++) {
                if (cName.equals(cellSet1ComponentNames[j])){
                    DataArray component1 = cellSet1.getComponent(j);
                    if (component1.getVectorLength() == vLen && component1.getType() == type) {
                        out.addComponent(merge(component0, component1));
                        mergedcellSet1Components[j] = true;
                    }
                    else
                        cellSet1ComponentNames[j] = cellSet1ComponentNames[j] + "_";
                    continue cellSet0ComponentLoop;
                }
            }
            out.addComponent(merge(component0, vLen * component1Length));
        }
        for (int j = 0; j < cellSet1.getNComponents(); j++)
            if (!mergedcellSet1Components[j]) {
                DataArray component1 = cellSet1.getComponent(j);
                out.addComponent(merge(component1.getVectorLength() * component0Length, component1));
            }
        return out;
    }

    /**
     * Creates a cell set merged from cell sets given as arguments merging their respective cell arrays
     * @param cellSet0: first merged cell set
     * @param cellSet1: second merged cell set
     * @return merged cell set
     */
    private static CellSet add(CellSet cellSet0, CellSet cellSet1)
    {
        return add(cellSet0, cellSet1, 0);
    }

    private static CellSet merge(Collection<CellSet> cellSets)
    {
        CellSet out = new CellSet("merged");
        for (CellSet cellSet : cellSets)
            out = add(out, cellSet);
        return out;
    }

    public static IrregularField mergeCellSet(IrregularField inField)
    {
        IrregularField outField = inField.cloneShallow();
        outField.getCellSets().clear();
        outField.addCellSet(merge(inField.getCellSets()));
        return outField;
    }

    /**
     * Merges two irregular fields
     * @param inField0: first merged field
     * @param inField1: second merged field
     * @param separate: if false, cell sets from inField0 and inField1 with equal names will be merged,
     * otherwise, they will form separate cell sets in the resulting field
     * @param mergeCounter: used as suffix to new cell set names (only if separate is true or merged fields have different cell set names);
     * if merge is used to accumulate a series of fields,
     * increase mergeCounter at each operation to ensure that different cell sets will have different names
     * @return merged field (nNodes is the sum of inField0.nNodes and inField1.nNodes, compatible components are merged,
     * non-compatible components are discarded
     * and cell sets merged or concatenated according to the separate parameter
     * Warning: only current data will be merged, thus merged field has no nontrivial time data
     */
    public static IrregularField merge(IrregularField inField0, IrregularField inField1, int mergeCounter, boolean separate)
    {
        return merge(inField0, inField1, mergeCounter, separate, true);
    }

    /**
     * Merges two irregular fields
     * @param inField0: first merged field
     * @param inField1: second merged field
     * @param separate: if false, cell sets from inField0 and inField1 with equal names will be merged,
     * otherwise, they will form separate cell sets in the resulting field
     * @param mergeCounter: used as suffix to new cell set names (only if separate is true or merged fields have different cell set names);
     * if merge is used to accumulate a series of fields,
     * increase mergeCounter at each operation to ensure that different cell sets will have different names
     * @param mergeTimeData if true, timelines of node components in both merged fields are merged
     * @return merged field (nNodes is the sum of inField0.nNodes and inField1.nNodes, compatible components are merged,
     * non-compatible components are discarded
     * and cell sets merged or concatenated according to the separate parameter
     */
    public static IrregularField merge(IrregularField inField0, IrregularField inField1, int mergeCounter, boolean separate, boolean mergeTimeData)
    {
        if ((inField0 == null || inField0.getNNodes() == 0) && (inField1 != null && inField1.getNNodes() != 0))
            return inField1.cloneShallow();
        if ((inField0 != null && inField0.getNNodes() != 0) && (inField1 == null || inField1.getNNodes() == 0))
            return inField0.cloneShallow();
        if ((inField0 == null || inField0.getNNodes() == 0) && (inField1 == null || inField1.getNNodes() == 0))
            return null;
        int nNodes1 = (int) inField1.getNNodes();
        int nNodes0 = (int) inField0.getNNodes();
        int nNodes = nNodes1 + nNodes0;
        IrregularField outField = new IrregularField(nNodes);
        if (mergeTimeData) {
            outField.setCoords(merge(inField0.getCoords(), inField1.getCoords()));
            for (int i = 0; i < inField0.getNComponents(); i++) {
                DataArray inDA0 = inField0.getComponent(i);
                DataArray mDA = merge(inDA0, inField1.getComponent(inDA0.getName()));
                if (mDA != null)
                    outField.addComponent(mDA);
            }
        }
        else {
            float[] coords = new float[3 * nNodes];
            System.arraycopy(inField0.getCurrentCoords().getData(), 0, coords, 0, 3 * nNodes0);
            System.arraycopy(inField1.getCurrentCoords().getData(), 0, coords, 3 * nNodes0, 3 * nNodes1);
            outField.setCurrentCoords(new FloatLargeArray(coords));
            for (int i = 0; i < inField0.getNComponents(); i++) {
                DataArray inDA0 = inField0.getComponent(i);
                int vlen = inDA0.getVectorLength();
                String name = inDA0.getName();
                DataArray inDA1 = inField1.getComponent(name);
                if (inDA1 == null || inDA1.getType() != inDA0.getType() || inDA1.getVectorLength() != vlen)
                    continue;
                LargeArray outB = LargeArrayUtils.create(inDA1.getRawArray().getType(), vlen * nNodes, false);
                LargeArrayUtils.arraycopy(inDA0.getRawArray(), 0, outB, 0, vlen * nNodes0);
                LargeArrayUtils.arraycopy(inDA1.getRawArray(), 0, outB, vlen * nNodes0, vlen * nNodes1);
                outField.addComponent(DataArray.create(outB, vlen, inDA0.getName(), inDA0.getUnit(), inDA0.getUserData()));
            }
        }
        if (outField.getNComponents() == 0) {
            int[] dummy = new int[nNodes];
            for (int i = 0; i < nNodes0; i++)
                dummy[i] = 0;
            for (int i = nNodes0; i < nNodes1; i++)
                dummy[i] = 1;
            outField.addComponent(DataArray.create(dummy, 1, "dummy"));
        }
        boolean sep = separate;
        if (inField0.getNCellSets() != inField1.getNCellSets())
            sep = true;
        if (sep) {
            for (CellSet cs : inField0.getCellSets())
                outField.addCellSet(cs);
            for (CellSet cs : inField1.getCellSets()) {
                CellSet ncs = new CellSet();
                ncs.setName(cs.getName() + mergeCounter);
                ncs.setSelected(true);
                for (int i = 0; i < cs.getNComponents(); i++)
                    ncs.addComponent(cs.getComponent(i).cloneShallow());
                for (int i = 0; i < cs.getBoundaryCellArrays().length; i++) {
                    if (cs.getBoundaryCellArray(CellType.getType(i)) == null)
                        continue;
                    int[] inNds = cs.getBoundaryCellArray(CellType.getType(i)).getNodes();
                    int[] outNds = new int[inNds.length];
                    for (int j = 0; j < outNds.length; j++)
                        outNds[j] = inNds[j] + nNodes0;
                    byte[] inOr = cs.getBoundaryCellArray(CellType.getType(i)).getOrientations();
                    byte[] outOr = new byte[inOr.length];
                    System.arraycopy(inOr, 0, outOr, 0, inOr.length);
                    int[] inIds = cs.getBoundaryCellArray(CellType.getType(i)).getDataIndices();
                    int[] outIds = null;
                    if (inIds != null) {
                        outIds = new int[inIds.length];
                        System.arraycopy(inIds, 0, outIds, 0, inIds.length);
                    }
                    ncs.setBoundaryCellArray(new CellArray(cs.getBoundaryCellArray(CellType.getType(i)).getType(), outNds, outOr, outIds));
                }
                for (int i = 0; i < cs.getCellArrays().length; i++) {
                    if (cs.getCellArray(CellType.getType(i)) == null)
                        continue;
                    int[] inNds = cs.getCellArray(CellType.getType(i)).getNodes();
                    int[] outNds = new int[inNds.length];
                    for (int j = 0; j < outNds.length; j++)
                        outNds[j] = inNds[j] + nNodes0;
                    byte[] inOr = cs.getCellArray(CellType.getType(i)).getOrientations();
                    byte[] outOr = new byte[inOr.length];
                    System.arraycopy(inOr, 0, outOr, 0, inOr.length);
                    int[] inIds = cs.getCellArray(CellType.getType(i)).getDataIndices();
                    int[] outIds = null;
                    if (inIds != null) {
                        outIds = new int[inIds.length];
                        System.arraycopy(inIds, 0, outIds, 0, inIds.length);
                    }
                    ncs.setCellArray(new CellArray(cs.getCellArray(CellType.getType(i)).getType(), outNds, outOr, outIds));
                }
                outField.addCellSet(ncs);
            }
        } else {
            for (int i = 0; i < inField0.getNCellSets(); i++)
                outField.addCellSet(add(inField0.getCellSet(i), inField1.getCellSet(i), (int) inField0.getNNodes()));
        }
        return outField;
    }

    public static IrregularField merge(Collection<IrregularField> inFields, boolean separate, boolean mergeTimeData)
    {
        IrregularField out = null;
        int mergeCounter = 0;
        for (IrregularField inField : inFields) {
            out = merge(out, inField, mergeCounter, separate, mergeTimeData);
            mergeCounter += 1;
        }
        return out;
    }

    public static IrregularField merge(Collection<IrregularField> inFields, boolean separate)
    {
        return merge(inFields, separate, true);
    }

    private MergeIrregularField()
    {
    }
}
