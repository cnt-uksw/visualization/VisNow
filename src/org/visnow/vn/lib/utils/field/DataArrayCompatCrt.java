/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import java.util.Arrays;
import java.util.function.BiPredicate;
import org.visnow.jscic.dataarrays.DataArray;

/**
 *
 * DataArray compatibility criteria organized as an enum of atomic criteria that can be combined according to actual requirements
 * by the compatible method
 *
 * @author Krzysztof S. Nowinski (VisNow.org, University of Warsaw, ICM)
 */


public enum DataArrayCompatCrt
{
    TYPE_EQUAL     ((x, y) -> x.getType()        == y.getType()),
    VECLEN_EQUAL   ((x, y) -> x.getVectorLength()== y.getVectorLength()),
    BOTH_NUMERIC   ((x, y) -> x.isNumeric()      && y.isNumeric()),
    SIZE_EQUAL     ((x, y) -> x.getNElements()   == y.getNElements()),
    MATRIX_EQUIV   ((x, y) -> Arrays.equals(x.getMatrixDims(), y.getMatrixDims())),
    SAME_TIMESTEPS ((x, y) -> Arrays.equals(x.getTimeData().getTimesAsArray(), y.getTimeData().getTimesAsArray())),
    // new compatibility criteria can be inserted here according to the template
    // CRITERION_NAME  lambda ((x, y) -> check), where check is a boolean function of the DataArrays x,y describing atomic compatibility CRITERION_NAME
    ;
    private final BiPredicate<DataArray, DataArray> compat;

    private DataArrayCompatCrt(BiPredicate<DataArray, DataArray> compat)
    {
        this.compat = compat;
    }

    /**
     * checks if two data arrays are compatible according to given criteria
     * @param x first data array
     * @param y second data array
     * @param criteria array of atomic criteria to be checked
     * @return true if x and y are not null and satisfy the given criteria, false otherwise
     * Examples: <p>
     * <code>areCompatible(x, y, new DataArrayCompatibilityCriterion[]{BOTH_NUMERIC, VECLEN_EQUAL, SIZE_EQUAL})</code>
     * checks if we can add x and y element by element<p>
     * areCompatible(x, y, new DataArrayCompatibilityCriterion[]{TYPE_EQUAL, VECLEN_EQUAL, SIZE_EQUAL})</code>
     * checks if x and y time data can be concatenated
     */
    public static final boolean compatible(DataArray x, DataArray y,
                                                  DataArrayCompatCrt[] criteria)
    {
        if (x == null || y == null)
            return false;
        for (DataArrayCompatCrt cr : criteria)
            if (!cr.compat.test(x, y))
                return false;
        return true;
    }

}
