/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import java.util.Arrays;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.ShortLargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;
import static org.visnow.vn.lib.utils.interpolation.SubsetGeometryComponents.*;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */


public class IndexPointSlice  extends IndexSlice
{
    protected float[] sliceCrds = {0, 0, 0};
    protected IrregularField slice = new IrregularField(1);
    protected boolean adjusting = true;
    protected float[] pos = null;
    private IndexPointSlice(RegularField field, IndexSliceParams params) throws Exception
    {
        if (field == null)
            throw new IllegalArgumentException("null inField");
        this.params = params;
        adjusting = params.isAdjusting();
        inField = field;
        inDims = inField.getDims();
        nOutNodes = 1;
        collectData(adjusting);
        CellSet cs = new CellSet();
        cs.addCells(new CellArray(CellType.POINT, new int[] {0}, new byte[] {0}, null));
        slice.addCellSet(cs);
    }
    
    @Override
    void computeSlicedData()
    {
        pos = params.getPosition();
        for (int idata =  0; idata < inData.length; idata++) {
            switch (inData[idata].getType()) {
                case UNSIGNED_BYTE:
                    byte[] outB = (byte[])inField.getInterpolatedData(inData[idata], pos[0], pos[1], pos[2]);
                    outData[idata] = new UnsignedByteLargeArray(outB);
                    break;
                case SHORT:
                    outData[idata] = new ShortLargeArray((short[])inField.getInterpolatedData(inData[idata], pos[0], pos[1], pos[2]));
                    break;
                case INT:
                    outData[idata] = new IntLargeArray((int[])inField.getInterpolatedData(inData[idata], pos[0], pos[1], pos[2]));
                    break;
                case FLOAT:
                    outData[idata] = new FloatLargeArray((float[])inField.getInterpolatedData(inData[idata], pos[0], pos[1], pos[2]));
                    break;
                case DOUBLE:
                    outData[idata] = new DoubleLargeArray((double[])inField.getInterpolatedData(inData[idata], pos[0], pos[1], pos[2]));
                    break;
            }
        }
    }
    
    @Override
    void addIndexCoords()
    {
        float[] indexCoords = Arrays.copyOf(params.getPosition(), inDims.length);
        if (slice.getComponent(INDEX_COORDS) != null)
            slice.removeComponent(INDEX_COORDS);
        slice.addComponent(DataArray.create(indexCoords, inDims.length, INDEX_COORDS));
    }
    
    @Override
    protected  Field slice(boolean singleVal) 
    {
        for (int iDataItem = 0; iDataItem < nDataItems; iDataItem++)
            outData[iDataItem] = new FloatLargeArray(vlens[iDataItem]);
        for (int i =  0, idata =  0 ; i < nData; i++) {
            DataArray data = inField.getComponent(i);
            if (data == null || !data.isNumeric())
                continue;
            outDataArrs[idata] = DataArray.create(DataArrayType.FIELD_DATA_FLOAT, 1, data.getVectorLength(), 
                                                  data.getName(), data.getUnit(), 
                                                  data.getUserData());
            idata += 1;
        }
        computeSlicedData();
        int idata = 0;
        for (int i =  0; i < nData; i++) {
            DataArray data = inField.getComponent(i);
            if (data == null || !data.isNumeric())
                continue;
            
            if (singleVal) {
                slice.addComponent(DataArray.create(outData[idata], 
                                                    data.getVectorLength(), 
                                                    data.getName(), 
                                                    data.getUnit(), 
                                                    data.getUserData()).
                                    preferredRanges(data.getPreferredMinValue(), 
                                                    data.getPreferredMaxValue(), 
                                                    data.getPreferredPhysMinValue(), 
                                                    data.getPreferredPhysMaxValue()));
                idata += 1;
            }
            else {
                TimeData timeData = data.getTimeData();
                TimeData outTimeData = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
                int nTimeSteps = timeData.getNSteps();
                for (int j = 0; j < nTimeSteps; j++, idata++) 
                    outTimeData.setValue(outData[idata], data.getTime(j));
                slice.addComponent(DataArray.create(outTimeData, 
                                                    data.getVectorLength(), 
                                                    data.getName(), 
                                                    data.getUnit(), 
                                                    data.getUserData()).
                                    preferredRanges(data.getPreferredMinValue(), 
                                                    data.getPreferredMaxValue(), 
                                                    data.getPreferredPhysMinValue(), 
                                                    data.getPreferredPhysMaxValue()));
            }
        }
        float[] crd = inField.getDimNum() == 3 ? inField.getGridCoords(pos[0], pos[1], pos[2]) :
                      inField.getDimNum() == 2 ? inField.getGridCoords(pos[0], pos[1]) :
                                                 inField.getGridCoords(pos[0]);
        slice.setCurrentCoords(new FloatLargeArray(crd));
        addIndexCoords();
        return slice;
    }
    
    
    public static Field slice(RegularField inField, IndexSliceParams params)
    {
        try {
            return new IndexPointSlice(inField, params).slice(params.isAdjusting());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
