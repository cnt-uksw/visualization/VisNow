/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import org.visnow.jlargearrays.ByteLargeArray;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.vn.lib.utils.FastIntQueue;
import org.visnow.vn.lib.utils.FastLongQueue;

/**
 *
 * @author know
 */

public class FloodFill
{

    /**
     * creates boolean mask of the region defined by the condition data <gt> thr or data <lt> thr, depending of the param above
     * the mask is extended by a margin of width r in each direction to ensure the condition for proper flood fill
     * @param in  data array defining region
     * @param dims dimensions of the field containing the da data arrray
     * @param thr data threshold
     * @param above if above, region is defined by the condition data &gt thr, else 
     * it is defined by the condition data &lt thr
     * @param r
     * @return ByteLargeArray containing the created mask
     */
    public static ByteLargeArray createExtendedRegion(LargeArray in, int vlen, int[] dims, float thr, boolean above, int r)
    {
        int dim = dims.length;
        int[] xDims = new int[dim];
        long ilen = 1;
        long rLen = 1;
        for (int i = 0; i < dim; i++) {
            ilen *= dims[i];
            xDims[i] = dims[i] + 2 * r;
            rLen *= xDims[i];
        }
        ByteLargeArray region = new ByteLargeArray(rLen);
        int[] ind = new int[dims.length];
        for (long i = 0; i < ilen; i++) {
            long ii = i;
            for (int j = 0; j < dim; j++) {
                ind[j] = (int)(ii % dims[j]);
                ii /= dims[j];
            }
            long ll = ind[dim - 1] + r;
            for (int j = ind.length - 2; j >= 0; j--) 
                ll = ll * xDims[j] + ind[j] + r;
            if (vlen == 1)
                region.setByte(ll, (above && in.getFloat(ii) > thr) ||
                                  (!above && in.getFloat(ii) < thr) ? (byte)1 : 0);
            else {
                double d = 0;
                for (int j = 0; j < vlen; j++) {
                    d += in.getFloat(ii * vlen + j) * in.getFloat(ii * vlen + j);
                    region.setByte(ll, (above && d > thr * thr) ||
                                      (!above && d < thr * thr) ? (byte)1 : 0);
                    
                }
            }
        }
        return region;
    }
    
    /**
     * creates boolean mask of the region defined by the condition data <gt> thr or data <lt> thr, depending of the param above
     * the mask is extended by a margin of width 1 in each direction to ensure the condition for proper flood fill
     * @param da  data array defining region
     * @param dims dimensions of the field containing the da data arrray
     * @param thr data threshold
     * @param above if above, region is defined by the condition data &gt thr, else 
     * it is defined by the condition data &lt thr
     * @return BitArray containing the created mask
     */
    public static ByteLargeArray createExtendedRegion(DataArray da, int[] dims, float thr, boolean above)
    {
        return createExtendedRegion(da.getRawArray(), da.getVectorLength(), dims, thr, above, 1);
    }

    /**
     * creates boolean mask of the region defined by the condition data <gt> thr or data <lt> thr, depending of the param above
     * the mask is extended by a margin of width 1 in each direction to ensure the condition for proper flood fill
     * @param da  data array defining region
     * @param dims dimensions of the field containing the da data arrray
     * @param low lower data threshold
     * @param up upper data threshold: the region is defined by the condition low <lt> data <lt> up
     * @return BitArray containing the created mask
     */
    public static ByteLargeArray createExtendedRegion(LargeArray in,  int vlen, int[] dims, float low, float up, int r)
    {
        int dim = dims.length;
        int[] xDims = new int[dim];
        long count = 0;
        long ilen = 1;
        long rLen = 1;
        for (int i = 0; i < dim; i++) {
            ilen *= dims[i];
            xDims[i] = dims[i] + 2 * r;
            rLen *= xDims[i];
        }
        ByteLargeArray region = new ByteLargeArray(rLen);
        int[] ind = new int[dims.length];
        for (long i = 0; i < ilen; i++) {
            long ii = i;
            for (int j = 0; j < dim; j++) {
                ind[j] = (int)(ii % dims[j]);
                ii /= dims[j];
            }
            long ll = ind[dim - 1] + r;
            for (int j = ind.length - 2; j >= 0; j--)
                ll = ll * xDims[j] + ind[j] + r;
            if (vlen == 1)
                region.setByte(ll, in.getFloat(i) > low && in.getFloat(i) < up ? (byte)1 : 0);
            else {
                double d = 0;
                for (int j = 0; j < vlen; j++) {
                    d += in.getFloat(i * vlen + j) * in.getFloat(i * vlen + j);
                    region.setByte(ll, d > low * low && d < up * up ? (byte)1 : 0);
                    
                }
            }
            if (region.getByte(ll) != 0)
                count += 1;
        }
        System.out.println("count "+ count);
        return region;
    }
    
    public static ByteLargeArray createExtendedRegion(DataArray da, int[] dims, float low, float up)
    {
        return createExtendedRegion(da.getRawArray(), da.getVectorLength(), dims, low, up, 1);
    }

    public static void maskRegion(LogicLargeArray mask, int[] dims, ByteLargeArray region, int r)
    {
        int dim = dims.length;
        int[] xDims = new int[dim];
        long ilen = 1;
        for (int i = 0; i < dim; i++) {
            ilen *= dims[i];
            xDims[i] = dims[i] + 2 * r;
        }
        int[] ind = new int[dims.length];
        for (long i = 0; i < ilen; i++) {
            long ii = i;
            for (int j = 0; j < dim; j++) {
                ind[j] = (int)(ii % dims[j]);
                ii /= dims[j];
            }
            long ll = ind[dim - 1] + r;
            for (int j = ind.length - 2; j >= 0; j--)
                ll = ll * xDims[j] + ind[j] + r;
            region.setByte(ll, mask.getBoolean(ii) ? region.get(ll) : 0);
        }
    }
    
    /**
     * fills a connected component of a 1D, 2D or 3D area with the value fillVal using flood fill algorithm
     * @param fillVal value >= 0 that will be set to the elements of the out array in filled component
     * @param region boolean array with the region to be processed filled by <code>true</code> margins of the width 1 
     * along the boundaries of the array must be filled by <code>false</code>, otherwise unpredictable behavior can occur
     * @param start array of indices of the initial flood fill points 
     * @param startShift width of the margin of out array along the boundary of the region array
     * @param dims dimensions of the original box
     * @param neighbors array of offsets from a given point in <code>region</code> to its neighbors
     * @param out modifiable array, all points in the component found will be set to fillVal. 
     * It has to be initialized by -1 before calling a sequence of fill method calls
     * @param counts 
     * @param elongations
     */
    public static void fill(int fillVal, ByteLargeArray region, long[] start, int startShift, 
                            int[] dims, int[] neighbors, LargeArray out, 
                            int[][] counts, float[][] elongations)
    {
        boolean isCount = counts != null && counts.length >= 1 && counts[0] != null;
        boolean isElongation = elongations != null && elongations.length >= 1 && elongations[0] != null;
        FastLongQueue queue = new FastLongQueue();
        long[] s = start;
        int dim = dims.length;
        if (startShift > 0)
        {
            s = new long[start.length];
            for (int i = 0; i < start.length; i++) {
                long k = start[i];
                int[] ind = new int[dim];
                for (int j = 0; j < dim; i++) {
                     ind[i] = (int)(k % dims[i]);
                     k /= dims[i];
                }
                long ll = ind[dim - 1] + startShift;
                for (int j = ind.length - 2; j >= 0; j--) 
                    ll = ll * (dims[j] + 2 * startShift) + ind[j] + startShift;
                s[i] = ll;
            }
        }
        int elongation = 0, count = 1;
        int step = 0;
        int layerStep = 1;
        queue.init(s);
        while (!queue.isEmpty())
        {
            if (step == layerStep && isElongation) {
                elongation += 1;
                layerStep = queue.setStepMark();
                step = 0;
            }
            long k = queue.get();
            step += 1;
            for (int i = 0; i < neighbors.length; i++) {
                long j = k + neighbors[i];
                if (region.getByte(j) != 0 && out.getInt(j) < 0){
                    out.setInt(j, fillVal);
                    queue.insert(j);
                    count += 1;
                }
            }
        }
        if (isCount) {
            if (fillVal == counts[0].length) {
                int[] tCounts = new int[2 * counts[0].length];
                System.arraycopy(counts[0], 0, tCounts, 0, counts.length);
                counts[0] = tCounts;
            }
            counts[0][fillVal] = count;
        }
        if (isElongation) {
            if (fillVal == elongations[0].length) {
                float[] tElongs = new float[2 * elongations[0].length];
                System.arraycopy(elongations[0], 0, tElongs, 0, elongations[0].length);
                elongations[0] = tElongs;
            }
            float el = (float) elongation;
            elongations[0][fillVal] = el * (float) Math.sqrt(el / count);
        }
    }
    
    /**
     * fills a connected component of a 1D, 2D or 3D area with the value fillVal using flood fill algorithm
     * @param fillVal value >= 0 that will be set to the elements of the out array in filled component
     * @param region boolean array with the region to be processed filled by <code>true</code> margins of the width 1 
     * along the boundaries of the array must be filled by <code>false</code>, otherwise unpredictable behavior can occur
     * @param start array of indices of the initial flood fill points 
     * @param recomputeStart 
     * @param dims dimensions of the original box
     * @param neighbors array of offsets from a given point in <code>region</code> to its neighbors
     * @param out modifiable array, all points in the component found will be set to fillVal. 
     * It has to be initialized by -1 before calling a sequence of fill method calls
     * @param elongations
     */
    public static void fill(byte fillVal, byte[] region, int[] start, boolean recomputeStart, 
                            int[] dims, int[] neighbors, byte[] out, 
                            int[][] counts, float[][] elongations)
    {
        boolean isCount = counts != null && counts.length >= 1 && counts[0] != null;
        boolean isElongation = elongations != null && elongations.length >= 1 && elongations[0] != null;
        FastIntQueue queue = new FastIntQueue();
        int[] s = start;
        if (recomputeStart)
        {
            s = new int[start.length];
            for (int i = 0; i < start.length; i++) {
                int k = start[i];
                int l = k / dims[0];
                switch (dims.length) {
                case 1:
                    s[i] = k + 1;
                    break;
                case 2:
                    s[i] = (dims[0] + 2) * (l + 1) + k % dims[0] + 1;
                    break;
                case 3:
                    int m = l / dims[1];
                    s[i] = ((dims[1] + 2) * (m + 1) + l % dims[1] + 1) * (dims[0] + 2) + k % dims[0] + 1;
                }
            }
        }
        int elongation = 0, count = 1;
        int step = 0;
        int layerStep = 1;
        queue.init(s);
        while (!queue.isEmpty())
        {
            if (step == layerStep && isElongation) {
                elongation += 1;
                layerStep = queue.setStepMark();
                step = 0;
            }
            int k = queue.get();
            step += 1;
            for (int i = 0; i < neighbors.length; i++) {
                int j = k + neighbors[i];
                if (region[j] != 0 && out[j] < 0){
                    out[j] = fillVal;
                    queue.insert(j);
                    count += 1;
                }
            }
        }
        if (isCount) {
            if (fillVal == counts[0].length) {
                int[] tCounts = new int[2 * counts[0].length];
                System.arraycopy(counts[0], 0, tCounts, 0, counts.length);
                counts[0] = tCounts;
            }
            counts[0][fillVal] = count;
        }
        if (isElongation) {
            if (fillVal == elongations[0].length) {
                float[] tElongs = new float[2 * elongations[0].length];
                System.arraycopy(elongations[0], 0, tElongs, 0, elongations[0].length);
                elongations[0] = tElongs;
            }
            float el = (float) elongation;
            elongations[0][fillVal] = el * (float) Math.sqrt(el / count);
        }
    }
    
    public static void fill(byte fillVal, byte[] region, int[] start, boolean recomputeStart, 
                            int[] dims, int[] neighbors, byte[] out)
    {
        fill(fillVal, region, start, recomputeStart, dims, neighbors, out, null, null);
    }
    
    public static void fill(byte fillVal, byte[] region, int[] start, boolean recomputeStart, 
                            int[] dims, int[] neighbors, byte[] out, 
                            int[][] counts)
    {
        fill(fillVal, region, start, recomputeStart, dims, neighbors, out, counts, null);
    }

    public static int[] restoreResultDimensions(int[] dims, int[] xOut)
    {
        int nOut = 1;
        int[] xDims = new int[dims.length];
        for (int i = 0; i < xDims.length; i++) {
            xDims[i] = dims[i] + 2;
            nOut *= dims[i];
        }
        int[] out = new int[nOut];
        switch (dims.length) {
            case 3:
                for (int i = 0, l = 0; i < dims[2]; i++)
                    for (int j = 0; j < dims[1]; j++)
                        for (int k = 0,
                                m = ((i + 1) * xDims[1] + j + 1) * xDims[0] + 1;
                                k < dims[0]; k++, l++, m++) {
                            out[l] = xOut[m];
                        }
                break;
            case 2:
                for (int j = 0, l = 0; j < dims[1]; j++)
                    for (int k = 0,
                            m = (j + 1) * xDims[0] + 1;
                            k < dims[0]; k++, l++, m++) {
                        out[l] = xOut[m];
                    }
                break;
            case 1:
                for (int k = 0; k < dims[0]; k++) {
                    out[k] = xOut[k + 1];
                }
                break;
        }
        return out;
    }

    public static LargeArray restoreResultDimensions(LargeArray x, int[] dims, int r)
    {
        int dim = dims.length;
        int[] xDims = new int[dim];
        int[] srcPos = new int[dim];
        int[]destPos = new int[dim];
        long nOut = 1;
        for (int i = 0; i < dim; i++) {
            srcPos[i] = r;
            destPos[i] = 0;
            xDims[i] = dims[i] + 2 * r;
            nOut *= dims[i];
        }
        LargeArray out = LargeArrayUtils.create(x.getType(), nOut);
        LargeArrayUtils.subarraycopy(x, xDims, srcPos, out, dims, destPos, dims);
        return out;
    }
}
