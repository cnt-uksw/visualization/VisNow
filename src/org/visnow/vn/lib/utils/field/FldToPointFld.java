/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import java.util.ArrayList;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.PointField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class FldToPointFld
{
    private FldToPointFld()
    {
    }

    private static LargeArray select(LargeArray in, int vlen, long nOut, LogicLargeArray mask)
    {
        LargeArray out = LargeArrayUtils.create(in.getType(), vlen * nOut);
        for (long i = 0, iIn = 0, iOut = 0; i < in.length() / vlen; i++)
            if (mask.getBoolean(i)) {
                LargeArrayUtils.arraycopy(in, iIn, out, vlen * iOut, vlen);
                iIn += vlen;
                iOut += vlen;
            }
        return out;
    }

    private static TimeData select(TimeData in, int vlen, long nOut, LogicLargeArray mask)
    {
        TimeData out = new TimeData(in.getType());
        for (int i = 0; i < in.getNSteps(); i++)
            out.setValue(select(in.getValue(in.getTime(i)), vlen, nOut, mask), in.getTime(i));
        return out;
    }

    public static PointField convert(Field inField)
    {
        long nNodes = (int) inField.getNNodes();
        long nOutNodes = nNodes;
        LogicLargeArray mask = inField.getCurrentMask();
        boolean isMask = inField.hasMask();
        TimeData inTimeCoords = inField.getCoords();
        if (inTimeCoords == null) {
            FloatLargeArray inCoords = ((RegularField)inField).getCoordsFromAffine();
            ArrayList<Float> tSeries = new ArrayList<>();
            tSeries.add(0f);
            ArrayList<LargeArray> dSeries = new ArrayList<>();
            dSeries.add(inCoords);
            inTimeCoords = new TimeData(tSeries, dSeries, 0);
        }

        PointField outPointField = new PointField(nOutNodes);
        if (isMask) {
            outPointField.setCoords(select(inTimeCoords, 3, nOutNodes, mask));
            for (DataArray inDa : inField.getComponents()) {
                int vlen = inDa.getVectorLength();
                outPointField.addComponent(
                        DataArray.create(select(inDa.getTimeData(), vlen, nOutNodes, mask),
                                         vlen, inDa.getName()).unit(inDa.getUnit()).userData(inDa.getUserData()).
                                  preferredRanges(inDa.getPreferredMinValue(), inDa.getPreferredMaxValue(),
                                                  inDa.getPreferredPhysMinValue(), inDa.getPreferredPhysMaxValue()));
            }
        } else {
            outPointField.setCoords(inTimeCoords);
            for (DataArray inDa : inField.getComponents())
                outPointField.addComponent(inDa.cloneShallow());
        }
        outPointField.setCurrentTime(inField.getCurrentTime());
        return outPointField;
    }
}
