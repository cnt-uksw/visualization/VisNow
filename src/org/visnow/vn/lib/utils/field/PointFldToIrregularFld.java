/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import java.util.Arrays;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.PointField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.lib.utils.field.subset.FieldSubset;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class PointFldToIrregularFld
{
    private PointFldToIrregularFld()
    {
    }

    public static IrregularField convert(PointField inField)
    {
        if (inField.getNNodes() > 2L << 31)
            throw new IllegalArgumentException("large point field can not be converted to irregular field");
        int nNodes = (int) inField.getNNodes();
        boolean isMask = inField.hasMask();
        int nOutNodes = 0;
        IrregularField outIrregularField;
        if (isMask) {
            LogicLargeArray mask = inField.getCurrentMask();
            for (int i = 0; i < nNodes; i++)
                if (mask.getBoolean(i))
                    nOutNodes += 1;
            outIrregularField = new IrregularField(nOutNodes);
            outIrregularField.setCoords(FieldSubset.select(inField.getCoords(), 3, nOutNodes, mask));
            for (DataArray inDa : inField.getComponents())
                outIrregularField.addComponent(FieldSubset.select(inDa, nOutNodes, inField.getCurrentMask()));
        }
        else {
            nOutNodes = nNodes;
            outIrregularField = new IrregularField(nOutNodes);
            outIrregularField.setCoords(inField.getCoords().cloneShallow());
            for (DataArray inDa : inField.getComponents())
                outIrregularField.addComponent(inDa.cloneShallow());
        }
        if (outIrregularField.getComponent("subsets") != null &&
            outIrregularField.getComponent("subsets").getType() == DataArrayType.FIELD_DATA_BYTE) {
            String[] names = outIrregularField.getComponent("subsets").getUserData();
            UnsignedByteLargeArray sets = outIrregularField.getComponent("subsets").getRawByteArray();
            int[] nodesInSet = new int[255];
            Arrays.fill(nodesInSet, 0);
            for (long i = 0; i < nOutNodes; i++)
                nodesInSet[sets.getInt(i)] += 1;
            int[][] pts = new int[255][];
            for (int i = 0; i < nodesInSet.length; i++)
                if (nodesInSet[i] > 0) {
                    pts[i] = new int[nodesInSet[i]];
                    nodesInSet[i] = 0;
                }
            for (long i = 0; i < nOutNodes; i++) {
                int k = sets.getInt(i);
                pts[k][nodesInSet[k]] = (int)i;
                nodesInSet[k] += 1;
            }
            for (int i = 0; i < nodesInSet.length; i++)
                if (nodesInSet[i] > 0) {
                    CellArray ptsArr = new CellArray(CellType.POINT, pts[i], null, null);
                    CellSet cs = new CellSet(names != null && names.length >= i ?
                                             names[i] :
                                             "subset" + i);
                    cs.addCells(ptsArr);
                    outIrregularField.addCellSet(cs);
                }
        }
        else {
            int[] pts = new int[nOutNodes];
            for (int i = 0; i < pts.length; i++)
                pts[i] = i;
            CellArray ptsArr = new CellArray(CellType.POINT, pts, null, null);
            CellSet cs = new CellSet("cells");
            cs.addCells(ptsArr);
            outIrregularField.addCellSet(cs);
        }
        return outIrregularField;
    }
}
