/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import java.util.Arrays;
import java.util.function.BiPredicate;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;

/**
 *
 * Field compatibility criteria organized as an enum of atomic criteria that can be combined according to actual requirements
 * by the compatible method
 *
 * @author Krzysztof S. Nowinski (VisNow.org, University of Warsaw, ICM)
 */



public enum FieldCompatCrt
{
    TYPE_EQUAL                  ((x, y) -> x.getType()        == y.getType()),
    SIZE_EQUAL                  ((x, y) -> x.getNElements()   == y.getNElements()),
    NDIMS_EQUAL                 ((x, y) -> x instanceof RegularField && y instanceof RegularField &&
                                           ((RegularField)x).getDimNum() == ((RegularField)y).getDimNum()),
    DIMS_EQUAL                  ((x, y) -> x instanceof RegularField && y instanceof RegularField &&
                                           Arrays.equals(((RegularField)x).getDims(), ((RegularField)y).getDims())),
    NCOMPONENTS_EQUAL           ((x, y) -> x.getNComponents() == y.getNComponents()),
    COMPONENTS_NAMES_COMPATIBLE ((x, y) -> {if (!NCOMPONENTS_EQUAL.compat.test(x, y))
                                                return false;
                                            for (String name: x.getComponentNames())
                                                if (y.getComponent(name) == null)
                                                    return false;
                                            return true;}),
    ;
    private final BiPredicate<Field, Field> compat;

    private FieldCompatCrt(BiPredicate<Field, Field> compat)
    {
        this.compat = compat;
    }

    /**
     * checks if two fields are compatible according to given criteria
     * @param x first field
     * @param y second field
     * @param criteria array of criteria to be checked
     * @param daCrt array of criteria to be checked for each component
     * @return true if x and y are not null and satisfy the given criteria, false otherwise
     */
    public static final boolean compatible(Field x, Field y,
                                FieldCompatCrt[] criteria, DataArrayCompatCrt[] daCrt)
    {
        if (x == null || y == null)
            return false;
        for (FieldCompatCrt cr : criteria)
            if (!cr.compat.test(x, y))
                return false;
        if (daCrt != null && daCrt.length > 0) {
            for (String name: x.getComponentNames()) {
                if (y.getComponent(name) == null ||
                    !DataArrayCompatCrt.compatible(x.getComponent(name), y.getComponent(name), daCrt))
                    return false;
            }
        }
        return true;
    }

}
