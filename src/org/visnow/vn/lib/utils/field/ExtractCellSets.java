/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.IrregularField;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.TimeData;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class ExtractCellSets
{

    public static IrregularField extractCellSets(IrregularField in)
    {
        boolean[] vpts = new boolean[(int) in.getNNodes()];
        int[] vptinds = new int[(int) in.getNNodes()];
        for (int i = 0; i < vptinds.length; i++) {
            vptinds[i] = -1;
            vpts[i] = false;
        }
        for (CellSet s : in.getCellSets())
            if (s.isSelected()) {
                for (CellArray a : s.getCellArrays())
                    if (a != null) {
                        int[] nodes = a.getNodes();
                        for (int i = 0; i < nodes.length; i++)
                            vpts[nodes[i]] = true;
                    }
                for (CellArray a : s.getBoundaryCellArrays())
                    if (a != null) {
                        int[] nodes = a.getNodes();
                        for (int i = 0; i < nodes.length; i++)
                            vpts[nodes[i]] = true;
                    }
            }
        int nOut = 0;
        for (int i = 0; i < vptinds.length; i++)
            if (vpts[i]) {
                vptinds[i] = nOut;
                nOut += 1;
            }
        if (nOut == 0)
            return null;
        IrregularField out = new IrregularField(nOut);
        out.setCurrentCoords(in.getCurrentCoords() == null ? null : (FloatLargeArray)arrayCompact(in.getCurrentCoords(), 3, nOut, vpts));
        for (DataArray da : in.getComponents()) {
            TimeData inTD = da.getTimeData();
            TimeData outTD = new TimeData(da.getType());
            for (int i = 0; i < inTD.getNSteps(); i++)
                outTD.setValue(arrayCompact(inTD.getValue(inTD.getTime(i)), 
                                            da.getVectorLength(), nOut, vpts), inTD.getTime(i));
            out.addComponent(DataArray.create(outTD, da.getVectorLength(), da.getName(), da.getUnit(), da.getUserData()));
        }
            
        for (CellSet s : in.getCellSets())
            if (s.isSelected()) {
                CellSet outS = new CellSet(s.getName());
                for (DataArray component : s.getComponents()) 
                    outS.addComponent(component.cloneShallow());
                for (CellArray a : s.getCellArrays())
                    if (a != null) {
                        int[] nodes = a.getNodes();
                        int[] outNodes = new int[nodes.length];
                        for (int i = 0; i < nodes.length; i++)
                            outNodes[i] = vptinds[nodes[i]];
                        CellArray outA = new CellArray(a.getType(), outNodes, a.getOrientations(), a.getDataIndices());
                        outS.setCellArray(outA);
                    }
                for (CellArray a : s.getBoundaryCellArrays())
                    if (a != null) {
                        int[] nodes = a.getNodes();
                        int[] outNodes = new int[nodes.length];
                        for (int i = 0; i < nodes.length; i++)
                            outNodes[i] = vptinds[nodes[i]];
                        CellArray outA = new CellArray(a.getType(), outNodes, a.getOrientations(), a.getDataIndices());
                        outS.setBoundaryCellArray(outA);
                    }
                out.addCellSet(outS);
            }
        return out;
    }
    
    public static LargeArray arrayCompact(LargeArray data, int vlen, int outLen, boolean[] v)
    {
        long n = data.length() / vlen;
        if (n != v.length)
            return null;
        LargeArray out = LargeArrayUtils.create(data.getType(), outLen * vlen);
        for (int i = 0, j = 0; i < n; i++)
            if (v[i]) {
                LargeArrayUtils.arraycopy(data, vlen * i, out, j, vlen);
                j += vlen;
            }
        return out;
    }

    private ExtractCellSets()
    {
    }

}
