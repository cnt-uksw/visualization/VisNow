/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class PullBackArray2D
{

    /**
     * computes pull-back of data array from a trivially regular field by a mapping
     * defined by node coordinates using nearest neighbor interpolation
     * <p>
     * @param targetArray data array pulled back
     * @param targetDims  coordinates of mapped nodes (in index space of mapping target field)
     * @param coords      coordinates of mapped nodes
     * <p>
     * @return
     */
    public static DataArray pullBack3D(DataArray targetArray, int[] targetDims, float[] coords)
    {
        int n = coords.length / 3;
        LargeArray tData = targetArray.getRawArray();
        LargeArray outBData = LargeArrayUtils.create(tData.getType(), n, false);
        for (int i = 0; i < n; i++) {
            int i0 = (int) (.5 + coords[3 * i]);
            if (i0 < 0)
                i0 = 0;
            if (i0 >= targetDims[0])
                i0 = targetDims[0] - 1;
            int i1 = (int) (.5 + coords[3 * i + 1]);
            if (i1 < 0)
                i1 = 0;
            if (i1 >= targetDims[1])
                i1 = targetDims[1] - 1;
            outBData.set(i, tData.get(i1 * targetDims[0] + i0));
        }
        return (DataArray.create(outBData, 1, "" + targetArray.getName() + " pullback"));
    }

    private PullBackArray2D()
    {
    }

    /**
     * computes pull-back of data array from a trivially regular field by a mapping
     * defined by node coordinates using nearest neighbor interpolation
     * <p>
     * @param targetArray data array pulled back
     * @param targetDims  coordinates of mapped nodes (in index space of mapping target field)
     * @param coords      coordinates of mapped nodes
     * <p>
     * @return
     */
    public static DataArray pullBack2D(DataArray targetArray, int[] targetDims, float[] coords)
    {
        int n = coords.length / 2;
        LargeArray tData = targetArray.getRawArray();
        LargeArray outBData = LargeArrayUtils.create(tData.getType(), n, false);
        for (int i = 0; i < n; i++) {
            int i0 = (int) (.5 + coords[2 * i]);
            if (i0 < 0)
                i0 = 0;
            if (i0 >= targetDims[0])
                i0 = targetDims[0] - 1;
            int i1 = (int) (.5 + coords[2 * i + 1]);
            if (i1 < 0)
                i1 = 0;
            if (i1 >= targetDims[1])
                i1 = targetDims[1] - 1;
            outBData.set(i, tData.get(i1 * targetDims[0] + i0));
        }
        return (DataArray.create(outBData, 1, "" + targetArray.getName() + " pullback"));
    }

}
