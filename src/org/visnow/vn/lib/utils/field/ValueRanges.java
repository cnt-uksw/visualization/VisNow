/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import org.visnow.jscic.Field;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.lib.utils.MantissaRange;

/**
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */
public class ValueRanges {
    private ValueRanges()
    {

    }

    public static final void updateValueRanges(Field inField,
                                               float[] minima,  float[] maxima,
                                               float[] mMinima, float[] mMaxima,
                                               float[] pMinima, float[] physMinima,
                                               float[] coeffs,  boolean keepPreferredRanges)
    {
        int nNodes = (int)inField.getNNodes();
        for (int i = 0; i < inField.getNComponents(); i++) {
            DataArray da = inField.getComponent(i);
            float ct = da.getCurrentTime();
            if (da.isNumeric()) {

                if (keepPreferredRanges) {
                    maxima[i] = (float)da.getPreferredMaxValue();
                    minima[i] = (float)da.getPreferredMinValue();
                }
                else {
                    for (float time : da.getTimeSeries()) {
                        da.setCurrentTime(time);
                        if (da.getVectorLength() == 1)
                            for (int j = 0; j < nNodes; j++) {
                                float v = da.getFloatElement(j)[0];
                                maxima[i] = Math.max(v, maxima[i]);
                                minima[i] = Math.min(v, minima[i]);
                            }
                        else {
                            minima[i] = mMinima[i] = 0;
                            float[] vns = da.getVectorNorms().getData();
                            for (int j = 0; j < nNodes; j++) {
                                float v = vns[j];
                                maxima[i] = Math.max(v, maxima[i]);
                            }
                        }
                    }
                }
                pMinima[i]    = (float)da.getPreferredMinValue();
                physMinima[i] = (float)da.getPreferredPhysMinValue();
                coeffs[i]     = (float)((da.getPreferredPhysMaxValue() - physMinima[i]) /
                                        (da.getPreferredMaxValue() - pMinima[i]));
                da.setCurrentTime(ct);
            }
        }
    }

    public static final void updateExponentRanges(float[] minima,  float[] maxima,
                                                  float[] mMinima, float[] mMaxima,
                                                  float[] pMinima, float[] physMinima,
                                                  float[] coeffs,  int[] exponents)
    {
        for (int i = 0; i < minima.length; i++) {
            float pMin  = pMinima[i];
            float phMin = physMinima[i];
            float coeff = coeffs[i];
            minima[i]     = (phMin + coeff * (minima[i] - pMin));
            maxima[i]     = (phMin + coeff * (maxima[i] - pMin));
            float[] mR = MantissaRange.mantissaRange(minima[i], maxima[i]);
            mMinima[i] = mR[0];
            mMaxima[i] = mR[1];
            exponents[i] = (int)mR[2];
        }
    }
}
