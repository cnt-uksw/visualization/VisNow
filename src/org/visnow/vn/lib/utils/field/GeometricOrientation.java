/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class GeometricOrientation {
    
    private static final int N_THREADS = VisNow.availableProcessors();
    
    private static class CorrectPart implements Runnable
    {
        
        private final int iThread ;
        private final FloatLargeArray coords;
        private final int trueNSpace;
        private final CellArray ca;
        private final int nCellNodes;
        private final int[] nodes;
        private final int[] orv;
        private final byte[] orientations;
        
        public CorrectPart(int iThread, CellArray ca, FloatLargeArray coords, int trueNSpace)
        {
            this.iThread = iThread;
            this.coords = coords;
            this.trueNSpace = trueNSpace;
            this.ca = ca; 
            nodes = ca.getNodes();
            orv = ca.getType().getOrientingVerts();
            orientations = ca.getOrientations();
            nCellNodes = ca.getNCellNodes();
        }

        @Override
        public void run()
        {
            for (int k = iThread; k < ca.getNCells(); k += N_THREADS) {
                int[] verts = new int[orv.length];
                float[][] v = new float[trueNSpace][trueNSpace];
                float d = 0;
                for (int i = 0; i < orv.length; i++)
                    verts[i] = nodes[k * nCellNodes + orv[i]];
                for (int i = 0; i < trueNSpace; i++)
                    for (int j = 0; j < trueNSpace; j++)
                        v[i][j] = coords.getFloat(3 * verts[i + 1] + j) - 
                                  coords.getFloat(3 * verts[0] + j);
                switch (trueNSpace) {
                case 1:
                    d = v[0][0];
                    break;
                case 2:
                    d = v[0][0] * v[1][1] - v[0][1] * v[1][0];
                    break;
                case 3:
                    d = v[0][0] * v[1][1] * v[2][2] + v[0][1] * v[1][2] * v[2][0] + v[0][2] * v[1][0] * v[2][1] -
                        v[0][1] * v[1][0] * v[2][2] - v[0][0] * v[1][2] * v[2][1] - v[0][2] * v[1][1] * v[2][0];
                    break;
                default:
                    break;
                }
                 orientations[k] = (byte)(d > 0 ? 1 : 0);   
            }
        }
    }
    
    public static final void recomputeOrientations(CellArray ca, FloatLargeArray coords, int trueNSpace)
    {
        if (ca.getDim() != trueNSpace)
            return;
        Thread[] workThreads = new Thread[N_THREADS];
        for (int iThread = 0; iThread < N_THREADS; iThread++)
            workThreads[iThread] = new Thread(new CorrectPart(iThread, ca, coords, trueNSpace));
        for (int iThread = 0; iThread < N_THREADS; iThread++)
            workThreads[iThread].start();
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
    }
    
    private static class Correct2DPart implements Runnable
    {
        private final int iThread ;
        private final FloatLargeArray coords;
        private final FloatLargeArray normals;
        private final CellArray ca;
        private final int nCellNodes;
        private final int[] nodes;
        private final int[] orv;
        private final byte[] orientations;
        
        public Correct2DPart(int iThread, CellArray ca, FloatLargeArray coords, FloatLargeArray normals)
        {
            this.iThread = iThread;
            this.coords = coords;
            this.normals = normals;
            this.ca = ca; 
            nodes = ca.getNodes();
            orv = ca.getType().getOrientingVerts();
            orientations = ca.getOrientations();
            nCellNodes = ca.getNCellNodes();
        }

        @Override
        public void run()
        {
            for (int k = iThread; k < ca.getNCells(); k += N_THREADS) {
                int[] verts = new int[3];
                float[][] v = new float[3][3];
                float d = 0;
                for (int i = 0; i < verts.length; i++)
                    verts[i] = nodes[k * nCellNodes + orv[i]];
                for (int i = 0; i < 2; i++)
                    for (int j = 0; j < 3; j++)
                        v[i][j] = coords.getFloat(3 * verts[i + 1] + j) - 
                                  coords.getFloat(3 * verts[0] + j);
                for (int j = 0; j < 3; j++)
                    v[2][j] = normals.getFloat(3 * verts[0] + j);
                d = v[0][0] * v[1][1] * v[2][2] + v[0][1] * v[1][2] * v[2][0] + v[0][2] * v[1][0] * v[2][1] -
                    v[0][1] * v[1][0] * v[2][2] - v[0][0] * v[1][2] * v[2][1] - v[0][2] * v[1][1] * v[2][0];
                 orientations[k] = (byte)(d > 0 ? 1 : 0);   
            }
        }
    }
    
    public static final void recompute2DOrientations(CellArray ca, FloatLargeArray coords, FloatLargeArray normals)
    {
        if (ca.getDim() != 2 || normals == null)
            return;
        Thread[] workThreads = new Thread[N_THREADS];
        for (int iThread = 0; iThread < N_THREADS; iThread++)
            workThreads[iThread] = new Thread(new Correct2DPart(iThread, ca, coords, normals));
        for (int iThread = 0; iThread < N_THREADS; iThread++)
            workThreads[iThread].start();
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
    }
    
    
    private static class CorrectPlanarPart implements Runnable
    {
        private final int iThread ;
        private final FloatLargeArray coords;
        private final float[] normal;
        private final CellArray ca;
        private final int nCellNodes;
        private final int[] nodes;
        private final int[] orv;
        private final byte[] orientations;
        
        public CorrectPlanarPart(int iThread, CellArray ca, FloatLargeArray coords, float[] normal)
        {
            this.iThread = iThread;
            this.coords = coords;
            this.normal = normal;
            this.ca = ca; 
            nodes = ca.getNodes();
            orv = ca.getType().getOrientingVerts();
            orientations = ca.getOrientations();
            nCellNodes = ca.getNCellNodes();
        }

        @Override
        public void run()
        {
            for (int k = iThread; k < ca.getNCells(); k += N_THREADS) {
                int[] verts = new int[3];
                float[][] v = new float[3][3];
                v[2] = normal;
                float d = 0;
                for (int i = 0; i < verts.length; i++)
                    verts[i] = nodes[k * nCellNodes + orv[i]];
                for (int i = 0; i < 2; i++)
                    for (int j = 0; j < 3; j++)
                        v[i][j] = coords.getFloat(3 * verts[i + 1] + j) - 
                                  coords.getFloat(3 * verts[0] + j);
                d = v[0][0] * v[1][1] * v[2][2] + v[0][1] * v[1][2] * v[2][0] + v[0][2] * v[1][0] * v[2][1] -
                    v[0][1] * v[1][0] * v[2][2] - v[0][0] * v[1][2] * v[2][1] - v[0][2] * v[1][1] * v[2][0];
                 orientations[k] = (byte)(d > 0 ? 1 : 0);   
            }
        }
    }
    
    public static final void recomputePlanarOrientations(CellArray ca, FloatLargeArray coords, float[] normal)
    {
        if (ca.getDim() != 2 || normal == null)
            return;
        Thread[] workThreads = new Thread[N_THREADS];
        for (int iThread = 0; iThread < N_THREADS; iThread++)
            workThreads[iThread] = new Thread(new CorrectPlanarPart(iThread, ca, coords, normal));
        for (int iThread = 0; iThread < N_THREADS; iThread++)
            workThreads[iThread].start();
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
    }
    
    public static final void recomputeOrientations(IrregularField fld)
    {
        FloatLargeArray coords = fld.getCurrentCoords();
        for (CellSet cs : fld.getCellSets()) 
            for (CellArray ca : cs.getCellArrays()) {
                if (ca == null)
                    continue;
                if (ca.getDim() == fld.getTrueNSpace()) 
                    recomputeOrientations(ca, coords, fld.getTrueNSpace());
                else if (ca.getDim() == 2 && fld.getNormals() != null)
                    recompute2DOrientations(ca, coords, fld.getNormals());
            }
    }
    
    public static final void recomputeOrientations(IrregularField fld, float[] normal)
    {
        FloatLargeArray coords = fld.getCurrentCoords();
        for (CellSet cs : fld.getCellSets()) 
            for (CellArray ca : cs.getCellArrays()) {
                if (ca == null)
                    continue;
                else if (ca.getDim() == 2 && normal != null)
                    recomputePlanarOrientations(ca, coords, normal);
            }
    }
}
