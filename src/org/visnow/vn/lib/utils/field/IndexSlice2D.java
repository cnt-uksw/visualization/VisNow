/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.SliceUtils;
import org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;
import static org.visnow.vn.lib.utils.interpolation.SubsetGeometryComponents.*;

/**
 *
 * @author know
 */


public class IndexSlice2D extends IndexSlice
{
    
    private IndexSlice2D(RegularField inField, IndexSliceParams params) throws Exception
    {
        if (inField == null || inField.getDimNum() != 3)
            throw new IllegalArgumentException("in IndexSlice2D inField must be regular 3D");
        this.inField = inField;
        this.params = params;
        inDims = inField.getDims();
        lDims =  inField.getLDims();
        fSlice = new float[inDims.length - 2];
        low = new int[2];
        up = new int[2];
        outDims = new int[2];
        nOutNodes = 1;
        for (int i = 0, j = 0; i < 3; i++) 
            if (params.getFixed()[i]) {
                axis = i;
                fSlice[0] = params.getPosition()[i];
            }
            else {
                low[j] = params.getLow()[i];
                up[j] =  params.getUp()[i];
                outDims[j] = up[j] - low[j];
                nOutNodes *= outDims[j];
                j += 1;
            }
        collectData(params.isAdjusting());
    }
    
    
    void computeSlicedData()
    {
        int slice = (int)fSlice[0];
        long start = 0, n0 = outDims[0], step0 = 1, n1 = outDims[1], step1 = 0;
        int offset = 0;
        switch (axis) {
            case 2:
                start = slice * inDims[0] * inDims[1] + low[1] * inDims[0] + low[0];
                step0 = 1;
                step1 = inDims[0];
                offset = inDims[0] * inDims[1];
                break;
            case 1:
                start = slice * inDims[0] + low[0];
                step0 = 1; 
                step1 = inDims[1] * inDims[0];
                offset = inDims[0];
                break;
            case 0:
                start = low[1] * inDims[0] * inDims[1] + low[0] * inDims[0] + slice;
                step0 = inDims[0]; 
                step1 = inDims[1] * inDims[0];
                offset = 1;
                break;
        }
        for (int idata =  0; idata < inData.length; idata++) {
            long veclen = (long)vlens[idata];
            if (slice == fSlice[0]){
                if (inData[idata] instanceof FloatLargeArray)
                    outData[idata] = SliceUtils.get2DSlice(inData[idata], start, n0, step0, n1, step1, veclen);
                else {
                    LargeArray tmp = SliceUtils.get2DSlice(inData[idata], start, n0, step0, n1, step1, veclen);
                    outData[idata] = new FloatLargeArray(tmp.length());
                    for (long i = 0; i < tmp.length(); i++) 
                        outData[idata].setFloat(i, tmp.getFloat(i));
                }
            }
            else {
                float t = fSlice[0] - slice;
                LargeArray d0 = SliceUtils.get2DSlice(inData[idata], start, n0, step0, n1, step1, veclen);
                LargeArray d1 = SliceUtils.get2DSlice(inData[idata], start + offset, n0, step0, n1, step1, veclen);
                for (long i = 0; i < outData[idata].length(); i++)
                    outData[idata].setFloat(i, t * d1.getFloat(i) + (1 - t) * d0.getFloat(i));
            }
        }
    }
    
    void addIndexCoords()
    {
        float[] indexCoords= new float[3 * nOutNodes];
        for (int i = 0, l = 0; i < outDims[1]; i++) 
            for (int j = 0; j < outDims[0]; j++, l += 3) 
                switch (axis) {
                    case 0:
                        indexCoords[l] =     fSlice[0];
                        indexCoords[l + 1] = j + low[0];
                        indexCoords[l + 2] = i + low[1];
                        break;
                    case 1:
                        indexCoords[l] =     j + low[0];
                        indexCoords[l + 1] = fSlice[0];
                        indexCoords[l + 2] = i + low[1];
                        break;
                    case 2:
                        indexCoords[l] =     j + low[0];
                        indexCoords[l + 1] = i + low[1];
                        indexCoords[l + 2] = fSlice[0];
                        break;
                }
        if (regularSlice.getComponent(INDEX_COORDS) != null)
            regularSlice.removeComponent(INDEX_COORDS);
        regularSlice.addComponent(DataArray.create(indexCoords, 3, INDEX_COORDS));
    }

    
    public static RegularField slice(RegularField inField, IndexSliceParams params)
    {
        try {
            return (RegularField)(new IndexSlice2D(inField, params).slice(params.isAdjusting()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
}
