/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field.subset.subvolume;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */


public class NewNode
{
    public final long p0, p1;
    private int index = -1;
    public final float ratio;
    
/**
 * Creates a newNode on an existing cell edge
 * @param p0    
 * @param p1    edge vertices (will be stored in increasing order)
 * @param ratio relative position - newNode = ratio * p0 + (1 - ratio) * p1
 */
    public NewNode(long p0, long p1, float ratio)
    {
        if (p0 < p1){
            this.p0 = p0;
            this.p1 = p1;
            this.ratio = Math.min(.999f, Math.max(.001f, ratio));
        } else {
            this.p0 = p1;
            this.p1 = p0;
            this.ratio = 1 - Math.min(.999f, Math.max(.001f, ratio));
        }
    }
    
    public NewNode(int index, int p0, int p1, float ratio)
    {
        this.index = index;
        if (p0 < p1){
            this.p0 = p0;
            this.p1 = p1;
            this.ratio = Math.min(.999f, Math.max(.001f, ratio));
        } else {
            this.p0 = p1;
            this.p1 = p0;
            this.ratio = 1 - Math.min(.999f, Math.max(.001f, ratio));
        }
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }
    
/**
 * creates a long hash by interleaving bits of p0 and p1
 * @return  hash
 */
    public long getHash()
    {
        long q0 = p0 & 0x00000000ffffffffL;
        long q1 = p1 & 0x00000000ffffffffL;
        q0 = ((q0 << 16) | q0) & 0x0000ffff0000ffffL;
        q1 = ((q1 << 16) | q1) & 0x0000ffff0000ffffL;
        q0 = ((q0 <<  8) | q0) & 0x00ff00ff00ff00ffL;
        q1 = ((q1 <<  8) | q1) & 0x00ff00ff00ff00ffL;
        q0 = ((q0 <<  4) | q0) & 0x0f0f0f0f0f0f0f0fL;
        q1 = ((q1 <<  4) | q1) & 0x0f0f0f0f0f0f0f0fL;
        q0 = ((q0 <<  2) | q0) & 0x3333333333333333L;
        q1 = ((q1 <<  2) | q1) & 0x3333333333333333L;
        q0 = ((q0 <<  1) | q0) & 0x5555555555555555L;
        q1 = ((q1 <<  1) | q1) & 0x5555555555555555L;
        return q0 << 1 | q1;
    }
}
