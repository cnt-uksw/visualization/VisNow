/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field.subset.subvolume;

import java.util.EnumMap;
import org.visnow.jscic.cells.CellType;
import static org.visnow.jscic.cells.CellType.*;
import org.visnow.vn.lib.utils.field.subset.subvolume.FieldSplitter.NewCell;
import org.visnow.vn.lib.utils.field.subset.subvolume.FieldSplitter.PreservedNodes;
import static org.visnow.vn.lib.utils.field.subset.subvolume.FieldSplitter.Subset.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class SimplexSubCell
{
    /*
     * encode distribution of values of slice equation function as 3-base number 
     * with i-th digit 0 for zero value at i-th node, 1 for negative and 2 for positive value
     * the code will be used to look in the slice maps
     *  node codes meaning: 0 - value in vertex == threshold, 
     *                      1 - value in vertex < threshold, 
     *                      2 - value in vertex > threshold
  */
    public static int simplexCode(float[] v)
    {
        int code = 0;
        for (int i = 0, k = 1; i < v.length; i++, k *= 3)
            if (v[i] < 0)
                code += k;
            else if (v[i] > 0)
                code += 2 * k;
        return code;
    }

    /*
     * encode distribution of values of slice equation function as 3-base number 
     * if over,
     * i-th digit is 0 for zero value at i-th node, 1 for negative and 2 for positive value
     * else
     * i-th digit is 0 for zero value at i-th node, 1 for positive and 2 for negative value
     * the code will be used to look in the subcells mapative and 2 for positive value
     * the code will be used to look in the slice maps
     */
    public static int simplexCode(float[] v, boolean over)
    {
        int code = 0;
        if (over) {
            for (int i = 0, k = 1; i < v.length; i++, k *= 3)
                if (v[i] < 0)
                    code += k;
                else if (v[i] > 0)
                    code += 2 * k;
        } else {
            for (int i = 0, k = 1; i < v.length; i++, k *= 3)
                if (v[i] > 0)
                    code += k;
                else if (v[i] < 0)
                    code += 2 * k;
        }
        return code;
    }

    public static final EnumMap<CellType, int[][]> ADD_NODES = new EnumMap<>(CellType.class);
    public static final EnumMap<CellType, CellType[]> SLICE_TYPE = new EnumMap<>(CellType.class);
    public static final EnumMap<CellType, int[][]> SLICE_VERTS = new EnumMap<>(CellType.class);
    public static final EnumMap<CellType, CellType[]> SUBCELL_TYPE = new EnumMap<>(CellType.class);
    public static final EnumMap<CellType, int[][]> SUBCELL_VERTS = new EnumMap<>(CellType.class);
    
    static
    {
        ADD_NODES.put(POINT, new int[][] {});
        ADD_NODES.put(SEGMENT, new int[][] {{}, {}, {0, 1}});
        ADD_NODES.put(TRIANGLE, new int[][] {{}, {}, {}, {0, 1}, {0, 2}, {1, 2}});
        ADD_NODES.put(TETRA, new int[][] {{}, {}, {}, {}, {0, 1}, {0, 2}, {0, 3}, {1, 2}, {1, 3}, {2, 3}});
        
        SLICE_TYPE.put(POINT, new CellType[] {POINT, EMPTY, EMPTY});
        SLICE_TYPE.put(SEGMENT, new CellType[] {
                SEGMENT, POINT, POINT,//x0
                POINT,   EMPTY, POINT,//x1
                POINT,   POINT, EMPTY});//x2
        SLICE_TYPE.put(TRIANGLE, new CellType[] {
                TRIANGLE, SEGMENT, SEGMENT, //x00
                SEGMENT, EMPTY, SEGMENT,    //x10
                SEGMENT, SEGMENT, EMPTY,    //x20
                SEGMENT, EMPTY, SEGMENT,    //x01
                EMPTY, EMPTY, SEGMENT,      //x11
                SEGMENT, SEGMENT, SEGMENT,  //x21
                SEGMENT, SEGMENT, EMPTY,    //x02
                SEGMENT, SEGMENT, SEGMENT,  //x12
                EMPTY, SEGMENT, EMPTY});    //x22
        SLICE_TYPE.put(TETRA, new CellType[] {
                EMPTY, TRIANGLE, TRIANGLE,      //x000
                TRIANGLE, EMPTY, TRIANGLE,      //x100
                TRIANGLE, TRIANGLE, EMPTY,      //x200

                TRIANGLE, EMPTY, TRIANGLE,      //x010
                EMPTY, EMPTY, TRIANGLE,         //x110
                TRIANGLE, TRIANGLE, TRIANGLE,   //x210

                TRIANGLE, TRIANGLE, EMPTY,      //x020
                TRIANGLE, TRIANGLE, TRIANGLE,   //x120
                EMPTY, TRIANGLE, EMPTY,         //x220

                TRIANGLE, EMPTY, TRIANGLE,      //x001
                EMPTY, EMPTY, TRIANGLE,         //x101
                TRIANGLE, TRIANGLE, TRIANGLE,   //x201

                EMPTY, EMPTY, TRIANGLE,         //x011
                EMPTY, EMPTY, TRIANGLE,         //x111
                TRIANGLE, TRIANGLE, QUAD,       //x211

                TRIANGLE, TRIANGLE, QUAD,       //x021
                TRIANGLE, TRIANGLE, QUAD,       //x121
                TRIANGLE, QUAD, TRIANGLE,       //x221

                TRIANGLE, TRIANGLE, EMPTY,      //x002
                TRIANGLE, TRIANGLE, TRIANGLE,   //x102
                EMPTY, TRIANGLE, EMPTY,         //x202

                TRIANGLE, TRIANGLE, TRIANGLE,   //x012
                TRIANGLE, TRIANGLE, QUAD,       //x112
                TRIANGLE, QUAD, TRIANGLE,       //x212

                EMPTY, TRIANGLE, EMPTY,         //x022
                TRIANGLE, QUAD, TRIANGLE,       //x122
                EMPTY, TRIANGLE, EMPTY});       //x222
        
        SLICE_VERTS.put(POINT, new int[][] {
                {0}, {}, {}
            });
        SLICE_VERTS.put(SEGMENT, new int[][] {
                {0, 1}, {1}, {1}, //x0
                {0}, {}, {2},     //x1
                {0}, {2}, {} });  //x2
        SLICE_VERTS.put(TRIANGLE, new int[][] { 
                {0, 1, 2}, {1, 2}, {1, 2}, //x00
                {0, 2}, {}, {2, 3}, //x10
                {0, 2}, {2, 3}, {}, //x20
                {0, 1}, {}, {1, 4}, //x01
                {}, {}, {3, 4}, //x11
                {0, 5}, {3, 5}, {4, 5}, //x21
                {0, 1}, {1, 4}, {}, //x02
                {0, 5}, {4, 5}, {3, 5}, //x12
                {}, {3, 4}, {}, });//x22
        SLICE_VERTS.put(TETRA, new int[][] {
                {}, {1, 2, 3}, {1, 2, 3}, //x000
                {0, 2, 3}, {}, {2, 3, 4}, //x100
                {0, 2, 3}, {2, 3, 4}, {}, //x200

                {0, 1, 3}, {}, {1, 3, 5},        //x010
                {}, {}, {3, 4, 5},               //x110
                {0, 3, 7}, {3, 4, 7}, {3, 5, 7}, //x210

                {0, 1, 3}, {1, 3, 5}, {},        //x020
                {0, 3, 7}, {3, 5, 7}, {3, 4, 7}, //x120
                {}, {3, 4, 5}, {},               //x220

                {0, 1, 2}, {}, {1, 2, 6},        //x001
                {}, {}, {2, 4, 6},               //x101
                {0, 2, 8}, {2, 4, 8}, {2, 6, 8}, //x201

                {}, {}, {1, 5, 6},                  //x011
                {}, {}, {4, 5, 6},                  //x111
                {0, 7, 8}, {4, 7, 8}, {5, 6, 8, 7}, //x211

                {0, 1, 9}, {1, 5, 9}, {0, 6, 9},    //x021
                {0, 7, 9}, {5, 7, 9}, {4, 6, 9, 7}, //x121
                {0, 8, 9}, {4, 5, 9, 8}, {6, 8, 9}, //x221

                {0, 1, 2}, {1, 2, 6}, {},       //x002
                {0, 2, 8}, {2, 6, 8}, {2, 4, 8}, //x102
                {}, {2, 4, 6}, {},               //x202

                {0, 1, 9}, {1, 6, 9}, {1, 5, 9},    //x012
                {0, 8, 9}, {6, 8, 9}, {4, 5, 9, 8}, //x112
                {0, 7, 9}, {4, 6, 9, 7}, {5, 7, 9}, //x212

                {}, {1, 5, 6}, {},                   //x022
                {0, 7, 8}, {5, 6, 8, 7}, {4, 7, 8},  //x122
                {}, {4, 5, 6}, {} });               //x222
        
        SUBCELL_TYPE.put(POINT, new CellType[] {POINT, EMPTY, POINT});
        SUBCELL_TYPE.put(SEGMENT, new CellType[] {
                SEGMENT, EMPTY, SEGMENT,        //x0
                EMPTY, EMPTY, SEGMENT,          //x1
                SEGMENT, SEGMENT, SEGMENT });   //x2
        SUBCELL_TYPE.put(TRIANGLE, new CellType[] {
                TRIANGLE, EMPTY, TRIANGLE,      //x00
                EMPTY, EMPTY, TRIANGLE,         //x10
                TRIANGLE, TRIANGLE, TRIANGLE,   //x20
                EMPTY, EMPTY, TRIANGLE,         //x01
                EMPTY, EMPTY, TRIANGLE,         //x11
                TRIANGLE, TRIANGLE, QUAD,       //x21
                TRIANGLE, TRIANGLE, TRIANGLE,   //x02
                TRIANGLE, TRIANGLE, QUAD,       //x12
                TRIANGLE, QUAD, TRIANGLE});     //x22
        SUBCELL_TYPE.put(TETRA, new CellType[] {
                TETRA, EMPTY, TETRA, //x000
                EMPTY, EMPTY, TETRA, //x100
                TETRA, TETRA, TETRA, //x200

                EMPTY, EMPTY, TETRA, //x010
                EMPTY, EMPTY, TETRA, //x110
                TETRA, TETRA, PYRAMID,//x210

                TETRA, TETRA, TETRA, //x020
                TETRA, TETRA, PYRAMID,//x120
                TETRA, PYRAMID, TETRA, //x220

                EMPTY, EMPTY, TETRA, //x001
                EMPTY, EMPTY, TETRA, //x101
                TETRA, TETRA, PYRAMID,//x201

                EMPTY, EMPTY, TETRA, //x011
                EMPTY, EMPTY, TETRA, //x111
                TETRA, TETRA, PRISM, //x211

                TETRA, TETRA, PYRAMID,//x021
                TETRA, TETRA, PRISM, //x121
                PYRAMID, PRISM, PRISM, //x221

                TETRA, TETRA, TETRA, //x002
                TETRA, TETRA, PYRAMID,//x102
                TETRA, PYRAMID, TETRA, //x202

                TETRA, TETRA, PYRAMID,//x012
                TETRA, TETRA, PRISM, //x112
                PYRAMID, PRISM, PRISM, //x212

                TETRA, PYRAMID, TETRA, //x022
                PYRAMID, PRISM, PRISM, //x122
                TETRA, PRISM, TETRA });//x222
        
        SUBCELL_VERTS.put(POINT, new int[][] {{1}, {}, {1}});
        SUBCELL_VERTS.put(SEGMENT, new int[][] {
                {1, 2}, {}, {1, 2},         //x0
                {}, {}, {1, 2},             //x1
                {1, 2}, {2, 2}, {1, 2} });  //x2
        SUBCELL_VERTS.put(TRIANGLE, new int[][] {
                {0, 1, 2}, {},           {0, 1, 2},     //x00
                {},        {},           {0, 2, 3},     //x10
                {0, 1, 2}, {1, 2, 3},    {0, 1, 2},     //x20
                {},        {},           {0, 2, 3},     //x01
                {},        {},           {0, 3, 4},     //x11
                {0, 1, 5}, {1, 3, 5},    {0, 1, 5, 4},  //x21
                {0, 1, 2}, {1, 2, 4},    {0, 1, 2},     //x02
                {0, 2, 5}, {2, 4, 5},    {0, 2, 5, 3},  //x12
                {0, 1, 2}, {1, 2, 4, 3}, {0, 1, 2}});   //x22
        SUBCELL_VERTS.put(TETRA, new int[][] {
                {0, 1, 2, 3},    {},                 {0, 1, 2, 3},            //x000
                {},              {},                 {0, 2, 3, 4},            //x100
                {0, 1, 2, 3},    {1, 2, 3, 4},       {0, 1, 2, 3},            //x200

                {},              {},                 {0, 1, 3, 5},            //x010
                {},              {},                 {0, 3, 4, 5},            //x110
                {1, 1, 3, 7},    {1, 3, 4, 7},       {0, 1, 7, 5, 3},         //x210
   
                {0, 1, 2, 3},    {1, 2, 3, 5},       {0, 1, 2, 3},            //x020
                {0, 2, 3, 7},    {2, 3, 5, 7},       {0, 2, 7, 4, 3},         //x120
                {0, 1, 2, 3},    {1, 2, 5, 4, 3},    {0, 2, 2, 3},            //x220

                {},              {},                 {0, 1, 2, 6},            //x001
                {},              {},                 {0, 2, 4, 6},            //x101
                {0, 1, 2, 8},    {1, 2, 4, 8},       {0, 1, 8, 6, 2},         //x201

                {},              {},                 {0, 1, 5, 6},            //x011
                {},              {},                 {0, 4, 5, 6},            //x111
                {0, 1, 7, 8},    {1, 4, 7, 8},       {0, 5, 6, 1, 7, 8},      //x211

                {0, 1, 2, 9},    {1, 2, 5, 9},       {0, 2, 9, 6, 1},         //x021
                {1, 2, 7, 9},    {2, 5, 7, 9},       {0, 4, 6, 2, 7, 9},      //x121
                {1, 2, 9, 8, 0}, {1, 4, 8, 2, 5, 9}, {0, 1, 2, 6, 8, 9},      //x221

                {0, 1, 2, 3},    {1, 2, 3, 6},       {0, 1, 2, 3},            //x002
                {0, 2, 3, 8},    {2, 3, 6, 8},       {0, 3, 8, 4, 2},         //x102
                {0, 1, 2, 3},    {1, 3, 6, 4, 2},    {0, 1, 2, 3},            //x202

                {0, 1, 3, 9},    {1, 3, 6, 9},       {0, 3, 9, 5, 1},         //x012
                {0, 3, 8, 9},    {3, 6, 8, 9},       {0, 4, 5, 3, 8, 9},      //x112
                {1, 3, 9, 7, 0}, {1, 4, 7, 3, 6, 9}, {0, 1, 3, 5, 7, 9},      //x212

                {0, 1, 2, 3},    {2, 3, 6, 5, 1},    {0, 1, 2, 3},            //x022
                {2, 3, 8, 7, 0}, {2, 5, 7, 3, 6, 8}, {0, 2, 3, 4, 7, 8},      //x122
                {0, 1, 2, 3},    {1, 2, 3, 4, 5, 6}, {0, 1, 2, 3}});          //x222
    }
    
    public static final CellType[] SIMPLEX_TYPE = {POINT, SEGMENT, TRIANGLE, TETRA};
    
    public static NewCell cutSimplex(int[] simplexVerts, float[] vals, int position, int dataIndex, float[] normal, boolean orientation, PreservedNodes oldNodes)
    {
        CellType simplexType = SIMPLEX_TYPE[simplexVerts.length - 1];
        
        switch (FieldSplitter.subset(simplexType, vals)) {
        case VOID:
            return null;
        case WHOLE:    
            return new NewCell(simplexType, simplexVerts, new NewNode[simplexVerts.length], normal, dataIndex, oldNodes);
        default:
            int code = simplexCode(vals);
            CellType subsetType = SUBCELL_TYPE.get(simplexType)[code];
            int[] tVerts =        SUBCELL_VERTS.get(simplexType)[code];
            int[] verts = new int[tVerts.length];
            NewNode[] newNodes = new NewNode[verts.length];
            for (int i = 0; i < verts.length; i++) 
                if (tVerts[i] < simplexVerts.length) {
                    newNodes[i] = null;
                    verts[i] = simplexVerts[tVerts[i]];
                    oldNodes.addNode(verts[i]);
                }
                else {
                    int[] addNodes = ADD_NODES.get(simplexType)[tVerts[i]];
                    float ratio = vals[addNodes[1]] / (vals[addNodes[1]] - vals[addNodes[0]]);
                    newNodes[i] = new NewNode(code, simplexVerts[addNodes[0]], simplexVerts[addNodes[1]], ratio);
                    verts[i] = -1;
                }
            return new NewCell(subsetType, verts, newNodes, normal, dataIndex, oldNodes);
        }
    }
    
    
    public static NewCell sliceSimplex(int[] simplexVerts, float[] vals, int dataIndex, float[] normal, PreservedNodes oldNodes)
    {
        CellType simplexType = SIMPLEX_TYPE[simplexVerts.length - 1];
        
        switch (FieldSplitter.slice(simplexType, vals)) {
        case VOID:
            return null;
        case WHOLE:    
            return new NewCell(simplexType, simplexVerts, new NewNode[simplexVerts.length], normal, dataIndex, oldNodes);
        default:
            int code = simplexCode(vals);
            CellType subsetType = SLICE_TYPE.get(simplexType)[code];
            int[] tVerts =        SLICE_VERTS.get(simplexType)[code];
            int[] verts = new int[tVerts.length];
            NewNode[] newNodes = new NewNode[verts.length];
            for (int i = 0; i < verts.length; i++) 
                if (tVerts[i] < simplexVerts.length) {
                    newNodes[i] = null;
                    verts[i] = simplexVerts[tVerts[i]];
                    oldNodes.addNode(verts[i]);
                }
                else {
                    int[] addNodes = ADD_NODES.get(simplexType)[tVerts[i]];
                    float ratio = vals[addNodes[1]] / (vals[addNodes[1]] - vals[addNodes[0]]);
                    newNodes[i] = new NewNode(code, simplexVerts[addNodes[0]], simplexVerts[addNodes[1]], ratio);
                    verts[i] = -1;
                }
            return new NewCell(subsetType, verts, newNodes, normal, dataIndex, oldNodes);
        }
    }
    
    public static NewCell cutSimplex(int[] simplexVerts, float[] vals, int position, int dataIndex, boolean orientation, PreservedNodes oldNodes)
    {
        return cutSimplex( simplexVerts, vals, position, dataIndex, null, orientation, oldNodes);
    }
    
    public static NewCell cutSimplex(int[] simplexVerts, float[] vals, int position, int dataIndex, float[] normal, PreservedNodes oldNodes)
    {
        return cutSimplex(simplexVerts, vals, position, dataIndex, normal, true, oldNodes);
    }
    
    public static NewCell cutSimplex(int[] simplexVerts, float[] vals, int position, int dataIndex, PreservedNodes oldNodes)
    {
        return cutSimplex(simplexVerts, vals, position, dataIndex, null, true, oldNodes);
    }
    public static NewCell cutSimplex(int[] simplexVerts, float[] vals, int position, PreservedNodes oldNodes)
    {
        return cutSimplex(simplexVerts, vals, position, -1, null, true, oldNodes);
    }
}
