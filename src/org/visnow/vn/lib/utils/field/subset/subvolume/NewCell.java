/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field.subset.subvolume;

import java.util.Arrays;
import org.visnow.jscic.cells.CellType;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */
public class NewCell
{
    final CellType type;
    final int[] verts;
    final NewNode[] newVerts;
    final float[] normal;
    final int dataIndex;
    boolean orientation;

/**
 * creates new cell splitting a given simplex
 * @param type      new cell type
 * @param verts     new cell vertices equal to original cell vertices (-1 if a vertex is newly created)
 * @param newVerts  new vertices (newVertices[i] is non-null when verts[i] is -1
 * @param normal    for 2D cells, a versor used for determining new cell orientation (original cell normal or normal at the first vertex)
 * @param dataIndex index to data in the cell set
 */
    public NewCell(CellType type, int[] verts, NewNode[] newVerts, float[] normal, int dataIndex)
    {
        this.type = type;
        this.verts = Arrays.copyOf(verts, verts.length);
        this.newVerts = newVerts;
        for (int i = 0; i < newVerts.length; i++)
            if (newVerts[i] != null)
                verts[i] = -1;
        this.normal = normal;
        this.dataIndex = dataIndex;
    }
}
