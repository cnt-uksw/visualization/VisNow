/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field.subset;

import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.*;

/**
 *
 * @author know
 */


public class GeometricSubsetDefinition {
    
    private GlyphType type = BOX;
    private float[] center  = {0, 0, 0};
    private float   radius  = 1;
    private float[] versorU = {1, 0, 0};
    private float[] versorV = {0, 1, 0};
    private float[] versorW = {0, 0, 1};
    private float   radiusU = 1;
    private float   radiusV = 1;
    private float   radiusW = 1;

    public GeometricSubsetDefinition(GlyphType type,
                                     float[] center,
                                     float   radius,
                                     float[] versorU,
                                     float[] versorV,
                                     float[] versorW,
                                     float   radiusU,
                                     float   radiusV,
                                     float   radiusW ) {
        this.type   = type;
        this.center = center;
        this.radius = radius;
        this.versorU = versorU;
        this.versorV = versorV;
        this.versorW = versorW;
        this.radiusU = radiusU;
        this.radiusV = radiusV;
        this.radiusW = radiusW;
    }

    public GlyphType getType() {
        return type;
    }

    public void setType(GlyphType type) {
        this.type = type;
    }
    
    public float[] getCenter() {
        return center;
    }

    public void setCenter(float[] center) {
        this.center = center;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float[] getVersorU() {
        return versorU;
    }

    public void setVersorU(float[] versorU) {
        this.versorU = versorU;
    }

    public float[] getVersorV() {
        return versorV;
    }

    public void setVersorV(float[] versorV) {
        this.versorV = versorV;
    }

    public float[] getVersorW() {
        return versorW;
    }

    public void setVersorW(float[] versorW) {
        this.versorW = versorW;
    }

    public float getRadiusU() {
        return radiusU;
    }

    public void setRadiusU(float radiusU) {
        this.radiusU = radiusU;
    }

    public float getRadiusV() {
        return radiusV;
    }

    public void setRadiusV(float radiusV) {
        this.radiusV = radiusV;
    }

    public float getRadiusW() {
        return radiusW;
    }

    public void setRadiusW(float radiusW) {
        this.radiusW = radiusW;
    }

    
}
