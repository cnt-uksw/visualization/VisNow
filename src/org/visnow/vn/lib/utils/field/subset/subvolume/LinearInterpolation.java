/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field.subset.subvolume;

import java.util.Vector;
import org.visnow.jlargearrays.ComplexDoubleLargeArray;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.system.main.VisNow;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */


public class LinearInterpolation
{
    private static class InterpolatePart implements Runnable
    {
        private final int n;
        private final int from;
        private final int to;
        private final Vector<TimeData>inVals;
        private final Vector<TimeData>outVals;
        private final int[] vLens;
        private final NewNode[] newNodes;

        public InterpolatePart(int iThread, int nThreads, 
                               Vector<TimeData> inVals, Vector<TimeData> outVals, int[] vLens,
                               NewNode[] newNodes)
        {
            n = newNodes.length;
            from = (int)(n * iThread / nThreads);
            to = (int)(n * (iThread + 1) / nThreads);
            this.inVals = inVals;
            this.outVals = outVals;
            this.vLens = vLens;
            this.newNodes = newNodes;
        }

        @Override
        public void run()
        {
            for (int iData = 0; iData < inVals.size(); iData++) {
                boolean interpolable;
                int vLen = vLens[iData];
                TimeData inTD  = inVals.get(iData);
                switch (inTD.getType()) {
                    case FIELD_DATA_BYTE:
                    case FIELD_DATA_SHORT:
                    case FIELD_DATA_INT:
                    case FIELD_DATA_FLOAT:
                    case FIELD_DATA_DOUBLE:
                        interpolable = true;
                        break;
                    default:
                        interpolable = false;
                }
                TimeData outTD = outVals.get(iData);
                for (int iTime = 0; iTime < outTD.getNSteps(); iTime++) {
                    LargeArray in = inTD.getValue(outTD.getTime(iTime));
                    LargeArray out = outTD.getValue(outTD.getTime(iTime));
                    if (inTD.getType() == DataArrayType.FIELD_DATA_COMPLEX)
                        if (in instanceof ComplexFloatLargeArray)
                            for (int iNode = from, l = from * vLen; iNode < to; iNode++) {
                                NewNode node = newNodes[iNode];
                                for (int i = 0; i < vLen; i++, l++) {
                                    float[] cIn1 = ((ComplexFloatLargeArray) in).get(node.p1 * vLen + i);
                                    float[] cIn0 = ((ComplexFloatLargeArray) in).get(node.p0 * vLen + i);
                                    float[] cOut = new float[2];
                                    for (int j = 0; j < 2; j++)
                                        cOut[j] = node.ratio * cIn1[j] + (1 - node.ratio) * cIn0[j];
                                    ((ComplexFloatLargeArray) out).setComplexFloat(l, cOut);
                                }
                            }
                        else
                            for (int iNode = from, l = from * vLen; iNode < to; iNode++) {
                                NewNode node = newNodes[iNode];
                                for (int i = 0; i < vLen; i++, l++) {
                                    double[] cIn1 = ((ComplexDoubleLargeArray) in).get(node.p1 * vLen + i);
                                    double[] cIn0 = ((ComplexDoubleLargeArray) in).get(node.p0 * vLen + i);
                                    double[] cOut = new double[2];
                                    for (int j = 0; j < 2; j++)
                                        cOut[j] = node.ratio * cIn1[j] + (1 - node.ratio) * cIn0[j];
                                    ((ComplexDoubleLargeArray) out).setComplexDouble(l, cOut);
                                }
                            }
                    else if (interpolable)
                        for (int iNode = from, l = from * vLen; iNode < to; iNode++) {
                            NewNode node = newNodes[iNode];
                            for (int i = 0; i < vLen; i++, l++)
                                out.setFloat(l, node.ratio * in.getFloat(node.p1 * vLen + i)
                                        + (1 - node.ratio) * in.getFloat(node.p0 * vLen + i));
                        }
                    else
                        for (int iNode = from, l = from * vLen; iNode < to; iNode++) {
                            NewNode node = newNodes[iNode];
                            if (node.ratio > .5)
                                for (int i = 0; i < vLen; i++, l++)
                                    LargeArrayUtils.arraycopy(in, node.p1 * vLen, out, l, vLen);
                            else
                                for (int i = 0; i < vLen; i++, l++)
                                    LargeArrayUtils.arraycopy(in, node.p0 * vLen, out, l, vLen);
                        }
                }
            }
        }
    }
    
    public static Vector<TimeData> interpolateToNewNodesSet(int nNodes, Vector<TimeData>inVals, int[] vLens, 
                                                            NewNode[] newNodes, 
                                                            boolean singleTimeMoment)
    {
        int nThreads = VisNow.availableProcessors();
        Vector<TimeData> outVals = new Vector<>();
        for (int i = 0; i < inVals.size(); i++) {
            TimeData inVal = inVals.get(i);
            TimeData outVal = new TimeData(inVal.getType());
            if (singleTimeMoment)
                outVal.setValue(LargeArrayUtils.create(inVal.getType().toLargeArrayType(), nNodes * vLens[i]), inVal.getCurrentTime());
            else
                for (int j = 0; j < inVal.getNSteps(); j++) 
                    outVal.setValue(LargeArrayUtils.create(inVal.getType().toLargeArrayType(), nNodes * vLens[i]), inVal.getTime(j));
            outVals.add(outVal);
        }
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++)
            workThreads[iThread] = new Thread(new InterpolatePart(iThread, nThreads, 
                                             inVals, outVals, vLens, newNodes));
        for (int iThread = 0; iThread < nThreads; iThread++)
            workThreads[iThread].start();
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        return outVals;
    }
}
