/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field.subset;

import java.util.Arrays;
import org.apache.commons.lang3.ArrayUtils;
import org.visnow.jlargearrays.ByteLargeArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;

/**
 *
 * @author know
 */


public class DownsizeSelect {
    
    public static long[] select(Field inField, 
                                boolean random, int maxNGlyphs, 
                                NodeSelector nodeSelector,
                                int[] tLow, int[] tUp, int[] down, boolean centered)
    {
        int nGlyphs = 0;
        long[] gl = new long[maxNGlyphs];
        nGlyphs = 0;
        if (inField instanceof RegularField && !random) {
            RegularField inRegularField = (RegularField) inField;
            int[] dims = inRegularField.getDims();
            int[] low  = ArrayUtils.clone(tLow);
            int[] up   = ArrayUtils.clone(tUp);
            for (int i = 0; i < dims.length; i++) {
                maxNGlyphs *= (up[i] - low[i]) / down[i] + 1;
                if (centered){
                    int shift = (up[i] - low[i]) % down[i];
                    low[i] += shift / 2;
                }
            }
            Arrays.fill(gl, -1);
            long l;
            switch (dims.length) {
                case 3:
                    for (long i = low[2]; i < up[2]; i += down[2])
                        for (long j = low[1]; j < up[1]; j += down[1]) {
                            l = (dims[1] * i + j) * dims[0] + low[0];
                            for (long k = low[0]; k < up[0]; k += down[0], l += down[0])
                                if (nodeSelector.isValid(l)) {
                                    gl[nGlyphs] = l;
                                    nGlyphs += 1;
                                }
                        }
                    break;
                case 2:
                    for (long j = low[1]; j < up[1]; j += down[1]) {
                        l = j * dims[0] + low[0];
                        for (long k = low[0]; k < up[0]; k += down[0], l += down[0])
                            if (nodeSelector.isValid(l)) {
                                gl[nGlyphs] = l;
                                nGlyphs += 1;
                            }
                    }
                    break;
                case 1:
                    l = low[0];
                    for (long k = low[0]; k < up[0]; k += down[0], l += down[0])
                        if (nodeSelector.isValid(l)) {
                            gl[nGlyphs] = l;
                            nGlyphs += 1;
                        }
                    break;
            }
        } else {
            Arrays.fill(gl, -1);
            if (maxNGlyphs >= inField.getNNodes()) {
                for (long i = 0; i < inField.getNNodes(); i ++)
                    if (nodeSelector.isValid(i)) {
                        gl[nGlyphs] = i;
                        nGlyphs += 1;
                    }
            }
            else {
                int nValidPoints = 0;
                for (long i = 0; i < inField.getNNodes(); i ++)
                    if (nodeSelector.isValid(i))
                        nValidPoints += 1;
                if (nValidPoints < maxNGlyphs) {
                    for (long i = 0; i < inField.getNNodes(); i ++)
                        if (nodeSelector.isValid(i) && nGlyphs < gl.length) {
                            gl[nGlyphs] = i;
                            nGlyphs += 1;
                        }
                }
                else {
                    boolean dense = maxNGlyphs >  nValidPoints / 2;
                    long nPointsToDraw = dense ? nValidPoints - maxNGlyphs : maxNGlyphs;
                    int drawnPoints = 0;
                    ByteLargeArray drawn = new ByteLargeArray(inField.getNNodes(), true);
                    while (drawnPoints < nPointsToDraw) {
                        long d = (long)(inField.getNNodes() * Math.random());
                        if (nodeSelector.isValid(d) && drawn.getByte(d) == 0) {
                            drawn.setByte(d, (byte)1);
                            drawnPoints += 1;
                        }
                    }
                    byte test = dense ? 0 : (byte)1;
                    for (long i = 0; i < inField.getNNodes() && nGlyphs < gl.length; i++)
                        if (nodeSelector.isValid(i) && drawn.getByte(i) == test) {
                            gl[nGlyphs] = i;
                            nGlyphs += 1;
                        }
                }
            }         
        }
        long[] glyphIn = new long[nGlyphs];
        System.arraycopy(gl, 0, glyphIn, 0, nGlyphs);
        return glyphIn;
    }

}
