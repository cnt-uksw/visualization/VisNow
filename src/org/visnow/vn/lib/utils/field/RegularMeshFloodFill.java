/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import java.util.Arrays;
import org.visnow.jscic.utils.RegularFieldNeighbors;
import org.visnow.vn.lib.utils.FastIntQueue;

/**
 *
 * @author know
 */

public class RegularMeshFloodFill
{

    /**
     * flood fills a connected component of the given region containing nodes from the init array leaving unfilled margins of width 1
     * @param dims dimensions of the given area (dims can be 1-, 2- or 3-element array)
     * @param region region[i] true when i is in the filled area. Length of the region array must be a product of dims
     * @param init points where flood filling starts
     * @return
     */
    public static byte[] floodFill(int[] dims, boolean[] region, int[] init)
    {
        int n = 1;
        for (int i = 0; i < dims.length; i++) 
            n *= dims[i];
        if (n != region.length)
            return null;
        boolean[] reg = new boolean[n];
        byte[] out = new byte[n];
        System.arraycopy(region, 0, reg, 0, n);
        int[] neighbors = RegularFieldNeighbors.neighbors(dims)[0];
        Arrays.fill(out, (byte)0);
        switch (dims.length) {
        case 1:
            reg[0] = reg[n - 1] = false;
            break;
        case 2:
            for (int i = 0; i < dims[0]; i++)
                reg[i] = reg[n - i - 1] = false;
            for (int i = 0; i < n; i += dims[0])
                reg[i] = reg[n - i - 1] = false;
            break;
        case 3:
            for (int i = 0; i < dims[0] * dims[1]; i++)
                reg[i] = reg[n - i - 1] = false;
            for (int i = 0; i < dims[1] * dims[2]; i ++)
                reg[i * dims[0]] = reg[n - 1 - i * dims[0]] = false;
            for (int i = 0; i < dims[2]; i ++) 
                for (int j = 0, k = i * dims[0] * dims[1]; j < dims[0]; j++, k++) 
                    reg[k] = reg[n - k - 1] = false;
            break;
        }
        FastIntQueue queue = new FastIntQueue();
        for (int i = 0; i < init.length; i++) 
            if (reg[init[i]])
        queue.insert(init[i]);
        while (!queue.isEmpty()) {
            int k = queue.get();
            for (int i = 0; i < neighbors.length; i++) {
                int j = k + neighbors[i];
                if (reg[j] && out[j] <= 0) {
                    out[j] = 1;
                    queue.insert(j);
                }
            }
        }
        return out;
    }
}
