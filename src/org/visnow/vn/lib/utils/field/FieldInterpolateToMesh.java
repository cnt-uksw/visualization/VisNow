/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.cells.SimplexPosition;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;



public class FieldInterpolateToMesh
{
    protected Field inField = null;
    protected RegularField inRegularField = null;
    protected Field inMesh = null;
    protected Field outField = null;
    protected int trueDim;
    protected int nThreads = 1;
    protected int nMeshNodes = 0;
    protected float[] meshCoords;
    protected LogicLargeArray valid = null;
    protected int[] vLens;
    protected String[] names;
    protected DataArray[] inData;
    protected LargeArray[] outData;

    public FieldInterpolateToMesh(Field inField, Field inMesh)
    {
        this.inField = inField;
        this.inMesh = inMesh;
    }

    private class PartialInterpolation implements Runnable
    {

        private int iThread;
        private SimplexPosition interp = new SimplexPosition(new int[4], new float[4], null);

        public PartialInterpolation(int iThread)
        {
            this.iThread = iThread;
        }

        @Override
        public void run()
        {
            float[] fs = new float[trueDim];
            int dk = nMeshNodes / nThreads;
            int kstart = iThread * dk + min(iThread, nMeshNodes % nThreads);
            int kend = (iThread + 1) * dk + min(iThread + 1, nMeshNodes % nThreads);
            for (int n = kstart; n < kend; n++) {
                try {
                    System.arraycopy(meshCoords, 3 * n, fs, 0, fs.length);
                    if (inField.getFieldCoords(fs, interp)) {
                        float[] coeffs = interp.getCoords();
                        int[] verts = interp.getVertices();
                        for (int k = 0; k < inData.length; k++) {
                            float[] out = new float[vLens[k]];
                            for (int i = 0; i < out.length; i++)
                                out[i] = 0;
                            for (int i = 0; i < verts.length; i++) {
                                int iv = verts[i];
                                float c = coeffs[i];
                                float[] in = inData[k].getFloatElement(iv);
                                for (int j = 0; j < out.length; j++)
                                    out[j] += c * in[j];
                            }
                            for (int i = 0; i < out.length; i++)
                                outData[k].setFloat(out.length * n + i, out[i]);
                        }
                        valid.setBoolean(n, true);
                    } else {
                        valid.setBoolean(n, false);
                    }

                } catch (Exception e) {
//                    System.out.println("null at " + n + " from " + nMeshNodes);
                }
            }
        }
    }

    private void interpolateIrregularFieldToMesh()
    {
        if (inField.getGeoTree() == null) {
            System.out.println("creating cell tree");
            long start = System.currentTimeMillis();
            inField.createGeoTree();
            System.out.println("cell tree created in " + ((float) (System.currentTimeMillis() - start)) / 1000 + "seconds");
        }
        int nNumericData = 0;
        for (int i = 0; i < inField.getNComponents(); i++)
            if (inField.getComponent(i).isNumeric())
                nNumericData += 1;
        inData = new DataArray[nNumericData];
        outData = new LargeArray[nNumericData];
        vLens = new int[nNumericData];
        names = new String[nNumericData];
        for (int i = 0, j = 0; i < inField.getNComponents(); i++)
            if (inField.getComponent(i).isNumeric()) {
                inData[j] = inField.getComponent(i);
                vLens[j] = inField.getComponent(i).getVectorLength();
                names[j] = inField.getComponent(i).getName();
                outData[j] = new FloatLargeArray(vLens[j] * nMeshNodes);
                j += 1;
            }
//        nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        nThreads = 1;
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new PartialInterpolation(iThread));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        outField.setCurrentMask(valid);
        for (int i = 0; i < outData.length; i++)
            outField.addComponent(DataArray.create(outData[i], vLens[i], names[i]));
    }
    
    public Field interpolate()
    {
        trueDim = inField.getTrueNSpace();
        if (trueDim < 2)
            return null;
        nMeshNodes = (int) inMesh.getNNodes();
        outField = inMesh.cloneShallow();
        if (inMesh.hasCoords())
            meshCoords = inMesh.getCurrentCoords().getData();
        else if (inMesh instanceof RegularField)
            meshCoords = ((RegularField) inMesh).getCoordsFromAffine().getData();
        else
            return null;
        valid = new LogicLargeArray((int) outField.getNNodes());
        if (inField.hasCoords()) {
            interpolateIrregularFieldToMesh();
        } else {
            inRegularField = (RegularField) inField;
            org.visnow.vn.lib.utils.InterpolateToMesh.updateOutField(inRegularField, outField);
        }
        return outField;
    }
}
