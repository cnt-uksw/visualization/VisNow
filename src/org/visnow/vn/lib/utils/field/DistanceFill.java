/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import java.util.Arrays;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.vn.lib.utils.FastIntQueue;

/**
 *
 * @author know
 */
public class DistanceFill
{

    /**
     * creates boolean mask of the region defined by the condition low <lt> data <lt> up
     * the mask is extended by a margin of width 1 in each direction to ensure the condition for proper flood fill
     * <p>
     * @param da   data array defining region
     * @param dims dimensions of the field containing the da data arrray
     * @param low  lower data threshold
     * @param up   upper data threshold: the region is defined by the condition low <lt> data <lt> up
     * <p>
     * @return short array with value = -1 if point has value outside <lt>low,up<gt>, the value linearly mapped to <lt>0,1024<gt> otherwise
     */
    public static short[][] createExtendedRegion(DataArray[] dataArrays, float[] weights, int[] dims, float low, float up)
    {
        int[] xDims = new int[dims.length];
        int rLen = 1;
        for (int i = 0; i < xDims.length; i++) {
            xDims[i] = dims[i] + 2;
            rLen *= xDims[i];
        }
        float r = 1024 / (up - low);
        if (r < 1) r = 1;
        short[][] regions = new short[dataArrays.length][rLen];
        for (int array = 0; array < regions.length; array++) {
            DataArray da = dataArrays[array];
            short[] region = regions[array];
            switch (dims.length) {
                case 3:
                    switch (da.getType()) {
                        case FIELD_DATA_BYTE:
                            byte[] inBData = (byte[]) da.getRawArray().getData();
                            for (int i = 0, l = 0; i < dims[2]; i++)
                                for (int j = 0; j < dims[1]; j++)
                                    for (int k = 0,
                                        m = ((i + 1) * xDims[1] + j + 1) * xDims[0] + 1;
                                        k < dims[0]; k++, l++, m++) {
                                        int p = 0xff & inBData[l];
                                        region[m] = (short) (p < low || p > up ? -1 : p - low);
                                    }
                            break;
                        case FIELD_DATA_SHORT:
                        case FIELD_DATA_INT:
                        case FIELD_DATA_FLOAT:
                        case FIELD_DATA_DOUBLE:
                            LargeArray inDData = da.getRawArray();
                            for (int i = 0, l = 0; i < dims[2]; i++)
                                for (int j = 0; j < dims[1]; j++)
                                    for (int k = 0,
                                        m = ((i + 1) * xDims[1] + j + 1) * xDims[0] + 1;
                                        k < dims[0]; k++, l++, m++)
                                        region[m] = inDData.getDouble(l) < low || inDData.getDouble(l) > up ? -1 : (short) (r * (inDData.getDouble(l) - low));
                            break;
                    }
                    break;
                case 2:
                    switch (da.getType()) {
                        case FIELD_DATA_BYTE:
                            byte[] inBData = (byte[])da.getRawArray().getData();
                            for (int j = 0, l = 0; j < dims[1]; j++)
                                for (int k = 0,
                                    m = (j + 1) * xDims[0] + 1;
                                    k < dims[0]; k++, l++, m++) {
                                    int p = 0xff & inBData[l];
                                    region[m] = (short) (p < low || p > up ? -1 : p - low);
                                }
                            break;
                        case FIELD_DATA_SHORT:
                        case FIELD_DATA_INT:
                        case FIELD_DATA_FLOAT:
                        case FIELD_DATA_DOUBLE:
                            LargeArray inDData = da.getRawArray();
                            for (int j = 0, l = 0; j < dims[1]; j++)
                                for (int k = 0,
                                    m = (j + 1) * xDims[0] + 1;
                                    k < dims[0]; k++, l++, m++)
                                    region[m] = inDData.getDouble(l) < low || inDData.getDouble(l) > up ? -1 : (short) (r * (inDData.getDouble(l) - low));
                            break;
                    }
                    break;
                case 1:
                    switch (da.getType()) {
                        case FIELD_DATA_BYTE:
                            byte[] inBData = (byte[])da.getRawArray().getData();
                            for (int k = 0; k < dims[0]; k++) {
                                int p = 0xff & inBData[k];
                                region[k + 1] = (short) (p < low || p > up ? -1 : p - low);
                            }
                            break;
                        case FIELD_DATA_SHORT:
                        case FIELD_DATA_INT:
                        case FIELD_DATA_FLOAT:
                        case FIELD_DATA_DOUBLE:
                            LargeArray inDData = da.getRawArray();
                            for (int k = 0; k < dims[0]; k++)
                                region[k + 1] = inDData.getDouble(k) < low || inDData.getDouble(k) > up ? -1 : (short) (r * (inDData.getDouble(k) - low));
                            break;
                    }
                    break;
            }

        }
        return regions;
    }

    public static void maskRegion(LogicLargeArray mask, int[] dims, short[][] regions)
    {
        int[] xDims = new int[dims.length];
        for (int i = 0; i < xDims.length; i++)
            xDims[i] = dims[i] + 2;
        for (int reg = 0; reg < regions.length; reg++) {
            short[] region = regions[reg];

            switch (dims.length) {
                case 3:
                    for (int i = 0, l = 0; i < dims[2]; i++)
                        for (int j = 0; j < dims[1]; j++)
                            for (int k = 0,
                                m = ((i + 1) * xDims[1] + j + 1) * xDims[0] + 1;
                                k < dims[0]; k++, l++, m++)
                                if (!mask.getBoolean(l))
                                    region[m] = -1;
                    break;
                case 2:
                    for (int j = 0, l = 0; j < dims[1]; j++)
                        for (int k = 0,
                            m = (j + 1) * xDims[0] + 1;
                            k < dims[0]; k++, l++, m++)
                            if (!mask.getBoolean(l))
                                region[m] = -1;

                    break;
                case 1:
                    for (int k = 1; k <= dims[0]; k++)
                        if (!mask.getBoolean(k - 1))
                            region[k] = -1;
            }
        }

    }

    /**
     * fills a connected component of a 1D, 2D or 3D area with the value fillVal using flood fill algorithm
     * <p>
     * @param maxDist        The method will compute distances from the start point set only up to maxDist
     * @param vals           arrays with the region to be processed with margins of the width 1
     *                       along the boundaries of the array must be filled by -1, otherwise unpredictable behavior can occur
     * @param weights        of each value distance
     * @param start          array of indices of the initial flood fill points
     * @param recomputeStart
     * @param dims           dimensions of the original box
     * @param neighbors      array of offsets from a given point in <code>region</code> to its neighbors
     * @param out            modifiable array, all points in the component found will be set to fillVal.
     *                       It has to be initialized by -1 before calling a sequence of fill method calls
     */
    public static void fill(int maxDist, short[][] vals, float[] weights, int[] start, boolean recomputeStart,
                            int[] dims, int[] neighbors, int[] out)
    {
        FastIntQueue[] queues = new FastIntQueue[maxDist];
        int[] s = start;
        if (recomputeStart) {
            s = new int[start.length];
            for (int i = 0; i < start.length; i++) {
                int k = start[i];
                int l = k / dims[0];
                switch (dims.length) {
                    case 1:
                        s[i] = k + 1;
                        break;
                    case 2:
                        s[i] = (dims[0] + 2) * (l + 1) + k % dims[0] + 1;
                        break;
                    case 3:
                        int m = l / dims[1];
                        s[i] = ((dims[1] + 2) * (m + 1) + l % dims[1] + 1) * (dims[0] + 2) + k % dims[0] + 1;
                }
            }
        }
        Arrays.fill(out, maxDist);
        queues[0].init(s);
        for (int iQueue = 0; iQueue < queues.length; iQueue++) {
            FastIntQueue queue = queues[iQueue];
            while (!queue.isEmpty()) {
                int k = queue.get();
                int v = out[k];
                for (int i = 0; i < neighbors.length; i++) {
                    int j = k + neighbors[i];
                    if (vals[0][j] >= 0) {
                        float d = 0;
                        for (int l = 0; l < vals.length; l++)
                            d += weights[l] * (vals[l][j] - vals[l][k]) * (vals[l][j] - vals[l][k]);
                        if (v + d < out[j]) {
                            out[j] = v + (int) d;
                            if (out[j] < queues.length) {
                                if (queues[out[j]] == null)
                                    queues[out[j]] = new FastIntQueue();
                                queues[out[j]].insert(j);
                            }
                        }
                    }
                }
            }
            queues[iQueue] = null;
        }
    }

    public static int[] restoreResultDimensions(int[] dims, int[] xOut)
    {
        int nOut = 1;
        int[] xDims = new int[dims.length];
        for (int i = 0; i < xDims.length; i++) {
            xDims[i] = dims[i] + 2;
            nOut *= dims[i];
        }
        int[] out = new int[nOut];
        switch (dims.length) {
            case 3:
                for (int i = 0, l = 0; i < dims[2]; i++)
                    for (int j = 0; j < dims[1]; j++)
                        for (int k = 0,
                            m = ((i + 1) * xDims[1] + j + 1) * xDims[0] + 1;
                            k < dims[0]; k++, l++, m++) {
                            out[l] = xOut[m];
                        }
                break;
            case 2:
                for (int j = 0, l = 0; j < dims[1]; j++)
                    for (int k = 0,
                        m = (j + 1) * xDims[0] + 1;
                        k < dims[0]; k++, l++, m++) {
                        out[l] = xOut[m];
                    }
                break;
            case 1:
                for (int k = 0; k < dims[0]; k++) {
                    out[k] = xOut[k + 1];
                }
                break;
        }
        return out;
    }

    private DistanceFill()
    {
    }

}
