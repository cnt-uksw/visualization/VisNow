/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;
import static org.visnow.vn.lib.utils.interpolation.SubsetGeometryComponents.*;

/**
 *
 * @author know
 */


public class IndexSliceCodim0 extends IndexSlice
{
    private final int nDim;
    
    private IndexSliceCodim0(RegularField inField, IndexSliceParams params) throws Exception
    {
        this.inField = inField;
        this.params = params;
        inDims = inField.getDims();
        lDims =  inField.getLDims();
        nDim = inDims.length;
        low = params.getLow();
        up  = params.getUp();
        outDims = new int[nDim];
        nOutNodes = 1;
        for (int i = 0; i < nDim; i++) {
            outDims[i] = up[i] - low[i];
            nOutNodes *= outDims[i];
        }
        collectData(params.isAdjusting());
    }
    
    
    void computeSlicedData()
    {
        int[] xInDims   = new int[nDim];
        int[] xOutDims  = new int[nDim];
        int[] xlow      = new int[nDim];
        int[] dest      = new int[nDim];
        for (int i = 0; i < nDim; i++) {
            xInDims[i]  = inDims[i];
            xlow[i]     = low[i];
            xOutDims[i] = outDims[i];
            dest[i]     = 0;
        }
        for (int idata =  0; idata < inData.length; idata++) {
            int veclen = vlens[idata];
            xInDims[0] = veclen * inDims[0];
            xOutDims[0] = veclen * outDims[0];
            xlow[0] = veclen * low[0];
            
            LargeArrayUtils.subarraycopy(inData[idata], xInDims, xlow, outData[idata], xOutDims, dest, xOutDims);
        }
    }
    
    void addIndexCoords()
    {
        FloatLargeArray indexCoords= new  FloatLargeArray(nDim * nOutNodes);
        for (long i = 0, l = 0; i < nOutNodes; i++) {
            long ii = i;
            for (int k = 0; k < nDim; k++, l ++) {
                indexCoords.setFloat(l, (float)(low[k] + ii % outDims[k]));
                ii /= outDims[k];
            }
        }
        if (regularSlice.getComponent(INDEX_COORDS) != null)
            regularSlice.removeComponent(INDEX_COORDS);
        regularSlice.addComponent(DataArray.create(indexCoords, nDim, INDEX_COORDS));
    }

    
    public static RegularField slice(RegularField inField, IndexSliceParams params)
    {
        try {
            return (RegularField)(new IndexSliceCodim0(inField, params).slice(params.isAdjusting()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
}
