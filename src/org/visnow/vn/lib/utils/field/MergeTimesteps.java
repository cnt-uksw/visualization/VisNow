/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import java.util.Arrays;
import org.visnow.jscic.Field;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class MergeTimesteps
{


    /**
     * Checks compatibility of two DataArraySchemas.
     *
     * @param s0                  DataArraySchema to be checked for compatibility
     * @param s1                  DataArraySchema to be checked for compatibility
     *
     * @return true if name, type, veclen and units of s are compatible
     */
    public static boolean weaklyCompatible(DataArraySchema s0, DataArraySchema s1)
    {
        return s0.getType()         == s1.getType() &&
               s0.getVectorLength() == s1.getVectorLength() &&
               s0.getNElements() == s1.getNElements() &&
               ((s0.getUnit() == null && s1.getUnit() == null) ||
                (s0.getUnit() != null && s1.getUnit() != null &&
                 s0.getUnit().equals(s1.getUnit()))) &&
               ((s0.getMatrixDims() == null && s1.getMatrixDims() == null) ||
                (s0.getMatrixDims() != null && s1.getMatrixDims() != null &&
                 Arrays.equals(s0.getMatrixDims(), s1.getMatrixDims()))) &&
               s0.isSymmetric() == s1.isSymmetric();
    }

    /**
     * Checks compatibility of two DataArrays.
     *
     * @param s0                  DataArray to be checked for compatibility
     * @param s1                  DataArray to be checked for compatibility
     *
     * @return true if name, type, veclen and units of s are compatible
     */
    public static boolean weaklyCompatible(DataArray s0, DataArray s1)
    {
        return weaklyCompatible(s0.getSchema(), s1.getSchema());
    }

    public static Field mergeTimesteps(Field outField, Field inField1,
                                       boolean mergeCoords,
                                       boolean mergeData,
                                       boolean mergeMasks,
                                       boolean appendTime)
    {
        float maxt = -Float.MAX_VALUE;
        float t;
        if (outField == null) {
            outField = inField1.cloneDeep();
            if (outField.getCoords() == null)
                outField.addCoords(((RegularField)outField).getCoordsFromAffine());
            return outField;
        }
        if (inField1.getNNodes() == outField.getNNodes()) {
            if (mergeCoords) {
                TimeData outCoords = outField.getCoords();
                float endTime = outCoords.getEndTime();
                TimeData inCoords = inField1.getCoords();
                if (inCoords == null) {
                    inCoords = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
                    inCoords.setValue(((RegularField)inField1).getCoordsFromAffine(), 0);
                }
                int nSteps = inCoords.getNSteps();
                float startTime = inCoords.getStartTime();
                for (int i = 0; i < nSteps; i++) {
                    t = inCoords.getTime(i);
                    if (appendTime)
                        t += endTime + 1 - startTime;
                    outCoords.setValue(inCoords.getValues().get(i), t);
                    maxt = max(maxt, t);
                }
            }

            if (mergeMasks && outField.getMask() != null && inField1.getMask() != null) {
                TimeData outMasks = outField.getMask();
                float endTime = outMasks.getEndTime();
                TimeData inMasks = inField1.getMask();
                int nSteps = inMasks.getNSteps();
                float startTime = inMasks.getStartTime();
                for (int i = 0; i < nSteps; i++) {
                    t = inMasks.getTime(i);
                    if (appendTime)
                        t += endTime + 1 - startTime;
                    outMasks.setValue(inMasks.getValues().get(i), t);
                    maxt = max(maxt, t);
                }
            }

            if (mergeData) {
                for (int iData = 0; iData < inField1.getNComponents(); iData++) {
                    DataArray inDA = inField1.getComponent(iData);
                    DataArray outDA = outField.getComponent(inDA.getName());
                    if (outDA == null) {
                        outField.addComponent(inDA.cloneDeep());
                        continue;
                    }
                    if (weaklyCompatible(outDA, inDA)) {
                        float startTime, endTime;
                        int nSteps;
                        TimeData bOutData = outDA.getTimeData();
                        endTime = bOutData.getEndTime();
                        TimeData bInData =  inDA.getTimeData();
                        nSteps = bInData.getNSteps();
                        startTime = bInData.getStartTime();
                        for (int i = 0; i < nSteps; i++) {
                            t = bInData.getTime(i);
                            if (appendTime)
                                t += endTime + 1 - startTime;
                            bOutData.setValue(bInData.getValues().get(i), t);
                            maxt = max(maxt, t);
                        }
                    }
                }
            }
        }
        outField.setCurrentTime(maxt);
        return outField;
    }
}
