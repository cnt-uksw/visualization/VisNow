/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.field;

import java.util.ArrayList;
import java.util.Arrays;
import org.visnow.jlargearrays.ByteLargeArray;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.ShortLargeArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;

/**
 *
 * @author know
 */
public class VectorComponentCombiner
{

    private static char[][] vComponentNames = {{'X', 'Y', 'Z'}, {'x', 'y', 'z'},
                                               {'U', 'V', 'W'}, {'u', 'v', 'w'},
                                               {'0', '1', '2'}, {'1', '2', '3'}};

    private static class VectorComponentData
    {
        final String name;
        final int vLen;
        final int[] components;

        public VectorComponentData(String name, int vLen, int[] components) {
            this.name = name;
            this.vLen = vLen;
            this.components = components;
        }

    }
    
    private static VectorComponentData[] vectorComponents(DataContainer inData, boolean[] retain)
    {
        VectorComponentData[] vectorComponents = new VectorComponentData[inData.getNComponents()];
        Arrays.fill(retain, true);
        for (int i = 0; i < inData.getNComponents(); i++) {
            DataArray da = inData.getComponent(i);
            if (!da.isNumeric() || da.getVectorLength() != 1)
                continue;
            String cName = da.getName();
            int[] cmps = new int[inData.getNComponents()];
            cmps[0] = i;
    //looking for a vector component of the type {x..., y..., z...}        
            for (char[] vCmpChar : vComponentNames) 
                if (cName.charAt(0) == vCmpChar[0]) {       // da is a candidate for the first coordinate of a new vector component
                    int vLen = 1;
                    for (int j = 1; j < vCmpChar.length; j++) {// looking for further coordinates
                        boolean found = false;
                        for (int k = 0; k < inData.getNComponents(); k++) {
                            DataArray cda = inData.getComponent(k);
                            if (cda.getName().charAt(0) == vCmpChar[j] &&
                                cda.getName().substring(1).equals(cName.substring(1)) &&
                                cda.getType() == da.getType() && cda.getVectorLength() == 1) {
                                vLen += 1;
                                retain[k] = false;
                                cmps[j] = k;
                                found = true;
                            }
                        }
                        if (!found)
                            break;
                    }
                    if (vLen > 1) {
                        retain[i] = false;
                        vectorComponents[i] = new VectorComponentData(cName.substring(1), vLen, Arrays.copyOf(cmps, vLen));
                    }
            }
    //looking for a vector component of the type {...x, ...y, ...z}        
            int n = cName.length() - 1;
            for (char[] vCmpChar : vComponentNames) 
                if (cName.charAt(n) == vCmpChar[0]) {       // da is a candidate for the first coordinate of a new vector component
                    int vLen = 1;
                    for (int j = 1; j < vCmpChar.length; j++) {// looking for further coordinates
                        boolean found = false;
                        for (int k = 0; k < inData.getNComponents(); k++) {
                            DataArray cda = inData.getComponent(k);
                            if (cda.getName().length() - 1 == n &&
                                cda.getName().charAt(n) == vCmpChar[j] &&
                                cda.getName().substring(0, n).equals(cName.substring(0, n)) &&
                                cda.getType() == da.getType() && cda.getVectorLength() == 1) {
                                vLen += 1;
                                retain[k] = false;
                                cmps[j] = k;
                                found = true;
                            }
                        }
                        if (!found)
                            break;
                    }
                    if (vLen > 1) {
                        retain[i] = false;
                        vectorComponents[i] = new VectorComponentData(cName.substring(0, n), vLen, Arrays.copyOf(cmps, vLen));
                    }
                }
        }
        return vectorComponents;
    }

    
    private static void combineVectorsInContainer(DataContainer inData)
    {
        int nInData = inData.getNComponents();
        if(nInData < 2)
            return;
        long nElements = inData.getNElements();
        boolean[] retain = new boolean[nInData];
        VectorComponentData[] mergedData = vectorComponents(inData, retain);
        ArrayList<DataArray> outComponents = new ArrayList<>();
        for (int k = 0; k < nInData; k++) 
            if (retain[k])
                outComponents.add(inData.getComponent(k));
            else if (mergedData[k] != null) {
                DataArray inDA = inData.getComponent(k);
                int vlen = mergedData[k].vLen;
                int[] comps = mergedData[k].components;
                int nFrames = inDA.getNFrames();
                TimeData outTD = new TimeData(inDA.getType());
                for (int i = 0; i < nFrames; i++) {
                    switch (inDA.getType()) {
                    case FIELD_DATA_BYTE:
                        ByteLargeArray outB = new ByteLargeArray(vlen * nElements);
                        for (int j = 0; j < vlen; j++) {
                            ByteLargeArray inB = (ByteLargeArray)inData.getComponent(comps[j]).
                                                                  getTimeData().getValues().get(i);
                            for (long l = 0; l < nElements; l++) 
                                outB.setByte(vlen * l + j, inB.get(l));
                        }
                        outTD.setValue(outB, inDA.getTimeData().getTime(i));
                        break;
                    case FIELD_DATA_SHORT:
                        ShortLargeArray outS = new ShortLargeArray(vlen * nElements);
                        for (int j = 0; j < vlen; j++) {
                            ShortLargeArray inS = (ShortLargeArray)inData.getComponent(comps[j]).
                                                                  getTimeData().getValues().get(i);
                            for (long l = 0; l < nElements; l++) 
                                outS.setShort(vlen * l + j, inS.get(l));
                        }
                        outTD.setValue(outS, inDA.getTimeData().getTime(i));
                        break;
                    case FIELD_DATA_INT:
                        IntLargeArray outI = new IntLargeArray(vlen * nElements);
                        for (int j = 0; j < vlen; j++) {
                            IntLargeArray inI = (IntLargeArray)inData.getComponent(comps[j]).
                                                                  getTimeData().getValues().get(i);
                            for (long l = 0; l < nElements; l++) 
                                outI.setInt(vlen * l + j, inI.get(l));
                        }
                        outTD.setValue(outI, inDA.getTimeData().getTime(i));
                        break;
                    case FIELD_DATA_FLOAT:
                        FloatLargeArray outF = new FloatLargeArray(vlen * nElements);
                        for (int j = 0; j < vlen; j++) {
                            FloatLargeArray inF = (FloatLargeArray)inData.getComponent(comps[j]).
                                                                  getTimeData().getValues().get(i);
                            for (long l = 0; l < nElements; l++) 
                                outF.setFloat(vlen * l + j, inF.get(l));
                        }
                        outTD.setValue(outF, inDA.getTimeData().getTime(i));
                        break;
                    case FIELD_DATA_DOUBLE:
                        DoubleLargeArray outD = new DoubleLargeArray(vlen * nElements);
                        for (int j = 0; j < vlen; j++) {
                            DoubleLargeArray inD = (DoubleLargeArray)inData.getComponent(comps[j]).
                                                                  getTimeData().getValues().get(i);
                            for (long l = 0; l < nElements; l++) 
                                outD.setDouble(vlen * l + j, inD.get(l));
                        }
                        outTD.setValue(outD, inDA.getTimeData().getTime(i));
                        break;
                    }
                }
                
                outComponents.add(DataArray.create(outTD, vlen, 
                                                   mergedData[k].name.startsWith("_") ? 
                                                           mergedData[k].name.substring(1) :
                                                           mergedData[k].name, 
                                                   inDA.getUnit(), inDA.getUserData()));
            }
        inData.removeComponents();
        inData.setComponents(outComponents);
    }
    
    public static RegularField combineVectors(RegularField inField)
    {
        combineVectorsInContainer(inField);
        return inField;
    }
    
    public static IrregularField combineVectors(IrregularField inField)
    {
        combineVectorsInContainer(inField);
        for (CellSet cs : inField.getCellSets()) 
            combineVectorsInContainer(cs);
        return inField;
    }
}
