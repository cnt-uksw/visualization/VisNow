/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.pointProbe;

import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay;
import org.visnow.vn.lib.utils.probeInterfaces.MultiDisplay;
import java.util.Collections;
import org.jogamp.java3d.J3DGraphics2D;
import org.visnow.jscic.IrregularField;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import org.visnow.vn.lib.utils.graphing.GraphParams;
import static org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position.*;
/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class MultiLabels extends MultiDisplay
{
    
    protected GraphParams graphParams = new GraphParams();
    
    @Override
    public void addDisplay(IrregularField field)
    {
        addDisplay(field, new float[] {0, 0, 0});
        graphParams.setTitleInFrame(false);
    }
    
    public void addDisplay(IrregularField field, float[] center)
    {
        if (field != null
                && (probesPosition != TOP && probesPosition != BOTTOM || displays.size() * 180 < windowWidth)
                && (probesPosition != LEFT && probesPosition != RIGHT || displays.size() * 180 < windowHeight))
            displays.add(new PointValuesDisplay(field, center));
    }
    
    public PointValuesDisplay pickedDisplay(int ix, int iy)
    {
        for (ProbeDisplay display : displays) {
            PointValuesDisplay label = (PointValuesDisplay) display;
            if (label.pointInside(ix, iy)) 
                return label;
        }
        return null;
    }

    protected int[] probeOffset = {30, 60};
    protected float relMargin = .025f;

    public void setRelMargin(float relMargin) {
        this.relMargin = relMargin;
    }
    
    protected int currentXPosition = 0;
    protected int currentYPosition = 0;
    protected boolean packing = false;
    
    @Override
    public void draw2D(J3DGraphics2D gr, LocalToWindow ltw, int width, int height)
    {   
        windowWidth  = width;
        windowHeight = height;
        int vMargin = (int)(height * relMargin);
        int hMargin = (int)(width * relMargin);
        if (displays == null || displays.isEmpty())
            return;
        ltw.update();
        if (probesPosition == AT_POINT)
            for (int i = 0; i < displays.size(); i++) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
            }
        else {
            xSort = probesPosition == TOP || probesPosition == BOTTOM;
            for (ProbeDisplay display : displays) {
            }
            Collections.sort(displays);
            switch (probesPosition) {
            case TOP:
                break;
            case BOTTOM:
                break;
            case LEFT:
                break;
            case RIGHT:
                break;
            }
            for (int i = 0; i < displays.size(); i++) {
                switch (probesPosition) {
                case TOP:
                    break;
                case BOTTOM:
                    break;
                case LEFT:
                    break;
                case RIGHT:
                    break;
                }
            }
        }
    }

    public GraphParams getGraphParams()
    {
        return graphParams;
    }

    
    @Override
    public void updateTimeRange()
    {
    }
    
}
