/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.flowVisualizationUtils;


import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class GlyphFlowParams extends Parameters
{
    public static final int FORWARD = 1;
    public static final int STOP = 0;
    public static final int RESET = -1;

    public static final int ONCE = 0;
    public static final int CYCLE = 1;
    public static final int CONTINUOUS = 2;

    public static final int GEOMETRY_CHANGED = 3;
    public static final int GLYPHS_CHANGED = 2;
    public static final int COORDS_CHANGED = 1;
    
    protected static final String COMPONENT      = "glyph component";
    protected static final String DOWN           = "downsize array";
    protected static final String TYPE           = "glyph type";
    protected static final String LOD            = "glyph level of detail";
    protected static final String CONSTANT_SIZE  = "constant glyph size";
    protected static final String SCALE          = "glyph scale";
    protected static final String LINE_WIDTH     = "glyph line width";
    protected static final String USE_ABS        = "use absolute value of component as glyph size";
    protected static final String USE_SQRT       = "use square root of component as glyph size";
    protected static final String N_OF_BATCHES   = "number of glyph batches";
    protected static final String ANIMATION_TYPE = "animation type";
    protected static final String DELAY          = "delay";
    protected static final String ANIMATE        = "animate";
    
    protected static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Integer>(COMPONENT, ParameterType.dependent, 0),
        new ParameterEgg<int[]>(DOWN, ParameterType.dependent, null),
        new ParameterEgg<Integer>(TYPE, ParameterType.independent, 0),
        new ParameterEgg<Integer>(LOD, ParameterType.independent, 1),
        new ParameterEgg<Boolean>(CONSTANT_SIZE, ParameterType.dependent, false),
        new ParameterEgg<Float>(SCALE, ParameterType.dependent, .1f),
        new ParameterEgg<Float>(LINE_WIDTH, ParameterType.dependent, .1f),
        new ParameterEgg<Boolean>(USE_ABS, ParameterType.independent, true),
        new ParameterEgg<Boolean>(USE_SQRT, ParameterType.independent, false),
        new ParameterEgg<Integer>(N_OF_BATCHES, ParameterType.independent, 1),
        new ParameterEgg<Integer>(ANIMATION_TYPE, ParameterType.independent, ONCE),
        new ParameterEgg<Integer>(DELAY, ParameterType.independent, 0),
        new ParameterEgg<Integer>(ANIMATE, ParameterType.independent, STOP)};

    public GlyphFlowParams()
    {
        super(eggs);
        setValue(DOWN, new int[]{5, 5, 5});
    }

    public int getComponent()
    {
        return (Integer) getValue(COMPONENT);
    }

    public void setComponent(int component)
    {
        setValue(COMPONENT, component);
        if (getAnimate() == STOP)
            fireParameterChanged(COMPONENT);
    }

    public int[] getDown()
    {
        return (int[]) getValue(DOWN);
    }

    public void setDown(int[] down)
    {
        setValue(DOWN, down);
        if (getAnimate() == STOP)
            fireParameterChanged(DOWN);
    }

    public int getGlyphType()
    {
        return (Integer) getValue(TYPE);
    }

    public void setGlyphType(int type)
    {
        setValue(TYPE, type);
        if (getAnimate() == STOP)
            fireParameterChanged(TYPE);
    }

    public int getLevelOfDetail()
    {
        return (Integer) getValue(LOD);
    }

    public void setLevelOfDetail(int lod)
    {
        setValue(LOD, lod);
        if (getAnimate() == STOP)
            fireParameterChanged(LOD);
    }

    public boolean isConstantGlyphSize()
    {
        return (Boolean) getValue(CONSTANT_SIZE);
    }

    public void setConstantGlyphSize(boolean cDiam)
    {
        setValue(CONSTANT_SIZE, cDiam);
        if (getAnimate() == STOP)
            fireParameterChanged(CONSTANT_SIZE);
    }

    public float getScale()
    {
        return (Float) getValue(SCALE);
    }

    public void setScale(float scale)
    {
        setValue(SCALE, scale);
        if (getAnimate() == STOP)
            fireParameterChanged(SCALE);
    }

    public float getLineWidth()
    {
        return (Float) getValue(LINE_WIDTH);
    }

    public void setLineWidth(float thickness)
    {
        setValue(LINE_WIDTH, thickness);
        if (getAnimate() == STOP)
            fireParameterChanged(LINE_WIDTH);
    }

    public boolean isUseAbs()
    {
        return (Boolean) getValue(USE_ABS);
    }

    public void setUseAbs(boolean useAbs)
    {
        setValue(USE_ABS, useAbs);
        if (getAnimate() == STOP)
            fireParameterChanged(USE_ABS);
    }

    public boolean isUseSqrt()
    {
        return (Boolean) getValue(USE_SQRT);
    }

    public void setUseSqrt(boolean useSqrt)
    {
        setValue(USE_SQRT, useSqrt);
        if (getAnimate() == STOP)
            fireParameterChanged(USE_SQRT);
    }
    public int getNBatches()
    {
        return (Integer) getValue(N_OF_BATCHES);
    }

    public void setNBatches(int nBatches)
    {
        setValue(N_OF_BATCHES, nBatches);
        if (getAnimate() == STOP)
            fireParameterChanged(N_OF_BATCHES);
    }

    public int getAnimationType()
    {
        return (Integer) getValue(ANIMATION_TYPE);
    }

    public void setAnimationType(int animate)
    {
        setValue(ANIMATION_TYPE, animate);
        if (getAnimate() == STOP)
            fireParameterChanged(ANIMATION_TYPE);
    }

    public int getDelay()
    {
        return (Integer) getValue(DELAY);
    }

    public void setDelay(int delay)
    {
        setValue(DELAY, delay);
        if (getAnimate() == STOP)
            fireParameterChanged(DELAY);
    }
    public int getAnimate()
    {
        return (Integer) getValue(ANIMATE);
    }

    public void setAnimate(int animate)
    {
        setValue(ANIMATE, animate);
        fireParameterChanged(ANIMATE);
    }

    public void setDims(int[] dims)
    {
        int n = (int) (pow(1000., 1. / dims.length));
        int[] downsize = new int[dims.length];
        downsize[0] = (dims[0] + n - 1) / n;
        if (dims.length > 1)
            downsize[1] = (dims[1] + n - 1) / n;
        if (dims.length > 2)
            downsize[2] = (dims[2] + n - 1) / n;
        setDown(downsize);
    }
}
