/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.flowVisualizationUtils;

import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class AnimatedStreamParams extends Parameters
{

    public static enum ANIMATION {FORWARD, STOP, BACK};
    
    public static final String SEGMENT_COUNT = "segment count";
    public static final String SEGMENT_GAP    = "segment gap";
    public static final String LINE_WIDTH     = "line width";
    public static final String DELAY          = "delay";
    public static final String ANIMATE        = "animate";

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Integer>(SEGMENT_COUNT, ParameterType.dependent, 20),
        new ParameterEgg<Integer>(SEGMENT_GAP, ParameterType.dependent, 1),
        new ParameterEgg<Float>(LINE_WIDTH, ParameterType.dependent, 1.f),
        new ParameterEgg<Integer>(DELAY, ParameterType.independent, 0),
        new ParameterEgg<ANIMATION>(ANIMATE, ParameterType.independent, ANIMATION.STOP)
    };

    public AnimatedStreamParams()
    {
        super(eggs);
    }

    public int getGap()
    {
        return (Integer) getValue(SEGMENT_GAP);
    }

    public void setGap(int val)
    {
        if (val < 0)
            return;
        setValue(SEGMENT_GAP, val);
        fireParameterChanged(SEGMENT_GAP);
    }

    public ANIMATION getAnimate()
    {
        return (ANIMATION) getValue(ANIMATE);
    }

    public void setAnimate(ANIMATION animate)
    {
        setValue(ANIMATE, animate);
        fireParameterChanged(ANIMATE);
    }

    public int getDelay()
    {
        return (Integer) getValue(DELAY);
    }

    public void setDelay(int delay)
    {
        setValue(DELAY, delay);
        fireParameterChanged(DELAY);
    }

    public int getSegmentCount()
    {
        return (Integer) getValue(SEGMENT_COUNT);
    }

    public void setSegmentCount(int segmentLength)
    {
        if (segmentLength < 5)
            return;
        setValue(SEGMENT_COUNT, segmentLength);
        fireParameterChanged(SEGMENT_COUNT);
    }
    
    public float getLineWidth()
    {
        return (Float)getValue(LINE_WIDTH);
    }
    
    public void setLineWidth(float val)
    {
        setValue(LINE_WIDTH, val);
        fireParameterChanged(LINE_WIDTH);
    }
    
}
