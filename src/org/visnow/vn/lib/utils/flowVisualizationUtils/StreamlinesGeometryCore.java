/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.flowVisualizationUtils;

import java.util.Arrays;
import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedGeometryArray;
import org.jogamp.java3d.IndexedLineStripArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.ParameterChangeListener;

import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenLineAttributes;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.utils.ColorMapper;

import static org.visnow.vn.lib.utils.flowVisualizationUtils.StreamlinesShared.*;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 * Revisions above 564 modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
abstract public class StreamlinesGeometryCore
{
    protected Field inField;
    protected Field startField;
    protected IrregularField outField = null;
    protected int trueDim = 3;
    protected int nSpace = 3;
    protected float[] startCoords = null;
    protected FloatLargeArray coords = null;
    protected float[] vectors = null;
    protected int nvert = 0;
    protected int[] lines = null;
    protected Parameters parameters = null;
    protected int nForward = 0;
    protected int nBackward = 0;
    protected int step0 = 0;
    protected int nSteps = 0;
    protected int nSrc;           // number of source nodes
    protected int nTrajects;        // number of nontrivialtrajectories
    protected FloatLargeArray vects = null;
    protected int vlen = 3;
    protected float[][] rawCoords = null;
    protected float[][] rawVectors = null;
    protected int[][] stepXt;
    protected int nThreads;
    protected int[] indices = null;
    protected float effectiveScale = .001f, baseScale = 1;
    protected byte[] mask = null;
    protected byte[] colors = null;

    protected StreamlinePresentationParams presentationParams;
    protected ProgressAgent progressAgent = null;
    protected IndexedLineStripArray lineStrips = null;
    protected OpenShape3D lineShape = new OpenShape3D();
    protected OpenAppearance appearance = new OpenAppearance();
    protected OpenLineAttributes lineAttributes = new OpenLineAttributes(1.f, OpenLineAttributes.PATTERN_SOLID, true);
    protected OpenBranchGroup streamlines = new OpenBranchGroup();
    
    
    public StreamlinesGeometryCore(Field inField, float baseScale, 
                                 Parameters parameters, 
                                 StreamlinePresentationParams presentationParams, 
                                 Field startField, ProgressAgent progressAgent)
    {
        this.parameters = parameters;
        this.baseScale = baseScale;
        this.startField = startField;
        nSrc = (int)startField.getNNodes();
        this.inField = inField;
        trueDim = inField.getTrueNSpace();
        if (presentationParams != null) {
            this.presentationParams = presentationParams;
            presentationParams.addParameterChangelistener(new ParameterChangeListener()
            {
                @Override
                public void parameterChanged(String name)
                {
                    if (name.equals(StreamlinePresentationParams.LINE_WIDTH))
                        updateWidth();
                }
            });
        }
        this.progressAgent = progressAgent;
    }
    
    abstract public void setStartPoints();
    
    public void updateStreamlines()
    {
        vects = inField.getComponent(parameters.get(COMPONENT)).getRawFloatArray();
        vlen = inField.getComponent(parameters.get(COMPONENT)).getVectorLength();
        nBackward = step0     = parameters.get(NUM_STEPS_BACKWARD);
        nSteps    = parameters.get(NUM_STEPS_FORWARD) + step0;
        if (step0 > 0 && parameters.get(NUM_STEPS_FORWARD) > 0)
            nSteps -= 1;
        stepXt = new int[nSrc][2];
        rawCoords = new float[nSrc][trueDim * nSteps];
        rawVectors = new float[nSrc][trueDim * nSteps];
        nvert = nSrc * nSteps;
        effectiveScale = parameters.get(STEP) * baseScale;
        nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
    }
    
    public DataArray computeApproximatePeriodicity()
    {
        float[] outCrds = outField.getCoords(0).getData();
        float[] periodicityIndices = new float[nvert];
        int nValidSrc = nvert / nSteps;
        for (int iPoint = 0; iPoint < nValidSrc; iPoint++) {
            float[] startPoint = new float[3];
            System.arraycopy(outCrds, 3 * iPoint, startPoint, 0, 3);
            float maxDist = 0;
            float periodicityIndex = 1;
            for (int step = 1; step < nSteps; step++) 
                try {
                    float[] cPoint = new float[3];
                    System.arraycopy(outCrds, (nValidSrc * step + iPoint) * 3, cPoint, 0, 3);
                    float dist = 0;
                    for (int j = 0; j < 3; j++) 
                        dist += (cPoint[j] - startPoint[j]) * (cPoint[j] - startPoint[j]);
                    if (dist > maxDist)
                        maxDist = dist;
                    if (maxDist > 0)
                    {
                        float currentIndex = dist / maxDist;
                        if (currentIndex < periodicityIndex)
                            periodicityIndex = currentIndex;
                    }
                } catch (Exception e) {
                    System.out.printf("%8d %5d %5d %5d %8d%n", outCrds.length, nSrc, step, iPoint, (nSrc * step + iPoint) * 3);
                }
            periodicityIndex = 1 - (float)Math.sqrt(periodicityIndex);
            for (int i = 0; i < nSteps; i++) 
                periodicityIndices[nValidSrc * i + iPoint] = periodicityIndex;
        }    
        return DataArray.create(periodicityIndices, 1, "periodicity_index", "", null);
    }
    
    protected DataMappingParams dataMappingParams = null;

    public void setDataMappingParams(DataMappingParams dataMappingParams) {
        this.dataMappingParams = dataMappingParams;
    }
    
    public void createStreamlinesGeometry()
    {
        if (outField == null)
            return;
        coords = outField.getCoords(0);
        colors = new byte[4 * nvert];
        int[] coordIndices = new int[nvert];
        lineShape.removeAllGeometries();
        int[] lineLengths = new int[nTrajects];
        Arrays.fill(lineLengths, nSteps);
        for (int i = 0, l = 0; i < nSteps; i++) 
            for (int j = 0; j < nTrajects; j++, l++) 
                coordIndices[j * nSteps + i] = l;
        lineStrips = new IndexedLineStripArray(nvert,
                                               GeometryArray.COORDINATES | 
                                               GeometryArray.COLOR_4 | 
                                               GeometryArray.BY_REFERENCE,
                                               nvert, lineLengths);    
        lineStrips.setCapability(IndexedGeometryArray.ALLOW_COORDINATE_INDEX_READ);
        lineStrips.setCapability(IndexedGeometryArray.ALLOW_COORDINATE_INDEX_WRITE);  
        lineStrips.setCapability(GeometryArray.ALLOW_REF_DATA_READ);
        lineStrips.setCapability(GeometryArray.ALLOW_REF_DATA_WRITE);
        lineStrips.setCoordinateIndices(0, coordIndices);
        lineStrips.setColorIndices(0, coordIndices);
        lineStrips.setCoordRefFloat(coords.getData());
        ColorMapper.map(outField, dataMappingParams, colors);
        lineStrips.setColorRefByte(colors);
        lineShape.addGeometry(lineStrips);
        lineAttributes.setLineWidth(presentationParams.getLineWidth());
        appearance.setLineAttributes(lineAttributes);
        lineShape.setAppearance(appearance);
        streamlines.removeAllChildren();
        streamlines.addChild(lineShape);
    }
    
    public void updateWidth()
    {
        lineAttributes.setLineWidth(presentationParams.getLineWidth());
    }
    
    public void updateColors()
    {
        if (outField == null || dataMappingParams == null || 
            colors == null || lineStrips == null)
            return;
        ColorMapper.map(outField, dataMappingParams, colors);
        lineStrips.setColorRefByte(colors);
    }

    public OpenBranchGroup getGeometry() {
        return streamlines;
    }

    public IrregularField getOutField()
    {
        return outField;
    }

    public int[] getFromSteps()
    {
        int[] fromSteps = new int[nSrc];
        for (int i = 0; i < fromSteps.length; i++) 
            fromSteps[i] = stepXt[i][0];
        return fromSteps;
    }

    public int[] getToSteps()
    {
        int[] toSteps = new int[nSrc];
        for (int i = 0; i < toSteps.length; i++) 
            toSteps[i] = stepXt[i][1];
        return toSteps;
    }

    public int getNTrajects() {
        return nTrajects;
    }

}
