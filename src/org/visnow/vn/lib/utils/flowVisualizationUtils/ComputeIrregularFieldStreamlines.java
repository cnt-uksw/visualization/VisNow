/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.flowVisualizationUtils;

import java.util.Arrays;
import static org.apache.commons.math3.util.FastMath.*;
import org.apache.log4j.Logger;

import org.visnow.vn.lib.utils.numeric.ODE.Deriv;
import org.visnow.vn.lib.utils.numeric.ODE.RungeKutta;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.SimplexPosition;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.cells.CellType;

import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import static org.visnow.vn.lib.utils.flowVisualizationUtils.StreamlinesShared.*;
import org.visnow.vn.lib.utils.interpolation.FieldPosition;


/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 * 
 * Revisions above 564 modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class ComputeIrregularFieldStreamlines extends StreamlinesGeometryCore
{
    private static final Logger LOGGER = Logger.getLogger(ComputeIrregularFieldStreamlines.class);
    
    private final IrregularField inField;
    private FieldPosition[][] trajectoryPositions;
    
    public ComputeIrregularFieldStreamlines(IrregularField inField, float baseScale, 
                                            Parameters parameters, 
                                            StreamlinePresentationParams presentationParams, 
                                            Field startField, ProgressAgent progressAgent)
    {
        super(inField, baseScale, parameters, presentationParams, startField, progressAgent);
        this.inField = inField;
        if (inField.getGeoTree() == null) {
            LOGGER.info("creating cell tree");
            long start = System.currentTimeMillis();
            inField.createGeoTree();
            LOGGER.info("cell tree created in " + ((float) (System.currentTimeMillis() - start)) / 1000 + "seconds");
        }
        this.progressAgent = progressAgent;
    }
    
    public ComputeIrregularFieldStreamlines(IrregularField inField, float baseScale, 
                                            Parameters parameters, 
                                            StreamlinePresentationParams presentationParams, 
                                            Field startField)
    {
        this(inField, baseScale, parameters, presentationParams, startField, null);
    }

    @Override
    public void setStartPoints()
    {   
        if (startField == null)
            return;
        float[] tmpStartCoords;
        if (startField instanceof RegularField && !startField.hasCoords())
            tmpStartCoords = ((RegularField)startField).getCoordsFromAffine().getData();
        else
            startCoords = tmpStartCoords = startField.getCoords(0).getData();
        nTrajects = nSrc = (int) startField.getNNodes();
        if (inField.getTrueNSpace() == 3) 
            return;
        int k = inField.getTrueNSpace();
        startCoords = new float[inField.getTrueNSpace() * nSrc];
        for (int i = 0; i < nSrc; i++)
            System.arraycopy(tmpStartCoords, 3 * i, startCoords, k * i, k);
    }
    
    @Override
    public synchronized void updateStreamlines()
    {
        super.updateStreamlines();
        setStartPoints();
        trajectoryPositions = new FieldPosition[nSrc][nSteps];
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new Streamline(iThread, nThreads));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        nTrajects = 0;
        for (int i = 0; i < nSrc; i++) 
            if (stepXt[i][1] - stepXt[i][0] > 2)
                nTrajects += 1;
        if (nTrajects == 0)
            return;
        nvert = nTrajects * nSteps;
        lines = new int[2 * nTrajects * (nSteps - 1)];
        float[] crds = new float[3 * nvert];
        vectors = new float[trueDim * nvert];
        Arrays.fill(crds, 0);
        indices = new int[nvert];
        FieldPosition[] nodes = new FieldPosition[nvert];
        for (int i = 0, l = 0; i < nSrc; i++) 
            if (stepXt[i][1] - stepXt[i][0] > 2) {
                int j = l;
                for (int k = 0; k < nSteps; k++, j += nTrajects) {
                    nodes[j] = trajectoryPositions[i][k];
                    System.arraycopy(rawCoords[i], trueDim * k, crds, 3 * j, trueDim);
                    indices[j] = k;
                }
                l += 1;
            }
        coords = new FloatLargeArray(crds);

        rawCoords = null;
        for (int i = 0, j = 0; i < nTrajects; i++)
            for (int k = 0; k < nSteps - 1; k++, j += 2) {
                lines[j] = i + k * nTrajects;
                lines[j + 1] = lines[j] + nTrajects;
            }
        byte[] edgeOrientations = new byte[lines.length / 2];
        for (int i = 0; i < edgeOrientations.length; i++)
            edgeOrientations[i] = 1;
        outField = new IrregularField(nvert);
        outField.setCurrentCoords(coords);
        DataArray da = DataArray.create(indices, 1, "steps");
        da.recomputeStatistics(true);
        outField.addComponent(da);
        CellArray streamLines = new CellArray(CellType.SEGMENT, lines, edgeOrientations, null);
        CellSet cellSet = new CellSet(inField.getName() + " " + inField.getComponent(parameters.get(COMPONENT)).getName() + " streamlines");
        cellSet.setBoundaryCellArray(streamLines);
        cellSet.setCellArray(streamLines);
        outField.addCellSet(cellSet);
        for (DataArray inDA : inField.getComponents())
            outField.addComponent(FieldPosition.interpolate(nodes, inDA));
    }

    private class Streamline implements Runnable
    {
        private final int iThread;
        private final int nThreads;
        private final SimplexPosition interp = new SimplexPosition(new int[4], new float[4], null);
        private final IrregularVectInterpolate vInt = new IrregularVectInterpolate(inField, interp);

        public Streamline(int iThread, int nThreads)
        {
            this.iThread = iThread;
            this.nThreads = nThreads;
        }
        
        @Override
        public void run()
        {
            float[] x0 = new float[trueDim];
            for (int n = iThread; n < nSrc; n += nThreads) {
                progressAgent.increase();
                if (mask != null && mask[n] == 0) {
                    for (int i = 0; i < rawCoords[n].length; i += trueDim) 
                        System.arraycopy(startCoords, trueDim * n,   rawCoords[n], i, trueDim);
                    continue;
                }
                try {
                    System.arraycopy(startCoords, trueDim * n, x0, 0, x0.length);
                        stepXt[n] =  RungeKutta.fourthOrderRK(vInt, x0, effectiveScale, nBackward, 
                                                              rawCoords[n],   trajectoryPositions[n]);
                } catch (Exception e) {
                }
            }
        }
    }
    
    private class IrregularVectInterpolate extends Deriv
    {
        private final IrregularField fld;
        private final SimplexPosition interp;

        public IrregularVectInterpolate(IrregularField fld, SimplexPosition interp)
        {
            this.fld = fld;
            this.interp = interp;
        }

        @Override
        public float[] derivn(float[] y) throws Exception
        {
            if (fld.getFieldCoords(y, interp)) {
                float[] q = new float[]{0, 0, 0};
                for (int i = 0; i < vlen + 1; i++) {
                    int k = vlen * interp.getVertices()[i];
                    for (int j = 0; j < vlen; j++)
                        q[j] += interp.getCoords()[i] * vects.get(k + j);
                }
                return q;
            } else
                return null;
        }

        public FieldPosition getPosition(int n)
        {
            return new FieldPosition(n, n, interp.getVertices(), interp.getCoords());
        }
    }
    
    private class InterpolateToStreamline implements Runnable
    {
        private final int iThread;
        private final SimplexPosition position = new SimplexPosition(new int[4], new float[4], null);
        private int nsrc;
        private final int trajectoryLength;
        private final float[] crds;
        private final int nInterpolated;
        private final FloatLargeArray[] inData;
        private final int[] vLens;
        private final float[][] outData;

        public InterpolateToStreamline(int iThread, 
                                       int trajectoryLength, 
                                       float[] crds, 
                                       int nInterpolated, 
                                       FloatLargeArray[] inData, int[] vLens, float[][] outData)
        {
            this.iThread = iThread;
            this.trajectoryLength = trajectoryLength;
            this.crds = crds;
            this.nInterpolated = nInterpolated;
            this.inData = inData;
            this.vLens = vLens;
            this.outData = outData;
        }

        @Override
        public void run()
        {
            int nSp = inField.getTrueNSpace();
            int dk = nSrc / nThreads;
            int kstart = iThread * dk + min(iThread, nSrc % nThreads);
            int kend = (iThread + 1) * dk + min(iThread + 1, nSrc % nThreads);
            float[] y = new float[3];
            for (int n = kstart; n < kend; n++) {
                for (int iNode = 0; iNode < trajectoryLength; iNode++) {
                    int m = iNode * nSrc + n;
                    System.arraycopy(crds, 3 * m, y, 0, 3);
                    if (inField.getFieldCoords(y, position)) {
                        for (int interp = 0; interp < nInterpolated; interp++) {
                            int vlen = vLens[interp];
                            float[] q = new float[vlen];
                            for (int i = 0; i < nSp + 1; i++) {
                                int k = vlen * position.getVertices()[i];
                                for (int j = 0; j < vlen; j++)
                                    q[j] += position.getCoords()[i] * inData[interp].get(k + j);
                            }
                            System.arraycopy(q, 0, outData[interp], m * vlen, vlen);
                        }
                    }
                }
            }
        }
    }

}
