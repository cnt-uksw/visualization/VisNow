/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.flowVisualizationUtils;

import org.visnow.vn.engine.core.ParameterName;

import org.visnow.vn.gui.widgets.RunButton;

/**
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * Warsaw University, Interdisciplinary Centre for Mathematical and Computational Modelling
 */

public class StreamlinesShared
{
    
    public static final int NO_ANIMATION     = 0;
    public static final int STREAM_ANIMATION = 1;
    public static final int GLYPH_ANIMATION  = 2;
    // Specifications for parameters:
    
    // Value from META_VECTOR_COMPONENT_NAMES
    public static final ParameterName<String> COMPONENT = new ParameterName<>("Component (vector)");
    // ???
    public static final ParameterName<Float> STEP = new ParameterName<>("Step");
    // Non-negative integer.
    public static final ParameterName<Integer> NUM_STEPS_FORWARD = new ParameterName<>("Steps forward");
    // Non-negative integer.
    public static final ParameterName<Integer> NUM_STEPS_BACKWARD = new ParameterName<>("Steps backward");
    public static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");
    public static final ParameterName<Integer> ANIMATION_TYPE = new ParameterName<>("animation type");
    public static final ParameterName<Boolean> COMPUTE_PERIODICITY = new ParameterName<>("compute periodicity index");
    public static final ParameterName<Boolean> SETTING_SEED_POINTS = new ParameterName<>("setting seed points");

    // Meta-parameters:
    
    public static final ParameterName<Boolean> META_IS_START_POINT_FIELD_REGULAR = new ParameterName<>("(META) Is input field regular?");
    //not-empty array of unique, non-empty strings
    public static final ParameterName<String[]> META_VECTOR_COMPONENT_NAMES = new ParameterName<>("(META) Vector component names");
    public static final ParameterName<float[]> META_PREFERRED_STEPS = new ParameterName<>("(META) Preferred steps");
}
