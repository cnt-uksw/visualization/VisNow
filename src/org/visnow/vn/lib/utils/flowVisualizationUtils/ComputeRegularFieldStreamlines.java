/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.flowVisualizationUtils;

import java.util.Arrays;
import org.visnow.jscic.RegularField;
import org.visnow.vn.lib.utils.numeric.ODE.Deriv;
import org.visnow.vn.lib.utils.numeric.ODE.RungeKutta;
import org.visnow.jlargearrays.FloatLargeArray;
import static org.apache.commons.math3.util.FastMath.*;
import org.apache.log4j.Logger;
import org.visnow.jscic.Field;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.ObjectDataArray;

import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import static org.visnow.vn.lib.utils.flowVisualizationUtils.StreamlinesShared.COMPONENT;
import org.visnow.vn.lib.utils.interpolation.FieldPosition;
import org.visnow.vn.lib.utils.interpolation.SubsetGeometryComponents;
import static org.visnow.vn.lib.utils.interpolation.SubsetGeometryComponents.NODE_POSITIONS;


/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * 
 * Revisions above 564 modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class ComputeRegularFieldStreamlines extends StreamlinesGeometryCore
{
    protected static final Logger LOGGER = Logger.getLogger(ComputeRegularFieldStreamlines.class);
    
    protected RegularField inField = null;
    protected int[] dims = null;
    protected float[][] affine = null;
    protected float[][] invAffine = null;
    protected FloatLargeArray fldCoords = null;
    protected FloatLargeArray pullVects = null;
    protected float indexRadius = 1;
/**
     * Creates a new instance of ComputeRegularFieldStreamlines
     * <p>
     * @param inField field containing a vector component to be mapped with streamlines
     * @param baseScale field extent divided by sqr average of vector norms for RK step calculation
     * @param pullVects vector field pulled back to index coordinates
     * @param parameters  streamlines module parameters
     * @param presentationParams streamline presentation (line width, halo)
     * @param startField reference to seed points field
     */
    public ComputeRegularFieldStreamlines(RegularField inField, float baseScale, 
                                          FloatLargeArray pullVects, 
                                          Parameters parameters, 
                                          StreamlinePresentationParams presentationParams, 
                                          Field startField, ProgressAgent progressAgent)
    {
        super(inField, baseScale, parameters, presentationParams, startField, progressAgent);
        this.inField = inField;
        this.pullVects = pullVects;
        dims = inField.getDims();
        indexRadius = 0;
        for (int i = 0; i < dims.length; i++)
            indexRadius += dims[i] * dims[i];
        indexRadius = (float) sqrt(indexRadius);
        affine = inField.getAffine();
        fldCoords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords();
        if (fldCoords == null)
            invAffine = inField.getInvAffine();
        else {
            invAffine = new float[4][3];
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 4; j++)
                    invAffine[j][i] = 0;
                invAffine[i][i] = 1;
            }
        }
        nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
    }
    
    public ComputeRegularFieldStreamlines(RegularField inField, float baseScale, 
                                          FloatLargeArray pullVects, 
                                          Parameters parameters, 
                                          StreamlinePresentationParams presentationParams, 
                                          Field startField)
    {
        this(inField, baseScale, pullVects, parameters, presentationParams, startField, null);
    }
    
    @Override
    public final void setStartPoints()
    {   
        int trueNSpace = inField.getTrueNSpace();
        float[] tmpStartCoords;
        if (startField == null)
            return;
        nSrc = (int)startField.getNNodes();
        if (startField.getComponent(SubsetGeometryComponents.INDEX_COORDS) != null) {
            DataArray startCoordsCmp = startField.getComponent(SubsetGeometryComponents.INDEX_COORDS);
            startCoords = startCoordsCmp.getRawArray(0).getFloatData();
            return;
        }
        if (startField.getComponent(NODE_POSITIONS) != null) {
            ObjectDataArray pos = (ObjectDataArray)startField.getComponent(NODE_POSITIONS);
            FieldPosition[] positions = (FieldPosition[])pos.getRawArray(0).getData();
            startCoords = FieldPosition.interpolateToIndices(positions, inField.getDims());
            return;
        }
        
        if (startField instanceof RegularField && !startField.hasCoords())
            tmpStartCoords = ((RegularField)startField).getCoordsFromAffine().getData();
        else
            tmpStartCoords = startField.getCoords(0).getData();
        nTrajects = nSrc = (int) startField.getNNodes();
        int k = inField.getTrueNSpace();
        startCoords = new float[trueNSpace * nSrc];
        if (inField.hasCoords()) {
            if (trueNSpace == 2) 
                for (int i = 0; i < nSrc; i++) {
                    float[] v = inField.getFloatIndices(tmpStartCoords[2 * i], 
                                                        tmpStartCoords[2 * i + 1]);
                    System.arraycopy(v, 0, startCoords, 2 * i, 2);
                }
            if (trueNSpace == 3) 
                for (int i = 0; i < nSrc; i++) {
                    float[] v = inField.getFloatIndices(tmpStartCoords[3 * i], 
                                                        tmpStartCoords[3 * i + 1], 
                                                        tmpStartCoords[3 * i + 2]);
                    System.arraycopy(v, 0, startCoords, 3 * i, 3);
                }
        }
        else {
            for (int i = 0; i < nSrc; i++) {
                float[] crd = new float[trueNSpace];
                for (int j = 0; j < trueNSpace; j++)
                    crd[j] = tmpStartCoords[3 * i + j] - affine[3][j];
                float[] v = new float[trueNSpace];
                Arrays.fill(v, 0);
                for (int j = 0; j < trueNSpace; j++)
                    for (k = 0; k < trueNSpace; k++) 
                        v[j] += invAffine[j][k] * crd[k];
                System.arraycopy(v, 0, startCoords, i * trueNSpace, trueNSpace);
            }
        }
    }
    
    @Override
    public synchronized void updateStreamlines()
    {
        super.updateStreamlines();
        setStartPoints();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new Streamline(iThread));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        nTrajects = nSrc;
        outField = InterpolationToRegularFieldStreamline.
                createOutputStreamlines(inField, parameters.get(COMPONENT),
                                        nSrc, nSteps, rawCoords);
    }

    protected class Streamline implements Runnable
    {
        protected final int nThread;
        protected final RegularVectInterpolate vInt = new RegularVectInterpolate();

        public Streamline(int nThread)
        {
            this.nThread = nThread;
        }

        @Override
        public void run()
        {
            float[] x0 = new float[trueDim];
            for (int n = nThread; n < nSrc; n += nThreads) {
                progressAgent.increase();
                if (mask != null && mask[n] == 0) {
                    for (int i = 0; i < rawCoords[n].length; i += trueDim) 
                        System.arraycopy(startCoords, trueDim * n,   rawCoords[n], i, trueDim);
                    continue;
                }
                try {
                    System.arraycopy(startCoords, trueDim * n, x0, 0, x0.length);
                    stepXt[n] = RungeKutta.fourthOrderRK(vInt, x0, effectiveScale, nBackward, 
                                                              rawCoords[n]);

                } catch (Exception e) {
                }
            }
        }
    }

    protected class RegularVectInterpolate extends Deriv
    {
        @Override
        public float[] derivn(float[] y) throws Exception
        {
            for (int i = 0; i < dims.length; i++) 
                if (y[i] < 0 || y[i] > dims[i] - 1)
                    return null;
            int vlen = dims.length;
            float[] c = new float[vlen];
            int inexact = 0;
            float u, v, w;
            int i, j, k, m, n0, n1;
            switch (vlen) {
            case 3:
                u = y[0]; v = y[1]; w = y[2];
                i = (int) u;
                u -= i;
                if (u != 0) 
                    inexact += 1;
                j = (int) v;
                v -= j;
                if (v != 0) 
                    inexact += 2;
                k = (int) w;
                w -= k;
                if (w != 0) 
                    inexact += 4;
                m  = vlen * ((dims[1] * k + j) * dims[0] + i);
                n0 = vlen *   dims[0];
                n1 = vlen *   dims[0] * dims[1];
                switch (inexact) {
                case 0:
                    for (int l = 0; l < vlen; l++) 
                        c[l] = pullVects.get(m + l);
                    break;
                case 1:
                    for (int l = 0; l < vlen; l++) 
                        c[l] = u * pullVects.get(m + l + vlen) + (1 - u) * pullVects.get(m + l);
                    break;
                case 2:
                    for (int l = 0; l < vlen; l++) 
                        c[l] = v * pullVects.get(m + l + n0) + (1 - v) * pullVects.get(m + l);
                    break;
                case 3:
                    for (int l = 0; l < vlen; l++) 
                        c[l] = v * (u * pullVects.get(m + l + n0 + vlen) + (1 - u) * pullVects.get(m + l + n0)) +
                            (1 - v) * (u * pullVects.get(m + l + vlen) + (1 - u) * pullVects.get(m + l));
                    break;
                case 4:
                    for (int l = 0; l < vlen; l++) 
                        c[l] = w * pullVects.get(m + l + n1) + (1 - w) * pullVects.get(m + l);
                    break;
                case 5:
                    for (int l = 0; l < vlen; l++) 
                        c[l] = w * (u * pullVects.get(m + l + n1 + vlen) + (1 - u) * pullVects.get(m + l + n1)) +
                            (1 - w) * (u * pullVects.get(m + l + vlen) + (1 - u) * pullVects.get(m + l));
                    break;
                case 6:
                    for (int l = 0; l < vlen; l++) 
                        c[l] = w * (v * pullVects.get(m + l + n1 + n0) + (1 - v) * pullVects.get(m + l + n1)) +
                            (1 - w) * (v * pullVects.get(m + l + n0) + (1 - v) * pullVects.get(m + l));
                    break;
                case 7:
                    for (int l = 0; l < vlen; l++) 
                        c[l] = w * (v * (u * pullVects.get(m + l + n1 + n0 + vlen) + (1 - u) * pullVects.get(m + l + n1 + n0)) +
                            (1 - v) * (u * pullVects.get(m + l + n1 + vlen) + (1 - u) * pullVects.get(m + l + n1))) +
                            (1 - w) * (v * (u * pullVects.get(m + l + n0 + vlen) + (1 - u) * pullVects.get(m + l + n0)) +
                            (1 - v) * (u * pullVects.get(m + l + vlen) + (1 - u) * pullVects.get(m + l)));
                    break;
                }
                break;
            case 2:
                u = y[0]; v = y[1];
                i = (int) u;
                u -= i;
                if (u != 0) 
                    inexact += 1;
                j = (int) v;
                v -= j;
                if (v != 0) 
                    inexact += 2;
                m =  vlen * (j * dims[0] + i);
                n0 = vlen * dims[0];
                switch (inexact) {
                case 0:
                    for (int l = 0; l < vlen; l++) 
                        c[l] = pullVects.get(m + l);
                    break;
                case 1:
                    for (int l = 0; l < vlen; l++) 
                        c[l] = u * pullVects.get(m + l + vlen) + (1 - u) * pullVects.get(m + l);
                    break;
                case 2:
                    for (int l = 0; l < vlen; l++) 
                        c[l] = v * pullVects.get(m + l + n0) + (1 - v) * pullVects.get(m + l);
                    break;
                case 3:
                    for (int l = 0; l < vlen; l++) 
                        c[l] = v * (u * pullVects.get(m + l + n0 + vlen) + (1 - u) * pullVects.get(m + l + n0)) +
                            (1 - v) * (u * pullVects.get(m + l + vlen) + (1 - u) * pullVects.get(m + l));
                    break;
                }
                break;
            }
            return c;
        }
    }
}

