/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.flowVisualizationUtils;

import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.flowVisualizationUtils.AnimatedStreamParams.ANIMATION;

public class AnimatedStreamGUI extends javax.swing.JPanel
{

    protected AnimatedStreamParams params = null;
    private int nFrames = 1;
    private int segmentLength = 1;

    /**
     * Set the value of dims
     *
     * @param dims new value of dims
     */
    /**
     * Creates new form EmptyVisnowModuleGUI
     */
    public AnimatedStreamGUI()
    {
        initComponents();
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        segmentCountSlider = new javax.swing.JSlider();
        gapSlider = new javax.swing.JSlider();
        delaySlider = new javax.swing.JSlider();
        badInLabel = new javax.swing.JLabel();
        lineWidthSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        jPanel1 = new javax.swing.JPanel();
        backToggle = new javax.swing.JToggleButton();
        stopToggle = new javax.swing.JToggleButton();
        fwdToggle = new javax.swing.JToggleButton();
        jPanel2 = new javax.swing.JPanel();

        setLayout(new java.awt.GridBagLayout());

        segmentCountSlider.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        segmentCountSlider.setMajorTickSpacing(5);
        segmentCountSlider.setMaximum(50);
        segmentCountSlider.setMinimum(5);
        segmentCountSlider.setMinorTickSpacing(1);
        segmentCountSlider.setPaintLabels(true);
        segmentCountSlider.setPaintTicks(true);
        segmentCountSlider.setValue(20);
        segmentCountSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "segment count", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
        segmentCountSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                segmentCountSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(segmentCountSlider, gridBagConstraints);

        gapSlider.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        gapSlider.setMajorTickSpacing(50);
        gapSlider.setMinorTickSpacing(5);
        gapSlider.setPaintLabels(true);
        gapSlider.setPaintTicks(true);
        gapSlider.setValue(1);
        gapSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, " gap between segments ", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
        gapSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                gapSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 0);
        add(gapSlider, gridBagConstraints);

        delaySlider.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        delaySlider.setMajorTickSpacing(10);
        delaySlider.setMaximum(50);
        delaySlider.setMinorTickSpacing(1);
        delaySlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "animation speed", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
        delaySlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                delaySliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(delaySlider, gridBagConstraints);

        badInLabel.setForeground(new java.awt.Color(153, 0, 51));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(badInLabel, gridBagConstraints);

        lineWidthSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        lineWidthSlider.setMax(15);
        lineWidthSlider.setMin(0.1);
        lineWidthSlider.setScaleType(org.visnow.vn.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        lineWidthSlider.setValue(0.5);
        lineWidthSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "line width", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
        lineWidthSlider.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                lineWidthSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        add(lineWidthSlider, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridLayout(1, 0));

        buttonGroup1.add(backToggle);
        backToggle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/visnow/vn/gui/icons/bck.png"))); // NOI18N
        backToggle.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/org/visnow/vn/gui/icons/selbck.png"))); // NOI18N
        backToggle.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                backToggleActionPerformed(evt);
            }
        });
        jPanel1.add(backToggle);

        buttonGroup1.add(stopToggle);
        stopToggle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/visnow/vn/gui/icons/stop.png"))); // NOI18N
        stopToggle.setSelected(true);
        stopToggle.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/org/visnow/vn/gui/icons/stopact.png"))); // NOI18N
        stopToggle.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                stopToggleActionPerformed(evt);
            }
        });
        jPanel1.add(stopToggle);

        buttonGroup1.add(fwdToggle);
        fwdToggle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/visnow/vn/gui/icons/fwd.png"))); // NOI18N
        fwdToggle.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/org/visnow/vn/gui/icons/selfwd.png"))); // NOI18N
        fwdToggle.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                fwdToggleActionPerformed(evt);
            }
        });
        jPanel1.add(fwdToggle);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(jPanel1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        add(jPanel2, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Set the value of params
     *
     * @param params new value of params
     */
    public void setParams(AnimatedStreamParams params)
    {
        this.params = params;
    }
    

    public void setNFrames(int nFr)
    {
        this.nFrames = nFr;
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                boolean hasFrames = nFrames > 1;
                badInLabel.setEnabled(hasFrames);
                delaySlider.setEnabled(hasFrames);
                gapSlider.setEnabled(hasFrames);
                segmentCountSlider.setEnabled(hasFrames);
                segmentLength = Math.max(5, nFrames / 100);
                segmentCountSlider.setMaximum(nFrames / 10);
                segmentCountSlider.setValue(segmentLength);
            }
        });
    }

    public void setInText(String s)
    {
        badInLabel.setText(s);
    }

    private void segmentCountSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_segmentCountSliderStateChanged
    {//GEN-HEADEREND:event_segmentCountSliderStateChanged
        if (!segmentCountSlider.getValueIsAdjusting()) 
            params.setSegmentCount(segmentCountSlider.getValue());
    }//GEN-LAST:event_segmentCountSliderStateChanged

    private void gapSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_gapSliderStateChanged
    {//GEN-HEADEREND:event_gapSliderStateChanged
        if (!gapSlider.getValueIsAdjusting()) 
            params.setGap(gapSlider.getValue());
    }//GEN-LAST:event_gapSliderStateChanged

    private void delaySliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_delaySliderStateChanged
    {//GEN-HEADEREND:event_delaySliderStateChanged
        params.setDelay(50 - delaySlider.getValue());
    }//GEN-LAST:event_delaySliderStateChanged

    private void lineWidthSliderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_lineWidthSliderUserChangeAction
        float lineThickness = (Float)lineWidthSlider.getValue();
        if (params.getLineWidth() != lineThickness)
            params.setLineWidth(lineThickness);
    }//GEN-LAST:event_lineWidthSliderUserChangeAction

    private void backToggleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backToggleActionPerformed
        runCtrl();
    }//GEN-LAST:event_backToggleActionPerformed

    private void stopToggleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopToggleActionPerformed
        runCtrl();
    }//GEN-LAST:event_stopToggleActionPerformed

    private void fwdToggleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fwdToggleActionPerformed
        runCtrl();
    }//GEN-LAST:event_fwdToggleActionPerformed

    public void setStopAnimation()
    {
        stopToggle.setSelected(true);
        if (params != null)
            params.setAnimate(ANIMATION.STOP);
    }
    
    private void runCtrl()
    {
        params.setAnimate(backToggle.isSelected() ? ANIMATION.BACK : 
                          fwdToggle.isSelected() ?  ANIMATION.FORWARD :
                                                    ANIMATION.STOP);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton backToggle;
    private javax.swing.JLabel badInLabel;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JSlider delaySlider;
    private javax.swing.JToggleButton fwdToggle;
    private javax.swing.JSlider gapSlider;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private org.visnow.vn.gui.widgets.ExtendedSlider lineWidthSlider;
    private javax.swing.JSlider segmentCountSlider;
    private javax.swing.JToggleButton stopToggle;
    // End of variables declaration//GEN-END:variables
}
