/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.flowVisualizationUtils;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.apache.commons.lang3.ArrayUtils;
import org.visnow.jlargearrays.ByteLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.ParameterChangeListener;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams.*;
import org.visnow.vn.geometries.objects.CroppedRegularFieldOutline;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.parameters.PresentationParams;
import org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;
import org.visnow.vn.lib.gui.FieldBasedUI.SubsetGUI.SubsetParams;
import org.visnow.vn.lib.utils.field.IndexSlice1D;
import org.visnow.vn.lib.utils.field.IndexSlice2D;
import org.visnow.vn.lib.utils.field.IndexSliceCodim0;
import org.visnow.vn.lib.utils.field.subset.FieldSample;
import static org.visnow.vn.lib.utils.flowVisualizationUtils.SeedPointParams.SeedPointsSelection.*;
import static org.visnow.vn.lib.utils.interpolation.SubsetGeometryComponents.*;
import org.visnow.vn.lib.utils.lineProbe.BasicLineProbe;
import org.visnow.vn.lib.utils.pointProbe.GeometricPointProbe;
import org.visnow.vn.lib.utils.slicing.BasicPlanarSlice;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */

public class SeedPoints 
{
    /**
     * Parameters indicating choice of seed field, seed subset (slice) and downsize
     */
    protected SeedPointParams params;
    protected SubsetParams subsetParams;
    protected IndexSliceParams indexSliceParams;
    /**
     * main input field (i.e. field containing streamline vector components) 
     */
    protected Field inField;
    /**
     * auxiliary input field containing streamline start points
     */
    protected Field auxField;
    /**
     * field selected by inField/auxField UI selector widget
     */
    protected Field currentRawSeedField;
    /**
     * currentRawSeedField interpreted as regular field
     */
    protected RegularField regularCurrentRawSeedField;
    protected int[] currentRawSeedDims;
    /**
     * currentRawSeedField interpreted as irregular field
     */
    protected IrregularField irregularCurrentRawSeedField;
    /**
     * field selected by slice/probe/whole UI selector widget
     */
    protected Field unsampledSeedField;
    protected long nNodes;
    /**
     *  unsampledSeedField interpreted as regular field
     */
    protected RegularField regularUnsampledSeedField;
    protected int[] unsampledSeedDims;
    /**
     * unsampledSeedField interpreted as irregular field
     */
    protected IrregularField irregularUnsampledSeedField;
    /**
     * final subset of seed field after random or regular sampling
     */
    protected Field seedField;
    protected int nSeedNodes;
    /**
     * regularly sampled (downsized) seedField 
     */
    protected RegularField regularSeedField;
    protected int[] seedDims;
    /**
     * irregularly sampled (downsized) seedField 
     */
    protected IrregularField irregularSeedField;
    /**
     * necessary for interactive glyph presentation
     */
    protected PresentationParams presentationParams = null;
    protected DataMappingParams dataMappingParams;
    protected SeedPointsGUI gui;
    protected BasicPlanarSlice simplePlanarSlice;
    protected BasicLineProbe simpleLineProbe;
    protected GeometricPointProbe simplePointProbe;
    protected long[] seedIndices;
    protected float[] currentSeedCoords;
    protected float[] rawSeedCoords;
    protected FloatLargeArray seedCoords;
    protected float[] fieldSeedCoords;
    protected boolean probeActive = true;
    protected final CroppedRegularFieldOutline outline = new CroppedRegularFieldOutline();
    
    protected OpenBranchGroup seedPointsGroup = new OpenBranchGroup();
    protected boolean indexGlyphVisible =  true;
    
    public SeedPoints(SeedPointParams params, PresentationParams presentationParams)
    {
        this.params = params;
        this.presentationParams = presentationParams;
        dataMappingParams = presentationParams.getDataMappingParams();
        subsetParams = params.getSubsetParams();
        subsetParams.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (unsampledSeedField == null)
                    updateSelection();
                
                if (params.getSelectionType() != POINT) {
                    if (subsetParams.isRandom() || unsampledSeedField instanceof IrregularField)
                        seedField = FieldSample.randomSample(unsampledSeedField, subsetParams.getSubsetSize(), true);
                    else
                        seedField = FieldSample.regularSample((RegularField)unsampledSeedField, 
                                                              subsetParams.getDown(),
                                                              subsetParams.isCentered(), true);
                }
                fireStateChanged();
            }
        });
        indexSliceParams = params.getIndexSliceParams();
        indexSliceParams.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                if (currentRawSeedField instanceof RegularField) {
                    outline.setPosition(indexSliceParams.getPosition(), 
                                        indexSliceParams.getFixed(),
                                        indexSliceParams.getLow(),
                                        indexSliceParams.getUp());
                    if (indexSliceParams.isEssentialChange())
                        updateRegularSelection();
                    if (indexSliceParams.isGlyphVisible() && !indexGlyphVisible)
                    {
                        switch (params.getSelectionType()) {
                        case POINT:
                            if (inField instanceof RegularField)
                                seedPointsGroup.addChild(outline);
                            break;
                        case LINE_SLICE:
                        case PLANE_SLICE:
                            seedPointsGroup.addChild(outline);
                            break;
                        case WHOLE:
                            if (currentRawSeedField instanceof RegularField)
                                seedPointsGroup.addChild(outline);
                            break;
                        }
                        indexGlyphVisible = true;
                    }
                    else if (!indexSliceParams.isGlyphVisible()) {
                        seedPointsGroup.removeAllChildren();
                        indexGlyphVisible = false;
                    }
                    if (indexSliceParams.isEssentialChange()) {
                        if (params.getSelectionType() != POINT)
                            sampleSeedField();
                        else
                            fireStateChanged();
                    }
                }
            }
        });
        simplePlanarSlice = new BasicPlanarSlice(presentationParams, true);
        simplePlanarSlice.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            { 
                if (params.getSelectionType() == PLANAR_SLICE && simplePlanarSlice != null && 
                    simplePlanarSlice.getSliceAreaField() != null) {
                    IrregularField tmpField = simplePlanarSlice.getSliceAreaField();
                    unsampledSeedField = irregularUnsampledSeedField = tmpField.cloneShallow();
                    unsampledSeedField.removeComponents();
                    addIndexCoordsToUnsampledSeedField(tmpField);
                    sampleSeedField();
                }
            }
        });
        simpleLineProbe = new BasicLineProbe();
        simpleLineProbe.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            { 
                if (params.getSelectionType() == LINEAR_SLICE && simpleLineProbe != null && 
                    simpleLineProbe.getSliceField() != null)  {
                    IrregularField tmpField = simpleLineProbe.getSliceAreaField();
                    unsampledSeedField = irregularUnsampledSeedField = tmpField.cloneShallow();
                    unsampledSeedField.removeComponents();
                    addIndexCoordsToUnsampledSeedField(tmpField);
                    sampleSeedField();
                }
            }
        });
        simplePointProbe = new GeometricPointProbe();
        simplePointProbe.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                if (params.getSelectionType() == POINT && simplePointProbe.getSliceField() != null) {
                    seedField = irregularSeedField = simplePointProbe.getSliceField();
                    nNodes = 1;
                    fireStateChanged();
                }
            }
        });
        
        params.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (currentRawSeedField == null)
                    return;
                switch (name) {
                case SeedPointParams.TYPE:
                    seedPointsGroup.removeAllChildren();
                    switch (params.getSelectionType()) {
                        case POINT:
                            if (inField instanceof RegularField) {
                                seedPointsGroup.addChild(outline);
                                indexGlyphVisible = true;
                            }
                            else
                                seedPointsGroup.addChild(simplePointProbe.getGlyph());
                            break;
                        case PLANAR_SLICE:
                            seedPointsGroup.addChild(simplePlanarSlice.getGlyph());
                            break;
                        case LINEAR_SLICE:
                            seedPointsGroup.addChild(simpleLineProbe.getGlyph());
                            break;
                        case LINE_SLICE:
                        case PLANE_SLICE:
                            seedPointsGroup.addChild(outline);
                            indexGlyphVisible = true;
                            break;
                        case WHOLE:
                            if (currentRawSeedField instanceof RegularField) {
                                seedPointsGroup.addChild(outline);
                                indexGlyphVisible = true;
                            }
                            break;
                    }
                    updateSelection();
                    break;
                case SeedPointParams.COORDS:
                    updateSelection();
                    break;
                }
            }
        });
    }
    
    public void setGUI(SeedPointsGUI gui)
    {
        this.gui = gui;
        gui.getSubsetUI().setParams(subsetParams);
        int visibleWidgets = U_ROT_VIS   | V_ROT_VIS   | W_ROT_VIS |
                             U_TRANS_VIS | V_TRANS_VIS | W_TRANS_VIS |
                             U_RANGE_VIS | V_RANGE_VIS | 
                             SCALE_VIS   | AXES_VIS;
        simplePlanarSlice.getGlyph().getParams().setVisibleWidgets(visibleWidgets);
        simplePlanarSlice.getGlyphGUI().updateWidgetVisibility();
        gui.setPlanarSliceGUI(simplePlanarSlice.getGlyphGUI());
        gui.setLinearSliceGUI(simpleLineProbe.getGlyphGUI());
        gui.setPointProbeGUI(simplePointProbe.getGlyphGUI());
    }
    
    public void setInField(Field inField)
    {
        this.inField = inField;
        setCurrentRawSeedField(inField);
        params.setActiveValue(false);
        params.setInputField(inField);
        params.setActiveValue(true);
    }

    public void setAuxField(Field auxField)
    {
        this.auxField = auxField;
    }
    
    private void setCurrentRawSeedField(Field in)
    {
        currentRawSeedField = in;
        if (currentRawSeedField instanceof RegularField) {
            regularCurrentRawSeedField   = (RegularField)currentRawSeedField;
            irregularCurrentRawSeedField = null;
            currentRawSeedDims = regularCurrentRawSeedField.getDims();
            indexSliceParams.setFieldSchema(regularCurrentRawSeedField.getSchema());
            outline.setField(regularCurrentRawSeedField);
        }
        else {
            regularCurrentRawSeedField   = null;
            irregularCurrentRawSeedField = (IrregularField)currentRawSeedField;
            currentRawSeedDims = null;
        }
        probeActive = false;
        if (currentRawSeedField.getTrueNSpace() > 2)
            simplePlanarSlice.setInData(currentRawSeedField, dataMappingParams);
        simpleLineProbe.setInData(currentRawSeedField, dataMappingParams);
        simplePointProbe.setInData(currentRawSeedField, dataMappingParams);
        params.setSeedPointsType(WHOLE);
        if (gui != null) 
            gui.updateFromParams();
        probeActive = true;
    }
    
    private DataArray computeIndexCoords(RegularField regularTmpField)
    {
        int[] dims = regularTmpField.getDims();
        nNodes = regularTmpField.getNNodes();
        int nDims = dims.length;
        FloatLargeArray indexCoords= new FloatLargeArray(3 * nNodes, true);
        for (long i = 0; i < nNodes; i++) {
            long ii = i, l = 3 * i;
            for (int j = 0; j < nDims; j++, l++) {
                indexCoords.set(l, ii % dims[j]);
                ii /= dims[j];
            }
        }
        return DataArray.create(indexCoords, 3, INDEX_COORDS);
    }
    
    private void addIndexCoordsToUnsampledSeedField(IrregularField tmpField)
    {
        nNodes = unsampledSeedField.getNNodes();
        if (tmpField.getComponent(INDEX_COORDS) != null)
            unsampledSeedField.addComponent(tmpField.getComponent(INDEX_COORDS).cloneShallow());
        else if (inField instanceof RegularField && !((RegularField)inField).hasCoords()) {
            FloatLargeArray crds = unsampledSeedField.getCurrentCoords();
            float[][] affine     = ((RegularField)inField).getAffine();
            float[][] invAffine  = ((RegularField)inField).getInvAffine();
            FloatLargeArray indexCoords = new FloatLargeArray(3 * nNodes);
            float[] p = new float[3];
            for (long i = 0; i < nNodes; i++) {
                for (int j = 0; j < 3; j++) 
                    p[j] = crds.getFloat(3 * i + j) - affine[3][j];
                float[] q = new float[3];
                for (int j = 0; j < 3; j++)
                    for (int k = 0; k < 3; k++)
                        q[j] += invAffine[k][j] * p[k];
                LargeArrayUtils.arraycopy(q, 0, indexCoords, 3 * i, 3);
            }
            unsampledSeedField.addComponent(DataArray.create(indexCoords, 3, INDEX_COORDS));
        }
    }
    
    private IrregularField createPointSeedField(float[] position)
    {
        IrregularField pointSeedField = new IrregularField(1);
        pointSeedField.addComponent(DataArray.create(ArrayUtils.clone(position), 3, INDEX_COORDS));
        float[] coords = new float[3];
        if (!regularCurrentRawSeedField.hasCoords()) {
            System.arraycopy(regularCurrentRawSeedField.getAffine()[3], 0, coords, 0, 3);
            for (int i = 0; i < coords.length; i++) 
                for (int j = 0; j < regularCurrentRawSeedField.getDimNum(); j++) 
                    coords[i] += position[j] * regularCurrentRawSeedField.getAffine()[j][i];
        }
        else {
            int[] dims = regularCurrentRawSeedField.getDims();
            int k = (int)position[dims.length - 1];
            for (int i = regularCurrentRawSeedField.getDimNum() - 2; i >= 0; i--) 
            System.arraycopy(regularCurrentRawSeedField.getCurrentCoords().getData(), 3 * k, coords, 0, 3);
        }
        pointSeedField.setCurrentCoords(new FloatLargeArray(coords));
        CellArray pt = new CellArray(CellType.POINT, new int[]{0}, new byte[]{0}, null);
        CellSet cs = new CellSet("pt");
        cs.addCells(pt);
        pointSeedField.addCellSet(cs);
        return pointSeedField;
    }
    
    private void updateRegularSelection()
    {
        regularUnsampledSeedField   = null;
        irregularUnsampledSeedField = null;
        unsampledSeedDims = null;
        params.setActive(false);
        if (currentRawSeedField instanceof RegularField) {
            switch (params.getSelectionType()) {
            case PLANE_SLICE:
                unsampledSeedField = regularUnsampledSeedField = IndexSlice2D.slice(regularCurrentRawSeedField, indexSliceParams);
                break;
            case LINE_SLICE:
                unsampledSeedField = regularUnsampledSeedField = IndexSlice1D.slice(regularCurrentRawSeedField, indexSliceParams);
                break;
            case WHOLE:
                unsampledSeedField = regularUnsampledSeedField = IndexSliceCodim0.slice(regularCurrentRawSeedField, indexSliceParams);
                break;
            case POINT:
                seedField = irregularSeedField = createPointSeedField(indexSliceParams.getPosition());
                if (seedField.getComponent(INDEX_COORDS) == null)
                    seedField.addComponent(computeIndexCoords(regularUnsampledSeedField));
                DataArray indexCoords = seedField.getComponent(INDEX_COORDS);
                seedField.removeComponents();
                seedField.addComponent(indexCoords);
                nNodes = 1;
                params.setActive(true);
                return;
            }
        }
        params.setActive(true);
        if (unsampledSeedField.getComponent(INDEX_COORDS) == null)
            unsampledSeedField.addComponent(computeIndexCoords(regularUnsampledSeedField));
        DataArray indexCoords = unsampledSeedField.getComponent(INDEX_COORDS);
        unsampledSeedField.removeComponents();
        unsampledSeedField.addComponent(indexCoords);
        nNodes = unsampledSeedField.getNNodes();
    }
    
    private void updateIrregularSelection()
    {
        IrregularField tmpField = null;
        switch (params.getSelectionType()) {
        case PLANAR_SLICE:
            tmpField = simplePlanarSlice.getSliceAreaField();
            break;
        case LINEAR_SLICE:
            tmpField = simpleLineProbe.getSliceField();
            break;
        case WHOLE:
            tmpField = irregularCurrentRawSeedField;      
            break;
        }
        if (tmpField == null)
            return;
        unsampledSeedField = irregularUnsampledSeedField = tmpField.cloneShallow();
        unsampledSeedField.removeComponents();
        addIndexCoordsToUnsampledSeedField(tmpField);
    }
    
    private void sampleSeedField()
    {
        prepareSampling();
        if (subsetParams.isRandom() || unsampledSeedField instanceof IrregularField)
            seedField = FieldSample.randomSample(unsampledSeedField, subsetParams.getSubsetSize(), true);
//        else {
//            int dim = regularUnsampledSeedField.getDimNum();
//            int[] low = new int[3];
//            int[] up = new int[3];
//            switch (params.getSelectionType()) {
//            case LINE_SLICE:
//                low[0] = indexSliceParams.getExtents()[0][indexSliceParams.getAxis()];
//                up[0]  = indexSliceParams.getExtents()[1][indexSliceParams.getAxis()];
//                break;
//            case PLANE_SLICE:
//                for (int i = 0, j = 0; i < dim; i++) 
//                    if (i != indexSliceParams.getAxis()) {
//                        low[j] = indexSliceParams.getExtents()[0][i];
//                        up[j]  = indexSliceParams.getExtents()[1][i];
//                        j += 1;
//                    }
//                break;
//            case WHOLE:
//                System.arraycopy(indexSliceParams.getExtents()[0], 0, low, 0, dim);
//                System.arraycopy(indexSliceParams.getExtents()[1], 0, up, 0, dim);
//            }
//            seedField = FieldSample.regularSample(regularUnsampledSeedField, 
//                                                  subsetParams.getDown(), low, up, true);
//        }
        fireStateChanged();
    }
    
    /**
     * Creating intermediate field as a subset of the currentRawSeedField 
     * defined geometrically (as planar slice/line probe) 
     * or in index space as  generalized line/plane slice
     */
    private void updateSelection()
    {
        if (params.getSelectionType() == POINT) {
            fireStateChanged();
            return;
        }
        if (currentRawSeedField instanceof RegularField &&
           (params.getSelectionType() == PLANE_SLICE ||
            params.getSelectionType() == LINE_SLICE ||    
            params.getSelectionType() == WHOLE))
            updateRegularSelection();
        else 
            updateIrregularSelection();
        sampleSeedField();
    }
    
    private static ByteLargeArray sampleMask(ByteLargeArray mask, int sampleSize)
    {
        long nNodes = mask.length(); 
        if (sampleSize >= nNodes) {
            for (long i = 0; i < nNodes; i++)
                mask.setByte(i, (byte)1);
            return mask;
        }
        long done = 0;
        if (sampleSize < nNodes / 2) {
            for (long i = 0; i < nNodes; i++)
                mask.setByte(i, (byte)0);
            double rLimit = (double)sampleSize / nNodes;
            while (done < sampleSize)
                for (long i = 0; i < mask.length() && done < sampleSize; i++)
                    if (Math.random() < rLimit && mask.getByte(i) == 0) {
                        mask.setByte(i, (byte)1);
                        done += 1;
                    }
        }
        else {
            long sSize = nNodes - sampleSize;
            for (long i = 0; i < nNodes; i++)
                mask.setByte(i, (byte)1);
            double rLimit = (double)sSize / nNodes;
            while (done < sSize)
                for (long i = 0; i < mask.length() && done < sampleSize; i++)
                    if (Math.random() < rLimit && mask.getByte(i) == 1) {
                        mask.setByte(i, (byte)0);
                        done += 1;
                    }
        }
        return mask;
    }
    
    public int getNNodes()
    {
        return (int)seedField.getNNodes();
    }
    
    public Field getSeedField()
    {
        return seedField;
    }
    
    private void prepareSampling()
    {
        subsetParams.setFieldData(nNodes, 
                                  unsampledSeedField instanceof RegularField ? unsampledSeedDims : null);
        gui.getSubsetUI().showRegularDownsizeWidgets(unsampledSeedField instanceof RegularField);
    }
    
    /**
     * Utility field holding list of global ChangeListeners.
     */
    protected transient ArrayList<ChangeListener> changeListenerList = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     * <p/>
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * <p/>
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Clears ChangeListenerList.
     * <p/>
     */
    public synchronized void clearChangeListeners()
    {
        changeListenerList.clear();
    }

    /**
     * Notifies all registered listeners about the event (calls
     * <code>stateChanged()</code> on each listener in
     * <code>changeListenerList</code>).
     */
    public void fireStateChanged()
    {
        if (!params.isAdjusting() && 
            !subsetParams.isAdjusting() &&
            !indexSliceParams.isAdjusting() || 
              seedField.getNNodes() < 32) {
            ChangeEvent e = new ChangeEvent(this);
            for (int i = 0; i < changeListenerList.size(); i++) 
                changeListenerList.get(i).stateChanged(e);
        }
    }

    public OpenBranchGroup getSeedPointsGroup()
    {
        return seedPointsGroup;
    }
    
    public boolean isAdjusting()
    {
        return gui != null && gui.isAdjusting();
    }
}
