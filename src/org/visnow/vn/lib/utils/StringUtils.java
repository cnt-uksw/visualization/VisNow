/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
package org.visnow.vn.lib.utils;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Vector;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class StringUtils
{

    public static boolean isEqualStringArrays(String[] arr1, String[] arr2)
    {
        if (arr1 == null || arr2 == null)
            return false;

        if (arr1.length != arr2.length)
            return false;

        for (int i = 0; i < arr2.length; i++) {
            if (arr1[i] == null || arr2[i] == null)
                return false;

            if (!arr1[i].equals(arr2[i]))
                return false;
        }

        return true;
    }

    /**
     * Sorts filenames using numerical sort.
     * Example:
     * input: {f1.txt, f10.txt, f2.txt, f9.txt}
     * output: {f1.txt, f2.txt, f9.txt, f10.txt}
     * <p>
     * @param files list of files
     * <p>
     * @return numerically sorted list of filenames
     */
    public static String[] sortFilenameList(File[] files)
    {
        String[] paths = new String[files.length];
        for (int i = 0; i < paths.length; i++) {
            paths[i] = files[i].getAbsolutePath();
        }
        sortFilenameList(paths);
        return paths;
    }

    /**
     * Sorts filenames using numerical sort.
     * Example:
     * input: {f1.txt, f10.txt, f2.txt, f9.txt}
     * output: {f1.txt, f2.txt, f9.txt, f10.txt}
     * <p>
     * @param filenames list of filenames
     */
    public static void sortFilenameList(String[] filenames)
    {
        Comparator<String> comparator = new Comparator<String>()
        {
            @Override
            public int compare(String o1, String o2)
            {
                return getFileId(o1).compareTo(getFileId(o2));
            }

            private Long getFileId(String filePath)
            {
                String[] token = filePath.split("\\.");
                if (token.length == 0) {
                    return -1l;
                }
                String fileName = token[0];
                StringBuilder fileId = new StringBuilder();

                for (int i = fileName.length() - 1; i >= 0; i--) {
                    if (Character.isDigit(fileName.charAt(i))) {
                        fileId.append(fileName.charAt(i));
                    }
                }
                if (fileId.length() == 0) {
                    return -1l;
                }
                long res;
                try {
                    res = Long.parseLong(fileId.reverse().toString());
                } catch (NumberFormatException ex) {
                    res = -1;
                }
                return res;
            }
        };

        Arrays.sort(filenames, comparator);
    }

    public static String join(String[] strings, String separator)
    {
        StringBuffer joined = new StringBuffer();
        for (int i = 0; i < strings.length; i++) {
            if (i != 0) joined.append(separator);
            joined.append(strings[i]);
        }
        return joined.toString();
    }

    /**
     * Invokes toString on every element of input array and returns String array.
     */
    public static String[] toString(Object[] array)
    {
        String[] toStrings = new String[array.length];
        for (int i = 0; i < array.length; i++) toStrings[i] = array[i].toString();
        return toStrings;
    }

    /**
     * @return first element with toString().equals(toStringValue) == true
     *         null if element is not found.
     * <p>
     * @throws NullPointerException if any null array element is found before method exits.
     */
    public static <T> T findByToString(T[] array, String toStringValue)
    {
        for (T element : array) if (element.toString().equals(toStringValue)) return element;

        return null;
    }

    /**
     * Replaces all chars special to .vnf syntax by underscores
     *
     * @param s modified string
     *
     * @return replacement result
     */
    public static String replaceSpecCharsBy_(String s)
    {
        return s.replaceAll("\\s", "_").replaceAll(",", "_").replaceAll("\\.", "_").replaceAll("=", "_").replaceAll(":", "_");
    }

    public static final String[] findBlock(String[] src, int start)
    {
        if (src.length - start < 2)
            return null;
        int depth = 0;
        for (int i = start + 1, j = 0; i < src.length; i++, j++) {
            String s = src[i];
            if (s.contains("{"))
                depth += 1;
            else if (s.contains("}")) {
                depth -= 1;
                if (depth < 0) {
                    if (j == 0)
                        return null;
                    String[] block = new String[j];
                    System.arraycopy(src, start + 1, block, 0, j);
                    return block;
                }
            }
        }
        return null;
    }

    /**
     * finds all quote enclosed substrings in processedLine and replaces each such
     * substring by __n, where n is its number
     * <p>
     * @param processedLine - line to be processed
     * @param substrings    vector of quoted substrings found in the processed string
     * <p>
     * @return processedLine after substitution
     */
    public static String findSubstrings(String processedLine, Vector<String> substrings)
    {
        String[] stringsInLine = processedLine.split("\"");
        substrings.clear();
        if (stringsInLine.length >= 2) {
            for (int i = 1; i < stringsInLine.length; i += 2) {
                substrings.add(stringsInLine[i]);
                processedLine = processedLine.replaceFirst("\"[^\"]*\"", "__" + (i / 2));
            }
        }
        return processedLine;
    }

    /**
     * finds a triple of strings prefix+"red", prefix+"green", prefix+"blue" or prefix+"r", prefix+"g", prefix+"b"
     * or "red"+suffix, "green"+suffix, "blue"+suffix or "r"+suffix, "g"+suffix, "b"+suffix as candidates for RGB
     * coloring components names.
     *
     * @param in an array of strings (component names)
     *
     * @return an array of 3 selected indices or an empty array otherwise
     */
    public static final int[] findRGB(String[] in)
    {
        if (in.length < 3)
            return new int[0];
        String[] trm = new String[in.length];
        String[] low = new String[in.length];
        for (int i = 0; i < in.length; i++) {
            trm[i] = in[i].trim();
            low[i] = trm[i].toLowerCase();
        }

        int ir = -1, ig = -1, ib = -1;    // trying "red"+suffix, "green"+suffix, "blue"+suffix
        for (int i = 0; i < low.length; i++) {
            if (low[i].startsWith("red")) {
                ir = i;
                String suffix = trm[i].substring(3);
                for (int j = 0; j < low.length; j++) {
                    if (low[j].startsWith("green") && suffix.equals(trm[j].substring(5)))
                        ig = j;
                    if (low[j].startsWith("blue") && suffix.equals(trm[j].substring(4)))
                        ib = j;
                }
            }
        }
        if (ir >= 0 && ig >= 0 && ib >= 0)
            return new int[]{ir, ig, ib};
        ir = ig = ib = -1;              // trying "r"+suffix, "g"+suffix, "b"+suffix
        for (int i = 0; i < low.length; i++) {
            if (low[i].startsWith("r")) {
                ir = i;
                String suffix = trm[i].substring(1);
                for (int j = 0; j < low.length; j++) {
                    if (low[j].startsWith("g") && suffix.equals(trm[j].substring(1)))
                        ig = j;
                    if (low[j].startsWith("b") && suffix.equals(trm[j].substring(1)))
                        ib = j;
                }
            }
        }
        if (ir >= 0 && ig >= 0 && ib >= 0)
            return new int[]{ir, ig, ib};

        ir = ig = ib = -1;              // trying prefix+"red,  prefix+"green", prefix+"blue"
        for (int i = 0; i < low.length; i++) {
            if (low[i].endsWith("red")) {
                ir = i;
                String prefix = trm[i].substring(0, trm[i].length() - 3);
                for (int j = 0; j < low.length; j++) {
                    if (low[j].endsWith("green") && prefix.equals(trm[j].substring(0, trm[j].length() - 5)))
                        ig = j;
                    if (low[j].endsWith("blue") && prefix.equals(trm[j].substring(0, trm[j].length() - 4)))
                        ib = j;
                }
            }
        }
        if (ir >= 0 && ig >= 0 && ib >= 0)
            return new int[]{ir, ig, ib};
        ir = ig = ib = -1;              // trying prefix+"r,  prefix+"g", prefix+"b"
        for (int i = 0; i < low.length; i++) {
            if (low[i].endsWith("r")) {
                ir = i;
                String prefix = trm[i].substring(0, trm[i].length() - 1);
                for (int j = 0; j < low.length; j++) {
                    if (low[j].endsWith("g") && prefix.equals(trm[j].substring(0, trm[j].length() - 1)))
                        ig = j;
                    if (low[j].endsWith("b") && prefix.equals(trm[j].substring(0, trm[j].length() - 1)))
                        ib = j;
                }
            }
        }
        if (ir >= 0 && ig >= 0 && ib >= 0)
            return new int[]{ir, ig, ib};

        return new int[0];
    }

    /**
     * Generates the list of column names used in Microsoft Excel.
     *
     * @param ncolumns number of columns
     * @param numericSuffix if true, then numeric suffix (number) is added to each column name.
     * @param numericSuffixInitialValue initial value of numeric suffix. 
     *
     * @return list of column names used in Microsoft Excel.
     */
    public static String[] generateExcelColumnNames(int ncolumns, boolean numericSuffix, int numericSuffixInitialValue)
    {
        if (ncolumns <= 0) {
            return null;
        }
        String[] result = new String[ncolumns];
        for (int i = 0; i < ncolumns; i++) {
            int n = i + 1;
            StringBuilder sb = new StringBuilder();
            while (n > 0) {
                int index = (n - 1) % 26;
                sb.append((char) (index + 'A'));
                n = (n - 1) / 26;
            }
            String name = sb.reverse().toString();
            if(numericSuffix) {
                name += "(" + (numericSuffixInitialValue + i) + ")";
            }
            result[i] = name;

        }
        return result;
    }
    
    /**
     * Converts complex number to a string a+bi.
     * @param c complex number {a, b}
     * @param format string format
     * @return a+bi
     */
    public static String complexToString(float[] c, String format)
    {
        StringBuilder sb = new StringBuilder(String.format(format, c[0]));
        if (c[1] >= 0) {
            sb.append("+");
        }
        sb.append(String.format(format, c[1]));
        sb.append("i");
        return sb.toString();
    }

    private StringUtils()
    {

    }

    public static final void main(String[] args)
    {
//        String[] src = {"public static final String[] findBlock(String[] src, int start){"
//                , "        if (src.length - start <2)"
//                , "            return null;"
//                , "        int depth = 0;"
//                , "        for (int i = start + 1, j = 0; i < src.length; i++, j++) {"
//                , "            String s = src[i];"
//                , "            if (s.contains(x))"
//                , "                depth += 1;"
//                , "            else if (s.contains(y)) {"
//                , "                depth -= 1;"
//                , "                    if (j == 0)"
//                , "                        return null;"
//                , "                    String[] block = new String[j];"
//                , "                    System.arraycopy(src, start + 1, block, 0, j);"
//                , "                    return block;"
//                , "                }"
//                , "            }"
//                , "        }"
//                , "       return null;"};
//        String[] res = findBlock(src,0);
//        for (int i = 0; i < res.length; i++)
//            System.out.println(res[i ]);
//        System.out.println("");
//        res = findBlock(src,4);
//        for (int i = 0; i < res.length; i++)
//            System.out.println(res[i ]);
//        String s = "test \"aa bb cc dd\" and \"foo bar\"";
//        Vector<String> f = new Vector<>();
//        System.out.println(findSubstrings(s, f));
//        for (String string : f)
//            System.out.println(string);
        String[] in = {
            "aaared", "aqqblue", "aaagreen", "aaablue", "aaarrr"
        };
        int[] out = findRGB(in);
        for (int i = 0; i < out.length; i++)
            System.out.println(in[out[i]]);
        System.out.println("");
        in = new String[]{
            "aabr", "aqq", "aaag", "aaab", "aaar"
        };
        out = findRGB(in);
        for (int i = 0; i < out.length; i++)
            System.out.println(in[out[i]]);
        System.out.println("");
    }
}
