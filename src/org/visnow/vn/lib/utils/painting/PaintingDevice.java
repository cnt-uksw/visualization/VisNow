/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.painting;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import org.visnow.vn.lib.utils.ImageUtils;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public abstract class PaintingDevice
{

    protected PaintingDeviceUI ui;
    protected Cursor cursor;
    protected Color color = Color.RED;
    protected Color cursorColor = Color.WHITE;
    protected Color bgTransparencyColor = Color.MAGENTA;

    public PaintingDeviceUI getUI()
    {
        return ui;
    }

    public Cursor getCursor()
    {
        return cursor;
    }

    public void setColor(Color color)
    {
        this.color = color;
    }

    public Color getColor()
    {
        return color;
    }

    public abstract BufferedImage onMouseClicked(int x, int y, int button, int nClicks, BufferedImage cursorLayer);

    public abstract void onMousePressed(int x, int y, int button, BufferedImage cursorLayer);

    public abstract BufferedImage onMouseReleased(int x, int y, int button, BufferedImage cursorLayer);

    public abstract void onMouseDragged(int x, int y, int button, BufferedImage cursorLayer);

    public abstract void onMouseMoved(int x, int y, BufferedImage cursorLayer);

    public void onMouseWheelMoved(int x, int y, BufferedImage cursorLayer)
    {
        onMouseMoved(x, y, cursorLayer);
    }

    protected abstract void paintOn(int x, int y, BufferedImage layer, boolean inverse);

    protected abstract void paintCursorOn(int x, int y, BufferedImage layer);

    public Color getBgTransparencyColor()
    {
        return bgTransparencyColor;
    }

    public void setBgTransparencyColor(Color bgTransparencyColor)
    {
        this.bgTransparencyColor = bgTransparencyColor;
    }

    protected void paintBackground(BufferedImage layer)
    {
        Graphics g = layer.getGraphics();
        Color oldColor = g.getColor();
        g.setColor(bgTransparencyColor);
        g.fillRect(0, 0, layer.getWidth(), layer.getHeight());
        g.setColor(oldColor);
        ImageUtils.makeTransparent(layer, bgTransparencyColor);
    }

    public Color getCursorColor()
    {
        return cursorColor;
    }

    public void setCursorColor(Color cursorColor)
    {
        this.cursorColor = cursorColor;
    }

}
