/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.probeInterfaces;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import org.jogamp.java3d.J3DGraphics2D;

/**
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */

public class PointValuesDisplay {
    
    private static boolean isDark(Color c)
    {
        float[] v = new float[3];
        c.getRGBColorComponents(v);
        return 0.299 * v[0] + 0.587 * v[1] + 0.114 * v[2] < .5;
    }
    
    private static String displayVal(Object obj)
    {
        if (obj instanceof Byte)
            return String.format("%6d", 0xff & (Byte)obj);
        else if (obj instanceof Short)
            return String.format("%6d", (Short)obj);
        else if (obj instanceof Integer)
            return String.format("%6d", (Integer)obj);
        else if (obj instanceof Float)
            return String.format("%10.3f", (Float)obj);
        else if (obj instanceof Double)
            return String.format("%10.3f", (Double)obj);
        else if (obj instanceof byte[]) {
            byte[] arr = (byte[])obj;
            StringBuilder bld = new StringBuilder("[");
            for (int i = 0; i < arr.length; i++) {
                bld.append(String.format("%3d", 0xff&arr[i]));
                if (i < arr.length - 1)
                    bld.append(", ");
                else
                    bld.append("]");
            }
            return bld.toString();
        }
        else if (obj instanceof short[]) {
            short[] arr = (short[])obj;
            StringBuilder bld = new StringBuilder("[");
            for (int i = 0; i < arr.length; i++) {
                bld.append(String.format("%3d", arr[i]));
                if (i < arr.length - 1)
                    bld.append(", ");
                else
                    bld.append("]");
            }
            return bld.toString();
        }
        else if (obj instanceof int[]) {
            int[] arr = (int[])obj;
            StringBuilder bld = new StringBuilder("[");
            for (int i = 0; i < arr.length; i++) {
                bld.append(String.format("%3d", arr[i]));
                if (i < arr.length - 1)
                    bld.append(", ");
                else
                    bld.append("]");
            }
            return bld.toString();
        }
        else if (obj instanceof float[]) {
            float[] arr = (float[])obj;
            StringBuilder bld = new StringBuilder("[");
            for (int i = 0; i < arr.length; i++) {
                bld.append(String.format("%6.3f", arr[i]));
                if (i < arr.length - 1)
                    bld.append(", ");
                else
                    bld.append("]");
            }
            return bld.toString();
        }
        else if (obj instanceof double[]) {
            double[] arr = (double[])obj;
            StringBuilder bld = new StringBuilder("[");
            for (int i = 0; i < arr.length; i++) {
                bld.append(String.format("%10.3e", arr[i]));
                if (i < arr.length - 1)
                    bld.append(", ");
                else
                    bld.append("]");
            }
            return bld.toString();
        }
        else if (obj instanceof String)
            return (String) obj;
        return obj.toString();
    }
    
    public  static void displayPointValues(J3DGraphics2D gr, int ix, int iy, Object[] vals, Color[] colors,
                                           int width, int height, NumericDisplayParams params)
    {
        String[] valStrings = new String[vals.length];
        for (int i = 0; i < vals.length; i++) 
            valStrings[i] = displayVal(vals[i]);
        int nDisplayedComponents = 0;
        int fontHeight = (int)(.013 * height);
        gr.setFont(new java.awt.Font(Font.DIALOG_INPUT, 0, fontHeight));
        FontMetrics fm = gr.getFontMetrics();
        int valWidth  = 0;
        for (int i = 0; i < vals.length; i++) {
            int valW = fm.stringWidth(valStrings[i]);
            if (valW > valWidth)
                valWidth = valW;
        }
        int rectWidth = valWidth + 15;
        int rectHeight = nDisplayedComponents * (fontHeight + 2) + 8;
        //compute box position
        int xPos = ix, yPos = iy;
        if (yPos < rectHeight)
            yPos += rectHeight;
        if (xPos + rectWidth > width)
            xPos -= rectWidth;
        float[] colCmp = new float[3];
        Color bgr = params.getEffectiweBgrColor();
        bgr.getRGBColorComponents(colCmp);
        gr.setColor(new Color(colCmp[0], colCmp[1], colCmp[2], .5f));
        gr.fillRect(xPos, yPos - rectHeight, rectWidth, rectHeight);
        if (isDark(bgr))
            gr.setColor(Color.LIGHT_GRAY);
        else
            gr.setColor(Color.DARK_GRAY);
        gr.drawRect(xPos, yPos - rectHeight, rectWidth, rectHeight);
        int y = yPos - rectHeight + 3 + fontHeight;
        for (int i = 0; i <  vals.length; i++) {
            if (colors != null)
                gr.setColor(colors[i]);
            gr.drawString(valStrings[i], xPos + 3, y);
            y += fontHeight + 2;
        }
    }
}
