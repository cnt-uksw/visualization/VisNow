/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.probeInterfaces;

import org.jogamp.java3d.J3DGraphics2D;
import org.visnow.jscic.IrregularField;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;

public abstract class ProbeDisplay implements Comparable
{
    public static enum Position {AT_POINT, TOP, RIGHT, BOTTOM, LEFT, NONE};
    
    protected IrregularField fld;
    
    protected float[] center = {0, 0, 0};
    protected float[] crds;
    protected float[] cornerCoords;
    protected int[] screenHandle = new int[2];
    protected boolean xSort = false;
    protected boolean pointerLine = true;
    protected Position probesPosition = Position.RIGHT;
    protected int[] probeOffset = {30, 60};
    protected float probeScale = .2f;
    protected int currentPosition = 0;
    protected boolean selected = false;
    protected int ixmin = 0, ixmax = 0, iymin = 0, iymax = 0;
    protected int screenX, screenY, screenW, screenH;
    protected String title = null;
    protected float titleFontSize = .02f;
    
    public abstract void draw(J3DGraphics2D gr, LocalToWindow ltw, int width, int height, String title);

    public abstract void drawOnMargin(J3DGraphics2D gr, LocalToWindow ltw, 
                                      int width, int height, float scale, int index, 
                                      int ix, int iy, String title);
    
    public abstract void draw(J3DGraphics2D gr, LocalToWindow ltw, 
                              int x, int y, int width, int height, 
                              int fontSize, String title, int index);

    @Override
    public int compareTo(Object obj)
    {
        if (!(obj instanceof ProbeDisplay))
            return 0;
        ProbeDisplay p = (ProbeDisplay)obj;
        if (xSort)
            return p.screenHandle[0] > screenHandle[0] ? -1: 1;
        else
            return p.screenHandle[1] > screenHandle[1] ? -1: 1;
    }
    
    public IrregularField getField()
    {
        return fld;
    }
    
    public void setxSort(boolean xSort) {
        this.xSort = xSort;
    }

    public void setPointerLine(boolean pointerLine) {
        this.pointerLine = pointerLine;
    }

    public void setProbesPosition(Position probesPosition) {
        this.probesPosition = probesPosition;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    public float[] getCenter()
    {
        return center;
    }

    public void setCenter(float[] center)
    {
        this.center = center;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getTitleFontSize()
    {
        return titleFontSize;
    }

    public void setTitleFontSize(float titleFontSize)
    {
        this.titleFontSize = titleFontSize;
    }
    
    public boolean isInside(int ix, int iy)
    {
        selected = screenX <= ix && ix <= screenX + screenW && screenY - screenH <= iy && iy <= screenY;
        return selected;
    }

    public int[] getScreenHandle() {
        return screenHandle;
    }

    public abstract void updateScreenCoords(LocalToWindow ltw);
    
}
