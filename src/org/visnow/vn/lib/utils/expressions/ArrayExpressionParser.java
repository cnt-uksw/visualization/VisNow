/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.expressions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import static org.visnow.vn.lib.utils.expressions.Operator.*;
import static org.apache.commons.math3.util.FastMath.*;
import org.apache.log4j.Logger;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.utils.PhysicalConstants;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl), University of Warsaw, ICM 
 * based on:
 * http://www.technical-recipes.com/2011/a-mathematical-expression-parser-in-java-and-cpp
 * modified by:
 * Krzysztof Nowinski (know@icm.edu.pl, knowpl@gmail.com), University of Warsaw, ICM 
 *
 */
public class ArrayExpressionParser
{
    /**
     * enum containing acceptable types of tokens in an arithmetic expression
     */
    public static enum TOKEN {VARIABLE, NUMBER, 
                              LEFT_PARENTHESIS, RIGHT_PARENTHESIS, 
                              FUNCTION, OPERATOR, COMMA, UNKNOWN};

    private static final Logger LOGGER = Logger.getLogger(ArrayExpressionParser.class);
    private final boolean doublePrecision;
    private final boolean ignoreUnits;
    private final Map<String, DataArray> variables = new HashMap<>();
    private final long dataSize;

    public ArrayExpressionParser(long dataSize, boolean doublePrecision, boolean ignoreUnits, Map<String, DataArray> localVariables)
    {
        this.dataSize = dataSize;
        this.doublePrecision = doublePrecision;
        this.ignoreUnits = ignoreUnits;
        ArrayList<String> vars = new ArrayList<>();
        vars.addAll(localVariables.keySet());
        variables.put("PI", DataArray.createConstant(doublePrecision ? DataArrayType.FIELD_DATA_DOUBLE : DataArrayType.FIELD_DATA_FLOAT, dataSize, PI));
        variables.put("E",  DataArray.createConstant(doublePrecision ? DataArrayType.FIELD_DATA_DOUBLE : DataArrayType.FIELD_DATA_FLOAT, dataSize, E));
        variables.put("I",  DataArray.createConstant(DataArrayType.FIELD_DATA_COMPLEX, dataSize, new float[] { 0f, 1f}));
        for (PhysicalConstants var : PhysicalConstants.values()) {
            variables.put("_" + var.getSymbol(), DataArray.createConstant(doublePrecision ? 
                                                                    DataArrayType.FIELD_DATA_DOUBLE : 
                                                                    DataArrayType.FIELD_DATA_FLOAT, 
                                                     dataSize, var.getValue(), var.getSymbol(), var.getUnit(), null));
        }
        variables.putAll(localVariables);
    }
    
/**
 * checks the input token type
     * @param s          input token
     * @param variables  a collection of all currently available variables
     * @return the type of the input
*/
    public static TOKEN tokenType(String s, Map<String, DataArray> variables)
    {
        if (s.equals("("))
            return TOKEN.LEFT_PARENTHESIS;
        if (s.equals(")"))
            return TOKEN.RIGHT_PARENTHESIS;
        if (s.equals(","))
            return TOKEN.COMMA;      
        try {
            Double.parseDouble(s);
            return TOKEN.NUMBER;        
        } catch (NumberFormatException e) {
        }
        if (s.length() == 1 && s.matches("[+\\-*/<>^~]") ||
            s.equals("<=") || s.equals(">=") || 
            s.equals("==") || s.equals("!="))
            return TOKEN.OPERATOR;
        if (variables.containsKey(s))
            return TOKEN.VARIABLE;
        if (isOperator(s))
            return TOKEN.FUNCTION;
        return TOKEN.UNKNOWN;
    }
    
    
    private static int countArguments(String[] inputTokens, int position, ArrayList<String> out)
    {
        int level = -1;
        int count = 0;
        int i;
        for (i = position; i < inputTokens.length; i++) {
            String inputToken = inputTokens[i];
            if (inputToken.equals("(")) {
                level += 1;
                out.add(inputToken);
            }
            else if (inputToken.equals(",")) { 
                if (level == 0)
                    count += 1;
                out.add(inputToken);
            }
            else if (inputToken.equals(")")) {
                level -= 1;
                if (level < 0) {
                    out.add(",");
                    out.add("" + (count + 1));
                    out.add(")");
                    break;
                }
                else
                    out.add(inputToken);
            }
            else if (Operator.isOperator(inputToken) && Operator.getNArguments(inputToken) == Integer.MAX_VALUE) {
                out.add(inputToken);
                i = countArguments(inputTokens, i + 1, out);
            }
            else
                out.add(inputToken);
        }
        return i;
    }

/**
 * parses calls of multi-argument functions adding the arguments count at the end of the argument sequence
 * @param inputTokens an arithmetic expression (in infix format) 
 * @return The input expression with augmented multi-argument function calls, e.g.
 * max(a,b,min(c,d)) submitted as {"max", "(", "a", ",", "b", ",", "min", "(", "c", ",", "d", ")", ")"} will return
 * {"max", "(", "a", ",", "b", ",", "min", "(", "c", ",", "d" , ",", "2", ")", ",", "3", ")"}, that is max(a,b,min(c,d,2),3)
 */
    public static String[] addArgumentsCounts(String[] inputTokens)
    {
        ArrayList<String> out = new ArrayList<>();
        for (int i = 0; i < inputTokens.length; i++) {
            String inputToken = inputTokens[i];
            out.add(inputToken);
            if (Operator.isOperator(inputToken) && Operator.getNArguments(inputToken) == Integer.MAX_VALUE)
                i = countArguments(inputTokens, i + 1, out);
        }
        String[] output = new String[out.size()];
        out.toArray(output);
        return output;
    }
/**
 * Converts an arithmetic expression to the RPN form
 * @param input  an arithmetic expression in the form of an array of tokens
 * @param variables a map of predefined variables 
 * @return an RPN arithmetic expression in the form of an array of tokens
 * throws IllegalArgumentException when unknown token is found in the input
 */
    public static String[] infixToRPN(String[] input, Map<String, DataArray> variables) throws IllegalArgumentException
    {
        String[] inputTokens = addArgumentsCounts(input);
        ArrayList<String> out = new ArrayList<>();
        Stack<String> stack = new Stack<>();
        for (String token : inputTokens) 
            switch (tokenType(token, variables)) {
                case NUMBER:
                case VARIABLE:
                    out.add(token);
                    break;
                case FUNCTION:
                    stack.push(token);
                    break;
                case OPERATOR:
                    while (!stack.empty()) {
                        String tokenFromStack = stack.peek();
                        TOKEN tokenFromStackType = tokenType(tokenFromStack, variables);
                        if (tokenFromStackType == TOKEN.FUNCTION ||
                            tokenFromStackType == TOKEN.OPERATOR &&
                                (getPrecedence(token) < getPrecedence(tokenFromStack) ||
                                 getPrecedence(token) == getPrecedence(tokenFromStack) &&
                                 Operator.isAssociative(tokenFromStack, LEFT_ASSOC))) 
                            out.add(stack.pop());
                        else
                            break;
                    }
                    stack.push(token);
                    break;
                case LEFT_PARENTHESIS:
                    stack.push(token);
                    break;
                case COMMA:
                    while (!stack.empty()) {
                        String tokenFromStack = stack.peek();
                        TOKEN tokenFromStackType = tokenType(tokenFromStack, variables);
                        if (tokenFromStackType == TOKEN.LEFT_PARENTHESIS)
                            break;
                        out.add(stack.pop());
                    }
                    break;
                case RIGHT_PARENTHESIS:
                    while (!stack.empty()) {
                        String tokenFromStack = stack.pop();
                        TOKEN tokenFromStackType = tokenType(tokenFromStack, variables);
                        if (tokenFromStackType == TOKEN.LEFT_PARENTHESIS)
                            break;
                        out.add(tokenFromStack);
                    }
                    if (!stack.empty() && tokenType(stack.peek(), variables) == TOKEN.FUNCTION)
                        out.add(stack.pop());
                    break;
                default:
                    throw new IllegalArgumentException("unknown symbol " + token);
            }
        while (!stack.empty())
            out.add(stack.pop());
        String[] output = new String[out.size()];
        return out.toArray(output);
    }

    /**
     * Check if a string is a number literal
     * @param token string to check
     * @return true if token can be parsed as a (double) number, false otherwise
     */
    public static boolean isNumeric(String token)
    {
        try {
            double d = Double.parseDouble(token);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }
    

    /**
     * Check if a string is a name of a currently available variable
     * @param token string to check
     * @return true if token is a name of a currently available variable, false otherwise
     */
    public boolean isVariable(String token)
    {
        return variables.containsKey(token);
    }
/**
 * Evaluates an expression creating an output DataArray
 * @param tokens An arithmetic expression parsed to the RPN form
 * @return a Data array containing evaluated expression (the evaluation is done pointwise 
 * with the exception of avg (average) and stddev (standard deviation) operators returning single-value DataArray
 * @throws IllegalArgumentException in the case of an improperly formed expression or unknown variables
 */
    public DataArray RPNtoDataArray(String[] tokens) throws Exception
    {
         Stack<Object> stack = new Stack<>();

        //TODO introduce chunks
        for (String token : tokens) {
            if (!isOperator(token)) 
                stack.push(token);
            else {
                Operator op = getOperator(token);
                int nArguments = op.getNArguments();
                if (op.getNArguments() == Integer.MAX_VALUE) 
                    nArguments = Integer.parseInt((String)stack.pop());
                    
                DataArray[] args = new DataArray[nArguments];
                Object tmp;
                String var;
                for (int i = 0; i < args.length; i++) {
                    tmp = stack.pop();
                    if (tmp instanceof String && isVariable((String) tmp)) {
                        var = (String) tmp;
                        args[i] = getVariable(var, true);
                        if (args[i] == null)
                            throw new IllegalArgumentException("variable " + var + "not found");
                    } else if (tmp instanceof DataArray) {
                        args[i] = (DataArray) tmp;
                    } else if (tmp instanceof String && isNumeric((String) tmp)) {
                        args[i] = DataArray.createConstant(doublePrecision ? DataArrayType.FIELD_DATA_DOUBLE : DataArrayType.FIELD_DATA_FLOAT,
                                dataSize, Double.valueOf((String) tmp));
                    } else {
                        throw new IllegalArgumentException("unknown symbol" + tmp);
                    }
                }

                DataArray result = op.evaluate(dataSize, doublePrecision, ignoreUnits, args);
                stack.push(result);
            }
        }

        Object obj = stack.pop();
        DataArray out = null;
        if (!stack.isEmpty()) 
            throw new IllegalArgumentException("ERROR evaluating expression");
        if (obj instanceof DataArray) 
            out = (DataArray) obj;
        else if (obj instanceof String && isVariable((String) obj)) 
            out = getVariable((String) obj, true);

        else if (obj instanceof String && isNumeric((String) obj)) 
            out = DataArray.createConstant(doublePrecision ? DataArrayType.FIELD_DATA_DOUBLE : DataArrayType.FIELD_DATA_FLOAT, dataSize, 
                                           Double.valueOf((String) obj));
        
        if (out instanceof DataArray) {
            if(ignoreUnits) 
                out.setUnit("1");
            return out;
        }
        //this is error code - you should not be here if everything is OK                
        if (obj instanceof String) 
            throw new IllegalArgumentException("ERROR evaluating expression - wrong variable: " + ((String) obj));

        throw new IllegalArgumentException("ERROR evaluating expression");
    }

    private DataArray getVariable(String name, boolean remap)
    {
        if (remap && (name.contains(".") || name.equals("x") || name.equals("y") || name.equals("z")) &&
            !(name.equals("index.i") || name.equals("index.j") || name.equals("index.k"))) {
            DataArray data;
            if (variables.containsKey(name)) 
                data = (DataArray) variables.get(name);
            else 
                throw new IllegalArgumentException("ERROR: No such variable " + name);
            int veclen = data.getVectorLength();
            if (veclen == 1)
                return data;
            int v = interpretVindex(name);
            if (v == -1)
                return null; //this should never happen
            return data.getSubcomponent(v);

        } else if (variables.containsKey(name))
            return (DataArray) variables.get(name);
        else
            throw new IllegalArgumentException("ERROR: No such variable " + name);
    }

    public DataArray evaluateExpr(String expr) throws Exception
    {
        String str = preprocessExpr(expr);
        LOGGER.info("evaluating " + str);
        checkParentheses(str);
        String[] input = str.split(" +");
        String[] output = infixToRPN(input, variables);
        return RPNtoDataArray(output);
    }
    
/**
 * Normalization of an expression string to a string of tokens (operators, parentheses, function names and numbers) separated by single spaces.
 * In addition, ++ and -- are replaced by +, +- and -+ by -, unary - is changed to ~
 * @param input raw expression string (as keyed in by the user)
 * @return clean form of the input expression
*/
    public static String preprocessExpr(String input)
    {
        return input.replaceAll("\\s", "").                     //remove white chars
                     replaceAll("\\+-", "-").
                     replaceAll("-\\+", "-").
                     replaceAll("\\+\\+", "+").    
                     replaceAll("--", "+").                     // clean -- +- -+ ++
                     replaceAll("^-", "~").
                     replaceAll("^\\+", "").                    // at the begining remove "+", change "-" to unary minus ("~")
                     replaceAll("--", "-~").                    //
                     replaceAll("([^A-Za-z0-9)])-", "$1~").     // change "-" to unary minus when not between arguments
                     replaceAll("([^A-Za-z0-9)])\\+", "$1").    // remove "+" when not between arguments
                     replaceAll("([+\\-*/<>!=^()~,])", " $1 "). // add spaces between tokens
                     replaceAll(" +", " ").trim().              // replace multiple spaces with a single one
                     replaceAll("([<>!=]) +=", "$1=").          // remove spaces inside digraph comparators
                     replaceAll("([0-9]\\.?) ?[Ee] ?([+-]?) ?([0-9])", "$1e$2$3"); // remove spaces inserted into a float with a decimal exponent
    }
    
 /**
  * Check for unbalanced parentheses 
  * @param s expression to check
  * returns silently if parentheses are correct, throws IllegalArgumentException otherwise
  */   
    public static void checkParentheses(String s) throws IllegalArgumentException
    {
        int level = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(')
                level += 1;
            else if (s.charAt(i) == ')') {
                level -= 1;
                if (level < 0)
                    throw new IllegalArgumentException("unbalanced parentheses");
            }
        }
        if (level > 0)
            throw new IllegalArgumentException("unbalanced parentheses");
    }

    private static int interpretVindex(String var)
    {
        if (var.contains(".")) {
            String subVarName = var.substring(var.indexOf(".") + 1, var.length());
            try {
                int out = Integer.parseInt(subVarName);
                return out;
            } catch (NumberFormatException ex) {
                return -1;
            }

        } else {
            if (var.equals("_x"))
                return 0;
            if (var.equals("_y"))
                return 1;
            if (var.equals("_z"))
                return 2;
        }
        return -1;
    }

    public static String[] listVariablesInUse(String[] vars, String[] expressions)
    {
        Map<String, DataArray> variables = new HashMap<>();
        for (String var : vars) 
            variables.put(var, null);
        ArrayList<String> out = new ArrayList<>();
        for (String expr : expressions) {
            String str = preprocessExpr(expr);
            String[] input = str.split(" ");
            Map<String, DataArray> tokens = new HashMap<>();
            for (String input1 : input) 
                tokens.put(input1, null);

            for (String var : vars) 
                if (tokens.containsKey(var)) 
                    out.add(var);
        }

        String[] tmp = new String[out.size()];
        for (int i = 0; i < tmp.length; i++) {
            tmp[i] = out.get(i);
        }
        return tmp;
    }
}
