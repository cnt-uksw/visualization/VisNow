/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.apache_commons_math;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.NotPositiveException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.linear.RealVector;

/**
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * University of Warsaw, ICM
 */
public class FloatArrayVector extends RealVector 
{
    private int n;
    private float[] v;
    private int nThreads = Runtime.getRuntime().availableProcessors();
    
    public FloatArrayVector(float[] v)
    {
        this.v = v;
        n = v.length;
    }

    @Override
    public int getDimension()
    {
        return n;
    }

    @Override
    public double getEntry(int i) throws OutOfRangeException
    {
        if (i < 0 || i >= n)
            throw new OutOfRangeException(i, 0, n);
        return v[i];
    }

    @Override
    public void setEntry(int i, double d) throws OutOfRangeException
    {
        if (i < 0 || i >= n)
            throw new OutOfRangeException(i, 0, n);
        v[i] = (float)d;
    }

    @Override
    public RealVector append(RealVector rv)
    {
        float[] tmpV = new float[n + rv.getDimension()];
        System.arraycopy(v, 0, tmpV, 0, n);
        for (int i = 0; i < rv.getDimension(); i++) 
            tmpV[n + i] = (float)rv.getEntry(i);
        return new FloatArrayVector(tmpV);
    }

    @Override
    public RealVector append(double d)
    {
        float[] tmpV = new float[n + 1];
        System.arraycopy(v, 0, tmpV, 0, n);
        tmpV[n] = (float)d;
        return new FloatArrayVector(tmpV);
    }

    @Override
    public RealVector getSubVector(int i, int k) throws NotPositiveException, OutOfRangeException
    {
        if (i < 0)
            throw new OutOfRangeException(i, 0, n);
        if (k <= 0)
            throw new NotPositiveException(k);
        if (i + k > n)
            throw new OutOfRangeException(i + k, 0, n);
        float[] tmpV = new float[k];
        System.arraycopy(v, i, tmpV, 0, k);
        return new FloatArrayVector(tmpV);
    }

    @Override
    public void setSubVector(int i, RealVector rv) throws OutOfRangeException
    {
        int k = rv.getDimension();
        if (i < 0)
            throw new OutOfRangeException(i, 0, n);
        if (k <= 0)
            throw new NotPositiveException(k);
        if (i + k > n)
            throw new OutOfRangeException(i + k, 0, n);
        for (int j = 0; j < k; j++) {
            v[i + j] = (float)rv.getEntry(j);
            
        }
    }

    @Override
    public boolean isNaN()
    {
        for (float f : v) 
            if (Float.isNaN(f))
                return true;
        return false;
    }

    @Override
    public boolean isInfinite()
    {
        for (float f : v) 
            if (Float.isNaN(f))
                return false;
        for (float f : v) 
            if (Float.isInfinite(f))
                return true;
        return false;
    }
    
    private class DotProductPart implements Runnable
    {
        private int iThread; 
        private float[] rv;
        private double[] dpParts;

        public DotProductPart(int iThread, float[] rv, double[] dpParts)
        {
            this.iThread = iThread;
            this.rv = rv;
            this.dpParts = dpParts;
        }
        
        @Override
        public void run()
        {
            double partDotProduct = 0;
            for (int i = (iThread * n) / nThreads; i < ((iThread + 1) * n) / nThreads; i++)
                partDotProduct += v[i] * rv[i];
            dpParts[iThread] = partDotProduct;
        }

    }
    
    @Override
    public double dotProduct(RealVector rv) throws DimensionMismatchException
    {
        if (!(rv instanceof FloatArrayVector))
            return super.dotProduct(rv);
        if (n != rv.getDimension())
            throw new DimensionMismatchException(n, rv.getDimension());
        double dp = 0;
        double[] dpParts = new double[nThreads];
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new DotProductPart(iThread, ((FloatArrayVector)rv).getDataArray(), dpParts));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try
            {
                workThread.join();
            }catch (InterruptedException e) {
            }
        for (int i = 0; i < workThreads.length; i++)
            dp += dpParts[i];
        return dp;
    }


    @Override
    public RealVector copy()
    {
        float[] tmpV = new float[n];
        System.arraycopy(v, 0, tmpV, 0, n);
        return new FloatArrayVector(tmpV);
    }

    @Override
    public RealVector ebeDivide(RealVector rv) throws DimensionMismatchException
    {
        if (n != rv.getDimension())
            throw new DimensionMismatchException(rv.getDimension(), n);
        float[] tmpV = new float[n];
        for (int i = 0; i < n; i++) 
            tmpV[i] = (float)(v[i] / rv.getEntry(i));
        return new FloatArrayVector(tmpV);
    }

    @Override
    public RealVector ebeMultiply(RealVector rv) throws DimensionMismatchException
    {
        if (n != rv.getDimension())
            throw new DimensionMismatchException(rv.getDimension(), n);
        float[] tmpV = new float[n];
        for (int i = 0; i < n; i++) 
            tmpV[i] = (float)(v[i] * rv.getEntry(i));
        return new FloatArrayVector(tmpV);
        
    }
    
    public float[] getDataArray()
    { 
       return v;
    }
    
}
