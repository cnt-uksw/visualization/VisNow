/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.apache_commons_math;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.linear.RealLinearOperator;
import org.apache.commons.math3.linear.RealVector;
import org.visnow.jscic.utils.RegularFieldNeighbors;

/**
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * University of Warsaw, ICM
 */
public class SimplePoissonProblemMatrix extends RealLinearOperator
{
    private int[] dims;
    private int nData ;
    private boolean[] mask;
    private int[] neighbors;
    private int nNeighbors;
    private int nThreads = Runtime.getRuntime().availableProcessors();
    private int[] threadBoundaries = new int[nThreads + 1];
    
    public SimplePoissonProblemMatrix(int[] dims, boolean[] mask)
    {
        nData = 1;
        for (int i = 0; i < dims.length; i++) 
            nData *= dims[i];
        if (mask.length != nData)
            return;
        int [] stat;
        this.dims = dims;
        this.mask = mask;
        int n = 0;
        threadBoundaries[0] = 1;
        switch (dims.length) {
        case 1:
            for (int i = 0; i < dims[0]; i++) 
                if (mask[i])
                    n += 1;
            for (int i = 0, j = 1, k = 0; i < dims[0]; i++) {
                if (mask[i])
                    k += 1;
                if (k >= (j * n) / nThreads)
                {
                    threadBoundaries[j] = i;
                    j += 1;
                }
            }
            threadBoundaries[nThreads] = dims[0] - 1;
            break;
        case 2:
            stat = new int[dims[1] + 1];
            for (int i = 0, k = 0; i < dims[1]; i++) {
                for (int j = 0; j < dims[0]; j++, k++)
                    if (mask[k])
                        n += 1;
                stat[i + 1] = n;
            }
            for (int i = 0, j = 1; i < dims[1]; i++) 
                if (stat[i] >= (j * n) / nThreads) {
                    threadBoundaries[j] = i;
                    j += 1;
                }
            threadBoundaries[nThreads] = dims[1] - 1;
            break;
        case 3:
            stat = new int[dims[2] + 1];
            stat[0] = 0;
            for (int i = 0, k = 0; i < dims[2]; i++) {
                for (int j = 0; j < dims[0] * dims[1]; j++, k++)
                    if (mask[k])
                        n += 1;
                stat[i + 1] = n;
            }
            for (int i = 0, j = 1; i < dims[2]; i++) 
                if (stat[i] >= (j * n) / nThreads) {
                    threadBoundaries[j] = i;
                    j += 1;
                }
            threadBoundaries[nThreads] = dims[2] - 1;
            break;
        }
        neighbors = RegularFieldNeighbors.neighbors(dims)[2];
        nNeighbors = neighbors.length;
    }

    @Override
    public int getRowDimension()
    {
        return nData;
    }

    @Override
    public int getColumnDimension()
    {
        return nData;
    }

    private class ComputePart implements Runnable
    {

        int iThread = 0;

        public ComputePart(int iThread)
        {
            this.iThread = iThread;
        }

        @Override
        public void run()
        {            
            switch (dims.length) {
            case 3:
                for (int i = threadBoundaries[iThread]; i < threadBoundaries[iThread + 1]; i++) 
                    for (int j = 1; j < dims[1] - 1; j++) 
                        for (int k = 1, p = (i * dims[1] + j) * dims[0] + 1; k < dims[0] - 1; k++, p++) {
                            if (!mask[p])
                                continue;
                            float f = 0;
                            for (int l = 0; l < nNeighbors; l++) 
                                if (mask[p + neighbors[l]])
                                   f += in[p + neighbors[l]];
                            out[p] -= f / nNeighbors;
                        }
                break;
            case 2:
                for (int j = threadBoundaries[iThread]; j < threadBoundaries[iThread + 1]; j++) 
                    for (int k = 1, p = j * dims[0] + 1; k < dims[0] - 1; k++, p++) {
                        if (!mask[p])
                            continue;
                        float f = 0;
                        for (int l = 0; l < nNeighbors; l++) 
                            if (mask[p + neighbors[l]])
                                f += in[p + neighbors[l]];
                        out[p] -= f / nNeighbors;
                    }
                break;
            case 1:
                for (int k = threadBoundaries[iThread]; k < threadBoundaries[iThread + 1]; k++) { 
                    if (!mask[k])
                        continue;
                    float f = 0;
                    for (int l = 0; l < nNeighbors; l++) 
                        if (mask[k + neighbors[l]])
                            f += in[k + neighbors[l]];
                    out[k] -= f / nNeighbors;
                }
                break;
            }
        }
    }
    
    private float[] in;
    private float[] out;

    @Override
    public RealVector operate(RealVector rv) throws DimensionMismatchException
    {
        if (rv.getDimension() != nData)
            throw new DimensionMismatchException(rv.getDimension(), nData);
        out = new float[rv.getDimension()];
        if (rv instanceof FloatArrayVector) {
            in = ((FloatArrayVector)rv).getDataArray();
            System.arraycopy(in, 0, out, 0, nData);
            Thread[] workThreads = new Thread[nThreads];
            for (int iThread = 0; iThread < nThreads; iThread++) {
                workThreads[iThread] = new Thread(new ComputePart(iThread));
                workThreads[iThread].start();
            }
            for (Thread workThread : workThreads)
                try
                {
                    workThread.join();
                }catch (InterruptedException e) {
                }
            
        } else {
            for (int i = 0; i < nData; i++) 
                out[i] = (float)rv.getEntry(i);
            in = ((FloatArrayVector)rv).getDataArray();
            System.arraycopy(in, 0, out, 0, nData);
            switch (dims.length) {
            case 3:
                for (int i = 1; i < dims[2] - 1; i++) 
                    for (int j = 1; j < dims[1] - 1; j++) 
                        for (int k = 1, p = (i * dims[1] + j) * dims[0] + 1; k < dims[0] - 1; k++, p++) {
                            if (!mask[p])
                                continue;
                            float f = 0;
                            for (int l = 0; l < nNeighbors; l++) 
                                if (mask[p + neighbors[l]])
                                    f += (float)rv.getEntry(p + neighbors[l]);
                            out[p] -= f / 6;
                        }
                break;
            case 2:
                for (int j = 1; j < dims[1] - 1; j++) 
                    for (int k = 1, p = j * dims[0] + 1; k < dims[0] - 1; k++, p++) {
                        if (!mask[p])
                            continue;
                        float f = 0;
                        for (int l = 0; l < nNeighbors; l++) 
                            if (mask[p + neighbors[l]])
                                f += (float)rv.getEntry(p + neighbors[l]);
                        out[p] -= f / 4;
                    }
                break;
            case 1:
                for (int k = 1; k < dims[0] - 1; k++) {
                    if (!mask[k])
                        continue;
                    float f = 0;
                    for (int l = 0; l < nNeighbors; l++) 
                        if (mask[k + neighbors[l]])
                            f += (float)rv.getEntry(k + neighbors[l]);
                    out[k] -= f / 2;
                }
                break;
            }
        }
        return new FloatArrayVector(out);
    }
}
