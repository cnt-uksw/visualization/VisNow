/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.isolines;

import org.visnow.vn.lib.basic.mappers.Isolines.*;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Vector;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.TimeData;
import org.visnow.vn.lib.utils.field.subset.subvolume.LinearInterpolation;
import org.visnow.vn.lib.utils.field.subset.subvolume.NewNode;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class RegularFieldIsolines
{
    
    private static class IsolinesPart implements Runnable
    {
        final int[] dims;
        final int start, end;
        final FloatLargeArray data;
        final float[] thresholds;
        final float[] rowData;
        int nThresholds;
        Vector<NewNode>[] newNodes;
        int[][] topNodes = null; 
        int[][] vertNodes = null; 
        int[][] bottomNodes = null;
        Vector<int[]>[] edges;
        int[] nNewNodes;
        int[] lastNodes;

        public IsolinesPart(int[] dims, int start, int end, FloatLargeArray data, float[] thresholds)
        {
            this.dims = dims;
            this.start = start;
            this.end = end;
            this.data = data;
            this.thresholds = thresholds;
            rowData = new float[2 * dims[0]];
            nThresholds = thresholds.length;
            newNodes = new Vector[nThresholds];
            edges = new Vector[nThresholds];
            nNewNodes = new int[nThresholds];
            lastNodes = new int[nThresholds];
            for (int i = 0; i < thresholds.length; i++) {
                newNodes[i] = new Vector<>();
                edges[i] = new Vector<>();
                nNewNodes[i] = 0;
                lastNodes[i] = 0;
            }
        }
        
        private void processHorizontalEdge
        (int row, int off, int i, int[][] nodeIndices)
        {
            float v0 = rowData[off + i];
            float v1 = rowData[off + i + 1];
            float vMin = Math.min(v0, v1);
            float vMax = Math.max(v0, v1);
            float d = v1 - v0;
            for (int j = 0; j < nThresholds; j++)
                if (vMin <= thresholds[j] && thresholds[j] < vMax) {
                    newNodes[j].add(new NewNode(row * dims[0] + i, row * dims[0] + i + 1, (thresholds[j] - v0) / d));
                    nodeIndices[j][i] = nNewNodes[j];
                    nNewNodes[j] += 1;
                }
        }
        
        private void processVerticalEdge
        (int row, int i, int[][] nodeIndices)
        {
            float v0 = rowData[i];
            float v1 = rowData[i + dims[0]];
            float vMin = Math.min(v0, v1);
            float vMax = Math.max(v0, v1);
            float d = v1 - v0;
            for (int j = 0; j < nThresholds; j++)
                if (vMin <= thresholds[j] && thresholds[j] < vMax) {
                    newNodes[j].add(new NewNode(row * dims[0] + i, (row + 1) * dims[0] + i, (thresholds[j] - v0) / d));
                    nodeIndices[j][i] = nNewNodes[j];
                    nNewNodes[j] += 1;
                }
        }
        
        private void processQuad(int i)
        {
            
            try {
                for (int j = 0; j < thresholds.length; j++) {
                    float t = thresholds[j];
                    byte code = 0;
                    if (rowData[i] > t)               code |= (byte)0x01;
                    if (rowData[i + 1] > t)           code |= (byte)0x02;
                    if (rowData[i + dims[0]] > t)     code |= (byte)0x04;
                    if (rowData[i + dims[0] + 1] > t) code |= (byte)0x08;
//                    System.out.printf("   %3d", code);
                    switch (code) {
                        case 1:
                        case 14:
                            edges[j].add(new int[]{topNodes[j][i],    vertNodes[j][i]});
                            break;
                        case 2:
                        case 13:
                            edges[j].add(new int[]{topNodes[j][i],    vertNodes[j][i + 1]});
                            break;
                        case 4:
                        case 11:
                            edges[j].add(new int[]{bottomNodes[j][i], vertNodes[j][i]});
                            break;
                        case 8:
                        case 7:
                            edges[j].add(new int[]{bottomNodes[j][i], vertNodes[j][i + 1]});
                            break;
                        case 3:
                        case 12:
                            edges[j].add(new int[]{vertNodes[j][i],   vertNodes[j][i + 1]});
                            break;
                        case 5:
                        case 10:
                            edges[j].add(new int[]{topNodes[j][i],    bottomNodes[j][i]});
                            break;
                        case 9:
                        case 6:
                            edges[j].add(new int[]{topNodes[j][i],    bottomNodes[j][i]});
                            edges[j].add(new int[]{vertNodes[j][i],   vertNodes[j][i+1]});
                            break;
                    }
                }
            } catch (Exception e) {
//                System.out.println("");
            }
        }

        @Override
        public void run()
        {
            for (int row = start; row < Math.min(end, dims[1] - 1); row++) {
                LargeArrayUtils.arraycopy(data, row * dims[0], rowData, 0, 2 * dims[0]);
                if (row == start) {
                    topNodes    = new int[nThresholds][dims[0] - 1];
                    for (int i = 0; i < nThresholds; i++) 
                        Arrays.fill(topNodes[i], -1);
                    for (int i = 0; i < dims[0] - 1; i++) 
                        processHorizontalEdge(row, 0, i, topNodes);
                }
                else 
                    topNodes = bottomNodes;
                vertNodes   = new int[nThresholds][dims[0]];
                bottomNodes = new int[nThresholds][dims[0] - 1];
                for (int i = 0; i < nThresholds; i++) {
                    Arrays.fill(vertNodes[i], -1);
                    Arrays.fill(bottomNodes[i], -1);
                }
                for (int i = 0; i < dims[0]; i++) 
                        processVerticalEdge(row, i, vertNodes);
                if (row == end - 1) 
                    System.arraycopy(nNewNodes, 0, lastNodes, 0, nThresholds);
                for (int i = 0;i < dims[0] - 1; i++) 
                    processHorizontalEdge(row + 1, dims[0], i, bottomNodes);
                if (end == dims[1]) 
                    System.arraycopy(nNewNodes, 0, lastNodes, 0, nThresholds);
                for (int i = 0; i < dims[0] - 1; i++) 
                    processQuad(i);
            }
        }
    }
    /**
     * 
     * @param inField input field with 2D cells
     * @param params  isoline parameters (isoline component, thresholds)
     * @return irregular field of isolines with all components interpolated
     */
    public static IrregularField create(RegularField inField, IsolinesParams params)
    {
        float[] thresholds = params.getThresholds();
        
        DataArray component = inField.getComponent(params.getComponent());
        if (inField == null || component == null || 
            thresholds == null || thresholds.length < 1)
            return null;
        FloatLargeArray fData = component.getVectorLength() == 1 ?
                                component.getRawFloatArray() :
                                component.getVectorNorms();
        int nThresholds = thresholds.length;
        boolean timeDependentIsoComponent = component.isTimeDependant();
        int[] dims = inField.getDims();
        int nThreads = Math.max(1, Math.min(dims[1] / 5, VisNow.availableProcessors()));
        IsolinesPart[] parts = new IsolinesPart[nThreads];
        Thread[] threads = new Thread[nThreads];
        int height = dims[1];
        for (int iThread = 0; iThread < threads.length; iThread++) {
            parts[iThread] = new IsolinesPart(dims, (iThread * height) / nThreads, 
                                             ((iThread + 1) * height) / nThreads, 
                                              fData, thresholds);
            threads[iThread] = new Thread(parts[iThread]);
            threads[iThread].start();
        }
        for (Thread workThread : threads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        int nOutNodes = 0;
        int nEdges = 0;
        int[][] offsets = new int[nThresholds][nThreads];
        for (int iThreshold = 0; iThreshold < nThresholds; iThreshold++)             
            for (int iThread = 0; iThread < nThreads; iThread++) {
                IsolinesPart part = parts[iThread];
                offsets[iThreshold][iThread] = nOutNodes;
                nOutNodes += part.lastNodes[iThreshold];
                nEdges += part.edges[iThreshold].size();
            }
        NewNode[] outNodes = new NewNode[nOutNodes];
        int[] outEdges = new int[2 * nEdges];
        int lNode = 0;
        int lEdge = 0;
        for (int iThreshold = 0; iThreshold < nThresholds; iThreshold++)
            for (int iThread = 0; iThread < nThreads; iThread++) {
                IsolinesPart part = parts[iThread];
                int off = offsets[iThreshold][iThread];
                for (int i = 0; i < part.edges[iThreshold].size(); i++) {
                    int[] edge = part.edges[iThreshold].get(i);
                    for (int j = 0; j < 2; j++, lEdge++)
                        outEdges[lEdge] = edge[j] + off;
                }
                for (int i = 0; i < part.lastNodes[iThreshold]; i++, lNode++) 
                    outNodes[lNode] = part.newNodes[iThreshold].get(i);
            }
        if (nOutNodes < 2)
            return null;
        IrregularField outField = new IrregularField(nOutNodes);
        int nInterpolable = inField.getNComponents();
        if (inField.hasCoords())
            nInterpolable += 1;
        if (inField.hasMask())
            nInterpolable += 1;
        Vector<TimeData> interpolableData = new Vector<>();
        Vector<DataArray> components = new Vector<>();
        int[] vLens = new int[nInterpolable];
        interpolableData.add(component.getTimeData());
        components.add(component);
        vLens[0] = component.getVectorLength();
        int m = 1;
        for (int i = 0; i < inField.getNComponents(); i++) {
            DataArray cmp = inField.getComponent(i);
            if (cmp != component) {
                interpolableData.add(cmp.getTimeData());
                components.add(cmp);
                vLens[m] = cmp.getVectorLength();
                m += 1;
            }
        }
        if (inField.hasCoords()) {
            interpolableData.add(inField.getCoords());
            vLens[m] = 3;
            m += 1;
        }
        if (inField.hasMask()) {
            interpolableData.add(inField.getMask());
            vLens[m] = 1;
        }
            
        Vector<TimeData> interpolatedData = 
                LinearInterpolation.interpolateToNewNodesSet(nOutNodes, interpolableData, vLens, 
                                                             outNodes, timeDependentIsoComponent);
        
        for (m = 0; m < inField.getNComponents(); m++) {
            DataArray cmp = components.get(m);
            outField.addComponent(
                    DataArray.create(interpolatedData.get(m), vLens[m], 
                                     cmp.getName(), cmp.getUnit(), cmp.getUserData()).
                                           preferredRanges(cmp.getPreferredMinValue(), 
                                                           cmp.getPreferredMaxValue(), 
                                                           cmp.getPreferredPhysMinValue(), 
                                                           cmp.getPreferredPhysMaxValue()));
        }
        if (inField.hasCoords())
            outField.setCoords(interpolatedData.get(m));
        else {
            float[] crds = new float[3 * nOutNodes];
            float[][] affine = inField.getAffine();
            for (int i = 0, l = 0; i < nOutNodes; i++) {
                NewNode p = outNodes[i];
                int i0 = (int)(p.p0 % dims[0]);
                int i1 = (int)(p.p0 / dims[0]);
                int j0 = (int)(p.p1 % dims[0]);
                int j1 = (int)(p.p1 / dims[0]);
                float t0 = i0 * (1 - p.ratio) + j0 * p.ratio;
                float t1 = i1 * (1 - p.ratio) + j1 * p.ratio;
                for (int j = 0; j < 3; j++, l++) 
                    crds[l] = affine[3][j] + t0 * affine[0][j] + t1 * affine[1][j];
            }
            outField.setCoords(new FloatLargeArray(crds), 0);
        }
        if (inField.hasMask())
            outField.setMask(interpolatedData.get(m));
        CellArray segCellArray = new CellArray(CellType.SEGMENT, outEdges, null, null);
        CellSet outLines = new CellSet();
        outLines.addCells(segCellArray);
        outField.addCellSet(outLines);
        return outField;
    }
}
