/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.text;

import org.jogamp.java3d.*;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import static org.visnow.jscic.dataarrays.DataArrayType.*;
import org.visnow.jscic.dataarrays.StringDataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.geometries.objects.DataMappedGeometryObject;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.parameters.FontParams;
import org.visnow.vn.geometries.textUtils.Texts2D;
import org.visnow.vn.geometries.utils.Texts3D;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import org.visnow.vn.geometries.viewer3d.Display3DPanel;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.ObjectLargeArray;
import org.visnow.jlargearrays.StringLargeArray;
import org.visnow.jscic.dataarrays.ComplexDataArray;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.ObjectDataArray;
import org.visnow.vn.lib.basic.mappers.TextGlyphs.Params;
import org.visnow.vn.lib.utils.field.subset.DownsizeSelect;
import org.visnow.vn.lib.utils.field.subset.MaskSelector;
import org.visnow.vn.lib.utils.field.subset.NodeSelector;
import org.visnow.vn.lib.utils.field.subset.ThresholdMaskSelector;
import org.visnow.vn.lib.utils.field.subset.ThresholdSelector;
import org.visnow.vn.lib.utils.field.subset.subvolume.TrivialSelector;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class GlyphsObject extends DataMappedGeometryObject
{
    private Texts2D textGlyphs;
    private Params params;
    private FontParams fontParams;
    private Field inField = null;
    private DataArray glyphDataArray;
    private NodeSelector nodeSelector = new NodeSelector();
    private OpenBranchGroup outGroup = null;
    private float[] baseCoords = null;
    private long[] glyphIn = null;
    private int nGlyphs = 0;

    private String defaultTextFormat;

    public GlyphsObject()
    {
        name = "text glyphs";
    }
    
    public void clear()
    {
        if (textGlyphs != null)
            textGlyphs.clear();
        if (outGroup != null)
            outGroup.removeAllChildren();
    }

    public void setNodeSelector(NodeSelector nodeSelector)
    {
        this.nodeSelector = nodeSelector;
    }
    
    @Override
    public void drawLocal2D(J3DGraphics2D vGraphics, LocalToWindow ltw, int width, int height)
    {
        if (textGlyphs == null || ltw == null || params == null || params.getFontParams().isThreeDimensional())
            return;
        textGlyphs.draw(vGraphics, ltw, width, height);
    }

    private void updateCoords()
    {
        if (nGlyphs <= 0)
            return;
        if (baseCoords == null ||
            baseCoords.length != 3 * nGlyphs)
            baseCoords = new float[3 * nGlyphs];
        for (int i = 0; i < baseCoords.length; i++)
            baseCoords[i] = 0;
        if (inField.getCurrentCoords() != null) {
            FloatLargeArray inCoords = inField.getCurrentCoords();
            float[] p = new float[3];
            for (int i = 0; i < nGlyphs; i++)
                System.arraycopy(inCoords.getFloatData(p, 3 * glyphIn[i], 3 * glyphIn[i] + 3, 1), 0, baseCoords, 3 * i, 3);
        } else if (inField instanceof RegularField) {
            float[][] inAff = ((RegularField) inField).getAffine();
            int[] dims = ((RegularField) inField).getDims();
            long i0 = 0, i1 = 0, i2 = 0;
            for (int i = 0; i < nGlyphs; i++) {
                long j = glyphIn[i];
                i0 = j % dims[0];
                if (dims.length > 1) {
                    j /= dims[0];
                    i1 = j % dims[1];
                    if (dims.length > 2)
                        i2 = j / dims[1];
                }
                for (int k = 0; k < 3; k++)
                    baseCoords[3 * i + k] = inAff[3][k] + i0 * inAff[0][k] + i1 * inAff[1][k] + i2 * inAff[2][k];
            }
        }
    }

    private void prepareGlyphCount()
    {
        nGlyphs = 0;
        glyphDataArray = inField.getComponent(params.getComponent());
        if (glyphDataArray == null)
            return;
        NodeSelector nodeSelector = new NodeSelector();
        DataArray thresholdDataArray = inField.getComponent(params.getValidComponentRange().getComponentName());
        if (thresholdDataArray != null && inField.hasMask())
            nodeSelector = new ThresholdMaskSelector(inField.getCurrentMask(), 
                                                     thresholdDataArray, 
                                                     params.getValidComponentRange().getLow(), 
                                                     params.getValidComponentRange().getUp(),
                                                     params.isInsideRange());
        else if (thresholdDataArray != null)
            nodeSelector = new ThresholdSelector(thresholdDataArray, 
                                                 params.getValidComponentRange().getLow(), 
                                                 params.getValidComponentRange().getUp(),
                                                 params.isInsideRange());
        else if (inField.hasMask())
            nodeSelector = new MaskSelector(inField.getCurrentMask());
        
        int maxNGlyphs = 1;
        if (inField instanceof RegularField) {
            int[] low = params.getLowCrop();
            int[] up = params.getUpCrop();
            int[] down = params.getDown();
            for (int i = 0; i < ((RegularField) inField).getDimNum(); i++)
                maxNGlyphs *= (up[i] - low[i]) / down[i] + 1;
        } else
            maxNGlyphs = (int)params.getDownsizeParams().getPreferredSize();
        glyphIn = DownsizeSelect.select(inField, params.getDownsizeParams().isRandom(), maxNGlyphs, 
                                        nodeSelector, params.getLowCrop(), params.getUpCrop(), 
                                        params.getDown(), true);
        nGlyphs = glyphIn.length;
    }

    public void prepareGlyphs()
    {
        if (localToWindow == null)
            localToWindow = getCurrentViewer().getLocToWin();
        if (getCurrentViewer() == null || localToWindow == null || fontParams == null || inField == null)
            return;
        fontParams.createFontMetrics(localToWindow,
                                     getCurrentViewer().getWidth(),
                                     getCurrentViewer().getHeight());
        glyphDataArray = inField.getComponent(params.getComponent());
        if (glyphDataArray == null || nGlyphs < 1)
            return;
        int vlen = glyphDataArray.getVectorLength();
        String format = params.getFormat();
        if (outGroup != null)
            outGroup.detach();
        outGroup = null;
        String[][] texts = new String[nGlyphs][];
        switch (glyphDataArray.getType()) {
        case FIELD_DATA_STRING:
            StringLargeArray sData = ((StringDataArray)glyphDataArray).getRawArray();
            for (int i = 0; i < texts.length; i++) {
                texts[i] = new String[vlen];
                LargeArrayUtils.arraycopy(sData, vlen * glyphIn[i], texts[i], 0, vlen);
            }
            break;
        case FIELD_DATA_OBJECT:
            ObjectLargeArray objData = ((ObjectDataArray)glyphDataArray).getRawArray();
            for (int i = 0; i < texts.length; i++) {
                texts[i] = new String[vlen];
                for (int j = 0; j < vlen; j++) 
                    texts[i][j] = objData.get(glyphIn[i]).toString();
            }
            break;
        case FIELD_DATA_COMPLEX:
            texts = new String[nGlyphs][1];
            ComplexDataArray cDataArray = (ComplexDataArray)glyphDataArray;
            float[][] cData;
            for (int i = 0; i < texts.length; i++) {
                cData = cDataArray.getComplexFloatElement(i);
                if (format.endsWith("d")) {
                    StringBuilder b = new StringBuilder();
                    if (cData.length > 1)
                        b.append("[");
                    for (int j = 0; j < cData.length; j++) {
                        if (j > 0)
                            b.append(",");
                        b.append(String.format(format + "+" + format + "i", (int)cData[j][0], (int)cData[j][1]));
                    }
                    if (cData.length > 1)
                        b.append("]");
                    texts[i][0] = b.toString();
                }
                else{
                    StringBuilder b = new StringBuilder();
                    if (cData.length > 1)
                        b.append("[");
                    for (int j = 0; j < cData.length; j++) {
                        if (j > 0)
                            b.append(",");
                        b.append(String.format(format + "+" + format + "i", cData[j][0], cData[j][1]));
                    }
                    if (cData.length > 1)
                        b.append("]");
                    texts[i][0] = b.toString();
                }
            }
            break;
        default:             // real numeric data
            texts = new String[nGlyphs][1];
            format = format.trim();
            float[] fData;
            for (int i = 0; i < texts.length; i++) {
                fData = glyphDataArray.getFloatElement(glyphIn[i]);
                if (format.endsWith("d")) {
                    StringBuilder b = new StringBuilder();
                    if (fData.length > 1)
                        b.append("[");
                    for (int j = 0; j < fData.length; j++) {
                        if (j > 0)
                            b.append(",");
                        b.append(String.format(format, (int)fData[j]));
                    }
                    if (fData.length > 1)
                        b.append("]");
                    texts[i][0] = b.toString();
                }
                else{
                    StringBuilder b = new StringBuilder();
                    if (fData.length > 1)
                        b.append("[");
                    for (int j = 0; j < fData.length; j++) {
                        if (j > 0)
                            b.append(",");
                        b.append(String.format(format, fData[j]));
                    }
                    if (fData.length > 1)
                        b.append("]");
                    texts[i][0]= b.toString();
                }
            }
        }
        if (fontParams.isThreeDimensional()) {
            textGlyphs = null;
            String[] txts = new String[texts.length];
            for (int i = 0; i < txts.length; i++) 
                if (texts[i] != null && texts[i].length > 0)
                    txts[i] = texts[i][0];
                else
                    txts[i] = " ";
            outGroup = new Texts3D(baseCoords, txts, fontParams);
            addNode(outGroup);
            setExtents(inField.getPreferredExtents());
        } else {
            textGlyphs = new Texts2D(baseCoords, texts, fontParams);
            geometryObj.getCurrentViewer().refresh();
        }
    }


    public void update(Field inField, Params params)
    {
        this.inField = inField;
        if (params == null)
            return;
        fontParams = params.getFontParams();
        if (inField == null) {
            glyphIn = new long[0];
            baseCoords = new float[0];
            outGroup = new Texts3D(baseCoords, new String[0], fontParams);
            textGlyphs = new Texts2D(baseCoords, new String[0][0], fontParams);
            return;
        }
        this.params = params;
        if (defaultTextFormat == null)
            defaultTextFormat = params.getFormat();
        if (params.getChange() == Params.COUNT_CHANGED)
            prepareGlyphCount();
        if (nGlyphs < 1) {
            if (outGroup != null)
                outGroup.detach();
            outGroup = null;
            String[][] texts = new String[0][1];
            textGlyphs = new Texts2D(baseCoords, texts, fontParams);
            geometryObj.getCurrentViewer().refresh();
            return;
        }
        if (params.getChange() == Params.THRESHOLD_CHANGED) 
            prepareGlyphCount();
        if (params.getChange() >= Params.GLYPHS_CHANGED) {
            updateCoords();
            prepareGlyphs();
        }
        params.setChange(0);
    }

    @Override
    public void setCurrentViewer(Display3DPanel panel)
    {
        super.setCurrentViewer(panel);
        prepareGlyphs();
    }
}
