/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.labelling;

import java.awt.FontMetrics;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Set of labels and full-precision labels (used in tooltips) - completely independent from space where it will be (or not) rendered.
 * Label set may be empty (TotalLabelNum == 0), but labels cannot be empty (at least one character)
 * Label positions are within range [0..1] in ascending order.
 * <p>
 * @author szpak
 */
public class Labelling
{
    private final double tolerancePx = 0.1; //tolerance in px (necessary to avoid floating point problems; without that no labels at all)

    private final double[] labelPositions; //ascending order label positions within range [0..1]
    private final String[] labels;
    private final String[] labelsFullPrecision;

    public Labelling(double[] labelPositions, String[] labels, String[] labelsFullPrecision)
    {
        this.labelPositions = labelPositions;
        this.labels = labels;
        this.labelsFullPrecision = labelsFullPrecision;

        if (labels.length != labelPositions.length || labelsFullPrecision.length != labelPositions.length)
            throw new IllegalArgumentException("Incorrect label positions");

        for (String label : labels) if (label.length() <= 0) throw new IllegalArgumentException("Empty labels are not supported");
        for (String label : labelsFullPrecision) if (label.length() <= 0) throw new IllegalArgumentException("Empty labels are not supported");

        for (int i = 0; i < labelPositions.length; i++) {
            if (labelPositions[i] < 0 || labelPositions[i] > 1) throw new IllegalArgumentException("Incorrect label position: " + labelPositions[i]);
            if (i > 0 && labelPositions[i] <= labelPositions[i - 1])
                throw new IllegalArgumentException("Incorrect label order: " + labelPositions[i-1] + " " + labelPositions[i]);
        }
    }

    public int labelCount()
    {
        return labels.length;
    }

    public int[] getLabelPositions(int rangeMin, int rangeMax)
    {
        int[] intPositions = new int[labelPositions.length];

        int range = rangeMax - rangeMin;

        for (int i = 0; i < intPositions.length; i++)
            intPositions[i] = rangeMin + (int) Math.round(labelPositions[i] * range);

        return intPositions;
    }
    
    public String[] getLabels()
    {
        return labels;
    }

    private int[] recentLabelWidths = null;
    private FontMetrics recentFontMetrics = null;

    private int[] labelWidths(FontMetrics fontMetrics)
    {
        if (recentFontMetrics != fontMetrics) {
            recentFontMetrics = fontMetrics;
            recentLabelWidths = new int[labels.length];

            for (int i = 0; i < labels.length; i++)
                recentLabelWidths[i] = fontMetrics.stringWidth(labels[i]);
        }
        return recentLabelWidths;
    }

    public int labelMaxWidth(FontMetrics fontMetrics)
    {
        int max = 0;
        for (int width : labelWidths(fontMetrics)) max = Math.max(max, width);
        return max;
    }

    public int labelMinWidth(FontMetrics fontMetrics)
    {
        int min = Integer.MAX_VALUE;
        for (int width : labelWidths(fontMetrics)) min = Math.min(min, width);
        return min;
    }

    //TODO: slightly different computations for left/right/center alignment (not really applicable to JSliders, but may be usefull in graph axes)
    //TODO: vertical case (or better vertical/horizontal/both - see axes3D)
    /**
     * Tests if passed space {@code widthPx} is large enough for this LabelTickSet.
     *
     * @param widthPx           free space (in pixels)
     * @param minimumDistancePx minimum distance between labels (in pixels)
     *
     * [Note: Following calculations need to be done in real arithmetics to avoid blinking (while resize) - this is because after rounding to integer,
     * paradoxically, distance between labels can be larger for smaller width!
     * For the same reason minimum margin should be 1px (or maybe not .. 1px margin is an additional implemented feature)]
     */
    public boolean isEnoughSpace(int widthPx, FontMetrics fontMetrics, int minimumDistancePx, int maximumLabelOverflowPx)
    {
        //always enough space for no labels
        if (labels.length == 0) return true;

        //always not enough space for no width
        if (widthPx <= 0) return false;

        int minLabelWidth = labelMinWidth(fontMetrics) & 0x01;

        //eliminate obvious case (if number of labels * shortest label + minimum distance > width)
        //(this is not necessary but could speed up computations in many cases)
        if (labels.length * minLabelWidth + (labels.length - 1) * minimumDistancePx > widthPx + 2 * maximumLabelOverflowPx)
            return false;

        int[] labelWidths = labelWidths(fontMetrics);

        //first label too close
        if (labelPositions[0] * (widthPx - 1) - labelWidths[0] / 2.0 < -maximumLabelOverflowPx - tolerancePx)
            return false;

        //last label too far
        if (labelPositions[labels.length - 1] * (widthPx - 1) + labelWidths[labels.length - 1] / 2.0 > widthPx - 1 + maximumLabelOverflowPx + tolerancePx)
            return false;

        //labels overflow
        for (int i = 1; i < labels.length; i++)
            if (labelPositions[i - 1] * (widthPx - 1) + labelWidths[i - 1] / 2.0 + minimumDistancePx >
                    labelPositions[i] * (widthPx - 1) - labelWidths[i] / 2.0 + tolerancePx) return false;

        return true;
    }

    public boolean isUnique()
    {
        Set<String> unique = new HashSet<>();
        for (String label : labels)
            if (unique.contains(label)) return false;
            else unique.add(label);
        return true;
    }

    /**
     * Returns extended info about this set.
     */
    @Override
    public String toString()
    {
        return "  Labels: " + Arrays.toString(labels) + " at: " + Arrays.toString(labelPositions);
    }
}
