/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.slicing;

import java.util.Arrays;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import static org.jogamp.java3d.GeometryArray.ALLOW_COLOR_WRITE;
import static org.jogamp.java3d.GeometryArray.ALLOW_COORDINATE_WRITE;
import static org.jogamp.java3d.GeometryArray.ALLOW_NORMAL_WRITE;
import static org.jogamp.java3d.GeometryArray.COLOR_3;
import static org.jogamp.java3d.GeometryArray.COORDINATES;
import static org.jogamp.java3d.GeometryArray.NORMALS;
import static org.jogamp.java3d.GeometryArray.USE_COORD_INDEX_ONLY;
import org.jogamp.java3d.IndexedLineStripArray;
import org.jogamp.java3d.IndexedQuadArray;
import org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.FieldUtils;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;
import org.visnow.vn.geometries.parameters.ComponentColorMap;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.utils.ColorMapper;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceGUI;
import org.visnow.vn.lib.utils.field.IndexSlice2D;
import org.visnow.vn.lib.utils.probeInterfaces.Probe;

/**
 *
 * @author know
 */


public class IndexProbe2D extends Probe{
    
    protected RegularField inField = null;
    protected IndexSliceParams params = new IndexSliceParams();
    protected float[] position = params.getPosition();
    protected IndexSliceGUI gui = new IndexSliceGUI(IndexSliceParams.Type.PLANE);
    protected float[] sliceCrds;
    protected long[] lDims;
    protected int[] dims;
    
    protected int[] outDims = {-1, -1};
    protected int nOutNodes = 1;
    
    protected int nData;
    protected int[] vlens;
    
    protected float fSlice;
    protected IrregularField sliceArea = null;
    protected RegularField regularSliceArea = null;
    protected float[] sliceCoords;
    protected float[] planeCenter = new float[3]; 
    protected float[] planeX = new float[3]; 
    protected float[] planeY = new float[3];
            
    protected OpenBranchGroup parent = new OpenBranchGroup();

    protected ComponentColorMap cMap;
    // glyph used for interactive display (continuously updated when position slider is adjusting)
    protected OpenBranchGroup  glyphGroup = new OpenBranchGroup();
    protected OpenShape3D sliceLinesShape = new OpenShape3D();
    protected OpenShape3D sliceQuadsShape = new OpenShape3D();
    protected IndexedLineStripArray sliceLines;
    protected IndexedQuadArray sliceQuads;
    protected OpenBranchGroup glyphParentGroup = new OpenBranchGroup();
    
    protected boolean working;
    
    protected int[] low = new int[2], up = new int[2];
    protected float[] indexCoords;
    
    protected float currentTime = 0;
    
    protected RenderEventListener mapEventListener = new RenderEventListener()
    {
        @Override
        public void renderExtentChanged(RenderEvent e)
        {
            updateSliceGlyphData();
        }
    };
    
    public IndexProbe2D()
    {
        params.setType(IndexSliceParams.Type.PLANE);
        params.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (inField == null)
                    return;
                if (params.isGlyphVisible())
                    show();
                else
                    hide();
                regularSliceArea = IndexSlice2D.slice(inField, params);
                updateGlyph();
                fireStateChanged(params.isAdjusting());
            }
        });
        gui.setParams(params);
        glyphGroup.setName("probeGroup");
        parent.setName("probeParentGroup");
    }
    
    
    @Override
    public void setDataMappingParams(DataMappingParams dataMappingParams)
    {
        cMap = dataMappingParams.getColorMap0();
        dataMappingParams.addRenderEventListener(mapEventListener);
    }
    
    @Override
    public void setInData(Field field, DataMappingParams dataMappingParams)
    {
        if (!(field instanceof RegularField))
            return;
        setDataMappingParams(dataMappingParams);
        RegularField in = (RegularField)field;
        if (in.getDimNum() != 3)
            return;
        inField = in;
        params.setActive(false);
        params.setFieldSchema(inField.getSchema());
        params.setActive(true);
        regularSliceArea = IndexSlice2D.slice(inField, params);
        updateGlyph();
    }
    
    private void updateSliceGlyphStructure()
    {
        glyphGroup.detach();
        glyphGroup.removeAllChildren();
        nOutNodes = (int)regularSliceArea.getNNodes();
        int[] lineIndices = new int[2 * nOutNodes];
        int[] lineStrips = new int[outDims[0] + outDims[1]];
        int k = 0, l = 0;
        for (int i = 0; i < outDims[1]; i++, k++) {
            lineStrips[k] = outDims[0];
            for (int j = 0; j < outDims[0]; j++, l++)
                lineIndices[l] = i * outDims[0] + j;
        }
        for (int i = 0; i < outDims[0]; i++, k++) {
            lineStrips[k] = outDims[1];
            for (int j = 0; j < outDims[1]; j++, l++)
                lineIndices[l] = j * outDims[0] + i;
        }
        sliceLines = new IndexedLineStripArray(nOutNodes,
                             COORDINATES | COLOR_3 | USE_COORD_INDEX_ONLY,
                             2 * nOutNodes,
                             lineStrips);
        sliceLines.setCoordinateIndices(0, lineIndices);
        sliceLines.setCapability(ALLOW_COLOR_WRITE);
        sliceLines.setCapability(ALLOW_COORDINATE_WRITE);
        l = 0;
        int nQuads = (up[0] - low[0] - 1) * (up[1] - low[1] - 1);
        int[] quadIndices = new int[4 * nQuads];
        for (int i = low[1]; i < up[1] - 1; i++) 
            for (int j = low[0]; j < up[0] - 1; j++, l += 4) {
                quadIndices[l]     = i *       outDims[0] + j;
                quadIndices[l + 1] = i *       outDims[0] + j + 1;
                quadIndices[l + 2] = (i + 1) * outDims[0] + j + 1;
                quadIndices[l + 3] = (i + 1) * outDims[0] + j;
            }
        sliceQuads = new IndexedQuadArray(nOutNodes,
                                          COORDINATES | NORMALS |COLOR_3 | USE_COORD_INDEX_ONLY,
                                          4 * nQuads);
        sliceQuads.setCoordinateIndices(0, quadIndices);
        sliceQuads.setCapability(ALLOW_COLOR_WRITE);
        sliceQuads.setCapability(ALLOW_COORDINATE_WRITE);
        sliceQuads.setCapability(ALLOW_NORMAL_WRITE);
        sliceLinesShape = new OpenShape3D();
        sliceLinesShape.addGeometry(sliceLines);
        sliceQuadsShape = new OpenShape3D();
        sliceQuadsShape.setAppearance(new OpenAppearance());
        sliceQuadsShape.addGeometry(sliceQuads);
        
        glyphGroup.addChild(sliceLinesShape);
        glyphGroup.addChild(sliceQuadsShape);
        updateSliceGlyphData();
        parent.addChild(glyphGroup);
    }
    
    public void setCurrentTime(float time)
    {
        currentTime = time;
        updateSliceGlyphData();
    }
    
    private void updateSliceGlyphData()
    {
        if (regularSliceArea == null)
            return;
        if (regularSliceArea.hasCoords()) 
            sliceCoords = regularSliceArea.getCurrentCoords().getData();
        else 
            sliceCoords = regularSliceArea.getCoordsFromAffine().getData();
        if (sliceLines == null)
            updateSliceGlyphStructure();
        sliceLines.setCoordinates(0, sliceCoords);
        float[] normals = new float[3 * nOutNodes];
        for (int i = 0, l = 0; i < outDims[1]; i++) 
            for (int j = 0; j < outDims[0]; j++) {
                int i0 = Math.max(0, i - 1);
                int i1 = Math.min(outDims[1] - 1, i + 1);
                int j0 = Math.max(0, j - 1);
                int j1 = Math.min(outDims[0] - 1, j + 1);
                float[] u = new float[3];
                float[] v = new float[3];
                for (int k = 0; k < 3; k++) {
                    u[k] = sliceCoords[3 * (i * outDims[0] + j1) + k] - sliceCoords[3 * (i * outDims[0] + j0) + k];
                    v[k] = sliceCoords[3 * (i1 * outDims[0] + j) + k] - sliceCoords[3 * (i0 * outDims[0] + j) + k];
                }
                float[] w = new float[] {u[1] * v[2] - u[2] * v[1], u[2] * v[0] - u[0] * v[2],  u[0] * v[1] - u[1] * v[0]};
                float vNorm = (float)(Math.sqrt(w[0] * w[0] + w[1] * w[1] + w[2] * w[2]));
                
                for (int k = 0; k < 3; k++, l++) 
                    normals[l] = w[k] / vNorm;
            }
        sliceQuads.setCoordinates(0, sliceCoords);
        sliceQuads.setNormals(0, normals);
        
        DataArray data = regularSliceArea.getComponent(cMap.getDataComponentName());
        if (data == null || !data.isNumeric())
            return;
        int veclen = data.getVectorLength();
        float[] outColorData = data.getRawFloatArray().getData();
        float[] v = new float[veclen];
        byte[] colors = new byte[3  * outDims[1] * outDims[0]];
        for (int i = 0; i < outDims[1] * outDims[0]; i++) {
            System.arraycopy(outColorData, i * veclen, v, 0, veclen);
            byte[] c = ColorMapper.mapByteColor(v, cMap);
            System.arraycopy(c, 0, colors, 3 * i, 3);
        }
        sliceLines.setColors(0, colors);
        sliceQuads.setColors(0, colors);
        createOutputGeometryData();
    }
    
    private void createOutputGeometryData()
    {
        if (inField.hasCoords()) {
            Arrays.fill(planeCenter, 0);
            Arrays.fill(planeX, 0);
            Arrays.fill(planeY, 0);
            for (int i = 0; i < nOutNodes; i++) 
                for (int j = 0; j < 3; j++) 
                    planeCenter[j] += sliceCoords[3 * i + j];
            for (int j = 0; j < 3; j++) {
                planeCenter[j] /= nOutNodes;
                planeX[j] = sliceCoords[3 * (outDims[0] - 1) + j]              - sliceCoords[j];
                planeY[j] = sliceCoords[3 * (outDims[1] - 1) * outDims[0] + j] - sliceCoords[j];
            }
        }
        else {
            float[][] affine = inField.getAffine();
            float[][] sliceAffine = new float[4][3];
            for (int i = 0; i < 3; i++) {
                sliceAffine[3][i] = affine[3][i];
                for (int j = 0, k = 0; j < 3; j++) 
                    if (params.getFixed()[j]) {
                        sliceAffine[3][i] += position[j] * affine[j][i];
                    }
                    else {
                        sliceAffine[3][i] += params.getLow()[j] * affine[j][i];
                        sliceAffine[k][i] = affine[j][i];
                        k += 1;
                    }
            }
            for (int j = 0; j < 3; j++) {
                planeCenter[j] = sliceAffine[3][j] + .5f * (outDims[0] - 1) * sliceAffine[0][j] + 
                                                     .5f * (outDims[1] - 1) * sliceAffine[1][j];
                planeX[j] = sliceAffine[0][j];
                planeY[j] = sliceAffine[1][j];
            }
            regularSliceArea.setAffine(sliceAffine);
            regularSliceArea.setCoords(regularSliceArea.getCoordsFromAffine(), 0);
        }
        float xNorm = 0, yNorm = 0, sp = 0;
        for (int j = 0; j < 3; j++) {
            xNorm += planeX[j] * planeX[j];
            sp    += planeX[j] * planeY[j];
        }
        xNorm = (float)Math.sqrt(xNorm);
        sp /= xNorm;
        for (int j = 0; j < 3; j++) {
            planeX[j] /= xNorm;
            planeY[j] -= sp * planeX[j];
            yNorm += planeY[j] * planeY[j];
        }
        yNorm = (float)Math.sqrt(yNorm);
        for (int j = 0; j < 3; j++)
            planeY[j] /= yNorm;
    }
    
    public void updateGlyph()
    {
        if (regularSliceArea == null)
            return;
        if (outDims[0] != regularSliceArea.getDims()[0] ||
            outDims[1] != regularSliceArea.getDims()[1])
        {
            outDims = regularSliceArea.getDims();
            updateSliceGlyphStructure();
        }
        updateSliceGlyphData();
    }
    
    public OpenBranchGroup getGlyph()
    {
        return parent;
    }
    
    @Override
    public void hide()
    {
        if (glyphGroup != null) 
            glyphGroup.detach();
    }
    
    @Override
    public void show()
    {
        if (glyphGroup.getParent() == null)
            parent.addChild(glyphGroup);
    }

    @Override
    public float[] getPlaneCenter()
    {
        return planeCenter;
    }

    @Override
    public float[] getPlaneX()
    {
        return planeX;
    }

    @Override
    public float[] getPlaneY()
    {
        return planeY;
    }
    
    @Override
    public OpenBranchGroup getGlyphGeometry()
    {
        return parent;
    }

    @Override
    public JPanel getGlyphGUI()
    {
        return gui;
    }

    @Override
    public IrregularField getSliceField()
    {
        return FieldUtils.convertToIrregular(regularSliceArea);
    }
    
    @Override
    public IrregularField getSliceAreaField()
    {
        return FieldUtils.convertToIrregular(regularSliceArea);
    }
    
    @Override
    public RegularField getRegularSliceAreaField()
    {
        return regularSliceArea;
    }
    
    @Override
    public RegularField getRegularSliceField()
    {
        return regularSliceArea;
    }
}
