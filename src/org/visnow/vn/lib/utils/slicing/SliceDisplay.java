/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.slicing;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Vector;
import org.jogamp.java3d.GeometryArray;
import static org.jogamp.java3d.GeometryArray.ALLOW_COLOR_WRITE;
import static org.jogamp.java3d.GeometryArray.ALLOW_COORDINATE_WRITE;
import static org.jogamp.java3d.GeometryArray.COLOR_3;
import static org.jogamp.java3d.GeometryArray.COORDINATES;
import org.jogamp.java3d.IndexedGeometryArray;
import org.jogamp.java3d.IndexedLineArray;
import org.jogamp.java3d.IndexedLineStripArray;
import org.jogamp.java3d.J3DGraphics2D;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.FieldUtils;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.utils.ColorMapper;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import org.visnow.vn.lib.utils.interpolation.FieldPosition;
import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay;
import org.visnow.vn.lib.utils.interpolation.RegularInterpolation;
import static org.visnow.vn.lib.utils.interpolation.RegularInterpolation.*;
import static org.visnow.vn.lib.utils.interpolation.SubsetGeometryComponents.*;
import org.visnow.vn.lib.utils.probeInterfaces.NumericDisplay;
import org.visnow.vn.system.main.VisNow;


/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */


public class SliceDisplay extends ProbeDisplay
{
    protected final OpenAppearance lineAppearance     = new OpenAppearance();
    protected final OpenAppearance lineAppearance1    = new OpenAppearance();
    protected final float[] planeCenter;
    protected final float[] planeX;
    protected final float[] planeY;
    protected final float rX, rY;
    protected RegularField regularFld = null;
    
    protected int resolution = 300;
    protected float time = 0;
    
    protected IrregularField planarSlice;
    protected FieldPosition[] sliceImageInterpolationData;
    protected int[] imDims = {100, 100};
    protected BufferedImage sliceImage;
    
    protected final OpenShape3D sliceRect         = new OpenShape3D();
    protected final OpenShape3D sliceRect1        = new OpenShape3D();
    protected final OpenBranchGroup sliceGroup    = new OpenBranchGroup();
    protected IndexedGeometryArray sliceLines     = null;
    protected IndexedGeometryArray sliceLines1    = null;
    protected DataMappingParams dataMappingParams = null;
    /**
     * Creates new slice display (slice rectangle in 3D and the image data for 2D picture)
     * @param fld          planar slice field
     * @param planeCenter  origin of the slice (from the interactive glyph center point)
     * @param planeX       versor defining the slice plane (from the interactive glyph)
     * @param planeY       versor defining the slice plane (from the interactive glyph)
     * @param resolution   resolution of the slice image
     */
    public SliceDisplay(Field fld, DataMappingParams dataMappingParams,
                        float[] planeCenter, float[] planeX, float[] planeY,
                        float rX, float rY, int resolution)
    {
        if (fld instanceof IrregularField) 
            this.fld = (IrregularField)fld;
        else {
            regularFld = (RegularField)fld; 
            this.fld = FieldUtils.convertToIrregular(regularFld);
        }
        this.dataMappingParams = dataMappingParams;
        crds = this.fld.getCurrentCoords().getData();
        cornerCoords = new float[12];
        this.planeCenter = planeCenter;
        this.planeX = planeX;
        this.planeY = planeY;
        this.rX = rX;
        this.rY = rY;
        this.resolution = resolution;
        int nOut = (int)Math.min(.5e9,resolution * resolution);
        double vol = rX * rY;
        float delta = (float) Math.sqrt(vol / nOut);
        imDims[0] = (int) (rX / delta) + 1;
        imDims[1] = (int) (rY / delta) + 1;
        planarSlice = computePlaneSlice(this.fld, planeCenter, planeX, planeY);
        sliceImageInterpolationData = interpolatePlanarSlice(planarSlice, resolution);
        updateImage();
        computeSliceBoundary();
    }
    

    /**
     * Creates new slice display (slice rectangle in 3D and the image data for 2D picture)
     * @param fld          planar slice field
     * @param planeCenter  origin of the slice (from the interactive glyph center point)
     * @param planeX       versor defining the slice plane (from the interactive glyph)
     * @param planeY       versor defining the slice plane (from the interactive glyph)
     * @param resolution   resolution of the slice image
     */
    public SliceDisplay(Field fld, DataMappingParams dataMappingParams, float[] planeCenter, float[] planeX, float[] planeY, int resolution)
    {
        this(fld, dataMappingParams, planeCenter, planeX, planeY,
                        Float.MAX_VALUE, Float.MAX_VALUE, resolution);
    }
    
    private void computeSliceBoundary()
    {
        float[] colors = {1, 0, 0, 0, 0, 1};
        lineAppearance.getLineAttributes().setLineWidth(2);
        lineAppearance1.getLineAttributes().setLineWidth(.5f);
        if (regularFld == null || !regularFld.hasCoords()) {
            sliceLines  = new IndexedLineArray(4,  GeometryArray.COORDINATES | GeometryArray.COLOR_3, 4);
            sliceLines1 = new IndexedLineArray(4,  GeometryArray.COORDINATES | GeometryArray.COLOR_3, 4);
            sliceLines.setCoordinateIndices(0,  new int[] {0, 1, 0, 2});
            sliceLines1.setCoordinateIndices(0, new int[] {3, 1, 3, 2});
            sliceLines.setColorIndices(0, new int[] {0, 0, 1, 1});
            sliceLines1.setColorIndices(0, new int[] {1, 1, 0, 0});
            sliceLines.setCoordinates(0, cornerCoords);
            sliceLines1.setCoordinates(0, cornerCoords);
            sliceLines.setColors(0, colors);
            sliceLines1.setColors(0, colors);
            sliceRect.addGeometry(sliceLines);
            sliceRect1.addGeometry(sliceLines1);
            sliceRect.setAppearance(lineAppearance);
            sliceRect1.setAppearance(lineAppearance1);
            sliceGroup.addChild(sliceRect);
            sliceGroup.addChild(sliceRect1);
        }
        else {
            int[] dims = regularFld.getDims();
            int n = dims[0] + dims[1] - 1;
            float[] borderCrds  = new float[3 * n];
            float[] borderCrds1 = new float[3 * n];
            int[] coordIndices  = new int[n];
            int[] coordIndices1  = new int[n];
            int[] colorIndices  = new int[n];
            int[] colorIndices1  = new int[n];
            for (int i = 0; i < dims[0]; i++) 
                colorIndices[i] = colorIndices1[i] = 0;
            for (int i = dims[0]; i < n; i++) 
                colorIndices[i] = colorIndices1[i] = 1;
            for (int i = 0; i < n; i++) { 
                coordIndices[i] = i;
                coordIndices1[i] = i;
            }
            float[] fldCrds = regularFld.getCurrentCoords().getData();
            for (int i = 0, l = 0, j = 3 * (dims[0] - 1), k = 3 * (dims[1] - 1) * dims[0]; 
                     i < dims[0]; 
                     i++, j -= 3, k += 3, l += 3) {
                System.arraycopy(fldCrds, j, borderCrds,  l, 3);
                System.arraycopy(fldCrds, k, borderCrds1, l, 3);
            }
            
            for (int i = 1, j = 3 * dims[0], k = 3 * ((dims[1] - 1) * dims[0] - 1), l = 3 * dims[0]; 
                     i < dims[1]; i++, j += 3 * dims[0], k -= 3 * dims[0], l += 3) {
                System.arraycopy(fldCrds, j, borderCrds,  l, 3);
                System.arraycopy(fldCrds, k, borderCrds1, l, 3);
            }
            
            sliceLines = new IndexedLineStripArray(n, COORDINATES | COLOR_3, n, new int[] {n});
            sliceLines1 = new IndexedLineStripArray(n, COORDINATES | COLOR_3, n, new int[] {n});
            sliceLines.setCoordinateIndices(0, coordIndices);
            sliceLines1.setCoordinateIndices(0, coordIndices1);
            sliceLines.setColorIndices(0, colorIndices);
            sliceLines1.setColorIndices(0, colorIndices1);
            sliceLines.setCapability(ALLOW_COLOR_WRITE);
            sliceLines.setCapability(ALLOW_COORDINATE_WRITE);
            sliceLines.setCoordinates(0, borderCrds);
            sliceLines1.setCoordinates(0, borderCrds1);
            sliceLines.setColors(0, colors);
            sliceLines1.setColors(0, colors);
            sliceRect.addGeometry(sliceLines);
            sliceRect1.addGeometry(sliceLines1);
            sliceRect.setAppearance(lineAppearance);
            sliceRect1.setAppearance(lineAppearance1);
            sliceGroup.addChild(sliceRect);
            sliceGroup.addChild(sliceRect1);
        }
    }
    
    @Override
    public void updateScreenCoords(LocalToWindow ltw)
    {
        int[] screenOrigin = new int[2];
        int[] screenEndU = new int[2];
        int[] screenEndV = new int[2];
        int[] screenEndUV = new int[2];
        float[] corner = new float[3];
        int[] tmp = new int[2];
        
        for (int i = 0; i < 4; i++) {
            System.arraycopy(cornerCoords, 3 * i, corner, 0, 3);
            ltw.transformPt(corner, tmp);
            switch (i) {
                case 0:
                   System.arraycopy(tmp, 0, screenOrigin, 0, 2); 
                   System.arraycopy(tmp, 0, screenHandle, 0, 2); 
                case 1:
                   System.arraycopy(tmp, 0, screenEndU, 0, 2); 
                case 2:
                   System.arraycopy(tmp, 0, screenEndV, 0, 2); 
                case 3:
                   System.arraycopy(tmp, 0, screenEndUV, 0, 2); 
            }
            switch (probesPosition) {
            case TOP:
            case AT_POINT:
                if (tmp[1] < screenHandle[1])
                    System.arraycopy(tmp, 0, screenHandle, 0, 2);
                break;
            case BOTTOM:
                if (tmp[1] > screenHandle[1])
                    System.arraycopy(tmp, 0, screenHandle, 0, 2);
                break;
            case RIGHT:
                if (tmp[0] > screenHandle[0])
                    System.arraycopy(tmp, 0, screenHandle, 0, 2);
                break;
            case LEFT:
                if (tmp[0] < screenHandle[0])
                    System.arraycopy(tmp, 0, screenHandle, 0, 2);
                break;
            }
        }
    }
    
    private IrregularField computePlaneSlice(IrregularField sliceField, 
                                             float[] planeCenter, 
                                             float[] planeX, float[] planeY)
    {            
        if (sliceField == null)
            return null;
        float xmin = Float.MAX_VALUE;
        float xmax = -Float.MAX_VALUE;
        float ymin = Float.MAX_VALUE;
        float ymax = -Float.MAX_VALUE;
        IrregularField planeSliceField = sliceField.cloneShallow();
        float[] crds = sliceField.getCurrentCoords().getData();
        float[] planeCrds = new float[crds.length];
        for (int i = 0; i < crds.length; i += 3) {
            float u = 0, v = 0;
            for (int j = 0; j < 3; j++) {
                u += planeX[j] * (crds[i + j] - planeCenter[j]);
                v += planeY[j] * (crds[i + j] - planeCenter[j]);
            }
            planeCrds[i]     = u;
            planeCrds[i + 1] = v;
            planeCrds[i + 2] = 0;
            if (xmin > u) xmin = u;
            if (ymin > v) ymin = v;
            if (xmax < u) xmax = u;
            if (ymax < v) ymax = v;
        }
        for (int i = 0; i < 3; i++) {
            cornerCoords[i]     = cornerCoords[i + 3] = 
            cornerCoords[i + 6] = cornerCoords[i + 9] = planeCenter[i];
            cornerCoords[i]     += xmin * planeX[i] + ymin * planeY[i];
            cornerCoords[i + 3] += xmax * planeX[i] + ymin * planeY[i];
            cornerCoords[i + 6] += xmin * planeX[i] + ymax * planeY[i];
            cornerCoords[i + 9] += xmax * planeX[i] + ymax * planeY[i];
        }
        FloatLargeArray planeCoords = new FloatLargeArray(planeCrds);
        planeSliceField.setCoords(planeCoords, 0);
        planeSliceField.checkTrueNSpace();
        for (CellSet cs : planeSliceField.getCellSets()) 
            cs.addGeometryData(planeCoords);
        return planeSliceField;
    }
   
    public static final FieldPosition[] getFieldPositions(Field field, float[][] boxAffine, int[] resolution)
    {
        int[] outDims = new int[resolution.length];
        float[][] outAffine = new float[4][3];
        for (int i = 0; i < 3; i++) {
             outAffine[3][i] = boxAffine[3][i];
             for (int j = 0; j < resolution.length; j++) {
                outDims[j] = resolution[j] + 1;
                outAffine[j][i] =boxAffine[j][i] / resolution[j];
            }
        }
        for (int j = resolution.length; j < 3; j++) 
            outAffine[j][j] = 1;
        return computePositions(field, outDims, outAffine);
    }
    
    
    private FieldPosition[] interpolatePlanarSlice(IrregularField planeSliceField, 
                                                   int avgResolution)
    {
        if (planeSliceField == null || !planeSliceField.hasCells2D())
            return null;
        float[][] planeSliceExtents = planeSliceField.getExtents();
        float[][] boxAffine = new float[4][3];
        for (float[] boxAffine1 : boxAffine) 
            Arrays.fill(boxAffine1, 0);
        System.arraycopy(planeSliceExtents[0], 0, boxAffine[3], 0, 2);
        boxAffine[0][0] = planeSliceExtents[1][0] - planeSliceExtents[0][0];
        boxAffine[1][1] = planeSliceExtents[1][1] - planeSliceExtents[0][1];
        int nOut = (int)Math.min(.5e9, avgResolution * avgResolution);
        double[] r = new double[2];
        int[] resolution = new int[2];
        double vol = 1;
        for (int i = 0; i < 2; i++) {
            r[i] = 0;
            for (int j = 0; j < 2; j++)
                r[i] += boxAffine[i][j] * boxAffine[i][j];
            r[i] = Math.sqrt(r[i]);
            vol *= r[i];
        }
        float delta = (float) Math.sqrt(vol / nOut);
        for (int i = 0; i < 2; i++) 
            resolution[i] = (int) (r[i] / delta) + 1;
        float[][] outAffine = new float[4][3];
        for (int i = 0; i < 3; i++) {
             outAffine[3][i] = boxAffine[3][i];
             for (int j = 0; j < resolution.length; j++) {
                imDims[j] = resolution[j] + 1;
                outAffine[j][i] =boxAffine[j][i] / resolution[j];
            }
        }
        for (int j = resolution.length; j < 2; j++) 
            outAffine[j][j] = 1;
        return RegularInterpolation.computePositions(planeSliceField, imDims, outAffine);
    }
    
    public void updateImage(DataMappingParams dataMappingParams)
    {
        this. dataMappingParams = dataMappingParams;
        updateImage();
    }
    
    private void updateImage()
    {
        DataArray da0  = planarSlice.getComponent(dataMappingParams.
                                                  getColorMap0().
                                                  getDataComponentName());
        DataArray da1  = planarSlice.getComponent(dataMappingParams.
                                                  getColorMap1().
                                                  getDataComponentName());
        DataArray daTr = planarSlice.getComponent(dataMappingParams.
                                                  getTransparencyParams().
                                                  getComponentRange().
                                                  getComponentName());
        float[] imData0 = null, imData1 = null, imDataTr = null;
        int vlen0 = 1, vlen1 = 1, vlenTr = 1;
        byte[] mask = new byte[imDims[0] * imDims[1]];
        for (int i = 0; i < mask.length; i++)
            mask[i] = sliceImageInterpolationData[i] != null ? (byte)1 : (byte)0;
        if (da0 != null) {
            vlen0 = da0.getVectorLength();
            imData0 = FieldPosition.interpolate(sliceImageInterpolationData, 
                                                da0.getTimeData().getValue(time), vlen0).
                                                getFloatData();
        }
        if (da1 != null) {
            vlen1 = da1.getVectorLength();
            imData1 = FieldPosition.interpolate(sliceImageInterpolationData, 
                                                da1.getTimeData().getValue(time), vlen1).
                                                getFloatData();
        }
        if (daTr != null){
            vlenTr = daTr.getVectorLength();
            imDataTr = FieldPosition.interpolate(sliceImageInterpolationData, 
                                                 daTr.getTimeData().getValue(time), vlenTr).
                                                 getFloatData();
        }
        int[] buffer = ColorMapper.mapArraysToInt(imData0, vlen0, imData1, vlen1, imDataTr, vlenTr, mask,
                                                  dataMappingParams, VisNow.availableProcessors(), null);
        int[] t = new int[imDims[0]];
        if (buffer ==  null)
            return;
        for (int i = 0; i < imDims[1] / 2; i++) {                 //image inversion in y coordinate                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
            System.arraycopy(buffer,                   i * imDims[0], 
                             t,                                    0, imDims[0]);
            System.arraycopy(buffer, (imDims[1] - 1 - i) * imDims[0], 
                             buffer,                   i * imDims[0], imDims[0]);
            System.arraycopy(t,                                   0, 
                             buffer, (imDims[1] - 1 - i) * imDims[0], imDims[0]);
        }

        sliceImage = new BufferedImage(imDims[0], imDims[1], 
                                       BufferedImage.TYPE_INT_ARGB);
        sliceImage.setRGB(0, 0, imDims[0], imDims[1], buffer, 0, imDims[0]);
    }
    
    public void setTime(float time) {
        this.time = time;
        updateImage();
    }
    
    public int[] getImageDims()   
    {
        return new int[] {sliceImage.getWidth(), sliceImage.getHeight()};
    }
    
    private int width = 1000, height = 1000;
    private Color bgr = Color.BLACK;
    private int tooltipIx = -1, tooltipIy = -1;

    boolean isDark(Color c)
    {
        float[] v = new float[3];
        c.getRGBColorComponents(v);
        return 0.299 * v[0] + 0.587 * v[1] + 0.114 * v[2] < .5;
    }
    
    public void setBgr(Color bgr) {
        this.bgr = bgr;
    }
    
    public void setTooltipLocation(int ix, int iy)
    {
        tooltipIx = ix;
        tooltipIy = iy;
    }
    
    public boolean pointInside(int ix, int iy)
    {
        return ix >= ixmin && ix <= ixmax &&
               iy >= iymin && iy <= iymax;
    }

    
    public void draw(J3DGraphics2D gr, LocalToWindow ltw, int width, int height, float scale,String title)
    {
    }
    
    public void showTooltip(J3DGraphics2D gr)
    {
        if (!pointInside(tooltipIx, tooltipIy))
            return;
        int ix = (int)((tooltipIx - screenX) * sliceImage.getWidth() / (float)screenW);
        int iy = (int)((screenY - tooltipIy) * sliceImage.getHeight() / (float)screenH);
        FieldPosition fp = sliceImageInterpolationData[iy * sliceImage.getWidth() + ix];
        
        Vector<String> names = new Vector<>(); 
        Vector<float[]> vals = new Vector<>();
        for (int i = 0, l = 0; i < planarSlice.getNComponents(); i++) {
            DataArray component = planarSlice.getComponent(i);
            String componentName = component.getName();
            if (componentName.equalsIgnoreCase(NODE_POSITIONS) ||
                componentName.equalsIgnoreCase(INDEX_COORDS) ||
                componentName.equalsIgnoreCase(NODE_INDICES) &&
                component.isNumeric())
                continue;
            int vLen = component.getVectorLength();
            FloatLargeArray cmpData = component.getRawFloatArray();
            names.add(componentName);
            vals.add(fp.getInterpolatedVal(cmpData, vLen));
        }
        NumericDisplay.displayPointValues(gr, tooltipIx, tooltipIy, 
                                              names, vals, width, height, bgr);
    }
    
    @Override
    public void drawOnMargin(J3DGraphics2D gr, LocalToWindow ltw, int w, int h, 
                             float scale, int index, int ix, int iy, String title)
    {
    }

    public void setResolution(int resolution)
    {
        this.resolution = resolution;
        sliceImageInterpolationData = interpolatePlanarSlice(planarSlice, resolution);
        updateImage();
    }
    
    public OpenBranchGroup getSliceGroup()
    {
        return sliceGroup;
    }
    
    public void draw(J3DGraphics2D gr, LocalToWindow ltw, 
                     int x, int y, int w, int h, 
                     int fontSize, String index)
    {
        if (sliceImage == null)
            return;
        screenX = x;
        screenY = y;
        screenW = w;
        screenH = h;
        gr.setFont(new java.awt.Font("Dialog", 0, fontSize));
        gr.setColor(isDark(bgr) ? Color.LIGHT_GRAY : Color.DARK_GRAY);
        gr.setStroke(new BasicStroke(2));
        if (title != null && !title.isEmpty())
            gr.drawString(title, x, y + fontSize);
        gr.setColor(selected ? Color.RED : new Color(.6f, .6f, .8f));
        if (pointerLine)
            switch (probesPosition) {
            case TOP:
                gr.drawLine(x + w / 2, y,         screenHandle[0], screenHandle[1]);
                break;
            case BOTTOM:
                gr.drawLine(x + w / 2, y - h - 2, screenHandle[0], screenHandle[1]);
                break;
            case LEFT:
            case AT_POINT:
                gr.drawLine(x + w,     y - h / 2, screenHandle[0], screenHandle[1]);
                break;
            case RIGHT:
                gr.drawLine(x,         y - h / 2, screenHandle[0], screenHandle[1]);
                break;
            }
        else {
            gr.setFont(new Font(Font.DIALOG, Font.BOLD, (int)(1.4 * fontSize)));
            gr.drawString(index, x, y+18);
            gr.drawString(index, screenHandle[0], screenHandle[1]);
        }
        gr.setColor(Color.WHITE);
        gr.drawImage(sliceImage, x, y - h, w, h, null);
        ixmin = x; ixmax = x + w; 
        iymin = y - h; iymax = y;
        gr.setStroke(new BasicStroke(2));
        gr.setColor(Color.RED);
        gr.drawLine(x - 2, y + 2, x + w + 2, y + 2);
        gr.setColor(Color.BLUE);
        gr.drawLine(x - 2, y + 2, x - 2, y - h - 2);
        gr.setStroke(new BasicStroke(.5f));
        gr.setColor(Color.RED);
        gr.drawLine(x - 2, y - h - 2, x + w + 2, y - h - 2);
        gr.setColor(Color.BLUE);
        gr.drawLine(x + w + 2, y + 2, x + w + 2, y - h - 2);
        showTooltip(gr);
    }

    @Override
    public void draw(J3DGraphics2D gr, LocalToWindow ltw, int width, int height, String title) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void draw(J3DGraphics2D gr, LocalToWindow ltw, int x, int y, int width, int height, int fontSize, String title, int index) 
    {
    }
    

}
