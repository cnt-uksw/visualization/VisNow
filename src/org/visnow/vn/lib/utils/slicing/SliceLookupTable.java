/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.slicing;

import org.visnow.jscic.cells.CellType;
import static org.visnow.jscic.cells.CellType.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class SliceLookupTable
{
    /*
     * encode distribution of values of slice equation function as 3-base number 
     * with i-th digit 0 for zero value at i-th node, 1 for negative and 2 for positive value
     * the code will be used to look in the table of sections
     */

    public static int simplexCode(float[] v)
    {
        int code = 0;
        for (int i = 0, k = 1; i < v.length; i++, k *= 3)
            if (v[i] < 0)
                code += k;
            else if (v[i] > 0)
                code += 2 * k;
        return code;
    }

    /*
     * encode distribution of values of slice equation function as 3-base number 
     * if over,
     * i-th digit is 0 for zero value at i-th node, 1 for negative and 2 for positive value
     * else
     * i-th digit is 0 for zero value at i-th node, 1 for positive and 2 for negative value
     * the code will be used to look in the table of sections
     */
    public static int simplexCode(float[] v, boolean over)
    {
        int code = 0;
        if (over) {
            for (int i = 0, k = 1; i < v.length; i++, k *= 3)
                if (v[i] < 0)
                    code += k;
                else if (v[i] > 0)
                    code += 2 * k;
        } else {
            for (int i = 0, k = 1; i < v.length; i++, k *= 3)
                if (v[i] > 0)
                    code += k;
                else if (v[i] < 0)
                    code += 2 * k;
        }
        return code;
    }

    public static final int[][][] ADD_NODES
        = {
            {},
            {
                {}
            },
            {
                {}, {}, {0, 1}
            },
            {
                {}, {}, {}, {0, 1}, {0, 2}, {1, 2}
            },
            {
                {}, {}, {}, {}, {0, 1}, {0, 2}, {0, 3}, {1, 2}, {1, 3}, {2, 3}
            },};

    public static final CellType[][][] slice
        = {
            {
                {POINT}, {}, {}
            },
            { // node codes (0:0, 1:-, 2:+
                {POINT, SEGMENT},  {SEGMENT},  {SEGMENT}, //x0
                         {POINT},         {}, {TRIANGLE}, //x1
                         {POINT}, {TRIANGLE},         {} //x2
            },
            {
                {POINT, SEGMENT, TRIANGLE}, {SEGMENT, TRIANGLE},        {SEGMENT, TRIANGLE}, //x00
                {POINT,          TRIANGLE},                  {},                 {TRIANGLE, QUAD}, //x10
                {POINT,          TRIANGLE},           {TRIANGLE, QUAD}, {}, //x20
                {POINT, SEGMENT},                            {},        {SEGMENT,                 TETRA}, //x01
                {},                                          {},                           {QUAD, TETRA}, //x11
                {POINT, PYRAMID}, {QUAD, PYRAMID}, {TETRA, PYRAMID}, //x21
                {POINT, SEGMENT}, {SEGMENT, TETRA}, {}, //x02
                {POINT, PYRAMID}, {TETRA, PYRAMID}, {QUAD, PYRAMID}, //x12
                {}, {QUAD, TETRA}, {}, //x22
            },
            {},
            {
                {}, {SEGMENT, TRIANGLE, QUAD}, {SEGMENT, TRIANGLE, QUAD}, //x000
                {POINT, TRIANGLE, QUAD}, {}, {TRIANGLE, QUAD, TETRA}, //x100
                {POINT, TRIANGLE, QUAD}, {TRIANGLE, QUAD, TETRA}, {}, //x200

                {POINT, SEGMENT, QUAD}, {}, {SEGMENT, QUAD, PYRAMID}, //x010
                {}, {}, {QUAD, TETRA, PYRAMID}, //x110
                {POINT, QUAD, HEXAHEDRON}, {QUAD, TETRA, HEXAHEDRON}, {QUAD, PYRAMID, HEXAHEDRON}, //x210

                {POINT, SEGMENT, QUAD}, {SEGMENT, QUAD, PYRAMID}, {}, //x020
                {POINT, QUAD, HEXAHEDRON}, {QUAD, PYRAMID, HEXAHEDRON}, {QUAD, TETRA, HEXAHEDRON}, //x120
                {}, {QUAD, TETRA, PYRAMID}, {}, //x220

                {POINT, SEGMENT, TRIANGLE}, {}, {SEGMENT, TRIANGLE, PRISM}, //x001
                {}, {}, {TRIANGLE, TETRA, PRISM}, //x101
                {POINT, TRIANGLE, EXPLICIT}, {TRIANGLE, TETRA, EXPLICIT}, {TRIANGLE, PRISM, EXPLICIT}, //x201

                {}, {}, {SEGMENT, PYRAMID, PRISM}, //x011
                {}, {}, {TETRA, PYRAMID, PRISM}, //x111
                {POINT, HEXAHEDRON, EXPLICIT}, {TETRA, HEXAHEDRON, EXPLICIT}, {PYRAMID, PRISM, EXPLICIT, HEXAHEDRON}, //x211

                {POINT, SEGMENT, CUSTOM}, {SEGMENT, PYRAMID, CUSTOM}, {POINT, PRISM, CUSTOM}, //x021
                {POINT, HEXAHEDRON, CUSTOM}, {PYRAMID, HEXAHEDRON, CUSTOM}, {TETRA, PRISM, CUSTOM, HEXAHEDRON}, //x121
                {POINT, EXPLICIT, CUSTOM}, {TETRA, PYRAMID, CUSTOM, EXPLICIT}, {PRISM, EXPLICIT, CUSTOM}, //x221

                {POINT, SEGMENT, TRIANGLE}, {SEGMENT, TRIANGLE, PRISM}, {}, //x002
                {POINT, TRIANGLE, EXPLICIT}, {TRIANGLE, PRISM, EXPLICIT}, {TRIANGLE, TETRA, EXPLICIT}, //x102
                {}, {TRIANGLE, TETRA, PRISM}, {}, //x202

                {POINT, SEGMENT, CUSTOM}, {SEGMENT, PRISM, CUSTOM}, {SEGMENT, PYRAMID, CUSTOM}, //x012
                {POINT, EXPLICIT, CUSTOM}, {PRISM, EXPLICIT, CUSTOM}, {TETRA, PYRAMID, CUSTOM, EXPLICIT}, //x112
                {POINT, HEXAHEDRON, CUSTOM}, {TETRA, PRISM, CUSTOM, HEXAHEDRON}, {PYRAMID, HEXAHEDRON, CUSTOM}, //x212

                {}, {SEGMENT, PYRAMID, PRISM}, {}, //x022
                {POINT, HEXAHEDRON, EXPLICIT}, {PYRAMID, PRISM, EXPLICIT, HEXAHEDRON}, {TETRA, HEXAHEDRON, EXPLICIT}, //x122
                {}, {TETRA, PYRAMID, PRISM}, {} //x222
            }
        };

    public static final CellType[][] sliceType
        = {
            {
                POINT, EMPTY, EMPTY
            },
            { // node codes (0:0, 1:-, 2:+
                // node codes (0:0, 1:-, 2:+
                SEGMENT, POINT, POINT, //x0
                POINT, EMPTY, POINT, //x1
                POINT, POINT, EMPTY //x2
            },
            {
                TRIANGLE, SEGMENT, SEGMENT, //x00
                SEGMENT, EMPTY, SEGMENT, //x10
                SEGMENT, SEGMENT, EMPTY, //x20
                SEGMENT, EMPTY, SEGMENT, //x01
                EMPTY, EMPTY, SEGMENT, //x11
                SEGMENT, SEGMENT, SEGMENT, //x21
                SEGMENT, SEGMENT, EMPTY, //x02
                SEGMENT, SEGMENT, SEGMENT, //x12
                EMPTY, SEGMENT, EMPTY, //x22
            },
            {},
            {
                EMPTY, TRIANGLE, TRIANGLE,//x000
                TRIANGLE, EMPTY, TRIANGLE,//x100
                TRIANGLE, TRIANGLE, EMPTY, //x200

                TRIANGLE, EMPTY, TRIANGLE,//x010
                EMPTY, EMPTY, TRIANGLE,//x110
                TRIANGLE, TRIANGLE, TRIANGLE,//x210

                TRIANGLE, TRIANGLE, EMPTY, //x020
                TRIANGLE, TRIANGLE, TRIANGLE,//x120
                EMPTY, TRIANGLE, EMPTY, //x220

                TRIANGLE, EMPTY, TRIANGLE, //x001
                EMPTY, EMPTY, TRIANGLE, //x101
                TRIANGLE, TRIANGLE, TRIANGLE, //x201

                EMPTY, EMPTY, TRIANGLE, //x011
                EMPTY, EMPTY, TRIANGLE, //x111
                TRIANGLE, TRIANGLE, QUAD, //x211

                TRIANGLE, TRIANGLE, QUAD, //x021
                TRIANGLE, TRIANGLE, QUAD, //x121
                TRIANGLE, QUAD, TRIANGLE, //x221

                TRIANGLE, TRIANGLE, EMPTY, //x002
                TRIANGLE, TRIANGLE, TRIANGLE, //x102
                EMPTY, TRIANGLE, EMPTY, //x202

                TRIANGLE, TRIANGLE, TRIANGLE, //x012
                TRIANGLE, TRIANGLE, QUAD, //x112
                TRIANGLE, QUAD, TRIANGLE, //x212

                EMPTY, TRIANGLE, EMPTY, //x022
                TRIANGLE, QUAD, TRIANGLE, //x122
                EMPTY, TRIANGLE, EMPTY //x222
            }
        };

    
    public static final CellType[][][] subcell
        = {
            {
                {POINT}, {}, {POINT}
            },
            { // node codes (0:0, 1:-, 2:+
                {POINT, SEGMENT}, {}, {POINT, SEGMENT}, //x0
                {}, {}, {POINT, TRIANGLE}, //x1
                {POINT, SEGMENT}, {TRIANGLE, SEGMENT}, {POINT, SEGMENT} //x2
            },
            {
                {POINT, SEGMENT, TRIANGLE}, {}, {POINT, SEGMENT, TRIANGLE}, //x00
                {}, {}, {POINT, TRIANGLE, QUAD}, //x10
                {POINT, SEGMENT, TRIANGLE}, {SEGMENT, TRIANGLE, QUAD}, {POINT, SEGMENT, TRIANGLE}, //x20
                {}, {}, {POINT, SEGMENT, TETRA}, //x01
                {}, {}, {POINT, QUAD, TETRA}, //x11
                {POINT, SEGMENT, PYRAMID}, {SEGMENT, QUAD, PYRAMID}, {POINT, SEGMENT, PYRAMID, TETRA}, //x21
                {POINT, SEGMENT, TRIANGLE}, {SEGMENT, TRIANGLE, TETRA}, {POINT, SEGMENT, TRIANGLE}, //x02
                {POINT, TRIANGLE, PYRAMID}, {TRIANGLE, TETRA, PYRAMID}, {POINT, QUAD, PYRAMID, TRIANGLE}, //x12
                {POINT, SEGMENT, TRIANGLE}, {SEGMENT, QUAD, TETRA, TRIANGLE}, {POINT, SEGMENT, TRIANGLE}, //x22
            },
            {},
            {
                {POINT, SEGMENT, TRIANGLE, QUAD}, {}, {POINT, SEGMENT, TRIANGLE, QUAD}, //x000
                {}, {}, {POINT, TRIANGLE, QUAD, TETRA}, //x100
                {POINT, SEGMENT, TRIANGLE, QUAD}, {SEGMENT, TRIANGLE, QUAD, TETRA}, {POINT, SEGMENT, TRIANGLE, QUAD}, //x200

                {}, {}, {POINT, SEGMENT, QUAD, PYRAMID}, //x010
                {}, {}, {POINT, QUAD, TETRA, PYRAMID}, //x110
                {POINT, SEGMENT, QUAD, HEXAHEDRON}, {SEGMENT, QUAD, TETRA, HEXAHEDRON}, {POINT, SEGMENT, HEXAHEDRON, PYRAMID, QUAD}, //x210

                {POINT, SEGMENT, TRIANGLE, QUAD}, {SEGMENT, TRIANGLE, QUAD, PYRAMID}, {POINT, SEGMENT, TRIANGLE, QUAD}, //x020
                {POINT, TRIANGLE, QUAD, HEXAHEDRON}, {TRIANGLE, QUAD, PYRAMID, HEXAHEDRON}, {POINT, TRIANGLE, HEXAHEDRON, TETRA, QUAD}, //x120
                {POINT, SEGMENT, TRIANGLE, QUAD}, {SEGMENT, TRIANGLE, PYRAMID, TETRA, QUAD}, {POINT, SEGMENT, TRIANGLE, QUAD}, //x220

                {}, {}, {POINT, SEGMENT, TRIANGLE, PRISM}, //x001
                {}, {}, {POINT, TRIANGLE, TETRA, PRISM}, //x101
                {POINT, SEGMENT, TRIANGLE, EXPLICIT}, {SEGMENT, TRIANGLE, TETRA, EXPLICIT}, {POINT, SEGMENT, EXPLICIT, PRISM, TRIANGLE}, //x201

                {}, {}, {POINT, SEGMENT, PYRAMID, PRISM}, //x011
                {}, {}, {POINT, TETRA, PYRAMID, PRISM}, //x111
                {POINT, SEGMENT, HEXAHEDRON, EXPLICIT}, {SEGMENT, TETRA, HEXAHEDRON, EXPLICIT}, {POINT, PYRAMID, PRISM, SEGMENT, HEXAHEDRON, EXPLICIT}, //x211

                {POINT, SEGMENT, TRIANGLE, CUSTOM}, {SEGMENT, TRIANGLE, PYRAMID, CUSTOM}, {POINT, TRIANGLE, CUSTOM, PRISM, SEGMENT}, //x021
                {POINT, TRIANGLE, HEXAHEDRON, CUSTOM}, {TRIANGLE, PYRAMID, HEXAHEDRON, CUSTOM}, {POINT, TETRA, PRISM, TRIANGLE, HEXAHEDRON, CUSTOM}, //x121
                {SEGMENT, TRIANGLE, CUSTOM, EXPLICIT, POINT}, {SEGMENT, TETRA, EXPLICIT, TRIANGLE, PYRAMID, CUSTOM}, {POINT, SEGMENT, TRIANGLE, PRISM, EXPLICIT, CUSTOM}, //x221

                {POINT, SEGMENT, TRIANGLE, QUAD}, {SEGMENT, TRIANGLE, QUAD, PRISM}, {POINT, SEGMENT, TRIANGLE, QUAD}, //x002
                {POINT, TRIANGLE, QUAD, EXPLICIT}, {TRIANGLE, QUAD, PRISM, EXPLICIT}, {POINT, QUAD, EXPLICIT, TETRA, TRIANGLE}, //x102
                {POINT, SEGMENT, TRIANGLE, QUAD}, {SEGMENT, QUAD, PRISM, TETRA, TRIANGLE}, {POINT, SEGMENT, TRIANGLE, QUAD}, //x202

                {POINT, SEGMENT, QUAD, CUSTOM}, {SEGMENT, QUAD, PRISM, CUSTOM}, {POINT, QUAD, CUSTOM, PYRAMID, SEGMENT}, //x012
                {POINT, QUAD, EXPLICIT, CUSTOM}, {QUAD, PRISM, EXPLICIT, CUSTOM}, {POINT, TETRA, PYRAMID, QUAD, EXPLICIT, CUSTOM}, //x112
                {SEGMENT, QUAD, CUSTOM, HEXAHEDRON, POINT}, {SEGMENT, TETRA, HEXAHEDRON, QUAD, PRISM, CUSTOM}, {POINT, SEGMENT, QUAD, PYRAMID, HEXAHEDRON, CUSTOM}, //x212

                {POINT, SEGMENT, TRIANGLE, QUAD}, {TRIANGLE, QUAD, PRISM, PYRAMID, SEGMENT}, {POINT, SEGMENT, TRIANGLE, QUAD}, //x022
                {TRIANGLE, QUAD, EXPLICIT, HEXAHEDRON, POINT}, {TRIANGLE, PYRAMID, HEXAHEDRON, QUAD, PRISM, EXPLICIT}, {POINT, TRIANGLE, QUAD, TETRA, HEXAHEDRON, EXPLICIT}, //x122
                {POINT, SEGMENT, TRIANGLE, QUAD}, {SEGMENT, TRIANGLE, QUAD, TETRA, PYRAMID, PRISM}, {POINT, SEGMENT, TRIANGLE, QUAD} //x222
            }
        };

    public static final CellType[][] subcellType
        = {
            {
                POINT, EMPTY, POINT
            },
            { // node codes (0:0, 1:-, 2:+
                // node codes (0:0, 1:-, 2:+
                SEGMENT, EMPTY, SEGMENT, //x0
                EMPTY, EMPTY, SEGMENT, //x1
                SEGMENT, SEGMENT, SEGMENT //x2
            },
            {
                TRIANGLE, EMPTY, TRIANGLE, //x00
                EMPTY, EMPTY, TRIANGLE, //x10
                TRIANGLE, TRIANGLE, TRIANGLE, //x20
                EMPTY, EMPTY, TRIANGLE, //x01
                EMPTY, EMPTY, TRIANGLE, //x11
                TRIANGLE, TRIANGLE, QUAD, //x21
                TRIANGLE, TRIANGLE, TRIANGLE, //x02
                TRIANGLE, TRIANGLE, QUAD, //x12
                TRIANGLE, QUAD, TRIANGLE, //x22
            },
            {},
            {
                TETRA, EMPTY, TETRA, //x000
                EMPTY, EMPTY, TETRA, //x100
                TETRA, TETRA, TETRA, //x200

                EMPTY, EMPTY, TETRA, //x010
                EMPTY, EMPTY, TETRA, //x110
                TETRA, TETRA, PYRAMID,//x210

                TETRA, TETRA, TETRA, //x020
                TETRA, TETRA, PYRAMID,//x120
                TETRA, PYRAMID, TETRA, //x220

                EMPTY, EMPTY, TETRA, //x001
                EMPTY, EMPTY, TETRA, //x101
                TETRA, TETRA, PYRAMID,//x201

                EMPTY, EMPTY, TETRA, //x011
                EMPTY, EMPTY, TETRA, //x111
                TETRA, TETRA, PRISM, //x211

                TETRA, TETRA, PYRAMID,//x021
                TETRA, TETRA, PRISM, //x121
                PYRAMID, PRISM, PRISM, //x221

                TETRA, TETRA, TETRA, //x002
                TETRA, TETRA, PYRAMID,//x102
                TETRA, PYRAMID, TETRA, //x202

                TETRA, TETRA, PYRAMID,//x012
                TETRA, TETRA, PRISM, //x112
                PYRAMID, PRISM, PRISM, //x212

                TETRA, PYRAMID, TETRA, //x022
                PYRAMID, PRISM, PRISM, //x122
                TETRA, PRISM, TETRA //x222
            }
        };

    public static CellType getSliceType(CellType cellType, float[] vals)
    {
        return sliceType[cellType.getValue()][simplexCode(vals)];
    }

    public static CellType[] getSliceNodes(CellType cellType, float[] vals)
    {
        return slice[cellType.getValue()][simplexCode(vals)];
    }

    public static CellType getSubcellType(CellType cellType, float[] vals, boolean above)
    {
        return subcellType[cellType.getValue()][simplexCode(vals, above)];
    }

    public static CellType[] getSubcellNodes(CellType cellType, float[] vals, boolean above)
    {
        return subcell[cellType.getValue()][simplexCode(vals, above)];
    }

    static int[] v = {0, 1, -1};

    public static void main(String[] args)
    {
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                for (int k = 0; k < 3; k++)
                    for (int l = 0; l < 3; l++)
                        System.out.printf("%d%d%d%d, in%n", l, k, j, i);
        for (int node = 0; node < 10; node++) {
            System.out.printf("%2d %2d ", node + 1, node);
            for (int i = 0, n = 1; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    for (int k = 0; k < 3; k++)
                        for (int l = 0; l < 3; l++)
                            switch (node) {
                                case 0:
                                    System.out.printf("%2d ", v[l]);
                                    break;
                                case 1:
                                    System.out.printf("%2d ", v[k]);
                                    break;
                                case 2:
                                    System.out.printf("%2d ", v[j]);
                                    break;
                                case 3:
                                    System.out.printf("%2d ", v[i]);
                                    break;
                                default:
                                    System.out.printf("%2d ", 0);
                            }
            System.out.println("");
        }
    }
}
