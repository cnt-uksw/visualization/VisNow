/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.graphing;

import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.geom.AffineTransform;
import org.jogamp.java3d.J3DGraphics2D;
import org.visnow.jscic.IrregularField;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import static org.visnow.vn.lib.utils.interpolation.SubsetGeometryComponents.LINE_SLICE_COORDS;

/**
 *
 * @author know
 */
    
public class GraphDisplay extends ProbeDisplay
{
    private final GraphWorld graph;
    protected GraphParams graphParams;    

    public GraphDisplay(IrregularField fld, GraphParams graphParams, Position probesPosition, boolean pointerLine, float[] center)
    {
        int nNodes = (int)fld.getNNodes();
        float[] c = fld.getCoords(0).getData();
        crds = new float[6];
        System.arraycopy(c, 0, crds, 0, 3);
        System.arraycopy(c, 3 * (nNodes -1), crds, 3, 3);
        System.arraycopy(center, 0, this.center, 0, 3);
        this.fld = fld;
        graph = new GraphWorld(graphParams);
        graph.setComputeOrig(false);
        graph.setInField(fld);
        this.graphParams = graphParams;
        cornerCoords = new float[6];
        this.pointerLine = pointerLine;
        this.probesPosition = probesPosition;
    }
    
    public GraphDisplay(IrregularField fld, GraphParams graphParams, float[] center)
    {
        int nNodes = (int)fld.getNNodes();
        float[] c = fld.getCoords(0).getData();
        crds = new float[6];
        System.arraycopy(c, 0, crds, 0, 3);
        System.arraycopy(c, 3 * (nNodes -1), crds, 3, 3);
        this.fld = fld;
        graph = new GraphWorld(graphParams);
        graph.setComputeOrig(false);
        graph.setInField(fld);
        this.graphParams = graphParams;
        cornerCoords = new float[6];
        System.arraycopy(center, 0, this.center, 0, 3);
    }
    
    public void setGraphParams(GraphParams graphParams) {
        this.graphParams = graphParams;
    }
    

    @Override
    public void updateScreenCoords(LocalToWindow ltw)
    {
        int[] screenOrigin = new int[2];
        int[] screenEndU = new int[2];
        int[] screenEndV = new int[2];
        float[] corner = new float[3];
        int[] tmp = new int[2];
        for (int i = 0; i < 2; i++) {
            System.arraycopy(crds, 3 * i, corner, 0, 3);
            ltw.transformPt(corner, tmp);
            switch (i) {
                case 0:
                   System.arraycopy(tmp, 0, screenOrigin, 0, 2); 
                   System.arraycopy(tmp, 0, screenHandle, 0, 2); 
                case 1:
                   System.arraycopy(tmp, 0, screenEndU, 0, 2); 
                case 2:
                   System.arraycopy(tmp, 0, screenEndV, 0, 2); 
            }
        }
        for (int i = 0; i < 4; i++) {
            switch (probesPosition) {
            case TOP:
            case AT_POINT:
                if (tmp[1] < screenHandle[1])
                    System.arraycopy(tmp, 0, screenHandle, 0, 2);
                break;
            case BOTTOM:
                if (tmp[1] > screenHandle[1])
                    System.arraycopy(tmp, 0, screenHandle, 0, 2);
                break;
            case RIGHT:
                if (tmp[0] > screenHandle[0])
                    System.arraycopy(tmp, 0, screenHandle, 0, 2);
                break;
            case LEFT:
                if (tmp[0] < screenHandle[0])
                    System.arraycopy(tmp, 0, screenHandle, 0, 2);
                break;
            }
        }
    }
    
    public void draw(J3DGraphics2D gr, LocalToWindow ltw, int width, int height, String title)
        {
        gr.setTransform(new AffineTransform());
        gr.setStroke(new BasicStroke(graphParams.getLineWidth()));
        int[] tmp = new int[2];
        ltw.transformPt(center, tmp);
        graph.setOrig(screenHandle[0] + probeOffset[0], screenHandle[1] - probeOffset[1]);
        graph.prepareGraph(gr, width, height);
        graph.draw2D(gr, ltw, width, height, title);
        gr.setColor(selected ? Color.RED : graph.getForegroundColor());
        gr.drawLine(tmp[0] - 3, tmp[1], tmp[0] + 3, tmp[1]);
        gr.drawLine(tmp[0], tmp[1] - 3, tmp[0], tmp[1] + 3);
        gr.drawLine(screenHandle[0] + probeOffset[0], screenHandle[1] - probeOffset[1], 
                    screenHandle[0],                  screenHandle[1]);
        gr.setColor(graphParams.getColor());
        int[] graphRect = graph.getBackgroundDims();
        ixmin = graphRect[0];
        iymin = graphRect[1];
        ixmax = graphRect[0] + graphRect[2];
        iymax = graphRect[1] + graphRect[3];
    }

    @Override
    public void drawOnMargin(J3DGraphics2D gr, LocalToWindow ltw, int width, int height, 
                             float scale, int index, int ix, int iy, String title)
    {
        int anchorX = 0, anchorY = 0;
        graph.setOrig(ix, iy);
        graph.prepareGraph(gr, width, height);
        gr.setStroke(new BasicStroke(graphParams.getLineWidth()));
        int[] graphRect = graph.getBackgroundDims();
        switch (probesPosition) {
            case TOP:
                anchorX = graphRect[0] + graphRect[2] / 2;
                anchorY = graphRect[1] + graphRect[3];
                break;
            case BOTTOM:
                anchorX = graphRect[0] + graphRect[2] / 2;
                anchorY = graphRect[1];
                break;
            case LEFT:
                anchorX = graphRect[0] + graphRect[2];
                anchorY = graphRect[1] + graphRect[3] / 2;
                break;
            case RIGHT:
                anchorX = graphRect[0];
                anchorY = graphRect[1] + graphRect[3] / 2;
                break;
        }
        int[] tmp = new int[2];
        ltw.transformPt(center, tmp);
        if (graphParams.getArgument().equalsIgnoreCase(LINE_SLICE_COORDS)) {
            gr.drawLine(tmp[0] - 5, tmp[1], tmp[0] + 5, tmp[1]);
            gr.drawLine(tmp[0], tmp[1] - 5, tmp[0], tmp[1] + 5);
        }
        graph.setOrig(ix, iy);
        graph.draw2D(gr, ltw, width, height, title);
        gr.setColor(selected ? Color.RED : graph.getForegroundColor());
        if (pointerLine)
            gr.drawLine(anchorX, anchorY, screenHandle[0], screenHandle[1]);
        else {
            gr.setFont(new Font(Font.DIALOG, Font.BOLD, 16));
            gr.drawString(""+index, anchorX - 10, anchorY + 18);
            gr.drawString(""+index, screenHandle[0], screenHandle[1]);
        }
        gr.setColor(graphParams.getColor());
        ixmin = graphRect[0];
        iymin = graphRect[1];
        ixmax = graphRect[0] + graphRect[2];
        iymax = graphRect[1] + graphRect[3];
    }

    public GraphWorld getGraphWorld()
    {
        return graph;
    }

    @Override
    public void draw(J3DGraphics2D gr, LocalToWindow ltw, int x, int y, int width, int height, int fontSize, String title, int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
    
