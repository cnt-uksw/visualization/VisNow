/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.lineProbe;

import org.visnow.vn.lib.utils.probeInterfaces.Probe;
import org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.LineAttributes;
import static org.jogamp.java3d.LineAttributes.PATTERN_SOLID;
import org.jogamp.java3d.LineStripArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.FieldUtils;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;
import org.visnow.vn.geometries.parameters.ComponentColorMap;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.utils.ColorMapper;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceGUI;
import org.visnow.vn.lib.utils.field.IndexSlice1D;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */


public class IndexProbe1D extends Probe
{
    
    protected DataMappingParams mapParams;
    protected RegularField inField = null;
    protected IndexSliceParams params = new IndexSliceParams();
    protected IndexSliceGUI gui = new IndexSliceGUI(IndexSliceParams.Type.LINE);
    protected RegularField regularSlice = null;
    protected int[] inDims;
    protected float[] sliceCrds;
    protected int[] outDims = {-1};
    protected int nOutNodes = 0;
    protected FloatLargeArray outCoords;
    protected byte[] colors;
    
    protected int[] startstep = new int[2];
    protected int[] off;
    protected float[] w;
    
    protected int lastNOutNodes = -1;
    protected ComponentColorMap cMap;
    protected IrregularField slice = null;
    // glyph used for interactive display (continuously updated when position slider is adjusting)
    protected OpenShape3D lineShape = new OpenShape3D();
    protected OpenBranchGroup  glyphGroup = new OpenBranchGroup();
    protected LineStripArray glyphLines;
    protected OpenBranchGroup parent = new OpenBranchGroup();
    protected boolean glyphVisible = true, lastGlyphVisible = true;
    

    protected RenderEventListener mapEventListener = new RenderEventListener()
    {
        @Override
        public void renderExtentChanged(RenderEvent e)
        {
            updateCurrentProbeGeometry();
        }
    };
    
    public IndexProbe1D()
    {
        gui.setParams(params);
        params.setType(IndexSliceParams.Type.LINE);
        params.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                glyphVisible = params.isGlyphVisible();
                regularSlice = IndexSlice1D.slice(inField, params);
                slice = FieldUtils.convertToIrregular(regularSlice);
                updateCurrentProbeGeometry();
                fireStateChanged(params.isAdjusting());
            }
        });
        glyphGroup.setName("probeGroup");
        parent.setName("probeParentGroup");
    }
    
    @Override
    public final void setInData(Field field, DataMappingParams mapParams)
    {
        if (!(field instanceof RegularField))
            return;
        setDataMappingParams(mapParams);
        this.cMap = mapParams.getColorMap0();
        inField = (RegularField)field;
        inDims = inField.getDims();
        if (params.getAxis() >= this.inField.getDimNum() || params.getAxis() < 0) 
            params.setAxis(0);
        params.setActive(false);
        params.setFieldSchema(inField.getSchema());
        params.setActive(true);
        params.setFieldSchema(inField.getSchema());
        regularSlice = IndexSlice1D.slice(inField, params);
        slice = FieldUtils.convertToIrregular(regularSlice);
        updateCurrentProbeGeometry();
    }
    
    @Override
    public void setDataMappingParams(DataMappingParams dataMappingParams)
    {
        cMap = dataMappingParams.getColorMap0();
        updateColors();
        dataMappingParams.addRenderEventListener(mapEventListener);
    }
   
    @Override
    public OpenBranchGroup getGlyphGeometry()
    {
        return parent;
    }
    
    protected void updateColors()
    {
        if (slice == null)
            return;
        if (colors == null || colors.length != 3 * nOutNodes)
            colors = new byte[3 * nOutNodes];
        DataArray da =  slice.getComponent(cMap.getDataComponentIndex());
        if (da != null && da.isNumeric()) {
            FloatLargeArray outDa = new FloatLargeArray(da.getVectorLength() * nOutNodes);
            float[] fData = outDa.getData();
            int vlen = da.getVectorLength();
            float[] v = new float[vlen];
            for (int i = 0; i < nOutNodes; i++) {
                System.arraycopy(fData, i * vlen, v, 0, vlen);
                byte[] pointColor = ColorMapper.mapByteColor(v, cMap);
                for (int j = 0; j < 3; j++) 
                    pointColor[j] = (byte)(0xff & (int)(.8 * (pointColor[j] & 0xff)));
                System.arraycopy(pointColor, 0, colors, 3 * i, 3);
            }
        }
        else {
            float[] outT = new float[nOutNodes];
            float[] outC = slice.getCurrentCoords().getData();
            outT[0] = 0;
            for (int i = 1; i < nOutNodes; i++) 
                outT[i] = (float)Math.sqrt((outC[3 * i]     - outC[3 * i - 3]) * (outC[3 * i]     - outC[3 * i - 3]) +
                                           (outC[3 * i + 1] - outC[3 * i - 2]) * (outC[3 * i + 1] - outC[3 * i - 2]) +
                                           (outC[3 * i + 2] - outC[3 * i - 1]) * (outC[3 * i + 2] - outC[3 * i - 1])) +
                          outT[i - 1];
            float tMax = outT[nOutNodes - 1];
            for (int i = 0; i < nOutNodes; i++) {
                colors[3 * i]     = (byte)(0xff & (int)((255. * outT[i]) / tMax));
                colors[3 * i + 1] = (byte)(0xff & 255);
                colors[3 * i + 2] = (byte)(0xff & 255);
            }
            ColorMapper.hsvtorgb(colors);
        }
    }
    
    public void updateGlyph()
    {
        if (regularSlice == null)
            return;
        if (outDims[0] != regularSlice.getDims()[0]) {
            outDims = regularSlice.getDims();
            updateCurrentProbeGeometry();
        }
        updateGlyphData();
    }
    
    public void updateGlyphData()
    {
        
        updateColors();
        if (regularSlice.hasCoords())
            glyphLines.setCoordinates(0, regularSlice.getCurrentCoords().getData());
        else
            glyphLines.setCoordinates(0, regularSlice.getCoordsFromAffine().getData());
        glyphLines.setColors(0, colors);
    }
    
    int lastAxis = -1;
    
    public void updateCurrentProbeGeometry()
    {
        if (regularSlice == null) {
            glyphGroup.detach();
            glyphGroup.removeAllChildren();
            if (glyphVisible)
                parent.addChild(glyphGroup);
            return;
        }
        nOutNodes = (int) regularSlice.getNNodes();
        glyphGroup.detach();
        glyphGroup.removeAllChildren();
        glyphLines = new LineStripArray(nOutNodes,  GeometryArray.COORDINATES | GeometryArray.COLOR_3, new int[]{nOutNodes});
        glyphLines.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        glyphLines.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        glyphLines.setCapability(GeometryArray.ALLOW_COLOR_READ);
        glyphLines.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
        updateGlyphData();
        OpenAppearance app = new OpenAppearance();
        app.setLineAttributes(new LineAttributes(4, PATTERN_SOLID, true));
        lineShape.removeAllGeometries();
        lineShape.setAppearance(app);
        lineShape.addGeometry(glyphLines);
        glyphGroup.addChild(lineShape);
        if (glyphVisible)
            parent.addChild(glyphGroup);
        updateGlyphData();
        if (glyphVisible != lastGlyphVisible) {
            if (!glyphVisible)
                glyphGroup.detach();
            else {
                glyphGroup.detach();
                parent.addChild(glyphGroup);
            }
            lastGlyphVisible = glyphVisible;
        }
    }
    
    public float[] getPlaneCenter()
    {
        float[] startPoint = new float[3];
        if (regularSlice.hasCoords())
            LargeArrayUtils.arraycopy(regularSlice.getCurrentCoords(), 0, startPoint, 0, 3);
        else
            System.arraycopy(regularSlice.getAffine()[0], 0, startPoint, 0, 3);
        return startPoint;
    }
    
    public void hide()
    {
        if (glyphGroup != null) 
                glyphGroup.detach();
    }
    
    public void show()
    {
        if (glyphGroup.getParent() == null)
            parent.addChild(glyphGroup);
    }
    @Override
    public JPanel getGlyphGUI()
    {
        return gui;
    }

    @Override
    public IrregularField getSliceField()
    {
        return FieldUtils.convertToIrregular(regularSlice);
    }

    @Override
    public RegularField getRegularSliceField()
    {
        return regularSlice;
    }
}
