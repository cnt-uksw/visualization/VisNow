/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.fft;

import java.util.ArrayList;
import org.visnow.jscic.dataarrays.ComplexDataArray;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.dataarrays.FloatDataArray;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.system.main.VisNow;

/**
 * @author Piotr Wendykier (piotrw@icm.edu.pl)
 * @author Bartosz Borucki (babor@icm.edu.pl) Warsaw University, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public abstract class FftCore {

    protected FloatLargeArray[] outReal = null;
    protected FloatLargeArray[] outImag = null;
    protected ArrayList<Float> timeSeries = null;
    protected long[] dims;
    protected boolean working = false;
    protected boolean done = false;
    protected boolean complexInput = true;

    /**
     * Creates a new <code>FftCore</code> object.
     */
    public FftCore() {
    }

    public abstract void fft_r2c(FloatLargeArray real, FloatLargeArray imag);

    public abstract void fft_c2c(FloatLargeArray real, FloatLargeArray imag);

    public abstract void ifft(FloatLargeArray real, FloatLargeArray imag);

    public abstract void fft2_r2c(FloatLargeArray real, FloatLargeArray imag, long nx, long ny);

    public abstract void fft2_c2c(FloatLargeArray real, FloatLargeArray imag, long nx, long ny);

    public abstract void ifft2(FloatLargeArray real, FloatLargeArray imag, long nx, long ny);

    public abstract void fft3_r2c(FloatLargeArray real, FloatLargeArray imag, long nx, long ny, long nz);

    public abstract void fft3_c2c(FloatLargeArray real, FloatLargeArray imag, long nx, long ny, long nz);

    public abstract void ifft3(FloatLargeArray real, FloatLargeArray imag, long nx, long ny, long nz);

    /**
     * Runs Fast Fourier Transform calculations on the previously set input data, with the loaded core
     */
    public void calculateFFT(boolean center_origin) {
        if (outReal == null || outImag == null) {
            return;
        }
        working = true;
        if (dims.length == 1) {
            if (complexInput) {
                for (int i = 0; i < outReal.length; i++) {
                    fft_c2c(outReal[i], outImag[i]);
                }
            } else {
                for (int i = 0; i < outReal.length; i++) {
                    fft_r2c(outReal[i], outImag[i]);
                }
            }
        } else if (dims.length == 2) {
            if (complexInput) {
                for (int i = 0; i < outReal.length; i++) {
                    fft2_c2c(outReal[i], outImag[i], dims[0], dims[1]);
                }
            } else {
                for (int i = 0; i < outReal.length; i++) {
                    fft2_r2c(outReal[i], outImag[i], dims[0], dims[1]);
                }
            }
            if (center_origin) {
                for (int i = 0; i < outReal.length; i++) {
                    outReal[i] = circShift_2D(outReal[i], dims[0], dims[1]);
                    outImag[i] = circShift_2D(outImag[i], dims[0], dims[1]);
                }
            }
        } else if (dims.length == 3) {
            if (complexInput) {
                for (int i = 0; i < outReal.length; i++) {
                    fft3_c2c(outReal[i], outImag[i], dims[0], dims[1], dims[2]);
                }
            } else {
                for (int i = 0; i < outReal.length; i++) {
                    fft3_r2c(outReal[i], outImag[i], dims[0], dims[1], dims[2]);
                }
            }
            if (center_origin) {
                for (int i = 0; i < outReal.length; i++) {
                    outReal[i] = circShift_3D(outReal[i], dims[0], dims[1], dims[2]);
                    outImag[i] = circShift_3D(outImag[i], dims[0], dims[1], dims[2]);
                }
            }
        }
        working = false;
        done = true;
    }

    /**
     * Runs Inverse Fast Fourier Transform calculations on the previously set input data, with the loaded core
     * <p>
     * @param center_origin
     */
    public void calculateIFFT(boolean center_origin) {
        if (outReal == null || outImag == null) {
            return;
        }

        working = true;
        if (dims.length == 1) {
            for (int i = 0; i < outReal.length; i++) {
                ifft(outReal[i], outImag[i]);
            }
        } else if (dims.length == 2) {
            if (center_origin) {
                for (int i = 0; i < outReal.length; i++) {
                    outReal[i] = inverseCircShift_2D(outReal[i], dims[0], dims[1]);
                    outImag[i] = inverseCircShift_2D(outImag[i], dims[0], dims[1]);
                }
            }
            for (int i = 0; i < outReal.length; i++) {
                ifft2(outReal[i], outImag[i], dims[0], dims[1]);
            }
        } else if (dims.length == 3) {
            if (center_origin) {
                for (int i = 0; i < outReal.length; i++) {
                    outReal[i] = inverseCircShift_3D(outReal[i], dims[0], dims[1], dims[2]);
                    outImag[i] = inverseCircShift_3D(outImag[i], dims[0], dims[1], dims[2]);
                }
            }
            for (int i = 0; i < outReal.length; i++) {
                ifft3(outReal[i], outImag[i], dims[0], dims[1], dims[2]);
            }
        }
        working = false;
        done = true;
    }

    /**
     * Sets 1D input data for FFT calculations. Dimension is calculated as data length.
     *
     * @param	inData	Input data array.
     */
    public void setInput(DataArray inData) {
        if (inData == null || inData.getVectorLength() > 1) {
            return;
        }

        long[] dms = new long[1];
        dms[0] = (int) inData.getNElements();
        this.setInput(inData, dms);
    }

    /**
     * Sets input data for FFT calculations with given data dimensions.
     *
     * @param	inData	Input data array.
     * @param	dims	Data dimensions.
     */
    public void setInput(DataArray inData, long[] dims) {
        this.done = false;
        this.outReal = null;
        this.outImag = null;
        this.dims = new long[dims.length];
        if (dims.length == 1) {
            this.dims[0] = dims[0];
        } else if (dims.length == 2) {
            this.dims[0] = dims[0];
            this.dims[1] = dims[1];
        } else {
            this.dims[0] = dims[0];
            this.dims[1] = dims[1];
            this.dims[2] = dims[2];
        }

        if (inData == null || inData.getVectorLength() > 1) {
            return;
        }

        int frames = inData.getNFrames();
        this.outReal = new FloatLargeArray[frames];
        this.outImag = new FloatLargeArray[frames];
        if (frames == 1) {
            if (inData.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                this.outReal[0] = ((ComplexDataArray) inData).getFloatRealArray().clone();
                this.outImag[0] = ((ComplexDataArray) inData).getFloatImaginaryArray().clone();
                this.complexInput = true;
            } else {
                this.outReal[0] = inData.getRawFloatArray();
                this.outImag[0] = new FloatLargeArray(this.outReal[0].length());
                this.complexInput = false;
            }
        } else {
            timeSeries = inData.getTimeSeries();
            int i = 0;
            float currentTime = inData.getCurrentTime();
            for (Float t : timeSeries) {
                inData.setCurrentTime(t);
                if (inData.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                    this.outReal[i] = ((ComplexDataArray) inData).getFloatRealArray().clone();
                    this.outImag[i] = ((ComplexDataArray) inData).getFloatImaginaryArray().clone();
                    this.complexInput = true;
                } else {
                    this.outReal[i] = inData.getRawFloatArray();
                    this.outImag[i] = new FloatLargeArray(this.outReal[i].length());
                    this.complexInput = false;
                }
                i++;
            }
            inData.setCurrentTime(currentTime);
        }

    }

    /**
     * Returns real part of calculated output transform as a DataArray.
     *
     * @return	A <code>DataArray</code> object.
     */
    public DataArray getRealOutput() {
        if (done) {
            if (outReal.length == 1) {
                return new FloatDataArray(outReal[0], new DataArraySchema("output", DataArrayType.FIELD_DATA_FLOAT, outReal[0].length(), 1, false));
            } else {
                FloatDataArray res = new FloatDataArray(outReal[0].length(), 1);
                int i = 0;
                for (Float t : timeSeries) {
                    res.addRawArray(outReal[i++], t);
                }
                res.setCurrentTime(timeSeries.get(0));
                return res;
            }
        }
        return null;
    }

    /**
     * Returns imaginary part of calculated output transform as a DataArray.
     *
     * @return	A <code>DataArray</code> object.
     */
    public DataArray getImagOutput() {
        if (done) {
            if (outImag.length == 1) {
                return new FloatDataArray(outImag[0], new DataArraySchema("output", DataArrayType.FIELD_DATA_FLOAT, outImag[0].length(), 1, false));
            } else {
                FloatDataArray res = new FloatDataArray(outImag[0].length(), 1);
                int i = 0;
                for (Float t : timeSeries) {
                    res.addRawArray(outImag[i++], t);
                }
                res.setCurrentTime(timeSeries.get(0));
                return res;
            }
        }
        return null;
    }

    /**
     * Returns calculated output transform as a DataArray.
     *
     * @return	A <code>DataArray</code> object.
     */
    public DataArray getOutput() {
        if (done) {
            if (outImag.length == 1) {
                return new ComplexDataArray(new ComplexFloatLargeArray(outReal[0], outImag[0]), new DataArraySchema("output", DataArrayType.FIELD_DATA_COMPLEX, outReal[0].length(), 1, false));
            } else {
                ComplexDataArray res = new ComplexDataArray(outImag[0].length(), 1);
                int i = 0;
                for (Float t : timeSeries) {
                    res.addRawArray(new ComplexFloatLargeArray(outReal[i], outImag[i]), t);
                    i++;
                }
                res.setCurrentTime(timeSeries.get(0));
                return res;
            }
        }
        return null;
    }

    /**
     * Returns true if calculations are in progress, false otherwise.
     *
     * @return	<code>boolean</code> value.
     */
    public boolean isWorking() {
        return working;
    }

    /**
     * Static procedure that loads FFT core library. It tries to load the native FFTW library for the current architecture and OS. If fails it loads the default
     * Java FFT core.
     *
     * @return	<code>FftCore</code> object - instance of ready FFT calculation core
     */
    public static FftCore loadFftLibrary() {
        return new FftJ1Core();

    }

    public static float[] circShift_1D(float[] in) {
        int cc = (int) floor(in.length / 2.0);
        float[] out = new float[in.length];
        for (int c = 0; c < in.length - cc; c++) {
            out[c] = in[c + cc];
        }
        for (int c = 0; c < cc; c++) {
            out[in.length - cc + c] = in[c];
        }
        return out;
    }

    public static FloatLargeArray circShift_1D(FloatLargeArray in) {
        long cc = (long) floor(in.length() / 2.0);
        FloatLargeArray out = new FloatLargeArray(in.length());
        for (long c = 0; c < in.length() - cc; c++) {
            out.setFloat(c, in.getFloat(c + cc));
        }
        for (long c = 0; c < cc; c++) {
            out.setFloat(in.length() - cc + c, in.getFloat(c));
        }
        return out;
    }

    public static float[] inverseCircShift_1D(float[] in) {
        int cc = (int) floor(in.length / 2.0);
        float[] out = new float[in.length];
        for (int c = 0; c < in.length - cc; c++) {
            out[c + cc] = in[c];
        }
        for (int c = 0; c < cc; c++) {
            out[c] = in[in.length - cc + c];
        }
        return out;
    }

    public static FloatLargeArray inverseCircShift_1D(FloatLargeArray in) {
        long cc = (long) floor(in.length() / 2.0);
        FloatLargeArray out = new FloatLargeArray(in.length());
        for (long c = 0; c < in.length() - cc; c++) {
            out.setFloat(c + cc, in.getFloat(c));
        }
        for (long c = 0; c < cc; c++) {
            out.setFloat(c, in.getFloat(in.length() - cc + c));
        }
        return out;
    }

    public static float[] circShift_2D(float[] in, int nx, int ny) {
        int cr = (int) floor(ny / 2.0);
        int cc = (int) floor(nx / 2.0);
        float[] out = new float[ny * nx];
        for (int r = 0; r < ny - cr; r++) {
            for (int c = 0; c < nx - cc; c++) {
                out[r * nx + c] = in[(r + cr) * nx + (c + cc)];
            }
        }
        for (int r = 0; r < ny - cr; r++) {
            for (int c = 0; c < cc; c++) {
                out[r * nx + (nx - cc + c)] = in[(cr + r) * nx + c];
            }
        }
        for (int r = 0; r < cr; r++) {
            for (int c = 0; c < nx - cc; c++) {
                out[(ny - cr + r) * nx + c] = in[r * nx + (cc + c)];
            }
        }
        for (int r = 0; r < cr; r++) {
            for (int c = 0; c < cc; c++) {
                out[(ny - cr + r) * nx + (nx - cc + c)] = in[r * nx + c];
            }
        }
        return out;
    }

    public static FloatLargeArray circShift_2D(FloatLargeArray in, long nx, long ny) {
        long cr = (long) floor(ny / 2.0);
        long cc = (long) floor(nx / 2.0);
        FloatLargeArray out = new FloatLargeArray(ny * nx);
        for (long r = 0; r < ny - cr; r++) {
            for (long c = 0; c < nx - cc; c++) {
                out.setFloat(r * nx + c, in.getFloat((r + cr) * nx + (c + cc)));
            }
        }
        for (long r = 0; r < ny - cr; r++) {
            for (long c = 0; c < cc; c++) {
                out.setFloat(r * nx + (nx - cc + c), in.getFloat((cr + r) * nx + c));
            }
        }
        for (long r = 0; r < cr; r++) {
            for (long c = 0; c < nx - cc; c++) {
                out.setFloat((ny - cr + r) * nx + c, in.getFloat(r * nx + (cc + c)));
            }
        }
        for (long r = 0; r < cr; r++) {
            for (long c = 0; c < cc; c++) {
                out.setFloat((ny - cr + r) * nx + (nx - cc + c), in.getFloat(r * nx + c));
            }
        }
        return out;
    }

    public static float[] inverseCircShift_2D(float[] in, int nx, int ny) {
        int cr = (int) floor(ny / 2.0);
        int cc = (int) floor(nx / 2.0);
        float[] out = new float[ny * nx];
        for (int r = 0; r < ny - cr; r++) {
            for (int c = 0; c < nx - cc; c++) {
                out[(r + cr) * nx + (c + cc)] = in[r * nx + c];
            }
        }
        for (int r = 0; r < ny - cr; r++) {
            for (int c = 0; c < cc; c++) {
                out[(cr + r) * nx + c] = in[r * nx + (nx - cc + c)];
            }
        }
        for (int r = 0; r < cr; r++) {
            for (int c = 0; c < nx - cc; c++) {
                out[r * nx + (cc + c)] = in[(ny - cr + r) * nx + c];
            }
        }
        for (int r = 0; r < cr; r++) {
            for (int c = 0; c < cc; c++) {
                out[r * nx + c] = in[(ny - cr + r) * nx + (nx - cc + c)];
            }
        }
        return out;
    }

    public static FloatLargeArray inverseCircShift_2D(FloatLargeArray in, long nx, long ny) {
        long cr = (long) floor(ny / 2.0);
        long cc = (long) floor(nx / 2.0);
        FloatLargeArray out = new FloatLargeArray(ny * nx);
        for (long r = 0; r < ny - cr; r++) {
            for (long c = 0; c < nx - cc; c++) {
                out.setFloat((r + cr) * nx + (c + cc), in.getFloat(r * nx + c));
            }
        }
        for (long r = 0; r < ny - cr; r++) {
            for (long c = 0; c < cc; c++) {
                out.setFloat((cr + r) * nx + c, in.getFloat(r * nx + (nx - cc + c)));
            }
        }
        for (long r = 0; r < cr; r++) {
            for (long c = 0; c < nx - cc; c++) {
                out.setFloat(r * nx + (cc + c), in.getFloat((ny - cr + r) * nx + c));
            }
        }
        for (long r = 0; r < cr; r++) {
            for (long c = 0; c < cc; c++) {
                out.setFloat(r * nx + c, in.getFloat((ny - cr + r) * nx + (nx - cc + c)));
            }
        }
        return out;
    }

    public static float[] circShift_3D(float[] in, int nx, int ny, int nz) {
        int cs = (int) floor(nz / 2.0);
        int cr = (int) floor(ny / 2.0);
        int cc = (int) floor(nx / 2.0);
        int sstride = ny * nx;
        float[] out = new float[nz * ny * nx];
        for (int s = 0; s < nz - cs; s++) {
            for (int r = 0; r < ny - cr; r++) {
                for (int c = 0; c < nx - cc; c++) {
                    out[s * sstride + r * nx + c] = in[(s + cs) * sstride + (r + cr) * nx + (c + cc)];
                }
            }
        }
        for (int s = 0; s < nz - cs; s++) {
            for (int r = 0; r < cr; r++) {
                for (int c = 0; c < nx - cc; c++) {
                    out[s * sstride + (ny - cr + r) * nx + c] = in[(s + cs) * sstride + r * nx + (c + cc)];
                }
            }
        }

        for (int s = 0; s < nz - cs; s++) {
            for (int r = 0; r < ny - cr; r++) {
                for (int c = 0; c < cc; c++) {
                    out[s * sstride + r * nx + (nx - cc + c)] = in[(s + cs) * sstride + (r + cr) * nx + c];
                }
            }
        }

        for (int s = 0; s < nz - cs; s++) {
            for (int r = 0; r < cr; r++) {
                for (int c = 0; c < cc; c++) {
                    out[s * sstride + (ny - cr + r) * nx + (nx - cc + c)] = in[(s + cs) * sstride + r * nx + c];
                }
            }
        }

        for (int s = 0; s < cs; s++) {
            for (int r = 0; r < ny - cr; r++) {
                for (int c = 0; c < nx - cc; c++) {
                    out[(nz - cs + s) * sstride + r * nx + c] = in[s * sstride + (r + cr) * nx + c + cc];
                }
            }
        }

        for (int s = 0; s < cs; s++) {
            for (int r = 0; r < ny - cr; r++) {
                for (int c = 0; c < cc; c++) {
                    out[(nz - cs + s) * sstride + r * nx + (nx - cc + c)] = in[s * sstride + (r + cr) * nx + c];
                }
            }
        }

        for (int s = 0; s < cs; s++) {
            for (int r = 0; r < cr; r++) {
                for (int c = 0; c < nx - cc; c++) {
                    out[(nz - cs + s) * sstride + (ny - cr + r) * nx + c] = in[s * sstride + r * nx + c + cc];
                }
            }
        }

        for (int s = 0; s < cs; s++) {
            for (int r = 0; r < cr; r++) {
                for (int c = 0; c < cc; c++) {
                    out[(nz - cs + s) * sstride + (ny - cr + r) * nx + (nx - cc + c)] = in[s * sstride + r * nx + c];
                }
            }
        }

        return out;
    }

    public static FloatLargeArray circShift_3D(FloatLargeArray in, long nx, long ny, long nz) {
        long cs = (long) floor(nz / 2.0);
        long cr = (long) floor(ny / 2.0);
        long cc = (long) floor(nx / 2.0);
        long sstride = ny * nx;
        FloatLargeArray out = new FloatLargeArray(nz * ny * nx);
        for (long s = 0; s < nz - cs; s++) {
            for (long r = 0; r < ny - cr; r++) {
                for (long c = 0; c < nx - cc; c++) {
                    out.setFloat(s * sstride + r * nx + c, in.getFloat((s + cs) * sstride + (r + cr) * nx + (c + cc)));
                }
            }
        }
        for (long s = 0; s < nz - cs; s++) {
            for (long r = 0; r < cr; r++) {
                for (long c = 0; c < nx - cc; c++) {
                    out.setFloat(s * sstride + (ny - cr + r) * nx + c, in.getFloat((s + cs) * sstride + r * nx + (c + cc)));
                }
            }
        }

        for (long s = 0; s < nz - cs; s++) {
            for (long r = 0; r < ny - cr; r++) {
                for (long c = 0; c < cc; c++) {
                    out.setFloat(s * sstride + r * nx + (nx - cc + c), in.getFloat((s + cs) * sstride + (r + cr) * nx + c));
                }
            }
        }

        for (long s = 0; s < nz - cs; s++) {
            for (long r = 0; r < cr; r++) {
                for (long c = 0; c < cc; c++) {
                    out.setFloat(s * sstride + (ny - cr + r) * nx + (nx - cc + c), in.getFloat((s + cs) * sstride + r * nx + c));
                }
            }
        }

        for (long s = 0; s < cs; s++) {
            for (long r = 0; r < ny - cr; r++) {
                for (long c = 0; c < nx - cc; c++) {
                    out.setFloat((nz - cs + s) * sstride + r * nx + c, in.getFloat(s * sstride + (r + cr) * nx + c + cc));
                }
            }
        }

        for (long s = 0; s < cs; s++) {
            for (long r = 0; r < ny - cr; r++) {
                for (long c = 0; c < cc; c++) {
                    out.setFloat((nz - cs + s) * sstride + r * nx + (nx - cc + c), in.getFloat(s * sstride + (r + cr) * nx + c));
                }
            }
        }

        for (long s = 0; s < cs; s++) {
            for (long r = 0; r < cr; r++) {
                for (long c = 0; c < nx - cc; c++) {
                    out.setFloat((nz - cs + s) * sstride + (ny - cr + r) * nx + c, in.getFloat(s * sstride + r * nx + c + cc));
                }
            }
        }

        for (long s = 0; s < cs; s++) {
            for (long r = 0; r < cr; r++) {
                for (long c = 0; c < cc; c++) {
                    out.setFloat((nz - cs + s) * sstride + (ny - cr + r) * nx + (nx - cc + c), in.getFloat(s * sstride + r * nx + c));
                }
            }
        }

        return out;
    }

    public static float[] inverseCircShift_3D(float[] in, int nx, int ny, int nz) {
        int cs = (int) floor(nz / 2.0);
        int cr = (int) floor(ny / 2.0);
        int cc = (int) floor(nx / 2.0);
        int sstride = ny * nx;
        float[] out = new float[nz * ny * nx];
        for (int s = 0; s < nz - cs; s++) {
            for (int r = 0; r < ny - cr; r++) {
                for (int c = 0; c < nx - cc; c++) {
                    out[(s + cs) * sstride + (r + cr) * nx + (c + cc)] = in[s * sstride + r * nx + c];
                }
            }
        }
        for (int s = 0; s < nz - cs; s++) {
            for (int r = 0; r < cr; r++) {
                for (int c = 0; c < nx - cc; c++) {
                    out[(s + cs) * sstride + r * nx + (c + cc)] = in[s * sstride + (ny - cr + r) * nx + c];
                }
            }
        }

        for (int s = 0; s < nz - cs; s++) {
            for (int r = 0; r < ny - cr; r++) {
                for (int c = 0; c < cc; c++) {
                    out[(s + cs) * sstride + (r + cr) * nx + c] = in[s * sstride + r * nx + (nx - cc + c)];
                }
            }
        }

        for (int s = 0; s < nz - cs; s++) {
            for (int r = 0; r < cr; r++) {
                for (int c = 0; c < cc; c++) {
                    out[(s + cs) * sstride + r * nx + c] = in[s * sstride + (ny - cr + r) * nx + (nx - cc + c)];
                }
            }
        }

        for (int s = 0; s < cs; s++) {
            for (int r = 0; r < ny - cr; r++) {
                for (int c = 0; c < nx - cc; c++) {
                    out[s * sstride + (r + cr) * nx + c + cc] = in[(nz - cs + s) * sstride + r * nx + c];
                }
            }
        }

        for (int s = 0; s < cs; s++) {
            for (int r = 0; r < ny - cr; r++) {
                for (int c = 0; c < cc; c++) {
                    out[s * sstride + (r + cr) * nx + c] = in[(nz - cs + s) * sstride + r * nx + (nx - cc + c)];
                }
            }
        }

        for (int s = 0; s < cs; s++) {
            for (int r = 0; r < cr; r++) {
                for (int c = 0; c < nx - cc; c++) {
                    out[s * sstride + r * nx + c + cc] = in[(nz - cs + s) * sstride + (ny - cr + r) * nx + c];
                }
            }
        }

        for (int s = 0; s < cs; s++) {
            for (int r = 0; r < cr; r++) {
                for (int c = 0; c < cc; c++) {
                    out[s * sstride + r * nx + c] = in[(nz - cs + s) * sstride + (ny - cr + r) * nx + (nx - cc + c)];
                }
            }
        }

        return out;
    }

    public static FloatLargeArray inverseCircShift_3D(FloatLargeArray in, long nx, long ny, long nz) {
        long cs = (long) floor(nz / 2.0);
        long cr = (long) floor(ny / 2.0);
        long cc = (long) floor(nx / 2.0);
        long sstride = ny * nx;
        FloatLargeArray out = new FloatLargeArray(nz * ny * nx);
        for (long s = 0; s < nz - cs; s++) {
            for (long r = 0; r < ny - cr; r++) {
                for (long c = 0; c < nx - cc; c++) {
                    out.setFloat((s + cs) * sstride + (r + cr) * nx + (c + cc), in.getFloat(s * sstride + r * nx + c));
                }
            }
        }
        for (long s = 0; s < nz - cs; s++) {
            for (long r = 0; r < cr; r++) {
                for (long c = 0; c < nx - cc; c++) {
                    out.setFloat((s + cs) * sstride + r * nx + (c + cc), in.getFloat(s * sstride + (ny - cr + r) * nx + c));
                }
            }
        }

        for (long s = 0; s < nz - cs; s++) {
            for (long r = 0; r < ny - cr; r++) {
                for (long c = 0; c < cc; c++) {
                    out.setFloat((s + cs) * sstride + (r + cr) * nx + c, in.getFloat(s * sstride + r * nx + (nx - cc + c)));
                }
            }
        }

        for (long s = 0; s < nz - cs; s++) {
            for (long r = 0; r < cr; r++) {
                for (long c = 0; c < cc; c++) {
                    out.setFloat((s + cs) * sstride + r * nx + c, in.getFloat(s * sstride + (ny - cr + r) * nx + (nx - cc + c)));
                }
            }
        }

        for (long s = 0; s < cs; s++) {
            for (long r = 0; r < ny - cr; r++) {
                for (long c = 0; c < nx - cc; c++) {
                    out.setFloat(s * sstride + (r + cr) * nx + c + cc, in.getFloat((nz - cs + s) * sstride + r * nx + c));
                }
            }
        }

        for (long s = 0; s < cs; s++) {
            for (long r = 0; r < ny - cr; r++) {
                for (long c = 0; c < cc; c++) {
                    out.setFloat(s * sstride + (r + cr) * nx + c, in.getFloat((nz - cs + s) * sstride + r * nx + (nx - cc + c)));
                }
            }
        }

        for (long s = 0; s < cs; s++) {
            for (long r = 0; r < cr; r++) {
                for (long c = 0; c < nx - cc; c++) {
                    out.setFloat(s * sstride + r * nx + c + cc, in.getFloat((nz - cs + s) * sstride + (ny - cr + r) * nx + c));
                }
            }
        }

        for (long s = 0; s < cs; s++) {
            for (long r = 0; r < cr; r++) {
                for (long c = 0; c < cc; c++) {
                    out.setFloat(s * sstride + r * nx + c, in.getFloat((nz - cs + s) * sstride + (ny - cr + r) * nx + (nx - cc + c)));
                }
            }
        }

        return out;
    }
}
