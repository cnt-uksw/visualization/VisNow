/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.utils.io;

import java.io.File;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.commons.io.FilenameUtils;
import org.visnow.jscic.Field;
import org.visnow.vn.lib.basic.writers.FieldWriter.FieldWriterFileFormat;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.swing.filechooser.VNFileChooser;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.system.utils.usermessage.UserMessage;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class VisNowFieldWriter
{

    public static final int ASCII_SIZE_LIMIT = 10000;

    public static String createUnitString(Field field)
    {
        String[] coordNames = {"x ", "y ", "z "};
        String[] coordUnits = field.getCoordsUnits();
        String timeUnit     = field.getTimeUnit();
        String xUnit = "";
        int coordUnitType = 0;
        if (coordUnits != null && coordUnits.length > 0) {
            xUnit = coordUnits[0];
            if (!xUnit.isEmpty() && !xUnit.equals("1"))
                coordUnitType = 1;
            for (int i = 1; i < coordUnits.length; i++)
                if (!coordUnits[i].equals(xUnit))
                    coordUnitType = 2;
        }
        boolean isTimeUnit = timeUnit != null && !timeUnit.isEmpty() && !timeUnit.equals("1");
        if (coordUnitType == 0 && !isTimeUnit)
            return (null);
        StringBuilder unitBuilder = new StringBuilder("units ");
        if (isTimeUnit)
            unitBuilder.append("time ").append(timeUnit);
        if (coordUnitType == 1) {
            if (isTimeUnit)
                unitBuilder.append(", ");
            unitBuilder.append("coords ").append(xUnit);
        }
        else if (coordUnitType == 2) {
            if (isTimeUnit)
                unitBuilder.append(", ");
            for (int i = 0; i < coordUnits.length; i++) {
                unitBuilder.append(coordNames[i]).append(coordUnits[i]);
                if (i < coordUnits.length - 1)
                    unitBuilder.append(", ");
            }
        }
        return unitBuilder.toString();
    }

    /**
     * Writes field to a file.
     *
     * @param field     field
     * @param path      file path
     * @param format    file format
     * @param overwrite if true, then existing header and data files will be overwritten
     *
     * @return true if the operation was successful, false otherwise
     *
     * @throws FileAlreadyExistsException if overwrite == false and output files already exist.
     * @throws FileSystemException        if cannot write to output files.
     */
    public static boolean writeField(Field field, String path, FieldWriterFileFormat format, boolean overwrite) throws FileSystemException
    {
        if (format == FieldWriterFileFormat.SERIALIZED)
            return SerializedFieldWriter.writeField(field, FilenameUtils.removeExtension(path) + ".vns", overwrite);
        else
            return FieldWriter.writeField(field, path, format == FieldWriterFileFormat.VNF_BINARY, overwrite);
    }

    /**
     * Tests whether the application can modify the file denoted by path. A confirm dialog box will appear if the file exists.
     *
     * @param path   file path
     * @param format file format
     *
     * @return true if the application can modify the file denoted by path and the user confirmed the existing file can be overwritten, false otherwise.
     */
    public static boolean canWriteToOutputFile(String path, FieldWriterFileFormat format)
    {
        String baseName = FilenameUtils.removeExtension(path);
        String outHeaderFileName = format != FieldWriterFileFormat.SERIALIZED ? baseName + "." + format.getHeaderFileExtension() : null;
        String outDataFileName = baseName + "." + format.getDataFileExtension();
        boolean result = true;

        if (outHeaderFileName != null) {
            File outHeaderFile = new File(outHeaderFileName);
            if (!outHeaderFile.getParentFile().canWrite()) {
                JOptionPane.showMessageDialog(null, "Cannot write file (check write permissions)");
                return false;
            }
            if (outHeaderFile.exists()) {
                Object ans = JOptionPane.showConfirmDialog(null,
                                                           "File " + outHeaderFile.getName() + " exists.\nOwerwrite?", "",
                                                           JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                result = ans instanceof Integer && (Integer) ans == JOptionPane.YES_OPTION;
            }
        }
        File outDataFile = new File(outDataFileName);
        if (!outDataFile.getParentFile().canWrite()) {
            JOptionPane.showMessageDialog(null, "Cannot write file (check write permissions)");
            return false;
        }
        if (outDataFile.exists()) {
            Object ans;
            if (format != FieldWriterFileFormat.SERIALIZED) {
                ans = JOptionPane.showConfirmDialog(null, "Data file " + outDataFile.getName() + " exists in the selected target directory\n" +
                                                          "and may be referenced by other VNF files.\n"  +
                                                          "This operation will overwrite this data file.\nContinue?", "",
                                                          JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            } else {
                ans = JOptionPane.showConfirmDialog(null, "File " + outDataFile.getName() + " exists.\nOwerwrite?", "", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            }
            result = result && (ans instanceof Integer && (Integer) ans == JOptionPane.YES_OPTION);
        }
        return result;
    }

    public static void writeInvokedFromPortOrConnection(Field field, boolean asAscii, boolean asSerialized)
    {
        if (asAscii && field.getNNodes() > ASCII_SIZE_LIMIT)
            asAscii = (JOptionPane.showConfirmDialog(null,
                    "Writing large field in ASCII form is not particularly efficient. Force ASCII format?",
                    "choose one", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION);


        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(org.visnow.vn.lib.basic.writers.FieldWriter.FieldWriter.class)));
        FileNameExtensionFilter fieldFilter;
        if (asSerialized) {
            fieldFilter = new FileNameExtensionFilter("VisNow Field files (serialized)", "vns", "VNS");
        } else {
            fieldFilter = new FileNameExtensionFilter("VisNow Field files (VNF)", "vnf", "VNF");
        }
        fileChooser.setFileFilter(fieldFilter);

        int returnVal = fileChooser.showSaveDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            String path;
            if (asSerialized) {
                path = VNFileChooser.filenameWithExtenstionAddedIfNecessary(fileChooser.getSelectedFile(), new FileNameExtensionFilter("", "vns", "VNS"));
            } else {
                path = VNFileChooser.filenameWithExtenstionAddedIfNecessary(fileChooser.getSelectedFile(), new FileNameExtensionFilter("", "vnf", "VNF"));
            }
            VisNow.get().getMainConfig().setLastDataPath(path.substring(0, path.lastIndexOf(File.separator)), org.visnow.vn.lib.basic.writers.FieldWriter.FieldWriter.class);
            FieldWriterFileFormat format = asSerialized ? FieldWriterFileFormat.SERIALIZED : (asAscii ? FieldWriterFileFormat.VNF_ASCII : FieldWriterFileFormat.VNF_BINARY);
            if (VisNowFieldWriter.canWriteToOutputFile(path, format)) {
                try {
                    VisNowFieldWriter.writeField(field, path, format, true);
                    VisNow.get().userMessageSend(new UserMessage("VisNow", "", "Field successfully written", "", Level.INFO));
                } catch (Exception ex) {
                    VisNow.get().userMessageSend(new UserMessage("VisNow", "", "Error writing field", ex.getMessage(), Level.ERROR));
                }
            } else {
                VisNow.get().userMessageSend(new UserMessage("VisNow", "", "Error writing field", "", Level.ERROR));
            }
        }
    }
}
