/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.utils.io;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemException;
import org.apache.log4j.Logger;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import org.visnow.jlargearrays.UnsignedByteLargeArray;

/**
 * @author Krzysztof Nowinski (know@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class IrregularFieldWriter extends FieldWriter
{

    private final IrregularField irregularField;

    private static final Logger LOGGER = Logger.getLogger(IrregularFieldWriter.class);


    /**
     * Creates a new instance of IrregularFieldWriter.
     *
     * @param irregularField  the field to be written
     * @param path       a string indicating the directory and the base file name for the .vnf file
     * @param binary     true if writing binary data files and ascii string components file, false if all data will be written to an ASCII file
     * @param overwrite  if true, then existing header and data files will be overwritten
     *
     * @throws FileAlreadyExistsException if overwrite == false and output files already exist.
     * @throws FileSystemException        if cannot write to output files.
     */
    public IrregularFieldWriter(IrregularField irregularField, String path, boolean binary, boolean overwrite) throws FileSystemException, IOException
    {
        super(irregularField, path, binary, overwrite);
        this.irregularField = irregularField;
    }

    private String clean(String s)
    {
        return s.replaceAll("\\s", "_").replaceAll(",", "").
                 replaceAll("\\.", "").replaceAll("=", "").
                 replaceAll(":", "");
    }

    private boolean writeCellSetBinary(CellSet cellSet, PrintWriter headerWriter, MemoryMappedFileWriter largeContentOutput) throws Exception
    {
        String setName = clean(cellSet.getName());
        CellType[] cellTypes = Cell.getProperCellTypes();
        for (CellType cellType : cellTypes) {
            if (cellSet.getCellArray(cellType) != null) {
                CellArray cellArray = cellSet.getCellArray(cellType);
                headerWriter.println(setName + ":" + cellType.getPluralName() + ":nodes");
                IntLargeArray nodes = new IntLargeArray(cellArray.getNodes());
                largeContentOutput.writeIntLargeArray(nodes, 0, nodes.length());
                if (cellArray.getDataIndices() != null) {
                    headerWriter.println(setName + ":" + cellType.getPluralName() + ":indices");
                    nodes = new IntLargeArray(cellArray.getDataIndices());
                    largeContentOutput.writeIntLargeArray(nodes, 0, nodes.length());
                }
                if (cellArray.getOrientations() != null) {
                    UnsignedByteLargeArray orientations = new UnsignedByteLargeArray(cellArray.getOrientations());
                    headerWriter.println(setName + ":" + cellType.getPluralName() + ":orientations");
                    largeContentOutput.writeUnsignedByteLargeArray(orientations, 0, orientations.length());
                }
            }
        }
        if (cellSet.getNComponents() > 0)
            return WriteContainer.writeBinary(cellSet, setName, headerWriter, largeContentOutput);
        else
            return true;
    }

    private boolean writeCellSetASCII(CellSet cellSet, PrintWriter headerWriter, PrintWriter contentWriter, int nDigits) throws Exception
    {
        String setName = clean(cellSet.getName());
        headerWriter.println("skip 1");
        contentWriter.println(setName);
        CellType[] cellTypes = Cell.getProperCellTypes();
        for (CellType cellType : cellTypes)
            if (cellSet.getCellArray(cellType) != null) {
                headerWriter.println("skip 2");
                CellArray cellArray = cellSet.getCellArray(cellType);
                contentWriter.println(cellType.getPluralName());
                CellArray ca = cellSet.getCellArray(cellType);
                int[] nodes = ca.getNodes();
                int nn = cellType.getNVertices();
                headerWriter.print(setName + ":" + cellType.getPluralName() + ":nodes, ");
                contentWriter.printf("%" + ((nDigits + 1) * nn) + "s  ", "nodes     ");
                if (cellArray.getDataIndices() != null) {
                    headerWriter.print(setName + ":" + cellType.getPluralName() + ":indices, ");
                    contentWriter.print("   indices");
                }
                if (cellArray.getOrientations() != null) {
                    headerWriter.print(setName + ":" + cellType.getPluralName() + ":orientations, ");
                    contentWriter.print(" orientations");
                }
                contentWriter.println();
                headerWriter.println();
                for (int i = 0; i < ca.getNCells(); i++) {
                    for (int j = 0; j < nn; j++)
                        contentWriter.printf("%" + nDigits + "d ", nodes[nn * i + j]);
                    if (cellArray.getDataIndices() != null)
                        contentWriter.printf("%" + nDigits + "d ", cellArray.getDataIndices()[i]);
                    if (cellArray.getOrientations() != null)
                        contentWriter.print(cellArray.getOrientations()[i] == 1 ? "  1" : "  0");
                    contentWriter.println();
                }
            }
        if (cellSet.getNComponents() > 0)
           return  WriteContainer.writeASCII(cellSet, setName, headerWriter, contentWriter);
        else
            return true;
    }

    @Override
    public boolean writeField()
    {

        boolean status = true;
        try {
            headerWriter.println("#VisNow irregular field");
            if (irregularField.getName() != null)
                headerWriter.print("field \"" + irregularField.getName() + "\"");
            headerWriter.print(", nnodes = " + irregularField.getNNodes());
            if (irregularField.hasMask())
                headerWriter.print(", mask");
            if (irregularField.getUserData() != null) {
                headerWriter.print(", user:");
                String[] udata = irregularField.getUserData();
                for (int j = 0; j < udata.length; j++) {
                    if (j > 0)
                        headerWriter.print(";");
                    headerWriter.print("\"" + udata[j] + "\"");
                }
            }
            headerWriter.println();
            String unitString = VisNowFieldWriter.createUnitString(irregularField);
            if (unitString != null)
                headerWriter.println(unitString);
            writeExtents();
            WriteContainer.writeHeader(inField, headerWriter);
            for (CellSet cellSet : irregularField.getCellSets()) {
                int nData = 0;
                if (cellSet.getNComponents() > 0)
                    nData = (int)cellSet.getComponent(0).getNElements();
                headerWriter.print("CellSet " + cellSet.getName().replaceAll("\\s", "_").replaceAll(",", "").replaceAll("\\.", "").replaceAll("=", "").replaceAll(":", ""));
                if (nData > 0)
                    headerWriter.println(", nData " + nData);
                else
                    headerWriter.println();
                String filler = "             ";
                CellType[] cellTypes = Cell.getProperCellTypes();
                for (CellType cellType : cellTypes)
                    if (cellSet.getCellArray(cellType) != null) {
                        CellArray cellArray = cellSet.getCellArray(cellType);
                        if (cellArray.getNCells() > 1)
                            headerWriter.printf("%s%s%7d%n", cellType.getPluralName(),
                                                filler.substring(0, 12 - cellType.getPluralName().length()),
                                                cellArray.getNCells());
                        else
                            headerWriter.printf("%s%s%7d%n", cellType.getName(),
                                                filler.substring(0, 12 - cellType.getName().length()),
                                                cellArray.getNCells());
                    }
                if (cellSet.getNComponents() > 0)
                    WriteContainer.writeHeader(cellSet, headerWriter);
            }
            if (binary) {
                printDataFileHeader();
                status = status && WriteContainer.writeBinary(inField, "", headerWriter, largeContentOutput);
            } else {
                printAsciiFileHeader();
                if (!WriteContainer.writeASCII(inField, "", headerWriter, contentWriter))
                    status = false;
            }
            int nDigits = (int)(Math.log10(irregularField.getNNodes())) + 1;
            for (CellSet cellSet : irregularField.getCellSets()) {
                if (binary)
                    status = status && writeCellSetBinary(cellSet, headerWriter, largeContentOutput);
                else
                    status = status && writeCellSetASCII(cellSet, headerWriter, contentWriter, nDigits);
            }
            if (binary && strings) {
                printAsciiFileHeader();
                status = status && WriteContainer.writeStrings(irregularField, "", headerWriter, contentWriter);
                for (CellSet cellSet : irregularField.getCellSets()) {
                    String setName = clean(cellSet.getName());
                    if (cellSet.getNComponents() > 0)
                        status = status && WriteContainer.writeStrings(cellSet, setName, headerWriter, contentWriter);
                }
            }
        }
        catch (Exception e) {
            status = false;
            LOGGER.error("Could not write field ", e);
        }
        if (!status)
            LOGGER.error("Could not write field ", null);
        return status;
    }
}
