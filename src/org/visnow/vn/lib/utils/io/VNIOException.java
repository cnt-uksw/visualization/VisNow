/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */

package org.visnow.vn.lib.utils.io;

/**
 * This exception contains all data necessary to locate an error in vnf data. It is intended to be thrown
 * all the way up to the calling module level and then handled by displaying FileErrorFrame
 * and silent finalizing the module execution with null outputs. Since the FileErrorFrame allows
 * to edit offending files the user can fix them and reread the .vnf file.
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */

public class VNIOException extends Exception
{
    public final String text;
    public final String fname;
    public final int lineNumber;
    public final String vnfName;
    public final int vnfLineNumber;
    public final Exception ex;

/**
 * The constructor to be used if the error has been detected when parsing .vnf or when reading a binary file
 * @param text        error description
 * @param fname       name of the offending file (.vnf)
 * @param lineNumber  location of the error (-1 when error has been detected in a binary file)
 */
    public VNIOException(String text, String fname, int lineNumber)
    {
        this(text, fname, lineNumber, "", -1);
    }

/**
 * The constructor to be used if the error has been detected when parsing .vnf or when reading a binary file
 * @param text        error description
 * @param fname       name of the offending file (.vnf)
 * @param lineNumber  location of the error (-1 when error has been detected in a binary file)
 */
    public VNIOException(String text, String fname, int lineNumber, Exception ex)
    {
        this(text, fname, lineNumber, "", -1, ex);
    }

/**
 * The constructor to be used if the error has been detected when reading a text file
 * @param text             error description
 * @param fname            name of the offending file
 * @param lineNumber       location of the error
 * @param vnfName          name of the .vnf file
 * @param vnfLineNumber    line containing the offending file section description
 */
    public VNIOException(String text, String fname, int lineNumber, String vnfName, int vnfLineNumber)
    {
        this(text, fname, lineNumber, vnfName, vnfLineNumber, null);
    }


/**
 * The constructor to be used if the error has been detected when reading a text file
 * @param text             error description
 * @param fname            name of the offending file
 * @param lineNumber       location of the error
 * @param vnfName          name of the .vnf file
 * @param vnfLineNumber    line containing the offending file section description
 * @param ex               if the VNIOException is raised because of some exception ex, ex is passed
 */
    public VNIOException(String text, String fname, int lineNumber, String vnfName, int vnfLineNumber, Exception ex)
    {
        this.text = text;
        this.fname = fname;
        this.lineNumber = lineNumber;
        this.vnfName = vnfName;
        this.vnfLineNumber = vnfLineNumber;
        this.ex = ex;
    }

}
