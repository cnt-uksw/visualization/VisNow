/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 

package org.visnow.vn.lib.utils.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import javax.imageio.stream.FileImageOutputStream;
import org.apache.log4j.Logger;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class AVSRegularFieldWriter
{

    private final RegularField regularInField;
    private final boolean isSingleFile;
    private final String fileName;
    private final boolean asciiFormat;
    private PrintWriter headerWriter;
    private PrintWriter contentWriter;
    protected static final Logger LOGGER = Logger.getLogger(AVSRegularFieldWriter.class);
    

    public AVSRegularFieldWriter(RegularField regularInField, String fileName, boolean asciiFormat, boolean isSingleFile)
    {
        this.regularInField = regularInField;
        this.fileName = fileName;
        this.asciiFormat = asciiFormat;
        this.isSingleFile = isSingleFile;
    }

    private static void printASCIIColumns(String genFileName, RegularField regularInField)
    {
        try {
            try (PrintWriter contentWriter = new PrintWriter(new FileOutputStream(genFileName + ".data"))) {
                for (int i = 0; i < regularInField.getNNodes(); i++) {
                    for (int j = 0; j < regularInField.getNComponents(); j++) {
                        DataArray da = regularInField.getComponent(j);
                        int vl = da.getVectorLength();
                        switch (da.getType()) {
                            case FIELD_DATA_BYTE:
                                for (int k = 0; k < vl; k++)
                                    contentWriter.printf("%3d ", ((byte[]) da.getRawArray().getData())[i * vl + k] & 0xff);
                                break;
                            case FIELD_DATA_SHORT:
                                for (int k = 0; k < vl; k++)
                                    contentWriter.printf("%3d ", ((short[]) da.getRawArray().getData())[i * vl + k]);
                                break;
                            case FIELD_DATA_INT:
                                for (int k = 0; k < vl; k++)
                                    contentWriter.printf("%6d ", ((int[]) da.getRawArray().getData())[i * vl + k]);
                                break;
                            case FIELD_DATA_FLOAT:
                                for (int k = 0; k < vl; k++)
                                    contentWriter.printf("%9f ", ((float[]) da.getRawArray().getData())[i * vl + k]);
                                break;
                            case FIELD_DATA_DOUBLE:
                                for (int k = 0; k < vl; k++)
                                    contentWriter.printf("%12f ", ((double[]) da.getRawArray().getData())[i * vl + k]);
                                break;
                        }
                    }
                    contentWriter.println();
                }
            }
        } catch (Exception e) {
            System.out.println("could not write file " + genFileName + ".data");
        }
    }

    private static void printAVSLabelsUnits(PrintWriter headerWriter, RegularField regularInField)
    {
        boolean nonEmptyUnits = false;
        headerWriter.print("label = ");
        for (int i = 0; i < regularInField.getNComponents(); i++) {
            int vn = regularInField.getComponent(i).getVectorLength();
            if (vn == 1)
                headerWriter.print(regularInField.getComponent(i).getName().replace(' ', '_') + " ");
            else
                for (int j = 0; j < vn; j++)
                    headerWriter.print(regularInField.getComponent(i).getName().replace(' ', '_') + j + " ");
        }
        headerWriter.println();
        for (int i = 0; i < regularInField.getNComponents(); i++) {
            DataArray comp = regularInField.getComponent(i);
            if (comp.getUnit() != null && !comp.getUnit().isEmpty()) {
                nonEmptyUnits = true;
                break;
            }
        }
        if (nonEmptyUnits) {
            headerWriter.print("unit = ");
            for (int i = 0; i < regularInField.getNComponents(); i++) {
                DataArray comp = regularInField.getComponent(i);
                if (comp.getVectorLength() == 1) {
                    if (comp.getUnit() != null && !comp.getUnit().isEmpty()) {
                        headerWriter.print(comp.getUnit().replace(' ', '_') + "_ ");
                    }
                } else {
                    if (comp.getUnit() != null && !comp.getUnit().isEmpty()) {
                        for (int j = 0; j < comp.getVectorLength(); j++) {
                            headerWriter.print(comp.getUnit().replace(' ', '_') + "_ ");
                        }
                    }
                }
            }
            headerWriter.println();
        }
    }

    public boolean writeField()
    {
        String outFileName = fileName;
        String genFileName = fileName;
        boolean allByte;
        DataArray da;
        byte[] bData;
        float[] fData;
        int scalars = 0;
        FileImageOutputStream out;

        if (outFileName.endsWith(".fld"))
            genFileName = outFileName.substring(0, outFileName.lastIndexOf(".fld"));
        else
            outFileName = genFileName + ".fld";

        try {
            headerWriter = new PrintWriter(new FileOutputStream(outFileName));
            headerWriter.println("# AVS field file");
            headerWriter.println("ndim = " + regularInField.getDims().length);
            for (int i = 0; i < regularInField.getDims().length; i++) {
                headerWriter.println("dim" + (i + 1) + " = " + regularInField.getDims()[i]);
            }
            headerWriter.println("nspace = " + 3);
            allByte = true;
            scalars = 0;
            for (int i = 0; i < regularInField.getNComponents(); i++) {
                scalars += regularInField.getComponent(i).getVectorLength();
                if (regularInField.getComponent(i).getType() != DataArrayType.FIELD_DATA_BYTE)
                    allByte = false;
            }
            headerWriter.println("veclen = " + scalars);
            if (allByte)
                headerWriter.println("data = byte");
            else
                headerWriter.println("data = xdr_float");
            if (regularInField.getCurrentCoords() == null) {
                headerWriter.println("field = uniform");
                headerWriter.print("min_ext = ");
                for (int i = 0; i < 3; i++)
                    headerWriter.print("" + regularInField.getPreferredExtents()[0][i] + " ");
                headerWriter.println();
                headerWriter.print("max_ext = ");
                for (int i = 0; i < 3; i++)
                    headerWriter.print("" + regularInField.getPreferredExtents()[1][i] + " ");
                headerWriter.println();
                printAVSLabelsUnits(headerWriter, regularInField);
            } else {
                headerWriter.println("field = irregular");
                printAVSLabelsUnits(headerWriter, regularInField);
                if (asciiFormat) {
                    for (int i = 0; i < 3; i++) {
                        headerWriter.print("coord " + (i + 1) + " file=" + genFileName + ".coord filetype=ASCII ");
                        headerWriter.print("stride=" + 3);
                        if (i > 0)
                            headerWriter.print(" offset=" + i);
                        headerWriter.println();
                    }
                    float[] coord = regularInField.getCurrentCoords() == null ? null : regularInField.getCurrentCoords().getData();
                    if (coord != null) {
                        contentWriter = new PrintWriter(new FileOutputStream(genFileName + ".coord"));
                        for (int i = 0; i < coord.length; i += 3) {
                            for (int j = 0; j < 3; j++)
                                contentWriter.printf("%7f ", coord[i + j]);
                            contentWriter.println();
                        }
                        contentWriter.close();
                    }
                } else {
                    for (int i = 0; i < 3; i++) {
                        headerWriter.print("coord " + (i + 1) + " file=" + genFileName + ".coord filetype=binary ");
                        headerWriter.print("stride=" + 3);
                        if (i > 0)
                            headerWriter.print(" skip=" + (4 * i));
                        headerWriter.println();
                    }
                    float[] coord = regularInField.getCurrentCoords() == null ? null : regularInField.getCurrentCoords().getData();
                    out = new FileImageOutputStream(new File(genFileName + ".coord"));
                    out.writeFloats(coord, 0, coord.length);
                    out.close();
                }
            }
            if (asciiFormat) {
                for (int i = 0, k = 0; i < regularInField.getNComponents(); i++) {
                    da = regularInField.getComponent(i);
                    for (int j = 0; j < da.getVectorLength(); j++, k++) {
                        if (k > 0)
                            headerWriter.println("variable " + (k + 1) + " file=" + genFileName + ".data filetype=ASCII stride=" + scalars + " offset=" + k);
                        else
                            headerWriter.println("variable " + (k + 1) + " file=" + genFileName + ".data filetype=ASCII stride=" + scalars);
                    }
                }
                printASCIIColumns(genFileName, regularInField);
            } else {
                int dSize = 4;
                if (allByte)
                    dSize = 1;
                if (isSingleFile) {
                    out = new FileImageOutputStream(new File(genFileName + ".data"));
                    for (int i = 0, k = 0; i < regularInField.getNComponents(); i++) {
                        da = regularInField.getComponent(i);
                        for (int j = 0; j < da.getVectorLength(); j++, k++)
                            headerWriter.println("variable " + (k + 1) +
                                " file=" + genFileName + ".data filetype=binary skip =" +
                                (out.getStreamPosition() + (j * dSize)) + " stride=" + da.getVectorLength());
                        if (allByte) {
                            bData = (byte[]) da.getRawArray().getData();
                            out.write(bData, 0, bData.length);
                        } else {
                            fData = (float[]) da.getRawArray().getData();
                            out.writeFloats(fData, 0, fData.length);
                        }
                    }
                    out.close();
                } else {
                    for (int i = 0, k = 0; i < regularInField.getNComponents(); i++) {
                        da = regularInField.getComponent(i);
                        String outFname = genFileName + "_" + da.getName().replace(' ', '_') + ".dat";
                        for (int j = 0; j < da.getVectorLength(); j++, k++) {
                            headerWriter.print("variable " + (k + 1) + " file=" + outFname + " filetype=binary");
                            if (da.getVectorLength() > 1) {
                                if (k == 0)
                                    headerWriter.println(" stride=" + da.getVectorLength());
                                else
                                    headerWriter.println(" skip=" + (j * dSize) + " stride=" + da.getVectorLength());
                            } else
                                headerWriter.println();
                        }
                        out = new FileImageOutputStream(new File(outFname));
                        if (allByte) {
                            bData = (byte[]) da.getRawArray().getData();
                            out.write(bData, 0, bData.length);
                        } else {
                            fData = (float[]) da.getRawArray().getData();
                            out.writeFloats(fData, 0, fData.length);
                        }
                        out.close();
                    }
                }
            }
            headerWriter.close();
            return true;
        } catch (IOException e) {
            LOGGER.error("Error writing field", e);
            return false;
        }
    }
}
