/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 

package org.visnow.vn.lib.utils.io;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.visnow.jscic.PointField;

/**
 * @author Krzysztof Nowinski (know@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class PointFieldWriter extends FieldWriter
{

    private static final Logger LOGGER = Logger.getLogger(PointFieldWriter.class);
    private final PointField pointField;

    /**
     * Creates a new instance of PointFieldWriter.
     *
     * @param pointField the field to be written
     * @param path       a string indicating the directory and the base file name for the .vnf file
     * @param binary     true if writing binary data files and ascii string components file, false if all data will be written to an ASCII file
     * @param overwrite  if true, then existing header and data files will be overwritten
     * @throws FileAlreadyExistsException if overwrite == false and output files already exist.
     * @throws FileSystemException        if cannot write to output files.
     */
    public PointFieldWriter(PointField pointField, String path, boolean binary, boolean overwrite) throws FileSystemException, IOException
    {
        super(pointField, path, binary, overwrite);
        this.pointField = pointField;
    }

    @Override
    public boolean writeField()
    {
        boolean status;
        try {
            headerWriter.println("#VisNow point field file");
            headerWriter.print("field = " + pointField.getName() + ", nnodes = " + pointField.getNNodes());
            if (pointField.hasMask())
                headerWriter.println(", mask");
            else
                headerWriter.println();
            WriteContainer.writeHeader(inField, headerWriter);
            if (binary) {
                printDataFileHeader();
                WriteContainer.writeBinary(pointField, "", headerWriter, largeContentOutput);
                if (strings) {
                    printAsciiFileHeader();
                    WriteContainer.writeStrings(pointField, "", headerWriter, contentWriter);
                }
            }
            else {
                printAsciiFileHeader();
                WriteContainer.writeASCII(pointField, "", headerWriter, contentWriter);
            }
            status =  true;
        }   catch (Exception ex) {
            LOGGER.log(Level.ERROR, null, ex);
            status = false;
        }
        closeAll();
        return status;
    }
}
