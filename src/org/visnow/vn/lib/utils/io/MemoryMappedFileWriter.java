/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.utils.io;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LongLargeArray;
import org.visnow.jlargearrays.ShortLargeArray;

/**
 *
 * @author piotrw
 */
public class MemoryMappedFileWriter
{

    private RandomAccessFile raf;
    private ByteOrder byteOrder;
    private final long CHUNKSIZE = (1 << 27);
    private long writerOffset;

    public MemoryMappedFileWriter(RandomAccessFile raf)
    {
        this(raf, ByteOrder.BIG_ENDIAN);
    }

    public MemoryMappedFileWriter(RandomAccessFile raf, ByteOrder byteOrder)
    {
        if (raf == null) {
            throw new IllegalArgumentException("raf == null!");
        }
        this.raf = raf;
        if (byteOrder == null) {
            throw new IllegalArgumentException("byteOrder == null!");
        }
        this.byteOrder = byteOrder;
        this.writerOffset = 0;
    }

    public void writeLargeArray(LargeArray f, long off, long len) throws IOException
    {
        switch (f.getType()) {
            case LOGIC:
                writeLogicLargeArray((LogicLargeArray) f, off, len);
                break;
            case UNSIGNED_BYTE:
                writeUnsignedByteLargeArray((UnsignedByteLargeArray) f, off, len);
                break;
            case SHORT:
                writeShortLargeArray((ShortLargeArray) f, off, len);
                break;
            case INT:
                writeIntLargeArray((IntLargeArray) f, off, len);
                break;
            case LONG:
                writeLongLargeArray((LongLargeArray) f, off, len);
                break;
            case FLOAT:
                writeFloatLargeArray((FloatLargeArray) f, off, len);
                break;
            case DOUBLE:
                writeDoubleLargeArray((DoubleLargeArray) f, off, len);
                break;
            default:
                throw new IllegalArgumentException("Unsupported array type");
        }
    }

    public void writeLogicLargeArray(LogicLargeArray f, long off, long len) throws IOException
    {
        if (off < 0 || len < 0 || off + len > f.length() || off + len < 0) {
            throw new IndexOutOfBoundsException("off < 0 || len < 0 || off + len > f.length()!");
        }
        int sizeof = 1;
        int nbuffers = (int) (len / CHUNKSIZE) + 1;
        long buffsize = len < CHUNKSIZE ? len * sizeof : CHUNKSIZE * sizeof;
        long idx = off;
        for (int i = 0; i < nbuffers; i++) {
            if (i < nbuffers - 1) {
                MappedByteBuffer buffer = raf.getChannel().map(FileChannel.MapMode.READ_WRITE, writerOffset + i * buffsize, buffsize);
                long end = (long) (i + 1) * buffsize;
                for (long j = i * buffsize; j < end; j += sizeof) {
                    buffer.put(f.getByte(idx++));
                }
                buffer = null;
            } else {
                MappedByteBuffer buffer = raf.getChannel().map(FileChannel.MapMode.READ_WRITE, writerOffset + i * buffsize, sizeof * len - i * buffsize);
                for (long j = i * buffsize; j < sizeof * len; j += sizeof) {
                    buffer.put(f.getByte(idx++));
                }
                buffer = null;
            }
        }
        writerOffset += len * sizeof;
    }

    public void writeUnsignedByteLargeArray(UnsignedByteLargeArray f, long off, long len) throws IOException
    {
        if (off < 0 || len < 0 || off + len > f.length() || off + len < 0) {
            throw new IndexOutOfBoundsException("off < 0 || len < 0 || off + len > f.length()!");
        }
        int sizeof = 1;
        int nbuffers = (int) (len / CHUNKSIZE) + 1;
        long buffsize = len < CHUNKSIZE ? len * sizeof : CHUNKSIZE * sizeof;
        long idx = off;
        for (int i = 0; i < nbuffers; i++) {
            if (i < nbuffers - 1) {
                MappedByteBuffer buffer = raf.getChannel().map(FileChannel.MapMode.READ_WRITE, writerOffset + i * buffsize, buffsize);
                long end = (long) (i + 1) * buffsize;
                for (long j = i * buffsize; j < end; j += sizeof) {
                    buffer.put(f.getByte(idx++));
                }
                buffer = null;
            } else {
                MappedByteBuffer buffer = raf.getChannel().map(FileChannel.MapMode.READ_WRITE, writerOffset + i * buffsize, sizeof * len - i * buffsize);
                for (long j = i * buffsize; j < sizeof * len; j += sizeof) {
                    buffer.put(f.getByte(idx++));
                }
                buffer = null;
            }
        }
        writerOffset += len * sizeof;
    }

    public void writeShortLargeArray(ShortLargeArray f, long off, long len) throws IOException
    {
        if (off < 0 || len < 0 || off + len > f.length() || off + len < 0) {
            throw new IndexOutOfBoundsException("off < 0 || len < 0 || off + len > f.length()!");
        }
        int sizeof = 2;
        int nbuffers = (int) (len / CHUNKSIZE) + 1;
        long buffsize = len < CHUNKSIZE ? len * sizeof : CHUNKSIZE * sizeof;
        long idx = off;
        for (int i = 0; i < nbuffers; i++) {
            if (i < nbuffers - 1) {
                MappedByteBuffer buffer = raf.getChannel().map(FileChannel.MapMode.READ_WRITE, writerOffset + i * buffsize, buffsize);
                buffer.order(byteOrder);
                long end = (long) (i + 1) * buffsize;
                for (long j = i * buffsize; j < end; j += sizeof) {
                    buffer.putShort(f.getShort(idx++));
                }
                buffer = null;
            } else {
                MappedByteBuffer buffer = raf.getChannel().map(FileChannel.MapMode.READ_WRITE, writerOffset + i * buffsize, sizeof * len - i * buffsize);
                buffer.order(byteOrder);
                for (long j = i * buffsize; j < sizeof * len; j += sizeof) {
                    buffer.putShort(f.getShort(idx++));
                }
                buffer = null;
            }
        }
        writerOffset += len * sizeof;
    }

    public void writeIntLargeArray(IntLargeArray f, long off, long len) throws IOException
    {
        if (off < 0 || len < 0 || off + len > f.length() || off + len < 0) {
            throw new IndexOutOfBoundsException("off < 0 || len < 0 || off + len > f.length()!");
        }
        int sizeof = 4;
        int nbuffers = (int) (len / CHUNKSIZE) + 1;
        long buffsize = len < CHUNKSIZE ? len * sizeof : CHUNKSIZE * sizeof;
        long idx = off;
        for (int i = 0; i < nbuffers; i++) {
            if (i < nbuffers - 1) {
                MappedByteBuffer buffer = raf.getChannel().map(FileChannel.MapMode.READ_WRITE, writerOffset + i * buffsize, buffsize);
                buffer.order(byteOrder);
                long end = (long) (i + 1) * buffsize;
                for (long j = i * buffsize; j < end; j += sizeof) {
                    buffer.putInt(f.getInt(idx++));
                }
                buffer = null;
            } else {
                MappedByteBuffer buffer = raf.getChannel().map(FileChannel.MapMode.READ_WRITE, writerOffset + i * buffsize, sizeof * len - i * buffsize);
                buffer.order(byteOrder);
                for (long j = i * buffsize; j < sizeof * len; j += sizeof) {
                    buffer.putInt(f.getInt(idx++));
                }
                buffer = null;
            }
        }
        writerOffset += len * sizeof;
    }

    public void writeLongLargeArray(LongLargeArray f, long off, long len) throws IOException
    {
        if (off < 0 || len < 0 || off + len > f.length() || off + len < 0) {
            throw new IndexOutOfBoundsException("off < 0 || len < 0 || off + len > f.length()!");
        }
        int sizeof = 8;
        int nbuffers = (int) (len / CHUNKSIZE) + 1;
        long buffsize = len < CHUNKSIZE ? len * sizeof : CHUNKSIZE * sizeof;
        long idx = off;
        for (int i = 0; i < nbuffers; i++) {
            if (i < nbuffers - 1) {
                MappedByteBuffer buffer = raf.getChannel().map(FileChannel.MapMode.READ_WRITE, writerOffset + i * buffsize, buffsize);
                buffer.order(byteOrder);
                long end = (long) (i + 1) * buffsize;
                for (long j = i * buffsize; j < end; j += sizeof) {
                    buffer.putLong(f.getLong(idx++));
                }
                buffer = null;
            } else {
                MappedByteBuffer buffer = raf.getChannel().map(FileChannel.MapMode.READ_WRITE, writerOffset + i * buffsize, sizeof * len - i * buffsize);
                buffer.order(byteOrder);
                for (long j = i * buffsize; j < sizeof * len; j += sizeof) {
                    buffer.putLong(f.getLong(idx++));
                }
                buffer = null;
            }
        }
        writerOffset += len * sizeof;
    }

    public void writeFloatLargeArray(FloatLargeArray f, long off, long len) throws IOException
    {
        if (off < 0 || len < 0 || off + len > f.length() || off + len < 0) {
            throw new IndexOutOfBoundsException("off < 0 || len < 0 || off + len > f.length()!");
        }
        int sizeof = 4;
        int nbuffers = (int) (len / CHUNKSIZE) + 1;
        long buffsize = len < CHUNKSIZE ? len * sizeof : CHUNKSIZE * sizeof;
        long idx = off;
        for (int i = 0; i < nbuffers; i++) {
            if (i < nbuffers - 1) {
                MappedByteBuffer buffer = raf.getChannel().map(FileChannel.MapMode.READ_WRITE, writerOffset + i * buffsize, buffsize);
                buffer.order(byteOrder);
                long end = (long) (i + 1) * buffsize;
                for (long j = i * buffsize; j < end; j += sizeof) {
                    buffer.putFloat(f.getFloat(idx++));
                }
                buffer = null;
            } else {
                MappedByteBuffer buffer = raf.getChannel().map(FileChannel.MapMode.READ_WRITE, writerOffset + i * buffsize, sizeof * len - i * buffsize);
                buffer.order(byteOrder);
                for (long j = i * buffsize; j < sizeof * len; j += sizeof) {
                    buffer.putFloat(f.getFloat(idx++));
                }
                buffer = null;
            }
        }
        writerOffset += len * sizeof;
    }

    public void writeDoubleLargeArray(DoubleLargeArray f, long off, long len) throws IOException
    {
        if (off < 0 || len < 0 || off + len > f.length() || off + len < 0) {
            throw new IndexOutOfBoundsException("off < 0 || len < 0 || off + len > f.length()!");
        }
        int sizeof = 8;
        int nbuffers = (int) (len / CHUNKSIZE) + 1;
        long buffsize = len < CHUNKSIZE ? len * sizeof : CHUNKSIZE * sizeof;
        long idx = off;
        for (int i = 0; i < nbuffers; i++) {
            if (i < nbuffers - 1) {
                MappedByteBuffer buffer = raf.getChannel().map(FileChannel.MapMode.READ_WRITE, writerOffset + i * buffsize, buffsize);
                buffer.order(byteOrder);
                long end = (long) (i + 1) * buffsize;
                for (long j = i * buffsize; j < end; j += sizeof) {
                    buffer.putDouble(f.getDouble(idx++));
                }
                buffer = null;
            } else {
                MappedByteBuffer buffer = raf.getChannel().map(FileChannel.MapMode.READ_WRITE, writerOffset + i * buffsize, sizeof * len - i * buffsize);
                buffer.order(byteOrder);
                for (long j = i * buffsize; j < sizeof * len; j += sizeof) {
                    buffer.putDouble(f.getDouble(idx++));
                }
                buffer = null;
            }
        }
        writerOffset += len * sizeof;
    }

    public void close() throws IOException
    {
        raf.close();
    }

}
