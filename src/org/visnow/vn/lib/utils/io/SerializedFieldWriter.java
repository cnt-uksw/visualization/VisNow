/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.utils.io;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.visnow.jscic.Field;

/**
 * @author Piotr Wendykier ICM, University of Warsaw
 */
public class SerializedFieldWriter
{
    private static final Logger LOGGER = Logger.getLogger(SerializedFieldWriter.class);

    public static boolean writeField(Field inField, String path, boolean overwrite)
    {
        String baseName = FilenameUtils.removeExtension(path);
        try {
            File dataFile = new File(baseName + "." + "vns");
            if (dataFile != null) {
                if (dataFile.isDirectory()) {
                    LOGGER.error("Expected file instead of directory.");
                    return false;
                }
                if (!dataFile.getParentFile().canWrite()) {
                    LOGGER.error("cannot write to file " + dataFile.getAbsolutePath());
                    return false;
                }
                if (!overwrite && dataFile.exists()) {
                    LOGGER.error("cannot overwrite file " + dataFile.getAbsolutePath());
                }
            }
            BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(dataFile));
            ObjectOutputStream objOut = new ObjectOutputStream(bout);
            objOut.writeObject(inField);
            objOut.close();
            return true;
        } catch (IOException ex) {
            return false;
        }
    }
}
