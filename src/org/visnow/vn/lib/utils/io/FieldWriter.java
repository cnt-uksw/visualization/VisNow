/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */


package org.visnow.vn.lib.utils.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemException;
import java.util.Locale;
import javax.imageio.stream.FileImageOutputStream;
import static org.apache.commons.io.FilenameUtils.*;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.PointField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public abstract class FieldWriter
{
    protected final Field inField;
    protected final File headerFile;
    protected final File dataFile;
    protected final File stringFile;
    protected final String path;
    protected final String baseName;
    protected boolean binary;
    protected boolean strings = false;
    protected boolean overwrite;
    protected PrintWriter headerWriter;
    protected PrintWriter contentWriter;
    protected FileImageOutputStream contentOutput;
    protected MemoryMappedFileWriter largeContentOutput;


    /**
     * Creates a new instance of FieldWriter.
     *
     * @param inField    field
     * @param path       a string indicating the directory and the base file name for the .vnf file
     * @param binary     true if writing binary data files and ascii string components file, false if all data will be written to an ASCII file
     * @param overwrite  if true, then existing header and data files will be overwritten
     * @throws FileAlreadyExistsException if overwrite == false and output files already exist.
     * @throws FileSystemException        if cannot write to output files.
     */
    public FieldWriter(Field inField, String path, boolean binary, boolean overwrite)
            throws IOException, IllegalArgumentException
    {
        this.path    = path;
        this.inField = inField;
        for (DataArray component : inField.getComponents())
            if (component.getType() == DataArrayType.FIELD_DATA_STRING)
                strings = true;
        if (inField instanceof IrregularField)
            for (CellSet cellSet : ((IrregularField)inField).getCellSets())
                for (DataArray component : cellSet.getComponents())
                    if (component.getType() == DataArrayType.FIELD_DATA_STRING)
                        strings = true;
        this.binary  = binary;
        baseName = removeExtension(path);
        headerFile = new File(baseName + "." + "vnf");
        if (headerFile != null) {
            if (headerFile.isDirectory())
                throw new IllegalArgumentException("Expected file instead of directory" + headerFile.getAbsolutePath());
            if (!headerFile.getParentFile().canWrite())
                throw new FileSystemException("cannot write to file " + headerFile.getAbsolutePath());
            if (overwrite == false && this.headerFile.exists())
                throw new FileAlreadyExistsException(this.headerFile.getAbsolutePath());
        }
        headerWriter = new PrintWriter(new FileOutputStream(headerFile));
        if (binary) {
            dataFile = new File(baseName + "." + "dat");
            if (dataFile != null) {
                if (dataFile.isDirectory())
                    throw new IllegalArgumentException("Expected file instead of directory" + dataFile.getAbsolutePath());
                if (!dataFile.getParentFile().canWrite())
                    throw new FileSystemException("cannot write to file " + dataFile.getAbsolutePath());
                if (overwrite == false && this.dataFile.exists())
                    throw new FileAlreadyExistsException(dataFile.getAbsolutePath());
            }
            largeContentOutput = new MemoryMappedFileWriter(new RandomAccessFile(dataFile, "rw"));
        }
        else
            dataFile = null;
        if (!binary || strings) {
            stringFile =new File(baseName + "." + "txt");
            if (stringFile != null) {
                if (stringFile.isDirectory())
                    throw new IllegalArgumentException("Expected file instead of directory.");
                if (!stringFile.getParentFile().canWrite())
                    throw new FileSystemException("cannot write to file " + stringFile.getAbsolutePath());
                if (overwrite == false && this.stringFile.exists())
                    throw new FileAlreadyExistsException(this.stringFile.getAbsolutePath());
            }
            contentWriter = new PrintWriter(new FileOutputStream(stringFile));
        }
        else
            stringFile = null;
    }

    abstract public boolean writeField();

    protected void writeExtents()
    {
        float[][] userExtents = inField.getPreferredPhysicalExtents();
        String[] cu = new String[] {"", "", ""};
        if (inField.getCoordsUnits() != null && inField.getCoordsUnits().length == 3)
            cu = inField.getCoordsUnits();
        else if (inField.getCoordsUnits() != null && inField.getCoordsUnits().length == 1) {
            String u = inField.getCoordsUnits()[0];
            cu = new String[] {u, u, u };
        }
        headerWriter.printf(Locale.US, "user extent x %10.4e %10.4e %s%n", userExtents[0][0], userExtents[1][0], cu[0]);
        headerWriter.printf(Locale.US, "user extent y %10.4e %10.4e %s%n", userExtents[0][1], userExtents[1][1], cu[1]);
        headerWriter.printf(Locale.US, "user extent z %10.4e %10.4e %s%n", userExtents[0][2], userExtents[1][2], cu[2]);
    }

    protected void printDataFileHeader()
    {
        headerWriter.println("file  \"" + getBaseName(path) + "." + "dat\" binary");
    }

    protected void printAsciiFileHeader()
    {
        headerWriter.println("file  \"" + getBaseName(path) + "." + "txt\" ascii column");
    }

    protected void closeAll()
    {
        if (headerWriter != null)
            headerWriter.close();
        if (contentWriter != null)
            contentWriter.close();
        try {
            if (largeContentOutput != null)
                largeContentOutput.close();
        } catch (Exception e) {
        }
    }

    /**
     * Writes a field to file(s).
     *
     * @return true if the operation was successful, false otherwise
     */
    public static boolean writeField(Field field, String path, boolean binary, boolean overwrite)
    {
        if (field == null)
            return false;
        boolean success = false;
        FieldWriter writer = null;
        try {
            writer = field instanceof RegularField ?
                     new RegularFieldWriter((RegularField)field, path, binary, overwrite) :
                     field instanceof IrregularField ?
                     new IrregularFieldWriter((IrregularField)field, path, binary, overwrite) :
                     new PointFieldWriter((PointField)field, path, binary, overwrite);
            success =  writer.writeField();
        } catch (Exception e) {
        }
        if (writer != null)
            writer.closeAll();
        return success;
    }

}
