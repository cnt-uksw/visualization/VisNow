/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.numeric.integration;

/**
 *
 * @author babor
 */
public class GeometricVolumes
{

    public static double hexahedronVolume(float[] coords)
    {
        if (coords == null || coords.length != 24)
            return 0;

        double[] x70 = new double[3];
        double[] x10 = new double[3];
        double[] x35 = new double[3];
        double[] x40 = new double[3];
        double[] x56 = new double[3];
        double[] x20 = new double[3];
        double[] x63 = new double[3];

        for (int i = 0; i < 3; i++) {
            x70[i] = coords[21 + i] - coords[i]; //(x7-x0)
            x10[i] = coords[3 + i] - coords[i]; //(x1-x0)
            x35[i] = coords[9 + i] - coords[15 + i]; //(x3-x5)
            x40[i] = coords[12 + i] - coords[i]; //(x4-x0)
            x56[i] = coords[15 + i] - coords[18 + i]; //(x5-x6)
            x20[i] = coords[6 + i] - coords[i]; //(x2-x0)
            x63[i] = coords[18 + i] - coords[9 + i]; //(x6-x3)
        }
        return (tripleProduct(x70, x10, x35) + tripleProduct(x70, x40, x56) + tripleProduct(x70, x20, x63)) / 6.0;
    }

    public static double hexahedronVolume(double[] coords)
    {
        if (coords == null || coords.length != 24)
            return 0;

        double[] x70 = new double[3];
        double[] x10 = new double[3];
        double[] x35 = new double[3];
        double[] x40 = new double[3];
        double[] x56 = new double[3];
        double[] x20 = new double[3];
        double[] x63 = new double[3];

        for (int i = 0; i < 3; i++) {
            x70[i] = coords[21 + i] - coords[i]; //(x7-x0)
            x10[i] = coords[3 + i] - coords[i]; //(x1-x0)
            x35[i] = coords[9 + i] - coords[15 + i]; //(x3-x5)
            x40[i] = coords[12 + i] - coords[i]; //(x4-x0)
            x56[i] = coords[15 + i] - coords[18 + i]; //(x5-x6)
            x20[i] = coords[6 + i] - coords[i]; //(x2-x0)
            x63[i] = coords[18 + i] - coords[9 + i]; //(x6-x3)
        }
        return (tripleProduct(x70, x10, x35) + tripleProduct(x70, x40, x56) + tripleProduct(x70, x20, x63)) / 6.0;
    }

    private static double tripleProduct(double[] A, double[] B, double[] C)
    {
        return A[0] * B[1] * C[2] + B[0] * C[1] * A[2] + C[0] * A[1] * B[2] - C[0] * B[1] * A[2] - B[0] * A[1] * C[2] - A[0] * C[1] * B[2];
    }

    private GeometricVolumes()
    {
    }

}
