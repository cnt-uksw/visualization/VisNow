/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.numeric.splines;

import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class SplineValueGradientDP
{

    public static double getSplineValueGradient(double[] target, int[] tDims, double[] coords, double[] gradient)
    {
        double u = coords[0];
        double v = coords[1];
        double w = coords[2];
        double val = 0;
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] = 0;
        }
        int off1 = tDims[0];
        int off2 = tDims[0] * tDims[1];
        int i = min(max((int) u, 0), tDims[0] - 1);
        u -= i;
        int j = min(max((int) v, 0), tDims[1] - 1);
        v -= j;
        int k = min(max((int) w, 0), tDims[2] - 1);
        w -= k;
        int il = -1;// if (i == 0) il = 0;
        int jl = -1;// if (j == 0) jl = 0;
        int kl = -1;// if (k == 0) kl = 0;
        int iu = 3;// if (i + iu > tDims[0])  iu = tDims[0] - i;
        int ju = 3;// if (j + ju > tDims[1])  ju = tDims[1] - j;
        int ku = 3;// if (k + ku > tDims[2])  ku = tDims[2] - k;
        double vu = 0, vv = 0, vw = 0;
        double du = 0, dv = 0, dw = 0;
        for (int k1 = kl; k1 < ku; k1++) {
            switch (k1) {
                case -1:
                    vw = ((-.5 * w + 1.0) * w - .5) * w;
                    dw = (-1.5 * w + 2.0) * w - .5;
                    break;
                case 0:
                    vw = (1.5 * w - 2.5) * w * w + 1.;
                    dw = (4.5 * w - 5.0) * w;
                    break;
                case 1:
                    vw = ((-1.5 * w + 2.) * w + .5) * w;
                    dw = (-4.5 * w + 4.) * w + .5;
                    break;
                case 2:
                    vw = (.5 * w - .5) * w * w;
                    dw = (1.5 * w - 1.) * w;
                    break;
            }
            for (int j1 = jl; j1 < ju; j1++) {
                switch (j1) {
                    case -1:
                        vv = ((-.5 * v + 1.0) * v - .5) * v;
                        dv = (-1.5 * v + 2.0) * v - .5;
                        break;
                    case 0:
                        vv = (1.5 * v - 2.5) * v * v + 1.;
                        dv = (4.5 * v - 5.0) * v;
                        break;
                    case 1:
                        vv = ((-1.5 * v + 2.) * v + .5) * v;
                        dv = (-4.5 * v + 4.) * v + .5;
                        break;
                    case 2:
                        vv = (.5 * v - .5) * v * v;
                        dv = (1.5 * v - 1.) * v;
                        break;
                }
                for (int i1 = il; i1 < iu; i1++) {
                    switch (i1) {
                        case -1:
                            vu = ((-.5 * u + 1.0) * u - .5) * u;
                            du = (-1.5 * u + 2.0) * u - .5;
                            break;
                        case 0:
                            vu = (1.5 * u - 2.5) * u * u + 1.;
                            du = (4.5 * u - 5.0) * u;
                            break;
                        case 1:
                            vu = ((-1.5 * u + 2.) * u + .5) * u;
                            du = (-4.5 * u + 4.) * u + .5;
                            break;
                        case 2:
                            vu = (.5 * u - .5) * u * u;
                            du = (1.5 * u - 1.) * u;
                            break;
                    }
                    double t = target[max(0, min(k + k1, tDims[2] - 1)) * off2 + max(0, min(j + j1, tDims[1] - 1)) * off1 + max(0, min(i + i1, tDims[0] - 1))];
                    val += (t * (vu * (vv * vw)));
                    gradient[0] += (2.0 * (t * (du * (vv * vw))));
                    gradient[1] += (2.0 * (t * (vu * (dv * vw))));
                    gradient[2] += (2.0 * (t * (vu * (vv * dw))));
                }
            }
        }
        return val;
    }

    public static double getSplineValueGradient2D(double[] target, int[] tDims, double[] coords, double[] gradient)
    {
        double u = coords[0];
        double v = coords[1];
        double val = 0;
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] = 0;
        }
        int off1 = tDims[0];
        int i = min(max((int) u, 0), tDims[0] - 1);
        u -= i;
        int j = min(max((int) v, 0), tDims[1] - 1);
        v -= j;
        int il = -1;// if (i == 0) il = 0;
        int jl = -1;// if (j == 0) jl = 0;
        int iu = 3;// if (i + iu > tDims[0])  iu = tDims[0] - i;
        int ju = 3;// if (j + ju > tDims[1])  ju = tDims[1] - j;
        double vu = 0, vv = 0;
        double du = 0, dv = 0;
        for (int j1 = jl; j1 < ju; j1++) {
            switch (j1) {
                case -1:
                    vv = ((-.5 * v + 1.0) * v - .5) * v;
                    dv = (-1.5 * v + 2.0) * v - .5;
                    break;
                case 0:
                    vv = (1.5 * v - 2.5) * v * v + 1.;
                    dv = (4.5 * v - 5.0) * v;
                    break;
                case 1:
                    vv = ((-1.5 * v + 2.) * v + .5) * v;
                    dv = (-4.5 * v + 4.) * v + .5;
                    break;
                case 2:
                    vv = (.5 * v - .5) * v * v;
                    dv = (1.5 * v - 1.) * v;
                    break;
            }
            for (int i1 = il; i1 < iu; i1++) {
                switch (i1) {
                    case -1:
                        vu = ((-.5 * u + 1.0) * u - .5) * u;
                        du = (-1.5 * u + 2.0) * u - .5;
                        break;
                    case 0:
                        vu = (1.5 * u - 2.5) * u * u + 1.;
                        du = (4.5 * u - 5.0) * u;
                        break;
                    case 1:
                        vu = ((-1.5 * u + 2.) * u + .5) * u;
                        du = (-4.5 * u + 4.) * u + .5;
                        break;
                    case 2:
                        vu = (.5 * u - .5) * u * u;
                        du = (1.5 * u - 1.) * u;
                        break;
                }
                double t = target[max(0, min(j + j1, tDims[1] - 1)) * off1 + max(0, min(i + i1, tDims[0] - 1))];
                val += (t * (vu * vv));
                gradient[0] += (2.0 * (t * (du * vv)));
                gradient[1] += (2.0 * (t * (vu * dv)));
            }
        }
        return val;
    }

    public static double getSplineValueGradient1D(double[] target, int[] tDims, double coord, double[] gradient)
    {
        double u = coord;
        double val = 0;
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] = 0;
        }
        int i = min(max((int) u, 0), tDims[0] - 1);
        u -= i;
        int il = -1;// if (i == 0) il = 0;
        int iu = 3;// if (i + iu > tDims[0])  iu = tDims[0] - i;
        double vu = 0;
        double du = 0;
        for (int i1 = il; i1 < iu; i1++) {
            switch (i1) {
                case -1:
                    vu = ((-.5 * u + 1.0) * u - .5) * u;
                    du = (-1.5 * u + 2.0) * u - .5;
                    break;
                case 0:
                    vu = (1.5 * u - 2.5) * u * u + 1.;
                    du = (4.5 * u - 5.0) * u;
                    break;
                case 1:
                    vu = ((-1.5 * u + 2.) * u + .5) * u;
                    du = (-4.5 * u + 4.) * u + .5;
                    break;
                case 2:
                    vu = (.5 * u - .5) * u * u;
                    du = (1.5 * u - 1.) * u;
                    break;
            }
            double t = target[max(0, min(i + i1, tDims[0] - 1))];
            val += t * vu;
            gradient[0] += (2.0 * (t * du));
        }
        return val;
    }
    
    
    //    public static double getSplineValueGradient(DataArray tArr, int d0, int d1, int d2, double[] coords, double[] gradient)
    //   {
    //      double u = coords[0];
    //      double v = coords[1];
    //      double w = coords[2];
    //      double val = 0;
    //      for (int i = 0; i < gradient.length; i++)
    //         gradient[i] = 0;
    //      int off1 = d0;
    //      int off2 = d0 * d1;
    //      int i = min(max((int) u, 0), d0 - 1); u -= i;
    //      int j = min(max((int) v, 0), d1 - 1); v -= j;
    //      int k = min(max((int) w, 0), d2 - 1); w -= k;
    //      int il = -1;// if (i == 0) il = 0;
    //      int jl = -1;// if (j == 0) jl = 0;
    //      int kl = -1;// if (k == 0) kl = 0;
    //      int iu =  3;// if (i + iu > tDims[0])  iu = tDims[0] - i;
    //      int ju =  3;// if (j + ju > tDims[1])  ju = tDims[1] - j;
    //      int ku =  3;// if (k + ku > tDims[2])  ku = tDims[2] - k;
    //      double vu = 0, vv = 0, vw = 0;
    //      double du = 0, dv = 0, dw = 0;
    //      for (int k1 = kl; k1 < ku; k1++)
    //      {
    //         switch (k1)
    //         {
    //         case -1:
    //            vw = (( -.5 * w + 1.0) * w - .5) * w;
    //            dw =  (-1.5 * w + 2.0) * w - .5;
    //            break;
    //         case 0:
    //            vw =  ( 1.5 * w - 2.5) * w * w + 1.;
    //            dw =  ( 4.5 * w - 5.0) * w;
    //            break;
    //         case 1:
    //            vw = ((-1.5 * w + 2. ) * w + .5) * w ;
    //            dw =  (-4.5 * w + 4.)  * w + .5;
    //            break;
    //         case 2:
    //            vw =  (  .5 * w -  .5) * w * w ;
    //            dw =  ( 1.5 * w -  1.) * w;
    //            break;
    //         }
    //         for (int j1 = jl; j1 < ju; j1++)
    //         {
    //            switch (j1)
    //            {
    //            case -1:
    //               vv = (( -.5 * v + 1.0) * v - .5) * v;
    //               dv =  (-1.5 * v + 2.0) * v - .5;
    //               break;
    //            case 0:
    //               vv =  ( 1.5 * v - 2.5) * v * v + 1.;
    //               dv =  ( 4.5 * v - 5.0) * v;
    //               break;
    //            case 1:
    //               vv = ((-1.5 * v + 2. ) * v + .5) * v ;
    //               dv =  (-4.5 * v + 4.)  * v + .5;
    //               break;
    //            case 2:
    //               vv =  (  .5 * v -  .5) * v * v ;
    //               dv =  ( 1.5 * v -  1.) * v;
    //               break;
    //            }
    //            for (int i1 = il; i1 < iu; i1++)
    //            {
    //               switch (i1)
    //               {
    //               case -1:
    //                  vu = (( -.5 * u + 1.0) * u - .5) * u;
    //                  du =  (-1.5 * u + 2.0) * u - .5;
    //                  break;
    //               case 0:
    //                  vu =  ( 1.5 * u - 2.5) * u * u + 1.;
    //                  du =  ( 4.5 * u - 5.0) * u;
    //                  break;
    //               case 1:
    //                  vu = ((-1.5 * u + 2. ) * u + .5) * u ;
    //                  du =  (-4.5 * u + 4.)  * u + .5;
    //                  break;
    //               case 2:
    //                  vu =  (  .5 * u -  .5) * u * u ;
    //                  du =  ( 1.5 * u -  1.) * u;
    //                  break;
    //               }
    //               double t = tArr.getData(max(0,min(k + k1, d2 - 1)) * off2 + max(0,min(j + j1, d1 - 1)) * off1 + max(0,min(i + i1, d0 - 1)));
    //               val += t * vu * vv * vw;
    //               gradient[0] += 2 * t * du * vv * vw;
    //               gradient[1] += 2 * t * vu * dv * vw;
    //               gradient[2] += 2 * t * vu * vv * dw;
    //            }
    //         }
    //      }
    //      return val;
    //   }
    private SplineValueGradientDP()
    {
    }

}
