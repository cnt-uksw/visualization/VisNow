/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.numeric;

import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;

public class EuclideanDistanceMap {

    private EuclideanDistanceMap()
    {
    }
        
    private static float[] compute1D(float[] map, float dx)
    {
        float[] out0 = new float[map.length];
        System.arraycopy(map, 0, out0, 0, map.length);
        float dx2 = dx * dx;
        int d = 0, i0 = -1;
        float current = dx2 * map.length * map.length;
        for (int i = 0; i < map.length; i++, current += dx2 * (2 * d + 1)) {
            if (map[i] <= current) {
                current = map[i];
                d = 0;
            }
            else {
                int k = -1;
                for (int j = i - 1; j > i0; j--) {
                    float f = map[j] + dx2 * (i - j) * (i - j);
                    if (f < current) {
                       k = j;
                       current = f;
                    }
                }
                if (k != -1) {
                    i0 = k;
                    d = i - i0;
                }
                d += 1;
                out0[i] = current;
            }
        }
        float[] out1 = new float[map.length];
        System.arraycopy(map, 0, out1, 0, map.length);
        current = dx2 * map.length * map.length;
        d = 0;
        i0 = map.length;
        for (int i = map.length - 1; i >= 0; i--, current += dx2 * (2 * d + 1)) {
            if (map[i] <= current) {
                current = map[i];
                d = 0;
            }
            else {
                int k = -1;
                for (int j = i + 1; j < i0; j++) {
                    float f = map[j] + dx2 * (i - j) * (i - j);
                    if (f < current) {
                       k = j;
                       current = f;
                    }
                }
                if (k != -1) {
                    i0 = k;
                    d = i0 - i;
                }
                d += 1;
                out1[i] = current;
            }
        }
        for (int i = 0; i < out0.length; i++)
            if (out1[i] < out0[i])
                out0[i] = out1[i];
        return out0;
    }
    
    
    
    public static float[] map(int[] dims, float[] delta, byte[] shape)
    {
        int n = shape.length;
        int nn = 1;
        for (int i = 0; i < dims.length; i++)
            nn *= dims[i];
        if (nn != n || delta.length != dims.length)
            return null;
        float[] sqDist = new float[n];
        for (int i = 0; i < n; i++)
           sqDist[i] = shape[i] != 0 ? 0 : Float.MAX_VALUE;
        switch (dims.length) {
        case 1:
            sqDist = compute1D(sqDist, delta[0]);
            break;
        case 2:
            float[] rowDist = new float[dims[0]];
            for (int i = 0; i < dims[1]; i++) {
                System.arraycopy(sqDist, i * dims[0], rowDist, 0, dims[0]);
                rowDist = compute1D(rowDist, delta[0]);
                System.arraycopy(rowDist, 0, sqDist, i * dims[0], dims[0]);
            }
            float[] colDist =  new float[dims[1]];
            for (int i = 0; i < dims[0]; i++) {
                for (int j = 0; j < dims[1]; j++) 
                    colDist[j] = sqDist[j * dims[0] + i];
                colDist =  compute1D(colDist, delta[1]);
                for (int j = 0; j < dims[1]; j++) 
                    sqDist[j * dims[0] + i] = colDist[j];
            }
            break;
        case 3:
            float[] dist0 = new float[dims[0]];
            for (int i = 0; i < dims[1] * dims[2]; i++) {
                System.arraycopy(sqDist, i * dims[0], dist0, 0, dims[0]);
                dist0 = compute1D(dist0, delta[0]);
                System.arraycopy(dist0, 0, sqDist, i * dims[0], dims[0]);
            }
            float[] dist1 =  new float[dims[1]];
            for (int i = 0; i < dims[2]; i++)
                for (int j = 0; j < dims[0]; j++) {
                    for (int k = 0, l = i * dims[0] * dims[1] + j; k < dims[1]; k++, l += dims[0])
                        dist1[k] = sqDist[l];
                    dist1 =  compute1D(dist1, delta[1]);
                    for (int k = 0, l = i * dims[0] * dims[1] + j; k < dims[1]; k++, l += dims[0])
                        sqDist[l] = dist1[k];
                }
            float[] dist2 =  new float[dims[2]];
            for (int i = 0; i < dims[1] * dims[0]; i++) {
                for (int k = 0, l = i; k < dims[2]; k++, l += dims[1] * dims[0])
                    dist2[k] = sqDist[l];
                dist2 =  compute1D(dist2, delta[2]);
                for (int k = 0, l = i; k < dims[2]; k++, l += dims[1] * dims[0])
                    sqDist[l] = dist2[k];
            }
            break;
        }
        for (int i = 0; i < sqDist.length; i++)
            sqDist[i] = (float)Math.sqrt(sqDist[i]);
        return sqDist;
    }

    

    private static class ComputePart extends Thread
    {

        private final int iThread;
        private final int nThreads;
        private final int[] dims;
        private final float[] map;
        private final float[] delta;
        private final int dir;

        public ComputePart(int iThread, int nThreads, int[] dims, float[] map, float[] delta, int dir)
        {
            this.iThread = iThread;
            this.nThreads = nThreads;
            this.dims = dims;
            this.map = map;
            this.delta = delta;
            this.dir = dir;
        }

        @Override
        public void run()
        {
            int n = dims[dir];
            float dx = delta[dir];
            int nRows = 1;
            int skip = 1;
            int start = 0;
            for (int i = 0; i < dims.length; i++) {
                if (i != dir)
                    nRows *= dims[i];
                if (i < dir-1)
                skip *= dims[i];
            }
            float[] row = new float[n];
            for (int step = (iThread * nRows) / nThreads; step < ((iThread + 1) * nRows) / nThreads; step++) {
                switch (dir) {
                case 2:
                    start = step;
                    break;
                case 1:
                    start = dims[1] * dims[0] * (step / dims[0]) + step % dims[0];
                    break;
                case 0:
                    start = step * dims[0];
                    break;
                }
                for (int i = 0, l = start; i < n; i++, l+= skip) 
                    row[i] = map[l];
                float[] mrow = compute1D(row, dx);
                for (int i = 0, l = start; i < n; i++, l+= skip) 
                    map[l] = row[i];
            }
        }
    }
    
    public static float[] map(int[] dims, float[] delta, byte[] shape, int nThreads)
    {
        int n = shape.length;
        int nn = 1;
        for (int i = 0; i < dims.length; i++)
            nn *= dims[i];
        if (nn != n || delta.length != dims.length)
            return null;
        float[] sqDist = new float[n];
        for (int i = 0; i < n; i++)
           sqDist[i] = shape[i] != 0 ? 0 : Float.MAX_VALUE;
        switch (dims.length) {
        case 1:
            sqDist = compute1D(sqDist, delta[0]);
            break;
        case 2:
            float[] rowDist = new float[dims[0]];
            for (int i = 0; i < dims[1]; i++) {
                System.arraycopy(sqDist, i * dims[0], rowDist, 0, dims[0]);
                rowDist = compute1D(rowDist, delta[0]);
                System.arraycopy(rowDist, 0, sqDist, i * dims[0], dims[0]);
            }
            float[] colDist =  new float[dims[1]];
            for (int i = 0; i < dims[0]; i++) {
                for (int j = 0; j < dims[1]; j++) 
                    colDist[j] = sqDist[j * dims[0] + i];
                colDist =  compute1D(colDist, delta[1]);
                for (int j = 0; j < dims[1]; j++) 
                    sqDist[j * dims[0] + i] = colDist[j];
            }
            break;
        case 3:
            float[] dist0 = new float[dims[0]];
            for (int i = 0; i < dims[1] * dims[2]; i++) {
                System.arraycopy(sqDist, i * dims[0], dist0, 0, dims[0]);
                dist0 = compute1D(dist0, delta[0]);
                System.arraycopy(dist0, 0, sqDist, i * dims[0], dims[0]);
            }
            float[] dist1 =  new float[dims[1]];
            for (int i = 0; i < dims[2]; i++)
                for (int j = 0; j < dims[0]; j++) {
                    for (int k = 0, l = i * dims[0] * dims[1] + j; k < dims[1]; k++, l += dims[0])
                        dist1[k] = sqDist[l];
                    dist1 =  compute1D(dist1, delta[1]);
                    for (int k = 0, l = i * dims[0] * dims[1] + j; k < dims[1]; k++, l += dims[0])
                        sqDist[l] = dist1[k];
                }
            float[] dist2 =  new float[dims[2]];
            for (int i = 0; i < dims[1] * dims[0]; i++) {
                for (int k = 0, l = i; k < dims[2]; k++, l += dims[1] * dims[0])
                    dist2[k] = sqDist[l];
                dist2 =  compute1D(dist2, delta[2]);
                for (int k = 0, l = i; k < dims[2]; k++, l += dims[1] * dims[0])
                    sqDist[l] = dist2[k];
            }
            break;
        }
        for (int i = 0; i < sqDist.length; i++)
            sqDist[i] = (float)Math.sqrt(sqDist[i]);
        return sqDist;
    }

    
    public static FloatLargeArray map(int[] dims, float[] delta, LargeArray shape, int nThreads)
    {
        long n = shape.length();
        long nn = 1;
        for (int i = 0; i < dims.length; i++)
            nn *= dims[i];
        if (nn != n || delta.length != dims.length)
            return null;
        float r = 0;
        for (int i = 0; i < delta.length; i++) {
            float ri = delta[i] * (dims[i] - 1);
            r += ri * ri;
        }
        r = (float)Math.sqrt(r);
        FloatLargeArray sqDist = new FloatLargeArray(n);
        for (long i = 0; i < n; i++)
           sqDist.set(i, shape.getInt(i) > 0 ? 0 : r);
        switch (dims.length) {
        case 1:
            sqDist = new FloatLargeArray(compute1D(sqDist.getData(), delta[0]));
            break;
        case 2:
            float[] rowDist = new float[dims[0]];
            for (int i = 0; i < dims[1]; i++) {
                LargeArrayUtils.arraycopy(sqDist, i * dims[0], rowDist, 0, dims[0]);
                rowDist = compute1D(rowDist, delta[0]);
                LargeArrayUtils.arraycopy(rowDist, 0, sqDist, i * dims[0], dims[0]);
            }
            float[] colDist =  new float[dims[1]];
            for (int i = 0; i < dims[0]; i++) {
                for (int j = 0; j < dims[1]; j++) 
                    colDist[j] = sqDist.get(j * dims[0] + i);
                colDist =  compute1D(colDist, delta[1]);
                for (int j = 0; j < dims[1]; j++) 
                    sqDist.set(j * dims[0] + i, colDist[j]);
            }
            break;
        case 3:
            float[] dist0 = new float[dims[0]];
            for (int i = 0; i < dims[1] * dims[2]; i++) {
                LargeArrayUtils.arraycopy(sqDist, i * dims[0], dist0, 0, dims[0]);
                dist0 = compute1D(dist0, delta[0]);
                LargeArrayUtils.arraycopy(dist0, 0, sqDist, i * dims[0], dims[0]);
            }
            float[] dist1 =  new float[dims[1]];
            long l;
            for (int i = 0; i < dims[2]; i++)
                for (int j = 0; j < dims[0]; j++) {
                    l = i * dims[0] * dims[1] + j;
                    for (int k = 0;  k < dims[1]; k++, l += dims[0])
                        dist1[k] = sqDist.get(l);
                    dist1 =  compute1D(dist1, delta[1]);
                    l = i * dims[0] * dims[1] + j;
                    for (int k = 0; k < dims[1]; k++, l += dims[0])
                        sqDist.set(l, dist1[k]);
                }
            float[] dist2 =  new float[dims[2]];
            for (int i = 0; i < dims[1] * dims[0]; i++) {
                l = i; 
                for (int k = 0; k < dims[2]; k++, l += dims[1] * dims[0])
                    dist2[k] = sqDist.get(l);
                dist2 =  compute1D(dist2, delta[2]);
                l = i; 
                for (int k = 0; k < dims[2]; k++, l += dims[1] * dims[0])
                    sqDist.set(l, dist2[k]);
            }
            break;
        }
        for (long i = 0; i < sqDist.length(); i++)
            sqDist.set(i, (float)Math.sqrt(sqDist.get(i)));
        return sqDist;
    }

}
