/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.numeric.minimization;

import static org.apache.commons.math3.util.FastMath.*;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.visnow.vn.lib.utils.events.MinimizationStepEvent;
import org.visnow.vn.lib.utils.events.MinimizationStepListener;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ConjugateGradients
{

    public static final int CG_ERROR_GRADIENT_0 = 2;
    public static final int LINEAR_SEARCH_ERROR = 9999;
    private int size;
    private float[] ptb = null;
    private float[] ptc = null;
    private float[] ptm = null;
    private float[] direction = null;
    private float[] lastgrad = null;
    private float[] gradient = null;
    private int nextra, ninterp, nbackst;
    private int totalExtra, totalInterp, totalBackst, stepNumber;
    private boolean running = true;
    private boolean paused = false;
    private boolean oneStep = false;
    private ConjugateGradientsParameters params = null;
    private static final Logger LOGGER = Logger.getLogger(ConjugateGradients.class);

    /**
     * Creates a new instance of ConjugatedGradients
     */
    public ConjugateGradients()
    {
    }

    public ConjugateGradients(int size)
    {
        this.size = size;
        ptm = new float[size];
        gradient = new float[size];
        lastgrad = new float[size];
        direction = new float[size];
        ptb = new float[size];
        ptc = new float[size];
    }

    public ConjugateGradients(float[] pt0)
    {
        this(pt0.length);
    }

    public ConjugateGradients(ConjugateGradientsParameters params)
    {
        this.params = params;
    }

    public ConjugateGradients(ConjugateGradientsParameters params, int size)
    {
        this.params = params;
        this.size = size;
        ptm = new float[size];
        gradient = new float[size];
        lastgrad = new float[size];
        direction = new float[size];
        ptb = new float[size];
        ptc = new float[size];
    }

    public ConjugateGradients(ConjugateGradientsParameters params, float[] pt0)
    {
        this(params, pt0.length);
    }

    float scalarProduct(float[] w1, float[] w2)
    {
        if (w1.length != w2.length)
            return 0.f;
        float s = 0;
        for (int i = 0; i < w1.length; i++)
            s += (w1[i] * w2[i]);
        return (s);
    }

    float maxabs(float[] w)
    {
        float s = 0;
        for (int i = 0; i < w.length; i++)
            if (s < Math.abs(w[i]))
                s = Math.abs(w[i]);
        return (s);
    }

    void inPlaceLinearCombination(double a, float[] w1, double b, float[] w2)
    {
        if (w1.length != w2.length)
            return;
        for (int i = 0; i < w1.length; i++)
            w1[i] = (float) (a * w1[i] + b * w2[i]);
    }

    float[] linearCombination(float[] w, double a, float[] w1, double b, float[] w2)
    {
        if (w1.length != w2.length)
            return null;
        if (w == null || w.length != w1.length)
            w = new float[w1.length];
        for (int i = 0; i < w1.length; i++)
            w[i] = (float) (a * w1[i] + b * w2[i]);
        return w;
    }

    double getNorm(float[] w)
    {
        return (float) Math.sqrt(scalarProduct(w, w));
    }

    float[] mv(float[] w1, float[] w2)
    {
        if (w1.length != w2.length)
            w1 = new float[w2.length];
        System.arraycopy(w2, 0, w1, 0, w2.length);
        return w1;
    }

    double minPoly(double v0, double v1, double d0, double d1, double h)
    {
        double a3, a2, a1, p, q, xmin, x1, x2, d;
        d0 *= h;
        d1 *= h;
        v1 -= v0;
        a1 = d0;
        a2 = 3 * v1 - 2 * d0 - d1;
        a3 = d1 + d0 - 2 * v1;
        if (a3 == 0)
            xmin = -a1 / (2 * a2);
        else /*
         * find root of 3 a3 x^2 + 2 a2 x + a1 =0 corresponding to the minimum of
         * a3 x^3 + a2 x^2 + a1 x
         */ {
            p = -a2 / (3 * a3);
            q = a1 / (3 * a3);
            d = Math.sqrt(p * p - q);
            if (p > 0)
                x1 = p + d;
            else
                x1 = p - d;
            x2 = q / x1;
            if (a3 < 0)
                xmin = (x1 < x2 ? x1 : x2);
            else
                xmin = (x1 < x2 ? x2 : x1);
        }
        if (xmin > 0 && xmin < 1)
            return (h * xmin);
        return h / 2;
    }

    /**
     *
     * @param pt0     - starting point
     * @param grad    - gradient in the starting point
     * @param v0      - value in the starting point
     * @param vseek   - direction of the seek
     * @param eps     - precision parameter: point is treated as minimum if
     *                grad.vseek/(|vseek|*|grad|) <lt> eps
     * @param maxile  - maximum limit of iterations
     * @param h       - initial step
     * @param gradVal - gradient function
     * <p>
     * @return 1 if minimum has been found 0 if failed to find minimum in maxile
     *         steps
     */
    int linearMinimization(float[] pt0, float[] grad, double[] v0, float[] vseek,
                           float eps, int maxLinInterp, double[] h,
                           GradVal gradVal)
    {
        double hb, new_coeff, wb, wc, normw, dir_deriv_b, dir_deriv_c, dir_deriv_0;
        int success;
        new_coeff = hb = h[0];
        wc = 0;
        normw = getNorm(vseek);
        dir_deriv_0 = scalarProduct(grad, vseek);
        nextra = ninterp = 1;
        nbackst = 0;
        if (dir_deriv_0 > 0) {
            /*
             * serious problem, should never occur!
             */
            LOGGER.warn("dir_deriv_0 = " + dir_deriv_0 + " > 0");
            return LINEAR_SEARCH_ERROR;
        }

        success = 0;
        // going forward with increasing steps until minimum is bracketed
        while (true) {
            linearCombination(ptb, 1.f, pt0, new_coeff, vseek);
            wb = gradVal.computeValGrad(ptb, grad);
            dir_deriv_b = scalarProduct(grad, vseek);
            if (wb > v0[0] - 10 * new_coeff * dir_deriv_0 && nbackst < 10) {
                nbackst += 1;
                new_coeff /= 20;
                continue;
            }
            // forward step too long - very high function value obtained
            if (wb > v0[0] || dir_deriv_b > 0)
                break;
            // minimum bracketed - end forward search         
            nextra += 1;
            v0[0] = wb;
            dir_deriv_0 = dir_deriv_b;
            new_coeff *= 2.f;
            mv(pt0, ptb);
        }

        h[0] = new_coeff;
        hb = new_coeff;

        // finding minimum by recurrent cubic interpolation
        for (ninterp = 0; ninterp < maxLinInterp; ninterp++) {
            new_coeff = minPoly(v0[0], wb, dir_deriv_0, dir_deriv_b, hb);
            linearCombination(ptc, 1.f, pt0, new_coeff, vseek);
            wc = gradVal.computeValGrad(ptc, grad);
            dir_deriv_c = scalarProduct(grad, vseek);
            double dd = dir_deriv_c / (eps + normw * getNorm(grad));
            if (wc > v0[0] || wc > wb)
                // new point value is bigger than one of the values at the interval ends
                if (wb < v0[0]) {
                    mv(pt0, ptc);
                    v0[0] = wc;
                    dir_deriv_0 = dir_deriv_c;
                    hb -= new_coeff;
                } else {
                    mv(ptb, ptc);
                    wb = wc;
                    dir_deriv_b = dir_deriv_c;
                    hb = new_coeff;
                }
            else // new point value is smaller than the values at the interval ends
            {
                if (Math.abs(dd) < eps) // minimum reached with sufficient precision               
                {
                    success = 1;
                    break;
                }
                // choosing new bracketing interval           
                if (dir_deriv_c < 0) {
                    mv(pt0, ptc);
                    v0[0] = wc;
                    dir_deriv_0 = dir_deriv_c;
                    hb -= new_coeff;
                } else {
                    mv(ptb, ptc);
                    wb = wc;
                    dir_deriv_b = dir_deriv_c;
                    hb = new_coeff;
                }
            }
        }
        if (wc < v0[0]) {
            mv(pt0, ptc);
            v0[0] = wc;
        }
        // either very irregular behavior of minimized  function or (more probably)
        // error in gradient computation
        if (success == 0)
            LOGGER.warn("max interpolations exceeded");
        return (success);
    }

    public boolean minimum_cg(float[] point, double[] val, double[] h,
                              int[] restartSchedule, int maxLinInterp,
                              GradVal gradVal)
    {
        if (params == null)
            return false;
        return minimum_cg(point, val,
                          params.getGradientEpsToExit(),
                          params.getLinearMinimizationPrecision(),
                          params.getMaxCoordMoveToExit(),
                          h, restartSchedule, maxLinInterp, gradVal);
    }

    /**
     * Minimizes the function passed by the gradVal class using conjugate gradient method with exact linear search
     * <p>
     * @param point           starting point for minimization
     * @param val             on return, min value found
     * @param eps             success only if gradient <lt>eps in last 5 steps
     * @param eps1            precision of linear minimization
     * @param eps2            success only if movement <lt>eps2 in last 5 steps
     * @param h               h[0] - initial search step in pt space
     * @param restartSchedule max number of restarts (direction resets) (not used yet)
     * @param maxLinInterp    max number of spline interpolations in linear search
     * @param gradVal         an object implementing GradValDP interface providing values
     *                        and gradients of minimized function
     * <p>
     * @return true if exit criterion fulfilled, false if max iteration count reached or
     *         serious misbehavior occured
     */
    public boolean minimum_cg(float[] point, double[] val,
                              float eps, float eps1, float eps2, double[] h,
                              int[] restartSchedule, int maxLinInterp, GradVal gradVal)
    {
        double[] w0 = new double[1];
        double[] lastGradNorms = new double[5];
        double[] lastMaxSteps = new double[5];
        double n0, lgradlgrad, gradgrad, gradlgrad;
        int i;
        int exceeds = 0;
        double dis, maxDiff;
        boolean forceRefresh = false;
        totalExtra = totalInterp = totalBackst = 0;
        nextra = ninterp = nbackst = 0;
        for (int j = 0; j < lastMaxSteps.length; j++)
            lastMaxSteps[j] = lastGradNorms[j] = Double.MAX_VALUE;
        for (int restarts = 0; restarts < restartSchedule.length && running; restarts++) {
            for (int j = 0; j < lastMaxSteps.length; j++)
                lastMaxSteps[j] = lastGradNorms[j] = Double.MAX_VALUE;
            if (restartSchedule[restarts] <= 0)
                break;
            w0[0] = gradVal.computeValGrad(point, gradient);
            fireMinimizationStepChanged(0, w0[0], getNorm(gradient), 0, 0, 0, 0, 0, forceRefresh);
            lgradlgrad = gradgrad = scalarProduct(gradient, gradient);
            n0 = maxabs(gradient);
            if (n0 == 0.)
                return (false);
            h[0] /= n0;
            for (i = 0; i < size; i++)
                direction[i] = -gradient[i];
            mv(ptm, point);
            for (stepNumber = 1; stepNumber <= restartSchedule[restarts] && running; stepNumber++) {
                //System.out.println("step number = "+stepNumber+" rs = "+restartSchedule[restarts]);

                while (!oneStep && paused)
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                    }
                if (oneStep)
                    forceRefresh = true;
                oneStep = false;

                int s = linearMinimization(ptm, gradient, w0, direction, eps1, maxLinInterp, h, gradVal);
                if (s != 1) {
                    exceeds++;

                    for (int j = 0; j < lastMaxSteps.length; j++)
                        lastMaxSteps[j] = lastGradNorms[j] = Double.MAX_VALUE;
                    w0[0] = gradVal.computeValGrad(point, gradient);
                    fireMinimizationStepChanged(0, w0[0], getNorm(gradient), 0, 0, 0, 0, 0, forceRefresh);
                    lgradlgrad = gradgrad = scalarProduct(gradient, gradient);
                    n0 = maxabs(gradient);
                    if (n0 == 0.)
                        return (false);
                    h[0] /= n0;
                    for (i = 0; i < size; i++)
                        direction[i] = -gradient[i];
                    mv(ptm, point);

                    if (exceeds > 3) {
                        exceeds = 0;
                        break;
                    }
                    continue;
                }

                exceeds = 0;

                dis = maxDiff = 0;
                for (i = 0; i < size; i++) {
                    dis += (ptm[i] - point[i]) * (ptm[i] - point[i]);
                    if (abs(ptm[i] - point[i]) > maxDiff)
                        maxDiff = abs(ptm[i] - point[i]);
                }
                mv(point, ptm);
                gradgrad = scalarProduct(gradient, gradient);
                gradlgrad = scalarProduct(gradient, lastgrad);
                mv(lastgrad, gradient);
                double avgStep = Math.sqrt(dis / size);
                for (int j = 0; j < lastMaxSteps.length - 1; j++) {
                    lastMaxSteps[j] = lastMaxSteps[j + 1];
                    lastGradNorms[j] = lastGradNorms[j + 1];
                }
                lastMaxSteps[lastMaxSteps.length - 1] = maxDiff;
                lastGradNorms[lastMaxSteps.length - 1] = sqrt(gradgrad);
                boolean readyToExit = true;
                for (int j = 0; j < lastMaxSteps.length; j++)
                    if (lastGradNorms[j] > eps || lastMaxSteps[j] > eps2)
                        readyToExit = false;
                double beta = (gradgrad - gradlgrad) / lgradlgrad;
                if (beta < 0)
                    beta = 0;
                inPlaceLinearCombination(beta, direction, -1., gradient);
                lgradlgrad = gradgrad;
                fireMinimizationStepChanged(stepNumber, w0[0], getNorm(gradient),
                                            avgStep, maxDiff, nextra, nbackst, ninterp + 1, forceRefresh);
                totalExtra += nextra;
                totalBackst += nbackst;
                totalInterp += ninterp + 1;
                if (readyToExit) {
                    w0[0] = gradVal.computeValGrad(point, gradient);
                    val[0] = w0[0];
                    return true;
                }

                forceRefresh = false;
            }
        }
        w0[0] = gradVal.computeValGrad(point, gradient);
        val[0] = w0[0];
        return false;
    }

    public int getStepNumber()
    {
        return stepNumber;
    }

    public void setRunning(boolean running)
    {
        this.running = running;
    }

    public void setPaused(boolean paused)
    {
        this.paused = paused;
    }

    public void setOneStep(boolean step)
    {
        this.oneStep = step;
    }

    public int getTotalBackst()
    {
        return totalBackst;
    }

    public int getTotalExtra()
    {
        return totalExtra;
    }

    public int getTotalInterp()
    {
        return totalInterp;
    }

    /**
     * Utility field holding list of ChangeListeners.
     */
    private transient ArrayList<MinimizationStepListener> MinimizationStepListenerList
        = new ArrayList<MinimizationStepListener>();

    /**
     * Registers MinimizationStepListener to receive events.
     *
     * @param listener The listener to register.
     */
    public synchronized void addMinimizationStepListener(MinimizationStepListener listener)
    {
        MinimizationStepListenerList.add(listener);
    }

    /**
     * Removes MinimizationStepListener from the list of listeners.
     *
     * @param listener The listener to remove.
     */
    public synchronized void removeMinimizationStepListener(MinimizationStepListener listener)
    {
        MinimizationStepListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event - used for monitoring
     * of the minimization proces
     *
     * @param step            current minimization step
     * @param value           function value after step
     * @param gradientNorm    norm of the gradient after step
     * @param avgStep         average displacement of last 5 steps
     * @param maxStep         maximum displacement of last 5 steps
     * @param nExtrapolations number of forward steps during linear minimization
     * @param nBacksteps      number of overshots during linear minimization
     * @param nInterpolations number of bracketing interpolation steps during linear minimization
     * @param forceRefresh
     */
    public void fireMinimizationStepChanged(int step,
                                            double value, double gradientNorm,
                                            double avgStep, double maxStep,
                                            int nExtrapolations, int nBacksteps, int nInterpolations, boolean forceRefresh)
    {
        MinimizationStepEvent e = new MinimizationStepEvent(this, step, value, gradientNorm,
                                                            avgStep, maxStep,
                                                            nExtrapolations, nBacksteps, nInterpolations, forceRefresh);
        for (MinimizationStepListener listener : MinimizationStepListenerList)
            listener.minimizationStepChanged(e);
    }
}
