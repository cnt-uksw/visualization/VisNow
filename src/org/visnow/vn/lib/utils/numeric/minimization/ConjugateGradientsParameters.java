/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.numeric.minimization;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ConjugateGradientsParameters
{

    //   double gradientEpsToExit;
    //   double maxCoordMoveToExit;
    //   double linearMinimizationPrecision
    //   double[] initialStep;
    //   int[] restartSchedule;
    //   int maxLinearInterp;
    private float gradientEpsToExit = .001f;
    private float maxCoordMoveToExit = .001f;
    private float linearMinimizationPrecision = .001f;
    private int[] restartSchedule = new int[]{20, 50, 200};

    private GradValDP gradValDP;
    private GradVal gradVal;

    /**
     * Mimimization will finish if for last 5 steps all gradient components are
     * <lt> gradientEpsToExit
     *
     * @return the value of gradientEpsToExit
     */
    public float getGradientEpsToExit()
    {
        return gradientEpsToExit;
    }

    /**
     * Set the value of gradientEpsToExit
     *
     * @param gradientEpsToExit new value of gradientEpsToExit
     */
    public void setGradientEpsToExit(float gradientEpsToExit)
    {
        this.gradientEpsToExit = gradientEpsToExit;
    }

    /**
     * Mimimization will finish if for last 5 steps max coordinate change <lt>
     * maxCoordMoveToExit
     *
     * @return the value of maxCoordMoveToExit
     */
    public float getMaxCoordMoveToExit()
    {
        return maxCoordMoveToExit;
    }

    /**
     * Set the value of maxCoordMoveToExit
     *
     * @param maxCoordMoveTo exit new value of maxCoordMoveToExit
     */
    public void setMaxCoordMoveToExit(float maxCoordMoveToExit)
    {
        this.maxCoordMoveToExit = maxCoordMoveToExit;
    }

    /**
     * Get the value of linearMinimizationPrecision
     *
     * @return the value of linearMinimizationPrecision
     */
    public float getLinearMinimizationPrecision()
    {
        return linearMinimizationPrecision;
    }

    /**
     * Set the value of linearMinimizationPrecision
     *
     * @param linearMinimizationPrecision new value of
     *                                    linearMinimizationPrecision
     */
    public void setLinearMinimizationPrecision(float linearMinimizationPrecision)
    {
        this.linearMinimizationPrecision = linearMinimizationPrecision;
    }

    /**
     * Get the value of restartSchedule
     *
     * @return the value of restartSchedule
     */
    public int[] getRestartSchedule()
    {
        return restartSchedule;
    }

    /**
     * Set the value of restartSchedule
     *
     * @param restartSchedule new value of restartSchedule
     */
    public void setRestartSchedule(int[] restartSchedule)
    {
        this.restartSchedule = restartSchedule;
    }

    /**
     * Get the value of restartSchedule at specified index
     *
     * @param index
     *              <p>
     * @return the value of restartSchedule at specified index
     */
    public int getRestartSchedule(int index)
    {
        return this.restartSchedule[index];
    }

    /**
     * Set the value of restartSchedule at specified index.
     *
     * @param index
     * @param newRestartSchedule new value of restartSchedule at specified index
     */
    public void setRestartSchedule(int index, int newRestartSchedule)
    {
        this.restartSchedule[index] = newRestartSchedule;
    }

}
