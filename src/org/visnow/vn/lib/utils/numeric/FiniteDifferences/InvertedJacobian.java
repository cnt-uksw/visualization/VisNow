/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.numeric.FiniteDifferences;

import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.utils.MatrixMath;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class InvertedJacobian
{

    private InvertedJacobian()
    {
    }

    private static class ComputeLargeInvertedJacobian3D implements Runnable
    {

        private final int nThreads;
        private final int iThread;
        private final long[] dims;
        private final FloatLargeArray coords;
        private final FloatLargeArray invJacobian; //ordered: dv0/di0, dv1/di0, dv2/di0, dv0/di1, dv1/di1, dv2/di1, dv0/di2, dv1/di2, dv2/di2 

        public ComputeLargeInvertedJacobian3D(int nThreads, int iThread, long[] dims, FloatLargeArray coords, FloatLargeArray invJacobian)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.dims = dims;
            this.coords = coords;
            this.invJacobian = invJacobian;
        }

        @Override
        public void run()
        {
            long off1 = 3 * dims[0];
            long off2 = 3 * dims[0] * dims[1];
            float[][] d = new float[3][3];
            float[][] dinv = new float[3][3];
            for (long i = (iThread * dims[2]) / nThreads; i < ((iThread + 1)  * dims[2]) / nThreads; i++) {
                for (long j = 0, n = 3 * i * off2, m = i * off2; j < dims[1]; j++)
                    for (long k = 0; k < dims[0]; k++) {
                        for (int l = 0; l < 3; l++, m++) {
                            if (k == 0)
                                d[0][l] = coords.getFloat(m + 3) - coords.getFloat(m);
                            else if (k == dims[0] - 1)
                                d[0][l] = coords.getFloat(m) - coords.getFloat(m - 3);
                            else
                                d[0][l] = .5f * (coords.getFloat(m + 3) - coords.getFloat(m - 3));
                            if (j == 0)
                                d[1][l] = coords.getFloat(m + off1) - coords.getFloat(m);
                            else if (j == dims[1] - 1)
                                d[1][l] = coords.getFloat(m) - coords.getFloat(m - off1);
                            else
                                d[1][l] = .5f * (coords.getFloat(m + off1) - coords.getFloat(m - off1));
                            if (i == 0)
                                d[2][l] = coords.getFloat(m + off2) - coords.getFloat(m);
                            else if (i == dims[2] - 1)
                                d[2][l] = coords.getFloat(m) - coords.getFloat(m - off2);
                            else
                                d[2][l] = .5f * (coords.getFloat(m + off2) - coords.getFloat(m - off2));
                        }
                        MatrixMath.invert(d, dinv);
                        for (int l = 0; l < 3; l++)
                            for (int p = 0; p < 3; p++, n++)
                                invJacobian.setFloat(n, dinv[l][p]);
                    }
            }
        }
    }

    private static class ComputeLargeInvertedJacobian2D implements Runnable
    {

        private final int nThreads;
        private final int iThread;
        private final long[] dims;
        private final FloatLargeArray coords;
        private final FloatLargeArray invJacobian; //ordered: dv0/di0, dv1/di0, dv0/di1, dv1/di1

        public ComputeLargeInvertedJacobian2D(int nThreads, int iThread, long[] dims, FloatLargeArray inFData, FloatLargeArray outFData)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.dims = dims;
            this.coords = inFData;
            this.invJacobian = outFData;
        }

        @Override
        public void run()
        {
            float[][] d = new float[2][2];
            float[][] dinv = new float[2][2];
            long off1 = 3 * dims[0];
            for (long j = (iThread * dims[1]) / nThreads; j <  ((iThread + 1)  * dims[1]) / nThreads; j++)
                for (long k = 0, n = 4 * j * dims[0], m = 3 * j * dims[0]; k < dims[0]; k++) {
                    for (int l = 0; l < 2; l++, m++) {
                        if (k == 0)
                            d[0][l] = coords.getFloat(m + 3) - coords.getFloat(m);
                        else if (k == dims[0] - 1)
                            d[0][l] = coords.getFloat(m) - coords.getFloat(m - 3);
                        else
                            d[0][l] = .5f * (coords.getFloat(m + 3) - coords.getFloat(m - 3));
                        if (j == 0)
                            d[1][l] = coords.getFloat(m + off1) - coords.getFloat(m);
                        else if (j == dims[1] - 1)
                            d[1][l] = coords.getFloat(m) - coords.getFloat(m - off1);
                        else
                            d[1][l] = .5f * (coords.getFloat(m + off1) - coords.getFloat(m - off1));
                    }
                    m += 1;
                    MatrixMath.invert(d, dinv);
                    for (int l = 0; l < 2; l++)
                        for (int p = 0; p < 2; p++, n++)
                            invJacobian.setFloat(n, dinv[l][p]);
                }
        }
    }

    public static FloatLargeArray computeLargeInvertedJacobian(int nThreads, RegularField inField)
    {
        if (inField == null)
            return null;
        long[] dims = inField.getLDims();
        int dim = dims.length;
        FloatLargeArray coords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords();
        if (coords == null) {
            float[][] iA = inField.getInvAffine();
            FloatLargeArray invJacobian = new FloatLargeArray(dim * dim);
            for (int i = 0, k = 0; i < dim; i++)
                for (int j = 0; j < dim; j++, k++)
                    invJacobian.setFloat(k, iA[i][j]);
            return invJacobian;
        }
        FloatLargeArray invJacobian = new FloatLargeArray(dim * dim * inField.getNNodes());
        Thread[] workThreads = new Thread[nThreads];
        switch (dim) {
        case 3:
            for (int iThread = 0; iThread < nThreads; iThread++) {
                workThreads[iThread] = new Thread(new ComputeLargeInvertedJacobian3D(nThreads, iThread, dims, coords, invJacobian));
                workThreads[iThread].start();
            }
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                }catch (InterruptedException e) {
                }
            break;
        case 2:
            for (int iThread = 0; iThread < nThreads; iThread++) {
                workThreads[iThread] = new Thread(new ComputeLargeInvertedJacobian2D(nThreads, iThread, dims, coords, invJacobian));
                workThreads[iThread].start();
            }
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                }catch (InterruptedException e) {
                }
            break;
        case 1:
            invJacobian.setFloat(0, 1 / (coords.getFloat(3) - coords.getFloat(0)));
            for (int i = 1; i < dims[0] - 1; i++)
                invJacobian.setFloat(i, .5f / (coords.getFloat(3 * (i + 1)) - coords.getFloat(3 * (i - 1))));
            invJacobian.setFloat(dims[0] - 1, 1 / (coords.getFloat(3 * (dims[0] - 1)) - coords.getFloat(3 * (dims[0] - 2))));
            break;
        }
        return invJacobian;
    }
}
