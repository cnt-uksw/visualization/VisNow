/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.convolution;

import static org.apache.commons.math3.util.FastMath.*;
import org.jtransforms.fft.FloatFFT_1D;
import org.jtransforms.fft.FloatFFT_2D;
import org.jtransforms.fft.FloatFFT_3D;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jvia.spatialops.Padding.PaddingType;
import org.visnow.vn.lib.utils.fft.FftCore;
import org.visnow.jvia.spatialops.Padding;

/**
 * @author Piotr Wendykier (piotrw@icm.edu.pl)
 */
public class ConvolutionJavaCore extends ConvolutionCore
{

    private FloatFFT_1D fft1;
    private FloatFFT_2D fft2;
    private FloatFFT_3D fft3;

    /**
     * Creates a new
     * <code>ConvolutionJavaCore</code> object.
     */
    public ConvolutionJavaCore()
    {
    }

    @Override
    protected void convolve(FloatLargeArray data, long dimsData, FloatLargeArray kernel, long dimsKernel, PaddingType padding)
    {
        FloatLargeArray paddedData;
        FloatLargeArray paddedKernel;
        long[][] dataMargins = new long[1][2];
        long[][] kernelMargins = new long[1][2];
        long[] dimsPaddedData = {dimsData + dimsKernel};
        dataMargins[0][0] = (long) floor(dimsKernel / 2.0);
        dataMargins[0][1] = (long) floor((dimsKernel - 1) / 2.0);
        if ((dimsData + dataMargins[0][0] + dataMargins[0][1]) % 2 == 0) {
            dataMargins[0][1] += 1;
        }

        kernelMargins[0][0] = (long) floor((dimsPaddedData[0] - dimsKernel) / 2.0);
        kernelMargins[0][1] = dimsPaddedData[0] - dimsKernel - kernelMargins[0][0];

        paddedData = (FloatLargeArray)Padding.padLargeArray(data, new long[]{dimsData}, 1, dataMargins, padding, false);
        paddedKernel = (FloatLargeArray)Padding.padLargeArray(kernel, new long[]{dimsKernel}, 1, kernelMargins, PaddingType.ZERO, false);

        long length = dimsPaddedData[0];
        FloatLargeArray fftData = new FloatLargeArray(2 * length, false);
        FloatLargeArray fftKernel = new FloatLargeArray(2 * length, false);
        LargeArrayUtils.arraycopy(paddedData, 0, fftData, 0, length);
        LargeArrayUtils.arraycopy(paddedKernel, 0, fftKernel, 0, length);
        fft1 = null;
        fft1 = new FloatFFT_1D(length);
        fft1.realForwardFull(fftData);
        fft1.realForwardFull(fftKernel);
        for (long i = 0; i < length; i++) {
            float reData = fftData.getFloat(2 * i);
            float imData = fftData.getFloat(2 * i + 1);
            float reKernel = fftKernel.getFloat(2 * i);
            float imKernel = fftKernel.getFloat(2 * i + 1);
            fftData.setFloat(2 * i, reData * reKernel - imData * imKernel);
            fftData.setFloat(2 * i + 1, imData * reKernel + reData * imKernel);
        }
        fft1.complexInverse(fftData, true);
        for (long i = 0; i < length; i++) {
            paddedData.setFloat(i, fftData.getFloat(2 * i));
        }
        paddedData = FftCore.circShift_1D(paddedData);
        for (long i = 0; i < dimsData; i++) {
            data.setFloat(i, paddedData.getFloat(i + dataMargins[0][0]));
        }
    }

    @Override
    protected void convolve2(FloatLargeArray data, long[] dimsData, FloatLargeArray kernel, long[] dimsKernel, PaddingType padding)
    {
        long rowsData, colsData;
        long rowsPaddedData, colsPaddedData;
        FloatLargeArray paddedData;
        FloatLargeArray paddedKernel;
        long[][] dataMargins = new long[2][2];
        long[][] kernelMargins = new long[2][2];
        rowsData = dimsData[1];
        colsData = dimsData[0];

        for (int i = 0; i < 2; i++) {
            dataMargins[i][0] = (long) floor(dimsKernel[i] / 2.0);
            dataMargins[i][1] = (long) floor((dimsKernel[i] - 1) / 2.0);
            if ((dimsData[i] + dataMargins[i][0] + dataMargins[i][1]) % 2 == 0) {
                dataMargins[i][0] += 1;
            }
        }

        long[] dimsPaddedData = {dimsData[0] + dataMargins[0][0] + dataMargins[0][1], dimsData[1] + dataMargins[1][0] + dataMargins[1][1]};

        for (int i = 0; i < 2; i++) {
            kernelMargins[i][0] = (long) floor((dimsPaddedData[i] - dimsKernel[i]) / 2.0);
            kernelMargins[i][1] = dimsPaddedData[i] - dimsKernel[i] - kernelMargins[i][0];
        }

        paddedData = (FloatLargeArray)Padding.padLargeArray(data, dimsData, 1, dataMargins, padding, false);
        paddedKernel = (FloatLargeArray)Padding.padLargeArray(kernel, dimsKernel, 1, kernelMargins, PaddingType.ZERO, false);

        rowsPaddedData = dimsPaddedData[1];
        colsPaddedData = dimsPaddedData[0];

        long length = rowsPaddedData * colsPaddedData;
        FloatLargeArray fftData = new FloatLargeArray(2 * length, false);
        FloatLargeArray fftKernel = new FloatLargeArray(2 * length, false);
        LargeArrayUtils.arraycopy(paddedData, 0, fftData, 0, length);
        LargeArrayUtils.arraycopy(paddedKernel, 0, fftKernel, 0, length);
        fft2 = null;
        fft2 = new FloatFFT_2D(rowsPaddedData, colsPaddedData);
        fft2.realForwardFull(fftData);
        fft2.realForwardFull(fftKernel);
        for (long i = 0; i < length; i++) {
            float reData = fftData.getFloat(2 * i);
            float imData = fftData.getFloat(2 * i + 1);
            float reKernel = fftKernel.getFloat(2 * i);
            float imKernel = fftKernel.getFloat(2 * i + 1);
            fftData.setFloat(2 * i, reData * reKernel - imData * imKernel);
            fftData.setFloat(2 * i + 1, imData * reKernel + reData * imKernel);
        }
        fft2.complexInverse(fftData, true);
        for (long i = 0; i < length; i++) {
            paddedData.setFloat(i, fftData.getFloat(2 * i));
        }
        paddedData = FftCore.circShift_2D(paddedData, colsPaddedData, rowsPaddedData);
        long l = 0;
        for (long i = dataMargins[1][0]; i < rowsData + dataMargins[1][0]; i++) {
            for (long j = dataMargins[0][0]; j < colsData + dataMargins[0][0]; j++) {
                data.setFloat(l++, paddedData.getFloat(i * colsPaddedData + j));
            }
        }
    }

    @Override
    protected void convolve3(FloatLargeArray data, long[] dimsData, FloatLargeArray kernel, long[] dimsKernel, PaddingType padding)
    {
        long slicesData, rowsData, colsData;
        long slicesPaddedData, rowsPaddedData, colsPaddedData;
        FloatLargeArray paddedData;
        FloatLargeArray paddedKernel;
        long[][] dataMargins = new long[3][2];
        long[][] kernelMargins = new long[3][2];
        slicesData = dimsData[2];
        rowsData = dimsData[1];
        colsData = dimsData[0];

        for (int i = 0; i < 3; i++) {
            dataMargins[i][0] = (long) floor(dimsKernel[i] / 2.0);
            dataMargins[i][1] = (long) floor((dimsKernel[i] - 1) / 2.0);
            if ((dimsData[i] + dataMargins[i][0] + dataMargins[i][1]) % 2 == 0) {
                dataMargins[i][0] += 1;
            }
        }

        long[] dimsPaddedData = {dimsData[0] + dataMargins[0][0] + dataMargins[0][1], dimsData[1] + dataMargins[1][0] + dataMargins[1][1], dimsData[2] + dataMargins[2][0] + dataMargins[2][1]};

        for (int i = 0; i < 3; i++) {
            kernelMargins[i][0] = (long) floor((dimsPaddedData[i] - dimsKernel[i]) / 2.0);
            kernelMargins[i][1] = dimsPaddedData[i] - dimsKernel[i] - kernelMargins[i][0];
        }

        paddedData = (FloatLargeArray)Padding.padLargeArray(data, dimsData, 1, dataMargins, padding, false);
        paddedKernel = (FloatLargeArray)Padding.padLargeArray(kernel, dimsKernel, 1, kernelMargins, PaddingType.ZERO, false);

        slicesPaddedData = dimsPaddedData[2];
        rowsPaddedData = dimsPaddedData[1];
        colsPaddedData = dimsPaddedData[0];

        long length = slicesPaddedData * rowsPaddedData * colsPaddedData;
        FloatLargeArray fftData = new FloatLargeArray(2 * length, false);
        FloatLargeArray fftKernel = new FloatLargeArray(2 * length, false);
        LargeArrayUtils.arraycopy(paddedData, 0, fftData, 0, length);
        LargeArrayUtils.arraycopy(paddedKernel, 0, fftKernel, 0, length);
        fft3 = null;
        fft3 = new FloatFFT_3D(slicesPaddedData, rowsPaddedData, colsPaddedData);
        fft3.realForwardFull(fftData);
        fft3.realForwardFull(fftKernel);
        for (long i = 0; i < length; i++) {
            float reData = fftData.getFloat(2 * i);
            float imData = fftData.getFloat(2 * i + 1);
            float reKernel = fftKernel.getFloat(2 * i);
            float imKernel = fftKernel.getFloat(2 * i + 1);
            fftData.setFloat(2 * i, reData * reKernel - imData * imKernel);
            fftData.setFloat(2 * i + 1, imData * reKernel + reData * imKernel);
        }
        fft3.complexInverse(fftData, true);
        long l = 0;
        for (long i = 0; i < length; i++) {
            paddedData.setFloat(i, fftData.getFloat(2 * i));
        }
        paddedData = FftCore.circShift_3D(paddedData, colsPaddedData, rowsPaddedData, slicesPaddedData);

        for (long i = dataMargins[2][0]; i < slicesData + dataMargins[2][0]; i++) {
            for (long j = dataMargins[1][0]; j < rowsData + dataMargins[1][0]; j++) {
                for (long k = dataMargins[0][0]; k < colsData + dataMargins[0][0]; k++) {
                    data.setFloat(l++, paddedData.getFloat(i * rowsPaddedData * colsPaddedData + j * colsPaddedData + k));
                }
            }
        }
    }
}
