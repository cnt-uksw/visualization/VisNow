/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.types;

import org.visnow.jscic.Field;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import java.util.ArrayList;
import org.visnow.jscic.PointField;
import org.visnow.jscic.cells.CellType;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University, Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */
public class VNField implements VNDataSchemaInterface
{

    protected Field field = null;

    public VNField(Field field)
    {
        if (field == null) throw new NullPointerException("Field cannot be null.");
        if (field instanceof RegularField && field.getNComponents() == 0) throw new IllegalArgumentException("Fields without components are not supported!");
        if (field instanceof IrregularField && field.getNComponents() == 0) {
            boolean componentExists = false;
            for (CellSet cellSet : ((IrregularField) field).getCellSets())
                if (cellSet.getNComponents() != 0) componentExists = true;
            if (!componentExists) throw new IllegalArgumentException("Fields without components are not supported!");
        }
        this.field = field;
        field.checkTrueNSpace();
    }

    public Field getField()
    {
        return field;
    }

    @Override
    public int getTrueNSpace()
    {
        return field.getTrueNSpace();
    }

    @Override
    public String toString()
    {
        return toString(false);
    }

    public String toString(boolean debug)
    {
        if (field == null) {
            return "empty field";
        }
        return field.description(debug);
    }

    @Override
    public Class getVnDataType()
    {
        return VNField.class;
    }

    @Override
    public boolean isEmpty()
    {
        return (field == null);
    }

    @Override
    public boolean isField()
    {
        return true;
    }

    @Override
    public boolean isRegular()
    {
        return (field != null && field instanceof RegularField);
    }

    @Override
    public int getNDims()
    {
        return isRegular() ? ((RegularField) field).getDims().length : -1;
    }

    @Override
    public int[] getDims()
    {
        return isRegular() ? ((RegularField) field).getDims() : null;
    }

    @Override
    public boolean isAffine()
    {
        return isRegular() && field.getCurrentCoords() == null;
    }

    @Override
    public boolean isCoords()
    {
        if (this.isRegular()) {
            return (((RegularField) field).getCurrentCoords() != null);
        } else {
            return false;
        }
    }

    @Override
    public int getNData()
    {
        if (field != null) {
            return field.getNComponents();
        } else {
            return 0;
        }
    }

    @Override
    public int[] getDataVeclens()
    {
        if (field != null) {
            int[] out = new int[field.getNComponents()];
            for (int i = 0; i < out.length; i++) {
                out[i] = field.getComponent(i).getVectorLength();
            }
            return out;
        } else {
            return null;
        }
    }

    @Override
    public int[] getDataTypes()
    {
        if (field != null) {
            int[] out = new int[field.getNComponents()];
            for (int i = 0; i < out.length; i++) {
                out[i] = field.getComponent(i).getType().getValue();
            }
            return out;
        } else {
            return null;
        }
    }

    @Override
    public String[] getDataNames()
    {
        if (field != null) {
            String[] out = new String[field.getNComponents()];
            for (int i = 0; i < out.length; i++) {
                out[i] = field.getComponent(i).getName();
            }
            return out;
        } else {
            return null;
        }
    }

    @Override
    public boolean hasScalarComponent()
    {
        return hasVectorComponent(1);
    }

    @Override
    public boolean hasVectorComponent(int veclen)
    {
        int[] v = this.getDataVeclens();
        if (v == null) {
            return false;
        }
        for (int i = 0; i < v.length; i++) {
            if (v[i] == veclen) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isIrregular()
    {
        return field != null && field instanceof IrregularField;
    }

    @Override
    public boolean isTime()
    {

        return field != null && field.isTimeDependant();
    }

    @Override
    public boolean isLarge()
    {

        return field != null && field.isLarge();
    }

    @Override
    public boolean isCellSets()
    {
        if (field == null || !this.isIrregular()) {
            return false;
        }
        return (((IrregularField) field).getNCellSets() > 0);
    }

    @Override
    public int getNCellSets()
    {
        if (field == null || !this.isIrregular()) {
            return -1;
        }
        return ((IrregularField) field).getNCellSets();
    }

    @Override
    public int[] getNCellData()
    {
        if (field == null || !this.isIrregular()) {
            return null;
        }

        ArrayList<CellSet> css = ((IrregularField) field).getCellSets();
        if (css == null || css.size() == 0)
            return null;

        int count = 0;
        for (int i = 0; i < css.size(); i++) {
            if (css.get(i).getNComponents() != 0) {
                count++;
            }
        }
        int[] out = new int[count];
        for (int i = 0, k = 0; i < css.size(); i++) {
            if (css.get(i).getNComponents() != 0) {
                out[k++] = css.get(i).getNComponents();
            }
        }
        return out;
    }

    @Override
    public int[][] getCellDataVeclens()
    {
        if (field == null || !this.isIrregular()) {
            return null;
        }

        ArrayList<CellSet> css = ((IrregularField) field).getCellSets();
        if (css == null || css.size() == 0)
            return null;

        int count = 0;
        for (int i = 0; i < css.size(); i++) {
            if (css.get(i).getNComponents() != 0) {
                count++;
            }
        }
        int[][] out = new int[count][];
        for (int i = 0, k = 0; i < css.size(); i++) {
            if (css.get(i).getNComponents() != 0) {
                out[k] = new int[css.get(i).getNComponents()];
                for (int j = 0; j < css.get(i).getNComponents(); j++) {
                    out[k][j] = css.get(i).getComponent(j).getVectorLength();
                }
                k++;
            }
        }
        return out;
    }

    @Override
    public int[][] getCellDataTypes()
    {
        if (field == null || !this.isIrregular()) {
            return null;
        }

        ArrayList<CellSet> css = ((IrregularField) field).getCellSets();
        if (css == null || css.size() == 0)
            return null;

        int count = 0;
        for (int i = 0; i < css.size(); i++) {
            if (css.get(i).getNComponents() != 0) {
                count++;
            }
        }
        int[][] out = new int[count][];
        for (int i = 0, k = 0; i < css.size(); i++) {
            if (css.get(i).getNComponents() != 0) {
                out[k] = new int[css.get(i).getNComponents()];
                for (int j = 0; j < css.get(i).getNComponents(); j++) {
                    out[k][j] = css.get(i).getComponent(j).getType().getValue();
                }
                k++;
            }
        }
        return out;
    }

    @Override
    public String[][] getCellDataNames()
    {
        if (field == null || !this.isIrregular()) {
            return null;
        }

        ArrayList<CellSet> css = ((IrregularField) field).getCellSets();
        if (css == null || css.size() == 0)
            return null;

        int count = 0;
        for (int i = 0; i < css.size(); i++) {
            if (css.get(i).getNComponents() != 0) {
                count++;
            }
        }
        String[][] out = new String[count][];
        for (int i = 0, k = 0; i < css.size(); i++) {
            if (css.get(i).getNComponents() != 0) {
                out[k] = new String[css.get(i).getNComponents()];
                for (int j = 0; j < css.get(i).getNComponents(); j++) {
                    out[k][j] = css.get(i).getComponent(j).getName();
                }
                k++;
            }
        }
        return out;
    }

    @Override
    public boolean hasCellScalarComponent()
    {
        return hasCellVectorComponent(1);
    }

    @Override
    public boolean hasCellVectorComponent(int veclen)
    {
        int[][] v = this.getCellDataVeclens();
        if (v == null) {
            return false;
        }
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                if (v[i][j] == veclen) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String[] getCellSetNames()
    {
        if (field == null || !isIrregular() || ((IrregularField) field).getNCellSets() == 0)
            return null;

        String[] out = new String[((IrregularField) field).getNCellSets()];
        for (int i = 0; i < out.length; i++) {
            out[i] = ((IrregularField) field).getCellSet(i).getName();
        }
        return out;
    }

    private boolean hasCellsType(CellType type)
    {
        if (field == null || !this.isIrregular())
            return false;

        return ((IrregularField) field).hasCellsType(type);
    }

    @Override
    public boolean hasCellsPoint()
    {
        if (field == null || !this.isIrregular())
            return false;

        return ((IrregularField) field).hasCellsPoint();
    }

    @Override
    public boolean hasCellsSegment()
    {
        if (field == null || !this.isIrregular())
            return false;

        return ((IrregularField) field).hasCellsSegment();
    }

    @Override
    public boolean hasCellsTriangle()
    {
        if (field == null || !this.isIrregular())
            return false;

        return ((IrregularField) field).hasCellsTriangle();
    }

    @Override
    public boolean hasCellsQuad()
    {
        if (field == null || !this.isIrregular())
            return false;

        return ((IrregularField) field).hasCellsQuad();
    }

    @Override
    public boolean hasCellsTetra()
    {
        if (field == null || !this.isIrregular())
            return false;

        return ((IrregularField) field).hasCellsTetra();
    }

    @Override
    public boolean hasCellsPyramid()
    {
        if (field == null || !this.isIrregular())
            return false;

        return ((IrregularField) field).hasCellsPyramid();
    }

    @Override
    public boolean hasCellsPrism()
    {
        if (field == null || !this.isIrregular())
            return false;

        return ((IrregularField) field).hasCellsPrism();
    }

    @Override
    public boolean hasCellsHexahedron()
    {
        if (field == null || !this.isIrregular())
            return false;

        return ((IrregularField) field).hasCellsHexahedron();
    }

    @Override
    public boolean hasCells2D()
    {
        if (field == null || !this.isIrregular())
            return false;

        return ((IrregularField) field).hasCells2D();
    }

    @Override
    public boolean hasCells3D()
    {
        if (field == null || !this.isIrregular())
            return false;

        return ((IrregularField) field).hasCells3D();
    }

    @Override
    public boolean isPoint() {
        return field != null && field instanceof PointField;
    }

}
