/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.types;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class IntImage
{

    private int width;

    public int getWidth()
    {
        return width;
    }

    private int height;

    public int getHeight()
    {
        return height;
    }

    private Integer[][] tab;

    public int getCell(int x, int y)
    {
        //System.out.println(x%width);
        //System.out.println(y%height);
        return tab[(x + width) % width][(y + height) % height];
    }

    public Integer[][] getContent()
    {
        return tab;
    }

    public void setValueAt(int x, int y, int value)
    {
        tab[(x + width) % width][(y + height) % height] = value;
    }

    public IntImage(IntImage from)
    {
        this(from.getWidth(), from.getHeight());
        for (int i = 0; i < width; ++i)
            for (int j = 0; j < height; ++j)
                tab[i][j] = from.getCell(i, j);
    }

    /**
     * Creates a new instance of IntImage
     */
    public IntImage()
    {
        this(32, 32);
    }

    public IntImage(int width, int height)
    {
        this.width = width;
        this.height = height;
        tab = new Integer[width][height];
        for (int i = 0; i < width; ++i)
            for (int j = 0; j < height; ++j)
                tab[i][j] = 0;
    }

    @Override
    public String toString()
    {
        return "IntImage[" + width + "," + height + "]" + "((" + super.toString() + "))\n";
    }
}
