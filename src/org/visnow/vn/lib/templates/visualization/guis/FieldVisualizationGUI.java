/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.templates.visualization.guis;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.visnow.jscic.Field;
import org.visnow.vn.geometries.gui.PresentationGUI;
import org.visnow.vn.system.swing.CustomSizePanel;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class FieldVisualizationGUI extends javax.swing.JPanel
{
    private JPanel computePanel;

    protected Field inField;
    protected CardLayout cL;

    /**
     * Creates new form FieldVisualizationGUI
     */
    public FieldVisualizationGUI()
    {
        initComponents();
        postInitComponents();
    }

    private void postInitComponents()
    {
        computePanel = new CustomSizePanel(new BorderLayout());
        computeScrollPane.setViewportView(computePanel);
        hidePresentation();
    }

    public void hidePresentation()
    {
        mainPane.remove(presentationGUI);
    }

    public void showPresentation()
    {
        if (mainPane.indexOfComponent(presentationGUI) == -1)
            mainPane.addTab("Presentation", presentationGUI);
    }
    
    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT
     * modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        computeScrollPane = new javax.swing.JScrollPane();
        mainPane = new javax.swing.JTabbedPane();
        presentationGUI = new org.visnow.vn.geometries.gui.PresentationGUI();

        computeScrollPane.setName("computeScrollPane"); // NOI18N

        setLayout(new java.awt.BorderLayout());

        mainPane.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        mainPane.setName("mainPane"); // NOI18N

        presentationGUI.setName("presentationGUI"); // NOI18N
        mainPane.addTab("Presentation", presentationGUI);

        add(mainPane, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    public void addComputeGUI(JPanel gui, String title)
    {
        computePanel.removeAll();
        computePanel.add(gui, BorderLayout.CENTER);
        mainPane.insertTab(title, null, computeScrollPane, "", 0);
        mainPane.setSelectedIndex(0);
    }
    
    public void addComputeGUI(JPanel gui)
    {
        addComputeGUI(gui, "Computation");
    }
    
    public void insertAdditionalGUI(JPanel gui, String title)
    {
        JPanel additionalPanel = new CustomSizePanel(new BorderLayout());
        JScrollPane additionalScrollPane = new javax.swing.JScrollPane();
        additionalScrollPane.setViewportView(additionalPanel);
        additionalPanel.add(gui, BorderLayout.NORTH);
        mainPane.insertTab(title, null, additionalScrollPane, "", 1);
    }

    public PresentationGUI getPresentationGUI()
    {
        return presentationGUI;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JScrollPane computeScrollPane;
    protected javax.swing.JTabbedPane mainPane;
    protected org.visnow.vn.geometries.gui.PresentationGUI presentationGUI;
    // End of variables declaration//GEN-END:variables

}
