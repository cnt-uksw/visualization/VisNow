/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.parameters;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import org.jogamp.java3d.Material;
import org.jogamp.java3d.PolygonAttributes;
import org.jogamp.java3d.TransparencyAttributes;
import org.jogamp.vecmath.Color3f;
import org.apache.log4j.Logger;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.PointField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.FieldType;
import static org.visnow.jscic.FieldType.*;
import org.visnow.vn.geometries.gui.RenderingGUI;
import org.visnow.vn.geometries.objects.GeometryObject;
import org.visnow.vn.geometries.objects.GeometryParent;
import org.visnow.vn.geometries.objects.FieldGeometry;
import org.visnow.vn.geometries.objects.RegularFieldGeometry;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenMaterial;
import org.visnow.vn.geometries.utils.ColorMapper;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * @author Bartosz Borucki, University of Warsaw, ICM
 * <p>
 */
public class RenderingParams
{

    private static final Logger LOGGER = Logger.getLogger(RenderingParams.class);

    public static final int OUTLINE_BOX = 128;
    public static final int SURFACE = 1;
    public static final int SEGMENT_CELLS = 2;
    public static final int EDGES = 4;
    public static final int POINT_CELLS = 8;
    public static final int NODES = 16;
    public static final int IMAGE = 64;
    public static final int UNSHADED = 0;
    public static final int GOURAUD_SHADED = 1;
    public static final int FLAT_SHADED = 2;
    public static final int BACKGROUND = 3;
    public static final int INHERIT = -1;

    protected static final Color   DEFAULT_COLOR = Color.WHITE;
    protected static final Color3f DEFAULT_AMBIENT_COLOR = new Color3f(.5f, .5f, .5f);
    protected static final Color3f DEFAULT_DIFFUSE_COLOR = new Color3f(.5f, .5f, .5f);
    protected static final Color3f DEFAULT_SPECULAR_COLOR = new Color3f(.2f, .2f, .2f);
    protected static final Font    DEFAULT_ANNO_FONT = new Font("Dialog", Font.PLAIN, 10);

    protected String name = "";
    protected int displayMode = SURFACE | POINT_CELLS | SEGMENT_CELLS | IMAGE;
    protected int shadingMode = GOURAUD_SHADED;
    protected float minEdgeDihedral = 0;
    protected byte surfaceOrientation = 1;
    protected FieldType type;

    protected OpenAppearance appearance = new OpenAppearance();
    protected OpenMaterial material = (OpenMaterial) appearance.getMaterial();
    protected OpenAppearance lineAppearance = new OpenAppearance();
    protected Color color = null;
    protected GeometryParent object = null;
    protected PresentationParams parentPresentationParams = null;
    protected Color bgrColor = Color.BLACK;
    protected Color3f bgrColor3f = ColorMapper.convertColorToColor3f(bgrColor);
    protected boolean lineLighting = false;
    protected boolean ignoreMask = false;
    protected boolean inherited = true;
    protected boolean active = true;
    protected boolean lightedBackground = true;
    protected boolean transparentlyRenderedMaskedNodes;
    protected RenderingGUI gui = null;
    protected int maxCellDims = 2;
    protected boolean regularFieldData = true;

    protected boolean objectHasNodeCells = false;
    protected boolean objectHasSegmentCells = false;
    protected boolean objectHasSurfaceCells = false;

    protected boolean silentInheritingStatus = false;

    protected RenderEventListener transparencyChangeListener = new RenderEventListener()
    {
        @Override
        public void renderExtentChanged(RenderEvent e)
        {
            if (e.getSource() instanceof TransparencyParams) {
                TransparencyParams trSource = (TransparencyParams) e.getSource();
                if (trSource.getComponentRange().getComponentSchema() == null && appearance.getTransparencyAttributes().getTransparency() == 0)
                    appearance.getTransparencyAttributes().setTransparencyMode(TransparencyAttributes.NONE);
                else
                    appearance.getTransparencyAttributes().setTransparencyMode(TransparencyAttributes.NICEST);
                if (active)
                   fireStateChanged(RenderEvent.TRANSPARENCY);
            }
        }
    };

    protected RenderEventListener parentChangeListener = new RenderEventListener()
    {
        @Override
        public void renderExtentChanged(RenderEvent e)
        {
            if ((e.getSource() instanceof RenderingParams) && inherited)
                copyValuesFrom((RenderingParams)e.getSource());
        }
    };


    /**
     * Creates a new instance of RenderingParams
     */
    public RenderingParams()
    {
        material.setColorTarget(Material.AMBIENT_AND_DIFFUSE);
        material.setAmbientColor(DEFAULT_AMBIENT_COLOR);
        material.setDiffuseColor(DEFAULT_DIFFUSE_COLOR);
        material.setSpecularColor(DEFAULT_SPECULAR_COLOR);
        material.setLightingEnable(true);
        boolean detach = detachUserData(appearance.getUserData());
        if (appearance != null && appearance.getPolygonAttributes() != null) {
            appearance.getPolygonAttributes().setCullFace(PolygonAttributes.CULL_NONE);
            appearance.getPolygonAttributes().setPolygonOffset(VisNow.get()!=null?VisNow.get().getMainConfig().getRenderingSurfaceOffset():200);
            appearance.getPolygonAttributes().setBackFaceNormalFlip(false);
            fireStateChanged(RenderEvent.GEOMETRY);
        }
        lineAppearance.getPointAttributes().setPointSize(3 * lineAppearance.getLineAttributes().getLineWidth());
        if (detach)
            attachUserData(appearance.getUserData());
    }

    public RenderingParams(GeometryParent object)
    {
        this();
        this.object = object;
    }

    public void setParentParams(PresentationParams parentParams)
    {
        this.parentPresentationParams = parentParams;
    }


    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public RenderingGUI getGui()
    {
        return gui;
    }

    public void setGui(RenderingGUI gui)
    {
        this.gui = gui;
    }

    public void copyValuesFrom(RenderingParams src)
    {
        active = false;
        boolean geometryChanged = (displayMode != src.displayMode) ||
                                  (shadingMode != src.shadingMode) ||
                                  (minEdgeDihedral != src.minEdgeDihedral);
        displayMode = src.displayMode;
        shadingMode = src.shadingMode;
        minEdgeDihedral = src.minEdgeDihedral;
        surfaceOrientation = src.surfaceOrientation;
        appearance.copyValuesFrom(src.appearance);
        lineAppearance.copyValuesFrom(src.lineAppearance);
        material.copyValuesFrom(src.material);
        color = src.color;
        if (gui != null)
            gui.updateDataValuesFromParams();
        active = true;
        if (geometryChanged)
            fireStateChanged(RenderEvent.GEOMETRY);
        else
            fireStateChanged(RenderEvent.APPEARANCE);
    }

    @Override
    public String toString()
    {
        StringBuilder s = new StringBuilder();
        if ((displayMode & SURFACE) != 0)
            s.append(" surface ");
        if ((displayMode & EDGES) != 0)
            s.append(" edges ");
        return s.toString();
    }

    /**
     * Getter for property color.
     * <p>
     * @return Value of property color.
     */

    public Color getColor()
    {
        if (color != null) {
            Color3f c = new Color3f();
            appearance.getColoringAttributes().getColor(c);
            return ColorMapper.convertColor3fToColor(c);
        }
        return DEFAULT_COLOR;
    }


    public OpenAppearance getAppearance()
    {
        return appearance;
    }


    public OpenAppearance getLineAppearance()
    {
        return lineAppearance;
    }


    public int getDisplayMode()
    {
        return displayMode;
    }


    public void setDisplayMode(int displayMode)
    {
        this.displayMode = displayMode;
        if (parentPresentationParams != null && !silentInheritingStatus)
            parentPresentationParams.setRenderingInherited(false);
        if (gui != null)
            gui.updateDataValuesFromParams();
        fireStateChanged(RenderEvent.GEOMETRY);
    }


    public byte getSurfaceOrientation()
    {
        return surfaceOrientation;
    }


    public void setSurfaceOrientation(byte orientation)
    {
        this.surfaceOrientation = orientation == 0 ? 0 : (byte)1;
        if (parentPresentationParams != null && !silentInheritingStatus)
            parentPresentationParams.setRenderingInherited(false);
        fireStateChanged(RenderEvent.GEOMETRY);
    }

    /**
     * Setter for property color.
     * <p>
     * @param color New value of property color.
     */

    public void setColor(Color color)
    {
        boolean detach = detachUserData(appearance.getUserData());
        float[] fC = new float[3];
        color.getColorComponents(fC);
        this.color = color;
        Color3f color3f = new Color3f(fC);
        appearance.getColoringAttributes().setColor(color3f);
        material.setAmbientColor(color3f);
        material.setDiffuseColor(color3f);
        if (object != null)
            object.setColor();
        if (detach)
            attachUserData(appearance.getUserData());
        if (parentPresentationParams != null && !silentInheritingStatus)
            parentPresentationParams.setRenderingInherited(false);
        fireStateChanged(RenderEvent.COLORS);
    }

    public void resetColor()
    {

        this.color = null;
    }

    /**
     * Getter for property ambientColor.
     * <p>
     * @return Value of property ambientColor.
     */

    public Color3f getAmbientColor()
    {
        Color3f c = new Color3f(0, 0, 0);
        material.getAmbientColor(c);
        return c;
    }

    /**
     * Setter for property ambientColor.
     * <p>
     * @param ambientColor New value of property fColor.
     */

    public void setAmbientColor(Color3f ambientColor)
    {
        material.setAmbientColor(ambientColor);
        if (parentPresentationParams != null && !silentInheritingStatus)
            parentPresentationParams.setRenderingInherited(false);
        fireStateChanged(RenderEvent.COLORS);
    }

    /**
     * Getter for property diffuseColor.
     * <p>
     * @return Value of property diffuseColor.
     */

    public Color3f getDiffuseColor()
    {
        Color3f c = new Color3f();
        material.getDiffuseColor(c);
        return c;
    }

    /**
     * Setter for property diffuseColor.
     * <p>
     * @param diffuseColor New value of property fColor.
     */

    public void setDiffuseColor(Color3f diffuseColor)
    {
        material.setDiffuseColor(diffuseColor);
        lineAppearance.getColoringAttributes().setColor(diffuseColor);
        if (parentPresentationParams != null && !silentInheritingStatus)
            parentPresentationParams.setRenderingInherited(false);
        fireStateChanged(RenderEvent.COLORS);
    }

    /**
     * Getter for property specularColor.
     * <p>
     * @return Value of property specularColor.
     */

    public Color3f getSpecularColor()
    {
        Color3f c = new Color3f();
        material.getSpecularColor(c);
        return c;
    }

    /**
     * Setter for property specularColor.
     * <p>
     * @param specularColor New value of property fColor.
     */

    public void setSpecularColor(Color3f specularColor)
    {
        material.setSpecularColor(specularColor);
        if (parentPresentationParams != null && !silentInheritingStatus)
            parentPresentationParams.setRenderingInherited(false);
        fireStateChanged(RenderEvent.COLORS);
    }

    /**
     * Getter for property transparency.
     * <p>
     * @return Value of property transparency.
     */

    public float getTransparency()
    {
        return appearance.getTransparencyAttributes().getTransparency();
    }

    /**
     * Setter for property transparency.
     * <p>
     * @param transparency New value of property transparency.
     */

    public void setTransparency(float transparency)
    {
        boolean detach = detachUserData(appearance.getUserData());
        if (transparency == 0)
            appearance.getTransparencyAttributes().setTransparencyMode(TransparencyAttributes.NONE);
        else
            appearance.getTransparencyAttributes().setTransparencyMode(TransparencyAttributes.NICEST);
        appearance.getTransparencyAttributes().setTransparency(transparency);
        if (object != null)
            object.setTransparency();
        if (detach)
            attachUserData(appearance.getUserData());
        if (parentPresentationParams != null && !silentInheritingStatus)
            parentPresentationParams.setRenderingInherited(false);
        fireStateChanged(RenderEvent.TRANSPARENCY);
    }

    protected int[] culling = new int[] {PolygonAttributes.CULL_NONE,
                                         PolygonAttributes.CULL_BACK,
                                         PolygonAttributes.CULL_FRONT
    };

    protected int cullFace = 0;

    public int getCullMode()
    {
        return cullFace;
    }

    public void setCullMode(int cullFace)
    {
        this.cullFace = cullFace;
        boolean detach = detachUserData(appearance.getUserData());
        appearance.getPolygonAttributes().setCullFace(culling[cullFace]);
        if (parentPresentationParams != null && !silentInheritingStatus)
            parentPresentationParams.setRenderingInherited(false);
        if (detach)
            attachUserData(appearance.getUserData());
    }

    /**
     * Getter for property shininess.
     * <p>
     * @return Value of property shininess.
     */

    public float getShininess()
    {
        return material.getShininess();
    }

    /**
     * Setter for property shininess.
     * <p>
     * @param shininess New value of property shininess.
     */

    public void setShininess(float shininess)
    {
        material.setShininess(shininess);
        if (object != null)
            object.setShininess();
        if (parentPresentationParams != null && !silentInheritingStatus)
            parentPresentationParams.setRenderingInherited(false);
    }

    /**
     * Holds value of property lineThickness.
     */
    /**
     * Getter for property lineThickness.
     * <p>
     * @return Value of property lineThickness.
     */

    public float getLineThickness()
    {
        return lineAppearance.getLineAttributes().getLineWidth();
    }

    /**
     * Setter for property lineThickness.
     * <p>
     * @param lineThickness New value of property lineThickness.
     */

    public void setLineThickness(float lineThickness)
    {

        boolean detach = detachUserData(lineAppearance.getUserData());
        lineAppearance.getLineAttributes().setLineWidth(lineThickness);
        lineAppearance.getPointAttributes().setPointSize(3 * lineThickness);
        if (object != null)
            object.setLineThickness();
        if (detach)
            attachUserData(lineAppearance.getUserData());
        if (parentPresentationParams != null && !silentInheritingStatus)
            parentPresentationParams.setRenderingInherited(false);
        fireStateChanged(RenderEvent.APPEARANCE);
    }


    public int getLineStyle()
    {
        return lineAppearance.getLineAttributes().getLinePattern();
    }


    public void setLineStyle(int lineStyle)
    {
        boolean detach = detachUserData(lineAppearance.getUserData());
        lineAppearance.getLineAttributes().setLinePattern(lineStyle);
        if (object != null)
            object.setLineStyle();
        if (detach)
            attachUserData(lineAppearance.getUserData());
        if (parentPresentationParams != null && !silentInheritingStatus)
            parentPresentationParams.setRenderingInherited(false);
        fireStateChanged(RenderEvent.APPEARANCE);
    }


    public boolean isLineLighting()
    {
        return lineLighting;
    }


    public void setLineLighting(boolean lineLighting)
    {
        this.lineLighting = lineLighting;
        fireStateChanged(RenderEvent.GEOMETRY);
    }

    public Color getBackgroundColor()
    {
        return bgrColor;
    }

    public void setBackgroundColor(Color backgroundColor)
    {
        bgrColor3f = ColorMapper.convertColorToColor3f(backgroundColor);
        setDiffuseColor(bgrColor3f);
        material.setDiffuseColor(bgrColor3f);
        material.setAmbientColor(bgrColor3f);
        material.setEmissiveColor(bgrColor3f);
        fireStateChanged(RenderEvent.COLORS);
    }

    public int getShadingMode()
    {
        return shadingMode;
    }

    public void setShadingMode(int shadingMode)
    {
        boolean detach = detachUserData(appearance.getUserData());
        this.shadingMode = shadingMode;
        switch (shadingMode) {
        case GOURAUD_SHADED:
//            appearance.getColoringAttributes().setShadeModel(ColoringAttributes.SHADE_GOURAUD);
            appearance.setMaterial(material);
            break;
        case FLAT_SHADED:
//            appearance.getColoringAttributes().setShadeModel(ColoringAttributes.SHADE_FLAT);
            appearance.setMaterial(material);
            break;
        case UNSHADED:
            appearance.setMaterial(null);
            break;
        case BACKGROUND:
            displayMode |= FieldGeometry.EDGES;
            if (appearance != null && appearance.getColoringAttributes() != null)
                appearance.getColoringAttributes().setColor(bgrColor3f);
            setDiffuseColor(bgrColor3f);
            material.setDiffuseColor(bgrColor3f);
            material.setAmbientColor(bgrColor3f);
            material.setEmissiveColor(bgrColor3f);
            if (gui != null)
                gui.updateDataValuesFromParams();
            break;
        }
        if (parentPresentationParams != null && !silentInheritingStatus)
            parentPresentationParams.setRenderingInherited(false);
        fireStateChanged(RenderEvent.GEOMETRY);
        if (detach)
            attachUserData(appearance.getUserData());
    }


    public void setAppearance(OpenAppearance appearance)
    {
        this.appearance = appearance;
    }


    public void setLineAppearance(OpenAppearance appearance)
    {
        this.lineAppearance = appearance;
    }


    public float getMinEdgeDihedral()
    {
        return minEdgeDihedral;
    }


    public void setMinEdgeDihedral(float minEdgeDihedral)
    {
        this.minEdgeDihedral = minEdgeDihedral;
        if (parentPresentationParams != null && !silentInheritingStatus)
            parentPresentationParams.setRenderingInherited(false);
        fireStateChanged(RenderEvent.GEOMETRY);
    }

    public void setObjectCharacteristics(Object field)
    {

        regularFieldData = false;
        if (field instanceof RegularField) {
            type = FieldType.FIELD_REGULAR;
            maxCellDims = ((RegularField)field).getDimNum();
            if (maxCellDims == 2) {
                displayMode |= IMAGE;
                objectHasNodeCells = false;
                objectHasSegmentCells = false;
                objectHasSurfaceCells = true;
            }
            else if (maxCellDims == 1) {
                displayMode &= ~IMAGE;
                objectHasNodeCells = false;
                objectHasSegmentCells = true;
                objectHasSurfaceCells = false;
            }
            regularFieldData = true;
        }
        else if (field instanceof PointField) {
            maxCellDims = 0;
            type = FieldType.FIELD_POINT;
            regularFieldData = false;
        }
        else if (field instanceof CellSet) {
            type = FieldType.FIELD_IRREGULAR;
            CellSet cs = (CellSet)field;
            objectHasNodeCells = cs.hasCellsPoint();
            objectHasSegmentCells = cs.hasCellsSegment();
            objectHasSurfaceCells = cs.hasCells2D() || cs.hasCells3D();
        }
        else if (field instanceof IrregularField) {
            type = FieldType.FIELD_IRREGULAR;
            for (CellSet cs : ((IrregularField)field).getCellSets()) {
                objectHasNodeCells |= cs.hasCellsPoint();
                objectHasSegmentCells |= cs.hasCellsSegment();
                objectHasSurfaceCells |= cs.hasCells2D() || cs.hasCells3D();
            }
        }
        maxCellDims = 0;
        if (objectHasSegmentCells)
            maxCellDims = 1;
        if (objectHasSurfaceCells)
            maxCellDims = 2;
        if (gui != null)
            gui.setFieldType(type, maxCellDims);
    }

    public boolean isRenderingRegularField()
    {
        return regularFieldData;
    }

    public int getMaxCellDims()
    {
        return maxCellDims;
    }

    public float getPointSize()
    {
        return lineAppearance.getPointAttributes().getPointSize();
    }


    public void setPointSize(float pointSize)
    {
        boolean detach = detachUserData(lineAppearance.getUserData());
        lineAppearance.getPointAttributes().setPointSize(pointSize);
        if (detach)
            attachUserData(lineAppearance.getUserData());
    }

    public boolean isInherited()
    {
        return inherited;
    }

    public void setInherited(boolean inherited)
    {
        this.inherited = inherited;
    }


    public boolean ignoreMask()
    {
        return ignoreMask;
    }


    public void setIgnoreMask(boolean ignoreMask)
    {
        this.ignoreMask = ignoreMask;
        fireStateChanged(RenderEvent.GEOMETRY);
    }

    /**
     * Get the value of lightedBackground
     *
     * @return the value of lightedBackground
     */

    public boolean isLightedBackside()
    {
        return lightedBackground;
    }

    /**
     * Set the value of lightedBackground
     *
     * @param lightedBackground new value of lightedBackground
     */

    public void setLightedBackside(boolean lightedBackground)
    {
        this.lightedBackground = lightedBackground;
        fireStateChanged(RenderEvent.APPEARANCE);
    }

    /**
     * Utility field holding list of RenderEventListeners.
     */
    private ArrayList<RenderEventListener> renderEventListenerList
        = new ArrayList<>();

    /**
     * Registers RenderEventListener to receive events.
     * <p>
     * @param listener The listener to register.
     */

    public synchronized void addRenderEventListener(RenderEventListener listener)
    {
        if (!renderEventListenerList.contains(listener))
            renderEventListenerList.add(listener);
    }

    /**
     * Removes RenderEventListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */

    public synchronized void removeRenderEventListener(RenderEventListener listener)
    {
        renderEventListenerList.remove(listener);
    }

    /**
     * Removes all RenderEventListeners
     */
    public synchronized void clearRenderEventListeners()
    {
        renderEventListenerList.clear();
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param change
     */
    public final void fireStateChanged(int change)
    {
        if (!active)
            return;
        RenderEvent e = new RenderEvent(this, change);
        try {
            for (RenderEventListener renderEventListener : renderEventListenerList)
                renderEventListener.renderExtentChanged(e);
        } catch (ConcurrentModificationException exc) {
//            VisNowCallTrace.trace();
        }
        if (parentPresentationParams != null)
            parentPresentationParams.spreadRenderingParams();
    }

    public float getSurfaceOffset()
    {
        return appearance.getPolygonAttributes().getPolygonOffset();
    }

    /**
     * Get the value of transparentlyRenderedMaskedNodes
     *
     * @return true if nodes with mask=false are rendered as transparent,
     *         false if cells containing masked nodes are not rendered
     */

    public boolean isTransparentlyRenderedMaskedNodes()
    {
        return transparentlyRenderedMaskedNodes;
    }

    /**
     * Set the value of transparentlyRenderedMaskedNodes
     *
     * @param transparentlyRenderedMaskedNodes true if nodes with mask=false are rendered as transparent,
     *                                         false if cells containing masked nodes are not rendered
     */

    public void setTransparentlyRenderedMaskedNodes(boolean transparentlyRenderedMaskedNodes)
    {
        boolean change = this.transparentlyRenderedMaskedNodes != transparentlyRenderedMaskedNodes;
        this.transparentlyRenderedMaskedNodes = transparentlyRenderedMaskedNodes;
        if (change)
            fireStateChanged(RenderEvent.GEOMETRY);
    }

    public void setSurfaceOffset(float offset)
    {
        boolean detach = detachUserData(appearance.getUserData());
        if (appearance != null && appearance.getPolygonAttributes() != null) {
            appearance.getPolygonAttributes().setPolygonOffset(offset);
            fireStateChanged(RenderEvent.GEOMETRY);
        }
        if (detach)
            attachUserData(appearance.getUserData());
    }

    private boolean detachUserData(Object obj)
    {
        if (obj != null) {
            if (obj instanceof OpenBranchGroup) {
                return ((OpenBranchGroup) obj).postdetach();
            } else if (obj instanceof GeometryObject) {
                return ((GeometryObject) obj).getGeometryObj().postdetach();
            } else if (obj instanceof RegularFieldGeometry) {
                return ((RegularFieldGeometry) obj).getGeometryObject().postdetach();
            }
        }
        return false;
    }

    private void attachUserData(Object obj)
    {
        if (obj != null) {
            if (obj instanceof OpenBranchGroup) {
                ((OpenBranchGroup) obj).postattach();
            } else if (obj instanceof GeometryObject) {
                ((GeometryObject) obj).getGeometryObj().postattach();
            } else if (obj instanceof RegularFieldGeometry) {
                ((RegularFieldGeometry) obj).getGeometryObject().postattach();
            }
        }
    }

    public boolean isSilentInheritingStatus()
    {
        return silentInheritingStatus;
    }

    public void setSilentInheritingStatus(boolean silentInheritingStatus)
    {
        this.silentInheritingStatus = silentInheritingStatus;
    }

    public RenderEventListener getParentChangeListener()
    {
        return parentChangeListener;
    }

    public RenderEventListener getTransparencyChangeListener()
    {
        return transparencyChangeListener;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String[] valuesToStringArray()
    {
        ArrayList<String> res = new ArrayList<>();
        res.add("displayMode: " + displayMode);
        res.add("shadingMode: " + shadingMode);
        res.add("minEdgeDihedral: " + minEdgeDihedral);
        res.add("surfaceOrientation: " + surfaceOrientation);
        res.add("surfaceOffset: " + getSurfaceOffset());
        res.add("lightedBackground: " + lightedBackground);
        if (material != null) {
            Color3f specColor = new Color3f();
            material.getSpecularColor(specColor);
            float[] dc = new float[3];
            specColor.get(dc);
            res.add(String.format("specularColor: %6.3f %6.3f %6.3f", dc[0], dc[1], dc[2]));
        }
        if (lineAppearance != null) {
            res.add("lineStyle: " + lineAppearance.getLineAttributes().getLinePattern());
            res.add("lineThickness: " + lineAppearance.getLineAttributes().getLineWidth());
        }

        String[] r = new String[res.size()];
        for (int i = 0; i < r.length; i++)
           r[i] = res.get(i);
        return r;
    }

    public void restoreValuesFrom(String[] saved)
    {
        LOGGER.debug("restoring presentationParams");
        for (String line : saved) {
            if (line.trim().startsWith("displayMode:")) {
                try {
                    displayMode = Integer.parseInt(line.trim().split(" *:* +")[1]);
                }catch (Exception e) {}
                continue;
            }
            if (line.trim().startsWith("shadingMode:")) {
                try {
                    shadingMode = Integer.parseInt(line.trim().split(" *:* +")[1]);
                }catch (Exception e) {}
                continue;
            }
            if (line.trim().startsWith("minEdgeDihedral:")) {
                try {
                    minEdgeDihedral = Float.parseFloat(line.trim().split(" *:* +")[1]);
                }catch (Exception e) {}
                continue;
            }
            if (line.trim().startsWith("surfaceOrientation:")) {
                surfaceOrientation = line.endsWith("true") ? 1 : (byte)0;
                continue;
            }
            if (line.trim().startsWith("surfaceOffset:")) {
                try {
                    int surfaceOffset = (int) Float.parseFloat(line.trim().split(" *:* +")[1]);
                    setSurfaceOffset(surfaceOffset);
                }catch (Exception e) {}
                continue;
            }
            if (line.trim().startsWith("lightedBackground:")) {
                lightedBackground = line.endsWith("true");
                continue;
            }
            if (line.trim().startsWith("specularColor:") && material != null) {
                try {
                    String[] s = line.trim().split(" *:* +");
                    material.setSpecularColor(Float.parseFloat(s[1]), Float.parseFloat(s[2]), Float.parseFloat(s[3]));
                }catch (Exception e) {}
                continue;
            }
            if (lineAppearance != null) {
                if (line.trim().startsWith("lineStyle:")) {
                    try {
                        lineAppearance.getLineAttributes().setLinePattern(Integer.parseInt(line.trim().split(" *:* +")[1]));
                    }catch (Exception e) {}
                    continue;
                }
                if (line.trim().startsWith("lineThickness:")) {
                    try {
                        lineAppearance.getLineAttributes().setLineWidth(Float.parseFloat(line.trim().split(" *:* +")[1]));
                    }catch (Exception e) {}
                }
            }
        }
        if (gui != null)
            gui.updateDataValuesFromParams();
    }

}
