/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.parameters;

import java.util.ArrayList;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.geometries.gui.VolumeShadingGUI;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class VolumeShadingParams
{
    public static enum ShadingType {NO_SHADING, TRANSPARENCY_GRADIENT, COMPONENT_GRADIENT};
    public static final String SHADING_TYPE      = "shading type";
    public static final String SHADING_COMPONENT = "shading component";
    public static final String SHADING_RANGE     = "shading range";
    public static final String SHADING_INTENSITY = "shading intensity";
    
    private ShadingType type = ShadingType.NO_SHADING;
    private ComponentFeature component = new ComponentFeature(true, false, false,false, false, false);
    private float[] shadingRange = new float[] {.1f,2};
    private float shadingIntensity = .3f;
    private boolean active = true;
    
    private VolumeShadingGUI gui = null;

    @SuppressWarnings("unchecked")
    public VolumeShadingParams()
    {
    }

    public ShadingType getShadingType()
    {
        return type;
    }

    public void setShadingType(ShadingType shadow)
    {
        type = shadow;
        fireParameterChanged(SHADING_TYPE);
    }

    public String getShadingComponent()
    {
        return component.getComponentName();
    }

    public void setShadingComponent(String cmp)
    {
        component.setComponentSchema(cmp);
        fireParameterChanged(SHADING_COMPONENT);
    }

    public float[] getShadingRange()
    {
        return shadingRange;
    }

    public void setShadingRange(float[] shadingRange)
    {
        this.shadingRange = shadingRange;
        fireParameterChanged(SHADING_RANGE);
    }
    
    public float getShadingIntensity()
    {
        return shadingIntensity;
    }

    public void setShadingIntensity(float shadingIntensity)
    {
        this.shadingIntensity = shadingIntensity;
        fireParameterChanged(SHADING_INTENSITY);
    }   
    
    public final void setInDataSchema(DataContainerSchema dataSchema)
    {
        component.setContainerSchema(dataSchema);
    }

    public void setGui(VolumeShadingGUI gui) {
        this.gui = gui;
    }

    public ComponentFeature getComponent() {
        return component;
    }
        
    public String[] valuesToStringArray()
    {
        ArrayList<String> res = new ArrayList<>();
        res.add(SHADING_TYPE + ": " + type.name());
        res.add(SHADING_COMPONENT + ": " + component.getComponentName());
        res.add(SHADING_RANGE + ": " + shadingRange[0] + " " + shadingRange[1]);
        res.add(SHADING_INTENSITY + ": " + shadingIntensity);
        String[] r = new String[res.size()];
        for (int i = 0; i < r.length; i++) 
           r[i] = res.get(i);
        return r;
    }
    
    public void restoreValuesFrom(String[] saved)
    {
        active = false;
        itemLoop:
        for (String saved1 : saved) {
            if (saved1.trim().startsWith(SHADING_TYPE)) {
                try {
                    String s = saved1.trim().split(" *: +")[1];
                    for (ShadingType shType: ShadingType.values())
                        if (shType.name().equals(s))
                            setShadingType(shType);
                }catch (Exception e) {}
                continue;
            }
            if (saved1.trim().startsWith(SHADING_COMPONENT)) {
                try {
                    component.setComponentSchema(saved1.trim().split(" *:* +")[1]);
                }catch (Exception e) {}
                continue;
            }
            if (saved1.trim().startsWith(SHADING_RANGE)) {
                try {
                    String c = saved1.trim().split(" *: +")[1];
                    String[] vals = c.trim().split(" +");
                    shadingRange = new float[] {Float.parseFloat(vals[0]),
                        Float.parseFloat(vals[1])};
                }catch (Exception e) {}
                continue;
            }
            if (saved1.trim().startsWith(SHADING_INTENSITY)) {
                try {
                    shadingIntensity = Float.parseFloat(saved1.trim().split(" *: +")[1]);
                }catch (Exception e) {}
            }
        }
        if (gui != null)
            gui.updateDataValuesFromParams();
        active = true;
        fireParameterChanged(SHADING_TYPE);
    }
    
//<editor-fold defaultstate="collapsed" desc=" Parameter change listeners ">
    private ArrayList<ParameterChangeListener> listeners = new ArrayList<>();

    public synchronized void addParameterChangelistener(ParameterChangeListener listener)
    {
        listeners.add(listener);
    }

    public synchronized void removeParameterChangeListener(ParameterChangeListener listener)
    {
        listeners.remove(listener);
    }

    public void fireParameterChanged(String parameter)
    {
        if (active)
            for (ParameterChangeListener listener : listeners)
                listener.parameterChanged(parameter);
    }

    public static void main(String args[])
    {
        VolumeShadingParams p = new VolumeShadingParams();
        String[] s = p.valuesToStringArray();
        for (String item : s) {
            System.out.println(item);
        }
        String[] t = new String[4];
        t[0] = "shading type: " + ShadingType.values()[1];
        t[1] = "shading component: null";
        t[2] = "shading range: 1.0 5.0";
        t[3] = "shading intensity: 0.55";
        p.restoreValuesFrom(t);
        s = p.valuesToStringArray();
        for (String item : s) {
            System.out.println(item);
        }
    }  
}
