/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.parameters;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.apache.log4j.Logger;
import org.visnow.jscic.RegularFieldSchema;
import org.visnow.vn.geometries.gui.RegularField3DMapPanel;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class RegularField3DParams
{
    public static final int SLICE = 0;
    public static final int AVG = 1;
    public static final int MAX = 2;
    public static final int STDDEV = 3;
    public static final int GRID_TYPE_NONE = 0;
    public static final int GRID_TYPE_POINTS = 1;
    public static final int GRID_TYPE_LINES = 2;
    public static final int RENDER_VOLUME = 3;
    public static final int NO_FACES = 0;
    public static final int INSIDE_BOX = 1;
    public static final int BOX = 2;
    public static final int SELECTED_FACES = 3;
    protected RegularFieldSchema inFieldSchema;
    protected boolean outline = true;
    protected boolean active = true;
    protected int facesDisplay = NO_FACES;
    protected boolean[][] surFaces = {{false, false}, {false, false}, {false, false}};
    protected Color boxColor = Color.LIGHT_GRAY;
    protected int dataMap = SLICE;
    protected int gridLines = 20;
    protected int gridWidth = 10;
    protected int gridType = GRID_TYPE_NONE;
    protected boolean inside = true;
    protected boolean volumeRenderingPossible = false;
    protected VolumeShadingParams volumeShadingParams = new VolumeShadingParams();
    protected RegularField3DMapPanel gui = null;
    private static final Logger LOGGER = Logger.getLogger(RegularField3DParams.class);

    public RegularFieldSchema getInFieldSchema()
    {
        return inFieldSchema;
    }

    public void setInFieldSchema(RegularFieldSchema inFieldSchema) {
        this.inFieldSchema = inFieldSchema;
        volumeShadingParams.setInDataSchema(inFieldSchema);
    }

    public void setGui(RegularField3DMapPanel gui)
    {
        this.gui = gui;
    }

    /**
     * Get the value of dataMap
     *
     * @return the value of dataMap
     */
    public int getDataMap()
    {
        return dataMap;
    }

    /**
     * Set the value of dataMap
     *
     * @param dataMap new value of dataMap
     */
    public void setDataMap(int dataMap)
    {
        this.dataMap = dataMap;
        fireStateChanged();
    }

    /**
     * Get the value of boxColor
     *
     * @return the value of boxColor
     */
    public Color getBoxColor()
    {
        return boxColor;
    }

    /**
     * Set the value of boxColor
     *
     * @param boxColor new value of boxColor
     */
    public void setBoxColor(Color boxColor)
    {
        this.boxColor = boxColor;
        fireStateChanged();
    }

    /**
     * Get the value of surFaces
     *
     * @return the value of surFaces
     */
    public boolean[][] isSurFaces()
    {
        return surFaces;
    }

    /**
     * Set the value of surFaces
     *
     * @param surFaces new value of surFaces
     */
    public void setSurFaces(boolean[][] surFaces)
    {
        this.surFaces = surFaces;
        fireStateChanged();
    }

    /**
     * Get the value of surFaces at specified [ind][side]
     *
     * @param ind
     * @param side
     *             <p>
     * @return the value of surFaces at specified [ind][side]
     */
    public boolean isSurFaces(int ind, int side)
    {
        return this.surFaces[ind][side];
    }

    /**
     * Set the value of surFaces at specified ind][side.
     *
     * @param ind
     * @param side
     * @param newSurFaces new value of surFaces at specified [ind][side]
     */
    public void setSurfaces(int ind, int side, boolean newSurFaces)
    {
        this.surFaces[ind][side] = newSurFaces;
        fireStateChanged();
    }

    public float getGridWidth()
    {
        return gridWidth / 10.f + .01f;
    }

    public void setGridWidth(int gridWidth)
    {
        this.gridWidth = gridWidth;
        fireStateChanged();
    }

    /**
     * Get the value of gridLines
     *
     * @return the value of gridLines
     */
    public int getGridLines()
    {
        return gridLines;
    }

    /**
     * Set the value of gridLines
     *
     * @param gridLines new value of gridLines
     */
    public void setGridLines(int gridLines)
    {
        this.gridLines = gridLines;
        fireStateChanged();
    }

    public boolean isOutline()
    {
        return outline;
    }

    public void setOutline(boolean outline)
    {
        this.outline = outline;
        fireStateChanged();
    }

    public boolean isVolumeRenderingPossible()
    {
        return volumeRenderingPossible;
    }

    public void setVolumeRenderingPossible(boolean volumeRenderingPossible)
    {
        this.volumeRenderingPossible = volumeRenderingPossible;
        if (gui != null)
            gui.setVolumeRenderingPossible(volumeRenderingPossible);
    }
    
    /**
     * Get the value of gridType
     *
     * @return the value of gridType
     */
    public int getGridType()
    {
        return gridType;
    }

    /**
     * Set the value of gridType
     *
     * @param gridType new value of gridType
     */
    public void setGridType(int gridType)
    {
        this.gridType = gridType;
        fireStateChanged();
    }

    public boolean showInside()
    {
        return inside;
    }

    public void setShowInside(boolean in)
    {
        inside = in;
        fireStateChanged();
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
        fireStateChanged();
    }

    public int getFacesDisplay()
    {
        return facesDisplay;
    }

    public void setFacesDisplay(int facesDisplay)
    {
        this.facesDisplay = facesDisplay;
        switch (facesDisplay) {
            case NO_FACES:
                for (int i = 0; i < surFaces.length; i++) 
                    for (int j = 0; j < surFaces[i].length; j++)
                        surFaces[i][j] = false;
                setSurFaces(new boolean[][]{{false, false}, {false, false}, {false, false}});
                break;
            case INSIDE_BOX:
                setShowInside(true);
                for (int i = 0; i < surFaces.length; i++) 
                    for (int j = 0; j < surFaces[i].length; j++)
                        surFaces[i][j] = true;
                break;
            case BOX:
                setShowInside(false);
                for (int i = 0; i < surFaces.length; i++) 
                    for (int j = 0; j < surFaces[i].length; j++)
                        surFaces[i][j] = true;
                break;
            case SELECTED_FACES:
                setShowInside(false);
                break;
        }
    }

    public boolean isInside()
    {
        return inside;
    }

    public void setInside(boolean inside)
    {
        this.inside = inside;
    }
    
    
    //<editor-fold defaultstate="collapsed" desc=" Change listeners ">
    /**
     * Utility field holding list of ChangeListeners.
     */
    private transient ArrayList<ChangeListener> changeListenerList = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     * <p>
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     */
    public void fireStateChanged()
    {
        if (active) {
            ChangeEvent e = new ChangeEvent(this);
            for (ChangeListener listener : changeListenerList)
                listener.stateChanged(e);
        }
    }
    //</editor-fold>

    public VolumeShadingParams getVolumeShadingParams() {
        return volumeShadingParams;
    }
    
    public String[] valuesToStringArray()
    {
        Vector<String> res = new Vector<>();
        res.add("outline: " + outline);
        res.add("surFaces: " + surFaces[0][0] + " " + surFaces[0][1] + " " + 
                               surFaces[1][0] + " " + surFaces[1][1] + " " + 
                               surFaces[2][0] + " " + surFaces[2][1]);
        float[] dc = new float[4];
        boxColor.getComponents(dc);
        res.add(String.format("boxColor: %6.3f %6.3f %6.3f %6.3f", dc[0], dc[1], dc[2], dc[3])); 
        res.add("dataMap: " + dataMap);
        res.add("gridLines: " + gridLines);
        res.add("gridWidth: " + gridWidth);
        res.add("gridType: " + gridType);
        res.add("inside: " + inside);
        String[] r = new String[res.size()];
        for (int i = 0; i < r.length; i++) 
           r[i] = res.get(i);
        return r;
    }
    
    public void restoreValuesFrom(String[] saved)
    {
        LOGGER.debug("restoring RegularField3DParams");
        setActive(false);
        for (String s : saved) {
            if (s.startsWith("outline: "))
                 outline = Boolean.parseBoolean(s.trim().split(" *:* +")[1]);
            if (s.trim().startsWith("surFaces: ")) {
                String[] v = s.trim().split(" *:* +");
                for (int i = 0, k = 1; i < surFaces.length; i++) 
                    for (int j = 0; j < surFaces[0].length; j++, k++) 
                        surFaces[i][j] = Boolean.parseBoolean(v[k]);
            }
            if (s.trim().startsWith("boxColor: ")) {
                String[] v = s.trim().split(" *:* +");
                boxColor = new Color(Float.parseFloat(v[1]), Float.parseFloat(v[2]), Float.parseFloat(v[3]));
            }
            if (s.trim().startsWith("dataMap: "))
                dataMap = Integer.parseInt(s.trim().split(" *:* +")[1]);
            if (s.trim().startsWith("gridLines: "))
                gridLines = Integer.parseInt(s.trim().split(" *:* +")[1]);
            if (s.trim().startsWith("gridWidth: "))
                gridWidth = Integer.parseInt(s.trim().split(" *:* +")[1]);
            if (s.trim().startsWith("gridType: "))
                gridType = Integer.parseInt(s.trim().split(" *:* +")[1]);
            if (s.trim().startsWith("inside: "))
                inside = Boolean.parseBoolean(s.trim().split(" *:* +")[1]);
        }
        if (gui != null)
            gui.updateDataValuesFromParams();
        active = true;
        fireStateChanged();
    }
}
