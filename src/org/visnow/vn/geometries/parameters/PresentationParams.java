/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.parameters;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.apache.log4j.Logger;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.Field;
import org.visnow.jscic.FieldType;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.PointField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.RegularFieldSchema;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.geometries.gui.PresentationGUI;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.StringUtils;

/**
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * University of Warsaw, ICM
 */
public class PresentationParams
{
    private static final Logger LOGGER = Logger.getLogger(PresentationParams.class);

    public static final ParameterName<String> VNA_PARAMETER_NAME
            = new ParameterName<>("org.visnow.vn.geometries.parameters.PresentationParams");

    public static final int NO_PICK_INDICATOR  = 0;
    public static final int BOX_PICK_INDICATOR = 1;
    public static final int EDG_PICK_INDICATOR = 2;
    public static final int SRF_PICK_INDICATOR = 4;

    protected String name = "";
    protected DataMappingParams dataMappingParams;
    protected RenderingParams renderingParams;
    protected TransformParams transformParams;
    protected RegularField3DParams content3DParams = new RegularField3DParams();
    protected PointFieldRenderingParams pointFieldRenderingParams = new PointFieldRenderingParams();

    protected int dim = -1;

    protected boolean active = true;

    protected boolean cellSelectionEnabled = false;
    protected boolean pickSelectionEnabled = false;
    protected boolean content3DEnabled = false;

    protected boolean selectionActive = false;
    protected int selectByComponent = -1;
    protected float selectOver = 0;
    protected float selectUnder = 1;
    protected int pickIndicator = NO_PICK_INDICATOR;

    protected PresentationParams parentParams = null;
    protected boolean mappingInherited = false;
    protected boolean renderingInherited = false;
    protected boolean transformInherited = false;

    protected ArrayList<PresentationParams> childrenParams = new ArrayList<PresentationParams>();
    protected PresentationGUI ui = null;

    public PresentationParams()
    {
        dataMappingParams = new DataMappingParams();
        renderingParams = new RenderingParams();
        transformParams = new TransformParams();
        dataMappingParams.getTransparencyParams().getComponentRange().addNull(true);
        dataMappingParams.getTransparencyParams().getComponentRange().prefereNull(true);
        dataMappingParams.getTransparencyParams().addListener(renderingParams.getTransparencyChangeListener());
        dataMappingParams.setParentParams(this);
        renderingParams.setParentParams(this);
    }

    public PresentationParams(String name)
    {
        this();
        setName(name);
    }

    public PresentationParams(DataMappingParams mappingParams,
                              RenderingParams displayParams,
                              TransformParams transformParams)
    {
        this.renderingParams = displayParams;
        this.dataMappingParams = mappingParams;
        dataMappingParams.getTransparencyParams().addListener(renderingParams.getTransparencyChangeListener());
        this.transformParams = transformParams;
        dataMappingParams.setParentParams(this);
        renderingParams.setParentParams(this);
    }

    public PresentationParams(RegularField3DParams content3DParams,
                              DataMappingParams mappingParams,
                              RenderingParams displayParams,
                              TransformParams transformParams)
    {
        this(mappingParams, displayParams, transformParams);
        this.content3DParams = content3DParams;
        dataMappingParams.setParentParams(this);
        renderingParams.setParentParams(this);
    }

    @Override
    public String toString()
    {
        return name;
    }

    public String getName()
    {
        return name;
    }

    public final void setName(String name)
    {
        this.name = name;
        dataMappingParams.setName(name);
        renderingParams.setName(name);
    }

    public void setGUI(PresentationGUI ui)
    {
        this.ui = ui;
    }

    /**
     * Get the value of transformParams
     *
     * @return the value of transformParams
     */
    public TransformParams getTransformParams()
    {
        return transformParams;
    }

    public PointFieldRenderingParams getPointFieldParams()
    {
        return pointFieldRenderingParams;
    }

    /**
     * Get the value of renderingParams
     *
     * @return the value of renderingParams
     */
    public RenderingParams getRenderingParams()
    {
        return renderingParams;
    }

    public boolean isCellSelectionEnabled()
    {
        return cellSelectionEnabled;
    }

    public boolean isPickSelectionEnabled()
    {
        return pickSelectionEnabled;
    }

    public boolean isContent3DEnabled()
    {
        return content3DEnabled;
    }

    /**
     * Get the value of nodeDataMappingParams or cellDataMappingParams
     * depending on the value of mappingCellData
     *
     * @return the value of currently set DataMappingParams
     */
    public DataMappingParams getDataMappingParams()
    {
        return dataMappingParams;
    }

    public boolean isMappingCellData()
    {
        return dataMappingParams.isCellDataMapped();
    }

    public void setMappingCellData(boolean mappingCellData)
    {
        dataMappingParams.setCellDataMapped(mappingCellData);
    }

    public void setInField(RegularField field)
    {
        if (field == null)
            return;
        int oldDim = dim;
        renderingParams.setInherited(false);
        dataMappingParams.setInherited(false);
        renderingInherited = mappingInherited = false;
        childrenParams.clear();
        setName(field.getName());
        dim = field.getDimNum();
        boolean oldActive = active;
        renderingParams.setActive(false);
        dataMappingParams.setActive(false);
        dataMappingParams.getTransparencyParams().getComponentRange().addNull(true).prefereNull(dim != 3 || field.hasCoords());
        content3DEnabled = (dim == 3);
        content3DParams.setVolumeRenderingPossible(field.getDimNum() == 3 && !field.hasCoords());
        if (content3DParams.volumeRenderingPossible)
            content3DParams.getVolumeShadingParams().setInDataSchema(field.getSchema());
        dataMappingParams.setInData(field, (DataContainerSchema) null);
        renderingParams.setObjectCharacteristics(field);

        if (ui != null) {
            final PresentationParams thisParams = this;
            SwingInstancer.swingRunAndWait(new Runnable()
            {
                @Override
                public void run()
                {
                    ui.getRenderingGUI().setFieldType(FieldType.FIELD_REGULAR, dim);
                    ui.setPresentationParams(thisParams);
                    ui.updateParams();
                }
            });
        }
        if (field.isAffine() && field.getDimNum() == 3)
            content3DParams.setInFieldSchema(field.getSchema());
        active = oldActive;
        renderingParams.setActive(active);
        dataMappingParams.setActive(active);
    }

    public void setInField(IrregularField field)
    {
        setName(field.getName());
        boolean oldActive = active;
        active = false;
        renderingParams.setActive(false);
        dataMappingParams.setActive(false);
        dataMappingParams.getTransparencyParams().getComponentRange().addNull(true).prefereNull(true);
        content3DEnabled = false;
        renderingParams.setObjectCharacteristics(field);
        field.updateCellDataSchema();
        dataMappingParams.setInData(field, field.getSchema().getCellDataSchema());
        ArrayList<PresentationParams> tmpChildrenParams = new ArrayList<>();
        for (int i = 0; i < field.getNCellSets(); i++) {
            CellSet cs = field.getCellSet(i);
            PresentationParams tParam = null;
            for (PresentationParams childParam : childrenParams)
                if (childParam.getName().equalsIgnoreCase(cs.getName()) &&
                        childParam.getCellDataSchema().isCompatibleWith(cs.getSchema(), true))
                    tParam = childParam;
            if (tParam == null) {
                tParam = new PresentationParams(cs.getName());
                tParam.getDataMappingParams().setInData(field, cs);
                dataMappingParams.addRenderEventListener(tParam.getDataMappingParams().getParentChangeListener());
                tParam.renderingParams.setObjectCharacteristics(cs);
                renderingParams.addRenderEventListener(tParam.getRenderingParams().getParentChangeListener());
                tParam.setParentParams(this);
            }
            tParam.setCellData(cs);
            tParam.getDataMappingParams().setInData(field, cs);
            tParam.getDataMappingParams().setInDataSchemas(field.getSchema(), cs.getSchema());
            tParam.setMappingInherited(true);
            tParam.setRenderingInherited(true);
            tmpChildrenParams.add(i, tParam);
        }
        childrenParams = tmpChildrenParams;
        if (ui != null) {
            SwingInstancer.swingRunAndWait(new Runnable()
            {
                @Override
                public void run()
                {
                    ui.getRenderingGUI().setFieldType(FieldType.FIELD_IRREGULAR, 3);
                    ui.setPresentationParams(PresentationParams.this);
                    PresentationParams.this.active = oldActive;
                    PresentationParams.this.renderingParams.setActive(active);
                    PresentationParams.this.dataMappingParams.setActive(active);
                }
            });
        } else {
            active = oldActive;
            renderingParams.setActive(active);
            dataMappingParams.setActive(active);
        }
    }

    public void setInField(PointField field)
    {
        renderingParams.setInherited(false);
        dataMappingParams.setInherited(false);
        renderingInherited = mappingInherited = false;
        childrenParams.clear();
        setName(field.getName());
        boolean oldActive = active;
        renderingParams.setActive(false);
        dataMappingParams.setActive(false);
        dataMappingParams.getTransparencyParams().getComponentRange().addNull(true).prefereNull(dim != 3 || field.hasCoords());
        dataMappingParams.setInData(field, (DataContainerSchema) null);
        renderingParams.setObjectCharacteristics(field);
        if (ui != null) {
            final PresentationParams thisParams = this;
            SwingInstancer.swingRunAndWait(new Runnable()
            {
                @Override
                public void run()
                {
                    ui.getRenderingGUI().setFieldType(FieldType.FIELD_POINT, 0);
                    ui.setPresentationParams(thisParams);
                    ui.updateParams();
                }
            });
        }
        active = oldActive;
        renderingParams.setActive(active);
        dataMappingParams.setActive(active);
    }

    public void setInField(Field field)
    {
        if (field instanceof IrregularField)
            setInField((IrregularField)field);
        if (field instanceof RegularField)
            setInField((RegularField)field);
        if (field instanceof PointField)
            setInField((PointField)field);
    }

    public DataContainer getCellData()
    {
        return dataMappingParams.cellData;
    }

    public void setCellData(DataContainer cellData)
    {
        dataMappingParams.setCellData(cellData);
    }

    public DataContainerSchema getCellDataSchema()
    {
        return dataMappingParams.cellDataSchema;
    }

    public void updateFieldSchema(RegularFieldSchema fieldSchema, boolean updateNDim)
    {
        if (fieldSchema == null)
            return;
        if (updateNDim)
            dim = fieldSchema.getNDims();
    }

    /**
     * Get the value of content3DParams
     *
     * @return the value of content3DParams
     */
    public RegularField3DParams getContent3DParams()
    {
        return content3DParams;
    }

    public int getDim()
    {
        return dim;
    }

    public PresentationParams getParentParams()
    {
        return parentParams;
    }

    public void setParentParams(PresentationParams parentParams)
    {
        this.parentParams = parentParams;
        mappingInherited = renderingInherited = transformInherited = true;
    }

    public boolean isMappingInherited()
    {
        return mappingInherited;
    }

    public void setMappingInherited(boolean mappingInherited)
    {
        this.mappingInherited = mappingInherited;
        if (ui != null)
            ui.setInheritMapBoxSelection(mappingInherited);
    }

    public boolean isRenderingInherited()
    {
        return renderingInherited;
    }

    public void setRenderingInherited(boolean renderingInherited)
    {
        this.renderingInherited = renderingInherited;
        if (ui != null)
            ui.setInheritRenderingBoxSelection(renderingInherited);
    }

    public boolean isTransformInherited()
    {
        return transformInherited;
    }

    public void setTransformInherited(boolean transformInherited)
    {
        this.transformInherited = transformInherited;
        transformParams.resetTransform();
    }

    public ArrayList<PresentationParams> getChildrenParams()
    {
        return childrenParams;
    }

    public PresentationParams getChild(int index)
    {
        if (index < 0 || index >= childrenParams.size())
            return null;
        return childrenParams.get(index);
    }

    public void addChildParameters(PresentationParams childParams)
    {
        childrenParams.add(childParams);
        dataMappingParams.addRenderEventListener(childParams.getDataMappingParams().getParentChangeListener());
        renderingParams.addRenderEventListener(childParams.getRenderingParams().getParentChangeListener());
        childParams.setParentParams(this);
    }

    public void removeChildParameters(PresentationParams childParams)
    {
        childParams.setParentParams(null);
        dataMappingParams.removeRenderEventListener(childParams.getDataMappingParams().getParentChangeListener());
        renderingParams.removeRenderEventListener(childParams.getRenderingParams().getParentChangeListener());
        childrenParams.remove(childParams);
    }

    public void clearChildParams()
    {
        childrenParams.clear();
    }

    protected ChangeListener mappingEventListener = new ChangeListener()
    {
        @Override
        public void stateChanged(ChangeEvent e)
        {
            for (PresentationParams childParam : childrenParams)
                if (childParam.mappingInherited) {
                    childParam.getDataMappingParams().copyValuesFrom(dataMappingParams);
                }
        }
    };

    protected ChangeListener renderingEventListener = new ChangeListener()
    {
        @Override
        public void stateChanged(ChangeEvent e)
        {
            for (PresentationParams childParam : childrenParams)
                if (childParam.renderingInherited)
                    childParam.getRenderingParams().copyValuesFrom(renderingParams);
        }
    };

    protected ChangeListener transformEventListener = new ChangeListener()
    {
        @Override
        public void stateChanged(ChangeEvent e)
        {
            for (PresentationParams childParam : childrenParams)
                if (childParam.transformInherited)
                    childParam.getTransformParams().resetTransform();
        }
    };

    public ChangeListener getMappingEventListener()
    {
        return mappingEventListener;
    }

    public ChangeListener getRenderingEventListener()
    {
        return renderingEventListener;
    }

    public ChangeListener getTransformEventListener()
    {
        return transformEventListener;
    }
    int stackDepth = 0;

    public void spreadMappingParams()
    {
        if (childrenParams == null || childrenParams.isEmpty())
            return;
        stackDepth += 1;
        if (childrenParams.size() > 1 && stackDepth > 2) {
            LOGGER.debug("stack overflow imminent");
        }
        for (PresentationParams childParam : childrenParams)
            if (childParam.mappingInherited)
                childParam.getDataMappingParams().copyValuesFrom(dataMappingParams);
        stackDepth -= 1;
    }

    public void spreadRenderingParams()
    {
        if (childrenParams == null || childrenParams.isEmpty())
            return;
        for (PresentationParams childParam : childrenParams)
            if (childParam.renderingInherited)
                childParam.renderingParams.copyValuesFrom(renderingParams);
    }

    public void spreadTransform()
    {
        for (PresentationParams childParam : childrenParams)
            if (childParam.transformInherited)
                childParam.getTransformParams().resetTransform();
    }

    public int getSelectByComponent()
    {
        return selectByComponent;
    }

    public void setSelectByComponent(int selectByComponent)
    {
        this.selectByComponent = selectByComponent;
    }

    public float getSelectOver()
    {
        return selectOver;
    }

    public void setSelectOver(float selectOver)
    {
        this.selectOver = selectOver;
    }

    public float getSelectUnder()
    {
        return selectUnder;
    }

    public void setSelectUnder(float selectUnder)
    {
        this.selectUnder = selectUnder;
    }

    public boolean isSelectionActive()
    {
        return selectionActive;
    }

    public void setSelectionActive(boolean selectionActive)
    {
        this.selectionActive = selectionActive;
    }

    public int getPickIndicator()
    {
        return pickIndicator;
    }

    public void setPickIndicator(int pickIndicator)
    {
        this.pickIndicator = pickIndicator;
        for (PresentationParams childParam : childrenParams)
            childParam.setPickIndicator(pickIndicator);
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
        dataMappingParams.setActive(active);
        renderingParams.setActive(active);
    }

    public String[] valuesToStringArray()
    {
        ArrayList<String> res = new ArrayList<>();
        res.add("presentation params {");
        if (childrenParams != null && !childrenParams.isEmpty()) {
            res.add("children params {");
            for (PresentationParams childParams : childrenParams) {
                String[] cp = childParams.valuesToStringArray();
                for (String p : cp)
                    res.add("    " + p);
            }
            res.add("}");
        }
        res.add("data mapping inherited: " + mappingInherited);
        res.add("rendering inherited: " + renderingInherited);
        if (dataMappingParams.getNodeData() != null &&
                dataMappingParams.getNodeData() instanceof RegularField) {
            res.add("regular field 3d rendering params {");
            String[] c3dp = content3DParams.valuesToStringArray();
            for (String p : c3dp)
                res.add("    " + p);
            res.add("}");
        }
        if (!renderingInherited) {
            res.add("rendering params {");
            String[] rp = renderingParams.valuesToStringArray();
            for (String p : rp)
                res.add("    " + p);
            res.add("}");
        }
        if (!mappingInherited) {
            res.add("data mapping params {");
            String[] dmp = dataMappingParams.valuesToStringArray();
            for (String p : dmp)
                res.add("    " + p);
            res.add("}");
        }
        String[] r = new String[res.size()];
        for (int i = 0; i < r.length; i++)
            r[i] = res.get(i);
        return r;
    }

    public void restoreValuesFrom(String[] saved)
    {
        LOGGER.debug("restoring presentationParams");
        setActive(false);
        itemLoop:
        for (int i = 0; i < saved.length; i++) {
            if (saved[i].trim().startsWith("data mapping inherited: ")) {
                dataMappingParams.setInherited(saved[i].endsWith("true"));
                continue;
            }
            if (saved[i].trim().startsWith("rendering inherited: ")) {
                renderingParams.setInherited(saved[i].endsWith("true"));
                continue;
            }
            if (saved[i].trim().equals("data mapping params {")) {
                String[] currentBlock = StringUtils.findBlock(saved, i);
                dataMappingParams.restoreValuesFrom(currentBlock);
                i += currentBlock.length + 1;
                continue;
            }
            if (saved[i].trim().equals("rendering params {")) {
                String[] currentBlock = StringUtils.findBlock(saved, i);
                renderingParams.restoreValuesFrom(currentBlock);
                i += currentBlock.length + 1;
                continue;
            }
            if (saved[i].trim().equals("regular field 3d rendering params {")) {
                String[] currentBlock = StringUtils.findBlock(saved, i);
                content3DParams.restoreValuesFrom(currentBlock);
                i += currentBlock.length + 1;
                continue;
            }
        }
        active = true;

    }

    private static final String VNA_PARAM_SEPARATOR = ";";
    private static final String VNA_PARAM_ESCAPE = "~";

    /**
     * Serializes parameters into one string. Typically it will be used to write into VNA.
     * Parameters will be separated by {@link #VNA_PARAM_SEPARATOR} and escaped with {@link #VNA_PARAM_ESCAPE}.
     */
    public String valuesToString()
    {
        String[] paramStrings = valuesToStringArray();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < paramStrings.length; i++) {
            String param = paramStrings[i];
            param = param.replace(VNA_PARAM_ESCAPE, VNA_PARAM_ESCAPE + VNA_PARAM_ESCAPE); //escape escape strings
            param = param.replace(VNA_PARAM_SEPARATOR, VNA_PARAM_ESCAPE + VNA_PARAM_SEPARATOR); //escape separator like sequences
            builder.append(param);
            if (i < paramStrings.length - 1) builder.append(VNA_PARAM_SEPARATOR);
        }
        return builder.toString();
    }

    /**
     * Deserializes and restores presentation parameters from string (typically from VNA).
     * Parameters should be separated by {@link #VNA_PARAM_SEPARATOR} and escaped with {@link #VNA_PARAM_ESCAPE}.
     * <p>
     * @param parameterString string to decode into parameters (typically read from vna)
     */
    public void restorePassivelyValuesFrom(String parameterString)
    {
        boolean pushActive = active;
        active = false;
        //find new temporary escape character
        String tmpEscapeChar = "@";
        String tmpEscape = "@";
        while (parameterString.contains(tmpEscape))
            tmpEscape += tmpEscapeChar;

        //replace escapes by temporary escape
        parameterString = parameterString.replaceAll(VNA_PARAM_ESCAPE + VNA_PARAM_ESCAPE, tmpEscape);

        //split by param separator (ma be preceded by escape characters but only even number of them)
        String[] parameterStrings = parameterString.split("(?<!" + VNA_PARAM_ESCAPE + ")" + VNA_PARAM_SEPARATOR, -1);
        for (int i = 0; i < parameterStrings.length; i++) {
            parameterStrings[i] = parameterStrings[i].replace(VNA_PARAM_ESCAPE + VNA_PARAM_SEPARATOR, VNA_PARAM_SEPARATOR); //unescape separator like sequences
            parameterStrings[i] = parameterStrings[i].replace(tmpEscape, VNA_PARAM_ESCAPE); //unescape escape characters
        }

        restoreValuesFrom(parameterStrings);
        active = pushActive;
    }

}
