/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.parameters;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterName;

/**
 *
 * @author norkap
 */


public class FontParamsShared {
    
    public static final ParameterName<Boolean> FONT_PARAMS_THREE_DIMENSIONAL = new ParameterName("three dimensional");
    
    // % from 0.1 to 100
    public static final ParameterName<Float> FONT_PARAMS_GLYPHS_SIZE = new ParameterName("glyphs size");
    
    public static final ParameterName<Float> FONT_PARAMS_PRECISION = new ParameterName("precision");
    
    public static final ParameterName<String> FONT_PARAMS_FONT_NAME = new ParameterName("font name");
    
    public static final ParameterName<Integer> FONT_PARAMS_FONT_TYPE = new ParameterName("font type");
    
    public static final ParameterName<Integer> FONT_PARAMS_COLOR = new ParameterName("color");
    
    public static final ParameterName<Integer> FONT_PARAMS_BRIGHTNESS = new ParameterName("brightness");
    
    public static List<Parameter> createDefaultParametersAsList()
    {
        
        List<Parameter> l = new ArrayList<>();
        l.add(new Parameter<>(FONT_PARAMS_THREE_DIMENSIONAL, false));
        l.add(new Parameter<>(FONT_PARAMS_GLYPHS_SIZE, .012f));
        l.add(new Parameter<>(FONT_PARAMS_PRECISION, 3.0f));
        l.add(new Parameter<>(FONT_PARAMS_FONT_NAME, "sans-serif"));
        l.add(new Parameter<>(FONT_PARAMS_FONT_TYPE, Font.PLAIN));
        l.add(new Parameter<>(FONT_PARAMS_COLOR, Integer.MAX_VALUE));
        l.add(new Parameter<>(FONT_PARAMS_BRIGHTNESS, 50));
        
        return l;
    }
}
