/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.utils;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.dataarrays.ComplexDataArray;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.geometries.parameters.ColorComponentParams;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jlargearrays.LargeArray;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class TextureMapper
{

    public static float[] map(DataContainer dataContainer, ColorComponentParams params, int start, int end, float[] uv, int iuv, float delta)
    {
        DataArray data = dataContainer.getComponent(params.getColorRange().getComponentName());
        double v;
        float t;
        if (start < 0 || start >= end || end > dataContainer.getNElements()) {
            return null;
        }
        if (uv == null || uv.length != 2 * (end - start)) {
            uv = new float[2 * (end - start)];
            Arrays.fill(uv, 0);
        }
        boolean wrap = params.isWrap();
        float low = params.getDataMin();
        float d = 1 / (params.getDataMax() - low);
        if (data != null) {
            int vl = data.getVectorLength();
            switch (data.getType()) {
                case FIELD_DATA_BYTE:
                    byte[] bData = (byte[]) data.getRawArray().getData();
                    for (int i = start, l = iuv; i < end; i++, l += 2) {
                        if (vl == 1) {
                            v = 0xff & bData[i];
                        } else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++) {
                                v += (0xff & bData[k]) * (0xff & bData[k]);
                            }
                            v = sqrt(v);
                        }
                        t = (float) (d * (v - low));
                        if (wrap) {
                            t = (t + 1000) - (int) (t + 1000);
                        } else {
                            if (t < delta) {
                                t = delta;
                            }
                            if (t > 1 - delta) {
                                t = 1 - delta;
                            }
                        }
                        uv[l] = t;
                    }
                    break;
                case FIELD_DATA_LOGIC:
                case FIELD_DATA_SHORT:
                case FIELD_DATA_INT:
                case FIELD_DATA_FLOAT:
                case FIELD_DATA_DOUBLE:
                    LargeArray dData = data.getRawArray();
                    for (int i = start, l = iuv; i < end; i++, l += 2) {
                        if (vl == 1) {
                            v = dData.getDouble(i);
                        } else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++) {
                                v += dData.getDouble(k) * dData.getDouble(k);
                            }
                            v = sqrt(v);
                        }
                        t = (float) (d * (v - low));
                        if (wrap) {
                            t = (t + 1000) - (int) (t + 1000);
                        } else {
                            if (t < delta) {
                                t = delta;
                            }
                            if (t > 1 - delta) {
                                t = 1 - delta;
                            }
                        }
                        uv[l] = t;
                    }
                    break;
                case FIELD_DATA_COMPLEX:
                    float[] reData = ((ComplexDataArray) data).getFloatRealArray().getData();
                    float[] imData = ((ComplexDataArray) data).getFloatImaginaryArray().getData();
                    float v1,
                     v2;
                    for (int i = start, l = iuv; i < end; i++, l += 2) {
                        if (vl == 1) {
                            v1 = reData[i];
                            v2 = imData[i];
                            v = (float) sqrt(v1 * v1 + v2 * v2);
                        } else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++) {
                                v1 = reData[k];
                                v2 = imData[k];
                                v += sqrt(v1 * v2 + v1 * v2);
                            }
                            v = sqrt(v);
                        }
                        t = (float) (d * (v - low));
                        if (wrap) {
                            t = (t + 1000) - (int) (t + 1000);
                        } else {
                            if (t < delta) {
                                t = delta;
                            }
                            if (t > 1 - delta) {
                                t = 1 - delta;
                            }
                        }
                        uv[l] = t;
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported array type");

            }
        } else { // time for pseudocomponents
            if (dataContainer instanceof Field && params.getColorRange().getComponentName().startsWith("__coord")) {
                Field fld = (Field) dataContainer;
                int coord = 0;
                if (params.getColorRange().getComponentName().endsWith("y"))
                    coord = 1;
                else if (params.getColorRange().getComponentName().endsWith("z"))
                    coord = 2;
                float coordMin = params.getColorRange().getLow();
                float coordMax = params.getColorRange().getUp();
                float dx = 1 / (coordMax - coordMin + .0001f);
                if (fld.getCurrentCoords() != null) {
                    float[] coords = fld.getCurrentCoords().getData();
                    for (int i = 3 * start + coord, l = iuv; i < 3 * end + coord; i += 3, l += 2)
                        uv[l] = dx * (coords[i] - coordMin);
                } else if (fld instanceof RegularField) {
                    RegularField rFld = (RegularField) fld;
                    int[] dims = rFld.getDims();
                    float[][] affine = rFld.getAffine();
                    switch (rFld.getDimNum()) {
                        case 1:
                            for (int i = start, l = iuv; i < end; i++, l += 2)
                                uv[l] = dx * (affine[3][coord] + i * affine[0][coord] - coordMin);
                            break;
                        case 2:
                            for (int i = start, l = iuv; i < end; i++, l += 2) {
                                int i0 = i % dims[0];
                                int i1 = i / dims[0];
                                uv[l] = dx * (affine[3][coord] + i0 * affine[0][coord] + i1 * affine[1][coord] - coordMin);
                            }
                            break;
                        case 3:
                            for (int i = start, l = iuv; i < end; i++, l += 2) {
                                int i0 = i % dims[0];
                                int i1 = (i / dims[0]) % dims[1];
                                int i2 = i / (dims[0] * dims[1]);
                                uv[l] = dx * (affine[3][coord] + i0 * affine[0][coord] + i1 * affine[1][coord] + i2 * affine[2][coord] - coordMin);
                            }
                            break;
                    }
                }
            } else if (dataContainer instanceof RegularField && params.getColorRange().getComponentName().startsWith("__index")) {

                RegularField rFld = (RegularField) dataContainer;
                int[] dims = rFld.getDims();
                int coord = 0;
                if (params.getColorRange().getComponentName().endsWith("j"))
                    coord = 1;
                else if (params.getColorRange().getComponentName().endsWith("k"))
                    coord = 2;
                float coordMin = params.getColorRange().getLow();
                float coordMax = params.getColorRange().getUp();
                float dx = 1 / (coordMax - coordMin + .0001f);
                switch (coord) {
                    case 0:
                        for (int i = start, l = iuv; i < end; i++, l += 2)
                            uv[l] = dx * (i % dims[0] - coordMin);
                        break;
                    case 1:
                        for (int i = start, l = iuv; i < end; i++, l += 2) {
                            uv[l] = dx * ((i / dims[0]) % dims[1] - coordMin);
                        }
                        break;
                    case 3:
                        for (int i = start, l = iuv; i < end; i++, l += 2) {
                            int k = dims[0] * dims[1];
                            uv[l] = dx * (i / k - coordMin);
                        }
                        break;
                }
            }
        }
        return uv;
    }

    public static float[] map(DataContainer data, ColorComponentParams params, float[] uv, int iuv)
    {
        return map(data, params, 0, (int) data.getNElements(), uv, iuv, .005f);
    }

    public static int[] map(DataContainer inField, DataMappingParams params, int[] textureImageData, int start, int end, int[] texture)
    {
        if (texture == null || texture.length != end - start) {
            texture = new int[end - start];
        }
        for (int i = 0; i < texture.length; i++) {
            texture[i] = 0;
        }
        DataArray uData = inField.getComponent(params.getUParams().getDataComponentIndex());
        DataArray vData = inField.getComponent(params.getVParams().getDataComponentIndex());
        BufferedImage textureImage = params.getTextureImage();
        if (uData == null || vData == null || textureImage == null) {
            return texture;
        }
        double v;
        int t;
        int dim = textureImage.getWidth() - 1;
        float low = params.getUParams().getDataMin();
        float d = dim / (params.getUParams().getDataMax() - low);
        int vl = uData.getVectorLength();
        switch (uData.getType()) {
            case FIELD_DATA_BYTE:
                byte[] bData = (byte[]) uData.getRawArray().getData();
                for (int i = start, l = 0; i < end; i++, l++) {
                    if (vl == 1) {
                        v = 0xff & bData[i];
                    } else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++) {
                            v += (0xff & bData[k]) * (0xff & bData[k]);
                        }
                        v = sqrt(v);
                    }
                    t = (int) (d * (v - low));
                    if (t < 0) {
                        t = 0;
                    }
                    if (t >= dim) {
                        t = dim - 1;
                    }
                    texture[l] = t;
                }
                break;
            case FIELD_DATA_LOGIC:
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
                LargeArray dData = uData.getRawArray();
                for (int i = start, l = 0; i < end; i++, l++) {
                    if (vl == 1) {
                        v = dData.getDouble(i);
                    } else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++) {
                            v += dData.getDouble(k) * dData.getDouble(k);
                        }
                        v = sqrt(v);
                    }
                    t = (int) (d * (v - low));
                    if (t < 0) {
                        t = 0;
                    }
                    if (t >= dim) {
                        t = dim - 1;
                    }
                    texture[l] = t;
                }
                break;
            case FIELD_DATA_COMPLEX:
                float[] reData = ((ComplexDataArray) uData).getFloatRealArray().getFloatData();
                float[] imData = ((ComplexDataArray) uData).getFloatImaginaryArray().getFloatData();
                float v1,
                 v2;
                for (int i = start, l = 0; i < end; i++, l++) {
                    if (vl == 1) {
                        v1 = reData[i];
                        v2 = imData[i];
                        v = (float) sqrt(v1 * v1 + v2 * v2);
                    } else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++) {
                            v1 = reData[k];
                            v2 = imData[k];
                            v += sqrt(v1 * v2 + v1 * v2);
                        }
                        v = sqrt(v);
                    }
                    t = (int) (d * (v - low));
                    if (t < 0) {
                        t = 0;
                    }
                    if (t >= dim) {
                        t = dim - 1;
                    }
                    texture[l] = t;
                }
                break;
            default:
                throw new IllegalArgumentException("Unsupported array type");

        }
        int width = params.getTextureImage().getWidth();
        dim = params.getTextureImage().getHeight() - 1;
        low = params.getVParams().getDataMin();
        d = dim / (params.getVParams().getDataMax() - low);
        vl = vData.getVectorLength();
        switch (vData.getType()) {
            case FIELD_DATA_BYTE:
                byte[] bData = (byte[]) vData.getRawArray().getData();
                for (int i = start, l = 0; i < end; i++, l++) {
                    if (vl == 1) {
                        v = 0xff & bData[i];
                    } else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++) {
                            v += (0xff & bData[k]) * (0xff & bData[k]);
                        }
                        v = sqrt(v);
                    }
                    t = (int) (d * (v - low));
                    if (t < 0) {
                        t = 0;
                    }
                    if (t >= dim) {
                        t = dim - 1;
                    }
                    texture[l] = textureImageData[t * width + texture[l]];
                }
                break;
            case FIELD_DATA_LOGIC:
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
                LargeArray dData = vData.getRawArray();
                for (int i = start, l = 0; i < end; i++, l++) {
                    if (vl == 1) {
                        v = dData.getDouble(i);
                    } else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++) {
                            v += dData.getDouble(k) * dData.getDouble(k);
                        }
                        v = sqrt(v);
                    }
                    t = (int) (d * (v - low));
                    if (t < 0) {
                        t = 0;
                    }
                    if (t >= dim) {
                        t = dim - 1;
                    }
                    texture[l] = textureImageData[t * width + texture[l]];
                }
                break;
            case FIELD_DATA_COMPLEX:
                float[] reData = ((ComplexDataArray) uData).getFloatRealArray().getFloatData();
                float[] imData = ((ComplexDataArray) uData).getFloatImaginaryArray().getFloatData();
                float v1,
                 v2;
                for (int i = start, l = 0; i < end; i++, l++) {
                    if (vl == 1) {
                        v1 = reData[i];
                        v2 = imData[i];
                        v = (float) sqrt(v1 * v1 + v2 * v2);
                    } else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++) {
                            v1 = reData[k];
                            v2 = imData[k];
                            v += sqrt(v1 * v2 + v1 * v2);
                        }
                        v = sqrt(v);
                    }
                    t = (int) (d * (v - low));
                    if (t < 0) {
                        t = 0;
                    }
                    if (t >= dim) {
                        t = dim - 1;
                    }
                    texture[l] = textureImageData[t * width + texture[l]];
                }
                break;
            default:
                throw new IllegalArgumentException("Unsupported array type");

        }
        return texture;
    }

    private TextureMapper()
    {
    }

}
