/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.textUtils;

import java.awt.*;
import org.jogamp.java3d.J3DGraphics2D;
import org.visnow.vn.geometries.parameters.FontParams;
import org.visnow.vn.geometries.parameters.FontParams.Position;
import static org.visnow.vn.geometries.parameters.FontParams.Position.*;
import static org.visnow.vn.geometries.parameters.FontParams.Decoration.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class CXYZString
{

    protected String[] text;
    protected int fontSize = 12;
    protected Font font = new Font("Dialog", Font.PLAIN, fontSize);
    protected float[] coords = new float[3];
    protected float[] sCoords = new float[3];
    protected float relativeHeight = .01f;

    /**
     * Creates a new instance of CXYZString
     */
    public CXYZString(String[] s, Color c, float[] coords, Font font, float relativeHeight)
    {
        this.coords = coords;
        this.font = font;
        this.relativeHeight = relativeHeight;
        this.text = s;
    }

    public CXYZString(String[] s, float[] coords, FontParams params)
    {
        this(s, params.getColor(), coords, params.getFont2D(), params.getSize());
    }

    public void update(org.visnow.vn.geometries.utils.transform.LocalToWindow ltw)
    {
        if (ltw == null)
            return;
        float depth = ltw.transformPt(coords, sCoords);
        sCoords[2] = 1 -Math.max(0, depth);
//        System.out.printf("%6.3f%n", depth);
    }

    @Override
    public String toString()
    {
        StringBuilder b = new StringBuilder();
        if (text.length > 1) b.append("[");
        for (int i = 0; i < text.length; i++) 
            b.append(text[i] + (i < text.length - 1 ? "," : text.length < 2 ? "" : "]" ));
        return text + " at(" + sCoords[0] + "," + sCoords[1] + ")";
    }

    public String[] getString()
    {
        return text;
    }

    public float[] getSCoords()
    {
        return sCoords;
    }

    
    
    /**
     *
     * @param vGraphics  current graphics context for the drawing operation (from 3D canvas)
     * @param params     parameters specifying font family, size, color etc., relative position of the drawing w/r to the anchor point, decoration etc.
     * @param fm         font metrics necessary for estimation of the drawinng image size
     * @param width      width of the rendering window
     * @param height     height of the rendering window
     * @param color      overrides color from current params
     */
    public void draw(J3DGraphics2D vGraphics, FontParams params, FontMetrics fm, int width, int height, Color color)
    {
        vGraphics.setFont(params.getFont2D());
        if (text == null || text.length == 0 || 
            text.length == 1 && (text[0] == null || text[0].isEmpty()))
            return;
        String[][]texts = new String[text.length][];
        int effectiveTextsLength = 0;
        for (int i = 0; i < texts.length; i++) {
            texts[i] = text[i].split("\\n");
            for (String txt : texts[i])
                if (!txt.isEmpty())  
                    effectiveTextsLength += 1;
        }
        String[] effectiveTexts = new String[effectiveTextsLength];
        for (int i = 0, k = 0; i < texts.length; i++)
            for (String text1 : texts[i]) 
                if (!text1.isEmpty()) {
                    effectiveTexts[k] = text1;
                    k += 1;
                }
        float depth = 1 - sCoords[2] / 2;
        if (depth <= .01)
            return;
        int textW = 0;
        for (int i = 0; i < effectiveTexts.length; i++) {
            int k = fm.stringWidth(effectiveTexts[i]);
            if (k > textW)
                textW = k;
        }
        int m = (int)(.3 * params.getFontSize()) + 1;
        textW += 2 * m;
        int textH = effectiveTexts.length * (params.getFontSize() + 1) + 2 * m - 1;
        float[] fc = new float[3];
        float[] bc = new float[3];
        if (color == null)
            params.getColor().getRGBColorComponents(fc);
        else
            color.getRGBColorComponents(fc);
        params.getBgColor().getRGBColorComponents(bc);
        int anchorX = (int)sCoords[0], anchorY = (int)sCoords[1];
        int glyphX, glyphY;
        int stemEndX, stemEndY;
        float shift = params.getShift();
        Position effectivePosition = params.getPosition();
        if (params.getPosition() == RADIAL) {
            effectivePosition = Position.getPosition(anchorX - width / 2,  height / 2 - anchorY);
            stemEndX = (int)(width  / 2 + (anchorX - width / 2) *  (1. + shift / 10));
            stemEndY = height - (int)(height / 2 - (anchorY - height / 2) * (1. + shift / 10));
        }
        else {
            stemEndX = (int)(anchorX + effectivePosition.getxShift() * shift * params.getFontSize());
            stemEndY = (int)(anchorY - effectivePosition.getyShift() * shift * params.getFontSize());
        }
        int glyphShiftX = params.getDecoration() == FRAME ? effectivePosition.getxFrameShift() - 1 : effectivePosition.getxEdgeShift() - 1;
        int glyphShiftY = params.getDecoration() == FRAME ? effectivePosition.getyFrameShift() + 1 : effectivePosition.getyEdgeShift() + 1;
        glyphX = stemEndX + textW * glyphShiftX / 2;
        glyphY = stemEndY + textH * glyphShiftY/ 2;
        vGraphics.setColor(new Color(bc[0], bc[1], bc[2], params.getOpacity()));
        vGraphics.fillRect(glyphX, glyphY - textH, textW, textH);
        vGraphics.setColor(new Color(depth * fc[0] + (1 - depth) * bc[0], 
                                     depth * fc[1] + (1 - depth) * bc[1], 
                                     depth * fc[2] + (1 - depth) * bc[2]));
        switch (params.getDecoration()) {
        case FRAME:
            vGraphics.drawRect(glyphX, glyphY - textH, textW, textH);
            break;
        case EDGE:
            vGraphics.drawLine(glyphX, glyphY, glyphX + textW, glyphY);
            vGraphics.drawLine(glyphX, glyphY, glyphX,         glyphY - textH);
            break;
        default:
        }
        if(shift != 0.0f)
            vGraphics.drawLine(anchorX, anchorY, stemEndX, stemEndY);
        
        for (int i = 0; i < effectiveTexts.length; i++) 
            vGraphics.drawString(effectiveTexts[i], glyphX + m, glyphY - m - (params.getFontSize() + 1) * (effectiveTexts.length - 1 - i));
    }
    
    
    /**
     *
     * @param vGraphics  current graphics context for the drawing operation (from 3D canvas)
     * @param params     parameters specifying font family, size, color etc., relative position of the drawing w/r to the anchor point, decoration etc.
     * @param fm         font metrics necessary for estimation of the drawinng image size
     * @param width      width of the rendering window
     * @param height     height of the rendering window
     */
    public void draw(J3DGraphics2D vGraphics, FontParams params, FontMetrics fm, int width, int height)
    {
        draw(vGraphics, params, fm, width, height, null);
    }
    
}
