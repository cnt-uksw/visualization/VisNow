/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.textUtils;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import org.jogamp.java3d.J3DGraphics2D;
import org.visnow.vn.geometries.parameters.FontParams;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import org.visnow.vn.lib.utils.sort.IndirectComparator;
import static org.visnow.vn.lib.utils.sort.IndirectSort.indirectSort;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Texts2D
{

    protected int[] indices;
    protected CXYZString[] glyphs  = null;
    protected FontParams fontParams;
    protected float relativeHeight = .01f;
    protected String[][] texts     = null;
    protected float[] textCoords   = null;

    public Texts2D(float[] textCoords, String[][] texts, FontParams fontParams)
    {
        this.textCoords = textCoords;
        this.texts      = texts;
        this.fontParams = fontParams;
        int nGlyphs = texts.length;
        Color textColor2d = fontParams.getColor();
        Font font = fontParams.getFont2D();
        glyphs = new CXYZString[nGlyphs];
        for (int i = 0; i < nGlyphs; i++) {
            float[] coords = new float[3];
            System.arraycopy(textCoords, 3 * i, coords, 0, 3);
            glyphs[i] = new CXYZString(texts[i], textColor2d, coords, font, fontParams.getSize());
        }
    }
    
    public void clear()
    {
        glyphs = new CXYZString[0];
    }
    
    public void update(LocalToWindow ltw)
    {
        if (ltw == null)
            return;
        indices = new int[glyphs.length];
        for (int i = 0; i < glyphs.length; i++) 
            glyphs[i].update(ltw);
        IndirectComparator cmp = new IndirectComparator()
        {
            @Override
            public int compare(int i, int j)
            {
                float di = glyphs[i].getSCoords()[2];
                float dj = glyphs[j].getSCoords()[2];
                return (di < dj ? -1 : di == dj ? 0 : 1);
            }
        };
        indirectSort(indices, cmp, true);
    } 
    
    public void draw(J3DGraphics2D vGraphics, LocalToWindow ltw, int width, int height, Color color)
    {
        if (glyphs == null || glyphs.length < 1)
            return;
        update(ltw);
        Font f = vGraphics.getFont();
        Font ft = fontParams.getFont2D();
        vGraphics.setFont(ft);
        FontMetrics fm = vGraphics.getFontMetrics();
        for (int i = 0; i < indices.length; i++) {
            int k = indices[i];
            glyphs[k].draw(vGraphics, fontParams, fm, width, height, color);
        }
        vGraphics.setFont(f);
    }
    
    public void draw(J3DGraphics2D vGraphics, LocalToWindow ltw, int width, int height)
    {
        draw(vGraphics, ltw, width, height, null);
    }

    public String[][] getTexts() {
        return texts;
    }

    public float[] getTextCoords() {
        return textCoords;
    }
    
}
