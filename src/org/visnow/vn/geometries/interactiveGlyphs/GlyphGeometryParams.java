/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.interactiveGlyphs;


/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */

/**
 * container of immutable copies of interactive glyph geometry params passed to modules
 * using glyph geometry
 */
public class GlyphGeometryParams
{
    private final float[] center;  // rotation center of the glyph
    
    private final float[] u;  // current u reper versor
    private final float   shiftU;          // current center shift in the u direction
    private final float[] uMinMax;     // extreme values of the u coordinate of the field nodes
    private final float[] uRange;    // u range of the selected box
    
    private final float[] v; 
    private final float   shiftV;
    private final float[] vMinMax;
    private final float[] vRange;
    
    private final float[] w;
    private final float   shiftW;
    private final float[] wMinMax;
    private final float[] wRange;
    
    private final float   radius;
    private final float   glyphScale;

    public GlyphGeometryParams(float[] center, 
                               float[] u, float shiftU, float[] uMinMax, float[] uRange, 
                               float[] v, float shiftV, float[] vMinMax, float[] vRange, 
                               float[] w, float shiftW, float[] wMinMax, float[] wRange, 
                               float radius, float glyphScale)
    {
        this.center = center;
        this.u = u;
        this.shiftU = shiftU;
        this.uMinMax = uMinMax;
        this.uRange = uRange;
        this.v = v;
        this.shiftV = shiftV;
        this.vMinMax = vMinMax;
        this.vRange = vRange;
        this.w = w;
        this.shiftW = shiftW;
        this.wMinMax = wMinMax;
        this.wRange = wRange;
        this.radius = radius;
        this.glyphScale = glyphScale;
    }

    public float[] getCenter()
    {
        return center;
    }

    public float[] getU()
    {
        return u;
    }

    public float getShiftU()
    {
        return shiftU;
    }

    public float[] getuMinMax()
    {
        return uMinMax;
    }

    public float[] getuRange()
    {
        return uRange;
    }

    public float[] getV()
    {
        return v;
    }

    public float getShiftV()
    {
        return shiftV;
    }

    public float[] getvMinMax()
    {
        return vMinMax;
    }

    public float[] getvRange()
    {
        return vRange;
    }

    public float[] getW()
    {
        return w;
    }

    public float getShiftW()
    {
        return shiftW;
    }

    public float[] getwMinMax()
    {
        return wMinMax;
    }

    public float[] getwRange()
    {
        return wRange;
    }

    public float getRadius()
    {
        return radius;
    }

    public float getGlyphScale()
    {
        return glyphScale;
    }
    
    
}
