/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.interactiveGlyphs;

import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineArray;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.BOX;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams.*;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class BoxGlyph extends Glyph
{

    public BoxGlyph(InteractiveGlyphParams params)
    {
        super(params);
        setName("box glyph");
        type = BOX;
        glyphVerts = new float[]{-1, -1, -1,   -1, -1,  1,
                                  1, -1, -1,    1, -1,  1,
                                  1,  1, -1,    1,  1,  1,
                                 -1,  1, -1,   -1,  1,  1,
                                 -1, -1, -1,    1, -1, -1, 
                                 -1,  1, -1,    1,  1, -1,
                                 -1,  1,  1,    1,  1,  1,
                                 -1, -1,  1,    1, -1,  1,
                                 -1, -1, -1,   -1,  1, -1,
                                  1, -1, -1,    1,  1, -1,
                                  1, -1,  1,    1,  1,  1,
                                 -1, -1,  1,   -1,  1,  1};
        visibleWidgets = U_ROT_VIS   | V_ROT_VIS   | W_ROT_VIS |
                         U_TRANS_VIS | V_TRANS_VIS | W_TRANS_VIS |
                         U_RANGE_VIS | V_RANGE_VIS | W_RANGE_VIS |
                         SCALE_VIS   | VOLUME_VIS;
        params.setVisibleWidgets(visibleWidgets);
        lines = new IndexedLineArray(24,  GeometryArray.COORDINATES | GeometryArray.COLOR_3, 24);
        lines.setCoordinateIndices(0, new int[] { 0,  1,  2,  3,  4,  5,  6,  7, 
                                                  8,  9, 10, 11, 12, 13, 14, 15,
                                                 16, 17, 18, 19, 20, 21, 22, 23,});
        lines.setColorIndices(0, new int[]      { 0,  0,  0,  0,  0,  0,  0,  0, 
                                                  1,  1,  1,  1,  1,  1,  1,  1, 
                                                  2,  2,  2,  2,  2,  2,  2,  2});
        lines.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        lines.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        lines.setCapability(GeometryArray.ALLOW_COLOR_READ);
        lines.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
        lines.setCoordinates(0, glyphVerts);
        lines.setColors(0, lineColors);
        lineShape.addGeometry(lines);
        glyphGroup.addChild(lineShape);
        reper = new Reper3D(params);
        addChild(reper);
    }
    
    @Override
    public void update()
    {
        for (int i = 0; i < 3; i++) {
            float c = params.center[i];
            float u0 = params.uRange[0] * params.u[i];
            float u1 = params.uRange[1] * params.u[i];
            float v0 = params.vRange[0] * params.v[i];
            float v1 = params.vRange[1] * params.v[i];
            float w0 = params.wRange[0] * params.w[i];
            float w1 = params.wRange[1] * params.w[i];
            
            glyphVerts[     i] = glyphVerts[24 + i] = glyphVerts[48 + i] = c + u0 + v0 + w0;
            glyphVerts[ 3 + i] = glyphVerts[42 + i] = glyphVerts[66 + i] = c + u0 + v0 + w1;
            
            glyphVerts[ 6 + i] = glyphVerts[27 + i] = glyphVerts[54 + i] = c + u1 + v0 + w0;
            glyphVerts[ 9 + i] = glyphVerts[45 + i] = glyphVerts[60 + i] = c + u1 + v0 + w1;
            
            glyphVerts[12 + i] = glyphVerts[33 + i] = glyphVerts[57 + i] = c + u1 + v1 + w0;
            glyphVerts[15 + i] = glyphVerts[39 + i] = glyphVerts[63 + i] = c + u1 + v1 + w1;
            
            glyphVerts[18 + i] = glyphVerts[30 + i] = glyphVerts[51 + i] = c + u0 + v1 + w0;
            glyphVerts[21 + i] = glyphVerts[36 + i] = glyphVerts[69 + i] = c + u0 + v1 + w1;
        } 
        
        lines.setCoordinates(0, glyphVerts);
        lines.setColors(0, lineColors);
        float s = params.getScale();
        if (showReper && reper.getParent() == null)
            addChild(reper);
        if (!showReper && reper.getParent() != null)
            removeChild(reper);
        if (reper.getParent() != null)
            reper.update(new float[] {s, s, s});
    }
}
