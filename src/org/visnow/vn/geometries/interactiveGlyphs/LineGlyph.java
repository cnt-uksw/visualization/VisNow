/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.interactiveGlyphs;

import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.LINE;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams.*;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class LineGlyph extends Glyph
{
    
    public LineGlyph(InteractiveGlyphParams params)
    {
        super(params);
        type = LINE;
        setName("line glyph");
        visibleWidgets = U_ROT_VIS   | V_ROT_VIS   | W_ROT_VIS |
                         U_TRANS_VIS | V_TRANS_VIS | W_TRANS_VIS | 
                         SCALE_VIS   | AXES_VIS;
        params.setVisibleWidgets(visibleWidgets);
        reper = new Reper3D(params);
        addChild(reper);
    }
    
    public LineGlyph(InteractiveGlyphParams params, boolean useRadius)
    {
        super(params, useRadius);
        type = LINE;
        setName("line glyph");
        visibleWidgets = useRadius ? U_ROT_VIS   | V_ROT_VIS   | W_ROT_VIS |
                                     U_TRANS_VIS | V_TRANS_VIS | W_TRANS_VIS | 
                                     SCALE_VIS   | AXES_VIS  | RADIUS_VIS 
                                   : U_ROT_VIS   | V_ROT_VIS   | W_ROT_VIS |
                                     U_TRANS_VIS | V_TRANS_VIS | W_TRANS_VIS | 
                                     SCALE_VIS   | AXES_VIS;
        params.setVisibleWidgets(visibleWidgets);
        reper = new Reper3D(params);
        addChild(reper);
    }
    
    @Override
    public void updateColors()
    {
        reper.updateColors(currentColors);
    }
    
    @Override
    public void update()
    {
        float s = params.getScale();
        reper.update(new float[]{.3f * s, .3f * s, s});
    }
    
}
