/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.interactiveGlyphs;

import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedTriangleStripArray;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.SPHERE;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams.*;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class SphereGlyph extends Glyph
{
    protected static final int LOD = 32;
    protected static int nstrips = 4 * (LOD - 1);
    protected static int nverts = 4 * LOD * LOD;
    protected static int ninds = 8 * (LOD - 1) * LOD;
    protected static int [] strips = new int[4 * (LOD - 1)];
    protected static float[] templateVerts = new float[12 * LOD * LOD];
    protected static float[] normals = new float[12 * LOD * LOD];
    protected static int[] pntsIndex = new int[8 * (LOD - 1) * LOD];
    protected static int[] clrsIndex = new int[8 * (LOD - 1) * LOD];
    protected static boolean templateCreated = false;
    protected static IndexedTriangleStripArray surf;
    
    public SphereGlyph(InteractiveGlyphParams params)
    {
        super(params);
        reper = new Reper3D(params);
        type = SPHERE;
        setName("sphere glyph");
        visibleWidgets = U_ROT_VIS   | V_ROT_VIS   | W_ROT_VIS |
                         U_TRANS_VIS | V_TRANS_VIS | W_TRANS_VIS |  
                         RADIUS_VIS  | VOLUME_VIS  | SCALE_VIS;
        params.setVisibleWidgets(visibleWidgets);
        params.setScaleSliderTitle("sphere radius (in % of field radius)");
        if (!templateCreated) {
            int i, j, k, k0, l;
            float t0, t1, t2, r;

            float[] p0 = new float[]{0, 0, 1};
            float[] p1 = new float[]{1, 0, 0};
            float[] p2 = new float[]{0, 1, 0};

            float[] q0 = new float[]{0, 1, 0};
            float[] q1 = new float[]{0, 0, 1};
            float[] q2 = new float[]{-1, 0, 0};

            for (i = 0; i < 4 * (LOD - 1); i++)
                strips[i] = 2 * LOD;

            k = 0;
            k0 = k;
            for (i = 0; i < LOD; i++) {
                for (j = 0; j < LOD - i; j++) {
                    t0 = (float) i / (LOD - 1);
                    if (LOD - i - 1 > 0)
                        t2 = (1.f - t0) * (float) j / (LOD - i - 1);
                    else
                        t2 = 0.f;
                    t1 = 1.f - t0 - t2;
                    r = 0.f;
                    for (l = 0; l < 3; l++) {
                        templateVerts[3 * k + l] = t0 * p0[l] + t1 * p1[l] + t2 * p2[l];
                        r += templateVerts[3 * k + l] * templateVerts[3 * k + l];
                    }
                    r = (float) Math.sqrt(r);
                    for (l = 0; l < 3; l++)
                        templateVerts[3 * k + l] /= r;
                    k += 1;
                }
                for (j = 0; j < i; j++) {
                    t0 = (float) (LOD - 1 - i) / (LOD - 1);
                    t2 = (1.f - t0) * (float) (j + 1) / i;
                    t1 = 1.f - t0 - t2;
                    r = 0.f;
                    for (l = 0; l < 3; l++) {
                        templateVerts[3 * k + l] = t0 * q0[l] + t1 * q1[l] + t2 * q2[l];
                        r += templateVerts[3 * k + l] * templateVerts[3 * k + l];
                    }
                    r = (float) Math.sqrt(r);
                    for (l = 0; l < 3; l++)
                        templateVerts[3 * k + l] /= r;
                    k += 1;
                }
            }

            for (i = 0; i < k; i++) {
                templateVerts[3 * (i + k)]         = templateVerts[3 * i];
                templateVerts[3 * (i + k) + 1]     = -templateVerts[3 * i + 2];
                templateVerts[3 * (i + k) + 2]     = templateVerts[3 * i + 1];
                templateVerts[3 * (i + 2 * k)]     = templateVerts[3 * i];
                templateVerts[3 * (i + 2 * k) + 1] = -templateVerts[3 * i + 1];
                templateVerts[3 * (i + 2 * k) + 2] = -templateVerts[3 * i + 2];
                templateVerts[3 * (i + 3 * k)]     = templateVerts[3 * i];
                templateVerts[3 * (i + 3 * k) + 1] = templateVerts[3 * i + 2];
                templateVerts[3 * (i + 3 * k) + 2] = -templateVerts[3 * i + 1];
            }
            System.arraycopy(templateVerts, 0, normals, 0, 3 * nverts);
            l = 0;
            k = LOD * LOD;
            k0 = 2 * LOD * (LOD - 1);
            for (i = 0; i < LOD - 1; i++)
                for (j = 0; j < LOD; j++) {
                    pntsIndex[         l]     =         j * LOD + i;
                    pntsIndex[         l + 1] =         j * LOD + i + 1;
                    pntsIndex[    k0 + l]     =     k + j * LOD + i;
                    pntsIndex[    k0 + l + 1] =     k + j * LOD + i + 1;
                    pntsIndex[2 * k0 + l]     = 2 * k + j * LOD + i;
                    pntsIndex[2 * k0 + l + 1] = 2 * k + j * LOD + i + 1;
                    pntsIndex[3 * k0 + l]     = 3 * k + j * LOD + i;
                    pntsIndex[3 * k0 + l + 1] = 3 * k + j * LOD + i + 1;
                    l += 2;
                }

            for (i = 0; i < l; i++)
                clrsIndex[i] = 0;
            templateCreated = true;
        }
        int verticesMode = GeometryArray.COORDINATES | GeometryArray.NORMALS;
        surf = new IndexedTriangleStripArray(nverts, verticesMode, ninds, strips);
        surf.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        surf.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        surf.setCoordinates(0, templateVerts);
        surf.setCoordinateIndices(0, pntsIndex);
        surf.setCapability(GeometryArray.ALLOW_NORMAL_READ);
        surf.setCapability(GeometryArray.ALLOW_NORMAL_WRITE);
        surf.setNormals(0, normals);
        surf.setNormalIndices(0, pntsIndex);
        surfShape.setAppearance(surfApp);
        surfShape.addGeometry(surf);
        transpAttr.setTransparency(.8f);

        glyphVerts = new float[12 * LOD * LOD];
        glyphGroup.addChild(surfShape);
        glyphGroup.addChild(lineShape);
        addChild(reper);
    }
    
    @Override
    public void update()
    {   
        float s = .3f * params.glyphDim;
        float r = params.radius;
        for (int i = 0; i < 3; i++) {
            float c = params.center[i];
            for (int j = 0; j < glyphVerts.length; j += 3) 
                glyphVerts[i + j] = r * templateVerts[i + j] + c;
        }
        surf.setCoordinates(0, glyphVerts);
        reper.update(new float[]{s, s, s});
    }
    
    public void print()
    {
        for (int i = 0; i < nverts; i++) 
            System.out.printf("%7.4f, %7.4f, %7.4f%n", templateVerts[3 * i], templateVerts[3 * i + 1], templateVerts[3 * i + 2]);
        for (int i = 0; i < 8 * (LOD - 1) * LOD; i += LOD)  {
            for (int j = 0; j < LOD; j++) 
                System.out.printf("%5d, " , pntsIndex[i + j]);
            System.out.println("");
        }   
            
    }

}
