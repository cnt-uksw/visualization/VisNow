/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.interactiveGlyphs;

import static org.apache.commons.math3.util.FastMath.*;
import java.util.Arrays;
import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineStripArray;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.CIRCLE;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams.*;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class CircleGlyph extends Glyph
{
    private static final int LOD = 50;
    private static final int NVERTS = 4 * LOD + 5;
    protected int[] colorIndices = new int[NVERTS];
    protected IndexedLineStripArray glyphLineStrips;
    
    public CircleGlyph(InteractiveGlyphParams params)
    {
        super(params);
        setName("circle glyph");
        type = CIRCLE;
        params.setRadiusSliderTitle("circle radius (in % of field radius)");
        visibleWidgets = W_ROT_VIS | U_TRANS_VIS | V_TRANS_VIS | SCALE_VIS | RADIUS_VIS;
        params.setVisibleWidgets(visibleWidgets);
        glyphVerts = new float[3 * NVERTS];
        for (int i = 0; i <= 4 * LOD; i++) {
            double phi = (PI * i) / (2. * LOD);
            glyphVerts[3 * i]     = (float)cos(phi);
            glyphVerts[3 * i + 1] = (float)sin(phi);
        }
        glyphVerts[12 * LOD + 3]  = -.3f;
        glyphVerts[12 * LOD + 6]  = -.3f;
        glyphVerts[12 * LOD + 10] = -.3f;
        glyphVerts[12 * LOD + 13] = -.3f;
        Arrays.fill(glyphVerts, 0);
        glyphLineStrips = new IndexedLineStripArray(NVERTS,
                                                GeometryArray.COORDINATES |
                                                GeometryArray.COLOR_3,
                                                NVERTS, new int[] {4 * LOD + 1, 2, 2});
        int[] coordIndices = new int[NVERTS];
        for (int i = 0; i < NVERTS; i++) 
            coordIndices[i] = i;
        for (int i = 0; i < 4 * LOD + 1; i++) 
            colorIndices[i] = 0;
        colorIndices[4 * LOD + 1] = colorIndices[4 * LOD + 2] = 1;
        colorIndices[4 * LOD + 3] = colorIndices[4 * LOD + 4] = 2;
        glyphLineStrips.setCoordinateIndices(0, coordIndices);
        glyphLineStrips.setColorIndices(0, colorIndices);
        glyphLineStrips.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        glyphLineStrips.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        glyphLineStrips.setCapability(GeometryArray.ALLOW_COLOR_READ);
        glyphLineStrips.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
        glyphLineStrips.setCoordinates(0, glyphVerts);
        glyphLineStrips.setColors(0, lineColors);
        lineShape.setAppearance(lineApp);
        lineShape.addGeometry(glyphLineStrips);
        glyphGroup.addChild(lineShape);
    }
    
    @Override
    public void update()
    {
        float r = params.radius;
        for (int i = 0; i <= 4 * LOD; i++) {
            double phi = (PI * i) / (2. * LOD);
            glyphVerts[3 * i]     = params.center[0] + r * (float)cos(phi);
            glyphVerts[3 * i + 1] = params.center[1] + r * (float)sin(phi);
        }

        for (int i = 0; i < 3; i++) {
            float c = params.center[i];
            float u = params.glyphDim * params.u[i];
            float v = params.glyphDim * params.v[i];
            
            glyphVerts[12 * LOD +  3 + i] = c - u;
            glyphVerts[12 * LOD +  6 + i] = c + u;
            
            glyphVerts[12 * LOD +  9 + i] = c - v;
            glyphVerts[12 * LOD + 12 + i] = c + v;
        }
            
        glyphLineStrips.setCoordinates(0, glyphVerts);
        glyphLineStrips.setColors(0, lineColors);
    }
}
