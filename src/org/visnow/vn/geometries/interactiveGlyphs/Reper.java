/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.interactiveGlyphs;

import org.jogamp.java3d.IndexedLineStripArray;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
abstract public class Reper extends OpenBranchGroup
{
    protected InteractiveGlyphParams params;
    protected IndexedLineStripArray glyphLines;
    protected OpenShape3D lineShape    = new OpenShape3D("lines");
    protected float[]   scale;
    protected float[]   glyphVerts;
    protected float[][] currentColors = InteractiveGlyphParams.LIGHT_COLORS;
    protected float[]   lineColors = {0, .6f, .6f, .6f, .6f, 0, .6f, 0, .6f};
    
    public Reper(InteractiveGlyphParams params)
    {
        super();
        this.params = params;
        scale = new float[] {1, 1, 1};
    }
    
    public Reper(InteractiveGlyphParams params, float[] scale)
    {
        super();
        this.params = params;
        this.scale = scale;
    }
    
    public void updateColors(float[][] currentColors)
    {
        for (int i = 0; i < 3; i++)
            System.arraycopy(currentColors[(i + 1) % 3], 0, lineColors, 3 * i, 3);
    }
    
    abstract public void update(float[] scale);
    
    public final void update()
    {
        update(scale);
    }
}
