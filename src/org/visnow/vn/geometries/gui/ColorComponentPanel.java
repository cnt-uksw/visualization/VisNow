/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import org.visnow.vn.geometries.parameters.ColorComponentParams;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ColorComponentPanel extends javax.swing.JPanel
{

    private ColorComponentParams params = null;
    private Color c2 = null;
    private Color c1 = getBackground();
    private Color c0 = getBackground();
    private BufferedImage sample = new BufferedImage(100, 10, BufferedImage.TYPE_INT_RGB);
    private int lastX = 0, lastY = 0;
    private float lastMin = .1f, cMin = .1f;


    /**
     * Creates new form ColorComponentPanel
     */
    public ColorComponentPanel()
    {
        initComponents();
        updateSample();
        //      setEnabled(false);
    }

    public void setParams(ColorComponentParams params)
    {
        this.params = params;
        cMin = params.getColorComponentMin();
        params.setActive(false);
        dataRangeUI.setComponentValue(params.getColorRange());
        params.setActive(true);
    }

    private void updateSample()
    {
        int w = sample.getWidth();
        int h = sample.getHeight();
        float s = 1.f / (w - 1);
        int[] b0 = new int[]{c0.getRed(), c0.getGreen(), c0.getBlue()};
        int[] b1 = new int[]{c1.getRed(), c1.getGreen(), c1.getBlue()};
        if (c2 == null) {
            int[] b = new int[3];
            int[] sampleData = sample.getRGB(0, 0, w, h, null, 0, w);
            for (int i = 0; i < w; i++) {
                float t = i * s;
                for (int k = 0; k < 3; k++)
                    b[k] = (int) (t * b1[k] + (1 - t) * b0[k]);
                for (int j = 0; j < h; j++)
                    sampleData[j * w + i] = (b[0] << 16) | (b[1] << 8) | b[2];
                sample.setRGB(0, 0, w, h, sampleData, 0, w);
            }
        } else {
            int[] b2 = new int[]{c2.getRed(), c2.getGreen(), c2.getBlue()};
            int[] b = new int[3];
            int[] sampleData = sample.getRGB(0, 0, w, h, null, 0, w);
            for (int i = 0; i < w; i++) {
                float t = 2 * i * s;
                if (t <= 1)
                    for (int k = 0; k < 3; k++)
                        b[k] = (int) (t * b1[k] + (1 - t) * b0[k]);
                else
                    for (int k = 0; k < 3; k++)
                        b[k] = (int) ((t - 1) * b2[k] + (2 - t) * b1[k]);
                for (int j = 0; j < h; j++)
                    sampleData[j * w + i] = (b[0] << 16) | (b[1] << 8) | b[2];
                sample.setRGB(0, 0, w, h, sampleData, 0, w);
            }
        }
        this.repaint();
    }

    public void setSampleColors(Color col0, Color col1)
    {
        c0 = col0;
        c1 = col1;
        updateSample();
    }

    public void setSampleColors(Color col0, Color col1, Color col2)
    {
        c0 = col0;
        c1 = col1;
        c2 = col2;
        updateSample();
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        samplePanel = new JPanel()
        {
            public void paint(Graphics g)
            {
                Graphics2D gr = (Graphics2D)g;
                gr.drawImage(sample,0,0,getWidth(), getHeight(), null);
                gr.setColor(Color.DARK_GRAY);
                int p = (int)(cMin * getWidth());
                gr.fillRect(0,0,p,getHeight());
            }
        };
        dataRangeUI = new org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrangeUI();

        setLayout(new java.awt.GridBagLayout());

        samplePanel.setName("samplePanel"); // NOI18N
        samplePanel.setPreferredSize(new java.awt.Dimension(20, 20));
        samplePanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                samplePanelMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                samplePanelMouseReleased(evt);
            }
        });
        samplePanel.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                samplePanelMouseDragged(evt);
            }
        });

        javax.swing.GroupLayout samplePanelLayout = new javax.swing.GroupLayout(samplePanel);
        samplePanel.setLayout(samplePanelLayout);
        samplePanelLayout.setHorizontalGroup(
            samplePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        samplePanelLayout.setVerticalGroup(
            samplePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 4, 0);
        add(samplePanel, gridBagConstraints);

        dataRangeUI.setName("dataRangeUI"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(dataRangeUI, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void samplePanelMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_samplePanelMousePressed
    {//GEN-HEADEREND:event_samplePanelMousePressed
        lastX = evt.getX();
        lastMin = cMin;
    }//GEN-LAST:event_samplePanelMousePressed

    private void samplePanelMouseDragged(java.awt.event.MouseEvent evt)//GEN-FIRST:event_samplePanelMouseDragged
    {//GEN-HEADEREND:event_samplePanelMouseDragged
        if (samplePanel.isEnabled()) {
            cMin = lastMin + (float) (evt.getX() - lastX) / samplePanel.getWidth();
            if (cMin < 0)
                cMin = 0;
            if (cMin > 1)
                cMin = 1;
            samplePanel.repaint();
            if (params != null) {
                params.setAdjusting(true);
                params.setColorComponentMin(cMin);
            }
        }
    }//GEN-LAST:event_samplePanelMouseDragged

    private void samplePanelMouseReleased(java.awt.event.MouseEvent evt)//GEN-FIRST:event_samplePanelMouseReleased
    {//GEN-HEADEREND:event_samplePanelMouseReleased
        if (params != null)
            params.setAdjusting(false);
    }//GEN-LAST:event_samplePanelMouseReleased

    /**
     * Sets enabled state on this widget.
     */
    @Override
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        samplePanel.setEnabled(enabled);
    }
    
    public void updateDataValuesFromParams()
    {
        params.setActive(false);
        dataRangeUI.updateSubrange();
        cMin = params.getColorComponentMin();
        updateSample();
        params.setActive(true);
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrangeUI dataRangeUI;
    private javax.swing.JPanel samplePanel;
    // End of variables declaration//GEN-END:variables
    
}
