/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.gui;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.log4j.Logger;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.vn.geometries.events.ColorListener;
import org.visnow.vn.geometries.gui.TransparencyEditor.TransparencyEditor;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.gui.widgets.SectionHeader;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class DataMappingGUI extends javax.swing.JPanel
{

    private static final Logger LOGGER = Logger.getLogger(DataMappingGUI.class);
    private boolean debug = VisNow.isDebug();
    protected Field inField = null;
    protected CellSet inCellSet = null;
    protected DataMappingParams params = null;
    protected int nComponents = 0;
    protected int nCellComps = 0;
    protected boolean active = true;
    protected float colorMin = 0, colorMax = 255, colorPhysMin = 0, colorPhysMax = 1, colorD = 255;
    protected ComponentColormappingPanel[] colormappingPanels;
    protected ColorComponentPanel[] colorComponentPanels;
    protected DataContainerSchema lastFieldDataSchema = null;
    protected DataContainerSchema lastCellSetDataSchema = null;
    protected boolean transparencyActive = false;
    protected boolean transparencyStartNull = false;
    protected BufferedImage image = null;

    protected boolean showNodeCellPanel = true;
    private static final String TEXTURE_FILE_PROPERTY = "visnow.paths.data.last.org.visnow.vn.geometries.gui.DataMappingGUI";

    public static enum MappingType
    {
        INDEXED("Indexed"), RGB("RGB"), Textured("Textured");

        private String text;

        private MappingType(String text)
        {
            this.text = text;
        }

        @Override
        public String toString()
        {
            return text;
        }
    }

    private String lastTexturePath = null;

    /**
     * Creates new form DataMappingGUI
     */
    public DataMappingGUI()
    {
        initComponents();
        colormappingPanels = new ComponentColormappingPanel[] {
            blendingAlphaMapPanel, indexedPanel};
        colorComponentPanels = new ColorComponentPanel[]{
            redComponentPanel, greenComponentPanel, blueComponentPanel,blendingSBColorComponentPanel};
        redComponentPanel.setSampleColors(Color.black, Color.RED);
        greenComponentPanel.setSampleColors(Color.black, Color.GREEN);
        blueComponentPanel.setSampleColors(Color.black, Color.BLUE);
        blendingSBColorComponentPanel.setSampleColors(Color.GRAY, Color.RED);
        transparencyEditor.setStartNullTransparencyComponent(true);
        FileNameExtensionFilter allImagesFilter = new FileNameExtensionFilter("All image files", "jpg", "jpeg", "gif", "png", "JPG", "JPEG", "GIF", "PNG");
        FileNameExtensionFilter jpegImagesFilter = new FileNameExtensionFilter("JPEG images (*.jpg, *.jpeg)", "jpg", "jpeg", "JPG", "JPEG");
        FileNameExtensionFilter gifImagesFilter = new FileNameExtensionFilter("GIF images (*.gif)", "gif", "GIF");
        FileNameExtensionFilter pngImagesFilter = new FileNameExtensionFilter("PNG images (*.png)", "png", "PNG");
        textureFileChooser.addChoosableFileFilter(jpegImagesFilter);
        textureFileChooser.addChoosableFileFilter(gifImagesFilter);
        textureFileChooser.addChoosableFileFilter(pngImagesFilter);
        textureFileChooser.addChoosableFileFilter(allImagesFilter);

        panelsUpdate();
    }

    public DataMappingGUI(boolean showNodeCellPanel)
    {
        this();
        this.showNodeCellPanel = showNodeCellPanel;
        nodeCellPanel.setVisible(showNodeCellPanel);
    }

    public DataMappingGUI(CellSet inCellSet, Field inField, DataMappingParams params)
    {
        this();
        setTextureDir();
        setInData(inCellSet, inField, params);
    }

    public DataMappingGUI(CellSet inCellSet, Field inField, DataMappingParams params, boolean showNodeCellPanel)
    {
        this();
        setTextureDir();
        this.showNodeCellPanel = showNodeCellPanel;
        nodeCellPanel.setVisible(showNodeCellPanel);
        setInData(inCellSet, inField, params);
    }


    //<editor-fold defaultstate="collapsed" desc="PANELS: visible/hidden, collapse/expand, option switching, cross relation">
    public void panelsUpdate()
    {
        panelsUpdateSelectedOption();
        panelsUpdateEnabledState();
        panelsUpdateExpandedState();
    }

    private void panelsUpdateExpandedState()
    {
        dataMappingPanel.setVisible(mappingSectionHeader.isExpanded() && mappingSectionHeader.isVisible());
        transparencyEditor.setVisible(transparencySectionHeader.isExpanded() && transparencySectionHeader.isVisible());
        blendingPanel.setVisible(blendingSectionHeader.isExpanded() && blendingSectionHeader.isVisible());
        legendPanel.setVisible(legendSectionHeader.isExpanded() && legendSectionHeader.isVisible());
    }

    private void panelsUpdateEnabledState()
    {
        MappingType mapping_type = (MappingType) mappingTypeComboBox.getSelectedItem();

        boolean blendingPossible = mapping_type == MappingType.INDEXED;
        blendingPanelSetEnabled(blendingPossible && blendingSectionHeader.isSelected());
        blendingSectionHeader.setSelectable(blendingPossible);

        boolean legendPossible = mapping_type == MappingType.INDEXED && !blendingSectionHeader.isSelected();
        colormapLegendGUI.setEnabled(legendPossible && legendSectionHeader.isSelected());
        legendSectionHeader.setSelectable(legendPossible);
    }

    private void panelsUpdateSelectedOption()
    {
        panelsUpdateMappingTypeSelection();
        panelsUpdateBlendingSelection();
    }

    public void panelsUpdateMappingTypeSelection(MappingType mapping_type)
    {
        indexedPanel.setVisible(mapping_type == MappingType.INDEXED);
        rgbPanel.setVisible(mapping_type == MappingType.RGB);
        texturePanel.setVisible(mapping_type == MappingType.Textured);
    }

    private void panelsUpdateMappingTypeSelection()
    {
        MappingType mapping_type = (MappingType) mappingTypeComboBox.getSelectedItem();
        indexedPanel.setVisible(mapping_type == MappingType.INDEXED);
        rgbPanel.setVisible(mapping_type == MappingType.RGB);
        texturePanel.setVisible(mapping_type == MappingType.Textured);
    }

    private void panelsUpdateBlendingSelection()
    {
        switch (((BlendingMode) blendingTypeComboBox.getSelectedItem())) {
            case ALPHA:
                blendingAlphaPanel.setVisible(true);
                blendingSBPanel.setVisible(false);
                break;
            case SATURATION:
            case BRIGHTNESS:
                blendingAlphaPanel.setVisible(false);
                blendingSBPanel.setVisible(true);
                break;
            default:
                throw new IllegalStateException("Incorrect blending type");
        }
    }

    private void blendingPanelSetEnabled(boolean enabled)
    {
        blendingTypeLabel.setEnabled(enabled);
        blendingTypeComboBox.setEnabled(enabled);
        blendingAlphaRatioLabel.setEnabled(enabled);
        blendingAlphaRatioSlider.setEnabled(enabled);
        blendingAlphaMapPanel.setVisible(enabled);
        blendingAlphaMapPanel.setEnabled(enabled);
        blendingSBColorComponentPanel.setVisible(enabled);
        blendingSBColorComponentPanel.setEnabled(enabled);
    }

    public enum FoldablePanel
    {
        Mapping, Transparency, Blending, Legend
    }

    public void panelFold(FoldablePanel panel, boolean expanded)
    {
        SectionHeader sectionHeader = null;

        switch (panel) {
            case Mapping:
                sectionHeader = mappingSectionHeader;
                break;
            case Transparency:
                sectionHeader = transparencySectionHeader;
                break;
            case Blending:
                sectionHeader = blendingSectionHeader;
                break;
            case Legend:
                sectionHeader = legendSectionHeader;
                break;
        }

        sectionHeader.setExpanded(expanded);
        panelsUpdate();
    }

    //</editor-fold>
    private void updateBlendingMode()
    {
        if (!blendingSectionHeader.isSelected()) {
            params.setColorMapModification(DataMappingParams.NO_MAP_MODIFICATION);
            colormapLegendGUI.processEnable(legendSectionHeader.isSelected());
        } else {
            colormapLegendGUI.processEnable(false);
            DataMappingGUI.BlendingMode blendingMode = (BlendingMode) blendingTypeComboBox.getSelectedItem();
            if (null == blendingMode)
                throw new IllegalStateException("Incorrect blending type");
            else switch (blendingMode) {
                case ALPHA:
                    params.setColorMapModification(DataMappingParams.BLEND_MAP_MODIFICATION);
                    break;
                case SATURATION:
                    params.setColorMapModification(DataMappingParams.SAT_MAP_MODIFICATION);
                    updateColorModification();
                    break;
                case BRIGHTNESS:
                    params.setColorMapModification(DataMappingParams.VAL_MAP_MODIFICATION);
                    updateColorModification();
                    break;
                default:
                    throw new IllegalStateException("Incorrect blending type");
            }
        }
    }

    private static enum BlendingMode
    {
        ALPHA("alpha"),
        SATURATION("saturation"),
        BRIGHTNESS("brightness");

        private final String text;

        private BlendingMode(String text)
        {
            this.text = text;
        }

        @Override
        public String toString()
        {
            return text;
        }
    };

    private void updateColorModification()
    {
        if (((BlendingMode) blendingTypeComboBox.getSelectedItem()).equals(DataMappingGUI.BlendingMode.SATURATION)) {
            blendingSBColorComponentPanel.setParams(params.getSatParams());
            blendingSBColorComponentPanel.setSampleColors(Color.white, Color.red);
            params.setColorMapModification(DataMappingParams.SAT_MAP_MODIFICATION);
        } else if (((BlendingMode) blendingTypeComboBox.getSelectedItem()).equals(DataMappingGUI.BlendingMode.BRIGHTNESS)) {
            blendingSBColorComponentPanel.setParams(params.getValParams());
            blendingSBColorComponentPanel.setSampleColors(Color.black, Color.red);
            params.setColorMapModification(DataMappingParams.VAL_MAP_MODIFICATION);
        }
        params.fireStateChanged(RenderEvent.COLORS);
    }

    public void setShowNodeCellPanel(boolean showNodeCellPanel)
    {
        this.showNodeCellPanel = showNodeCellPanel;
        nodeCellPanel.setVisible(showNodeCellPanel);
    }

    private void setTextureDir()
    {
        params.setTextureFileName(VisNow.get().getMainConfig().getProperty(TEXTURE_FILE_PROPERTY));
        if (params.getTextureFileName() != null)
            textureFileChooser.setSelectedFile(new File(params.getTextureFileName()));
        else
            textureFileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getDefaultDataPath()));
    }

    public void setNodeCellData(boolean isNodeData, boolean isCellData)
    {
        nodeCellPanel.setVisible(isNodeData && isCellData);
    }

    public void setParams(DataMappingParams params)
    {
        this.params = params;
        params.setGUI(this);
        redComponentPanel.setParams(params.getRedParams());
        greenComponentPanel.setParams(params.getGreenParams());
        blueComponentPanel.setParams(params.getBlueParams());
        uComponentPanel.setParams(params.getUParams());
        vComponentPanel.setParams(params.getVParams());
        transparencyEditor.setParams(params.getTransparencyParams());
        indexedPanel.setMap(params.getColorMap0());
        blendingAlphaMapPanel.setMap(params.getColorMap1());
        colormapLegendGUI.setParams(params.getColormapLegendParameters());
        updateDataValuesFromParams();
    }

    public final void setInData(Field inField, DataMappingParams params)
    {
        if (params == null || inField == null)
            return;
        boolean hasCellData = false;
        if (inField instanceof IrregularField) {
            IrregularField iIF = (IrregularField)inField;
            hasCellData = (iIF.getCellDataSchema() != null) &&
                          (iIF.getCellDataSchema().getComponentSchemas() != null) &&
                          !iIF.getCellDataSchema().getComponentSchemas().isEmpty();
        nodeCellPanel.setVisible(!(inField.getSchema().getPseudoComponentSchemas().isEmpty() &&
                                   inField.getSchema().getComponentSchemas().isEmpty()) &&
                                 hasCellData);
        }
        inCellSet = null;
        this.inField = inField;
        setParams(params);
        if (!inField.getSchema().getComponentSchemas().isEmpty())
             params.setCellDataMapped(false);
        else
             params.setCellDataMapped(hasCellData);
        if (lastFieldDataSchema != null && inField.getSchema().isCompatibleWith(lastFieldDataSchema, true))
            return;
        lastFieldDataSchema = inField.getSchema();
        lastCellSetDataSchema = null;
        nComponents = nCellComps = 0;
        for (int i = 0; i < inField.getNComponents(); i++)
            if (inField.getComponent(i).isNumeric())
                nComponents += 1;
    }

    public final void setInData(CellSet inCellSet, Field inField, DataMappingParams params)
    {
        if (params == null || inCellSet == null || inField == null)
            return;
        nodeCellPanel.setVisible((inField.getSchema().getPseudoComponentSchemas().isEmpty() &&
                                  inField.getSchema().getComponentSchemas().isEmpty()) &&
                                  !inCellSet.getSchema().getComponentSchemas().isEmpty());

        if (!inField.getSchema().getPseudoComponentSchemas().isEmpty())
           nodeCellPanel.setVisible(true);
        this.inCellSet = inCellSet;
        this.inField = inField;
        setParams(params);
        if (lastFieldDataSchema != null && inField.getSchema().isCompatibleWith(lastFieldDataSchema, true) &&
                (lastCellSetDataSchema == null || inCellSet.getSchema().isCompatibleWith(lastCellSetDataSchema, active)))
            return;
        lastFieldDataSchema = inField.getSchema();
        lastCellSetDataSchema = inCellSet.getSchema();
        nComponents = nCellComps = 0;
        for (int i = 0; i < inField.getNComponents(); i++)
            if (inField.getComponent(i).isNumeric())
                nComponents += 1;

        for (int i = 0; i < inCellSet.getNComponents(); i++)
            if (inCellSet.getComponent(i).isNumeric())
                nCellComps += 1;
        if (nComponents == 0 && nCellComps != 0) {
            this.params.setCellDataMapped(true);
            cellDataButton.setSelected(true);
            nodeDataButton.setEnabled(false);
            nodeDataButton.setVisible(false);
        }
        if (nComponents != 0) {
            this.params.setCellDataMapped(false);
            nodeDataButton.setSelected(true);
            nodeDataButton.setEnabled(true);
            if (nCellComps == 0) {
                cellDataButton.setEnabled(false);
                cellDataButton.setVisible(false);
            }
        }
    }

    public TransparencyEditor getTransparencyEditor()
    {
        return transparencyEditor;
    }

    public ComponentColormappingPanel getMap0Panel()
    {
        return indexedPanel;
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT
     * modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        textureFileChooser = new javax.swing.JFileChooser();
        nodeCellGroup = new javax.swing.ButtonGroup();
        colormapTextureGroup = new javax.swing.ButtonGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        mappingSectionHeader = new org.visnow.vn.gui.widgets.SectionHeader();
        dataMappingPanel = new javax.swing.JPanel();
        nodeCellPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        nodeDataButton = new javax.swing.JRadioButton();
        cellDataButton = new javax.swing.JRadioButton();
        mappingTypePanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        mappingTypeComboBox = new org.visnow.vn.gui.widgets.WidePopupComboBox();
        indexedPanel = new org.visnow.vn.geometries.gui.ComponentColormappingPanel();
        rgbPanel = new javax.swing.JPanel();
        redComponentPanel = new org.visnow.vn.geometries.gui.ColorComponentPanel();
        greenComponentPanel = new org.visnow.vn.geometries.gui.ColorComponentPanel();
        blueComponentPanel = new org.visnow.vn.geometries.gui.ColorComponentPanel();
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        texturePanel = new javax.swing.JPanel();
        uComponentPanel = new org.visnow.vn.geometries.gui.TextureComponentPanel();
        vComponentPanel = new org.visnow.vn.geometries.gui.TextureComponentPanel();
        textureFilePanel = new javax.swing.JPanel();
        readTextureButton = new javax.swing.JButton();
        imagePathField = new javax.swing.JTextField();
        textureImageFlipXCB = new javax.swing.JCheckBox();
        textureImageFlipYCB = new javax.swing.JCheckBox();
        textureImagePanel = new javax.swing.JPanel();
        imagePanel = new org.visnow.vn.gui.widgets.ImagePanel();
        transparencySectionHeader = new org.visnow.vn.gui.widgets.SectionHeader();
        transparencyEditor = new org.visnow.vn.geometries.gui.TransparencyEditor.TransparencyEditor();
        blendingSectionHeader = new org.visnow.vn.gui.widgets.SectionHeader();
        blendingPanel = new javax.swing.JPanel();
        blendingTypePanel = new javax.swing.JPanel();
        blendingTypeLabel = new javax.swing.JLabel();
        blendingTypeComboBox = new org.visnow.vn.gui.swingwrappers.ComboBox();
        blendingAlphaPanel = new javax.swing.JPanel();
        ratioPanel = new javax.swing.JPanel();
        blendingAlphaRatioLabel = new javax.swing.JLabel();
        blendingAlphaRatioSlider = new javax.swing.JSlider();
        blendingAlphaMapPanel = new org.visnow.vn.geometries.gui.ComponentColormappingPanel();
        filler4 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        blendingSBPanel = new javax.swing.JPanel();
        blendingSBColorComponentPanel = new org.visnow.vn.geometries.gui.ColorComponentPanel();
        legendSectionHeader = new org.visnow.vn.gui.widgets.SectionHeader();
        legendPanel = new javax.swing.JPanel();
        colormapLegendGUI = new org.visnow.vn.geometries.gui.ColormapLegendGUI();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        textureFileChooser.setName("textureFileChooser"); // NOI18N

        setRequestFocusEnabled(false);
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });
        setLayout(new java.awt.GridBagLayout());

        mappingSectionHeader.setName("mappingSectionHeader"); // NOI18N
        mappingSectionHeader.setShowCheckBox(false);
        mappingSectionHeader.setText("Mapping");
        mappingSectionHeader.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                mappingSectionHeaderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(mappingSectionHeader, gridBagConstraints);

        dataMappingPanel.setName("dataMappingPanel"); // NOI18N
        dataMappingPanel.setLayout(new java.awt.GridBagLayout());

        nodeCellPanel.setName("nodeCellPanel"); // NOI18N
        nodeCellPanel.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("Data source:");
        jLabel1.setName("jLabel1"); // NOI18N
        nodeCellPanel.add(jLabel1, new java.awt.GridBagConstraints());

        nodeCellGroup.add(nodeDataButton);
        nodeDataButton.setSelected(true);
        nodeDataButton.setText("Nodes");
        nodeDataButton.setName("nodeDataButton"); // NOI18N
        nodeDataButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nodeDataButtonActionPerformed(evt);
            }
        });
        nodeCellPanel.add(nodeDataButton, new java.awt.GridBagConstraints());

        nodeCellGroup.add(cellDataButton);
        cellDataButton.setText("Cells");
        cellDataButton.setName("cellDataButton"); // NOI18N
        cellDataButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cellDataButtonActionPerformed(evt);
            }
        });
        nodeCellPanel.add(cellDataButton, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 4);
        dataMappingPanel.add(nodeCellPanel, gridBagConstraints);

        mappingTypePanel.setName("mappingTypePanel"); // NOI18N
        mappingTypePanel.setLayout(new java.awt.GridBagLayout());

        jLabel3.setText("Mapping type:");
        jLabel3.setName("jLabel3"); // NOI18N
        mappingTypePanel.add(jLabel3, new java.awt.GridBagConstraints());

        mappingTypeComboBox.setListData(MappingType.values());
        mappingTypeComboBox.setName("mappingTypeComboBox"); // NOI18N
        mappingTypeComboBox.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                mappingTypeComboBoxUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 0);
        mappingTypePanel.add(mappingTypeComboBox, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 4, 4);
        dataMappingPanel.add(mappingTypePanel, gridBagConstraints);

        indexedPanel.setBorder(null);
        indexedPanel.setName("indexedPanel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 28, 4);
        dataMappingPanel.add(indexedPanel, gridBagConstraints);

        rgbPanel.setName("rgbPanel"); // NOI18N
        rgbPanel.setLayout(new java.awt.GridBagLayout());

        redComponentPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "red", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10), new java.awt.Color(204, 0, 51))); // NOI18N
        redComponentPanel.setName("redComponentPanel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        rgbPanel.add(redComponentPanel, gridBagConstraints);

        greenComponentPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "green", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10), new java.awt.Color(0, 155, 0))); // NOI18N
        greenComponentPanel.setName("greenComponentPanel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        rgbPanel.add(greenComponentPanel, gridBagConstraints);

        blueComponentPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "blue", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10), new java.awt.Color(0, 64, 255))); // NOI18N
        blueComponentPanel.setName("blueComponentPanel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        rgbPanel.add(blueComponentPanel, gridBagConstraints);

        filler2.setName("filler2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weighty = 1.0;
        rgbPanel.add(filler2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 28, 4);
        dataMappingPanel.add(rgbPanel, gridBagConstraints);

        texturePanel.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        texturePanel.setName("texturePanel"); // NOI18N
        texturePanel.setLayout(new java.awt.GridBagLayout());

        uComponentPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "u component", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        uComponentPanel.setName("uComponentPanel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        texturePanel.add(uComponentPanel, gridBagConstraints);

        vComponentPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "v component", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        vComponentPanel.setName("vComponentPanel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        texturePanel.add(vComponentPanel, gridBagConstraints);

        textureFilePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "texture image/colormap", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 10))); // NOI18N
        textureFilePanel.setName("textureFilePanel"); // NOI18N
        textureFilePanel.setLayout(new java.awt.GridBagLayout());

        readTextureButton.setText("read"); // NOI18N
        readTextureButton.setName("readTextureButton"); // NOI18N
        readTextureButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                readTextureButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        textureFilePanel.add(readTextureButton, gridBagConstraints);

        imagePathField.setText(" "); // NOI18N
        imagePathField.setName("imagePathField"); // NOI18N
        imagePathField.setPreferredSize(new java.awt.Dimension(10, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        textureFilePanel.add(imagePathField, gridBagConstraints);

        textureImageFlipXCB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        textureImageFlipXCB.setText("flip x");
        textureImageFlipXCB.setEnabled(false);
        textureImageFlipXCB.setName("textureImageFlipXCB"); // NOI18N
        textureImageFlipXCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textureImageFlipXCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        textureFilePanel.add(textureImageFlipXCB, gridBagConstraints);

        textureImageFlipYCB.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        textureImageFlipYCB.setText("flip y");
        textureImageFlipYCB.setEnabled(false);
        textureImageFlipYCB.setName("textureImageFlipYCB"); // NOI18N
        textureImageFlipYCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textureImageFlipYCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        textureFilePanel.add(textureImageFlipYCB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        texturePanel.add(textureFilePanel, gridBagConstraints);

        textureImagePanel.setName("textureImagePanel"); // NOI18N
        textureImagePanel.setLayout(new java.awt.BorderLayout());

        imagePanel.setMinimumSize(new java.awt.Dimension(200, 200));
        imagePanel.setName("imagePanel"); // NOI18N
        imagePanel.setPreferredSize(new java.awt.Dimension(200, 200));

        javax.swing.GroupLayout imagePanelLayout = new javax.swing.GroupLayout(imagePanel);
        imagePanel.setLayout(imagePanelLayout);
        imagePanelLayout.setHorizontalGroup(
            imagePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 329, Short.MAX_VALUE)
        );
        imagePanelLayout.setVerticalGroup(
            imagePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 200, Short.MAX_VALUE)
        );

        textureImagePanel.add(imagePanel, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        texturePanel.add(textureImagePanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 28, 4);
        dataMappingPanel.add(texturePanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(8, 0, 0, 0);
        add(dataMappingPanel, gridBagConstraints);

        transparencySectionHeader.setExpanded(false);
        transparencySectionHeader.setName("transparencySectionHeader"); // NOI18N
        transparencySectionHeader.setShowCheckBox(false);
        transparencySectionHeader.setText("Transparency");
        transparencySectionHeader.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                transparencySectionHeaderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(transparencySectionHeader, gridBagConstraints);

        transparencyEditor.setName("transparencyEditor"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(8, 4, 28, 4);
        add(transparencyEditor, gridBagConstraints);

        blendingSectionHeader.setExpanded(false);
        blendingSectionHeader.setName("blendingSectionHeader"); // NOI18N
        blendingSectionHeader.setText("Blending");
        blendingSectionHeader.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                blendingSectionHeaderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(blendingSectionHeader, gridBagConstraints);

        blendingPanel.setName("blendingPanel"); // NOI18N
        blendingPanel.setLayout(new java.awt.GridBagLayout());

        blendingTypePanel.setName("blendingTypePanel"); // NOI18N
        blendingTypePanel.setLayout(new java.awt.BorderLayout());

        blendingTypeLabel.setText("Type: ");
        blendingTypeLabel.setEnabled(false);
        blendingTypeLabel.setName("blendingTypeLabel"); // NOI18N
        blendingTypePanel.add(blendingTypeLabel, java.awt.BorderLayout.WEST);

        blendingTypeComboBox.setEnabled(false);
        blendingTypeComboBox.setListData(BlendingMode.values());
        blendingTypeComboBox.setName("blendingTypeComboBox"); // NOI18N
        blendingTypeComboBox.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                blendingTypeComboBoxUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        blendingTypePanel.add(blendingTypeComboBox, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        blendingPanel.add(blendingTypePanel, gridBagConstraints);

        blendingAlphaPanel.setName("blendingAlphaPanel"); // NOI18N
        blendingAlphaPanel.setRequestFocusEnabled(false);
        blendingAlphaPanel.setLayout(new java.awt.GridBagLayout());

        ratioPanel.setName("ratioPanel"); // NOI18N
        ratioPanel.setLayout(new java.awt.GridBagLayout());

        blendingAlphaRatioLabel.setText("Ratio:");
        blendingAlphaRatioLabel.setEnabled(false);
        blendingAlphaRatioLabel.setName("blendingAlphaRatioLabel"); // NOI18N
        ratioPanel.add(blendingAlphaRatioLabel, new java.awt.GridBagConstraints());

        blendingAlphaRatioSlider.setFont(new java.awt.Font("Dialog", 0, 8)); // NOI18N
        blendingAlphaRatioSlider.setMajorTickSpacing(20);
        blendingAlphaRatioSlider.setMinorTickSpacing(2);
        blendingAlphaRatioSlider.setPaintLabels(true);
        blendingAlphaRatioSlider.setPaintTicks(true);
        blendingAlphaRatioSlider.setEnabled(false);
        blendingAlphaRatioSlider.setName("blendingAlphaRatioSlider"); // NOI18N
        blendingAlphaRatioSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                blendingAlphaRatioSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        ratioPanel.add(blendingAlphaRatioSlider, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        blendingAlphaPanel.add(ratioPanel, gridBagConstraints);

        blendingAlphaMapPanel.setBorder(null);
        blendingAlphaMapPanel.setEnabled(false);
        blendingAlphaMapPanel.setName("blendingAlphaMapPanel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        blendingAlphaPanel.add(blendingAlphaMapPanel, gridBagConstraints);

        filler4.setName("filler4"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weighty = 1.0;
        blendingAlphaPanel.add(filler4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(8, 0, 0, 0);
        blendingPanel.add(blendingAlphaPanel, gridBagConstraints);

        blendingSBPanel.setName("blendingSBPanel"); // NOI18N
        blendingSBPanel.setLayout(new java.awt.GridBagLayout());

        blendingSBColorComponentPanel.setEnabled(false);
        blendingSBColorComponentPanel.setName("blendingSBColorComponentPanel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        blendingSBPanel.add(blendingSBColorComponentPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(8, 0, 0, 0);
        blendingPanel.add(blendingSBPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(8, 4, 28, 4);
        add(blendingPanel, gridBagConstraints);

        legendSectionHeader.setExpanded(false);
        legendSectionHeader.setName("legendSectionHeader"); // NOI18N
        legendSectionHeader.setText("Legend");
        legendSectionHeader.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                legendSectionHeaderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(legendSectionHeader, gridBagConstraints);

        legendPanel.setName("legendPanel"); // NOI18N
        legendPanel.setLayout(new java.awt.BorderLayout());

        colormapLegendGUI.setName("colormapLegendGUI"); // NOI18N
        legendPanel.add(colormapLegendGUI, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(8, 4, 0, 4);
        add(legendPanel, gridBagConstraints);

        filler1.setName("filler1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        add(filler1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void readTextureButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_readTextureButtonActionPerformed
    {//GEN-HEADEREND:event_readTextureButtonActionPerformed
        if (lastTexturePath != null)
            textureFileChooser.setCurrentDirectory(new File(lastTexturePath));
        else if (VisNow.get() != null)
            textureFileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(DataMappingGUI.class)));

        if (textureFileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            String path = textureFileChooser.getSelectedFile().getAbsolutePath();
            lastTexturePath = path.substring(0, path.lastIndexOf(File.separator));
            if (VisNow.get() != null)
                VisNow.get().getMainConfig().setLastDataPath(lastTexturePath, DataMappingGUI.class);
            params.setTextureFileName(path);
        }
    }//GEN-LAST:event_readTextureButtonActionPerformed

    private void cellDataButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cellDataButtonActionPerformed
    {//GEN-HEADEREND:event_cellDataButtonActionPerformed
        if (params == null)
            return;
        params.setCellDataMapped(cellDataButton.isSelected());

    }//GEN-LAST:event_cellDataButtonActionPerformed

    private void nodeDataButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_nodeDataButtonActionPerformed
    {//GEN-HEADEREND:event_nodeDataButtonActionPerformed
        if (params == null)
            return;
        params.setCellDataMapped(cellDataButton.isSelected());
    }//GEN-LAST:event_nodeDataButtonActionPerformed

    private void textureImageFlipXCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textureImageFlipXCBActionPerformed
        params.setTextureImageFlipX(textureImageFlipXCB.isSelected());
    }//GEN-LAST:event_textureImageFlipXCBActionPerformed

    private void textureImageFlipYCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textureImageFlipYCBActionPerformed
        params.setTextureImageFlipY(textureImageFlipYCB.isSelected());
    }//GEN-LAST:event_textureImageFlipYCBActionPerformed

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
//        LOGGER.debug(getPreferredSize());
    }//GEN-LAST:event_formComponentResized

    private void transparencySectionHeaderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_transparencySectionHeaderUserChangeAction
    {//GEN-HEADEREND:event_transparencySectionHeaderUserChangeAction
        if (evt.getEventType() == SectionHeader.EVENT_TOGGLE_EXPANDED)
            panelsUpdate();
    }//GEN-LAST:event_transparencySectionHeaderUserChangeAction

    private void mappingSectionHeaderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_mappingSectionHeaderUserChangeAction
    {//GEN-HEADEREND:event_mappingSectionHeaderUserChangeAction
        if (evt.getEventType() == SectionHeader.EVENT_TOGGLE_EXPANDED)
            panelsUpdate();
    }//GEN-LAST:event_mappingSectionHeaderUserChangeAction


    private void mappingTypeComboBoxUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_mappingTypeComboBoxUserChangeAction
    {//GEN-HEADEREND:event_mappingTypeComboBoxUserChangeAction
        panelsUpdate();
        if (params != null) {
            switch ((MappingType) mappingTypeComboBox.getSelectedItem()) {
                case INDEXED:
                    if (params.getColorMode() != DataMappingParams.COLORMAPPED)
                        params.setColorMode(DataMappingParams.COLORMAPPED);
                    break;
                case RGB:
                    if (params.getColorMode() != DataMappingParams.RGB)
                        params.setColorMode(DataMappingParams.RGB);
                    break;
                case Textured:
                    if (params.getColorMode() != DataMappingParams.UVTEXTURED)
                        params.setColorMode(DataMappingParams.UVTEXTURED);
                    break;
                default:
                    throw new IllegalStateException("Mapping type: " + mappingTypeComboBox.getSelectedItem() + " not supported");
            }

        }
    }//GEN-LAST:event_mappingTypeComboBoxUserChangeAction

    private void blendingSectionHeaderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_blendingSectionHeaderUserChangeAction
    {//GEN-HEADEREND:event_blendingSectionHeaderUserChangeAction
        panelsUpdate();
        if (evt.getEventType() == SectionHeader.EVENT_CHECKBOX_SWITCHED)
            if (params != null) updateBlendingMode();
    }//GEN-LAST:event_blendingSectionHeaderUserChangeAction

    private void blendingTypeComboBoxUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_blendingTypeComboBoxUserChangeAction
    {//GEN-HEADEREND:event_blendingTypeComboBoxUserChangeAction
        panelsUpdate();
        if (params != null) updateBlendingMode();
    }//GEN-LAST:event_blendingTypeComboBoxUserChangeAction

    private void blendingAlphaRatioSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_blendingAlphaRatioSliderStateChanged
    {//GEN-HEADEREND:event_blendingAlphaRatioSliderStateChanged
        params.setAdjusting(blendingAlphaRatioSlider.getValueIsAdjusting());
        params.setBlendRatio(blendingAlphaRatioSlider.getValue() / 100.f);
    }//GEN-LAST:event_blendingAlphaRatioSliderStateChanged

    private void legendSectionHeaderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_legendSectionHeaderUserChangeAction
    {//GEN-HEADEREND:event_legendSectionHeaderUserChangeAction
        panelsUpdate();
        if (evt.getEventType() == SectionHeader.EVENT_CHECKBOX_SWITCHED)
            colormapLegendGUI.processEnable(legendSectionHeader.isSelected());
    }//GEN-LAST:event_legendSectionHeaderUserChangeAction

    public ColorListener getBackgroundColorListener()
    {
        return transparencyEditor.getColorListener();
    }

    public DataMappingParams getDataMappingParams()
    {
        return params;
    }

    public boolean isTransparencyStartNull()
    {
        return transparencyStartNull;
    }

    public void setStartNullTransparencyComponent(boolean startNull)
    {
        transparencyStartNull = startNull;
        transparencyEditor.setStartNullTransparencyComponent(startNull);
    }

    public void updateWidgets()
    {
        if (params == null)
            return;
        active = false;
        active = true;
    }

    public void updateDataValuesFromParams()
    {
        params.setActive(false);
        if ((params.getColorMode() & DataMappingParams.COLORMAPPED) != 0)
            mappingTypeComboBox.setSelectedIndex(0);
        if ((params.getColorMode() & DataMappingParams.RGB) != 0)
            mappingTypeComboBox.setSelectedIndex(1);
        if ((params.getColorMode() & DataMappingParams.UVTEXTURED) != 0)
            mappingTypeComboBox.setSelectedIndex(2);
        indexedPanel.updateDataValuesFromParams();
        blendingAlphaMapPanel.updateDataValuesFromParams();
        redComponentPanel.updateDataValuesFromParams();
        greenComponentPanel.updateDataValuesFromParams();
        blueComponentPanel.updateDataValuesFromParams();
        uComponentPanel.updateDataValuesFromParams();
        vComponentPanel.updateDataValuesFromParams();
        switch (params.getColorMapModification()) {
            case DataMappingParams.SAT_MAP_MODIFICATION:
                blendingTypeComboBox.setSelectedItem(BlendingMode.SATURATION);
                blendingSectionHeader.setExpanded(true);
                blendingSectionHeader.setSelected(true);
                break;
            case DataMappingParams.VAL_MAP_MODIFICATION:
                blendingTypeComboBox.setSelectedItem(BlendingMode.BRIGHTNESS);
                blendingSectionHeader.setExpanded(true);
                blendingSectionHeader.setSelected(true);
                break;
            case DataMappingParams.BLEND_MAP_MODIFICATION:
                blendingTypeComboBox.setSelectedItem(BlendingMode.ALPHA);
                blendingSectionHeader.setExpanded(true);
                blendingSectionHeader.setSelected(true);
                break;
            case DataMappingParams.NO_MAP_MODIFICATION:
                blendingSectionHeader.setExpanded(false);
                blendingSectionHeader.setSelected(false);
                break;
        }
        blendingAlphaRatioSlider.setValue((int)(params.getBlendRatio() * 100));
        blendingAlphaMapPanel.updateDataValuesFromParams();
        params.setActive(true);
    }

    public void setTransparencyControlsVisible(boolean visible)
    {
        transparencyEditor.setVisible(visible);
        transparencySectionHeader.setVisible(visible);
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled); //To change body of generated methods, choose Tools | Templates.
        nodeCellPanel.setEnabled(enabled);
        nodeDataButton.setEnabled(enabled);
        cellDataButton.setEnabled(enabled);
        indexedPanel.setEnabled(enabled);
        rgbPanel.setEnabled(enabled);
        texturePanel.setEnabled(enabled);

        blendingTypeComboBox.setEnabled(enabled);
        colormapLegendGUI.setEnabled(enabled);
        blendingAlphaRatioSlider.setEnabled(enabled);
        blendingAlphaMapPanel.setEnabled(enabled);
        blendingSBColorComponentPanel.setEnabled(enabled);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected org.visnow.vn.geometries.gui.ComponentColormappingPanel blendingAlphaMapPanel;
    protected javax.swing.JPanel blendingAlphaPanel;
    protected javax.swing.JLabel blendingAlphaRatioLabel;
    protected javax.swing.JSlider blendingAlphaRatioSlider;
    protected javax.swing.JPanel blendingPanel;
    protected org.visnow.vn.geometries.gui.ColorComponentPanel blendingSBColorComponentPanel;
    protected javax.swing.JPanel blendingSBPanel;
    protected org.visnow.vn.gui.widgets.SectionHeader blendingSectionHeader;
    protected org.visnow.vn.gui.swingwrappers.ComboBox blendingTypeComboBox;
    protected javax.swing.JLabel blendingTypeLabel;
    protected javax.swing.JPanel blendingTypePanel;
    protected org.visnow.vn.geometries.gui.ColorComponentPanel blueComponentPanel;
    protected javax.swing.ButtonGroup buttonGroup1;
    protected javax.swing.JRadioButton cellDataButton;
    protected org.visnow.vn.geometries.gui.ColormapLegendGUI colormapLegendGUI;
    protected javax.swing.ButtonGroup colormapTextureGroup;
    protected javax.swing.JPanel dataMappingPanel;
    protected javax.swing.Box.Filler filler1;
    protected javax.swing.Box.Filler filler2;
    protected javax.swing.Box.Filler filler4;
    protected org.visnow.vn.geometries.gui.ColorComponentPanel greenComponentPanel;
    protected org.visnow.vn.gui.widgets.ImagePanel imagePanel;
    protected javax.swing.JTextField imagePathField;
    protected org.visnow.vn.geometries.gui.ComponentColormappingPanel indexedPanel;
    protected javax.swing.JLabel jLabel1;
    protected javax.swing.JLabel jLabel3;
    protected javax.swing.JPanel legendPanel;
    protected org.visnow.vn.gui.widgets.SectionHeader legendSectionHeader;
    protected org.visnow.vn.gui.widgets.SectionHeader mappingSectionHeader;
    protected org.visnow.vn.gui.widgets.WidePopupComboBox mappingTypeComboBox;
    protected javax.swing.JPanel mappingTypePanel;
    protected javax.swing.ButtonGroup nodeCellGroup;
    protected javax.swing.JPanel nodeCellPanel;
    protected javax.swing.JRadioButton nodeDataButton;
    protected javax.swing.JPanel ratioPanel;
    protected javax.swing.JButton readTextureButton;
    protected org.visnow.vn.geometries.gui.ColorComponentPanel redComponentPanel;
    protected javax.swing.JPanel rgbPanel;
    protected javax.swing.JFileChooser textureFileChooser;
    protected javax.swing.JPanel textureFilePanel;
    protected javax.swing.JCheckBox textureImageFlipXCB;
    protected javax.swing.JCheckBox textureImageFlipYCB;
    protected javax.swing.JPanel textureImagePanel;
    protected javax.swing.JPanel texturePanel;
    protected org.visnow.vn.geometries.gui.TransparencyEditor.TransparencyEditor transparencyEditor;
    protected org.visnow.vn.gui.widgets.SectionHeader transparencySectionHeader;
    protected org.visnow.vn.geometries.gui.TextureComponentPanel uComponentPanel;
    protected org.visnow.vn.geometries.gui.TextureComponentPanel vComponentPanel;
    // End of variables declaration//GEN-END:variables

}
