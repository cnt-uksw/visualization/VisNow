/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.viewer3d.MoviesManager;

import java.text.DecimalFormat;
import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineStripArray;
import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.Point3d;
import org.jogamp.vecmath.Vector3d;
import org.visnow.vn.geometries.geometryTemplates.Glyph;
import org.visnow.vn.geometries.geometryTemplates.Templates;
import org.visnow.vn.geometries.objects.GeometryObject;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;

/**
 * 
 * This class holds information about a single movie frame.
 * 
 * @author Norbert Kapiński (norkap@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 
 */


public class MovieFrame {

    private final Point3d cameraPosition;
    private final Transform3D cameraTransform;
    private final GeometryObject marker;
    private final String name;
    protected Glyph gt = new Templates.DiamondTemplate(0);
    protected int nvertl, nvertp, nindl, nindp;
    protected int[] stripsl, stripsp, pIndexl, pIndexp, cIndexl, cIndexp;
    protected double[] vertsl, vertsp;
    protected float scale = .1f;
    protected float[] resultColors = {.3f, .6f, 1.f};
    
    
    /**
     * creates movie frame
     * @param cameraTransform camera position and rotation in a current frame
     */
    public MovieFrame(Transform3D cameraTransform){
        initArrays();
        Vector3d p = new Vector3d();
        cameraTransform.get(p);
        DecimalFormat df = new DecimalFormat("0.00##");
        String resultX = df.format(p.x);
        String resultY = df.format(p.y);
        String resultZ = df.format(p.z);
        name = "("+resultX+", "+resultY+", "+resultZ+")";
        marker = new GeometryObject("frame marker - "+name);
        this.cameraPosition = new Point3d(p.x, p.y, p.z);
        this.cameraTransform = cameraTransform;
        createPickPointGeometry();
    }
    
    /**
     * Camera position.
     * @return camera position
     */

    public Point3d getCameraPosition() {
        return cameraPosition;
    }
    
    protected void createPickPointGeometry()
    {
        marker.clearAllGeometry();
        IndexedLineStripArray glyph = new IndexedLineStripArray(nvertp,
                                          GeometryArray.COORDINATES | GeometryArray.COLOR_3,
                                          nindp, stripsp);

            for (int i = 0; i < gt.getNverts(); i++) {
                vertsp[3 * i] = cameraPosition.x + scale * gt.getVerts()[3 * i];
                vertsp[3 * i + 1] = cameraPosition.y + scale * gt.getVerts()[3 * i + 1];
                vertsp[3 * i + 2] = cameraPosition.z + scale * gt.getVerts()[3 * i + 2];
            }
            
        glyph.setColors(0, resultColors);
        glyph.setCoordinates(0, vertsp);
        glyph.setCoordinateIndices(0, pIndexp);
        glyph.setColorIndices(0, cIndexp);
        OpenShape3D pickLine = new OpenShape3D();
        pickLine.addGeometry(glyph);
        OpenBranchGroup pLine = new OpenBranchGroup();
        pLine.addChild(pickLine);
        marker.addNode(pLine);
    }
        
    private void initArrays()
    {
        nvertl = 2 * gt.getNverts() + 2;
        int nstripl = 2 * gt.getNstrips() + 1;
        nindl = 2 * gt.getNinds() + 2;
        stripsl = new int[nstripl];
        for (int i = 0; i < gt.getNstrips(); i++) {
            stripsl[i] = stripsl[i + gt.getNstrips()] = gt.getStrips()[i];
        }
        stripsl[2 * gt.getNstrips()] = 2;
        pIndexl = new int[nindl];
        cIndexl = new int[nindl];
        for (int i = 0; i < gt.getNinds(); i++) {
            pIndexl[i] = gt.getPntsIndex()[i];
            pIndexl[i + gt.getNinds()] = gt.getPntsIndex()[i] + gt.getNverts();
        }
        pIndexl[2 * gt.getNinds()] = 2 * gt.getNverts();
        pIndexl[2 * gt.getNinds() + 1] = 2 * gt.getNverts() + 1;
        for (int i = 0; i < nindl; i++) {
            cIndexl[i] = 0;
        }
        vertsl = new double[3 * nvertl];

        nvertp = gt.getNverts();
        int nstripp = gt.getNstrips();
        nindp = gt.getNinds();
        stripsp = new int[nstripp];
        System.arraycopy(gt.getStrips(), 0, stripsp, 0, gt.getNstrips());
        pIndexp = new int[nindp];
        cIndexp = new int[nindp];
        System.arraycopy(gt.getPntsIndex(), 0, pIndexp, 0, gt.getNinds());
        for (int i = 0; i < nindp; i++) {
            cIndexp[i] = 0;
        }
        vertsp = new double[3 * nvertp];
    }
    
    /**
     * Geometry object to mark camera position in the current frame.
     * @return geometry object that represents camera position in the scene 
     */
    
    public GeometryObject getOutObject()
    {
        return marker;
    }
    
    /**
     * Overrides the toString method.
     * @return camera position in 3D coords.
     */
    @Override
    public String toString(){
        return name;
    }
}
