/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.viewer3d.MoviesManager;

import java.util.ArrayList;
import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineStripArray;
import org.jogamp.vecmath.Point3d;
import org.apache.log4j.Logger;
import org.visnow.vn.geometries.objects.GeometryObject;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;
import org.visnow.vn.lib.utils.curves.Cubic;

/**
 * This class holds and manage information about key frames of a movie.
 * 
 * @author Norbert Kapiński (norkap@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * /**
 * This class holds and manage information about key frames of a movie.
 * 
 * @author Norbert Kapiński (norkap@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 
 * 
 */


public class MoviesManager {

    protected static Logger LOGGER = Logger.getLogger(MoviesManager.class);
    
    private ArrayList<MovieFrame> keyFrames;
    private GeometryObject trajOutObj;
    
    /**
     * This method creates geometry object representing camera trajectory. Trajectory is interpolated based on key frames.
     * @param isClosed if true than the starting point is also the ending point and the trajectory is closed.
     * @param nKnots number of interpolation points on every segment between key frames. Must be greater than 0
     * @return camera trajectory or null if number of key frames is lower than 4
     */
    
    public GeometryObject createTrajectorieGeometry(boolean isClosed, int nKnots)
    {
        if(!(nKnots>0)){
            throw new IllegalArgumentException("Interpolation knots number = "+nKnots+", Interpolation knots number must be greater than 0");
        }

        int nKeyFrames = keyFrames.size();
        if(nKeyFrames<4){
            return null;
        }
        
        Point3d[] vertsl;
        if(isClosed){
            vertsl = new Point3d[nKeyFrames + 1];
            vertsl[nKeyFrames] = new Point3d(keyFrames.get(0).getCameraPosition());
        }else{
            vertsl = new Point3d[nKeyFrames];
        }
        
        for (int i = 0; i < nKeyFrames; i++) {
            vertsl[i] = new Point3d(keyFrames.get(i).getCameraPosition());
        }

        ArrayList<Point3d> interpolatedPoints = interpolateCameraTrajectory(vertsl, nKnots);
        int nFrames = interpolatedPoints.size();
        
        Point3d[] nVertsl = new Point3d[nFrames];
        
        for (int i = 0; i < nFrames; i++) {
            nVertsl[i] = new Point3d(interpolatedPoints.get(i));
        }
        
        int[] pIndexl = new int[2*(nFrames - 1)];
        
        for (int i = 0; i < nFrames - 1; i++) {
            pIndexl[2*i] = i;
            pIndexl[2*i + 1] = i+1;
        }
        
        trajOutObj = new GeometryObject("trajectory");
        
        int[] stripsl = new int[nFrames - 1];
        for (int i = 0; i < stripsl.length; i++) {
            stripsl[i] = 2;
        }

        IndexedLineStripArray glyph = new IndexedLineStripArray(nFrames,
                                          GeometryArray.COORDINATES,
                                          2 * nFrames, stripsl);
        
        glyph.setCoordinates(0, nVertsl);
        glyph.setCoordinateIndices(0, pIndexl);
        OpenShape3D pickLine = new OpenShape3D();
        pickLine.addGeometry(glyph);
        OpenBranchGroup pLine = new OpenBranchGroup();
        pLine.addChild(pickLine);
        trajOutObj.addNode(pLine);
        
        return trajOutObj;
    }
    
    private static ArrayList<Point3d> interpolateCameraTrajectory(Point3d[] points, int nKnots)
    {
        ArrayList<Point3d> newPoints = new ArrayList<>();
        
        int nSplines = points.length - 1;
        
        float[][][] splines = Cubic.calcCubic3D(nSplines, points);
        
        float[][] splinesX = splines[0];
        float[][] splinesY = splines[1];
        float[][] splinesZ = splines[2];
        float t;
        
        for (int i = 0; i < nSplines; i++) {
            for (int j = 0; j < nKnots; j++) {
                t=j/(float)nKnots;
                newPoints.add(new Point3d(Cubic.eval(splinesX[i], t), Cubic.eval(splinesY[i], t), Cubic.eval(splinesZ[i], t)));
            }
        }
        newPoints.add(new Point3d(Cubic.eval(splinesX[nSplines - 1], 1), Cubic.eval(splinesY[nSplines - 1], 1), Cubic.eval(splinesZ[nSplines - 1], 1)));
        
      return newPoints;
    }
    
    /**
     * init key frame list
     */
    public MoviesManager(){
        keyFrames = new ArrayList<>();
    }   
    
    /**
     * adds new frame to the key frames list
     * @param frame new frame
     */
    public void addKeyFrame(MovieFrame frame)
    {
        keyFrames.add(frame);
    }
    /**
     * removes frame from the key frames list
     * @param idx index of the frame to be removed
     */
    public void removeKeyFrame(int idx)
    {
        if(idx>=0 || idx<keyFrames.size()){
            keyFrames.remove(idx);
        }else{
            LOGGER.error(new IndexOutOfBoundsException("idx"+idx+"; size of keyFrames list = "+keyFrames.size()));
        }
    }

    /**
     * Key frames list.
     * @return key frames list 
     */
    
    public ArrayList<MovieFrame> getKeyFrames() {
        return keyFrames;
    }
    
    /**
     * This method returns camera trajectory geometry object
     * @return geometry object representing camera trajectory 
     */
    public GeometryObject getTrajectoryGeometryObject() {
        return trajOutObj;
    }
    
}
