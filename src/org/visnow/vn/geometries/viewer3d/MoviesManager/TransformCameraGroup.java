/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.viewer3d.MoviesManager;

import java.util.ArrayList;
import org.jogamp.java3d.Transform3D;
import org.visnow.vn.geometries.objects.generics.OpenTransformGroup;

/**
 * @author Norbert Kapiński (norkap@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 
 * This class extends the OpenTranfromGroup class to allow addition of the listeners for monitoring transformation change.
 * 
 * 
 */


public class TransformCameraGroup extends OpenTransformGroup{
    
    
    ArrayList<Listener> listeners = new ArrayList<>();
    
    /**
     * creates a new transform camera group
     */
    public TransformCameraGroup(){
        
    }
    
    /**
     * creates a new transform camera group with a specific name
     * @param name specific name for the camera transform group.
     */
    
    public TransformCameraGroup(String name){
        super.setName(name);
    }
    
    /**
     * interface for camera group transform changed listeners
     */
    public interface Listener{
        public void transformCameraGroupChanged(Transform3D newTransform);
    }
    
    /**
     * sets new transform of the transform camera group
     * @param newTransform - new transform
     */
    
    @Override
    public void setTransform(Transform3D newTransform){
        super.setTransform(newTransform);
        fireTransformChanged(newTransform);
    }
    
    
    private void fireTransformChanged(Transform3D newTransform) {
        for (Listener l : listeners)
            l.transformCameraGroupChanged(newTransform);
    }
    
    /**
     * adds a transform change listener to the list
     * @param l listener
     * @return true (as specified by Collection.add)
     */
    
    public boolean addTransformCameraGroupListener(Listener l){
        return listeners.add(l);
    }
    
    /**
     * removes a transform changed listener from the list
     * @param l listener
     * @return true if this list contained the specified element, false otherwise.
     */
    
    public boolean removeTransformCameraGroupListener(Listener l){
        return listeners.remove(l);
    }
    
}
