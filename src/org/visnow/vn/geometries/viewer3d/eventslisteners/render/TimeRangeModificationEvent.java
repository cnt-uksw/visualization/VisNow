/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.viewer3d.eventslisteners.render;

import java.util.*;

/**
 * This event is fired from the AnimationPanel class when the time range is changed by timeRangeSlider
 * <p>
 * @author Jędrzej M. Nowosielski (jnow@icm.edu.pl), University of Warsaw ICM
 */
public class TimeRangeModificationEvent extends EventObject
{
    /**
     * minimum value (of the timeRangeSlider from AnimationPanel)
     */
    private float tMin;

    /**
     * maximum value
     */
    private float tMax;

    /**
     * current low value
     */
    private float tLow;

    /**
     * current high value
     */
    private float tHigh;

    /**
     * Creates a new instance of TimeRangeModificationEvent
     * <p>
     * @param source
     * @param tMin   minimum value (of the timeRangeSlider from AnimationPanel)
     * @param tMax   maximum value
     * @param tLow   current low value
     * @param tHigh  current high value
     * <p>
     */
    public TimeRangeModificationEvent(Object source, float tMin, float tMax, float tLow, float tHigh)
    {
        super(source);
        this.tMin = tMin;
        this.tMax = tMax;
        this.tLow = tLow;
        this.tHigh = tHigh;

    }

    /**
     * Returns minimum value
     * <p>
     * @return minimum value
     */
    public float getTMin()
    {
        return tMin;
    }

    /**
     * Returns maximum value
     * <p>
     * @return maximum value
     */
    public float getTMax()
    {
        return tMax;
    }

    /**
     * Returns current low value
     * <p>
     * @return current low value
     */
    public float getTLow()
    {
        return tLow;
    }

    /**
     * Returns current high value
     * <p>
     * @return current high value
     */
    public float getTHigh()
    {
        return tHigh;
    }

    @Override
    public String toString()
    {
        return "TimeRangeModificationEvent";
    }
}
