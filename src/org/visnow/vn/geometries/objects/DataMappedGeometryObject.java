/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.objects;

import org.visnow.jlargearrays.ByteLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jscic.Field;
import org.visnow.vn.datamaps.colormap1d.DefaultColorMap1D;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.parameters.DataMappingParams;

/**
 *
 * @author Krzysztof S. Nowinski, Warsaw University, ICM
 *
 */
public class DataMappedGeometryObject extends GeometryObject
{

    public static final int SURFACE = 1;
    public static final int EDGES = 2;
    public static final int EXTEDGES = 4;

    protected boolean structureChanged = true;
    protected boolean dataChanged = true;
    protected boolean rangeChanged = true;
    protected boolean isTextureChanged = true;
    protected boolean coordsChanged = true;
    protected boolean colorsChanged = true;
    protected boolean uvCoordsChanged = true;
    protected boolean debug = false;
    protected boolean newParams = true;

    protected long nNodes = 0;
    protected long nTriangleIndices = 0;
    protected long nLineIndices = 0;
    protected IntLargeArray coordIndices = null;
    protected FloatLargeArray coords = null;
    protected FloatLargeArray normals = null;
    protected FloatLargeArray uvData = null;
    protected ByteLargeArray colors = null;
    protected DefaultColorMap1D colorMap = null;
    protected ColormapLegend colormapLegend = new ColormapLegend();

    /**
     * Transient place holder for all geometries created by the module - detached
     * and re-attached for all structural changes
     */
    protected OpenBranchGroup geometry = new OpenBranchGroup("data mapped geom");

    /**
     * Creates a new instance of DataMappedGeometryObject
     */
    public DataMappedGeometryObject()
    {
        dataMappingParams = new DataMappingParams();
        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
        dataMappingParams.getTransparencyParams().addListener(renderingParams.getTransparencyChangeListener());
        addGeometry2D(colormapLegend);
    }

    public DataMappedGeometryObject(DataMappingParams dataMappingParams)
    {
        this.dataMappingParams = dataMappingParams;
        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
        dataMappingParams.getTransparencyParams().addListener(renderingParams.getTransparencyChangeListener());
        addGeometry2D(colormapLegend);
    }

    public DataMappedGeometryObject(String name)
    {
        super(name);
        dataMappingParams = new DataMappingParams();
        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
        dataMappingParams.getTransparencyParams().addListener(renderingParams.getTransparencyChangeListener());
        addGeometry2D(colormapLegend);
    }
    
    public DataMappedGeometryObject(String name, DataMappingParams dataMappingParams)
    {
        super(name);
        this.dataMappingParams = dataMappingParams;
        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
        dataMappingParams.getTransparencyParams().addListener(renderingParams.getTransparencyChangeListener());
        addGeometry2D(colormapLegend);
    }

    public DataMappedGeometryObject(String name, int timestamp)
    {
        super(name, timestamp);
        dataMappingParams = new DataMappingParams();
        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
        dataMappingParams.getTransparencyParams().addListener(renderingParams.getTransparencyChangeListener());
        addGeometry2D(colormapLegend);
    }

    public DataMappedGeometryObject(String name, int timestamp, DataMappingParams dataMappingParams)
    {
        super(name, timestamp);
        this.dataMappingParams = dataMappingParams;
        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
        dataMappingParams.getTransparencyParams().addListener(renderingParams.getTransparencyChangeListener());
        addGeometry2D(colormapLegend);
    }

    public DataMappingParams getDataMappingParams()
    {
        return dataMappingParams;
    }

    public ColormapLegend getColormapLegend()
    {
        return colormapLegend;
    }

    public boolean setField(Field inField)
    {
        return false;
    }
}
