/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.objects;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import org.visnow.jscic.Field;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.CellArray;
import org.jogamp.java3d.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jogamp.vecmath.Color3f;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.geometries.events.ColorEvent;
import org.visnow.vn.geometries.events.ColorListener;
import org.visnow.vn.geometries.objects.generics.*;
import org.visnow.vn.geometries.parameters.*;
import org.visnow.vn.geometries.utils.ColorMapper;
import org.visnow.vn.geometries.utils.TextureMapper;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.*;
import org.visnow.vn.lib.utils.geometry2D.GeometryObject2DStruct;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArraySchema;
import static org.visnow.vn.geometries.objects.generics.ObjectCapabilities.*;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 *
 */
public class CellSetGeometry extends OpenBranchGroup
{

    protected static final boolean[][] resetGeometry
            = {
                {false, true, true, true, true, true, true, true, true},
                {true, false, false, false, false, false, false, false, true},
                {true, false, false, false, false, false, false, false, true},
                {true, false, false, false, false, false, false, false, true},
                {true, false, false, false, false, false, false, false, true},
                {true, false, false, false, false, false, false, false, true},
                {true, false, false, false, false, false, false, false, true},
                {true, false, false, false, false, false, false, false, true},
                {true, true, true, true, true, true, true, true, false}
            };
    protected String name;
    protected boolean ignoreUpdate = false;
    protected Field inField;
    protected CellSet cellSet;
    protected CellArray triangleCellArray = null;
    protected CellArray quadCellArray = null;
    protected CellArray segCellArray = null;
    protected CellArray lastCellEdgesArray = null;
    protected CellArray lastCellSegmentsArray = null;
    protected CellArray pointCellArray = null;
    protected CellArray lastPointCellArray = null;
    protected CellArray lastNodesCellArray = null;

    protected PresentationParams params = null;
    protected RenderingParams renderingParams = null;
    protected DataMappingParams dataMappingParams = null;
    protected TransformParams transformParams = null;
    protected boolean cellSelectionActive = false;
    protected int colorMode = DataMappingParams.UNCOLORED;
    protected int currentColorMode = -1;
    protected int nNodes = 0;
    protected int nCellNodes = 0;
    protected int nCellEdgeNodes = 0;
    protected int nIndices = 0;
    protected int nPointIndices = 0;
    protected int nEdgeIndices = 0;
    protected int nPoints = 0;
    protected int nTriangles = 0;
    protected int nSegments = 0;

    protected int[] coordIndices = null;
    protected int[] normalsIndices = null;
    protected int[] colorIndices = null;

    protected int[] nodeNormalsIndices = null;
    protected int[] nodeColorIndices = null;
    protected int[] cellNormalsIndices = null;
    protected int[] cellColorIndices = null;

    protected int[] coordEdgeIndices = null;
    protected int[] colorEdgeIndices = null;
    protected int[] cellColorEdgeIndices = null;

    protected int[] coordPointIndices = null;
    protected int[] colorPointIndices = null;

    protected float[] coords = null;
    protected float[] cellCoords = null;
    protected float[] cellEdgeCoords = null;
    protected float[] normals = null;
    protected float[] cellNormals = null;
    protected float[] uvData = null;
    protected byte[] colors = null;
    protected float[] cellColors = null;
    protected float[] cellEdgeColors = null;
    protected float[][] extents = {{0, 0, 0}, {0, 0, 0}};
    protected OpenMaterial material;
    protected OpenAppearance appearance = null;
    protected OpenAppearance lineAppearance = null;
    protected OpenAppearance boxLineAppearance = new OpenAppearance();
    protected Texture2D texture = null;
    protected TextureAttributes texAtt = new TextureAttributes();
    protected ColormapLegend colormapLegend = new ColormapLegend();
    protected OpenShape3D surfaceShape = new OpenShape3D();
    protected OpenShape3D lineShape = new OpenShape3D();
    protected OpenShape3D pointShape = new OpenShape3D();
    protected OpenShape3D frameShape = new OpenShape3D();
    protected IndexedTriangleArray triangleArr = null;
    protected IndexedLineArray edgeArr = null;
    protected IndexedPointArray pointArr = null;
    protected IndexedLineStripArray boxArr = null;
    protected OpenBranchGroup geometryGroup = new OpenBranchGroup();
    protected OpenTransformGroup transformGroup = new OpenTransformGroup();
    protected OpenTransparencyAttributes transparencyAttributes = new OpenTransparencyAttributes();
    protected boolean structureChanged = true;
    protected boolean isTextureChanged = true;
    protected boolean coordsChanged = true;
    protected boolean colorsChanged = true;
    protected boolean uvCoordsChanged = true;
    protected int mode = 0;
    protected byte surfaceOrientation = 1;
    protected byte lastSurfaceOrientation = 1;
    protected boolean[] shownPoints = null;
    protected boolean[] shownTriangles = null;
    protected boolean[] shownQuads = null;
    protected boolean[] shownSegments = null;
    protected boolean isPicked = false;
    protected Color3f bgrColor = new Color3f(0, 0, 0);
    protected int[] timeRange = null;
    protected float currentT = 0;
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CellSetGeometry.class);
    protected GeometryObject2DStruct outObj2DStruct = new GeometryObject2DStruct();
    protected LocalToWindow localToWindow = null;

    protected RenderEventListener renderEventListener = new RenderEventListener()
    {
        @Override
        public void renderExtentChanged(RenderEvent e)
        {
            if (ignoreUpdate)
                return;
            int extent = e.getUpdateExtent();
            int cMode = dataMappingParams.getColorMode();
            if (renderingParams.getDisplayMode() == RenderingParams.BACKGROUND)
                cMode = DataMappingParams.UNCOLORED;
            if (currentColorMode < 0) {
                currentColorMode = cMode;
                updateGeometry();
                return;
            }
            if (extent == RenderEvent.COLORS ||
                    extent == RenderEvent.TRANSPARENCY ||
                    extent == RenderEvent.TEXTURE ||
                    extent == RenderEvent.DATA_MAP) {
                validateColorMode();
                if (resetGeometry[currentColorMode][cMode])
                    updateGeometry();
                else if (colorMode == DataMappingParams.UVTEXTURED)
                    updateTextureCoords();
                else
                    updateColors();
                currentColorMode = cMode;
                return;
            }
            if (extent == RenderEvent.COORDS)
                updateCoords();
            if (extent == RenderEvent.GEOMETRY)
                updateGeometry();
            currentColorMode = cMode;
        }
    };

    public CellSetGeometry(String name)
    {
        this.name = name;
        setName(name);
        geometryGroup.addChild(surfaceShape);
        geometryGroup.addChild(lineShape);
        geometryGroup.addChild(pointShape);
        geometryGroup.addChild(frameShape);
        transformGroup.addChild(geometryGroup);
        addChild(transformGroup);
        transformGroup.setPickable(true);
        transparencyAttributes.setTransparencyMode(TransparencyAttributes.NICEST);
        transparencyAttributes.setTransparency(.5f);
        outObj2DStruct.setName(name);
        boxLineAppearance.setLineAttributes(new LineAttributes(1.5f, LineAttributes.PATTERN_SOLID, true));
    }

    public void setIgnoreUpdate(boolean ignoreUpdate)
    {
        this.ignoreUpdate = ignoreUpdate;
    }

    protected void createBoundaryCellNormals()
    {
        if (cellSet == null || cellSet.getBoundaryCellArrays() == null)
            return;
        for (CellArray ar : cellSet.getBoundaryCellArrays())
            if (ar != null && ar.getNCells() > 0 && ar.getDim() == 2)
                ar.computeCellNormals(new FloatLargeArray(coords));
    }

    protected void createBoundaryNodeNormals()
    {
        normals = new float[3 * (int)nNodes];

        for (int m = Cell.getNProperCellTypesUpto1D(); m < Cell.getNProperCellTypesUpto2D(); m++) {
            CellArray ar = cellSet.getBoundaryCellArray(CellType.getType(m));
            if (ar == null)
                continue;
            float[] cNormals = ar.getCellNormals().getData();
            for (int i = 0; i < ar.getNCells(); i++) {
                try {
                    int[] cellNodes = ar.getNodes(i);
                    for (int j = 0; j < cellNodes.length; j++) {
                        int k = cellNodes[(int) j];
                        for (int l = 0; l < 3; l++)
                            normals[3 * k + l] = normals[3 * k + l] + cNormals[3 * i + l];
                    }
                } catch (Exception e) {
                }
            }
        }
        for (int i = 0; i < nNodes; i++) {
            double d = normals[3 * i] * normals[3 * i] + normals[3 * i + 1] * normals[3 * i + 1] + normals[3 * i + 2] * normals[3 * i + 2];
            if (d == 0)
                normals[3 * i] = 1;
            else {
                float r = (float) (sqrt(d));
                for (int j = 0; j < 3; j++)
                    normals[3 * i + j] /= r;
            }
        }
    }

    public void setInData(IrregularField inField, CellSet cellSet)
    {
        if (inField == null || cellSet == null)
            return;
        coordsChanged = this.inField == null;
        coords = inField.getCurrentCoords().getData();
        extents = inField.getPreferredExtents();
        this.inField = inField;
        structureChanged = (this.cellSet == null || !cellSet.isStructCompatibleWith(this.cellSet));
        if (structureChanged) {
            coordIndices = null;
            normalsIndices = null;
            colorIndices = null;

            nodeNormalsIndices = null;
            nodeColorIndices = null;
            cellNormalsIndices = null;
            cellColorIndices = null;

            coordEdgeIndices = null;
            colorEdgeIndices = null;

            coordPointIndices = null;
            colorPointIndices = null;
        }
        this.cellSet = cellSet;
        nNodes = (int) inField.getNNodes();
        if (inField.hasCells2D() || inField.hasCells3D()) {
            createBoundaryCellNormals();
            createBoundaryNodeNormals();
        }
    }

    public void setPresentationParams(PresentationParams params)
    {
        this.params = params;
        dataMappingParams = params.getDataMappingParams();
        renderingParams = params.getRenderingParams();
    }

    /**
     * Edges with dihedral angles below threshold level are removed from display,
     * nTriangles, nSegments, nPoints and n*Indices are updated.
     */
    protected void setDisplayedItemsCount()
    {
        nTriangles = 0;
        nSegments = 0;
        nPoints = 0;
        nIndices = nEdgeIndices = nPointIndices = 0;

        if (triangleCellArray != null)
            nTriangles += triangleCellArray.getNCells();
        if (quadCellArray != null)
            nTriangles += 2 * quadCellArray.getNCells();
        if (segCellArray != null) {
            shownSegments = new boolean[segCellArray.getNCells()];
            float featureAngle = renderingParams.getMinEdgeDihedral();
            float[] edgeAngles = segCellArray.getCellDihedrals();
            if (edgeAngles != null)
                for (int i = 0; i < segCellArray.getNCells(); i++) {
                    shownSegments[i] = edgeAngles[i] >= featureAngle;
                    if (shownSegments[i])
                        nSegments += 1;
                }
            else {
                for (int i = 0; i < segCellArray.getNCells(); i++)
                    shownSegments[i] = true;
                nSegments = segCellArray.getNCells();
            }
        }
        if (pointCellArray != null) {
            shownPoints = new boolean[pointCellArray.getNCells()];
            for (int i = 0; i < shownPoints.length; i++) {
                shownPoints[i] = true;
            }
            nPoints = pointCellArray.getNCells();
        }
        nIndices = 3 * nTriangles;
        nEdgeIndices = 2 * nSegments;
        nPointIndices = nPoints;
    }

    protected void generate2DNodeIndices()
    {
        if (nIndices > 0) {
            coordIndices = new int[nIndices];
        } else {
            coordIndices = null;
            return;
        }
        int k = 0;
        int[] nodes;
        byte[] orientations;
        if (triangleCellArray != null) {
            int nCells = triangleCellArray.getNCells();
            nodes = triangleCellArray.getNodes();
            orientations = triangleCellArray.getOrientations();
            for (int i = 0; i < nCells; i++) {
                try {
                    coordIndices[k] = nodes[3 * i];
                    if (orientations[i] == surfaceOrientation) {
                        coordIndices[k + 1] = nodes[3 * i + 1];
                        coordIndices[k + 2] = nodes[3 * i + 2];
                    } else {
                        coordIndices[k + 1] = nodes[3 * i + 2];
                        coordIndices[k + 2] = nodes[3 * i + 1];
                    }
                    k += 3;
                } catch (Exception e) {
                    log.error("" + k + "<" + coordIndices.length + "  " + (3 * i) + "<" + nodes.length);
                }
            }
        }
        if (quadCellArray != null) {
            nodes = quadCellArray.getNodes();
            orientations = quadCellArray.getOrientations();
            for (int i = 0; i < quadCellArray.getNCells(); i++) {
                coordIndices[k] = nodes[4 * i];
                coordIndices[k + 3] = nodes[4 * i];
                if (orientations[i] == surfaceOrientation) {
                    coordIndices[k + 1] = nodes[4 * i + 1];
                    coordIndices[k + 2] = nodes[4 * i + 2];
                    coordIndices[k + 4] = nodes[4 * i + 2];
                    coordIndices[k + 5] = nodes[4 * i + 3];
                } else {
                    coordIndices[k + 1] = nodes[4 * i + 2];
                    coordIndices[k + 2] = nodes[4 * i + 1];
                    coordIndices[k + 4] = nodes[4 * i + 3];
                    coordIndices[k + 5] = nodes[4 * i + 2];
                }
                k += 6;
            }
        }
    }

    protected void generate2DCellColorIndices()
    {
        if (nIndices == 0)
            return;
        cellColorIndices = new int[nIndices];
        int[] cindices;
        int k = 0;

        if (triangleCellArray != null) {
            if (triangleCellArray.getDataIndices() == null)
                throw new RuntimeException("data indices are missing, perphaps you've added dataArray without indices?");

            cindices = triangleCellArray.getDataIndices();
            for (int i = 0; i < triangleCellArray.getNCells(); i++)
                //                if (!cellSelectionActive || shownTriangles[i])
                for (int j = 0; j < 3; j++, k++)
                    cellColorIndices[k] = cindices[i];
        }

        if (quadCellArray != null) {
            if (quadCellArray.getDataIndices() == null)
                throw new RuntimeException("data indices are missing, perphaps you've added dataArray without indices?");
            cindices = quadCellArray.getDataIndices();
            for (int i = 0; i < quadCellArray.getNCells(); i++)
                //                if (!cellSelectionActive || shownQuads[i])
                for (int j = 0; j < 6; j++, k++)
                    cellColorIndices[k] = cindices[i];
        }
    }

    protected void generate2DCellNormalsData()
    {
        int n2DCells = 0;
        if (nTriangles != 0) {
            nCellNodes = 3 * nTriangles;
            cellNormalsIndices = new int[nIndices];
            int k = 0, ni = 0;
            if (triangleCellArray != null) {
                n2DCells += triangleCellArray.getNCells();
                for (int i = 0; i < triangleCellArray.getNCells(); i++, ni++)
                    for (int j = 0; j < 3; j++, k++)
                        cellNormalsIndices[k] = ni;
            }
            if (quadCellArray != null) {
                n2DCells += quadCellArray.getNCells();
                for (int i = 0; i < quadCellArray.getNCells(); i++, ni++)
                    for (int j = 0; j < 6; j++, k++)
                        cellNormalsIndices[k] = ni;
            }
        }
        if (n2DCells > 0)
            cellNormals = new float[3 * n2DCells];
        else
            cellNormals = null;
    }

    protected void generate1DNodeIndices()
    {
        if (nSegments > 0) {
            coordEdgeIndices = new int[nEdgeIndices];
            int[] nodes = segCellArray.getNodes();
            int k = 0;
            for (int i = 0; i < segCellArray.getNCells(); i++)
                if (shownSegments[i]) {
                    coordEdgeIndices[k] = nodes[2 * i];
                    coordEdgeIndices[k + 1] = nodes[2 * i + 1];
                    k += 2;
                }
        }
    }

    protected void generate1DCellColorIndices()
    {
        if (segCellArray == null)
            return;
        int[] dataIndices = segCellArray.getDataIndices();
        if (dataIndices == null)
            return;
        if (nSegments == 0)
            return;
        cellColorEdgeIndices = new int[nEdgeIndices];
        for (int i = 0, k = 0; i < segCellArray.getNCells(); i++)
            if (shownSegments[i]) {
                cellColorEdgeIndices[k] = cellColorEdgeIndices[k + 1] = dataIndices[i];
                k += 2;
            }
    }

    protected void generate0DNodeIndices()
    {
        if (nPoints > 0) {
            coordPointIndices = new int[nPointIndices];
            int[] nodes = pointCellArray.getNodes();
            int k = 0;
            for (int i = 0; i < pointCellArray.getNCells(); i++)
                if (shownPoints[i]) {
                    coordPointIndices[k] = nodes[i];
                    k += 1;
                }
        }
    }

    protected void generate0DCellColorIndices()
    {
        if (nPoints > 0) {
            int[] dataIndices = pointCellArray.getDataIndices();
            if (dataIndices == null || nPoints == 0)
                return;
            colorPointIndices = new int[nPoints];
            for (int i = 0, k = 0; i < pointCellArray.getNCells(); i++)
                if (shownPoints[i]) {
                    colorPointIndices[k] = dataIndices[i];
                    k += 1;
                }
        }
    }

    protected void updateIndices()
    {
        if ((renderingParams.getDisplayMode() & RenderingParams.SURFACE) != 0) {
            if (coordIndices == null)
                generate2DNodeIndices();

            if ((renderingParams.getShadingMode() == RenderingParams.GOURAUD_SHADED) && normalsIndices == null)
                normalsIndices = coordIndices;
            if ((renderingParams.getShadingMode() == RenderingParams.FLAT_SHADED) && cellNormalsIndices == null)
                generate2DCellNormalsData();
            if (dataMappingParams.isCellDataMapped() && cellColorIndices == null)
                generate2DCellColorIndices();
        }

        if ((renderingParams.getDisplayMode() & (RenderingParams.SEGMENT_CELLS | RenderingParams.EDGES)) != 0) {
            if (coordEdgeIndices == null || segCellArray != lastCellSegmentsArray ||
                    nEdgeIndices != coordEdgeIndices.length)
                generate1DNodeIndices();
            if (dataMappingParams.isCellDataMapped() && (cellColorEdgeIndices == null || segCellArray != lastCellEdgesArray))
                generate1DCellColorIndices();
            lastCellEdgesArray = segCellArray;
            lastCellSegmentsArray = segCellArray;
        }

        if ((renderingParams.getDisplayMode() & (RenderingParams.POINT_CELLS | RenderingParams.NODES)) != 0) {
            if (coordPointIndices == null || pointCellArray != lastPointCellArray)
                generate0DNodeIndices();
            if (dataMappingParams.isCellDataMapped() && (colorPointIndices == null || pointCellArray != lastPointCellArray))
                generate0DCellColorIndices();
            lastPointCellArray = pointCellArray;
        }
    }

    protected void generateTriangles()
    {
        if (nTriangles == 0)
            return;

        boolean useCoordsIndicesOnly = true;

        if (renderingParams.getDisplayMode() != RenderingParams.BACKGROUND &&
                dataMappingParams.getColorMode() != DataMappingParams.UNCOLORED &&
                dataMappingParams.getColorMode() != DataMappingParams.UVTEXTURED &&
                dataMappingParams.isCellDataMapped()) {
            colorIndices = cellColorIndices;
            useCoordsIndicesOnly = false;
        } else
            colorIndices = coordIndices;

        if (renderingParams.getShadingMode() == RenderingParams.FLAT_SHADED) {
            normalsIndices = cellNormalsIndices;
            useCoordsIndicesOnly = false;
        } else
            normalsIndices = coordIndices;

        int vertexFormat = GeometryArray.COORDINATES | GeometryArray.BY_REFERENCE;
        if (renderingParams.getShadingMode() == RenderingParams.GOURAUD_SHADED ||
                renderingParams.getShadingMode() == RenderingParams.FLAT_SHADED) {
            vertexFormat |= GeometryArray.NORMALS;
        }
        if (renderingParams.getShadingMode() != RenderingParams.BACKGROUND) {
            if (colorMode == DataMappingParams.COLORMAPPED || colorMode == DataMappingParams.RGB)
                vertexFormat |= GeometryArray.COLOR_4;
            if (colorMode == DataMappingParams.UVTEXTURED)
                vertexFormat |= GeometryArray.TEXTURE_COORDINATE_2;
        }
        if (useCoordsIndicesOnly)
            vertexFormat |= GeometryArray.USE_COORD_INDEX_ONLY;
        triangleArr = new IndexedTriangleArray((int) nNodes, vertexFormat, nIndices);

        setStandardCapabilities(triangleArr);
        setIndexingCapabilities(triangleArr);
        if ((triangleArr.getVertexFormat() & GeometryArray.COLOR_4) != 0 && !useCoordsIndicesOnly)
            triangleArr.setColorIndices(0, colorIndices);
        if ((triangleArr.getVertexFormat() & (GeometryArray.TEXTURE_COORDINATE_2 | GeometryArray.TEXTURE_COORDINATE_3)) != 0) {
            if (!useCoordsIndicesOnly)
                triangleArr.setTextureCoordinateIndices(0, 0, coordIndices);
        }
        if (renderingParams.getShadingMode() == RenderingParams.FLAT_SHADED)
            triangleArr.setNormalIndices(0, cellNormalsIndices);
        else if (renderingParams.getShadingMode() == RenderingParams.GOURAUD_SHADED &&
                !useCoordsIndicesOnly)
            triangleArr.setNormalIndices(0, coordIndices);

        int iMax = 0;
        for (int i = 0; i < coordIndices.length; i++) {
            if (coordIndices[i] > iMax)
                iMax = coordIndices[i];
            if (coordIndices[i] < 0)
                coordIndices[i] = 0;
        }
        triangleArr.setCoordinateIndices(0, coordIndices);
    }

    protected void generateEdges()
    {
        edgeArr = null;
        if (nSegments == 0)
            return;
        edgeArr = new IndexedLineArray((int) nNodes,
                                       GeometryArray.COORDINATES |
                                       GeometryArray.USE_COORD_INDEX_ONLY |
                                       GeometryArray.BY_REFERENCE,
                                       nEdgeIndices);
        setStandardCapabilities(edgeArr);
        setIndexingCapabilities(edgeArr);
        edgeArr.setCoordinateIndices(0, coordEdgeIndices);
    }

    protected void generateNodeColoredEdges()
    {
        edgeArr = null;
        if (nSegments == 0)
            return;
        edgeArr = new IndexedLineArray((int) nNodes,
                                       GeometryArray.COORDINATES |
                                       GeometryArray.COLOR_4 |
                                       GeometryArray.USE_COORD_INDEX_ONLY |
                                       GeometryArray.BY_REFERENCE,
                                       nEdgeIndices);
        setStandardCapabilities(edgeArr);
        setIndexingCapabilities(edgeArr);
        edgeArr.setCoordinateIndices(0, coordEdgeIndices);
    }

    protected void generateCellColoredEdges()
    {
        edgeArr = null;
        nSegments = segCellArray.getNCells();
        coordEdgeIndices = segCellArray.getNodes();
        edgeArr = new IndexedLineArray((int) nNodes,
                                       GeometryArray.COORDINATES |
                                       GeometryArray.COLOR_4 |
                                       GeometryArray.BY_REFERENCE,
                                       nEdgeIndices);
        setStandardCapabilities(edgeArr);
        setIndexingCapabilities(edgeArr);
        edgeArr.setColorIndices(0, cellColorEdgeIndices);
        edgeArr.setCoordinateIndices(0, coordEdgeIndices);
    }

    protected void generatePoints()
    {
        if (nPoints > 0) {
            pointCellArray = null;
            if ((mode & RenderingParams.POINT_CELLS) != 0)
                pointCellArray = cellSet.getCellArray(CellType.POINT);
            if ((mode & RenderingParams.NODES) != 0)
                pointCellArray = cellSet.getBoundaryCellArray(CellType.POINT);
            if (colorMode == DataMappingParams.UNCOLORED)
                pointArr = new IndexedPointArray(nPoints,
                                                 GeometryArray.COORDINATES |
                                                 GeometryArray.USE_COORD_INDEX_ONLY |
                                                 GeometryArray.BY_REFERENCE, nPoints);
            else
                pointArr = new IndexedPointArray(nPoints,
                                                 GeometryArray.COORDINATES |
                                                 GeometryArray.COLOR_4 |
                                                 GeometryArray.USE_COORD_INDEX_ONLY |
                                                 GeometryArray.BY_REFERENCE, nPoints);
            setStandardCapabilities(pointArr);
            pointArr.setCoordinateIndices(0, coordPointIndices);
        }
    }

    public void updateCoords(float[] crds)
    {
        if (crds.length == 3 * nNodes)
            coords = crds;
        else {
            int nSp = (int) (crds.length / nNodes);
            coords = new float[3 * (int)inField.getNNodes()];
            for (int i = 0; i < inField.getNNodes(); i++)
                for (int j = 0; j < nSp; j++)
                    coords[3 * i + j] = crds[nSp * i + j];
        }
        updateCoords(true);
    }

    public void updateCoords()
    {
        updateCoords(!ignoreUpdate);
    }

    public void updateCoords(boolean force)
    {
        if (!force || coords == null)
            return;
        boolean detach = this.postdetach();
        if (triangleArr != null) {
            triangleArr.setCoordRefFloat(coords);
            if ((triangleArr.getVertexFormat() & GeometryArray.NORMALS) != 0) {
                if (renderingParams.getShadingMode() == RenderingParams.FLAT_SHADED) {
                    float[] cNormals;
                    int k = 0;
                    if (triangleCellArray != null) {
                        cNormals = triangleCellArray.getCellNormals().getData();
                        if (cNormals == null) {
                            createBoundaryCellNormals();
                            cNormals = triangleCellArray.getCellNormals().getData();
                        }
                        for (int i = 0; i < triangleCellArray.getNCells(); i++)
                            if (!cellSelectionActive || shownTriangles[i])
                                for (int l = 0; l < 3; l++, k++)
                                    cellNormals[k] = cNormals[3 * (int)i + l];
                    }
                    if (quadCellArray != null) {
                        cNormals = quadCellArray.getCellNormals().getData();
                        if (cNormals == null) {
                            createBoundaryCellNormals();
                            cNormals = quadCellArray.getCellNormals().getData();
                        }
                        for (int i = 0; i < quadCellArray.getNCells(); i++)
                            if (!cellSelectionActive || shownQuads[i])
                                for (int l = 0; l < 3; l++, k++)
                                    cellNormals[k] = cNormals[3 * i + l];
                    }
                    triangleArr.setNormalRefFloat(cellNormals);
                } else
                    triangleArr.setNormalRefFloat(normals);
            }
        }
        if (edgeArr != null)
            edgeArr.setCoordRefFloat(coords);
        if (pointArr != null)
            pointArr.setCoordRefFloat(coords);
        if (detach)
            this.postattach();
    }

    protected void updateTextureCoords()
    {
        if (triangleArr == null ||
                (triangleArr.getVertexFormat() & (GeometryArray.ALLOW_TEXCOORD_WRITE |
                GeometryArray.TEXTURE_COORDINATE_2 |
                GeometryArray.TEXTURE_COORDINATE_3 |
                GeometryArray.TEXTURE_COORDINATE_4)) == 0)
            return;
        boolean detach = this.postdetach();

        uvData = new float[2 * nNodes];
        ColorComponentParams[] tParams = new ColorComponentParams[]{
            dataMappingParams.getUParams(), dataMappingParams.getVParams()
        };
        for (int i = 0; i < tParams.length; i++)
            if (tParams[i].getDataComponentSchema() != null)
                uvData = TextureMapper.map(inField, tParams[i], uvData, i);
        triangleArr.setTexCoordRefFloat(0, uvData);
        if (detach)
            this.postattach();
    }

    private class MapColors implements Runnable
    {

        int nThreads = 1;
        int iThread = 0;
        int nNds = (int) dataContainer.getNElements();

        public MapColors(int nThreads, int iThread)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
        }

        @Override
        public void run()
        {
            int kstart = (nNds * iThread) / nThreads;
            int kend = (nNds * (iThread + 1)) / nThreads;
            ColorMapper.map(dataContainer, dataMappingParams, kstart, kend, kstart, dataMappingParams.getDefaultColor(), colors);
            ColorMapper.mapTransparency(dataContainer, dataMappingParams.getTransparencyParams(), kstart, kend, colors);
            if (timeRange != null)
                ColorMapper.mapTimeValidityTransparency(timeRange, currentT, kstart, kend, kstart, colors);
        }
    }

    private DataContainer dataContainer = null;

    /**
     * Is responsible for drawing data.
     */
    public void updateColors()
    {
        boolean detach = this.postdetach();
        timeRange = null;
        dataContainer = dataMappingParams.getDataContainer();
        if (colors == null || colors.length != 4 * dataContainer.getNElements())
            colors = new byte[4 * (int) dataContainer.getNElements()];
        material.setAmbientColor(dataMappingParams.getDefaultColor());
        material.setDiffuseColor(dataMappingParams.getDefaultColor());
        material.setSpecularColor(dataMappingParams.getDefaultColor());
        appearance.getColoringAttributes().setColor(dataMappingParams.getDefaultColor());
        // colors array will be filled in threads started below

        for (DataArray da : dataContainer.getComponents())
            if (da.getUserData() != null &&
                    da.getUserData().length == 1 &&
                    "valid time range".equalsIgnoreCase(da.getUserData()[0])) {
                timeRange = da.getRawIntArray().getData();
                currentT = dataContainer.getCurrentTime();
            }
        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new MapColors(nThreads, iThread));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {

            }

        if (triangleArr != null &&
                renderingParams.getShadingMode() != RenderingParams.BACKGROUND &&
                (colorMode == DataMappingParams.COLORMAPPED ||
                colorMode == DataMappingParams.RGB)) {
            if ((triangleArr.getVertexFormat() & (GeometryArray.COLOR_3 | GeometryArray.COLOR_4)) == 0)
                generateTriangles();
            try {
                triangleArr.setColorRefByte(colors);
            } catch (Exception e) {
            }
        }
        if (edgeArr != null &&
                (colorMode == DataMappingParams.COLORMAPPED ||
                colorMode == DataMappingParams.RGB)) {
            // if edges don't have (RGB) or (ARGB), create colored edges
            if ((edgeArr.getVertexFormat() & (GeometryArray.COLOR_3 | GeometryArray.COLOR_4)) == 0) {
                if (dataMappingParams.isCellDataMapped())
                    generateCellColoredEdges();
                else
                    generateNodeColoredEdges();
            }
            try {
                edgeArr.setColorRefByte(colors);
            } catch (Exception e) {
            }
        }
        if (pointArr != null &&
                (colorMode == DataMappingParams.COLORMAPPED ||
                colorMode == DataMappingParams.RGB) && !dataMappingParams.isCellDataMapped())
            pointArr.setColorRefByte(colors);
        if (detach)
            this.postattach();
    }

    protected void updateFixedColor()
    {
        appearance.getColoringAttributes().setColor(dataMappingParams.getDefaultColor());

        material.setDiffuseColor(dataMappingParams.getDefaultColor());
        material.setAmbientColor(dataMappingParams.getDefaultColor());
        material.setEmissiveColor(dataMappingParams.getDefaultColor());
    }

    public void updateAppearance()
    {
        appearance.getLineAttributes().setLineWidth(renderingParams.getLineThickness());
        appearance.getLineAttributes().setLinePattern(renderingParams.getLineStyle());
        appearance.getPointAttributes().setPointSize(renderingParams.getLineThickness());
        appearance.getPolygonAttributes().setBackFaceNormalFlip(renderingParams.isLightedBackside());
    }

    public void updateTexture()
    {
        if (ignoreUpdate)
            return;
        boolean detach = this.postdetach();
        texture = dataMappingParams.getTexture();
        if (texture != null && dataMappingParams.getColorMode() == DataMappingParams.UVTEXTURED) {
            appearance.setTexture(texture);
        }
        updateTextureCoords();
        if (detach)
            this.postattach();
    }

    public void updataDataMap()
    {
        boolean detach = this.postdetach();
        if (dataMappingParams.getColorMode() == DataMappingParams.COLORMAPPED ||
                dataMappingParams.getColorMode() == DataMappingParams.COLORMAPPED2D ||
                dataMappingParams.getColorMode() == DataMappingParams.COLORED)
            updateColors();
        else if (dataMappingParams.getColorMode() == DataMappingParams.UVTEXTURED)
            updateTexture();
        if (detach)
            this.postattach();
    }

    private void validateColorMode()
    {
        colorMode = dataMappingParams.getColorMode();
        dataContainer = inField;
        ArrayList<DataArraySchema> componentSchemas = dataContainer.getSchema().getComponentSchemas();
        ArrayList<DataArraySchema> pseudoCmpSchemas = dataContainer.getSchema().getPseudoComponentSchemas();
        if (dataMappingParams.isCellDataMapped())
            dataContainer = cellSet;
        // check if color mode and selected components combination is valid; fall back to UNCOLORED otherwise
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
                if (dataContainer.getComponent(dataMappingParams.getColorMap0().getDataComponentName()) != null)
                    return;
                break;
            case DataMappingParams.RGB:
                if (dataContainer.getComponent(dataMappingParams.getRedParams().getDataComponentName()) != null ||
                        dataContainer.getComponent(dataMappingParams.getGreenParams().getDataComponentName()) != null ||
                        dataContainer.getComponent(dataMappingParams.getBlueParams().getDataComponentName()) != null)
                    return;
                break;
            case DataMappingParams.UVTEXTURED:
                if ((componentSchemas.contains(dataMappingParams.getUParams().getDataComponentSchema()) ||
                        pseudoCmpSchemas.contains(dataMappingParams.getUParams().getDataComponentSchema())) &&
                        (componentSchemas.contains(dataMappingParams.getUParams().getDataComponentSchema()) ||
                        pseudoCmpSchemas.contains(dataMappingParams.getUParams().getDataComponentSchema())))
                    return;
                break;
        }
        colorMode = DataMappingParams.UNCOLORED;
    }

    public void updateGeometry()
    {
        if (cellSet == null || params == null || ignoreUpdate)
            return;
        if (renderingParams.getShadingMode() == RenderingParams.BACKGROUND &&
                (mode & (RenderingParams.EDGES | RenderingParams.SEGMENT_CELLS |
                RenderingParams.NODES | RenderingParams.POINT_CELLS)) == 0) {
            if (cellSet.getCellArray(CellType.SEGMENT) != null)
                mode |= RenderingParams.SEGMENT_CELLS;
            else
                mode |= RenderingParams.EDGES;
        }
        dataMappingParams.setParentObjectSize((int) nNodes);
        boolean detach = this.postdetach();
        mode = renderingParams.getDisplayMode();
        if (isPicked && params.getPickIndicator() == PresentationParams.EDG_PICK_INDICATOR)
            mode |= RenderingParams.EDGES;
        if (isPicked && params.getPickIndicator() == PresentationParams.SRF_PICK_INDICATOR)
            mode |= RenderingParams.SURFACE;
        pointCellArray = null;
        if ((mode & RenderingParams.POINT_CELLS) != 0)
            pointCellArray = cellSet.getCellArray(CellType.POINT);
        if ((mode & RenderingParams.NODES) != 0)
            pointCellArray = cellSet.getBoundaryCellArray(CellType.POINT);
        segCellArray = null;
        if ((mode & RenderingParams.SEGMENT_CELLS) != 0)
            segCellArray = cellSet.getCellArray(CellType.SEGMENT);
        if ((mode & RenderingParams.EDGES) != 0)
            segCellArray = cellSet.getBoundaryCellArray(CellType.SEGMENT);
        triangleCellArray = null;
        quadCellArray = null;
        if ((mode & RenderingParams.SURFACE) != 0 || renderingParams.getShadingMode() == RenderingParams.BACKGROUND) {
            triangleCellArray = cellSet.getBoundaryCellArray(CellType.TRIANGLE);
            quadCellArray = cellSet.getBoundaryCellArray(CellType.QUAD);
        }

//        if (segCellArray == null && triangleCellArray == null && quadCellArray == null && pointCellArray == null) {
//            System.out.println("[CellSetGeometry.updateGeometry()]: boundary cell arrays are empty, " +
//                    "nothing to visualize (tip: try calling generateExternFaces() on your cell set)");
//            if (detach)
//                this.postattach();
//            return;
//        }
        outObj2DStruct.removeAllChildren();
        outObj2DStruct.setName("irregular geometry");

        setDisplayedItemsCount();
        if (nIndices + nEdgeIndices + nPointIndices == 0) {
            if (detach)
                this.postattach();
            if (surfaceShape.numGeometries() > 0)
                surfaceShape.removeAllGeometries();
            if (lineShape.numGeometries() > 0)
                lineShape.removeAllGeometries();
            if (pointShape.numGeometries() > 0)
                pointShape.removeAllGeometries();
            if (frameShape.numGeometries() > 0)
                frameShape.removeAllGeometries();
            return;
        }

        surfaceOrientation = renderingParams.getSurfaceOrientation();

        if (nIndices != 0) {
            if (surfaceOrientation != lastSurfaceOrientation) {
                if (cellNormals != null)
                    for (int i = 0; i < cellNormals.length; i++)
                        cellNormals[i] = -cellNormals[i];
                if (normals != null)
                    for (int i = 0; i < normals.length; i++)
                        normals[i] = -normals[i];
                lastSurfaceOrientation = surfaceOrientation;
            }
        }

        if (dataMappingParams.getColorModeChanged() > 0)
            validateColorMode();
        coords = inField.getCurrentCoords().getData();
        updateIndices();
        if (structureChanged ||
                dataMappingParams.getModeChanged() > 0 ||
                dataMappingParams.getColorModeChanged() > 1) {
            if (surfaceShape.numGeometries() > 0)
                surfaceShape.removeAllGeometries();
            if (lineShape.numGeometries() > 0)
                lineShape.removeAllGeometries();
            if (pointShape.numGeometries() > 0)
                pointShape.removeAllGeometries();
            if (frameShape.numGeometries() > 0)
                frameShape.removeAllGeometries();
            if (material == null) {
                material = new OpenMaterial();
            }
            material.setAmbientColor(dataMappingParams.getDefaultColor());
            material.setDiffuseColor(dataMappingParams.getDefaultColor());
            material.setSpecularColor(dataMappingParams.getDefaultColor());

            lineAppearance.getColoringAttributes().setColor(dataMappingParams.getDefaultColor());

            if (renderingParams.getShadingMode() == RenderingParams.BACKGROUND) {
                appearance.getColoringAttributes().setColor(bgrColor);
                material.setDiffuseColor(bgrColor);
                material.setAmbientColor(bgrColor);
            } else
                appearance.getColoringAttributes().setColor(dataMappingParams.getDefaultColor());
            texture = dataMappingParams.getTexture();
            if (texture != null && dataMappingParams.getColorMode() == DataMappingParams.UVTEXTURED)
                appearance.setTexture(texture);
            else
                appearance.setTexture(null);
            lineShape.setAppearance(lineAppearance);
            frameShape.setAppearance(lineAppearance);
            pointShape.setAppearance(lineAppearance);
            surfaceShape.setAppearance(appearance);
            triangleArr = null;
            edgeArr = null;
            pointArr = null;
            if (triangleCellArray != null || quadCellArray != null) {
                generateTriangles();
                if (triangleArr != null)
                    try {
                        surfaceShape.addGeometry(triangleArr);
                    } catch (Exception e) {
                    }
            }
            if (segCellArray != null) {
                if (dataMappingParams.isCellDataMapped())
                    switch (colorMode) {
                        case DataMappingParams.COLORMAPPED:
                        case DataMappingParams.RGB:
                            generateCellColoredEdges();
                            break;
                        case DataMappingParams.UVTEXTURED:
                        case DataMappingParams.UNCOLORED:
                            generateEdges();
                            break;
                    }
                else
                    switch (colorMode) {
                        case DataMappingParams.COLORMAPPED:
                        case DataMappingParams.RGB:
                            generateNodeColoredEdges();
                            break;
                        case DataMappingParams.UVTEXTURED:
                        case DataMappingParams.UNCOLORED:
                            generateEdges();
                            break;
                    }
                if (edgeArr != null)
                    lineShape.addGeometry(edgeArr);
            }
            if (pointCellArray != null) {
                generatePoints();
                pointShape.addGeometry(pointArr);
            }
            structureChanged = false;
        }
        updateCoords();
        
        if (isPicked && params.getPickIndicator() == PresentationParams.BOX_PICK_INDICATOR ||
                       (params.getRenderingParams().getDisplayMode() & RenderingParams.OUTLINE_BOX) != 0) {
            updateExtents();
            float[] boxVerts = new float[24];
            boxArr = new IndexedLineStripArray(8, GeometryArray.COORDINATES, 24, new int[]{
                2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
            });
            boxArr.setCoordinateIndices(0, new int[]{
                0, 1, 2, 3, 4, 5, 6, 7, 0, 2, 1, 3, 4, 6, 5, 7, 0, 4, 1, 5, 2, 6, 3, 7
            });
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 2; j++)
                    for (int k = 0; k < 2; k++) {
                        int m = 3 * (4 * i + 2 * j + k);
                        boxVerts[m] = extents[k][0];
                        boxVerts[m + 1] = extents[j][1];
                        boxVerts[m + 2] = extents[i][2];
                    }
            boxArr.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
            boxArr.setCoordinates(0, boxVerts);
           
            frameShape.addGeometry(boxArr);
        }
        switch (colorMode) {
            case DataMappingParams.COLORMAPPED:
            case DataMappingParams.RGB:
                updateColors();
                break;
            case DataMappingParams.UVTEXTURED:
                updateTextureCoords();
                break;
            case DataMappingParams.UNCOLORED:
                break;
        }
        if (detach)
            this.postattach();
    }

    public void setParams(PresentationParams params)
    {
        if (params == null)
            return;
        this.params = params;
        dataMappingParams = params.getDataMappingParams();
        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
        renderingParams = params.getRenderingParams();
        transformParams = params.getTransformParams();
        appearance = renderingParams.getAppearance();
        appearance.setUserData(this);
        material = (OpenMaterial) appearance.getMaterial();
        lineAppearance = renderingParams.getLineAppearance();
        lineAppearance.setUserData(this);
        dataMappingParams.addRenderEventListener(renderEventListener);
        renderingParams.addRenderEventListener(renderEventListener);
        transformParams.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                transformGroup.setTransform(new Transform3D(transformParams.getTransform()));
            }
        });
        transformGroup.setTransform(new Transform3D(transformParams.getTransform()));
    }

    public ColormapLegend getColormapLegend()
    {
        return colormapLegend;
    }

    public void updateExtents()
    {
        for (int i = 0; i < 3; i++) {
            extents[0][i] = Float.MAX_VALUE;
            extents[1][i] = -Float.MAX_VALUE;
        }
        if (triangleCellArray != null) {
            int[] nodes = triangleCellArray.getNodes();
            for (int i = 0; i < nodes.length; i++) {
                int j = 3 * nodes[i];
                for (int k = 0; k < 3; k++) {
                    if (coords[j + k] > extents[1][k])
                        extents[1][k] = coords[j + k];
                    if (coords[j + k] < extents[0][k])
                        extents[0][k] = coords[j + k];
                }
            }
        }
        if (quadCellArray != null) {
            int[] nodes = quadCellArray.getNodes();
            for (int i = 0; i < nodes.length; i++) {
                int j = 3 * nodes[i];
                for (int k = 0; k < 3; k++) {
                    if (coords[j + k] > extents[1][k])
                        extents[1][k] = coords[j + k];
                    if (coords[j + k] < extents[0][k])
                        extents[0][k] = coords[j + k];
                }
            }
        }
        if (segCellArray != null) {
            int[] nodes = segCellArray.getNodes();
            for (int i = 0; i < nodes.length; i++) {
                int j = 3 * nodes[i];
                for (int k = 0; k < 3; k++) {
                    if (coords[j + k] > extents[1][k])
                        extents[1][k] = coords[j + k];
                    if (coords[j + k] < extents[0][k])
                        extents[0][k] = coords[j + k];
                }
            }
        }
        if (pointCellArray != null) {
            int[] nodes = pointCellArray.getNodes();
            for (int i = 0; i < nodes.length; i++) {
                int j = 3 * nodes[i];
                for (int k = 0; k < 3; k++) {
                    if (coords[j + k] > extents[1][k])
                        extents[1][k] = coords[j + k];
                    if (coords[j + k] < extents[0][k])
                        extents[0][k] = coords[j + k];
                }
            }
        }
    }

    public void draw2D(J3DGraphics2D vGraphics, LocalToWindow ltw, int w, int h)
    {
        localToWindow = ltw;
        Font f = vGraphics.getFont();
        Color c = vGraphics.getColor();
        vGraphics.setColor(renderingParams.getColor());
        localToWindow.update(this);
        colormapLegend.draw2D(vGraphics, localToWindow, w, h);
        vGraphics.setColor(c);
        vGraphics.setFont(f);
    }

    public DataMappingParams getDataMappingParams()
    {
        return dataMappingParams;
    }

    public boolean isPicked() {
        return isPicked;
    }

    public void setPicked(boolean picked)
    {
        this.isPicked = picked;
        updateGeometry();
    }

    public void flipPicked()
    {
        isPicked = !isPicked;
        cellSet.setSelected(isPicked);
        updateGeometry();
    }

    public Transform3D getTransform()
    {
        return new Transform3D(transformParams.getTransform());
    }

    public PresentationParams getParams()
    {
        return params;
    }

    public CellSet getCellSet()
    {
        return cellSet;
    }

    ColorListener bgrColorListener = new ColorListener()
    {
        @Override
        public void colorChoosen(ColorEvent e)
        {
            bgrColor = ColorMapper.convertColorToColor3f(e.getSelectedColor());
            if (renderingParams.getShadingMode() == RenderingParams.BACKGROUND) {
                boolean detach = CellSetGeometry.this.postdetach();
                renderingParams.setDiffuseColor(bgrColor);
                if (appearance != null && appearance.getColoringAttributes() != null)
                    appearance.getColoringAttributes().setColor(bgrColor);
                material.setAmbientColor(bgrColor);
                material.setDiffuseColor(bgrColor);
                material.setEmissiveColor(bgrColor);
                dataMappingParams.getColorMap0().setDefaultColor(bgrColor);
                ignoreUpdate = false;
                updateGeometry();
                if (detach)
                    CellSetGeometry.this.postattach();
            }
        }
    };

    public ColorListener getBgrColorListener()
    {
        return bgrColorListener;
    }

    public GeometryObject2DStruct getGeometryObj2DStruct()
    {
        return outObj2DStruct;
    }

}
