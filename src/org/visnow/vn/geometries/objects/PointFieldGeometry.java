/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.objects;

import java.util.Arrays;
import javax.swing.event.ChangeEvent;
import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineStripArray;
import org.jogamp.java3d.PointArray;
import org.jogamp.java3d.Transform3D;
import org.visnow.jscic.Field;
import org.visnow.jscic.PointField;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.utils.ColorMapper;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import static org.visnow.vn.geometries.objects.generics.ObjectCapabilities.*;

import org.visnow.vn.geometries.parameters.PointFieldRenderingParams;
import org.visnow.vn.geometries.parameters.PresentationParams;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 *
 * A geometric object created from a PointField under control of PresentationParams
 *
 * The Shape built is a PointArray (colored or not, depending on DataMappingParams and representing the fileld nodes selected accordingly to the RenderingParams
 *
 */
public class PointFieldGeometry extends FieldGeometry
{

    protected OpenShape3D pointShape = new OpenShape3D();
    protected OpenShape3D frameShape = new OpenShape3D();
    protected LogicLargeArray lastMask = null;
    protected int colorMode = DataMappingParams.UNCOLORED;
    protected int currentColorMode = -1;
    protected int nNodePoints = 0;
    protected long[] indices;
    protected float[] coords;
    protected byte[] colors;
    protected PointFieldRenderingParams dispParams;
    protected RenderEventListener renderEventListener;
    protected PointArray nodeArr = null;
    protected IndexedLineStripArray boxArr = null;

    private long nFieldPoints;
    private int preferredNNodes = 1;
    private double ratio = 1;

    protected PointFieldGeometry(String name)
    {
        super(name);
        geometries.removeAllChildren();
        geometries.addChild(pointShape);
        geometries.addChild(frameShape);
        transformedGeometries.addChild(geometries);
        renderEventListener = new RenderEventListener()
        {
            @Override
            public void renderExtentChanged(RenderEvent e)
            {
                if (ignoreUpdate)
                    return;
                if (field != dispParams.getInField())
                    return;
                int extent = e.getUpdateExtent();
                int cMode = dataMappingParams.getColorMode();
                if (currentColorMode < 0) {
                    currentColorMode = cMode;
                    updateShapes();
                    return;
                }
                if (extent == RenderEvent.COLORS) {
                    validateColorMode();
                    updateColors();
                    currentColorMode = cMode;
                    return;
                }
                if (extent == RenderEvent.COORDS)
                    updateCoords();
                if (extent == RenderEvent.GEOMETRY)
                    updateShapes();
                currentColorMode = cMode;
            }
        };
    }

    public PointFieldGeometry(String name, PresentationParams presentationParams)
    {
        this(name);
        this.dataMappingParams    = presentationParams.getDataMappingParams();
        this.dataMappingParams.addRenderEventListener(renderEventListener);
        this.transformParams      = presentationParams.getTransformParams();
        this.transformParams.addChangeListener((ChangeEvent e) -> {
            transformedGeometries.setTransform(new Transform3D(transformParams.getTransform()));
        });
        this.dispParams = presentationParams.getPointFieldParams();
        this.appearance           = dispParams.getAppearance();
        this.dispParams.addRenderEventListener(renderEventListener);
        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
    }

    @Override
    public Field getField()
    {
        return field;
    }

    @Override
    public boolean setField(Field inField)
    {
        if (inField == null)
            return false;
        field = inField;
        dispParams.setField(field);
        nFieldPoints = field.getNNodes();
        return true;
    }


    private void estimateDisplayRatio()
    {
        preferredNNodes = (int)dispParams.getNNodes();
        ratio = (float)dispParams.getNNodes() / dispParams.getNPreselectedNodes();
        if (ratio > 1) {
            ratio = 1;
            preferredNNodes = (int)dispParams.getNPreselectedNodes();
        }
    }

    private void generateNodeIndices()
    {

        estimateDisplayRatio();
        if (preferredNNodes <= 0)
            return;
        long[] gl = new long[preferredNNodes];
        nNodePoints = 0;
        Arrays.fill(gl, -1);

        switch (dispParams.getSelectBy()) {
        case INDEX:
            for (long i = (long)(dispParams.getFrom() * nFieldPoints) /100;
                      i < (long)(dispParams.getTo() * nFieldPoints) / 100 && nNodePoints < gl.length;
                      i++)
                if (Math.random() < ratio) {
                    gl[nNodePoints] = i;
                    nNodePoints += 1;
                }
            break;
        case SUBSET:
            UnsignedByteLargeArray sets = dispParams.getSubsetDataArray().getRawByteArray();
            int[] reverseIndices = dispParams.getReverseIndices();
            boolean[] activeSubsets = dispParams.getSubsetActive();
            for (long i = 0; i < sets.length() && nNodePoints < gl.length; i++)
                if (activeSubsets[reverseIndices[sets.getInt(i)]] &&
                    Math.random() < ratio) {
                    gl[nNodePoints] = i;
                    nNodePoints += 1;
                }
            break;
        case COMPONENT:
            DataArray da = field.getComponent(dispParams.getValRange().getComponentName());
            int vlen = da.getVectorLength();
            LargeArray data = da.getRawArray();
            float low = dispParams.getValRange().getLow();
            float up  = dispParams.getValRange().getUp();
            for (long i = 0; i < nFieldPoints; i++) {
                float val = 0;
                if (vlen == 1)
                    val = data.getFloat(i);
                else {
                    for (int j = 0; j < vlen; j++)
                        val += data.getFloat(vlen * i + j) * data.getFloat(vlen * i + j);
                    val = (float)Math.sqrt(val);
                }
                if (val <= up && val >= low && Math.random() < ratio && nNodePoints < gl.length) {
                    gl[nNodePoints] = i;
                    nNodePoints += 1;
                }
             }
             break;
        }
        indices = new long[nNodePoints];
        System.arraycopy(gl, 0, indices, 0, nNodePoints);
    }

    public void generateColoredNodes()
    {
        boolean detach = geometry.postdetach();
        generateNodeIndices();
        if (nNodePoints == 0)
            return;
        nodeArr = new PointArray((int) nNodePoints,
                                 GeometryArray.COORDINATES |
                                 GeometryArray.COLOR_4);
        setStandardCapabilities(nodeArr);
        if (detach)
            geometry.postattach();
    }

    public void generateNodes()
    {
        boolean detach = geometry.postdetach();
        generateNodeIndices();
        if (nNodePoints == 0)
            return;
        nodeArr = new PointArray((int) nFieldPoints,
                                 GeometryArray.COORDINATES);
        setStandardCapabilities(nodeArr);
        if (detach)
            geometry.postattach();
    }

    @Override
    public void updateCoords()
    {
        updateCoords(!ignoreUpdate);
    }

    @Override
    public void updateCoords(boolean force)
    {
        if (!force || field == null || indices.length != nNodePoints)
            return;
        boolean detach = geometry.postdetach();
        FloatLargeArray crds = field.getCurrentCoords();
        coords = new float[3 * nNodePoints];
        for (int i = 0; i < nNodePoints; i++)
            LargeArrayUtils.arraycopy(crds, 3 * indices[i], coords, 3 * i, 3);
        if (nodeArr != null)
            nodeArr.setCoordinates(0, coords);
        if (detach)
            geometry.postattach();
    }

    @Override
    public void updateCoords(FloatLargeArray newCoords)
    {
        if (field == null || newCoords.length() != 3 * field.getNNodes()) {
            System.out.println("bad new coords");
            return;
        }
        boolean detach = geometry.postdetach();
        for (int i = 0; i < nNodePoints; i++)
            LargeArrayUtils.arraycopy(newCoords, 3 * indices[i], coords, 3 * i, 3);
        if (nodeArr != null)
            nodeArr.setCoordinates(0, coords);
        if (detach)
            geometry.postattach();
    }

    public void updateColors()
    {
        boolean detach = geometry.postdetach();
        if (colors == null || colors.length != 4 * nNodePoints)
            colors = new byte[4 * nNodePoints];
        ColorMapper.mapColorsIndexed(field, dataMappingParams, indices, dataMappingParams.getDefaultColor(), colors);
        ColorMapper.mapTransparencyIndexed(field, dataMappingParams.getTransparencyParams(), indices, colors);
        if (nodeArr != null)
            nodeArr.setColors(0, colors);
        if (detach)
            geometry.postattach();
    }

    @Override
    public void updateDataMap()
    {
        boolean detach = geometry.postdetach();
        if (dataMappingParams.getColorMode() == DataMappingParams.COLORMAPPED ||
            dataMappingParams.getColorMode() == DataMappingParams.COLORMAPPED2D ||
            dataMappingParams.getColorMode() == DataMappingParams.COLORED)
            updateColors();
        if (detach)
            geometry.postattach();
    }

    public void updateShapes()
    {
        boolean detach = geometry.postdetach();
        updateGeometry(dispParams.isOutline());
        if (geometries.getParent() == null)
            transformedGeometries.addChild(geometries);
        transformedGeometries.setTransform(new Transform3D(transformParams.getTransform()));
        if (transformedGeometries.getParent() == null)
            geometry.addChild(transformedGeometries);
        if (detach)
            geometry.postattach();
    }

    public void updateData()
    {
        boolean restructure = false;
        LogicLargeArray mask = field.getCurrentMask();
        if (mask != null)
            if (lastMask == null || lastMask.length() != mask.length()) {
                restructure = true;
                lastMask = mask;
            } else {
                for (int i = 0; i < mask.length(); i++)
                    if (mask.getByte(i) != lastMask.getByte(i)) {
                        restructure = true;
                        break;
                    }
            }
        updateExtents();
        if (restructure)
            updateShapes();
        else {
            updateColors();
            updateCoords();
        }
    }

    public void updateGeometry(PointField inField, boolean showBox)
    {
        if (field != inField && !setField(inField))
            return;
        updateGeometry(showBox);
    }

    public void updateGeometry(boolean showBox)
    {
        boolean detach = geometry.postdetach();
        if (pointShape != null)
            pointShape.removeAllGeometries();
        if (frameShape != null)
            frameShape.removeAllGeometries();
        nodeArr = null;
        boxArr = null;

        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
            case DataMappingParams.RGB:
                generateColoredNodes();
                break;
            case DataMappingParams.UVTEXTURED:
            case DataMappingParams.UNCOLORED:
                generateNodes();
                break;
        }
        pointShape.addGeometry(nodeArr);
        if (showBox) {
            float[][] ext = field.getPreferredExtents();
            float[] boxVerts = new float[24];
            boxArr = new IndexedLineStripArray(8, GeometryArray.COORDINATES, 24,
            new int[]{
                2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
            });
            boxArr.setCoordinateIndices(0, new int[]{
                0, 1, 2, 3, 4, 5, 6, 7, 0, 2, 1, 3, 4, 6, 5, 7, 0, 4, 1, 5, 2, 6, 3, 7
            });
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 2; j++)
                    for (int k = 0; k < 2; k++) {
                        int m = 3 * (4 * i + 2 * j + k);
                        boxVerts[m] = ext[k][0];
                        boxVerts[m + 1] = ext[j][1];
                        boxVerts[m + 2] = ext[i][2];
                    }
            boxArr.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
            boxArr.setCoordinates(0, boxVerts);
            frameShape.addGeometry(boxArr);
            frameShape.setAppearance(appearance);
        }

        structureChanged = false;
        updateCoords();
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
            case DataMappingParams.RGB:
                updateColors();
                break;
            case DataMappingParams.UVTEXTURED:
                break;
            case DataMappingParams.UNCOLORED:
                break;
        }
        appearance = dispParams.getAppearance();
        appearance.setUserData(this);
        lineAppearance = appearance;

        lineAppearance.setUserData(this);
        if (appearance.getMaterial() != null) {
            appearance.getMaterial().setAmbientColor(dataMappingParams.getDefaultColor());
            appearance.getMaterial().setDiffuseColor(dataMappingParams.getDefaultColor());
            appearance.getMaterial().setSpecularColor(dataMappingParams.getDefaultColor());
        }

        lineAppearance.getColoringAttributes().setColor(dispParams.getColor3f());
        lineAppearance.setLineAttributes(dispParams.getAppearance().getLineAttributes());
        if (dataMappingParams.getColorMode() != DataMappingParams.UVTEXTURED)
            updateColors();
        pointShape.setAppearance(lineAppearance);
        frameShape.setAppearance(lineAppearance);
        transformedGeometries.setTransform(new Transform3D(transformParams.getTransform()));
        if (detach)
            geometry.postattach();
    }

    public OpenBranchGroup getGeometry(PointField inField)
    {
        updateGeometry(inField);
        return geometry;
    }

    @Override
    public OpenBranchGroup getGeometry(Field inField)
    {
        if (!(inField instanceof PointField))
            return null;
        field = (PointField) inField;
        return getGeometry((PointField) inField);
    }

    @Override
    public void createGeometry(Field inField)
    {
        if (!(inField instanceof PointField))
            return;
        updateGeometry((PointField) inField);
    }

    public void updateGeometry(PointField inField)
    {
        setField(inField);
        updateShapes();
    }

    @Override
    public void updateGeometry(Field inField)
    {
        if (inField instanceof PointField)
            updateGeometry((PointField) inField);
    }

    private void validateColorMode()
    {
        colorMode = dataMappingParams.getColorMode();
        // check if color mode and selected components combination is valid; fall back to UNCOLORED otherwise
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
                if (field.getComponent(dataMappingParams.getColorMap0().getDataComponentName()) != null)
                    return;
                break;
            case DataMappingParams.RGB:
                if (field.getComponent(dataMappingParams.getRedParams().getDataComponentName()) != null ||
                        field.getComponent(dataMappingParams.getGreenParams().getDataComponentName()) != null ||
                        field.getComponent(dataMappingParams.getBlueParams().getDataComponentName()) != null)
                    return;
                break;
        }
        colorMode = DataMappingParams.UNCOLORED;
    }

    @Override
    public void updateGeometry()
    {
        if (ignoreUpdate)
            return;
        dataMappingParams.setParentObjectSize((int) nFieldPoints);
        updateShapes();
    }

    @Override
    public OpenBranchGroup getGeometry()
    {
        return geometry;
    }

}
