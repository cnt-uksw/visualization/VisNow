/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.objects;

import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineStripArray;
import org.apache.log4j.Logger;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.RegularField;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class RegularField3DOutline extends OpenBranchGroup
{

    protected RegularField field;
    protected OpenShape3D outlineShape = new OpenShape3D();
    protected OpenBranchGroup geometry = new OpenBranchGroup();
    protected IndexedLineStripArray box = null;

    protected int nLineStrips = 0;
    protected int[] dims = null;
    protected FloatLargeArray coords = null;
    protected int[] lineStripCounts = null;
    protected float[] boxVerts = null;

    static Logger logger = Logger.getLogger(RegularField3DOutline.class);

    public RegularField3DOutline()
    {
        super();
        geometry.addChild(outlineShape);
        this.addChild(geometry);
    }

    public boolean setField(RegularField inField)
    {
        if (inField == null || inField.getDims() == null || inField.getDims().length != 3)
            return false;
        dims = inField.getDims();
        coords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords();
        this.field = inField;
        makeOutlineBox();
        return true;
    }

    private void updateBoxCoords()
    {
        long[] lDims = field.getLDims();
        boolean detach = postdetach();
        if (coords == null) {
            float[][] af = field.getAffine();
            for (int i = 0; i < 3; i++) {
                boxVerts[i] = af[3][i];
                boxVerts[i + 3] = af[3][i] + (lDims[0] - 1) * af[0][i];
                boxVerts[i + 6] = af[3][i] + (lDims[1] - 1) * af[1][i];
                boxVerts[i + 9] = af[3][i] + (lDims[1] - 1) * af[1][i] + (lDims[0] - 1) * af[0][i];
                boxVerts[i + 12] = af[3][i] + (lDims[2] - 1) * af[2][i];
                boxVerts[i + 15] = af[3][i] + (lDims[2] - 1) * af[2][i] + (lDims[0] - 1) * af[0][i];
                boxVerts[i + 18] = af[3][i] + (lDims[2] - 1) * af[2][i] + (lDims[1] - 1) * af[1][i];
                boxVerts[i + 21] = af[3][i] + (lDims[2] - 1) * af[2][i] + (lDims[1] - 1) * af[1][i] + (lDims[0] - 1) * af[0][i];
            }
        } else {
            int k = 0;
            long l = 0, s = 3;
            
            // i0 axis edges
            for (int i = 0; i < lDims[0]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * lDims[0] * (lDims[1] - 1);
            for (int i = 0; i < lDims[0]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * lDims[0] * lDims[1] * (lDims[2] - 1);
            for (int i = 0; i < lDims[0]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * lDims[0] * lDims[1] * (lDims[2] - 1) + 3 * lDims[0] * (lDims[1] - 1);
            for (int i = 0; i < lDims[0]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);

            // i1 axis edges
            s = 3 * lDims[0];
            l = 0;
            for (int i = 0; i < lDims[1]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * (lDims[0] - 1);
            for (int i = 0; i < lDims[1]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * lDims[0] * lDims[1] * (lDims[2] - 1);
            for (int i = 0; i < lDims[1]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * lDims[0] * lDims[1] * (lDims[2] - 1) + 3 * (lDims[0] - 1);
            for (int i = 0; i < lDims[1]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);

            // i2 axis edges
            s = 3 * lDims[0] * lDims[1];
            l = 0;
            for (int i = 0; i < lDims[2]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * (lDims[0] - 1);
            for (int i = 0; i < lDims[2]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * lDims[0] * (lDims[1] - 1);
            for (int i = 0; i < lDims[2]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * lDims[0] * (lDims[1] - 1) + 3 * (lDims[0] - 1);
            for (int i = 0; i < lDims[2]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
        }
        box.setCoordinates(0, boxVerts);
        if (detach)
            postattach();
    }

    private void makeOutlineBox()
    {
        boolean detach = postdetach();
        outlineShape.removeAllGeometries();
        if (field == null) {
            if (detach)
                postattach();
            return;
        }

        if (coords == null) {
            boxVerts = new float[24];
            box = new IndexedLineStripArray(8, GeometryArray.COORDINATES, 24, new int[]{2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2});

            box.setCoordinateIndices(0, new int[]{0, 1, 2, 3, 4, 5, 6, 7, 0, 2, 1, 3, 4, 6, 5, 7, 0, 4, 1, 5, 2, 6, 3, 7});
        } else {
            boxVerts = new float[3 * 4 * (dims[0] + dims[1] + dims[2])];
            box = new IndexedLineStripArray(4 * (dims[0] + dims[1] + dims[2]),
                                            GeometryArray.COORDINATES,
                                            4 * (dims[0] + dims[1] + dims[2]),
                                            new int[]{
                                                dims[0], dims[0], dims[0], dims[0],
                                                dims[1], dims[1], dims[1], dims[1],
                                                dims[2], dims[2], dims[2], dims[2]
                                            });

            int[] cInd = new int[4 * (dims[0] + dims[1] + dims[2])];
            for (int i = 0; i < cInd.length; i++)
                cInd[i] = i;
            box.setCoordinateIndices(0, cInd);
        }
        box.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        updateBoxCoords();

        outlineShape.addGeometry(box);
        if (detach)
            postattach();
    }
}
