/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.objects.generics;

import org.jogamp.java3d.Appearance;
import org.jogamp.java3d.TextureAttributes;

/**
 *
 * @author Krzysztof S. Nowinski, Warsaw University, ICM
 *
 */
public class OpenAppearance extends Appearance
{

    /**
     * Creates a new instance of OpenAppearance
     */
    public OpenAppearance()
    {
        setCapability(Appearance.ALLOW_LINE_ATTRIBUTES_READ);
        setCapability(Appearance.ALLOW_LINE_ATTRIBUTES_WRITE);
        setCapability(Appearance.ALLOW_TRANSPARENCY_ATTRIBUTES_WRITE);
        setCapability(Appearance.ALLOW_TRANSPARENCY_ATTRIBUTES_READ);
        setCapability(Appearance.ALLOW_COLORING_ATTRIBUTES_READ);
        setCapability(Appearance.ALLOW_COLORING_ATTRIBUTES_WRITE);
        setCapability(Appearance.ALLOW_POLYGON_ATTRIBUTES_READ);
        setCapability(Appearance.ALLOW_POLYGON_ATTRIBUTES_WRITE);
        setCapability(Appearance.ALLOW_POINT_ATTRIBUTES_READ);
        setCapability(Appearance.ALLOW_POINT_ATTRIBUTES_WRITE);
        setCapability(Appearance.ALLOW_MATERIAL_READ);
        setCapability(Appearance.ALLOW_MATERIAL_WRITE);
        setCapability(Appearance.ALLOW_TEXTURE_READ);
        setCapability(Appearance.ALLOW_TEXTURE_WRITE);
        setCapability(Appearance.ALLOW_POLYGON_ATTRIBUTES_WRITE);
        setMaterial(new OpenMaterial());
        setTransparencyAttributes(new OpenTransparencyAttributes());
        setLineAttributes(new OpenLineAttributes());
        setPointAttributes(new OpenPointAttributes());
        setPolygonAttributes(new OpenPolygonAttributes());
        setColoringAttributes(new OpenColoringAttributes());
        setTextureAttributes(new TextureAttributes());
        setCapability(Appearance.ALLOW_TEXTURE_ATTRIBUTES_READ);
        setCapability(Appearance.ALLOW_TEXTURE_ATTRIBUTES_WRITE);
    }

    @Override
    public OpenAppearance cloneNodeComponent(boolean forceDuplicate)
    {
        //      getPolygonAttributes().setCapability(PolygonAttributes.);
        OpenAppearance openAppearance = new OpenAppearance();
        openAppearance.duplicateNodeComponent(this, forceDuplicate);
        return openAppearance;
    }
    
    public void copyValuesFrom(Appearance src)
    {
        ((OpenTransparencyAttributes)getTransparencyAttributes()).
                copyValuesFrom(src.getTransparencyAttributes());
        ((OpenLineAttributes)getLineAttributes()).
                copyValuesFrom(src.getLineAttributes());
        ((OpenPointAttributes)getPointAttributes()).
                copyValuesFrom(src.getPointAttributes());
        ((OpenPolygonAttributes)getPolygonAttributes()).
                copyValuesFrom(src.getPolygonAttributes());
        ((OpenColoringAttributes)getColoringAttributes()).
                copyValuesFrom(src.getColoringAttributes());
        if (getMaterial() == null || !(getMaterial() instanceof OpenMaterial))
            setMaterial(new OpenMaterial());
        if (src.getMaterial() == null || !(src.getMaterial() instanceof OpenMaterial))   
            src.setMaterial(new OpenMaterial());
        else
            ((OpenMaterial)getMaterial()).copyValuesFrom(src.getMaterial());
    }

}
