/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.geometries.objects;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.ArrayList;
import org.jogamp.java3d.*;
import org.jogamp.vecmath.Color3f;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.geometries.events.ColorEvent;
import org.visnow.vn.geometries.events.ColorListener;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;
import org.visnow.vn.geometries.parameters.ColorComponentParams;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.geometries.utils.ColorMapper;
import org.visnow.vn.geometries.utils.TextureMapper;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import org.visnow.vn.lib.utils.geometry2D.*;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.ByteLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.vn.system.main.VisNow;
import static org.visnow.vn.geometries.objects.generics.ObjectCapabilities.*;
import static org.visnow.jlargearrays.LargeArrayUtils.*;
import org.visnow.vn.geometries.parameters.PresentationParams;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class RegularField2DGeometry extends RegularFieldGeometry
{

    public static final int NOT_RENDERED_YET = -1;
    public static final int TEXTURE_RENDERED = 0;
    public static final int SURFACE_RENDERED = 1;
    protected OpenShape3D surfaceShape = new OpenShape3D();
    protected OpenShape3D lineShape = new OpenShape3D();
    protected OpenShape3D pointShape = new OpenShape3D();
    protected OpenShape3D frameShape = new OpenShape3D();
    protected byte lastSurfaceOrientation = 1;
    protected byte surfaceOrientation = 1;
    protected LogicLargeArray lastMask = null;
    protected int[] timeRange = null;
    protected float currentT = 0;
    protected ImageArray2D image2D = null;
    protected LineArray2D edges2D = null;
    protected PointArray2D points2D = null;
    protected BoxArray2D box2D = null;
    protected int renderAs = NOT_RENDERED_YET, lastRender = NOT_RENDERED_YET;
    protected int textureRenderingLimit
            = Integer.parseInt(VisNow.get().getMainConfig().getProperty("visnow.continuousColorAdjustingLimit")) / 10;

    private final void init()
    {
        geometries.removeAllChildren();
        geometries.addChild(surfaceShape);
        geometries.addChild(lineShape);
        geometries.addChild(pointShape);
        geometries.addChild(frameShape);
        renderEventListener = new RenderEventListener()
        {
            @Override
            public void renderExtentChanged(RenderEvent e)
            {
                if (ignoreUpdate || regularField == null)
                    return;
                int extent = e.getUpdateExtent();
                int cMode = dataMappingParams.getColorMode();
                if (renderingParams.getDisplayMode() == RenderingParams.BACKGROUND)
                    cMode = DataMappingParams.UNCOLORED;
                if (currentColorMode < 0) {
                    currentColorMode = cMode;
                    updateShapes();
                    return;
                }
                if (extent == RenderEvent.COLORS || extent == RenderEvent.TRANSPARENCY || extent == RenderEvent.TEXTURE) {
                    validateColorMode();
                    if (resetGeometry[currentColorMode][cMode])
                        updateShapes();
                    else if (colorMode == DataMappingParams.UVTEXTURED)
                        updateTextureCoords();
                    else
                        updateColors();
                    currentColorMode = cMode;
                    return;
                }
                if (extent == RenderEvent.COORDS)
                    updateCoords();
                if (extent == RenderEvent.GEOMETRY)
                    updateShapes();
                currentColorMode = cMode;
            }
        };

    }

    public RegularField2DGeometry(String name)
    {
        super(name);
        init();
    }

    public RegularField2DGeometry(String name, PresentationParams presentationParams)
    {
        super(name, presentationParams);
        init();
    }

    /**
     * Set the value of field
     *
     * @param inField new value of field
     * <p>
     * @return boolean flag if set was successful
     */
    @Override
    public boolean setField(RegularField inField)
    {
        if (inField == null || inField.getDims() == null || inField.getDims().length != 2)
            return false;
        renderAs = SURFACE_RENDERED;
        coords = inField.getCurrentCoords();
        if (inField.hasCoords()) {
            coordsChanged = this.regularField == null;
        } else {
            coordsChanged = true;
        }
        if (inField.getTrueNSpace() == 2)
            renderingParams.setShadingMode(RenderingParams.UNSHADED);
        super.setField(inField);
        normals = inField.getNormals() == null ? null : inField.getNormals();
        return true;
    }

    public void setSurfaceOrientation(byte surfaceOrientation)
    {
        this.surfaceOrientation = surfaceOrientation;
    }

    private void generateTriangleIndices()
    {
        if (regularField.getCurrentMask() == null || renderingParams.ignoreMask() || renderingParams.isTransparentlyRenderedMaskedNodes()) {
            nTriangleStrips = dims[1] - 1;
            triangleStripCounts = new IntLargeArray(nTriangleStrips, false);
            for (long i = 0; i < nTriangleStrips; i++)
                triangleStripCounts.setLong(i, 2 * dims[0]);
            nTriangleIndices = 2 * dims[0] * (dims[1] - 1);
            coordIndices = new IntLargeArray(nTriangleIndices, false);
            if (renderingParams.getSurfaceOrientation() == surfaceOrientation)
                for (long i = 0, k = 0; i < dims[1] - 1; i++)
                    for (long j = 0; j < dims[0]; j++, k += 2) {
                        coordIndices.setLong(k, i * dims[0] + j);
                        coordIndices.setLong(k + 1, (i + 1) * dims[0] + j);
                    }
            else {
                for (long i = 0, k = 0; i < dims[1] - 1; i++)
                    for (long j = 0; j < dims[0]; j++, k += 2) {
                        coordIndices.setLong(k, (i + 1) * dims[0] + j);
                        coordIndices.setLong(k + 1, i * dims[0] + j);
                    }
            }
        } else {
            IntLargeArray ind = new IntLargeArray(2l * (long) (dims[0] + 1) * (long) (dims[1] + 1), true);
            IntLargeArray stripInd = new IntLargeArray(2l * (long) dims[0], true);
            long cl = 0;
            nTriangleStrips = 0;
            nTriangleIndices = 0;
            LogicLargeArray mask = field.getCurrentMask();

            for (long i = 0; i < dims[1] - 1; i++) {
                if (renderingParams.getSurfaceOrientation() == surfaceOrientation)
                    for (long j = 0; j < dims[0]; j++) {
                        stripInd.setLong(2 * j, i * dims[0] + j);
                        stripInd.setLong(2 * j + 1, (i + 1) * dims[0] + j);
                    }
                else
                    for (long j = 0; j < dims[0]; j++) {
                        stripInd.setLong(2 * j, (i + 1) * dims[0] + j);
                        stripInd.setLong(2 * j + 1, i * dims[0] + j);
                    }

                boolean fakeFirstTriangle = false;
                for (long j = 0; j < stripInd.length(); j++) {
                    long l = stripInd.getLong(j);
                    if (mask.getByte(l) == 1) {
                        if (cl == 0 && j % 2 == 1) {
                            ind.setLong(nTriangleIndices + cl, l);
                            cl += 1;
                            fakeFirstTriangle = true;
                        }
                        ind.setLong(nTriangleIndices + cl, l);
                        cl += 1;
                    } else {
                        if (cl < 3 || cl == 3 && fakeFirstTriangle)
                            cl = 0;
                        else {
                            nTriangleStrips += 1;
                            ind.setLong(ind.length() - nTriangleStrips, cl);
                            nTriangleIndices += cl;
                            cl = 0;
                        }
                        fakeFirstTriangle = false;
                    }
                }
                if (cl < 3 || cl == 3 && fakeFirstTriangle)
                    cl = 0;
                else {
                    nTriangleStrips += 1;
                    ind.setLong(ind.length() - nTriangleStrips, cl);
                    nTriangleIndices += cl;
                    cl = 0;
                }
            }
            if (nTriangleStrips == 0)
                return;
            triangleStripCounts = new IntLargeArray(nTriangleStrips, false);
            for (int i = 0; i < nTriangleStrips; i++)
                triangleStripCounts.setLong(i, ind.getLong(ind.length() - i - 1));
            coordIndices = new IntLargeArray(nTriangleIndices, false);
            arraycopy(ind, 0, coordIndices, 0, nTriangleIndices);
        }
    }

    public void generateTriangles()
    {
        if (dims.length != 2)
            return;
        boolean detach = geometry.postdetach();

        generateTriangleIndices();
        if (nTriangleStrips == 0)
            return;
        if (renderingParams.getDisplayMode() == RenderingParams.BACKGROUND || renderingParams.getDisplayMode() == RenderingParams.UNSHADED)
            triangleArr = new IndexedTriangleStripArray((int) nNodes,
                                                        GeometryArray.COORDINATES |
                                                        GeometryArray.USE_COORD_INDEX_ONLY |
                                                        GeometryArray.BY_REFERENCE,
                                                        (int) nTriangleIndices, triangleStripCounts.getData());
        else
            triangleArr = new IndexedTriangleStripArray((int) nNodes,
                                                        GeometryArray.COORDINATES |
                                                        GeometryArray.NORMALS |
                                                        GeometryArray.USE_COORD_INDEX_ONLY |
                                                        GeometryArray.BY_REFERENCE,
                                                        (int) nTriangleIndices, triangleStripCounts.getData());
        setStandardCapabilities(triangleArr);
        triangleArr.setCoordinateIndices(0, coordIndices.getData());
        if (detach)
            geometry.postattach();
    }

    public void generateColoredTriangles()
    {
        if (dims.length != 2)
            return;
        boolean detach = geometry.postdetach();
        generateTriangleIndices();
        if (nTriangleStrips == 0)
            return;
        triangleArr = new IndexedTriangleStripArray((int) nNodes,
                                                    GeometryArray.COORDINATES |
                                                    GeometryArray.NORMALS |
                                                    GeometryArray.COLOR_4 |
                                                    GeometryArray.USE_COORD_INDEX_ONLY |
                                                    GeometryArray.BY_REFERENCE,
                                                    (int) nTriangleIndices, triangleStripCounts.getData());
        setStandardCapabilities(triangleArr);
        triangleArr.setCoordinateIndices(0, coordIndices.getData());
        if (detach)
            geometry.postattach();
    }

    public void generateTexturedTriangles()
    {
        if (dims.length != 2)
            return;
        boolean detach = geometry.postdetach();
        generateTriangleIndices();
        if (nTriangleStrips == 0)
            return;
        triangleArr = new IndexedTriangleStripArray((int) nNodes,
                                                    GeometryArray.COORDINATES |
                                                    GeometryArray.NORMALS |
                                                    GeometryArray.TEXTURE_COORDINATE_2 |
                                                    GeometryArray.USE_COORD_INDEX_ONLY |
                                                    GeometryArray.BY_REFERENCE,
                                                    (int) nTriangleIndices, triangleStripCounts.getData());
        setStandardCapabilities(triangleArr);
        triangleArr.setCoordinateIndices(0, coordIndices.getData());
        if (detach)
            geometry.postattach();
    }

    private void generateEdgeIndices()
    {
        if (regularField.getCurrentMask() == null) {
            nLineStrips = dims[1] + dims[0];
            lineStripCounts = new IntLargeArray(nLineStrips);
            for (long i = 0; i < dims[1]; i++)
                lineStripCounts.setLong(i, dims[0]);
            for (int i = dims[1]; i < dims[1] + dims[0]; i++)
                lineStripCounts.setLong(i, dims[1]);
            nLineIndices = 2l * (long) dims[0] * (long) dims[1];
            coordIndices = new IntLargeArray(nLineIndices, true);
            long k = 0;
            for (long i = 0; i < dims[1]; i++)
                for (long j = 0; j < dims[0]; j++, k++)
                    coordIndices.setLong(k, i * dims[0] + j);
            for (long i = 0; i < dims[0]; i++)
                for (long j = 0; j < dims[1]; j++, k++)
                    coordIndices.setLong(k, j * dims[0] + i);
        } else {
            IntLargeArray ind = new IntLargeArray(2 * (long)dims[0] * (long)dims[1] + (long)dims[1] + (long)dims[0], true);
            long cl = 0;
            nLineStrips = 0;
            nLineIndices = 0;
            LogicLargeArray mask = field.getCurrentMask();
            for (long i = 0; i < dims[1]; i++) {
                for (long j = 0; j < dims[0]; j++) {
                    long l = i * dims[0] + j;
                    if (mask.getByte(l) == 1) {
                        ind.setLong(nLineIndices + cl, l);
                        cl += 1;
                    } else {
                        if (cl < 2)
                            cl = 0;
                        else {
                            nLineStrips += 1;
                            ind.setLong(ind.length() - nLineStrips, cl);
                            nLineIndices += cl;
                            cl = 0;
                        }
                    }
                }
                if (cl < 2)
                    cl = 0;
                else {
                    nLineStrips += 1;
                    ind.setLong(ind.length() - nLineStrips, cl);
                    nLineIndices += cl;
                    cl = 0;
                }
            }
            for (long j = 0; j < dims[0]; j++) {
                for (long i = 0; i < dims[1]; i++) {
                    long l = i * dims[0] + j;
                    if (mask.getByte(l) == 1) {
                        ind.setLong(nLineIndices + cl, l);
                        cl += 1;
                    } else {
                        if (cl < 2)
                            cl = 0;
                        else {
                            nLineStrips += 1;
                            ind.setLong(ind.length() - nLineStrips, cl);
                            nLineIndices += cl;
                            cl = 0;
                        }
                    }
                }
                if (cl < 2)
                    cl = 0;
                else {
                    nLineStrips += 1;
                    ind.setLong(ind.length() - nLineStrips, cl);
                    nLineIndices += cl;
                    cl = 0;
                }
            }
            if (nLineStrips == 0)
                return;
            lineStripCounts = new IntLargeArray(nLineStrips);
            for (long i = 0; i < nLineStrips; i++)
                lineStripCounts.setLong(i, ind.getLong(ind.length() - i - 1));
            coordIndices = new IntLargeArray(nLineIndices, false);
            arraycopy(ind, 0, coordIndices, 0, nLineIndices);
        }
    }

    public void generateEdges()
    {
        if (dims.length != 2)
            return;
        boolean detach = geometry.postdetach();
        generateEdgeIndices();
        if (nLineStrips == 0)
            return;
        edgeArr = new IndexedLineStripArray((int) nNodes,
                                            GeometryArray.COORDINATES |
                                            GeometryArray.USE_COORD_INDEX_ONLY |
                                            GeometryArray.BY_REFERENCE,
                                            (int) nLineIndices, lineStripCounts.getData());
        setStandardCapabilities(edgeArr);
        edgeArr.setCoordinateIndices(0, coordIndices.getData());
        if (detach)
            geometry.postattach();
    }

    public void generateColoredEdges()
    {
        if (dims.length != 2)
            return;
        boolean detach = geometry.postdetach();
        generateEdgeIndices();
        if (nLineStrips == 0)
            return;
        if (renderingParams.isLineLighting())
            edgeArr = new IndexedLineStripArray((int) nNodes,
                                                GeometryArray.COORDINATES |
                                                GeometryArray.NORMALS |
                                                GeometryArray.COLOR_4 |
                                                GeometryArray.USE_COORD_INDEX_ONLY |
                                                GeometryArray.BY_REFERENCE,
                                                (int) nLineIndices, lineStripCounts.getData());
        else
            edgeArr = new IndexedLineStripArray((int) nNodes,
                                                GeometryArray.COORDINATES |
                                                GeometryArray.COLOR_4 |
                                                GeometryArray.USE_COORD_INDEX_ONLY |
                                                GeometryArray.BY_REFERENCE,
                                                (int) nLineIndices, lineStripCounts.getData());
        setStandardCapabilities(edgeArr);
        edgeArr.setCoordinateIndices(0, coordIndices.getData());
        if (detach)
            geometry.postattach();
    }

    private void generateNodeIndices()
    {
        if (regularField.getCurrentMask() == null) {
            nNodePoints = (long) dims[1] * (long) dims[0];
            coordIndices = new IntLargeArray(nNodePoints, false);
            long k = 0;
            for (long i = 0; i < dims[1]; i++)
                for (long j = 0; j < dims[0]; j++, k++)
                    coordIndices.setLong(k, i * dims[0] + j);
        } else {
            LogicLargeArray mask = field.getCurrentMask();
            nNodePoints = 0;
            for (long i = 0; i < mask.length(); i++) {
                if (mask.getByte(i) == 1)
                    nNodePoints++;
            }
            coordIndices = new IntLargeArray(nNodePoints, true);
            long k = 0;
            long m = 0;
            for (long i = 0; i < dims[1]; i++)
                for (long j = 0; j < dims[0]; j++, k++)
                    if (mask.getByte(k) == 1)
                        coordIndices.setLong(m++, i * dims[0] + j);
        }
    }

    public void generateColoredNodes()
    {
        if (dims.length != 2)
            return;
        boolean detach = geometry.postdetach();
        generateNodeIndices();
        if (nNodePoints == 0)
            return;
        nodeArr = new IndexedPointArray((int) nNodes,
                                        GeometryArray.COORDINATES |
                                        GeometryArray.COLOR_4 |
                                        GeometryArray.USE_COORD_INDEX_ONLY |
                                        GeometryArray.BY_REFERENCE,
                                        (int) nNodePoints);
        setStandardCapabilities(nodeArr);
        nodeArr.setCoordinateIndices(0, coordIndices.getData());
        if (detach)
            geometry.postattach();
    }

    public void generateNodes()
    {
        if (dims.length != 2)
            return;
        boolean detach = geometry.postdetach();
        generateNodeIndices();
        if (nNodePoints == 0)
            return;
        nodeArr = new IndexedPointArray((int) nNodes,
                                        GeometryArray.COORDINATES |
                                        GeometryArray.USE_COORD_INDEX_ONLY |
                                        GeometryArray.BY_REFERENCE,
                                        (int) nNodePoints);
        setStandardCapabilities(nodeArr);
        nodeArr.setCoordinateIndices(0, coordIndices.getData());
        if (detach)
            geometry.postattach();
    }

    @Override
    public void updateCoords()
    {
        updateCoords(!ignoreUpdate);
    }

    private boolean canUseTexture()
    {
        return (!regularField.hasCoords() && regularField.getNNodes() > textureRenderingLimit &&
                (renderingParams.getDisplayMode() & RenderingParams.SURFACE) != 0 &&
                (renderingParams.getDisplayMode() & RenderingParams.EDGES) == 0 &&
                (renderingParams.getDisplayMode() & RenderingParams.NODES) == 0 &&
                dataMappingParams.getColorMode() == DataMappingParams.COLORMAPPED);
    }

    private void createTextureMappedQuad()
    {
        float[][] affine = regularField.getAffine();
        float[] nodeCoords = new float[12];
        for (int i = 0; i < 3; i++) {
            nodeCoords[i] = affine[3][i];
            nodeCoords[i + 3] = affine[3][i] + (dims[0] - 1) * affine[0][i];
            nodeCoords[i + 6] = affine[3][i] + (dims[1] - 1) * affine[1][i];
            nodeCoords[i + 9] = affine[3][i] + (dims[0] - 1) * affine[0][i] + (dims[1] - 1) * affine[1][i];
        }
        float[] nodeNormals = new float[12];
        float[] normal = new float[]{
            affine[0][1] * affine[1][2] - affine[0][2] * affine[1][1],
            affine[0][2] * affine[1][0] - affine[0][0] * affine[1][2],
            affine[0][0] * affine[1][1] - affine[0][1] * affine[1][0]
        };
        float nNorm = (float) sqrt(normal[0] * normal[0] + normal[1] * normal[1] + normal[2] * normal[2]);
        if (surfaceOrientation == 1)
            for (int i = 0; i < normal.length; i++)
                nodeNormals[i] = nodeNormals[i + 3] = nodeNormals[i + 6] = nodeNormals[i + 9] = normal[i] / nNorm;
        else
            for (int i = 0; i < normal.length; i++)
                nodeNormals[i] = nodeNormals[i + 3] = nodeNormals[i + 6] = nodeNormals[i + 9] = -normal[i] / nNorm;
        triangleArr.setCoordRefFloat(nodeCoords);
        triangleArr.setNormalRefFloat(nodeNormals);
    }

    @Override
    public void updateCoords(boolean force)
    {
        if (!force || regularField == null)
            return;
//        if (canUseTexture())
//            createTextureMappedQuad();
//        else
        {
            boolean detach = geometry.postdetach();
            coords = regularField.getCurrentCoords();
            if(coords == null) {
                coords = regularField.getCoordsFromAffine();
            }
            normals = regularField.getNormals() == null ? null : regularField.getNormals().clone();
            if (renderingParams.getSurfaceOrientation() != lastSurfaceOrientation)
                for (long i = 0; i < normals.length(); i++)
                    normals.setFloat(i, -normals.getFloat(i));
            lastSurfaceOrientation = renderingParams.getSurfaceOrientation();
            if (triangleArr != null) {
                triangleArr.setCoordRefFloat(coords.getData());
                if ((triangleArr.getVertexFormat() & GeometryArray.NORMALS) != 0)
                    triangleArr.setNormalRefFloat(normals.getData());
            }
            if (edgeArr != null)
                edgeArr.setCoordRefFloat(coords.getData());
            if (nodeArr != null)
                nodeArr.setCoordRefFloat(coords.getData());
            if (detach)
                geometry.postattach();
        }
    }

    @Override
    public void updateCoords(FloatLargeArray newCoords)
    {
        if (regularField == null || regularField.getDims() == null || newCoords.length() != 3 * regularField.getNNodes()) {
            System.out.println("bad new coords");
            return;
        }
        boolean detach = geometry.postdetach();
        coords = newCoords;
        if (normals == null)
            normals = regularField.getNormals() == null ? null : regularField.getNormals();
        if (renderingParams.getSurfaceOrientation() != lastSurfaceOrientation)
            for (long i = 0; i < normals.length(); i++)
                normals.setFloat(i, -normals.getFloat(i));
        lastSurfaceOrientation = renderingParams.getSurfaceOrientation();
        if (triangleArr != null) {
            triangleArr.setCoordRefFloat(coords.getData());
            triangleArr.setNormalRefFloat(normals.getData());
        }
        if (edgeArr != null)
            edgeArr.setCoordRefFloat(coords.getData());
        if (nodeArr != null)
            nodeArr.setCoordRefFloat(coords.getData());
        if (detach)
            geometry.postattach();
    }

    private void updateTextureCoords()
    {
        boolean detach = geometry.postdetach();
        uvData = new FloatLargeArray(2 * nNodes, true);
        ColorComponentParams[] tParams = new ColorComponentParams[]{
            dataMappingParams.getUParams(), dataMappingParams.getVParams()
        };
        for (int i = 0; i < tParams.length; i++)
            if (tParams[i].getDataComponentSchema() != null)
                uvData = new FloatLargeArray(TextureMapper.map(regularField, tParams[i], uvData.getData(), i));
        if (triangleArr != null)
            triangleArr.setTexCoordRefFloat(0, uvData.getData());
        appearance.setTexture(dataMappingParams.getTexture());
        if (detach)
            geometry.postattach();
    }

    private class MapColors implements Runnable
    {
        int nThreads = 1;
        int iThread = 0;
        int nNds = (int) regularField.getNNodes();

        public MapColors(int nThreads, int iThread)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
        }

        @Override
        public void run()
        {
            int kstart = nNds * iThread / nThreads;
            int kend = nNds * (iThread + 1) / nThreads;
            //map from kstart to kend (excluding kend)
            if (iThread == 0) {
                int a = 1;
            }
            ColorMapper.map(regularField, dataMappingParams, kstart, kend, kstart, dataMappingParams.getDefaultColor(), colors.getData());
            ColorMapper.mapTransparency(regularField, dataMappingParams.getTransparencyParams(), kstart, kend, colors.getData());
            if (timeRange != null)
                ColorMapper.mapTimeValidityTransparency(timeRange, currentT, kstart, kend, kstart, colors.getData());
            if (renderingParams.isTransparentlyRenderedMaskedNodes() && regularField.hasMask())
                ColorMapper.setTransparencyMask(colors.getData(), regularField.getCurrentMask(), kstart, kend);

        }
    }

    private void updateTextureFromColormap()
    {
        BufferedImage textureImage = new BufferedImage(dims[0], dims[1], BufferedImage.TYPE_INT_ARGB);
        int[] imageData = ((DataBufferInt) textureImage.getRaster().getDataBuffer()).getData();
        ColorMapper.map(regularField, dataMappingParams, new Color3f(0, 0, 0), imageData, false, 0, 0);
        textureImage.setRGB(0, 0, dims[0], dims[1], imageData, 0, dims[0]);
        texture = new Texture2D(Texture2D.BASE_LEVEL, Texture2D.RGBA,
                                textureImage.getWidth(), textureImage.getHeight());
        texture.setBoundaryModeS(Texture.CLAMP_TO_EDGE); // Texture.CLAMP_TO_BOUNDARY ??
        texture.setBoundaryModeT(Texture.CLAMP_TO_EDGE);
        texture.setImage(0, new ImageComponent2D(ImageComponent2D.FORMAT_RGBA, textureImage));
        if (appearance.getMaterial() != null) {
            texture.setMinFilter(Texture.NICEST);
            texture.setMagFilter(Texture.NICEST);
            appearance.setTexture(texture);
        }
    }

    public void updateColors()
    {
//        if (canUseTexture())
//            updateTextureFromColormap();
//        else
        {
            boolean detach = geometry.postdetach();
            if (colors == null || colors.length() != 4 * regularField.getNNodes())
                colors = new ByteLargeArray(4 * regularField.getNNodes(), true);
            timeRange = null;
            for (DataArray da : regularField.getComponents())
                if (da.getUserData() != null &&
                        da.getUserData().length == 1 &&
                        "valid time range".equalsIgnoreCase(da.getUserData()[0])) {
                    timeRange = da.getRawIntArray().getData();
                    currentT = regularField.getCurrentTime();
                }
            int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
            Thread[] workThreads = new Thread[nThreads];
            for (int iThread = 0; iThread < nThreads; iThread++) {
                workThreads[iThread] = new Thread(new MapColors(nThreads, iThread));
                workThreads[iThread].start();
            }
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {

                }
            if (triangleArr != null && (triangleArr.getVertexFormat() & GeometryArray.COLOR_4) != 0)
                triangleArr.setColorRefByte(colors.getData());
            if (edgeArr != null)
                edgeArr.setColorRefByte(colors.getData());
            if (nodeArr != null)
                nodeArr.setColorRefByte(colors.getData());

            if (image2D != null) {
                image2D.setColors(colors.getData());
            }
            if (edges2D != null) {
                edges2D.setColors(colors.getData());
            }
            if (points2D != null) {
                points2D.setColors(colors.getData());
            }
            if (detach)
                geometry.postattach();
        }
    }

    @Override
    public void updateDataMap()
    {
//        if (canUseTexture())
//            updateTextureFromColormap();
//        else
        {
            boolean detach = geometry.postdetach();
            if (dataMappingParams.getColorMode() == DataMappingParams.COLORMAPPED ||
                    dataMappingParams.getColorMode() == DataMappingParams.COLORMAPPED2D ||
                    dataMappingParams.getColorMode() == DataMappingParams.COLORED)
                updateColors();
            else if (dataMappingParams.getColorMode() == DataMappingParams.UVTEXTURED)
                updateTextureCoords();
            if (detach)
                geometry.postattach();
        }
    }

    public void updateShapes()
    {
        updateGeometry();
        if (geometries.getParent() == null)
            transformedGeometries.addChild(geometries);
        if (transformedGeometries.getParent() == null)
            geometry.addChild(transformedGeometries);
    }

    public void updateData()
    {
        boolean restructure = false;
        LogicLargeArray mask = field.getCurrentMask();
        if (mask != null)
            if (lastMask == null || lastMask.length() != mask.length()) {
                restructure = true;
                lastMask = mask;
            } else {
                for (int i = 0; i < mask.length(); i++)
                    if (mask.getByte(i) != lastMask.getByte(i)) {
                        restructure = true;
                        break;
                    }
            }
        updateExtents();
        if (restructure)
            updateShapes();
        else {
            updateColors();
            updateCoords();
        }
    }

    public void updateGeometry(RegularField inField)
    {
        if (regularField != inField && !setField(inField))
            return;
        updateGeometry();
    }

    public void createTexturedGeometry()
    {
        boolean detach = geometry.postdetach();
        if (surfaceShape != null)
            surfaceShape.removeAllGeometries();
        if (lineShape != null)
            lineShape.removeAllGeometries();
        if (pointShape != null)
            pointShape.removeAllGeometries();
        if (frameShape != null)
            frameShape.removeAllGeometries();
        triangleArr = null;
        edgeArr = null;
        nodeArr = null;
        boxArr = null;
        edges2D = null;
        points2D = null;
        int[] triangles;
        float[][] affine = regularField.getAffine();
        float[] nodeCoords = new float[12];
        for (int i = 0; i < 3; i++) {
            nodeCoords[i] = affine[3][i];
            nodeCoords[i + 3] = affine[3][i] + (dims[0] - 1) * affine[0][i];
            nodeCoords[i + 6] = affine[3][i] + (dims[1] - 1) * affine[1][i];
            nodeCoords[i + 9] = affine[3][i] + (dims[0] - 1) * affine[0][i] + (dims[1] - 1) * affine[1][i];
        }
        float[] nodeNormals = new float[12];
        float[] normal = new float[]{
            affine[0][1] * affine[1][2] - affine[0][2] * affine[1][1],
            affine[0][2] * affine[1][0] - affine[0][0] * affine[1][2],
            affine[0][0] * affine[1][1] - affine[0][1] * affine[1][0]
        };
        float nNorm = (float) sqrt(normal[0] * normal[0] + normal[1] * normal[1] + normal[2] * normal[2]);
        if (surfaceOrientation == 1)
            for (int i = 0; i < normal.length; i++)
                nodeNormals[i] = nodeNormals[i + 3] = nodeNormals[i + 6] = nodeNormals[i + 9] = normal[i] / nNorm;
        else
            for (int i = 0; i < normal.length; i++)
                nodeNormals[i] = nodeNormals[i + 3] = nodeNormals[i + 6] = nodeNormals[i + 9] = -normal[i] / nNorm;
        if (surfaceOrientation == 1)
            triangles = new int[]{1, 0, 3, 2};
        else
            triangles = new int[]{0, 1, 2, 3};
        triangleArr = new IndexedTriangleStripArray(4,
                                                    GeometryArray.COORDINATES | GeometryArray.NORMALS |
                                                    GeometryArray.TEXTURE_COORDINATE_2 | GeometryArray.USE_COORD_INDEX_ONLY |
                                                    GeometryArray.BY_REFERENCE, 4, new int[]{4});
        setStandardCapabilities(triangleArr);
        triangleArr.setCoordinateIndices(0, triangles);
        triangleArr.setCoordRefFloat(nodeCoords);
        triangleArr.setNormalRefFloat(nodeNormals);
        float[] uv = new float[]{0, 1, 1, 1, 0, 0, 1, 0};
        triangleArr.setTexCoordRefFloat(0, uv);
        surfaceShape.addGeometry(triangleArr);

        BufferedImage textureImage = new BufferedImage(dims[0], dims[1], BufferedImage.TYPE_INT_ARGB);
        int[] imageData = ((DataBufferInt) textureImage.getRaster().getDataBuffer()).getData();
        ColorMapper.map(regularField, dataMappingParams, new Color3f(0, 0, 0), imageData, false, 0, 0);
        textureImage.setRGB(0, 0, dims[0], dims[1], imageData, 0, dims[0]);
        texture = new Texture2D(Texture2D.BASE_LEVEL, Texture2D.RGBA,
                                textureImage.getWidth(), textureImage.getHeight());
        texture.setImage(0, new ImageComponent2D(ImageComponent2D.FORMAT_RGBA, textureImage));
        if (appearance.getMaterial() != null) {
            texture.setMinFilter(Texture.NICEST);
            texture.setMagFilter(Texture.NICEST);
            appearance.setTexture(texture);
        }

        surfaceShape.setAppearance(appearance);
        structureChanged = false;
        if (detach)
            geometry.postattach();
    }

    @Override
    public void updateGeometry()
    {
        int mode = renderingParams.getDisplayMode();
        boolean showSurface = (mode & RenderingParams.SURFACE) != 0;
        boolean showEdges = (mode & RenderingParams.EDGES) != 0;
        boolean showNodes = (mode & RenderingParams.NODES) != 0;
        boolean showImage = (mode & RenderingParams.IMAGE) != 0;
        boolean showBox = (mode & RenderingParams.OUTLINE_BOX) != 0;
        boolean detach = geometry.postdetach();
        if (surfaceShape != null)
            surfaceShape.removeAllGeometries();
        if (lineShape != null)
            lineShape.removeAllGeometries();
        if (pointShape != null)
            pointShape.removeAllGeometries();
        if (frameShape != null)
            frameShape.removeAllGeometries();
        triangleArr = null;
        edgeArr = null;
        nodeArr = null;
        boxArr = null;

        outObj2DStruct.removeAllChildren();
        outObj2DStruct.setName("regular field 2d geometry");

        edges2D = null;
        points2D = null;
//        String[] shModes = new String[]{"unshaded", "smooth", "flat", "background"};
//        if (showSurface)
//            System.out.print("surfaces " + shModes[renderingParams.getShadingMode()] + ", ");
//        if (showEdges)
//            System.out.print("edges, ");
//        if (showNodes)
//            System.out.print("nodes, ");
//        System.out.println("");

//        if (
////                !regularField.hasCoords() && regularField.getNNodes()  >  textureRenderingLimit &&
//            showSurface && !showEdges && !showNodes &&
//            dataMappingParams.getColorMode() == DataMappingParams.COLORMAPPED) {
//            appearance = renderingParams.getAppearance().cloneNodeComponent(true);
//            appearance.setUserData(this);
//            createTexturedGeometry();
//            if (detach)
//                geometry.postattach();
//            return;
//        }
        if (showSurface) {
            int cMode = dataMappingParams.getColorMode();
            if (renderingParams.getShadingMode() == RenderingParams.BACKGROUND)
                cMode = DataMappingParams.UNCOLORED;
            switch (cMode) {
                case DataMappingParams.COLORMAPPED:
                case DataMappingParams.RGB:
                    generateColoredTriangles();
                    appearance.setTexture(null);
                    break;
                case DataMappingParams.UVTEXTURED:
                    generateTexturedTriangles();
                    appearance.setTexture(dataMappingParams.getTexture());
                    break;
                case DataMappingParams.UNCOLORED:
                    generateTriangles();
                    appearance.setTexture(null);
                    break;
            }
            surfaceShape.addGeometry(triangleArr);
        }
        if (showEdges) {
            switch (dataMappingParams.getColorMode()) {
                case DataMappingParams.COLORMAPPED:
                case DataMappingParams.RGB:
                    generateColoredEdges();
                    break;
                case DataMappingParams.UVTEXTURED:
                case DataMappingParams.UNCOLORED:
                    generateEdges();
                    break;
            }
            lineShape.addGeometry(edgeArr);
        }
        if (showNodes) {
            switch (dataMappingParams.getColorMode()) {
                case DataMappingParams.COLORMAPPED:
                case DataMappingParams.RGB:
                    generateColoredNodes();
                    break;
                case DataMappingParams.UVTEXTURED:
                case DataMappingParams.UNCOLORED:
                    generateNodes();
                    break;
            }
            pointShape.addGeometry(nodeArr);
        }
        if (showBox) {
            //updateExtents();
            float[][] ext = regularField.getExtents();
            float[] boxVerts = new float[24];
            boxArr = new IndexedLineStripArray(8, GeometryArray.COORDINATES, 24, new int[]{
                2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
            });
            boxArr.setCoordinateIndices(0, new int[]{
                0, 1, 2, 3, 4, 5, 6, 7, 0, 2, 1, 3, 4, 6, 5, 7, 0, 4, 1, 5, 2, 6, 3, 7
            });
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 2; j++)
                    for (int k = 0; k < 2; k++) {
                        int m = 3 * (4 * i + 2 * j + k);
                        boxVerts[m] = ext[k][0];
                        boxVerts[m + 1] = ext[j][1];
                        boxVerts[m + 2] = ext[i][2];
                    }
            boxArr.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
            boxArr.setCoordinates(0, boxVerts);
            frameShape.addGeometry(boxArr);
        }

        structureChanged = false;
        updateCoords();
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
            case DataMappingParams.RGB:
                updateColors();
                break;
            case DataMappingParams.UVTEXTURED:
                updateTextureCoords();
                break;
            case DataMappingParams.UNCOLORED:
                break;
        }
        appearance = renderingParams.getAppearance();
        appearance.setUserData(this);
        lineAppearance = renderingParams.getLineAppearance();
        lineAppearance.setUserData(this);
        if (appearance.getMaterial() != null) {
            appearance.getMaterial().setAmbientColor(dataMappingParams.getDefaultColor());
            appearance.getMaterial().setDiffuseColor(dataMappingParams.getDefaultColor());
            appearance.getMaterial().setSpecularColor(dataMappingParams.getDefaultColor());
        }
        if (showImage) {
            createImage2D();
            GeometryObject2DStruct imgStruct = new GeometryObject2DStruct(image2D);
            imgStruct.setName("regular field 2D image");
            outObj2DStruct.addChild(imgStruct);
        }
        if (showEdges) {
            updateEdges2D();
            GeometryObject2DStruct edgStruct = new GeometryObject2DStruct(edges2D);
            edgStruct.setName("regular field 2D edges");
            outObj2DStruct.addChild(edgStruct);
        }
        if (showNodes) {
            updatePoints2D();
            GeometryObject2DStruct ptsStruct = new GeometryObject2DStruct(points2D);
            ptsStruct.setName("regular field 2D nodes");
            outObj2DStruct.addChild(ptsStruct);
        }
        if (showBox) {
            updateBox2D();
            GeometryObject2DStruct boxStruct = new GeometryObject2DStruct(box2D);
            boxStruct.setName("regular field 2D box");
            outObj2DStruct.addChild(boxStruct);
        }

        bgrColor = ColorMapper.convertColorToColor3f(renderingParams.getBackgroundColor());
        if (renderingParams.getShadingMode() == RenderingParams.BACKGROUND) {
            appearance.getColoringAttributes().setColor(bgrColor);
            appearance.getMaterial().setDiffuseColor(bgrColor);
        } else
            appearance.getColoringAttributes().setColor(renderingParams.getDiffuseColor());
        lineAppearance.getColoringAttributes().setColor(renderingParams.getDiffuseColor());
        lineAppearance.setLineAttributes(renderingParams.getLineAppearance().getLineAttributes());
        if (dataMappingParams.getColorMode() != DataMappingParams.UVTEXTURED)
            updateColors();
        texture = dataMappingParams.getTexture();
        if (texture != null && dataMappingParams.getColorMode() == DataMappingParams.UVTEXTURED)
            appearance.setTexture(texture);
        pointShape.setAppearance(lineAppearance);
        lineShape.setAppearance(lineAppearance);
        surfaceShape.setAppearance(appearance);
        frameShape.setAppearance(lineAppearance);
        if (detach)
            geometry.postattach();

    }

    @Override
    public OpenBranchGroup getGeometry()
    {
        updateGeometry();
        return geometry;
    }

    @Override
    public OpenBranchGroup getGeometry(Field inField)
    {
        if (!(inField instanceof RegularField))
            return null;
        regularField = (RegularField) inField;
        return getGeometry((RegularField) inField);
    }

    @Override
    public void createGeometry(Field inField)
    {
        if (inField instanceof RegularField)
            updateGeometry((RegularField) inField);
    }

    @Override
    public void updateGeometry(Field inField)
    {
        if (inField instanceof RegularField)
            updateGeometry((RegularField) inField);
    }

    private void validateColorMode()
    {
        colorMode = dataMappingParams.getColorMode();
        ArrayList<DataArraySchema> componentSchemas = regularField.getSchema().getComponentSchemas();
        ArrayList<DataArraySchema> pseudoCmpSchemas = regularField.getSchema().getPseudoComponentSchemas();
        // check if color mode and selected components combination is valid; fall back to UNCOLORED otherwise
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
                if (componentSchemas.contains(dataMappingParams.getColorMap0().getDataComponentSchema()))
                    return;
                break;
            case DataMappingParams.RGB:
                if (componentSchemas.contains(dataMappingParams.getRedParams().getDataComponentSchema()) ||
                        componentSchemas.contains(dataMappingParams.getGreenParams().getDataComponentSchema()) ||
                        componentSchemas.contains(dataMappingParams.getBlueParams().getDataComponentSchema()))
                    return;
                break;
            case DataMappingParams.UVTEXTURED:
                if ((componentSchemas.contains(dataMappingParams.getUParams().getDataComponentSchema()) ||
                        pseudoCmpSchemas.contains(dataMappingParams.getUParams().getDataComponentSchema())) &&
                        (componentSchemas.contains(dataMappingParams.getUParams().getDataComponentSchema()) ||
                        pseudoCmpSchemas.contains(dataMappingParams.getUParams().getDataComponentSchema())))
                    return;
                break;
        }
        colorMode = DataMappingParams.UNCOLORED;
    }

    private void createImage2D()
    {
        if (image2D != null)
            image2D.clearParamListeners();

        image2D = new ImageArray2D();
        image2D.setRenderingParams(renderingParams);

        image2D.setField(regularField);
    }

    private void updateEdges2D()
    {
        if (edges2D == null) {
            edges2D = new LineArray2D();
            edges2D.setRenderingParams(renderingParams);
        }
        edges2D.setField(regularField);
        //System.out.println("geometry "+this.getName()+" updated 2D edges object");
    }

    private void updatePoints2D()
    {
        if (points2D == null) {
            points2D = new PointArray2D();
            points2D.setRenderingParams(renderingParams);
        }
        points2D.setField(regularField);
        //System.out.println("geometry "+this.getName()+" updated 2D points object");
    }

    private void updateBox2D()
    {
        if (box2D == null) {
            box2D = new BoxArray2D();
            box2D.setRenderingParams(renderingParams);
        }
        box2D.setField(regularField);
        //System.out.println("geometry "+this.getName()+" updated 2D points object");
    }

    ColorListener bgrColorListener = new ColorListener()
    {
        @Override
        public void colorChoosen(ColorEvent e)
        {
            bgrColor = ColorMapper.convertColorToColor3f(e.getSelectedColor());
            if (renderingParams.getShadingMode() == RenderingParams.BACKGROUND) {
                renderingParams.setDiffuseColor(bgrColor);
                renderingParams.setColor(ColorMapper.convertColor3fToColor(bgrColor));
                if (appearance != null) {
                    if (appearance.getColoringAttributes() != null)
                        appearance.getColoringAttributes().setColor(bgrColor);
                    appearance.getMaterial().setAmbientColor(bgrColor);
                    appearance.getMaterial().setDiffuseColor(bgrColor);
                    appearance.getMaterial().setEmissiveColor(bgrColor);
                    appearance.getMaterial().setSpecularColor(0, 0, 0);
                }
                ignoreUpdate = false;
                updateGeometry();
            }
        }
    };

    @Override
    public ColorListener getBackgroundColorListener()
    {
        return bgrColorListener;
    }

}
