/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.applications;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import org.visnow.vn.application.application.Application;
import org.visnow.vn.engine.Engine;
import org.visnow.vn.engine.core.CoreName;
import org.visnow.vn.engine.core.Link;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.main.ModuleBox;
import org.visnow.vn.geometries.viewer3d.Display3DFrame;
import org.visnow.vn.lib.basic.viewers.Viewer3D.Viewer3D;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
abstract public class Application3DTemplate
{
    protected JPanel guiPanel = new JPanel();
    protected JTabbedPane modulePane = new JTabbedPane();
    protected Application application = new Application("viewer");
    protected Engine engine = application.getEngine();

    protected final ModuleCore addModule(String name, String classDesc)
    {
        try {
            ModuleCore core = application.getLibraries().generateCore(new CoreName("internal", classDesc));
            core.setHideGUIwhenNoData(false);
            core.setApplication(application);
            ModuleBox mb = new ModuleBox(engine, name, core);
            engine.getModules().put(name, mb);
            mb.run();
            core.onInitFinished();
            return core;
        } catch (Exception e) {
            return null;
        }
    }

    protected final void newModule(String name, String classDesc)
    {
        modulePane.addTab(name, addModule(name, classDesc).getPanel());
    }

    protected final Link addLink(String source, String sourcePort, String target, String targetPort)
    {
        try {
            return new Link(engine.getModule(source).getOutput(sourcePort),
                                 engine.getModule(target).getInput(targetPort));
        } catch (Exception e) {
            return null;
        }
    }

    protected Application3DTemplate()
    {
        modulePane.setMinimumSize(new java.awt.Dimension(220, 500));
        modulePane.setPreferredSize(new java.awt.Dimension(240, 600));
        
        guiPanel.setMinimumSize(new java.awt.Dimension(220, 600));
        guiPanel.setLayout(new BorderLayout(5,5));
        guiPanel.add(modulePane, BorderLayout.CENTER);

        Display3DFrame frame = ((Viewer3D) addModule("viewer 3D", "org.visnow.vn.lib.basic.viewers.Viewer3D.Viewer3D")).getWindow();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(guiPanel, BorderLayout.WEST);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
    }
    
    protected void addPanel(JPanel panel)
    {
        guiPanel.add(panel, BorderLayout.NORTH);
    }

}
