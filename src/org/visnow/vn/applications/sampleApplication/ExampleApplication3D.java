/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.applications.sampleApplication;

import java.util.HashMap;
import java.util.Map;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.applications.Application3DTemplate;
import org.visnow.vn.engine.core.Link;
import org.visnow.vn.system.main.VisNow;

/**
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ExampleApplication3D extends Application3DTemplate
{
    private Map <String, Link> displayLinks = new HashMap<>();
    private ModuleActivationPanel modPanel  = new ModuleActivationPanel();
    
    public ExampleApplication3D()
    {
        super();

        newModule("VN fieldreader", "org.visnow.vn.lib.basic.readers.ReadVisNowField.ReadVisNowField");
        newModule("slice", "org.visnow.vn.lib.basic.mappers.RegularFieldSlice.RegularFieldSlice");
        newModule("isosurface", "org.visnow.vn.lib.basic.mappers.Isosurface.Isosurface");
        newModule("isolines", "org.visnow.vn.lib.basic.mappers.Isolines.Isolines");
        newModule("glyphs", "org.visnow.vn.lib.basic.mappers.Glyphs.Glyphs");

        addLink("VN fieldreader", "regularOutField", "slice", "inField");
        addLink("VN fieldreader", "regularOutField", "isosurface", "inField");
        addLink("VN fieldreader", "regularOutField", "glyphs", "inField");
        addLink("slice", "outField", "isolines", "inField");
        addLink("VN fieldreader", "outObj", "viewer 3D", "inObject");
        displayLinks.put("slice", addLink("slice", "outObj", "viewer 3D", "inObject"));
        displayLinks.put("isosurface", addLink("isosurface", "outObj", "viewer 3D", "inObject"));
        displayLinks.put("isolines", addLink("isolines", "outObj", "viewer 3D", "inObject"));
        displayLinks.put("glyphs", addLink("glyphs", "outObj", "viewer 3D", "inObject"));
        
        modPanel.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                if (modPanel.showGlyphs() && !displayLinks.containsKey("glyphs")) 
                    displayLinks.put("glyphs", addLink("glyphs", "outObj", "viewer 3D", "inObject"));
                else if (!modPanel.showGlyphs() && displayLinks.containsKey("glyphs")) {
                    Link link = displayLinks.get("glyphs");
                    link.getInput().removeLink(link, true);
                    displayLinks.remove("glyphs");
                }
                if (modPanel.showIsosurface()&& !displayLinks.containsKey("isosurface")) 
                     displayLinks.put("isosurface", addLink("isosurface", "outObj", "viewer 3D", "inObject"));
                else if (!modPanel.showIsosurface() && displayLinks.containsKey("isosurface")) {
                    Link link = displayLinks.get("isosurface");
                    link.getInput().removeLink(link, true);
                    displayLinks.remove("isosurface");
                }
                if (modPanel.showSlice() && !displayLinks.containsKey("slice"))
                     displayLinks.put("slice", addLink("slice", "outObj", "viewer 3D", "inObject"));
                else if (!modPanel.showSlice() && displayLinks.containsKey("slice")) {
                    Link link = displayLinks.get("slice");
                    link.getInput().removeLink(link, true);
                    displayLinks.remove("slice");
                }
                if (modPanel.showIsolines() && !displayLinks.containsKey("isolines")) 
                     displayLinks.put("isolines", addLink("isolines", "outObj", "viewer 3D", "inObject"));
                else if (!modPanel.showIsolines() && displayLinks.containsKey("isolines")) {
                    Link link = displayLinks.get("isolines");
                    link.getInput().removeLink(link, true);
                    displayLinks.remove("isolines");
                }
                    
                    
            }
        });
        addPanel(modPanel);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(final String args[])
    {
        VisNow.mainBlocking(args, false);
        ExampleApplication3D simpleViewer = new ExampleApplication3D();
    }
        
}
