/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.applications;

import java.awt.Color;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.viewer3d.Display3DPanel;
import org.visnow.vn.lib.basic.filters.VolumeSegmentation.VolumeSegmentation;
import org.visnow.vn.lib.basic.mappers.VolumeRenderer.FieldPick.FieldPickEvent;
import org.visnow.vn.lib.basic.mappers.VolumeRenderer.FieldPick.FieldPickListener;
import org.visnow.vn.lib.basic.mappers.VolumeRenderer.VolumeRenderer;
import org.visnow.vn.lib.basic.readers.medreaders.ReadDICOM.ReadDICOMShared;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.DataProvider.DataProvider;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.DataProvider.DataProviderParams;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.FieldDisplay3DFrame;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.GUI;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.CalculableParams;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.GeometryParams;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.Glypher;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.GeometryToolsStorage;
import org.visnow.vn.system.main.VisNow;
import static org.visnow.vn.lib.basic.readers.medreaders.ReadDICOM.ReadDICOMShared.*;

/**
 * @author Krzysztof Nowinski (know@icm.edu.pl) University of Warsaw, Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */
public class DicomSegmentation
{

    private static VolumeSegmentation _volumeSegmentation;
    private static FieldDisplay3DFrame _display3DFrame;
    private static VolumeRenderer volumeRenderer;
    private static RegularField _currentField;
    private static GUI _ui;
    private static DataProvider dataProvider;
    private static GeometryParams _gparams;
    private static Glypher _glypher;
    private static org.visnow.vn.lib.basic.readers.medreaders.ReadDICOM.GUI _dicomui;
    private static Parameters _dicomParams;
    private static org.visnow.vn.lib.basic.readers.medreaders.ReadDICOM.ReadDICOMCore _dicomCore;
    private static org.visnow.vn.lib.basic.readers.medreaders.ReadDICOM.ReadProgressDialog _dicomReadProgressDialog;
    private static CalculableParams _cparams = new CalculableParams();
    private static int[] segTransparency = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17,
        17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17,
        17, 17, 17, 17, 17, 17, 17, 17, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 17, 17, 17,
        17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17,};
    private static int[] dataTransparency = new int[]{
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2,
        2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4,
        4, 4, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 7,
        7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9,
        9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11,
        11, 12, 12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 14, 14,
        14, 14, 14, 14, 14, 15, 15, 15, 15, 15, 15, 15, 16, 16, 16, 16,
        16, 16, 17, 17, 17, 17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 18,
        19, 19, 19, 19, 19, 19, 19, 20, 20, 20, 20, 20, 20, 21, 21, 21,
        21, 21, 21, 21, 22, 22, 22, 22, 22, 22, 22, 23, 23, 23, 23, 23,
        23, 24, 24, 24, 24, 24, 24, 24, 25, 25, 25, 25, 25, 25, 25, 26,
        26, 26, 26, 26, 26, 27, 27, 27, 27, 27, 27, 27, 27, 28, 28, 28,
        28, 28, 28, 28, 28, 29, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
    };
    private static float[] segTransparencyMap = new float[segTransparency.length];
    private static float[] dataTransparencyMap = new float[segTransparency.length];

    public static void main(final String args[])
    {
        java.awt.EventQueue.invokeLater(
            new Runnable()
            {
                @Override
                public void run()
                {
                    for (int i = 0; i < segTransparencyMap.length; i++) {
                        segTransparencyMap[i] = segTransparency[i] / 255.f;
                        dataTransparencyMap[i] = dataTransparency[i] / 255.f;
                    }
                    VisNow.mainBlocking(args, false);
                    _volumeSegmentation = new VolumeSegmentation(false)
                    {
                        @Override
                        public void renderVolume(int vol)
                        {
                            switch (vol) {
                                case 0:
                                    if (inField != null)
                                        volRender.setInField(inField);
                                    volRender.getDataMappingParams().getTransparencyParams().setPrototypeMap(dataTransparencyMap);
                                    break;
                                case 1:
                                    if (distField != null)
                                        volRender.setInField(distField);
                                    break;
                                case 2:
                                    if (outField != null)
                                        volRender.setInField(outField);
                                    volRender.getDataMappingParams().getTransparencyParams().setPrototypeMap(segTransparencyMap);
                                    break;
                                default:
                                    if (inField != null)
                                        volRender.setInField(inField);
                                    volRender.getDataMappingParams().getTransparencyParams().setPrototypeMap(dataTransparencyMap);
                            }
                        }

                    };
                    _display3DFrame = _volumeSegmentation.getDisplay3DFrame();
                    volumeRenderer = _volumeSegmentation.getVolRender();
                    volumeRenderer.getDataMappingParams().getTransparencyParams().setPrototypeMap(dataTransparencyMap);
                    dataProvider = _volumeSegmentation.getDp();
                    _gparams = _volumeSegmentation.getGparams();
                    _currentField = null;
                    _ui = new GUI();

                    _glypher = new Glypher();
                    _dicomParams = new Parameters(ReadDICOMShared.getDefaultParameters());
                    DataProviderParams dpParams = dataProvider.getParams();

                    _dicomui = new org.visnow.vn.lib.basic.readers.medreaders.ReadDICOM.GUI();
                    _dicomParams.set(READ_AS, ReadDICOMShared.READ_AS_BYTES);
                    _dicomParams.set(LOW, 0);
                    _dicomParams.set(HIGH, 64);
                    _dicomParams.set(INTERPOLATE_DATA, true);
                    _dicomParams.set(INTERPOLATE_DATA_VOXEL_SIZE_FROM, ReadDICOMShared.VOXELSIZE_FROM_MANUALVALUE);
                    _dicomui.setParameters(_dicomParams);
                    _dicomCore = new org.visnow.vn.lib.basic.readers.medreaders.ReadDICOM.ReadDICOMCore();
                    _dicomCore.setParameters(_dicomParams);

                    _dicomReadProgressDialog = new org.visnow.vn.lib.basic.readers.medreaders.ReadDICOM.ReadProgressDialog(_display3DFrame, true, _dicomCore);

                    _dicomParams.addChangeListener(new ChangeListener()
                    {
                        @Override
                        public void stateChanged(ChangeEvent e)
                        {
                            if (_dicomParams.get(FILE_LIST).length < 1)
                                return;

                            _dicomReadProgressDialog.setVisible(true);

                            _currentField = _dicomCore.getOutField();
                            if (_currentField == null)
                                return;
                            _currentField.getComponent(0).setPreferredRanges(0, 255, 0, 64);
                            dataProvider.setInField(_currentField);
                            dataProvider.centerSlices();

                            _volumeSegmentation.setInField(_currentField);
                            _display3DFrame.locateDividers();
                            _display3DFrame.getDisplay3DPanel().reset();
                            _display3DFrame.getDisplay3DPanel().setReperType(Display3DPanel.ReperType.MEDICAL);

                            _gparams.setInField(_currentField);
                            _gparams.setInfoString(_dicomParams.get(INFO_STRING));
                            _gparams.setPatientString(_dicomParams.get(PATIENT_NAME));

                            DataProviderParams dpParams = dataProvider.getParams();
                            dpParams.setSimpleOverlay(true);
                            dpParams.setSimpleOverlayColor(new Color(150, 150, 255));
                            dpParams.setSimpleOverlayLowUp(2, 200);
                            dpParams.setSimpleOverlayComponent(0);
                            dpParams.setSimpleOverlayInvert(true);
                            _volumeSegmentation.setBackgroundThresholdRange(2, 200, true);
                        }
                    });

                    _display3DFrame.setTitle("Volume segmentation");
                    _display3DFrame.setLocation(100, 100);
                    _display3DFrame.setDataProvider(dataProvider);

                    _display3DFrame.setVisible(true);
                    _display3DFrame.getDisplay3DPanel().setPick3DActive(false);
                    _display3DFrame.getParams().setSelectedGeometryTool(GeometryToolsStorage.GEOMETRY_TOOL_POINT, true);
                    _display3DFrame.getParams().setPlanes3DVisible(false);

                    _glypher.setParams(_gparams);
                    _display3DFrame.setGeometryParams(_gparams);
                    _display3DFrame.setCalculableParams(_cparams);
                    _display3DFrame.getDisplay3DPanel().addChild(_glypher.getGlyphsObject());
                    _display3DFrame.addWindowListener(new WindowAdapter()
                    {
                        public void windowClosing(WindowEvent e)
                        {
                            VisNow.get().finish();
                            System.exit(0);
                        }
                    });

                    _glypher.addChangeListener(new ChangeListener()
                    {
                        public void stateChanged(ChangeEvent e)
                        {
                            _display3DFrame.getDisplay3DPanel().repaint();
                        }
                    });

                    volumeRenderer.addPick3DListener(new FieldPickListener()
                    {
                        @Override
                        public void handlePick3D(FieldPickEvent e)
                        {
                            _gparams.addPoint(e.getIndices());
                        }
                    });

                    _ui.addChangeListener(new ChangeListener()
                    {
                        @Override
                        public void stateChanged(ChangeEvent evt)
                        {
                            _display3DFrame.setVisible(true);
                        }
                    });

                    _display3DFrame.insertUI(_dicomui, "DICOM");
                    _display3DFrame.getDisplay3DPanel().setPick3DActive(false);
                    _display3DFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                    _display3DFrame.setExtendedState(Frame.MAXIMIZED_BOTH);
                    _display3DFrame.setVisible(true);
                }
            });
    }

    private DicomSegmentation()
    {
    }
}
