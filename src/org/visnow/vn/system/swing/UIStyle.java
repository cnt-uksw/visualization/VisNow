/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.system.swing;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import javax.swing.UIManager;
import org.apache.log4j.Logger;

//TODO: switch to class, extract VN keys
/**
 * Style additional to used L&F
 *
 * @author szpak
 */
public enum UIStyle
{
    vn_warningColor("vn.warningColor"),
    vn_SectionHeader_select("vn.SectionHeader.select"),
    vn_ExtendedSlider_font("vn.ExtendedSlider.font");

    private static final Logger LOGGER = Logger.getLogger(UIStyle.class);

    private final String key;
    private static boolean initialized = false;

    private UIStyle(String key)
    {
        this.key = key;
    }

    public static void initStyle()
    {

        Font labelFont = new javax.swing.plaf.FontUIResource(new Font("Dialog", Font.PLAIN, 12));
        Font sliderFont = new javax.swing.plaf.FontUIResource(new Font("Dialog", Font.PLAIN, 11));
        Font titledBorderFont = new javax.swing.plaf.FontUIResource(new Font("Dialog", Font.PLAIN, 11));
        //        Font font = new javax.swing.plaf.FontUIResource(new Font("Courier", Font.PLAIN, 11));
        //        Font font = new javax.swing.plaf.FontUIResource(new Font("Century Schoolbook L", Font.PLAIN, 11));

        //----------VisNow keys----------//
        UIManager.put(vn_warningColor.getKey(), new Color(255, 0, 0));
        UIManager.put(vn_SectionHeader_select.getKey(), new Color(0xA3B8CC));
        UIManager.put(vn_ExtendedSlider_font.getKey(), sliderFont);

        //----------Swing keys-----------//
        //resize borders from tabbedPane
        //            try {
        UIManager.getDefaults().put(UIManagerKey.TabbedPane_contentBorderInsets.getKey(), new Insets(4, 0, 0, 0));
        //            } catch (Exception ex) {
        //this causes exception on MacOS X
        //TODO: test it (+test if following rules don't throw exception too)

        //            }
        //fonts            
        UIManager.put(UIManagerKey.Button_font.getKey(), labelFont);
        UIManager.put(UIManagerKey.ToggleButton_font.getKey(), labelFont);
        UIManager.put(UIManagerKey.ComboBox_font.getKey(), labelFont);
        UIManager.put(UIManagerKey.CheckBox_font.getKey(), labelFont);
        UIManager.put(UIManagerKey.RadioButton_font.getKey(), labelFont);
        UIManager.put(UIManagerKey.Spinner_font.getKey(), labelFont);
        UIManager.put(UIManagerKey.Label_font.getKey(), labelFont);
        UIManager.put(UIManagerKey.TitledBorder_font.getKey(), titledBorderFont);
        UIManager.put(UIManagerKey.TabbedPane_font.getKey(), labelFont);
        UIManager.put(UIManagerKey.List_font.getKey(), labelFont);
        //
        UIManager.put(UIManagerKey.Slider_font.getKey(), sliderFont);

        //            UIManager.put(UIManagerKey.TitledBorder_titleColor.getKey(), new Color(0xaaaaaa));
        //            UIManager.put(UIManagerKey.TitledBorder_titleColor.getKey(), ((Color)UIManager.get(UIManagerKey.TitledBorder_titleColor.getKey())).brighter().brighter());
        UIManager.put(UIManagerKey.MenuItem_font.getKey(), labelFont);
        UIManager.put(UIManagerKey.MenuBar_font.getKey(), labelFont);
        UIManager.put(UIManagerKey.Menu_font.getKey(), labelFont);
        UIManager.put(UIManagerKey.Panel_font.getKey(), labelFont);
        initialized = true;
        //        }
    }

    public String getKey()
    {
        return key;
    }

    @Override
    public String toString()
    {
        return key;
    }

    public static void main(String[] args)
    {
        UIStyle.initStyle();
    }
}
