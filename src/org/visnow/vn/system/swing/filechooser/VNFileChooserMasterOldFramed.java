/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.system.swing.filechooser;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JDialog;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class VNFileChooserMasterOldFramed
{

    //private JComponentViewer frame;
    private JDialog frame;
    //private JFileChooser ch;
    //JOptionPane p;
    private VNFileChooser panel;
    private Runnable start;
    private final Object lock = new Object();

    private boolean fileChosen = false;

    //<editor-fold defaultstate="collapsed" desc=" [CONSTRUCTOR] ">
    protected VNFileChooserMasterOldFramed(VNFileChooser chooser)
    {
        panel = chooser;
        //frame = new JComponentViewer(panel, "Select file", 800, 500, true, false);
        frame = new JDialog(VisNow.get().getMainWindow(), "Select file", false);
        frame.setContentPane(panel);
        frame.setSize(800, 500);
        frame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosed(WindowEvent e)
            {
                dialogClosed(false);
            }
        });

        //        dialog = new JDialog((JFrame)null, "Select file", true);
        //        dialog.addWindowListener(new WindowAdapter() {
        //            @Override
        //            public void windowClosed(WindowEvent e) {
        //                dialogClosed(false);
        //            }
        //        });
        start = new Runnable()
        {
            public void run()
            {
                runnableRun();
            }
        };
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" start ">
    //    protected boolean showDialog() {
    //        SwingWorker sw = new SwingWorker() {
    //            @Override
    //            protected Object doInBackground() throws Exception {
    //                return showWorkerDialog();
    //            }
    //
    //        };
    //        sw.execute();
    //        try {
    //            return ((Boolean) sw.get()).booleanValue();
    //        } catch (InterruptedException ex) {
    //            return false;
    //        } catch (ExecutionException ex) {
    //            return false;
    //        }
    //    }
    protected boolean showDialog()
    {

        panel.showing();
        frame.setVisible(true);
        frame.setLocationRelativeTo(VisNow.get().getMainWindow());

        /*
         Thread t = new Thread(start);
         t.start();
         try {
         synchronized(lock){lock.wait();}
         } catch (InterruptedException ex) {
         frame.setVisible(false);
         return false;
         }
         */
        boolean ret = fileChosen;
        fileChosen = false;
        return ret;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" runnable ">
    private void runnableRun()
    {
        panel.showing();
        frame.setVisible(true);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" stop ">
    protected void dialogClosed(boolean confirmed)
    {
        fileChosen = confirmed;
        frame.setVisible(false);
        synchronized (lock) {
            lock.notifyAll();
        }
        panel.windowCancel();
    }
    //</editor-fold>

}
