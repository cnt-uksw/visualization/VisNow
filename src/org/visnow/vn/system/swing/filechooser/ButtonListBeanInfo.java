/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.system.swing.filechooser;

import java.beans.*;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class ButtonListBeanInfo extends SimpleBeanInfo
{

    // Bean descriptor//GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor() {
        BeanDescriptor beanDescriptor = new BeanDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class, null); // NOI18N//GEN-HEADEREND:BeanDescriptor

        // Here you can add code for customizing the BeanDescriptor.
        return beanDescriptor;
    }//GEN-LAST:BeanDescriptor

    // Property identifiers//GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_actionMap = 1;
    private static final int PROPERTY_alignmentX = 2;
    private static final int PROPERTY_alignmentY = 3;
    private static final int PROPERTY_ancestorListeners = 4;
    private static final int PROPERTY_autoscrolls = 5;
    private static final int PROPERTY_background = 6;
    private static final int PROPERTY_backgroundSet = 7;
    private static final int PROPERTY_baselineResizeBehavior = 8;
    private static final int PROPERTY_border = 9;
    private static final int PROPERTY_bounds = 10;
    private static final int PROPERTY_colorModel = 11;
    private static final int PROPERTY_columnHeader = 12;
    private static final int PROPERTY_columnHeaderView = 13;
    private static final int PROPERTY_component = 14;
    private static final int PROPERTY_componentCount = 15;
    private static final int PROPERTY_componentListeners = 16;
    private static final int PROPERTY_componentOrientation = 17;
    private static final int PROPERTY_componentPopupMenu = 18;
    private static final int PROPERTY_components = 19;
    private static final int PROPERTY_containerListeners = 20;
    private static final int PROPERTY_cursor = 21;
    private static final int PROPERTY_cursorSet = 22;
    private static final int PROPERTY_debugGraphicsOptions = 23;
    private static final int PROPERTY_displayable = 24;
    private static final int PROPERTY_doubleBuffered = 25;
    private static final int PROPERTY_dropTarget = 26;
    private static final int PROPERTY_enabled = 27;
    private static final int PROPERTY_focusable = 28;
    private static final int PROPERTY_focusCycleRoot = 29;
    private static final int PROPERTY_focusCycleRootAncestor = 30;
    private static final int PROPERTY_focusListeners = 31;
    private static final int PROPERTY_focusOwner = 32;
    private static final int PROPERTY_focusTraversable = 33;
    private static final int PROPERTY_focusTraversalKeys = 34;
    private static final int PROPERTY_focusTraversalKeysEnabled = 35;
    private static final int PROPERTY_focusTraversalPolicy = 36;
    private static final int PROPERTY_focusTraversalPolicyProvider = 37;
    private static final int PROPERTY_focusTraversalPolicySet = 38;
    private static final int PROPERTY_font = 39;
    private static final int PROPERTY_fontSet = 40;
    private static final int PROPERTY_foreground = 41;
    private static final int PROPERTY_foregroundSet = 42;
    private static final int PROPERTY_graphics = 43;
    private static final int PROPERTY_graphicsConfiguration = 44;
    private static final int PROPERTY_height = 45;
    private static final int PROPERTY_hierarchyBoundsListeners = 46;
    private static final int PROPERTY_hierarchyListeners = 47;
    private static final int PROPERTY_horizontalScrollBar = 48;
    private static final int PROPERTY_horizontalScrollBarPolicy = 49;
    private static final int PROPERTY_ignoreRepaint = 50;
    private static final int PROPERTY_inheritsPopupMenu = 51;
    private static final int PROPERTY_inputContext = 52;
    private static final int PROPERTY_inputMap = 53;
    private static final int PROPERTY_inputMethodListeners = 54;
    private static final int PROPERTY_inputMethodRequests = 55;
    private static final int PROPERTY_inputVerifier = 56;
    private static final int PROPERTY_insets = 57;
    private static final int PROPERTY_keyListeners = 58;
    private static final int PROPERTY_layout = 59;
    private static final int PROPERTY_lightweight = 60;
    private static final int PROPERTY_locale = 61;
    private static final int PROPERTY_location = 62;
    private static final int PROPERTY_locationOnScreen = 63;
    private static final int PROPERTY_managingFocus = 64;
    private static final int PROPERTY_maximumSize = 65;
    private static final int PROPERTY_maximumSizeSet = 66;
    private static final int PROPERTY_minimumSize = 67;
    private static final int PROPERTY_minimumSizeSet = 68;
    private static final int PROPERTY_mouseListeners = 69;
    private static final int PROPERTY_mouseMotionListeners = 70;
    private static final int PROPERTY_mousePosition = 71;
    private static final int PROPERTY_mouseWheelListeners = 72;
    private static final int PROPERTY_name = 73;
    private static final int PROPERTY_nextFocusableComponent = 74;
    private static final int PROPERTY_opaque = 75;
    private static final int PROPERTY_optimizedDrawingEnabled = 76;
    private static final int PROPERTY_paintingForPrint = 77;
    private static final int PROPERTY_paintingTile = 78;
    private static final int PROPERTY_parent = 79;
    private static final int PROPERTY_peer = 80;
    private static final int PROPERTY_preferredSize = 81;
    private static final int PROPERTY_preferredSizeSet = 82;
    private static final int PROPERTY_propertyChangeListeners = 83;
    private static final int PROPERTY_registeredKeyStrokes = 84;
    private static final int PROPERTY_requestFocusEnabled = 85;
    private static final int PROPERTY_rootPane = 86;
    private static final int PROPERTY_rowHeader = 87;
    private static final int PROPERTY_rowHeaderView = 88;
    private static final int PROPERTY_showing = 89;
    private static final int PROPERTY_size = 90;
    private static final int PROPERTY_toolkit = 91;
    private static final int PROPERTY_toolTipText = 92;
    private static final int PROPERTY_topLevelAncestor = 93;
    private static final int PROPERTY_transferHandler = 94;
    private static final int PROPERTY_treeLock = 95;
    private static final int PROPERTY_UI = 96;
    private static final int PROPERTY_UIClassID = 97;
    private static final int PROPERTY_valid = 98;
    private static final int PROPERTY_validateRoot = 99;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 100;
    private static final int PROPERTY_verticalScrollBar = 101;
    private static final int PROPERTY_verticalScrollBarPolicy = 102;
    private static final int PROPERTY_vetoableChangeListeners = 103;
    private static final int PROPERTY_viewport = 104;
    private static final int PROPERTY_viewportBorder = 105;
    private static final int PROPERTY_viewportBorderBounds = 106;
    private static final int PROPERTY_viewportView = 107;
    private static final int PROPERTY_visible = 108;
    private static final int PROPERTY_visibleRect = 109;
    private static final int PROPERTY_wheelScrollingEnabled = 110;
    private static final int PROPERTY_width = 111;
    private static final int PROPERTY_x = 112;
    private static final int PROPERTY_y = 113;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor() {
        PropertyDescriptor[] properties = new PropertyDescriptor[114];

        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor("accessibleContext", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getAccessibleContext", null); // NOI18N
            properties[PROPERTY_actionMap] = new PropertyDescriptor("actionMap", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getActionMap", "setActionMap"); // NOI18N
            properties[PROPERTY_alignmentX] = new PropertyDescriptor("alignmentX", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getAlignmentX", "setAlignmentX"); // NOI18N
            properties[PROPERTY_alignmentY] = new PropertyDescriptor("alignmentY", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getAlignmentY", "setAlignmentY"); // NOI18N
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor("ancestorListeners", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getAncestorListeners", null); // NOI18N
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor("autoscrolls", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getAutoscrolls", "setAutoscrolls"); // NOI18N
            properties[PROPERTY_background] = new PropertyDescriptor("background", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getBackground", "setBackground"); // NOI18N
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor("backgroundSet", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isBackgroundSet", null); // NOI18N
            properties[PROPERTY_baselineResizeBehavior] = new PropertyDescriptor("baselineResizeBehavior", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getBaselineResizeBehavior", null); // NOI18N
            properties[PROPERTY_border] = new PropertyDescriptor("border", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getBorder", "setBorder"); // NOI18N
            properties[PROPERTY_bounds] = new PropertyDescriptor("bounds", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getBounds", "setBounds"); // NOI18N
            properties[PROPERTY_colorModel] = new PropertyDescriptor("colorModel", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getColorModel", null); // NOI18N
            properties[PROPERTY_columnHeader] = new PropertyDescriptor("columnHeader", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getColumnHeader", "setColumnHeader"); // NOI18N
            properties[PROPERTY_columnHeaderView] = new PropertyDescriptor("columnHeaderView", org.visnow.vn.system.swing.filechooser.ButtonList.class, null, "setColumnHeaderView"); // NOI18N
            properties[PROPERTY_component] = new IndexedPropertyDescriptor("component", org.visnow.vn.system.swing.filechooser.ButtonList.class, null, null, "getComponent", null); // NOI18N
            properties[PROPERTY_componentCount] = new PropertyDescriptor("componentCount", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getComponentCount", null); // NOI18N
            properties[PROPERTY_componentListeners] = new PropertyDescriptor("componentListeners", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getComponentListeners", null); // NOI18N
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor("componentOrientation", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getComponentOrientation", "setComponentOrientation"); // NOI18N
            properties[PROPERTY_componentPopupMenu] = new PropertyDescriptor("componentPopupMenu", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getComponentPopupMenu", "setComponentPopupMenu"); // NOI18N
            properties[PROPERTY_components] = new PropertyDescriptor("components", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getComponents", null); // NOI18N
            properties[PROPERTY_containerListeners] = new PropertyDescriptor("containerListeners", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getContainerListeners", null); // NOI18N
            properties[PROPERTY_cursor] = new PropertyDescriptor("cursor", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getCursor", "setCursor"); // NOI18N
            properties[PROPERTY_cursorSet] = new PropertyDescriptor("cursorSet", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isCursorSet", null); // NOI18N
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor("debugGraphicsOptions", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions"); // NOI18N
            properties[PROPERTY_displayable] = new PropertyDescriptor("displayable", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isDisplayable", null); // NOI18N
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor("doubleBuffered", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isDoubleBuffered", "setDoubleBuffered"); // NOI18N
            properties[PROPERTY_dropTarget] = new PropertyDescriptor("dropTarget", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getDropTarget", "setDropTarget"); // NOI18N
            properties[PROPERTY_enabled] = new PropertyDescriptor("enabled", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isEnabled", "setEnabled"); // NOI18N
            properties[PROPERTY_focusable] = new PropertyDescriptor("focusable", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isFocusable", "setFocusable"); // NOI18N
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor("focusCycleRoot", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isFocusCycleRoot", "setFocusCycleRoot"); // NOI18N
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor("focusCycleRootAncestor", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getFocusCycleRootAncestor", null); // NOI18N
            properties[PROPERTY_focusListeners] = new PropertyDescriptor("focusListeners", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getFocusListeners", null); // NOI18N
            properties[PROPERTY_focusOwner] = new PropertyDescriptor("focusOwner", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isFocusOwner", null); // NOI18N
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor("focusTraversable", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isFocusTraversable", null); // NOI18N
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor("focusTraversalKeys", org.visnow.vn.system.swing.filechooser.ButtonList.class, null, null, null, "setFocusTraversalKeys"); // NOI18N
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor("focusTraversalKeysEnabled", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled"); // NOI18N
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor("focusTraversalPolicy", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy"); // NOI18N
            properties[PROPERTY_focusTraversalPolicyProvider] = new PropertyDescriptor("focusTraversalPolicyProvider", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isFocusTraversalPolicyProvider", "setFocusTraversalPolicyProvider"); // NOI18N
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor("focusTraversalPolicySet", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isFocusTraversalPolicySet", null); // NOI18N
            properties[PROPERTY_font] = new PropertyDescriptor("font", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getFont", "setFont"); // NOI18N
            properties[PROPERTY_fontSet] = new PropertyDescriptor("fontSet", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isFontSet", null); // NOI18N
            properties[PROPERTY_foreground] = new PropertyDescriptor("foreground", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getForeground", "setForeground"); // NOI18N
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor("foregroundSet", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isForegroundSet", null); // NOI18N
            properties[PROPERTY_graphics] = new PropertyDescriptor("graphics", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getGraphics", null); // NOI18N
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor("graphicsConfiguration", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getGraphicsConfiguration", null); // NOI18N
            properties[PROPERTY_height] = new PropertyDescriptor("height", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getHeight", null); // NOI18N
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor("hierarchyBoundsListeners", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getHierarchyBoundsListeners", null); // NOI18N
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor("hierarchyListeners", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getHierarchyListeners", null); // NOI18N
            properties[PROPERTY_horizontalScrollBar] = new PropertyDescriptor("horizontalScrollBar", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getHorizontalScrollBar", "setHorizontalScrollBar"); // NOI18N
            properties[PROPERTY_horizontalScrollBarPolicy] = new PropertyDescriptor("horizontalScrollBarPolicy", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getHorizontalScrollBarPolicy", "setHorizontalScrollBarPolicy"); // NOI18N
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor("ignoreRepaint", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getIgnoreRepaint", "setIgnoreRepaint"); // NOI18N
            properties[PROPERTY_inheritsPopupMenu] = new PropertyDescriptor("inheritsPopupMenu", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getInheritsPopupMenu", "setInheritsPopupMenu"); // NOI18N
            properties[PROPERTY_inputContext] = new PropertyDescriptor("inputContext", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getInputContext", null); // NOI18N
            properties[PROPERTY_inputMap] = new PropertyDescriptor("inputMap", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getInputMap", null); // NOI18N
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor("inputMethodListeners", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getInputMethodListeners", null); // NOI18N
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor("inputMethodRequests", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getInputMethodRequests", null); // NOI18N
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor("inputVerifier", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getInputVerifier", "setInputVerifier"); // NOI18N
            properties[PROPERTY_insets] = new PropertyDescriptor("insets", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getInsets", null); // NOI18N
            properties[PROPERTY_keyListeners] = new PropertyDescriptor("keyListeners", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getKeyListeners", null); // NOI18N
            properties[PROPERTY_layout] = new PropertyDescriptor("layout", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getLayout", "setLayout"); // NOI18N
            properties[PROPERTY_lightweight] = new PropertyDescriptor("lightweight", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isLightweight", null); // NOI18N
            properties[PROPERTY_locale] = new PropertyDescriptor("locale", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getLocale", "setLocale"); // NOI18N
            properties[PROPERTY_location] = new PropertyDescriptor("location", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getLocation", "setLocation"); // NOI18N
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor("locationOnScreen", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getLocationOnScreen", null); // NOI18N
            properties[PROPERTY_managingFocus] = new PropertyDescriptor("managingFocus", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isManagingFocus", null); // NOI18N
            properties[PROPERTY_maximumSize] = new PropertyDescriptor("maximumSize", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getMaximumSize", "setMaximumSize"); // NOI18N
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor("maximumSizeSet", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isMaximumSizeSet", null); // NOI18N
            properties[PROPERTY_minimumSize] = new PropertyDescriptor("minimumSize", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getMinimumSize", "setMinimumSize"); // NOI18N
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor("minimumSizeSet", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isMinimumSizeSet", null); // NOI18N
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor("mouseListeners", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getMouseListeners", null); // NOI18N
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor("mouseMotionListeners", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getMouseMotionListeners", null); // NOI18N
            properties[PROPERTY_mousePosition] = new PropertyDescriptor("mousePosition", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getMousePosition", null); // NOI18N
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor("mouseWheelListeners", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getMouseWheelListeners", null); // NOI18N
            properties[PROPERTY_name] = new PropertyDescriptor("name", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getName", "setName"); // NOI18N
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor("nextFocusableComponent", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getNextFocusableComponent", "setNextFocusableComponent"); // NOI18N
            properties[PROPERTY_opaque] = new PropertyDescriptor("opaque", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isOpaque", "setOpaque"); // NOI18N
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor("optimizedDrawingEnabled", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isOptimizedDrawingEnabled", null); // NOI18N
            properties[PROPERTY_paintingForPrint] = new PropertyDescriptor("paintingForPrint", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isPaintingForPrint", null); // NOI18N
            properties[PROPERTY_paintingTile] = new PropertyDescriptor("paintingTile", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isPaintingTile", null); // NOI18N
            properties[PROPERTY_parent] = new PropertyDescriptor("parent", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getParent", null); // NOI18N
            properties[PROPERTY_peer] = new PropertyDescriptor("peer", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getPeer", null); // NOI18N
            properties[PROPERTY_preferredSize] = new PropertyDescriptor("preferredSize", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getPreferredSize", "setPreferredSize"); // NOI18N
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor("preferredSizeSet", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isPreferredSizeSet", null); // NOI18N
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor("propertyChangeListeners", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getPropertyChangeListeners", null); // NOI18N
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor("registeredKeyStrokes", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getRegisteredKeyStrokes", null); // NOI18N
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor("requestFocusEnabled", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isRequestFocusEnabled", "setRequestFocusEnabled"); // NOI18N
            properties[PROPERTY_rootPane] = new PropertyDescriptor("rootPane", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getRootPane", null); // NOI18N
            properties[PROPERTY_rowHeader] = new PropertyDescriptor("rowHeader", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getRowHeader", "setRowHeader"); // NOI18N
            properties[PROPERTY_rowHeaderView] = new PropertyDescriptor("rowHeaderView", org.visnow.vn.system.swing.filechooser.ButtonList.class, null, "setRowHeaderView"); // NOI18N
            properties[PROPERTY_showing] = new PropertyDescriptor("showing", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isShowing", null); // NOI18N
            properties[PROPERTY_size] = new PropertyDescriptor("size", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getSize", "setSize"); // NOI18N
            properties[PROPERTY_toolkit] = new PropertyDescriptor("toolkit", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getToolkit", null); // NOI18N
            properties[PROPERTY_toolTipText] = new PropertyDescriptor("toolTipText", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getToolTipText", "setToolTipText"); // NOI18N
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor("topLevelAncestor", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getTopLevelAncestor", null); // NOI18N
            properties[PROPERTY_transferHandler] = new PropertyDescriptor("transferHandler", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getTransferHandler", "setTransferHandler"); // NOI18N
            properties[PROPERTY_treeLock] = new PropertyDescriptor("treeLock", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getTreeLock", null); // NOI18N
            properties[PROPERTY_UI] = new PropertyDescriptor("UI", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getUI", "setUI"); // NOI18N
            properties[PROPERTY_UIClassID] = new PropertyDescriptor("UIClassID", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getUIClassID", null); // NOI18N
            properties[PROPERTY_valid] = new PropertyDescriptor("valid", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isValid", null); // NOI18N
            properties[PROPERTY_validateRoot] = new PropertyDescriptor("validateRoot", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isValidateRoot", null); // NOI18N
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor("verifyInputWhenFocusTarget", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget"); // NOI18N
            properties[PROPERTY_verticalScrollBar] = new PropertyDescriptor("verticalScrollBar", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getVerticalScrollBar", "setVerticalScrollBar"); // NOI18N
            properties[PROPERTY_verticalScrollBarPolicy] = new PropertyDescriptor("verticalScrollBarPolicy", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getVerticalScrollBarPolicy", "setVerticalScrollBarPolicy"); // NOI18N
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor("vetoableChangeListeners", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getVetoableChangeListeners", null); // NOI18N
            properties[PROPERTY_viewport] = new PropertyDescriptor("viewport", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getViewport", "setViewport"); // NOI18N
            properties[PROPERTY_viewportBorder] = new PropertyDescriptor("viewportBorder", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getViewportBorder", "setViewportBorder"); // NOI18N
            properties[PROPERTY_viewportBorderBounds] = new PropertyDescriptor("viewportBorderBounds", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getViewportBorderBounds", null); // NOI18N
            properties[PROPERTY_viewportView] = new PropertyDescriptor("viewportView", org.visnow.vn.system.swing.filechooser.ButtonList.class, null, "setViewportView"); // NOI18N
            properties[PROPERTY_visible] = new PropertyDescriptor("visible", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isVisible", "setVisible"); // NOI18N
            properties[PROPERTY_visibleRect] = new PropertyDescriptor("visibleRect", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getVisibleRect", null); // NOI18N
            properties[PROPERTY_wheelScrollingEnabled] = new PropertyDescriptor("wheelScrollingEnabled", org.visnow.vn.system.swing.filechooser.ButtonList.class, "isWheelScrollingEnabled", "setWheelScrollingEnabled"); // NOI18N
            properties[PROPERTY_width] = new PropertyDescriptor("width", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getWidth", null); // NOI18N
            properties[PROPERTY_x] = new PropertyDescriptor("x", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getX", null); // NOI18N
            properties[PROPERTY_y] = new PropertyDescriptor("y", org.visnow.vn.system.swing.filechooser.ButtonList.class, "getY", null); // NOI18N
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Properties

        // Here you can add code for customizing the properties array.
        return properties;
    }//GEN-LAST:Properties

    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_ancestorListener = 0;
    private static final int EVENT_componentListener = 1;
    private static final int EVENT_containerListener = 2;
    private static final int EVENT_focusListener = 3;
    private static final int EVENT_hierarchyBoundsListener = 4;
    private static final int EVENT_hierarchyListener = 5;
    private static final int EVENT_inputMethodListener = 6;
    private static final int EVENT_keyListener = 7;
    private static final int EVENT_mouseListener = 8;
    private static final int EVENT_mouseMotionListener = 9;
    private static final int EVENT_mouseWheelListener = 10;
    private static final int EVENT_propertyChangeListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor() {
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];

        try {
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] { "ancestorAdded", "ancestorRemoved", "ancestorMoved" }, "addAncestorListener", "removeAncestorListener"); // NOI18N
            eventSets[EVENT_componentListener] = new EventSetDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class, "componentListener", java.awt.event.ComponentListener.class, new String[] { "componentResized", "componentMoved", "componentShown", "componentHidden" }, "addComponentListener", "removeComponentListener"); // NOI18N
            eventSets[EVENT_containerListener] = new EventSetDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class, "containerListener", java.awt.event.ContainerListener.class, new String[] { "componentAdded", "componentRemoved" }, "addContainerListener", "removeContainerListener"); // NOI18N
            eventSets[EVENT_focusListener] = new EventSetDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class, "focusListener", java.awt.event.FocusListener.class, new String[] { "focusGained", "focusLost" }, "addFocusListener", "removeFocusListener"); // NOI18N
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] { "ancestorMoved", "ancestorResized" }, "addHierarchyBoundsListener", "removeHierarchyBoundsListener"); // NOI18N
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] { "hierarchyChanged" }, "addHierarchyListener", "removeHierarchyListener"); // NOI18N
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] { "inputMethodTextChanged", "caretPositionChanged" }, "addInputMethodListener", "removeInputMethodListener"); // NOI18N
            eventSets[EVENT_keyListener] = new EventSetDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class, "keyListener", java.awt.event.KeyListener.class, new String[] { "keyTyped", "keyPressed", "keyReleased" }, "addKeyListener", "removeKeyListener"); // NOI18N
            eventSets[EVENT_mouseListener] = new EventSetDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class, "mouseListener", java.awt.event.MouseListener.class, new String[] { "mouseClicked", "mousePressed", "mouseReleased", "mouseEntered", "mouseExited" }, "addMouseListener", "removeMouseListener"); // NOI18N
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] { "mouseDragged", "mouseMoved" }, "addMouseMotionListener", "removeMouseMotionListener"); // NOI18N
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] { "mouseWheelMoved" }, "addMouseWheelListener", "removeMouseWheelListener"); // NOI18N
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] { "propertyChange" }, "addPropertyChangeListener", "removePropertyChangeListener"); // NOI18N
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] { "vetoableChange" }, "addVetoableChangeListener", "removeVetoableChangeListener"); // NOI18N
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Events

        // Here you can add code for customizing the event sets array.
        return eventSets;
    }//GEN-LAST:Events

    // Method identifiers//GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_add2 = 2;
    private static final int METHOD_add3 = 3;
    private static final int METHOD_add4 = 4;
    private static final int METHOD_add5 = 5;
    private static final int METHOD_add6 = 6;
    private static final int METHOD_addNotify7 = 7;
    private static final int METHOD_addPropertyChangeListener8 = 8;
    private static final int METHOD_applyComponentOrientation9 = 9;
    private static final int METHOD_areFocusTraversalKeysSet10 = 10;
    private static final int METHOD_bounds11 = 11;
    private static final int METHOD_checkImage12 = 12;
    private static final int METHOD_checkImage13 = 13;
    private static final int METHOD_computeVisibleRect14 = 14;
    private static final int METHOD_contains15 = 15;
    private static final int METHOD_contains16 = 16;
    private static final int METHOD_countComponents17 = 17;
    private static final int METHOD_createHorizontalScrollBar18 = 18;
    private static final int METHOD_createImage19 = 19;
    private static final int METHOD_createImage20 = 20;
    private static final int METHOD_createToolTip21 = 21;
    private static final int METHOD_createVerticalScrollBar22 = 22;
    private static final int METHOD_createVolatileImage23 = 23;
    private static final int METHOD_createVolatileImage24 = 24;
    private static final int METHOD_deliverEvent25 = 25;
    private static final int METHOD_disable26 = 26;
    private static final int METHOD_dispatchEvent27 = 27;
    private static final int METHOD_doLayout28 = 28;
    private static final int METHOD_enable29 = 29;
    private static final int METHOD_enable30 = 30;
    private static final int METHOD_enableInputMethods31 = 31;
    private static final int METHOD_findComponentAt32 = 32;
    private static final int METHOD_findComponentAt33 = 33;
    private static final int METHOD_firePropertyChange34 = 34;
    private static final int METHOD_firePropertyChange35 = 35;
    private static final int METHOD_firePropertyChange36 = 36;
    private static final int METHOD_firePropertyChange37 = 37;
    private static final int METHOD_firePropertyChange38 = 38;
    private static final int METHOD_firePropertyChange39 = 39;
    private static final int METHOD_firePropertyChange40 = 40;
    private static final int METHOD_firePropertyChange41 = 41;
    private static final int METHOD_getActionForKeyStroke42 = 42;
    private static final int METHOD_getBaseline43 = 43;
    private static final int METHOD_getBounds44 = 44;
    private static final int METHOD_getClientProperty45 = 45;
    private static final int METHOD_getComponentAt46 = 46;
    private static final int METHOD_getComponentAt47 = 47;
    private static final int METHOD_getComponentZOrder48 = 48;
    private static final int METHOD_getConditionForKeyStroke49 = 49;
    private static final int METHOD_getCorner50 = 50;
    private static final int METHOD_getDefaultLocale51 = 51;
    private static final int METHOD_getFocusTraversalKeys52 = 52;
    private static final int METHOD_getFontMetrics53 = 53;
    private static final int METHOD_getInsets54 = 54;
    private static final int METHOD_getListeners55 = 55;
    private static final int METHOD_getLocation56 = 56;
    private static final int METHOD_getMousePosition57 = 57;
    private static final int METHOD_getPopupLocation58 = 58;
    private static final int METHOD_getPropertyChangeListeners59 = 59;
    private static final int METHOD_getSize60 = 60;
    private static final int METHOD_getToolTipLocation61 = 61;
    private static final int METHOD_getToolTipText62 = 62;
    private static final int METHOD_gotFocus63 = 63;
    private static final int METHOD_grabFocus64 = 64;
    private static final int METHOD_handleEvent65 = 65;
    private static final int METHOD_hasFocus66 = 66;
    private static final int METHOD_hide67 = 67;
    private static final int METHOD_imageUpdate68 = 68;
    private static final int METHOD_insets69 = 69;
    private static final int METHOD_inside70 = 70;
    private static final int METHOD_invalidate71 = 71;
    private static final int METHOD_isAncestorOf72 = 72;
    private static final int METHOD_isFocusCycleRoot73 = 73;
    private static final int METHOD_isLightweightComponent74 = 74;
    private static final int METHOD_keyDown75 = 75;
    private static final int METHOD_keyUp76 = 76;
    private static final int METHOD_layout77 = 77;
    private static final int METHOD_list78 = 78;
    private static final int METHOD_list79 = 79;
    private static final int METHOD_list80 = 80;
    private static final int METHOD_list81 = 81;
    private static final int METHOD_list82 = 82;
    private static final int METHOD_locate83 = 83;
    private static final int METHOD_location84 = 84;
    private static final int METHOD_lostFocus85 = 85;
    private static final int METHOD_minimumSize86 = 86;
    private static final int METHOD_mouseDown87 = 87;
    private static final int METHOD_mouseDrag88 = 88;
    private static final int METHOD_mouseEnter89 = 89;
    private static final int METHOD_mouseExit90 = 90;
    private static final int METHOD_mouseMove91 = 91;
    private static final int METHOD_mouseUp92 = 92;
    private static final int METHOD_move93 = 93;
    private static final int METHOD_nextFocus94 = 94;
    private static final int METHOD_paint95 = 95;
    private static final int METHOD_paintAll96 = 96;
    private static final int METHOD_paintComponents97 = 97;
    private static final int METHOD_paintImmediately98 = 98;
    private static final int METHOD_paintImmediately99 = 99;
    private static final int METHOD_postEvent100 = 100;
    private static final int METHOD_preferredSize101 = 101;
    private static final int METHOD_prepareImage102 = 102;
    private static final int METHOD_prepareImage103 = 103;
    private static final int METHOD_print104 = 104;
    private static final int METHOD_printAll105 = 105;
    private static final int METHOD_printComponents106 = 106;
    private static final int METHOD_putClientProperty107 = 107;
    private static final int METHOD_quickAdd108 = 108;
    private static final int METHOD_registerKeyboardAction109 = 109;
    private static final int METHOD_registerKeyboardAction110 = 110;
    private static final int METHOD_remove111 = 111;
    private static final int METHOD_remove112 = 112;
    private static final int METHOD_remove113 = 113;
    private static final int METHOD_removeAll114 = 114;
    private static final int METHOD_removeNotify115 = 115;
    private static final int METHOD_removePropertyChangeListener116 = 116;
    private static final int METHOD_repaint117 = 117;
    private static final int METHOD_repaint118 = 118;
    private static final int METHOD_repaint119 = 119;
    private static final int METHOD_repaint120 = 120;
    private static final int METHOD_repaint121 = 121;
    private static final int METHOD_requestDefaultFocus122 = 122;
    private static final int METHOD_requestFocus123 = 123;
    private static final int METHOD_requestFocus124 = 124;
    private static final int METHOD_requestFocusInWindow125 = 125;
    private static final int METHOD_resetKeyboardActions126 = 126;
    private static final int METHOD_reshape127 = 127;
    private static final int METHOD_resize128 = 128;
    private static final int METHOD_resize129 = 129;
    private static final int METHOD_revalidate130 = 130;
    private static final int METHOD_scrollRectToVisible131 = 131;
    private static final int METHOD_setBounds132 = 132;
    private static final int METHOD_setComponentZOrder133 = 133;
    private static final int METHOD_setCorner134 = 134;
    private static final int METHOD_setDefaultLocale135 = 135;
    private static final int METHOD_show136 = 136;
    private static final int METHOD_show137 = 137;
    private static final int METHOD_size138 = 138;
    private static final int METHOD_startAdding139 = 139;
    private static final int METHOD_stopAdding140 = 140;
    private static final int METHOD_toString141 = 141;
    private static final int METHOD_transferFocus142 = 142;
    private static final int METHOD_transferFocusBackward143 = 143;
    private static final int METHOD_transferFocusDownCycle144 = 144;
    private static final int METHOD_transferFocusUpCycle145 = 145;
    private static final int METHOD_unregisterKeyboardAction146 = 146;
    private static final int METHOD_update147 = 147;
    private static final int METHOD_updateUI148 = 148;
    private static final int METHOD_validate149 = 149;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor() {
        MethodDescriptor[] methods = new MethodDescriptor[150];

        try {
            methods[METHOD_action0] = new MethodDescriptor(java.awt.Component.class.getMethod("action", new Class[] { java.awt.Event.class, java.lang.Object.class })); // NOI18N
            methods[METHOD_action0].setDisplayName("");
            methods[METHOD_add1] = new MethodDescriptor(java.awt.Component.class.getMethod("add", new Class[] { java.awt.PopupMenu.class })); // NOI18N
            methods[METHOD_add1].setDisplayName("");
            methods[METHOD_add2] = new MethodDescriptor(java.awt.Container.class.getMethod("add", new Class[] { java.lang.String.class, java.awt.Component.class })); // NOI18N
            methods[METHOD_add2].setDisplayName("");
            methods[METHOD_add3] = new MethodDescriptor(java.awt.Container.class.getMethod("add", new Class[] { java.awt.Component.class, int.class })); // NOI18N
            methods[METHOD_add3].setDisplayName("");
            methods[METHOD_add4] = new MethodDescriptor(java.awt.Container.class.getMethod("add", new Class[] { java.awt.Component.class, java.lang.Object.class })); // NOI18N
            methods[METHOD_add4].setDisplayName("");
            methods[METHOD_add5] = new MethodDescriptor(java.awt.Container.class.getMethod("add", new Class[] { java.awt.Component.class, java.lang.Object.class, int.class })); // NOI18N
            methods[METHOD_add5].setDisplayName("");
            methods[METHOD_add6] = new MethodDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class.getMethod("add", new Class[] { java.awt.Component.class })); // NOI18N
            methods[METHOD_add6].setDisplayName("");
            methods[METHOD_addNotify7] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("addNotify", new Class[] {})); // NOI18N
            methods[METHOD_addNotify7].setDisplayName("");
            methods[METHOD_addPropertyChangeListener8] = new MethodDescriptor(java.awt.Container.class.getMethod("addPropertyChangeListener", new Class[] { java.lang.String.class, java.beans.PropertyChangeListener.class })); // NOI18N
            methods[METHOD_addPropertyChangeListener8].setDisplayName("");
            methods[METHOD_applyComponentOrientation9] = new MethodDescriptor(java.awt.Container.class.getMethod("applyComponentOrientation", new Class[] { java.awt.ComponentOrientation.class })); // NOI18N
            methods[METHOD_applyComponentOrientation9].setDisplayName("");
            methods[METHOD_areFocusTraversalKeysSet10] = new MethodDescriptor(java.awt.Container.class.getMethod("areFocusTraversalKeysSet", new Class[] { int.class })); // NOI18N
            methods[METHOD_areFocusTraversalKeysSet10].setDisplayName("");
            methods[METHOD_bounds11] = new MethodDescriptor(java.awt.Component.class.getMethod("bounds", new Class[] {})); // NOI18N
            methods[METHOD_bounds11].setDisplayName("");
            methods[METHOD_checkImage12] = new MethodDescriptor(java.awt.Component.class.getMethod("checkImage", new Class[] { java.awt.Image.class, java.awt.image.ImageObserver.class })); // NOI18N
            methods[METHOD_checkImage12].setDisplayName("");
            methods[METHOD_checkImage13] = new MethodDescriptor(java.awt.Component.class.getMethod("checkImage", new Class[] { java.awt.Image.class, int.class, int.class, java.awt.image.ImageObserver.class })); // NOI18N
            methods[METHOD_checkImage13].setDisplayName("");
            methods[METHOD_computeVisibleRect14] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("computeVisibleRect", new Class[] { java.awt.Rectangle.class })); // NOI18N
            methods[METHOD_computeVisibleRect14].setDisplayName("");
            methods[METHOD_contains15] = new MethodDescriptor(java.awt.Component.class.getMethod("contains", new Class[] { java.awt.Point.class })); // NOI18N
            methods[METHOD_contains15].setDisplayName("");
            methods[METHOD_contains16] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("contains", new Class[] { int.class, int.class })); // NOI18N
            methods[METHOD_contains16].setDisplayName("");
            methods[METHOD_countComponents17] = new MethodDescriptor(java.awt.Container.class.getMethod("countComponents", new Class[] {})); // NOI18N
            methods[METHOD_countComponents17].setDisplayName("");
            methods[METHOD_createHorizontalScrollBar18] = new MethodDescriptor(javax.swing.JScrollPane.class.getMethod("createHorizontalScrollBar", new Class[] {})); // NOI18N
            methods[METHOD_createHorizontalScrollBar18].setDisplayName("");
            methods[METHOD_createImage19] = new MethodDescriptor(java.awt.Component.class.getMethod("createImage", new Class[] { java.awt.image.ImageProducer.class })); // NOI18N
            methods[METHOD_createImage19].setDisplayName("");
            methods[METHOD_createImage20] = new MethodDescriptor(java.awt.Component.class.getMethod("createImage", new Class[] { int.class, int.class })); // NOI18N
            methods[METHOD_createImage20].setDisplayName("");
            methods[METHOD_createToolTip21] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("createToolTip", new Class[] {})); // NOI18N
            methods[METHOD_createToolTip21].setDisplayName("");
            methods[METHOD_createVerticalScrollBar22] = new MethodDescriptor(javax.swing.JScrollPane.class.getMethod("createVerticalScrollBar", new Class[] {})); // NOI18N
            methods[METHOD_createVerticalScrollBar22].setDisplayName("");
            methods[METHOD_createVolatileImage23] = new MethodDescriptor(java.awt.Component.class.getMethod("createVolatileImage", new Class[] { int.class, int.class })); // NOI18N
            methods[METHOD_createVolatileImage23].setDisplayName("");
            methods[METHOD_createVolatileImage24] = new MethodDescriptor(java.awt.Component.class.getMethod("createVolatileImage", new Class[] { int.class, int.class, java.awt.ImageCapabilities.class })); // NOI18N
            methods[METHOD_createVolatileImage24].setDisplayName("");
            methods[METHOD_deliverEvent25] = new MethodDescriptor(java.awt.Container.class.getMethod("deliverEvent", new Class[] { java.awt.Event.class })); // NOI18N
            methods[METHOD_deliverEvent25].setDisplayName("");
            methods[METHOD_disable26] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("disable", new Class[] {})); // NOI18N
            methods[METHOD_disable26].setDisplayName("");
            methods[METHOD_dispatchEvent27] = new MethodDescriptor(java.awt.Component.class.getMethod("dispatchEvent", new Class[] { java.awt.AWTEvent.class })); // NOI18N
            methods[METHOD_dispatchEvent27].setDisplayName("");
            methods[METHOD_doLayout28] = new MethodDescriptor(java.awt.Container.class.getMethod("doLayout", new Class[] {})); // NOI18N
            methods[METHOD_doLayout28].setDisplayName("");
            methods[METHOD_enable29] = new MethodDescriptor(java.awt.Component.class.getMethod("enable", new Class[] { boolean.class })); // NOI18N
            methods[METHOD_enable29].setDisplayName("");
            methods[METHOD_enable30] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("enable", new Class[] {})); // NOI18N
            methods[METHOD_enable30].setDisplayName("");
            methods[METHOD_enableInputMethods31] = new MethodDescriptor(java.awt.Component.class.getMethod("enableInputMethods", new Class[] { boolean.class })); // NOI18N
            methods[METHOD_enableInputMethods31].setDisplayName("");
            methods[METHOD_findComponentAt32] = new MethodDescriptor(java.awt.Container.class.getMethod("findComponentAt", new Class[] { int.class, int.class })); // NOI18N
            methods[METHOD_findComponentAt32].setDisplayName("");
            methods[METHOD_findComponentAt33] = new MethodDescriptor(java.awt.Container.class.getMethod("findComponentAt", new Class[] { java.awt.Point.class })); // NOI18N
            methods[METHOD_findComponentAt33].setDisplayName("");
            methods[METHOD_firePropertyChange34] = new MethodDescriptor(java.awt.Component.class.getMethod("firePropertyChange", new Class[] { java.lang.String.class, byte.class, byte.class })); // NOI18N
            methods[METHOD_firePropertyChange34].setDisplayName("");
            methods[METHOD_firePropertyChange35] = new MethodDescriptor(java.awt.Component.class.getMethod("firePropertyChange", new Class[] { java.lang.String.class, short.class, short.class })); // NOI18N
            methods[METHOD_firePropertyChange35].setDisplayName("");
            methods[METHOD_firePropertyChange36] = new MethodDescriptor(java.awt.Component.class.getMethod("firePropertyChange", new Class[] { java.lang.String.class, long.class, long.class })); // NOI18N
            methods[METHOD_firePropertyChange36].setDisplayName("");
            methods[METHOD_firePropertyChange37] = new MethodDescriptor(java.awt.Component.class.getMethod("firePropertyChange", new Class[] { java.lang.String.class, float.class, float.class })); // NOI18N
            methods[METHOD_firePropertyChange37].setDisplayName("");
            methods[METHOD_firePropertyChange38] = new MethodDescriptor(java.awt.Component.class.getMethod("firePropertyChange", new Class[] { java.lang.String.class, double.class, double.class })); // NOI18N
            methods[METHOD_firePropertyChange38].setDisplayName("");
            methods[METHOD_firePropertyChange39] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("firePropertyChange", new Class[] { java.lang.String.class, boolean.class, boolean.class })); // NOI18N
            methods[METHOD_firePropertyChange39].setDisplayName("");
            methods[METHOD_firePropertyChange40] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("firePropertyChange", new Class[] { java.lang.String.class, int.class, int.class })); // NOI18N
            methods[METHOD_firePropertyChange40].setDisplayName("");
            methods[METHOD_firePropertyChange41] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("firePropertyChange", new Class[] { java.lang.String.class, char.class, char.class })); // NOI18N
            methods[METHOD_firePropertyChange41].setDisplayName("");
            methods[METHOD_getActionForKeyStroke42] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getActionForKeyStroke", new Class[] { javax.swing.KeyStroke.class })); // NOI18N
            methods[METHOD_getActionForKeyStroke42].setDisplayName("");
            methods[METHOD_getBaseline43] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getBaseline", new Class[] { int.class, int.class })); // NOI18N
            methods[METHOD_getBaseline43].setDisplayName("");
            methods[METHOD_getBounds44] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getBounds", new Class[] { java.awt.Rectangle.class })); // NOI18N
            methods[METHOD_getBounds44].setDisplayName("");
            methods[METHOD_getClientProperty45] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getClientProperty", new Class[] { java.lang.Object.class })); // NOI18N
            methods[METHOD_getClientProperty45].setDisplayName("");
            methods[METHOD_getComponentAt46] = new MethodDescriptor(java.awt.Container.class.getMethod("getComponentAt", new Class[] { int.class, int.class })); // NOI18N
            methods[METHOD_getComponentAt46].setDisplayName("");
            methods[METHOD_getComponentAt47] = new MethodDescriptor(java.awt.Container.class.getMethod("getComponentAt", new Class[] { java.awt.Point.class })); // NOI18N
            methods[METHOD_getComponentAt47].setDisplayName("");
            methods[METHOD_getComponentZOrder48] = new MethodDescriptor(java.awt.Container.class.getMethod("getComponentZOrder", new Class[] { java.awt.Component.class })); // NOI18N
            methods[METHOD_getComponentZOrder48].setDisplayName("");
            methods[METHOD_getConditionForKeyStroke49] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getConditionForKeyStroke", new Class[] { javax.swing.KeyStroke.class })); // NOI18N
            methods[METHOD_getConditionForKeyStroke49].setDisplayName("");
            methods[METHOD_getCorner50] = new MethodDescriptor(javax.swing.JScrollPane.class.getMethod("getCorner", new Class[] { java.lang.String.class })); // NOI18N
            methods[METHOD_getCorner50].setDisplayName("");
            methods[METHOD_getDefaultLocale51] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getDefaultLocale", new Class[] {})); // NOI18N
            methods[METHOD_getDefaultLocale51].setDisplayName("");
            methods[METHOD_getFocusTraversalKeys52] = new MethodDescriptor(java.awt.Container.class.getMethod("getFocusTraversalKeys", new Class[] { int.class })); // NOI18N
            methods[METHOD_getFocusTraversalKeys52].setDisplayName("");
            methods[METHOD_getFontMetrics53] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getFontMetrics", new Class[] { java.awt.Font.class })); // NOI18N
            methods[METHOD_getFontMetrics53].setDisplayName("");
            methods[METHOD_getInsets54] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getInsets", new Class[] { java.awt.Insets.class })); // NOI18N
            methods[METHOD_getInsets54].setDisplayName("");
            methods[METHOD_getListeners55] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getListeners", new Class[] { java.lang.Class.class })); // NOI18N
            methods[METHOD_getListeners55].setDisplayName("");
            methods[METHOD_getLocation56] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getLocation", new Class[] { java.awt.Point.class })); // NOI18N
            methods[METHOD_getLocation56].setDisplayName("");
            methods[METHOD_getMousePosition57] = new MethodDescriptor(java.awt.Container.class.getMethod("getMousePosition", new Class[] { boolean.class })); // NOI18N
            methods[METHOD_getMousePosition57].setDisplayName("");
            methods[METHOD_getPopupLocation58] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getPopupLocation", new Class[] { java.awt.event.MouseEvent.class })); // NOI18N
            methods[METHOD_getPopupLocation58].setDisplayName("");
            methods[METHOD_getPropertyChangeListeners59] = new MethodDescriptor(java.awt.Component.class.getMethod("getPropertyChangeListeners", new Class[] { java.lang.String.class })); // NOI18N
            methods[METHOD_getPropertyChangeListeners59].setDisplayName("");
            methods[METHOD_getSize60] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getSize", new Class[] { java.awt.Dimension.class })); // NOI18N
            methods[METHOD_getSize60].setDisplayName("");
            methods[METHOD_getToolTipLocation61] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getToolTipLocation", new Class[] { java.awt.event.MouseEvent.class })); // NOI18N
            methods[METHOD_getToolTipLocation61].setDisplayName("");
            methods[METHOD_getToolTipText62] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getToolTipText", new Class[] { java.awt.event.MouseEvent.class })); // NOI18N
            methods[METHOD_getToolTipText62].setDisplayName("");
            methods[METHOD_gotFocus63] = new MethodDescriptor(java.awt.Component.class.getMethod("gotFocus", new Class[] { java.awt.Event.class, java.lang.Object.class })); // NOI18N
            methods[METHOD_gotFocus63].setDisplayName("");
            methods[METHOD_grabFocus64] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("grabFocus", new Class[] {})); // NOI18N
            methods[METHOD_grabFocus64].setDisplayName("");
            methods[METHOD_handleEvent65] = new MethodDescriptor(java.awt.Component.class.getMethod("handleEvent", new Class[] { java.awt.Event.class })); // NOI18N
            methods[METHOD_handleEvent65].setDisplayName("");
            methods[METHOD_hasFocus66] = new MethodDescriptor(java.awt.Component.class.getMethod("hasFocus", new Class[] {})); // NOI18N
            methods[METHOD_hasFocus66].setDisplayName("");
            methods[METHOD_hide67] = new MethodDescriptor(java.awt.Component.class.getMethod("hide", new Class[] {})); // NOI18N
            methods[METHOD_hide67].setDisplayName("");
            methods[METHOD_imageUpdate68] = new MethodDescriptor(java.awt.Component.class.getMethod("imageUpdate", new Class[] { java.awt.Image.class, int.class, int.class, int.class, int.class, int.class })); // NOI18N
            methods[METHOD_imageUpdate68].setDisplayName("");
            methods[METHOD_insets69] = new MethodDescriptor(java.awt.Container.class.getMethod("insets", new Class[] {})); // NOI18N
            methods[METHOD_insets69].setDisplayName("");
            methods[METHOD_inside70] = new MethodDescriptor(java.awt.Component.class.getMethod("inside", new Class[] { int.class, int.class })); // NOI18N
            methods[METHOD_inside70].setDisplayName("");
            methods[METHOD_invalidate71] = new MethodDescriptor(java.awt.Container.class.getMethod("invalidate", new Class[] {})); // NOI18N
            methods[METHOD_invalidate71].setDisplayName("");
            methods[METHOD_isAncestorOf72] = new MethodDescriptor(java.awt.Container.class.getMethod("isAncestorOf", new Class[] { java.awt.Component.class })); // NOI18N
            methods[METHOD_isAncestorOf72].setDisplayName("");
            methods[METHOD_isFocusCycleRoot73] = new MethodDescriptor(java.awt.Container.class.getMethod("isFocusCycleRoot", new Class[] { java.awt.Container.class })); // NOI18N
            methods[METHOD_isFocusCycleRoot73].setDisplayName("");
            methods[METHOD_isLightweightComponent74] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("isLightweightComponent", new Class[] { java.awt.Component.class })); // NOI18N
            methods[METHOD_isLightweightComponent74].setDisplayName("");
            methods[METHOD_keyDown75] = new MethodDescriptor(java.awt.Component.class.getMethod("keyDown", new Class[] { java.awt.Event.class, int.class })); // NOI18N
            methods[METHOD_keyDown75].setDisplayName("");
            methods[METHOD_keyUp76] = new MethodDescriptor(java.awt.Component.class.getMethod("keyUp", new Class[] { java.awt.Event.class, int.class })); // NOI18N
            methods[METHOD_keyUp76].setDisplayName("");
            methods[METHOD_layout77] = new MethodDescriptor(java.awt.Container.class.getMethod("layout", new Class[] {})); // NOI18N
            methods[METHOD_layout77].setDisplayName("");
            methods[METHOD_list78] = new MethodDescriptor(java.awt.Component.class.getMethod("list", new Class[] {})); // NOI18N
            methods[METHOD_list78].setDisplayName("");
            methods[METHOD_list79] = new MethodDescriptor(java.awt.Component.class.getMethod("list", new Class[] { java.io.PrintStream.class })); // NOI18N
            methods[METHOD_list79].setDisplayName("");
            methods[METHOD_list80] = new MethodDescriptor(java.awt.Component.class.getMethod("list", new Class[] { java.io.PrintWriter.class })); // NOI18N
            methods[METHOD_list80].setDisplayName("");
            methods[METHOD_list81] = new MethodDescriptor(java.awt.Container.class.getMethod("list", new Class[] { java.io.PrintStream.class, int.class })); // NOI18N
            methods[METHOD_list81].setDisplayName("");
            methods[METHOD_list82] = new MethodDescriptor(java.awt.Container.class.getMethod("list", new Class[] { java.io.PrintWriter.class, int.class })); // NOI18N
            methods[METHOD_list82].setDisplayName("");
            methods[METHOD_locate83] = new MethodDescriptor(java.awt.Container.class.getMethod("locate", new Class[] { int.class, int.class })); // NOI18N
            methods[METHOD_locate83].setDisplayName("");
            methods[METHOD_location84] = new MethodDescriptor(java.awt.Component.class.getMethod("location", new Class[] {})); // NOI18N
            methods[METHOD_location84].setDisplayName("");
            methods[METHOD_lostFocus85] = new MethodDescriptor(java.awt.Component.class.getMethod("lostFocus", new Class[] { java.awt.Event.class, java.lang.Object.class })); // NOI18N
            methods[METHOD_lostFocus85].setDisplayName("");
            methods[METHOD_minimumSize86] = new MethodDescriptor(java.awt.Container.class.getMethod("minimumSize", new Class[] {})); // NOI18N
            methods[METHOD_minimumSize86].setDisplayName("");
            methods[METHOD_mouseDown87] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseDown", new Class[] { java.awt.Event.class, int.class, int.class })); // NOI18N
            methods[METHOD_mouseDown87].setDisplayName("");
            methods[METHOD_mouseDrag88] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseDrag", new Class[] { java.awt.Event.class, int.class, int.class })); // NOI18N
            methods[METHOD_mouseDrag88].setDisplayName("");
            methods[METHOD_mouseEnter89] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseEnter", new Class[] { java.awt.Event.class, int.class, int.class })); // NOI18N
            methods[METHOD_mouseEnter89].setDisplayName("");
            methods[METHOD_mouseExit90] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseExit", new Class[] { java.awt.Event.class, int.class, int.class })); // NOI18N
            methods[METHOD_mouseExit90].setDisplayName("");
            methods[METHOD_mouseMove91] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseMove", new Class[] { java.awt.Event.class, int.class, int.class })); // NOI18N
            methods[METHOD_mouseMove91].setDisplayName("");
            methods[METHOD_mouseUp92] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseUp", new Class[] { java.awt.Event.class, int.class, int.class })); // NOI18N
            methods[METHOD_mouseUp92].setDisplayName("");
            methods[METHOD_move93] = new MethodDescriptor(java.awt.Component.class.getMethod("move", new Class[] { int.class, int.class })); // NOI18N
            methods[METHOD_move93].setDisplayName("");
            methods[METHOD_nextFocus94] = new MethodDescriptor(java.awt.Component.class.getMethod("nextFocus", new Class[] {})); // NOI18N
            methods[METHOD_nextFocus94].setDisplayName("");
            methods[METHOD_paint95] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("paint", new Class[] { java.awt.Graphics.class })); // NOI18N
            methods[METHOD_paint95].setDisplayName("");
            methods[METHOD_paintAll96] = new MethodDescriptor(java.awt.Component.class.getMethod("paintAll", new Class[] { java.awt.Graphics.class })); // NOI18N
            methods[METHOD_paintAll96].setDisplayName("");
            methods[METHOD_paintComponents97] = new MethodDescriptor(java.awt.Container.class.getMethod("paintComponents", new Class[] { java.awt.Graphics.class })); // NOI18N
            methods[METHOD_paintComponents97].setDisplayName("");
            methods[METHOD_paintImmediately98] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("paintImmediately", new Class[] { int.class, int.class, int.class, int.class })); // NOI18N
            methods[METHOD_paintImmediately98].setDisplayName("");
            methods[METHOD_paintImmediately99] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("paintImmediately", new Class[] { java.awt.Rectangle.class })); // NOI18N
            methods[METHOD_paintImmediately99].setDisplayName("");
            methods[METHOD_postEvent100] = new MethodDescriptor(java.awt.Component.class.getMethod("postEvent", new Class[] { java.awt.Event.class })); // NOI18N
            methods[METHOD_postEvent100].setDisplayName("");
            methods[METHOD_preferredSize101] = new MethodDescriptor(java.awt.Container.class.getMethod("preferredSize", new Class[] {})); // NOI18N
            methods[METHOD_preferredSize101].setDisplayName("");
            methods[METHOD_prepareImage102] = new MethodDescriptor(java.awt.Component.class.getMethod("prepareImage", new Class[] { java.awt.Image.class, java.awt.image.ImageObserver.class })); // NOI18N
            methods[METHOD_prepareImage102].setDisplayName("");
            methods[METHOD_prepareImage103] = new MethodDescriptor(java.awt.Component.class.getMethod("prepareImage", new Class[] { java.awt.Image.class, int.class, int.class, java.awt.image.ImageObserver.class })); // NOI18N
            methods[METHOD_prepareImage103].setDisplayName("");
            methods[METHOD_print104] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("print", new Class[] { java.awt.Graphics.class })); // NOI18N
            methods[METHOD_print104].setDisplayName("");
            methods[METHOD_printAll105] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("printAll", new Class[] { java.awt.Graphics.class })); // NOI18N
            methods[METHOD_printAll105].setDisplayName("");
            methods[METHOD_printComponents106] = new MethodDescriptor(java.awt.Container.class.getMethod("printComponents", new Class[] { java.awt.Graphics.class })); // NOI18N
            methods[METHOD_printComponents106].setDisplayName("");
            methods[METHOD_putClientProperty107] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("putClientProperty", new Class[] { java.lang.Object.class, java.lang.Object.class })); // NOI18N
            methods[METHOD_putClientProperty107].setDisplayName("");
            methods[METHOD_quickAdd108] = new MethodDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class.getMethod("quickAdd", new Class[] { java.awt.Component.class })); // NOI18N
            methods[METHOD_quickAdd108].setDisplayName("");
            methods[METHOD_registerKeyboardAction109] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("registerKeyboardAction", new Class[] { java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, int.class })); // NOI18N
            methods[METHOD_registerKeyboardAction109].setDisplayName("");
            methods[METHOD_registerKeyboardAction110] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("registerKeyboardAction", new Class[] { java.awt.event.ActionListener.class, javax.swing.KeyStroke.class, int.class })); // NOI18N
            methods[METHOD_registerKeyboardAction110].setDisplayName("");
            methods[METHOD_remove111] = new MethodDescriptor(java.awt.Component.class.getMethod("remove", new Class[] { java.awt.MenuComponent.class })); // NOI18N
            methods[METHOD_remove111].setDisplayName("");
            methods[METHOD_remove112] = new MethodDescriptor(java.awt.Container.class.getMethod("remove", new Class[] { int.class })); // NOI18N
            methods[METHOD_remove112].setDisplayName("");
            methods[METHOD_remove113] = new MethodDescriptor(java.awt.Container.class.getMethod("remove", new Class[] { java.awt.Component.class })); // NOI18N
            methods[METHOD_remove113].setDisplayName("");
            methods[METHOD_removeAll114] = new MethodDescriptor(java.awt.Container.class.getMethod("removeAll", new Class[] {})); // NOI18N
            methods[METHOD_removeAll114].setDisplayName("");
            methods[METHOD_removeNotify115] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("removeNotify", new Class[] {})); // NOI18N
            methods[METHOD_removeNotify115].setDisplayName("");
            methods[METHOD_removePropertyChangeListener116] = new MethodDescriptor(java.awt.Component.class.getMethod("removePropertyChangeListener", new Class[] { java.lang.String.class, java.beans.PropertyChangeListener.class })); // NOI18N
            methods[METHOD_removePropertyChangeListener116].setDisplayName("");
            methods[METHOD_repaint117] = new MethodDescriptor(java.awt.Component.class.getMethod("repaint", new Class[] {})); // NOI18N
            methods[METHOD_repaint117].setDisplayName("");
            methods[METHOD_repaint118] = new MethodDescriptor(java.awt.Component.class.getMethod("repaint", new Class[] { long.class })); // NOI18N
            methods[METHOD_repaint118].setDisplayName("");
            methods[METHOD_repaint119] = new MethodDescriptor(java.awt.Component.class.getMethod("repaint", new Class[] { int.class, int.class, int.class, int.class })); // NOI18N
            methods[METHOD_repaint119].setDisplayName("");
            methods[METHOD_repaint120] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("repaint", new Class[] { long.class, int.class, int.class, int.class, int.class })); // NOI18N
            methods[METHOD_repaint120].setDisplayName("");
            methods[METHOD_repaint121] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("repaint", new Class[] { java.awt.Rectangle.class })); // NOI18N
            methods[METHOD_repaint121].setDisplayName("");
            methods[METHOD_requestDefaultFocus122] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("requestDefaultFocus", new Class[] {})); // NOI18N
            methods[METHOD_requestDefaultFocus122].setDisplayName("");
            methods[METHOD_requestFocus123] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("requestFocus", new Class[] {})); // NOI18N
            methods[METHOD_requestFocus123].setDisplayName("");
            methods[METHOD_requestFocus124] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("requestFocus", new Class[] { boolean.class })); // NOI18N
            methods[METHOD_requestFocus124].setDisplayName("");
            methods[METHOD_requestFocusInWindow125] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("requestFocusInWindow", new Class[] {})); // NOI18N
            methods[METHOD_requestFocusInWindow125].setDisplayName("");
            methods[METHOD_resetKeyboardActions126] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("resetKeyboardActions", new Class[] {})); // NOI18N
            methods[METHOD_resetKeyboardActions126].setDisplayName("");
            methods[METHOD_reshape127] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("reshape", new Class[] { int.class, int.class, int.class, int.class })); // NOI18N
            methods[METHOD_reshape127].setDisplayName("");
            methods[METHOD_resize128] = new MethodDescriptor(java.awt.Component.class.getMethod("resize", new Class[] { int.class, int.class })); // NOI18N
            methods[METHOD_resize128].setDisplayName("");
            methods[METHOD_resize129] = new MethodDescriptor(java.awt.Component.class.getMethod("resize", new Class[] { java.awt.Dimension.class })); // NOI18N
            methods[METHOD_resize129].setDisplayName("");
            methods[METHOD_revalidate130] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("revalidate", new Class[] {})); // NOI18N
            methods[METHOD_revalidate130].setDisplayName("");
            methods[METHOD_scrollRectToVisible131] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("scrollRectToVisible", new Class[] { java.awt.Rectangle.class })); // NOI18N
            methods[METHOD_scrollRectToVisible131].setDisplayName("");
            methods[METHOD_setBounds132] = new MethodDescriptor(java.awt.Component.class.getMethod("setBounds", new Class[] { int.class, int.class, int.class, int.class })); // NOI18N
            methods[METHOD_setBounds132].setDisplayName("");
            methods[METHOD_setComponentZOrder133] = new MethodDescriptor(java.awt.Container.class.getMethod("setComponentZOrder", new Class[] { java.awt.Component.class, int.class })); // NOI18N
            methods[METHOD_setComponentZOrder133].setDisplayName("");
            methods[METHOD_setCorner134] = new MethodDescriptor(javax.swing.JScrollPane.class.getMethod("setCorner", new Class[] { java.lang.String.class, java.awt.Component.class })); // NOI18N
            methods[METHOD_setCorner134].setDisplayName("");
            methods[METHOD_setDefaultLocale135] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("setDefaultLocale", new Class[] { java.util.Locale.class })); // NOI18N
            methods[METHOD_setDefaultLocale135].setDisplayName("");
            methods[METHOD_show136] = new MethodDescriptor(java.awt.Component.class.getMethod("show", new Class[] {})); // NOI18N
            methods[METHOD_show136].setDisplayName("");
            methods[METHOD_show137] = new MethodDescriptor(java.awt.Component.class.getMethod("show", new Class[] { boolean.class })); // NOI18N
            methods[METHOD_show137].setDisplayName("");
            methods[METHOD_size138] = new MethodDescriptor(java.awt.Component.class.getMethod("size", new Class[] {})); // NOI18N
            methods[METHOD_size138].setDisplayName("");
            methods[METHOD_startAdding139] = new MethodDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class.getMethod("startAdding", new Class[] {})); // NOI18N
            methods[METHOD_startAdding139].setDisplayName("");
            methods[METHOD_stopAdding140] = new MethodDescriptor(org.visnow.vn.system.swing.filechooser.ButtonList.class.getMethod("stopAdding", new Class[] {})); // NOI18N
            methods[METHOD_stopAdding140].setDisplayName("");
            methods[METHOD_toString141] = new MethodDescriptor(java.awt.Component.class.getMethod("toString", new Class[] {})); // NOI18N
            methods[METHOD_toString141].setDisplayName("");
            methods[METHOD_transferFocus142] = new MethodDescriptor(java.awt.Component.class.getMethod("transferFocus", new Class[] {})); // NOI18N
            methods[METHOD_transferFocus142].setDisplayName("");
            methods[METHOD_transferFocusBackward143] = new MethodDescriptor(java.awt.Container.class.getMethod("transferFocusBackward", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusBackward143].setDisplayName("");
            methods[METHOD_transferFocusDownCycle144] = new MethodDescriptor(java.awt.Container.class.getMethod("transferFocusDownCycle", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusDownCycle144].setDisplayName("");
            methods[METHOD_transferFocusUpCycle145] = new MethodDescriptor(java.awt.Component.class.getMethod("transferFocusUpCycle", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusUpCycle145].setDisplayName("");
            methods[METHOD_unregisterKeyboardAction146] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("unregisterKeyboardAction", new Class[] { javax.swing.KeyStroke.class })); // NOI18N
            methods[METHOD_unregisterKeyboardAction146].setDisplayName("");
            methods[METHOD_update147] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("update", new Class[] { java.awt.Graphics.class })); // NOI18N
            methods[METHOD_update147].setDisplayName("");
            methods[METHOD_updateUI148] = new MethodDescriptor(javax.swing.JScrollPane.class.getMethod("updateUI", new Class[] {})); // NOI18N
            methods[METHOD_updateUI148].setDisplayName("");
            methods[METHOD_validate149] = new MethodDescriptor(java.awt.Container.class.getMethod("validate", new Class[] {})); // NOI18N
            methods[METHOD_validate149].setDisplayName("");
        } catch (Exception e) {
        }//GEN-HEADEREND:Methods

        // Here you can add code for customizing the methods array.
        return methods;
    }//GEN-LAST:Methods

    private static java.awt.Image iconColor16 = null;//GEN-BEGIN:IconsDef
    private static java.awt.Image iconColor32 = null;
    private static java.awt.Image iconMono16 = null;
    private static java.awt.Image iconMono32 = null;//GEN-END:IconsDef
    private static String iconNameC16 = null;//GEN-BEGIN:Icons
    private static String iconNameC32 = null;
    private static String iconNameM16 = null;
    private static String iconNameM32 = null;//GEN-END:Icons

    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx

//GEN-FIRST:Superclass

    // Here you can add code for customizing the Superclass BeanInfo.
//GEN-LAST:Superclass
    /**
     * Gets the bean's <code>BeanDescriptor</code>s.
     * <p>
     * @return BeanDescriptor describing the editable
     *         properties of this bean. May return null if the
     *         information should be obtained by automatic analysis.
     */
    public BeanDescriptor getBeanDescriptor()
    {
        return getBdescriptor();
    }

    /**
     * Gets the bean's <code>PropertyDescriptor</code>s.
     * <p>
     * @return An array of PropertyDescriptors describing the editable
     *         properties supported by this bean. May return null if the
     *         information should be obtained by automatic analysis.
     * <p>
     * If a property is indexed, then its entry in the result array will
     * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
     * A client of getPropertyDescriptors can use "instanceof" to check
     * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
     */
    public PropertyDescriptor[] getPropertyDescriptors()
    {
        return getPdescriptor();
    }

    /**
     * Gets the bean's <code>EventSetDescriptor</code>s.
     * <p>
     * @return An array of EventSetDescriptors describing the kinds of
     *         events fired by this bean. May return null if the information
     *         should be obtained by automatic analysis.
     */
    public EventSetDescriptor[] getEventSetDescriptors()
    {
        return getEdescriptor();
    }

    /**
     * Gets the bean's <code>MethodDescriptor</code>s.
     * <p>
     * @return An array of MethodDescriptors describing the methods
     *         implemented by this bean. May return null if the information
     *         should be obtained by automatic analysis.
     */
    public MethodDescriptor[] getMethodDescriptors()
    {
        return getMdescriptor();
    }

    /**
     * A bean may have a "default" property that is the property that will
     * mostly commonly be initially chosen for update by human's who are
     * customizing the bean.
     * <p>
     * @return Index of default property in the PropertyDescriptor array
     *         returned by getPropertyDescriptors.
     * <P>
     * Returns -1 if there is no default property.
     */
    public int getDefaultPropertyIndex()
    {
        return defaultPropertyIndex;
    }

    /**
     * A bean may have a "default" event that is the event that will
     * mostly commonly be used by human's when using the bean.
     * <p>
     * @return Index of default event in the EventSetDescriptor array
     *         returned by getEventSetDescriptors.
     * <P>
     * Returns -1 if there is no default event.
     */
    public int getDefaultEventIndex()
    {
        return defaultEventIndex;
    }

    /**
     * This method returns an image object that can be used to
     * represent the bean in toolboxes, toolbars, etc. Icon images
     * will typically be GIFs, but may in future include other formats.
     * <p>
     * Beans aren't required to provide icons and may return null from
     * this method.
     * <p>
     * There are four possible flavors of icons (16x16 color,
     * 32x32 color, 16x16 mono, 32x32 mono). If a bean choses to only
     * support a single icon we recommend supporting 16x16 color.
     * <p>
     * We recommend that icons have a "transparent" background
     * so they can be rendered onto an existing background.
     *
     * @param iconKind The kind of icon requested. This should be
     *                 one of the constant values ICON_COLOR_16x16, ICON_COLOR_32x32,
     *                 ICON_MONO_16x16, or ICON_MONO_32x32.
     * <p>
     * @return An image object representing the requested icon. May
     *         return null if no suitable icon is available.
     */
    public java.awt.Image getIcon(int iconKind)
    {
        switch (iconKind) {
            case ICON_COLOR_16x16:
                if (iconNameC16 == null)
                    return null;
                else {
                    if (iconColor16 == null)
                        iconColor16 = loadImage(iconNameC16);
                    return iconColor16;
                }
            case ICON_COLOR_32x32:
                if (iconNameC32 == null)
                    return null;
                else {
                    if (iconColor32 == null)
                        iconColor32 = loadImage(iconNameC32);
                    return iconColor32;
                }
            case ICON_MONO_16x16:
                if (iconNameM16 == null)
                    return null;
                else {
                    if (iconMono16 == null)
                        iconMono16 = loadImage(iconNameM16);
                    return iconMono16;
                }
            case ICON_MONO_32x32:
                if (iconNameM32 == null)
                    return null;
                else {
                    if (iconMono32 == null)
                        iconMono32 = loadImage(iconNameM32);
                    return iconMono32;
                }
            default:
                return null;
        }
    }

}
