/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.system.swing.split_1;

import java.awt.Color;
import javax.swing.JPanel;

/**
 *
 * @author gacek
 */
public class Main
{

    public static void Debug1(String text)
    {
        System.out.println(text);
    }

    private static Color[] tab = new Color[]{
        new Color(100, 000, 000),
        new Color(200, 100, 000),
        new Color(200, 200, 000),
        new Color(100, 200, 000),
        new Color(000, 200, 200),
        new Color(000, 100, 200),
        new Color(000, 000, 200)
    };

    public static void main(String[] args)
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                FAreaMajor a = new FAreaMajor(true);
                FComponentViewer v1 = new FComponentViewer(a, "test", 400, 600, true, FComponentViewer.EXIT_ON_CLOSE);
                v1.setVisible(true);

                JPanel j = new JPanel();
                j.setBackground(tab[1]);
                a.getSomePlace().addBox("tab1", j);

                j = new JPanel();
                j.setBackground(tab[4]);
                a.getSomePlace().addBox("tab4", j);

                j = new JPanel();
                j.setBackground(tab[5]);
                a.getSomePlace().addBox("tab5", j);

                a = new FAreaMajor(false);
                v1 = new FComponentViewer(a, "bitest", 400, 600, true, FComponentViewer.DISPOSE_ON_CLOSE);
                a.setFrame(v1);
                v1.setVisible(true);

                j = new JPanel();
                j.setBackground(tab[0]);
                a.getSomePlace().addBox("tab0", j);

                j = new JPanel();
                j.setBackground(tab[2]);
                a.getSomePlace().addBox("tab2", j);

                j = new JPanel();
                j.setBackground(tab[3]);
                a.getSomePlace().addBox("tab3", j);

            }
        });
    }

    private Main()
    {
    }

}
