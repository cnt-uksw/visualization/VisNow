/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.system.swing;

import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.JPanel;

/**
 * Panel which preferred width is constant. Preferred height is taken from preferred height of super component.
 * This panel is suitable for use in JScrollPane.
 * <p>
 * @author szpak
 */
public class CustomSizePanel extends JPanel
{
    //horizontal scroll should be visible up to this width
    private int customWidth = 240;
    private int customHeight = -1;

    private boolean keepSmallerWidth = false;
    private boolean keepSmallerHeight = false;

    private int minWidthIfKeepSmaller = 0;
    private int minHeightIfKeepSmaller = 0;

    private boolean overrideMinSize = false;

    public CustomSizePanel(LayoutManager layout, boolean isDoubleBuffered)
    {
        super(layout, isDoubleBuffered);
    }

    public CustomSizePanel(LayoutManager layout)
    {
        super(layout);
    }

    public CustomSizePanel(boolean isDoubleBuffered)
    {
        super(isDoubleBuffered);
    }

    public CustomSizePanel()
    {

    }

    /**
     * @return preferred size based on preferred height of super component and custom width.
     */
    @Override
    public Dimension getPreferredSize()
    {
        Dimension d = super.getPreferredSize();
        if (customWidth != -1)
            if (keepSmallerWidth)
                d.width = Math.max(minWidthIfKeepSmaller, Math.min(d.width, customWidth));
            else
                d.width = customWidth;

        if (customHeight != -1)
            if (keepSmallerHeight)
                d.height = Math.max(minHeightIfKeepSmaller, Math.min(d.height, customHeight));
            else
                d.height = customHeight;

        return d;
    }

    @Override
    public Dimension getMinimumSize()
    {
        if (overrideMinSize) return getPreferredSize();
        else return super.getMinimumSize(); //To change body of generated methods, choose Tools | Templates.
    }

    public int getCustomWidth()
    {
        return customWidth;
    }

    public void setCustomWidth(int width)
    {
        this.customWidth = width;
    }

    public boolean isKeepSmallerWidth()
    {
        return keepSmallerWidth;
    }

    public void setKeepSmallerWidth(boolean keepSmallerWidth)
    {
        this.keepSmallerWidth = keepSmallerWidth;
    }

    public int getCustomHeight()
    {
        return customHeight;
    }

    public void setCustomHeight(int customHeight)
    {
        this.customHeight = customHeight;
    }

    public boolean isKeepSmallerHeight()
    {
        return keepSmallerHeight;
    }

    public void setKeepSmallerHeight(boolean keepSmallerHeight)
    {
        this.keepSmallerHeight = keepSmallerHeight;
    }

    public int getMinWidthIfKeepSmaller()
    {
        return minWidthIfKeepSmaller;
    }

    public void setMinWidthIfKeepSmaller(int minWidthIfKeepSmaller)
    {
        this.minWidthIfKeepSmaller = minWidthIfKeepSmaller;
    }

    public int getMinHeightIfKeepSmaller()
    {
        return minHeightIfKeepSmaller;
    }

    public void setMinHeightIfKeepSmaller(int minHeightIfKeepSmaller)
    {
        this.minHeightIfKeepSmaller = minHeightIfKeepSmaller;
    }

    public boolean isOverrideMinSize()
    {
        return overrideMinSize;
    }

    public void setOverrideMinSize(boolean overrideMinSize)
    {
        this.overrideMinSize = overrideMinSize;
    }
}
