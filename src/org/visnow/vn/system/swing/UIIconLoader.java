/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.system.swing;

import java.awt.Image;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.UIManager;

/**
 * Static loader for UIManager icons. This class provides functionality for loading (and caching) icons in original size and resized.
 *
 * @author szpak
 */
public class UIIconLoader
{

    //map with String as a key (IconType.name() + _ + width + _ + height)
    static Map<String, Icon> icons = new HashMap<String, Icon>();

    /**
     * Returns icon for passed
     * <code>iconType</code>. This method cache loaded icons.
     *
     * @return icon or null if such icon cannot be found
     */
    public static Icon getIcon(UIManagerKey iconKey)
    {
        if (!icons.containsKey(cacheKey(iconKey))) {
            Icon icon = UIManager.getIcon(iconKey.getKey());
            //return null if not found
            if (icon == null)
                return null;
            icons.put(cacheKey(iconKey), icon);
        }
        return icons.get(cacheKey(iconKey));
    }

    /**
     * Returns icon for passed
     * <code>iconType</code> and size. This method cache loaded icons.
     *
     * @return icon or null if such icon cannot be found
     */
    public static Icon getIcon(UIManagerKey iconKey, int width, int height)
    {
        if (iconKey == null)
            throw new NullPointerException("iconKey cannot be null");

        if (!icons.containsKey(cacheKey(iconKey, width, height))) {
            Icon icon = getIcon(iconKey);
            //return null if icon not found
            if (icon == null)
                return null;

            Image img = ((ImageIcon) icon).getImage();
            Image newimg = img.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);
            icon = new ImageIcon(newimg);
            icons.put(cacheKey(iconKey, width, height), icon);
        }
        return icons.get(cacheKey(iconKey, width, height));
    }

    /**
     * Key to get/store icons with specified size.
     */
    private static String cacheKey(UIManagerKey iconKey, int width, int height)
    {
        return iconKey.getKey() + "_" + width + "_" + height;
    }

    /**
     * Key to get/store icons without specified size.
     */
    private static String cacheKey(UIManagerKey iconKey)
    {
        //added __ to make it unique comparing to width x height
        return iconKey.getKey() + "_" + "_";
    }
}
