/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.system.swing.trash;

import java.awt.Color;
import java.awt.Component;
import java.util.Stack;
import java.util.Vector;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.visnow.vn.system.swing.VNSwingUtils;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class VerticalPanel extends JPanel
{

    private int horizontalSpace = 5;

    private Vector<JComponent> components;

    public void addComponent(JComponent sp)
    {
        VNSwingUtils.setConstantHeight(sp, 80);
        components.add(sp);
        sp.setAlignmentX(Component.CENTER_ALIGNMENT);
    }

    public VerticalPanel()
    {
        this.components = new Vector<JComponent>();
        BoxLayout bl = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(bl);
        unusedSeparators = new Stack<JPanel>();
        usedSeparators = new Stack<JPanel>();
        this.parent = null;
    }

    //<editor-fold defaultstate="collapsed" desc=" Separators ">
    private Stack<JPanel> unusedSeparators;
    private Stack<JPanel> usedSeparators;

    private JPanel getSeparator()
    {
        if (!unusedSeparators.isEmpty()) {
            JPanel ret = unusedSeparators.pop();
            usedSeparators.push(ret);
            return ret;
        }
        JPanel ret = new JPanel();
        ret.setBackground(Color.LIGHT_GRAY);
        //System.out.println(this.getWidth());
        VNSwingUtils.setConstantHeight(ret, horizontalSpace);
        usedSeparators.push(ret);
        return ret;
    }

    //</editor-fold>
    private DuoPanel parent;
    private int height;

    public void setParentDuoPanel(DuoPanel panel)
    {
        this.parent = panel;
    }

    public void repack()
    {
        this.removeAll();
        height = horizontalSpace;
        unusedSeparators.addAll(usedSeparators);
        usedSeparators.clear();
        this.add(getSeparator());
        for (Component panel : components) {
            this.add(panel);
            height += panel.getPreferredSize().height;
            this.add(getSeparator());
            height += horizontalSpace;
        }
        VNSwingUtils.setConstantHeight(this, height);
        //this.doLayout();
        if (parent == null)
            repaint();
        else
            parent.repack();
    }

}
