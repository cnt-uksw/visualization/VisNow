/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.system.swing.trash;

import org.visnow.vn.system.swing.*;
import java.awt.Component;

/**
 *
 * @author gacek
 */
public class DuoPanel extends javax.swing.JPanel
{

    //<editor-fold defaultstate="collapsed" desc=" [VAR] static final ">
    private static final String UPARROW = "\u21E7";
    private static final String DOWNARROW = "\u21E9";
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc=" [VAR] Border, Label, UI ">
    private int outerBorder = 5;
    private int innerBorder = 5;

    public void setLabel(String text)
    {
        smallLabel.setText(text);
        bigLabel.setText(text);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" [VAR] VerticalPanel ">
    private VerticalPanel panel;

    public VerticalPanel getVerticalPanel()
    {
        return panel;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" [VAR] Heights ">
    public int getSmallHeight()
    {
        return smallPanel.getPreferredSize().height + 2 * outerBorder;
    }

    public int getBigHeight()
    {
        return labelPanel.getPreferredSize().height +
            mainPanel.getPreferredSize().height +
            2 * outerBorder;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Switch size ">
    public void switchToBig()
    {
        VNSwingUtils.setFillerComponent(this, bigPanel);
        VNSwingUtils.setConstantHeight(this, getBigHeight());
        panel.repack();
    }

    public void switchToSmall()
    {
        VNSwingUtils.setFillerComponent(this, smallPanel);
        VNSwingUtils.setConstantHeight(this, getSmallHeight());
        panel.repack();
    }

    public void repack()
    {
        switchToBig();
    }

    //</editor-fold>
    /**
     * Creates new form DuoPanel
     */
    public DuoPanel(VerticalPanel vp, Component comp)
    {
        initComponents();
        panel = vp;
        VNSwingUtils.setFillerComponent(mainPanel, comp);
        //switchToSmall();
    }

    public DuoPanel(VerticalPanel vp, Component comp, String label)
    {
        this(vp, comp);
        setLabel(label);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bigPanel = new javax.swing.JPanel();
        mainPanel = new javax.swing.JPanel();
        labelPanel = new javax.swing.JPanel();
        bigLabel = new javax.swing.JLabel();
        bigButton = new javax.swing.JButton();
        smallPanel = new javax.swing.JPanel();
        smallButton = new javax.swing.JButton();
        smallLabel = new javax.swing.JLabel();

        bigPanel.setLayout(new java.awt.BorderLayout());

        mainPanel.setBackground(new java.awt.Color(255, 255, 255));
        mainPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(235, 235, 235), 5));

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
                mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 405, Short.MAX_VALUE)
                );
        mainPanelLayout.setVerticalGroup(
                mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 277, Short.MAX_VALUE)
                );

        bigPanel.add(mainPanel, java.awt.BorderLayout.CENTER);

        bigLabel.setText("hide");

        bigButton.setFont(new java.awt.Font("Dialog", 0, 14));
        bigButton.setText("+");
        bigButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        bigButton.setMaximumSize(new java.awt.Dimension(25, 25));
        bigButton.setMinimumSize(new java.awt.Dimension(25, 25));
        bigButton.setPreferredSize(new java.awt.Dimension(25, 25));
        bigButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bigButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout labelPanelLayout = new javax.swing.GroupLayout(labelPanel);
        labelPanel.setLayout(labelPanelLayout);
        labelPanelLayout.setHorizontalGroup(
                labelPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(labelPanelLayout.createSequentialGroup()
                                .addComponent(bigButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bigLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE))
                );
        labelPanelLayout.setVerticalGroup(
                labelPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(labelPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(bigButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(bigLabel))
                );

        bigPanel.add(labelPanel, java.awt.BorderLayout.NORTH);

        setBackground(new java.awt.Color(0, 0, 0));
        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204), 5));

        smallButton.setFont(new java.awt.Font("Dialog", 0, 14));
        smallButton.setText("+");
        smallButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        smallButton.setMaximumSize(new java.awt.Dimension(25, 25));
        smallButton.setMinimumSize(new java.awt.Dimension(25, 25));
        smallButton.setPreferredSize(new java.awt.Dimension(25, 25));
        smallButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                smallButtonActionPerformed(evt);
            }
        });

        smallLabel.setText("show");

        javax.swing.GroupLayout smallPanelLayout = new javax.swing.GroupLayout(smallPanel);
        smallPanel.setLayout(smallPanelLayout);
        smallPanelLayout.setHorizontalGroup(
                smallPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(smallPanelLayout.createSequentialGroup()
                                .addComponent(smallButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(smallLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                );
        smallPanelLayout.setVerticalGroup(
                smallPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(smallPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(smallButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(smallLabel))
                );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(smallPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(smallPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                );
    }// </editor-fold>//GEN-END:initComponents

    //<editor-fold defaultstate="collapsed" desc=" Generated actions ">
    private void bigButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bigButtonActionPerformed
        switchToSmall();
    }//GEN-LAST:event_bigButtonActionPerformed

    private void smallButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_smallButtonActionPerformed
        switchToBig();
    }//GEN-LAST:event_smallButtonActionPerformed
     //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc=" Generated variables ">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bigButton;
    private javax.swing.JLabel bigLabel;
    private javax.swing.JPanel bigPanel;
    private javax.swing.JPanel labelPanel;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton smallButton;
    private javax.swing.JLabel smallLabel;
    private javax.swing.JPanel smallPanel;
    // End of variables declaration//GEN-END:variables
    //</editor-fold>

}
