/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.system.swing;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagLayoutInfo;
import java.awt.LayoutManager;
import javax.swing.JPanel;

/**
 * Panel that "cheats" backing GridBagLayout to prevent it from using minimumSize of components when there is not enough space for render them using preferredSize.
 * Cheating means passing preferredSize as minimumSize.
 * 
 * @author szpak
 */
public class FixedGridBagLayoutPanel extends JPanel
{
    private GridBagLayout fixedGridBagLayoutManager;

    public FixedGridBagLayoutPanel()
    {
        fixedGridBagLayoutManager = new GridBagLayout()
        {

            @Override
            protected GridBagLayoutInfo getLayoutInfo(Container parent, int sizeflag)
            {
                return super.getLayoutInfo(parent, PREFERREDSIZE);
            }

            public Dimension minimumLayoutSize(Container parent)
            {
                GridBagLayoutInfo info = super.getLayoutInfo(parent, MINSIZE);
                return getMinSize(parent, info);
            }
        };

        setLayout(fixedGridBagLayoutManager);
    }

    /**
     * This method should not be called as this JPanel is dedicated for use with customized GridBagLayout. Though, it is called in initComponents (in NB GUI Builder).
     * This method should have empty body but NB GUI Builder doesn't work properly then.
     * @param mgr
     * @deprecated
     */
    @Deprecated
    @Override
    public void setLayout(LayoutManager mgr)
    {
        super.setLayout(mgr); //To change body of generated methods, choose Tools | Templates.
        super.setLayout(fixedGridBagLayoutManager);
    }

}
