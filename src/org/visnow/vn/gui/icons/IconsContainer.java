/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.gui.icons;

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class IconsContainer
{

    private static BufferedImage lightColorTable;
    private static BufferedImage indexL, indexR, indexLDisabled, indexRDisabled;
    private static BufferedImage ballBlack, ballGray, ballBlue1, ballBlue2, ballLight, 
                                 ballRed, ballGreen, ballYellow, ballMagenta;
    private static BufferedImage gouraud, flat, unshaded, background;
    private static BufferedImage front, back, all;
    private static BufferedImage ramp,reverseRamp,valley,peak,comb1,comb2,not0;
    
    private static BufferedImage cnt, dashed, dotted, dashdotted, dotdashdot, dashdashdot;

    private static void input()
    {
        try {
            lightColorTable = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/lightChooser.png"));
            indexL = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/index_l.png"));
            indexR = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/index_r.png"));
            indexLDisabled = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/index_l_grey.png"));
            indexRDisabled = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/index_r_grey.png"));
            ballBlack = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/bbs.png"));
            ballGray = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/b0s.png"));
            ballBlue1 = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/b1s.png"));
            ballBlue2 = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/b12s.png"));
            ballLight = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/b2s.png"));
            ballRed = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/b3s.png"));
            ballGreen = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/b4s.png"));
            ballYellow = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/b5s.png"));
            ballMagenta = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/b6s.png"));
            gouraud = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/surface/gouraud.png"));
            flat = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/surface/flat.png"));
            unshaded = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/surface/unshaded.png"));
            background = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/surface/background.png"));
            front = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/surface/front.jpg"));
            back = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/surface/back.jpg"));
            all = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/surface/all.jpg"));
            ramp = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/transparencies/trx.png"));
            reverseRamp = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/transparencies/tr-x.png"));
            valley = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/transparencies/trabsx.png"));
            peak = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/transparencies/trinvabsx.png"));
            comb1 = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/transparencies/comb1.png"));
            comb2 = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/transparencies/comb2.png"));
            not0 = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/transparencies/not0.png"));
            cnt = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/strokes/cnt.png"));
            dashed = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/strokes/dashed.png"));
            dotted = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/strokes/dotted.png"));
            dashdotted = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/strokes/dashdotted.png"));
            dotdashdot = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/strokes/dotdashdot.png"));
            dashdashdot = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/strokes/dashdashdot.png"));
        } catch (IOException e) {
        }
    }

    public static BufferedImage getGouraud()
    {
        if (gouraud == null) {
            try {
                gouraud = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/surface/gouraud.png"));
            } catch (IOException e) {
            }
        }
        return gouraud;
    }

    public static BufferedImage getFlat()
    {
        if (flat == null) {
            try {
                flat = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/surface/flat.png"));
            } catch (IOException e) {
            }
        }
        return flat;
    }

    public static BufferedImage getUnshaded()
    {
        if (unshaded == null) {
            try {
                unshaded = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/surface/unshaded.png"));
            } catch (IOException e) {
            }
        }
        return unshaded;
    }

    public static BufferedImage getBackground()
    {
        if (background == null) {
            try {
                background = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/surface/background.png"));
            } catch (IOException e) {
            }
        }
        return background;
    }

    public static BufferedImage getIndexL()
    {
        if (indexL == null) {
            try {
                indexL = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/index_l.png"));
            } catch (IOException e) {
            }
        }
        return indexL;
    }

    public static BufferedImage getIndexR()
    {
        if (indexR == null) {
            try {
                indexR = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/index_r.png"));
            } catch (IOException e) {
            }
        }
        return indexR;
    }

    public static BufferedImage getIndexLDisabled()
    {
        if (indexL == null) {
            try {
                indexLDisabled = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/index_l_grey.png"));
            } catch (IOException e) {
            }
        }
        return indexLDisabled;
    }

    public static BufferedImage getIndexRDisabled()
    {
        if (indexRDisabled == null) {
            try {
                indexRDisabled = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/index_r_grey.png"));
            } catch (IOException e) {
            }
        }
        return indexRDisabled;
    }

    public static BufferedImage getLightColorTable()
    {
        if (lightColorTable == null) {
            try {
                lightColorTable = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/lightChooser.png"));
            } catch (IOException e) {
            }
        }
        return lightColorTable;
    }

    public static BufferedImage getBallBlack()
    {
        if (ballBlack == null) {
            try {
                ballBlack = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/bbs.png"));
            } catch (IOException e) {
            }
        }
        return ballBlack;
    }

    public static BufferedImage getBallGray()
    {
        if (ballGray == null) {
            try {
                ballGray = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/b0s.png"));
            } catch (IOException e) {
            }
        }
        return ballGray;
    }

    public static BufferedImage getBallBlue1()
    {
        if (ballBlue1 == null) {
            try {
                ballBlue1 = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/b1s.png"));
            } catch (IOException e) {
            }
        }
        return ballBlue1;
    }

    public static BufferedImage getBallBlue2()
    {
        if (ballBlue2 == null) {
            try {
                ballBlue2 = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/b12s.png"));
            } catch (IOException e) {
            }
        }
        return ballBlue2;
    }

    public static BufferedImage getBallLight()
    {
        if (ballLight == null) {
            try {
                ballLight = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/b2s.png"));
            } catch (IOException e) {
            }
        }
        return ballLight;
    }

    public static BufferedImage getBallRed()
    {
        if (ballRed == null) {
            try {
                ballRed = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/b3s.png"));
            } catch (IOException e) {
            }
        }
        return ballRed;
    }

    public static BufferedImage getBallGreen()
    {
        if (ballGreen == null) {
            try {
                ballGreen = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/b4s.png"));
            } catch (IOException e) {
            }
        }
        return ballGreen;
    }

    public static BufferedImage getBallYellow()
    {
        if (ballYellow == null) {
            try {
                ballYellow = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/b5s.png"));
            } catch (IOException e) {
            }
        }
        return ballYellow;
    }

    public static BufferedImage getBallMagenta()
    {
        if (ballMagenta == null) {
            try {
                ballMagenta = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/b6s.png"));
            } catch (IOException e) {
            }
        }
        return ballMagenta;
    }

    public static BufferedImage getRamp()
    {
        if (ramp == null) 
            try {
                ramp = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/transparencies/trx.png"));
            } catch (IOException e) {
            }
        return ramp;
    }

    public static BufferedImage getReverseRamp()
    {
        if (reverseRamp == null) 
            try {
                reverseRamp = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/transparencies/tr-x.png"));
            } catch (IOException e) {
            }
        return reverseRamp;
    }

    public static BufferedImage getValley()
    {
        if (valley == null) 
            try {
                valley = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/transparencies/trabsx.png"));
            } catch (IOException e) {
            }
        return valley;
    }

    public static BufferedImage getPeak()
    {
        if (peak == null) 
            try {
                peak = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/transparencies/trinvabsx.png"));
            } catch (IOException e) {
            }
        return peak;
    }

    public static BufferedImage getComb1()
    {
        if (comb1 == null) 
            try {
                comb1  = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/transparencies/comb1.png"));
            } catch (IOException e) {
            }
        return comb1;
    }

    public static BufferedImage getComb2()
    {
        if (comb2 == null) 
            try {
                comb2  = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/transparencies/comb2.png"));
            } catch (IOException e) {
            }
        return comb2;
    }

    public static BufferedImage getNot0()
    {
        if (not0 == null) 
            try {
                not0  = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/transparencies/not0.png"));
            } catch (IOException e) {
            }
        return not0;
    }

    public static BufferedImage getFront() {
        if (front == null) 
            try {
                    front = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/surface/front.jpg"));
                } catch (Exception e) {
            }
        return front;
    }

    public static BufferedImage getBack() {
        if (back == null) 
            try {
                     back = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/surface/back.jpg"));
                } catch (IOException e) {
            }
        return back;
    }

    public static BufferedImage getAll() {
        if (all == null) 
            try {
                     all = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/surface/all.jpg"));
                } catch (IOException e) {
            }
        return all;
    }

    public static BufferedImage getCnt() {
        if (cnt == null) 
            try {
                     cnt = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/strokes/cnt.png"));
                } catch (IOException e) {
            }
        return cnt;
    }
    
    public static BufferedImage getDashed() {
        if (dashed == null) 
            try {
                     dashed = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/strokes/dashed.png"));
                } catch (IOException e) {
            }
        return dashed;
    }
    
    public static BufferedImage getDotted() {
        if (dotted == null) 
            try {
                     dotted = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/strokes/dotted.png"));
                } catch (IOException e) {
            }
        return dotted;
    }
    
    public static BufferedImage getDashDotted() {
        if (dashdotted == null) 
            try {
                     dashdotted = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/strokes/dashdotted.png"));
                } catch (IOException e) {
            }
        return dashdotted;
    }
    
    public static BufferedImage getDotDashDot() {
        if (dotdashdot == null) 
            try {
                     dotdashdot = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/strokes/dotdashdot.png"));
                } catch (IOException e) {
            }
        return dotdashdot;
    }
    
    public static BufferedImage getDashDashDot() {
        if (dashdashdot == null) 
            try {
                     dashdashdot = ImageIO.read(IconsContainer.class.getResource("/org/visnow/vn/gui/icons/strokes/dashdashdot.png"));
                } catch (IOException e) {
            }
        return dashdashdot;
    }
    
    private IconsContainer()
    {
    }

}
