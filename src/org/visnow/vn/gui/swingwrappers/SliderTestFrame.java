/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.gui.swingwrappers;

import java.awt.Font;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import org.apache.log4j.Logger;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.swing.UIStyle;

/**
 *
 * @author szpak
 */
public class SliderTestFrame extends javax.swing.JFrame
{

    private static final Logger LOGGER = Logger.getLogger(SliderTestFrame.class);

    /**
     * Creates new form SliderTextFrame
     */
    public SliderTestFrame()
    {
        initComponents();
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        slider1 = new org.visnow.vn.gui.swingwrappers.Slider();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        slider1.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                slider1StateChanged(evt);
            }
        });
        slider1.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                slider1UserChangeAction(evt);
            }

            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                slider1UserAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        getContentPane().add(slider1, gridBagConstraints);

        jButton1.setText("Set min 10");
        jButton1.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new java.awt.GridBagConstraints());

        jButton2.setText("Show value");
        jButton2.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new java.awt.GridBagConstraints());

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void slider1StateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_slider1StateChanged
    {//GEN-HEADEREND:event_slider1StateChanged
        LOGGER.debug(slider1.getValue() + " " + slider1.getValueIsAdjusting());
    }//GEN-LAST:event_slider1StateChanged

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton1ActionPerformed
    {//GEN-HEADEREND:event_jButton1ActionPerformed
        //        JSlider jSlider = slider1;
        ////        jSlider.setExtent(10);
        //        jSlider.setFont(new Font("Dialog", Font.BOLD, 20));
        ////        jSlider.setInverted(true);
        //        jSlider.setLabelTable(jSlider.createStandardLabels(10));
        //        jSlider.setMajorTickSpacing(2);
        //        jSlider.setMinimum(60); 
        ////        jSlider.setMaximum(10);
        //        jSlider.setMinorTickSpacing(17);
        ////        jSlider.setOrientation(SwingConstants.VERTICAL);
        //        jSlider.setPaintLabels(true);
        //        jSlider.setPaintTicks(true);
        //        jSlider.setPaintTrack(true);
        ////        jSlider.setSnapToTicks(true);
        ////        jSlider.setValue(20); //BasicSliderUI :/
        ////        jSlider.setValueIsAdjusting(true);  //BasicSliderUI :/
        //        
        ////        setValue i setValueIsAdjusting ustawiaja sie w BasicSliderUI, wiec nie mozna ich tak przeciazyc
        ////                bedzie trzeba zrobic deprecated i zrobic setVal
        //                        
        //        
        //        //setMinimum/Maximum/Value/Extent/
        //        //setValueIsAdjusting
        //        //setSnapToTicks
        //     
        ////        jSlider.getModel().setValue(8);
        slider1.setVal(23);
        slider1.setMinimum(40);
        slider1.setMaximum(10);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton2ActionPerformed
    {//GEN-HEADEREND:event_jButton2ActionPerformed
        LOGGER.debug(slider1.getValue());
    }//GEN-LAST:event_jButton2ActionPerformed

    private void slider1UserAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_slider1UserAction
    {//GEN-HEADEREND:event_slider1UserAction
        LOGGER.debug(slider1.getValue() + " " + slider1.getValueIsAdjusting());
    }//GEN-LAST:event_slider1UserAction

    private void slider1UserChangeAction(java.util.EventObject evt)//GEN-FIRST:event_slider1UserChangeAction
    {//GEN-HEADEREND:event_slider1UserChangeAction
        LOGGER.debug(slider1.getValue() + " " + slider1.getValueIsAdjusting());
    }//GEN-LAST:event_slider1UserChangeAction

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        VisNow.initLogging(true);
        UIStyle.initStyle();
        /* Set the Nimbus look and feel */
        //        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        //        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
        //         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
        //         */
        //        try {
        //            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        //                if ("Nimbus".equals(info.getName())) {
        //                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
        //                    break;
        //                }
        //            }
        //        } catch (ClassNotFoundException ex) {
        //            java.util.logging.Logger.getLogger(SliderTextFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        //        } catch (InstantiationException ex) {
        //            java.util.logging.Logger.getLogger(SliderTextFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        //        } catch (IllegalAccessException ex) {
        //            java.util.logging.Logger.getLogger(SliderTextFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        //        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
        //            java.util.logging.Logger.getLogger(SliderTextFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        //        }
        //        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new SliderTestFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private org.visnow.vn.gui.swingwrappers.Slider slider1;
    // End of variables declaration//GEN-END:variables
}
