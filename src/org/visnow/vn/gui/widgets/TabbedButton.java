/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.gui.widgets;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;
import javax.swing.JComponent;

/**
 *
 * @author vis
 */
public class TabbedButton extends JComponent
{

    protected int clickedId;
    protected boolean clicked;
    protected Image image;
    protected Vector<Rectangle> clickAreas;
    protected Vector<Rectangle> selectedImageRect;
    protected Vector<Rectangle> clickedImageRect;

    public int getActiveTab()
    {
        return clickedId;
    }

    public TabbedButton()
    {
        this.clicked = false;
        this.clickedId = 0;
        this.clickAreas = new Vector<Rectangle>();
        this.selectedImageRect = new Vector<Rectangle>();
        this.clickedImageRect = new Vector<Rectangle>();
        addMouseListener(new MouseAdapter()
        {
            @Override
            public void mousePressed(MouseEvent e)
            {
                clicked = true;
                for (int i = 0; i < clickAreas.size(); i++) {
                    if (clickAreas.get(i).contains(e.getPoint())) {
                        clickedId = i;
                        break;
                    }
                }
                repaint();
            }

            @Override
            public void mouseReleased(MouseEvent e)
            {
                clicked = false;
                repaint();
            }
        });
    }

    public void addTab(Rectangle clickArea, Rectangle selectedArea, Rectangle clickedArea)
    {
        clickAreas.add(clickArea);
        selectedImageRect.add(selectedArea);
        clickedImageRect.add(clickedArea);
    }

    public void paintSubImage(Graphics g, Rectangle rect)
    {
        Insets insets;
        if (getBorder() != null) {
            insets = getBorder().getBorderInsets(this);
        } else {
            insets = new Insets(0, 0, 0, 0);
        }
        g.drawImage(image,
                    insets.left, insets.top,
                    rect.width - insets.left - insets.right,
                    rect.height - insets.top - insets.bottom,
                    rect.x, rect.y,
                    rect.x + rect.width, rect.y + rect.height,
                    getBackground(), null);
    }

    public boolean isClicked()
    {
        return clicked;
    }

    public void setClicked(boolean clicked)
    {
        this.clicked = clicked;
    }

    public Image getImage()
    {
        return image;
    }

    public void setImage(Image image)
    {
        this.image = image;
    }

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        if (image != null && clickAreas.size() > 0) {
            if (clicked) {
                paintSubImage(g, clickedImageRect.get(clickedId));
            } else {
                paintSubImage(g, selectedImageRect.get(clickedId));
            }

        }
    }
}
