/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.gui.widgets;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import javax.swing.FocusManager;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;
import org.apache.log4j.Logger;
import org.visnow.jscic.utils.ArrayUtils;
import org.visnow.jscic.utils.EngineeringFormattingUtils;
import static org.visnow.jscic.utils.NumberUtils.*;
import org.visnow.vn.gui.components.NumericTextField;
import org.visnow.vn.gui.components.NumericTextField.FieldType;
import static org.visnow.vn.gui.components.NumericTextField.*;
import org.visnow.vn.gui.swingwrappers.UserActionListener;
import org.visnow.vn.lib.utils.labelling.LabelGenerator;
import org.visnow.vn.lib.utils.labelling.LabelGenerator.SpaceChecker;
import org.visnow.vn.lib.utils.labelling.Labelling;
import org.visnow.vn.system.swing.UIStyle;

/**
 * Main assumption is that slider model is always in correct state! (so setters also validate model!)
 * (but be carefull because NB designer can set min/max/val in random order (by accident alphabetical order max &le; min &le; val is good))
 * <p>
 * Setters are silent! (no fire*Changed is called!)
 * <p>
 * There is no internal model directly in ExtendedSlider class - internal model is stored in NumericTextFields (!) and it consists of:
 * minValue (+ globalMinValue), currentValue, maxValue (+ globaMaxValue) so we always have
 * globaMinValue &le; minValue &le; currentValue &le; maxValue &le; globalMaxValue;
 * globalMin and globalMax are stored in both: minTF and maxTF.
 * <p>
 * This slider uses same field types as NumericTextField so Double, Float, Long, Integer, Short, Byte are allowed.
 * <p>
 * Main assumption is that min/max of JSlider are log of (sliderMax/sliderMin), + const, * const
 * <p>
 * Difference between linear and logarithmic scale are in:
 * - globalMin/Max (always positive for logarithmic scale)
 * - updateModel (depends on globalMin/Max - so no change in code)
 * - recalcSliderIfNeeded (needs to be called each time scale change is done)
 * - recalcSlider (labels calculated differently)
 * - sliderStateChanged (different calculations for log/linear scale)
 * - updateSliderThumb (different calculations for log/linear scale)
 * <p>
 * Slider should start as linear scale slider and then can be set to logarithmic to avoid NetBeans GUI designer bugs (linear range is wider).
 * <p>
 * When changing scale type then following actions are performed:
 * 1. global Min/Max are updated
 * 2. model is updated
 * 3. slider is recalculated
 * Note: Changing scale type is kind of setter and is not considered as User Action! So no event is fired here... (even if value has changed!)
 * <p>
 * ValueChangedAction (UserAction) is fired when:
 * 1. userVal is changed while user slides slider (if submitOnAdjusting == true)
 * 2. userVal is changed after user stopped adjusting (if submitOnAdjusting == false)
 * 3. userVal is changed after user entered new value in valNF
 * 4. userVal is changed after user entered new value in maxNF or minNF and then valNF was adjusted to range
 * <p>
 * Note:
 * This component assumes that input values are "good enough" to do Java provided arithmetics, so it may not work properly
 * if Double.MIN_VALUE and Double.MAX_VALUE are passed as slider range.
 * <p>
 * @author szpak
 */
//TODO: test number of runs in recalculateSlider/ifNeeded while init slider
//TODO: change numeric fields formatting (from scientific one) - now for log range 999999....999999.1 no change can be seen in numeric field
public class ExtendedSlider extends javax.swing.JPanel
{
    private static final Logger LOGGER = Logger.getLogger(ExtendedSlider.class);

    private NumericTextField.FieldType type = FieldType.DOUBLE;

    //is logarithmic or linear scale (default)
    private ScaleType scaleType = ScaleType.LINEAR;
    //numeric field font
//    private Font textFieldFont;
    //flag if slider should do any action in sliderStateChanged (this is to distinguish between user action and setters/update)
    private boolean jsliderActive = true;
    //
    private boolean showingFields = false;
    private boolean showFieldsButtonVisible = true;
    private boolean fireUserChangeOnMinMaxUpdate = false;

    //used in non-submitOnAdjusting mode to avoid fireUserChange only in one case: if slider goes back to initial position!
    private Object valueOnBeginSlide = null;

    public ExtendedSlider()
    {
        initComponents();
        FontUIResource sliderFont = (FontUIResource) UIManager.get(UIStyle.vn_ExtendedSlider_font.getKey());
        if (sliderFont != null) super.setFont(sliderFont.deriveFont(sliderFont.getSize()));
        setTextFieldFont(super.getFont());

        validateModelAndUpdateUI(ModelChangeSource.NONE, true);
        //recalculate and update thumb
        recalcSlider();
        updateShowFieldsButton();
        showFields();
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT
     * modify this code. The content of this method is always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        sliderPanel = new org.visnow.vn.system.swing.CustomSizePanel();
        slider = new RollSlider();
        fieldsPanel = new org.visnow.vn.system.swing.CustomSizePanel();
        maxNF = new org.visnow.vn.gui.components.NumericTextField();
        valNF = new org.visnow.vn.gui.components.NumericTextField();
        minNF = new org.visnow.vn.gui.components.NumericTextField();
        showFieldsButton = new javax.swing.JButton();

        addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                formAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });
        setLayout(new java.awt.GridBagLayout());

        sliderPanel.setCustomWidth(20);
        sliderPanel.setOverrideMinSize(true);
        sliderPanel.setLayout(new java.awt.GridBagLayout());

        slider.setMaximum(1000);
        slider.setPaintLabels(true);
        slider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sliderStateChanged(evt);
            }
        });
        slider.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                sliderMouseDragged(evt);
            }
        });
        slider.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                sliderMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                sliderMouseReleased(evt);
            }
        });
        slider.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                sliderComponentResized(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        sliderPanel.add(slider, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.6;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 2);
        add(sliderPanel, gridBagConstraints);

        fieldsPanel.setBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Slider.altTrackColor")));
        fieldsPanel.setCustomWidth(20);
        fieldsPanel.setOverrideMinSize(true);
        fieldsPanel.setLayout(new java.awt.GridBagLayout());

        maxNF.setValue(Double.MAX_VALUE);
        maxNF.setBackground(new java.awt.Color(247, 247, 247));
        maxNF.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 4, 0, 2, new java.awt.Color(247, 247, 247)));
        maxNF.setHorizontalAlignment(4);
        maxNF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                maxNFValueChangedAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        fieldsPanel.add(maxNF, gridBagConstraints);

        valNF.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 4, 0, 2, new java.awt.Color(255, 255, 255)));
        valNF.setHorizontalAlignment(4);
        valNF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                valNFValueChangedAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        fieldsPanel.add(valNF, gridBagConstraints);

        minNF.setBackground(new java.awt.Color(247, 247, 247));
        minNF.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 4, 0, 2, new java.awt.Color(247, 247, 247)));
        minNF.setHorizontalAlignment(4);
        minNF.setValue(-Double.MAX_VALUE);
        minNF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                minNFValueChangedAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        fieldsPanel.add(minNF, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.2;
        gridBagConstraints.weighty = 1.0;
        add(fieldsPanel, gridBagConstraints);

        showFieldsButton.setBackground(new java.awt.Color(200, 221, 242));
        showFieldsButton.setForeground(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow"));
        showFieldsButton.setText("<html>&minus;");
        showFieldsButton.setToolTipText("precise fields toggle");
        showFieldsButton.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 1, 0, 1, new java.awt.Color(200, 221, 242)));
        showFieldsButton.setBorderPainted(false);
        showFieldsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showFieldsButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.001;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        add(showFieldsButton, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void sliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sliderStateChanged
        if (jsliderActive && slider.getMaximum() != slider.getMinimum()) { //if user action
            Object prevVal = valNF.getValue();
            if (valueOnBeginSlide == null) valueOnBeginSlide = prevVal;

            double sliderPosNorm = Math.max(0.0, Math.min(1.0, (double) (slider.getValue() - slider.getMinimum()) / (slider.getMaximum() - slider.getMinimum())));

            if (type.isReal()) {

                BigDecimal sliderPosNormBD = toBigDecimal(sliderPosNorm);

                double[] mmv = getMinMaxValAsDouble();
                double min = mmv[0];
                double max = mmv[1];
                BigDecimal minBD = toBigDecimal(min);
                BigDecimal maxBD = toBigDecimal(max);
                BigDecimal value;

                //this is to avoid rounding problems
                if (sliderPosNorm == 1.0) value = maxBD;
                else if (sliderPosNorm == 0.0) value = minBD;
                else if (scaleType == ScaleType.LOGARITHMIC)
                    value = toBigDecimal(Math.pow(10, Math.log10(min) + sliderPosNorm * (Math.log10(max) - Math.log10(min))));
                else if (scaleType == ScaleType.LINEAR)
                    //              min + sliderPosNorm * (max - min);
                    value = minBD.add(sliderPosNormBD.multiply(maxBD.subtract(minBD)));
                else
                    throw new RuntimeException("Incorrect scale type");

                valNF.setValue(value.doubleValue());//doubleVal);
            } else {
                long[] mmv = getMinMaxValAsLong();
                long min = mmv[0];
                long max = mmv[1];
                long value;

                if (sliderPosNorm == 1.0) value = max;
                else if (sliderPosNorm == 0.0) value = min;
                else if (scaleType == ScaleType.LOGARITHMIC)
                    value = (long) Math.round(Math.pow(10, Math.log10(min) + sliderPosNorm * (Math.log10(max) - Math.log10(min))));
                else if (scaleType == ScaleType.LINEAR) value = (long) Math.round(min + sliderPosNorm * (max - min));
                else throw new RuntimeException("Incorrect scale type");

                valNF.setValue(value);
            }

            updateSliderToolTip();
            if (submitOnAdjusting && valNF.compareTo(prevVal) != 0 || !submitOnAdjusting && !isAdjusting() && valNF.compareTo(valueOnBeginSlide) != 0) {
                valueOnBeginSlide = null;
                fireValueChanged();
            }
            fireUserAction();
        }
    }//GEN-LAST:event_sliderStateChanged

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
//        LOGGER.debug("");
        recalcSliderIfNeeded(RecalculateReason.RESIZE);
    }//GEN-LAST:event_formComponentResized

    Map<RecalculateReason, Object> prevReason = new HashMap<RecalculateReason, Object>();

    /**
     * Recalculates slider only if reason-related data have changed. This is to avoid multiple recalculation (especially in resize-like events:
     * componentResized, ancestorAdded).
     * That was first added because componentResized was no fired in VolumeSegmentation so other componentEvent listeners were added which led to duplication
     * (call for recalculation for same slider width).
     */
    private void recalcSliderIfNeeded(RecalculateReason reason)
    {
        //        LOGGER.debug(reason);
        switch (reason) {
            case FONT_CHANGED:
                if (!super.getFont().equals(prevReason.get(reason))) {
                    prevReason.put(reason, super.getFont());
                    recalcSlider();
                }
                break;
            case RESIZE:
                //        LOGGER.info("" + slider.getWidth());
                if (!((Integer) slider.getWidth()).equals(prevReason.get(reason))) {
                    prevReason.put(reason, (Integer) slider.getWidth());
                    recalcSlider();
                }
                break;
            case MODEL_CHANGED:
                Object[] minMax = (Object[]) prevReason.get(reason);
                Object[] currentMinMax = new Object[]{minNF.getValue(), maxNF.getValue()};
                boolean changed = minMax == null ||
                        !minMax[0].getClass().equals(currentMinMax[0].getClass()) ||
                        minNF.compareTo(minMax[0]) != 0 ||
                        maxNF.compareTo(minMax[1]) != 0;
                if (changed) {
                    prevReason.put(reason, currentMinMax);
                    recalcSlider();
                }
                break;
            case SCALE_TYPE_CHANGED:
                if (!scaleType.equals(prevReason.get(reason))) {
                    prevReason.put(reason, scaleType);
                    recalcSlider();
                }
                break;
        }
    }

    /**
     * FontMetrics changed, model (userMin/userMax) changed, slider width changed, scale type (logarithmic/linear) changed;
     * This reason doesn't really mean that this particular property has changed. But possibly changed - and it's tested in
     * recalculateIfNeeded.
     */
    private enum RecalculateReason
    {
        FONT_CHANGED, MODEL_CHANGED, RESIZE, SCALE_TYPE_CHANGED;
    }

    public enum ScaleType
    {
        LINEAR, LOGARITHMIC;
    }

    public void setFireUserChangeOnMinMaxUpdate(boolean fireUserChangeOnMinMaxUpdate) {
        this.fireUserChangeOnMinMaxUpdate = fireUserChangeOnMinMaxUpdate;
    }

    private void formAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_formAncestorAdded
        recalcSliderIfNeeded(RecalculateReason.RESIZE);
    }//GEN-LAST:event_formAncestorAdded

    private void sliderComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_sliderComponentResized
//        LOGGER.debug("");
        recalcSliderIfNeeded(RecalculateReason.RESIZE);
    }//GEN-LAST:event_sliderComponentResized

    private void maxNFValueChangedAction(java.util.EventObject evt) {//GEN-FIRST:event_maxNFValueChangedAction
        Object prevVal = valNF.getValue();
        validateModelAndUpdateUI(ModelChangeSource.MAX, true);
        recalcSliderIfNeeded(RecalculateReason.MODEL_CHANGED);
        //fire state changed if userVal was changed (adjusted to range) (because it's fired by user action - type into max field)
        if (valNF.compareTo(prevVal) != 0 || fireUserChangeOnMinMaxUpdate) {
            fireValueChanged(); //real value change -> notify listeners
            fireUserAction();
        }
    }//GEN-LAST:event_maxNFValueChangedAction

    private void minNFValueChangedAction(java.util.EventObject evt) {//GEN-FIRST:event_minNFValueChangedAction
        Object prevVal = valNF.getValue();
        validateModelAndUpdateUI(ModelChangeSource.MIN, true);
        recalcSliderIfNeeded(RecalculateReason.MODEL_CHANGED);

        //fire state changed if userVal was changed (adjusted to range) (because it's fired by user action - type into max field)
        if (valNF.compareTo(prevVal) != 0 || fireUserChangeOnMinMaxUpdate) {
            fireValueChanged(); //real value change -> notify listeners
            fireUserAction();
        }
    }//GEN-LAST:event_minNFValueChangedAction

    private void valNFValueChangedAction(java.util.EventObject evt) {//GEN-FIRST:event_valNFValueChangedAction
        //assume: value is always inside min/max fields (proper .min/.max are set in valNF) 
        updateSliderThumb();

        //so value was always changed
        fireValueChanged();
        fireUserAction();
    }//GEN-LAST:event_valNFValueChangedAction

    private void showFieldsButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_showFieldsButtonActionPerformed
    {//GEN-HEADEREND:event_showFieldsButtonActionPerformed
        showingFields = !showingFields;
        showFields();
        FocusManager.getCurrentManager().focusPreviousComponent();
    }//GEN-LAST:event_showFieldsButtonActionPerformed


    private void sliderMouseDragged(java.awt.event.MouseEvent evt)//GEN-FIRST:event_sliderMouseDragged
    {//GEN-HEADEREND:event_sliderMouseDragged
        if (slider.getValueIsAdjusting()) {

            MouseEvent phantom = new MouseEvent(slider, MouseEvent.MOUSE_MOVED, System.currentTimeMillis(), 0, evt.getX(), evt.getY(), 0, false);

            ToolTipManager.sharedInstance().mouseMoved(phantom);

        }
    }//GEN-LAST:event_sliderMouseDragged

    private int initialDelay;
    private int reshowDelay;
    private int dismissDelay;

    private void sliderMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_sliderMousePressed
    {//GEN-HEADEREND:event_sliderMousePressed
        initialDelay = ToolTipManager.sharedInstance().getInitialDelay();
        reshowDelay = ToolTipManager.sharedInstance().getReshowDelay();
        dismissDelay = ToolTipManager.sharedInstance().getDismissDelay();
        ToolTipManager.sharedInstance().setInitialDelay(0);
        ToolTipManager.sharedInstance().setReshowDelay(0);
        ToolTipManager.sharedInstance().setDismissDelay(100000);
    }//GEN-LAST:event_sliderMousePressed

    private void sliderMouseReleased(java.awt.event.MouseEvent evt)//GEN-FIRST:event_sliderMouseReleased
    {//GEN-HEADEREND:event_sliderMouseReleased
        ToolTipManager.sharedInstance().setInitialDelay(initialDelay);
        ToolTipManager.sharedInstance().setReshowDelay(reshowDelay);
        ToolTipManager.sharedInstance().setDismissDelay(dismissDelay);
    }//GEN-LAST:event_sliderMouseReleased

    static class DefaultSpaceChecker implements SpaceChecker
    {
        int sliderWidth;
        FontMetrics fontMetrics;

        @Override
        public boolean isEnoughSpace(Labelling labelling)
        {
            int maxLabelWidth = labelling.labelMaxWidth(fontMetrics);
            //FIXME: hardcoded for Metal L&F
            //TODO: In VisNow Thumb has width 15px (7px each side). Good news is that slider.getWidth() gives correct answer (checked in Gimp)
            int thumbWidth = 15;
            //FIXME: precise calculations (+1)
            int maxOverflow = Math.max(thumbWidth / 2, (maxLabelWidth + 1) / 2);
            int sliderTrimmedWidth = sliderWidth - 2 * maxOverflow;

//            LOGGER.debug(maxLabelWidth + " " + sliderWidth);
            //if widest label is too wide (relatively to sliderWidth after padding) then mark as 'not enough space'            
            if (maxLabelWidth > sliderWidth * 3 / 5) return false;
            else {
                //minimum distance calculated as outcome of label count and label max width (previously slider width was taken into consideration but it triggered labels "blinking")
//                int minimumDistancePx = 1;//max(3, max(labelCount / 2, maxLabelWidth * 3 / 5));
                int minimumDistancePx = Math.max(3, Math.max(labelling.labelCount() / 2, maxLabelWidth / 3));
//                int minimumDistancePx = Math.max(3, Math.max(labelling.labelCount() / 2, maxLabelWidth / 2));
                return labelling.isEnoughSpace(sliderTrimmedWidth, fontMetrics, minimumDistancePx, maxOverflow);
            }
        }
    };

    private DefaultSpaceChecker defaultSpaceChecker = new DefaultSpaceChecker();

    private LabelGenerator.Formatter linearFormatter = new LabelGenerator.Formatter()
    {

        @Override
        public String[] format(Number[] numbersAscending, boolean fullPrecision)
        {
            if (numbersAscending.length == 0) return new String[]{};
            else if (isReal(numbersAscending[0])) {
                String[] labels;
                if (fullPrecision)
                    labels = EngineeringFormattingUtils.format(unbox(convertToDouble(numbersAscending)), 18, true);
                else labels = EngineeringFormattingUtils.formatInContext(unbox(convertToDouble(numbersAscending)), 300, 1, true);
                for (int i = 0; i < labels.length; i++)
                    labels[i] = labels[i].replace("E", "e");
                return labels;
            } else
                return ArrayUtils.toString(numbersAscending);
        }
    };

    private LabelGenerator.Formatter logarithmicFormatter = new LabelGenerator.Formatter()
    {

        @Override
        public String[] format(Number[] numbersAscending, boolean fullPrecision)
        {
            if (numbersAscending.length == 0) return new String[]{};
            else if (isReal(numbersAscending[0])) {
                String[] labels;
                if (fullPrecision)
                    labels = EngineeringFormattingUtils.format(unbox(convertToDouble(numbersAscending)), 18, true);
                else labels = EngineeringFormattingUtils.formatInPairs(unbox(convertToDouble(numbersAscending)), 1, true);
                for (int i = 0; i < labels.length; i++)
                    labels[i] = labels[i].replace("E", "e");
                return labels;
            } else
                return ArrayUtils.toString(numbersAscending);
        }
    };

    /**
     * Recalculates slider visual range, slider step, slider ticks, slider labels and updates thumb position.
     *
     * Assumes that model is correct so allowedMin &le; userMin &le; userVal &le; userMax &le; allowedMax.
     * Particularly it can be userMin == userMax.
     *
     * This method does not change userMax/Min/Val values nor text fields. It sets slider range/labels/ticks & updates thumb (passive - no event fired).
     */
    private void recalcSlider()
    {
        jsliderActive = false;
        slider.setPaintLabels(false);
        int sliderWidth = slider.getWidth();

        double[] mmv = getMinMaxValAsDouble();
        double min = mmv[0];
        double max = mmv[1];

        if (sliderWidth == 0 || min == max) { //just in case as a separate case
            slider.setMaximum(0);
        } else {
            defaultSpaceChecker.fontMetrics = slider.getFontMetrics(super.getFont());
            defaultSpaceChecker.sliderWidth = sliderWidth;

            //can't be done with real slider because double numbers are not uniformly distributed!
//            if (scaleType == ScaleType.LOGARITHMIC || (max - min) / Math.ulp(max) > sliderWidth * 4)
            if (scaleType == ScaleType.LINEAR && !type.isReal())// && (max - min) < sliderWidth * 4)
                slider.setMaximum(truncateToInt(Math.round(max - min)));
            else
                slider.setMaximum(sliderWidth * 4);

//            LOGGER.debug(slider.getMinimum() + " .. " + slider.getMaximum());
//            else {
//                slider.setMaximum((int)Math.round((max - min) / Math.ulp(max)));
//            } 
            //Limit labelling to avoid numeric problems
            if (max - min < Double.MAX_VALUE / 1e10 && (max == min || max - min > Double.MIN_VALUE * 1e10)) {
                Labelling labelling;
                if (scaleType == ScaleType.LINEAR) {
                    if (type.isReal()) labelling = LabelGenerator.createLinearLabels(min, max, defaultSpaceChecker, linearFormatter);
                    else labelling = LabelGenerator.createLinearLabels(Math.round(min), Math.round(max), defaultSpaceChecker, linearFormatter);
                } else {
                    if (type.isReal()) labelling = LabelGenerator.createLogLabels(min, max, defaultSpaceChecker, logarithmicFormatter);
                    else labelling = LabelGenerator.createLogLabels(Math.round(min), Math.round(max), defaultSpaceChecker, logarithmicFormatter);
                }

                Dictionary<Integer, JLabel> labels = new Hashtable<>();
                int[] labelPositions = labelling.getLabelPositions(0, slider.getMaximum());

                for (int i = 0; i < labelling.labelCount(); i++) {
                    JLabel label = new JLabel(labelling.getLabels()[i]);
                    label.setFont(super.getFont()); //need to set same font as parent font
                    labels.put(labelPositions[i], label);
                }

                if (labels.size() > 0) slider.setLabelTable(labels);
                slider.setPaintLabels(labels.size() > 0);
            }
            updateSliderThumb();
        }
        jsliderActive = true;
    }

    /**
     * Updates slider thumb; This method assumes that sliderMin/sliderMax and range of backed JSlider are correctly set.
     * This method is not active! so sliderStateChanged is set as inactive while updating slider.
     */
    private void updateSliderThumb()
    {
        if (maxNF.compareTo(minNF) != 0) {

            double[] mmv = getMinMaxValAsDouble();
            double min = mmv[0];
            double max = mmv[1];
            double val = mmv[2];

            double posNormalized;

            if (scaleType == ScaleType.LOGARITHMIC) {
                double size = Math.log(max) - Math.log(min);
                double pos = Math.log(val) - Math.log(min);
                posNormalized = pos / size; //position 0 ... 1
            } else if (scaleType == ScaleType.LINEAR) {
                if (type.isReal()) {
                    //scale is to avoid arithmetic problems when min/max are about Double.MIN/MAX_VALUE
                    //this could be possibly also tested against max - min == Infinity
                    //doesn't work properly for MAX_DOUBLE, MIN_DOUBLE anyway.
                    double scale = Math.abs(max) > 1 && Math.abs(min) > 1 ? 1e-10 : 1e10;
                    min *= scale;
                    max *= scale;
                    val *= scale;
                }

                posNormalized = (val - min) / (max - min);
            } else
                throw new RuntimeException("Incorrect scale type");

            if (Double.isInfinite(posNormalized) || Double.isNaN(posNormalized)) posNormalized = 0.5;

            jsliderActive = false;
            slider.setValue((int) Math.round(posNormalized * (slider.getMaximum() - slider.getMinimum()) + slider.getMinimum()));
            jsliderActive = true;
        }
    }

    private void updateSliderToolTip()
    {
        Number min = minNF.getValue();
        Number max = maxNF.getValue();
        Number val = valNF.getValue();
        String toolTip;

        if (type.isReal()) {
            if (max.doubleValue() - min.doubleValue() > Double.MAX_VALUE / 1000)
                toolTip = EngineeringFormattingUtils.format(val.doubleValue());
            else
                toolTip = EngineeringFormattingUtils.formatInContext(new double[]{min.doubleValue(), val.doubleValue(), max.doubleValue()})[1];

        } else
            toolTip = "" + val.longValue();

        slider.setToolTipText(toolTip);
    }

    private double[] getMinMaxValAsDouble()
    {
        if (type.isReal())
            return new double[]{
                createDoubleFromObject(type, minNF.getValue()),
                createDoubleFromObject(type, maxNF.getValue()),
                createDoubleFromObject(type, valNF.getValue())
            };
        else
            return new double[]{
                createLongFromObject(type, minNF.getValue()),
                createLongFromObject(type, maxNF.getValue()),
                createLongFromObject(type, valNF.getValue())
            };
    }

    private long[] getMinMaxValAsLong()
    {
        if (type.isReal()) throw new IllegalArgumentException("Illegal type");
        else
            return new long[]{
                createLongFromObject(type, minNF.getValue()),
                createLongFromObject(type, maxNF.getValue()),
                createLongFromObject(type, valNF.getValue())
            };
    }

    @Override
    public void setFont(Font font)
    {
        super.setFont(font); //To change body of generated methods, choose Tools | Templates.
        if (slider != null) {
            slider.setFont(font); //does not fire state changed (OK)
            recalcSliderIfNeeded(RecalculateReason.FONT_CHANGED);
        }
        if (valNF != null && maxNF != null && minNF != null)
            setTextFieldFont(font);
    }

    public Number getGlobalMin()
    {
        return minNF.getMin();
    }

    public void setGlobalMin(Number min)
    {
        setGlobalMinInternal(min);
    }

    private void setGlobalMinInternal(Number min)
    {
        minNF.setMin(min);
        maxNF.setMin(min);
        validateModelAndUpdateUI(ModelChangeSource.MIN, true);
        recalcSliderIfNeeded(RecalculateReason.MODEL_CHANGED);
    }

    public Number getGlobalMax()
    {
        return maxNF.getMax();
    }

    public void setGlobalMax(Number max)
    {
        minNF.setMax(max);
        maxNF.setMax(max);
        validateModelAndUpdateUI(ModelChangeSource.MAX, true);
        recalcSliderIfNeeded(RecalculateReason.MODEL_CHANGED);
    }

    public Number getMin()
    {
        return minNF.getValue();
    }

    /**
     * Sets userMin and validates full range: allowedMin &le; userMin &le; userVal &le; userMax &le; allowedMax.
     * Recalculates slider.
     */
    public void setMin(Number min)
    {
        minNF.setValue(min);
        validateModelAndUpdateUI(ModelChangeSource.MIN, true);
        recalcSliderIfNeeded(RecalculateReason.MODEL_CHANGED);
    }

    public Number getMax()
    {
        return maxNF.getValue();
    }

    /**
     * Recalculates slider.
     */
    public void setMax(Number max)
    {
        maxNF.setValue(max);
        validateModelAndUpdateUI(ModelChangeSource.MAX, true);
        recalcSliderIfNeeded(RecalculateReason.MODEL_CHANGED);
    }

    public Number getValue()
    {
        return valNF.getValue();
    }

    //XXX: should change value / formatting on set? -> NumericTextField
    /**
     * Recalculates slider.
     */
    public void setValue(Number val)
    {
        valNF.setValue((Number) val);
        validateModelAndUpdateUI(ModelChangeSource.VALUE, true);
        recalcSliderIfNeeded(RecalculateReason.MODEL_CHANGED);
    }

    /**
     * Sets globalMin, min, max, globalMax and validates full range:
     * globalMin &le; min &le; max &le; globalMax.
     * Recalculates slider.
     */
    public void setAllMinMax(Number min, Number max, Number globalMin, Number globalMax)
    {
        minNF.setValue(min);
        maxNF.setValue(max);
        minNF.setMin(globalMin);
        minNF.setMax(globalMax);
        maxNF.setMin(globalMin);
        maxNF.setMax(globalMax);
        validateModelAndUpdateUI(ModelChangeSource.NONE, true);
        recalcSliderIfNeeded(RecalculateReason.MODEL_CHANGED);
    }

    /**
     * Sets userMin, userMax, userVal and validates full range: allowedMin &le; userMin &le; userVal &le; userMax &le; allowedMax.
     * Recalculates slider.
     */
    public void setAll(Number min, Number max, Number val)
    {
        minNF.setValue(min);
        maxNF.setValue(max);
        valNF.setValue(val);
        validateModelAndUpdateUI(ModelChangeSource.NONE, true);
        recalcSliderIfNeeded(RecalculateReason.MODEL_CHANGED);
    }

    private enum ModelChangeSource
    {
        MIN, MAX, VALUE, NONE;
    }

    /**
     * Validates min, max and value to meet: min &le; val &le; max
     * Updates UI: text fields (and internal model in the same time) + thumb (does not recalculate slider).
     * Works in order:
     * <ol>
     * <li> sets userMin and userMax to meet allowedMin &le; userMin, userMax &le; allowedMax.
     * <li> sets userMin and userMax to meet userMin &le; userMax
     * <li> sets userVal to be between userMin and userMax
     * <li> update UI (calls updateUIValues)
     * </ol>
     * If changeSource == MIN || changeSource == NONE then userMax is updated to be &ge; userMin
     * If changeSource == MAX then userMin is updated to be &lt; userMax
     * On any change corresponding text field is updated (+ slider thumb if necessary). UI is updated ONLY ON CHANGE! so slider won't be updated
     * if value is changed but it's within range, so does not need to be adjusted to range.
     */
    private void validateModelAndUpdateUI(ModelChangeSource changeSource, boolean updateThumb)
    {
        //1. validate model when log scale
        if (scaleType == ScaleType.LOGARITHMIC) {
            Number currentGlobalMin = minNF.getMin();
            Number scaleMin;
            if (!type.isReal()) scaleMin = 1;
            else {
                if (type == FieldType.DOUBLE) scaleMin = Double.MIN_VALUE;
                else scaleMin = Float.MIN_VALUE;
            }
            if (NumericTextField.compare(scaleMin, currentGlobalMin) > 0)
                setGlobalMinInternal(scaleMin);
        }

        //2. cross-validate min/max
        if (minNF.compareTo(maxNF) > 0) //incorrect: min > max
            if (changeSource == ModelChangeSource.MAX)
                minNF.setValue(maxNF.getValue());
            else
                maxNF.setValue(minNF.getValue());

        //3a. validate min/max against value (if value is a change source)
        if (changeSource == ModelChangeSource.VALUE) {
            if (minNF.compareTo(valNF) > 0) minNF.setValue(valNF.getValue());
            if (maxNF.compareTo(valNF) < 0) maxNF.setValue(valNF.getValue());
        }

        //3b. (auto) validate/update value
        valNF.setMin(minNF.getValue());
        valNF.setMax(maxNF.getValue());

        if (updateThumb) {
            updateSliderThumb();
            updateSliderToolTip();
        }
    }

    private void setTextFieldFont(Font textFont)
    {
        maxNF.setFont(textFont);
        valNF.setFont(textFont);
        minNF.setFont(textFont);
    }

    public boolean isAdjusting()
    {
        return slider.getValueIsAdjusting();
    }

    public boolean isShowingFields()
    {
        return showingFields;
    }

    public void setShowingFields(boolean showingFields)
    {
        this.showingFields = showingFields;
        showFields();
    }

    public boolean isShowFieldsButtonVisible()
    {
        return showFieldsButtonVisible;
    }

    public void setShowFieldsButtonVisible(boolean showFieldsButtonVisible)
    {
        this.showFieldsButtonVisible = showFieldsButtonVisible;
        updateShowFieldsButton();
    }

    private void updateShowFieldsButton()
    {
        //right (big(full, empty), small(..)), left(..)
        //<html>&#9654;&#9655;&#9656;&#9657;<br/>&#9664;&#9665;&#9666;&#9667;        

//        showFieldsButton.setText(showingFields ? "<html>&#9664;" : "<html>&#9664;"); //small full left, full right
        showFieldsButton.setText(showingFields ? "<html>&minus;" : "+");
        showFieldsButton.setVisible(showFieldsButtonVisible);
    }

    public void showFields()
    {
        GridBagConstraints gbc = ((GridBagLayout) getLayout()).getConstraints(fieldsPanel);
        gbc.weightx = showingFields ? 0.2 : 0.0;
        ((GridBagLayout) getLayout()).setConstraints(fieldsPanel, gbc);
        fieldsPanel.setCustomWidth(showingFields ? 20 : 0);
        fieldsSetEnabled(showingFields && super.isEnabled());
        updateShowFieldsButton();
        invalidate();
        this.validate();
    }

    private void fieldsSetEnabled(boolean enabled)
    {
        minNF.setEnabled(enabled);
        maxNF.setEnabled(enabled);
        valNF.setEnabled(enabled);
        fieldsPanel.setEnabled(enabled);
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled); //enable/disable whole panel -> darken/lighten border
        slider.setEnabled(enabled); //does not fire state changed (OK)
        showFieldsButton.setEnabled(enabled);
        fieldsSetEnabled(enabled);
    }

    public FieldType getFieldType()
    {
        return type;
    }

    public void setFieldType(FieldType fieldType)
    {
        type = fieldType;
        minNF.setFieldType(fieldType);
        maxNF.setFieldType(fieldType);
        valNF.setFieldType(fieldType);

//        if (scaleType == ScaleType.LOGARITHMIC) validateModelAgainstLogBounds();
        validateModelAndUpdateUI(ModelChangeSource.NONE, true);
        recalcSliderIfNeeded(RecalculateReason.MODEL_CHANGED);
    }

    public ScaleType getScaleType()
    {
        return scaleType;
    }

    public void setScaleType(ScaleType scaleType)
    {
        boolean change = scaleType != this.scaleType;
        this.scaleType = scaleType;

        validateModelAndUpdateUI(ModelChangeSource.NONE, true);
        //there is change testing in here anyway
        recalcSliderIfNeeded(RecalculateReason.SCALE_TYPE_CHANGED);
    }

    /**
     * Using internal slider directly may cause problems with internal state of this ExtendedSlider. Use on your own risk.
     * You can use internal slider to attach different event listeners like RMB click, for example .
     * <p>
     * @return
     */
    public JSlider getInternalSlider()
    {
        return slider;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.visnow.vn.system.swing.CustomSizePanel fieldsPanel;
    private org.visnow.vn.gui.components.NumericTextField maxNF;
    private org.visnow.vn.gui.components.NumericTextField minNF;
    private javax.swing.JButton showFieldsButton;
    private javax.swing.JSlider slider;
    private org.visnow.vn.system.swing.CustomSizePanel sliderPanel;
    private org.visnow.vn.gui.components.NumericTextField valNF;
    // End of variables declaration//GEN-END:variables

    //by default submit every single change of value
    private boolean submitOnAdjusting = true;

    /**
     * Indicates if value changed should be fired when user adjusts this slider. If not then valueChanged is fired after stop adjusting.
     */
    public boolean isSubmitOnAdjusting()
    {
        return submitOnAdjusting;
    }

    /**
     * Indicates if value changed should be fired when user adjusts this slider. If not then valueChanged is fired after stop adjusting.
     */
    public void setSubmitOnAdjusting(boolean submitOnAdjusting)
    {
        this.submitOnAdjusting = submitOnAdjusting;
    }

    /**
     * Notifies all UserActionListeners about userChangeAction.
     */
    private void fireValueChanged()
    {
        //        LOGGER.debug("New value: " + userVal);
        for (UserActionListener listener : userActionListeners)
            listener.userChangeAction(new org.visnow.vn.gui.swingwrappers.UserEvent(this));
    }

    /**
     * Notifies all UserActionListeners about userAction.
     */
    private void fireUserAction()
    {
        for (UserActionListener listener : userActionListeners)
            listener.userAction(new org.visnow.vn.gui.swingwrappers.UserEvent(this));
    }

    private List<UserActionListener> userActionListeners = new ArrayList<UserActionListener>();

    public void addUserActionListener(UserActionListener listener)
    {
        userActionListeners.add(listener);
    }

    public void removeUserActionListener(UserActionListener listener)
    {
        userActionListeners.remove(listener);
    }
}
