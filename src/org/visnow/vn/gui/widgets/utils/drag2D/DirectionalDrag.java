/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.gui.widgets.utils.drag2D;

/**
 * Drag along specified axis.
 * <p>
 * One can specify two axes. They are considered to be orthogonal. If second axis is not specified then standard perpendicular axis is taken.
 * <p>
 * To work properly axes need to be linearly independent. Although it's not tested in constructor.
 * <p>
 * @author szpak
 */
public class DirectionalDrag
{
    private final double dragStartX;
    private final double dragStartY;
    //a11, a12, a21, a22
    private final double[] dragBasisMatrix;

    /**
     * Create new directional drag along specified axis1 and axis2 (which is considered orthogonal).
     * <p>
     * Axes are normalized so their length is not taken into consideration.
     */
    public DirectionalDrag(double dragStartX, double dragStartY, double axis1X, double axis1Y, double axis2X, double axis2Y)
    {
        this.dragStartX = dragStartX;
        this.dragStartY = dragStartY;
        double length1 = Math.sqrt(axis1X * axis1X + axis1Y * axis1Y);
        axis1X /= length1;
        axis1Y /= length1;
        double length2 = Math.sqrt(axis2X * axis2X + axis2Y * axis2Y);
        axis2X /= length1;
        axis2Y /= length1;

        double det = axis1X * axis2Y - axis2X * axis1Y;
        //matrix which converts to drag base
        dragBasisMatrix = new double[]{axis2Y / det, -axis2X / det,
                                       -axis1Y / det, axis1X / det};
    }

    /**
     * Create new directional drag along specified axis1 and axis perpendicular to axis1.
     * <p>
     * Invocation of this method is equal to
     * new DirectionalDrag(dragStartX, dragStartY, axis1X, axis1Y, -axis1Y, axis1X);
     */
    public DirectionalDrag(double dragStartX, double dragStartY, double axis1X, double axis1Y)
    {
        this(dragStartX, dragStartY, axis1X, axis1Y, -axis1Y, axis1X);
    }

    public double calculateDrag(double x, double y)
    {
        x -= dragStartX;
        y -= dragStartY;

        double dX = dragBasisMatrix[0] * x + dragBasisMatrix[1] * y;

        return dX;
    }

    public double[] calculateDoubleDrag(double x, double y)
    {
        x -= dragStartX;
        y -= dragStartY;

        double dX = dragBasisMatrix[0] * x + dragBasisMatrix[1] * y;
        double dY = dragBasisMatrix[2] * x + dragBasisMatrix[3] * y;

        return new double[]{dX, dY};
    }
}
