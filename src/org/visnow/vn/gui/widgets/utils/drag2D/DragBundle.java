/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.gui.widgets.utils.drag2D;

import java.awt.Cursor;
import org.visnow.vn.gui.widgets.utils.hyperrectangle.Endpoint;
import org.visnow.vn.gui.widgets.utils.hyperrectangle.Hyperrectangle;

/**
 * Drag bundle consists of dimension(s) and its endpoint(s) to drag, proper directionalDrag for dragging this direction, ratio(s) between screen distance and
 * dimension distance and proper cursor for showing to the user.
 * <p>
 * @author szpak
 */
public class DragBundle
{
    private final DirectionalDrag directionalDrag;
    private final Endpoint[] endpoints;
    private final int[] dimensions;
    private final double[] ratios;
    private final Cursor cursor;

    /**
     * Create single drag bundle
     */
    public DragBundle(DirectionalDrag directionalDrag, int dimension, Endpoint endpoint, double ratio, Cursor cursor)
    {
        this.directionalDrag = directionalDrag;
        this.dimensions = new int[]{dimension};
        this.endpoints = new Endpoint[]{endpoint};
        this.ratios = new double[]{ratio};
        this.cursor = cursor;
    }

    /**
     * Create double drag bundle
     */
    public DragBundle(DirectionalDrag directionalDrag, int dimension1, int dimension2, Endpoint endpoint1, Endpoint endpoint2, double ratio1, double ratio2, Cursor cursor)
    {
        this.directionalDrag = directionalDrag;
        this.dimensions = new int[]{dimension1, dimension2};
        this.endpoints = new Endpoint[]{endpoint1, endpoint2};
        this.ratios = new double[]{ratio1, ratio2};
        this.cursor = cursor;
    }

    public Cursor getCursor()
    {
        return cursor;
    }
    
    public void applyDrag(Hyperrectangle h, double x, double y, double minimumDimensionLength)
    {
        if (minimumDimensionLength < 0)
            throw new IllegalArgumentException("MinimumDimensionLength : " + minimumDimensionLength + " cannot be negative.");

        double[] distance = new double[h.getNumberOfDimensions()];
        double[] drags = directionalDrag.calculateDoubleDrag(x, y);
        Endpoint[] hyperrectangleEndpoints = new Endpoint[h.getNumberOfDimensions()];

        for (int i = 0; i < dimensions.length; i++) {
            int dim = dimensions[i];
            distance[dim] = drags[i] * ratios[i];
            hyperrectangleEndpoints[dim] = endpoints[i];
        }

        h.translate(hyperrectangleEndpoints, distance, minimumDimensionLength);
    }

}
