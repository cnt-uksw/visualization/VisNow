/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */

package org.visnow.vn.gui.widgets;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import org.visnow.vn.lib.utils.io.VNIOException;

/**
 * A frame displaying some file parsing error
 * It allows to display the exception caused by the error, the offending file
 * and optionally the metafile with edit capability
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class FileErrorFrame extends javax.swing.JFrame
{

    private final String fname;
    private final int lineNumber;
    private String vnfName = null;
    private int vnfLineNumber = -1;
    private final Highlighter.HighlightPainter redPainter =
            new DefaultHighlighter.DefaultHighlightPainter(new Color(1.f, .5f, .5f, .5f));

    /**
     * Creates new form FileErrorFrame
     */
    private FileErrorFrame(String text, String fname, int lineNumber, Exception e)
    {
        initComponents();
        int lastIndex = 0, count = 2;
        while (lastIndex != -1) {
            lastIndex = text.indexOf("<p>",lastIndex);
            if( lastIndex != -1)
                count++;
            lastIndex += 3;
        }
        errorLabel.setSize(500, count * (errorLabel.getFont().getSize() + 2));
        errorLabel.setText(lineNumber < 0 ? "<html>" + text + "</html>": "<html>" + text + "<p>in " + fname + ", line " + lineNumber + "</html>");
        this.fname = fname;
        this.lineNumber = lineNumber;
        if (e != null) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            exceptionTextArea.setText(sw.toString());
        }
        else
            tabbedPane.remove(exceptionPane);
        if (lineNumber >= 0)
            badFilePanel.setDataFromFile(fname, lineNumber);
        else
            tabbedPane.remove(badFilePanel);
        setVisible(true);
    }

    /**
     * Creates new form FileErrorFrame
     */
    private FileErrorFrame(VNIOException e)
    {
        fname = e.fname;
        lineNumber = e.lineNumber - 1;
        vnfName = e.vnfName;
        vnfLineNumber = e.vnfLineNumber - 1;
        initComponents();
        String txt = e.lineNumber < 0    ? e.text :
                     e.vnfLineNumber < 0 ? e.text + " in " + fname + ", line " + lineNumber :
                                           e.text + " in " + fname + ", line " + lineNumber + " from " + vnfName + ", line " + vnfLineNumber;
        errorLabel.setText("<html>" + txt + "</html>");
        if (e.ex != null) {
            StringWriter sw = new StringWriter();
            e.ex.printStackTrace(new PrintWriter(sw));
            exceptionTextArea.setText(sw.toString());
        }
        else
            tabbedPane.remove(exceptionPane);
        if (lineNumber >= 0)
            badFilePanel.setDataFromFile(fname, lineNumber);
        else
            tabbedPane.remove(badFilePanel);
        if (vnfLineNumber >= 0)
            vnfFilePanel.setDataFromFile(vnfName, vnfLineNumber);
        else
            tabbedPane.remove(vnfFilePanel);
        setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        errorLabel = new javax.swing.JLabel();
        tabbedPane = new javax.swing.JTabbedPane();
        badFilePanel = new org.visnow.vn.gui.widgets.EditorPanel();
        vnfFilePanel = new org.visnow.vn.gui.widgets.EditorPanel();
        exceptionPane = new javax.swing.JScrollPane();
        exceptionTextArea = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        editButton = new javax.swing.JButton();
        okButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setSize(new java.awt.Dimension(805, 469));

        errorLabel.setForeground(new java.awt.Color(153, 0, 51));
        errorLabel.setText("<html>error <p> in file</html>");
        errorLabel.setToolTipText("");
        errorLabel.setMaximumSize(new java.awt.Dimension(500, 200));
        errorLabel.setMinimumSize(new java.awt.Dimension(500, 20));
        errorLabel.setPreferredSize(new java.awt.Dimension(500, 100));
        getContentPane().add(errorLabel, java.awt.BorderLayout.NORTH);

        tabbedPane.addTab("offending file", badFilePanel);
        tabbedPane.addTab("vnfFile", vnfFilePanel);

        exceptionTextArea.setColumns(20);
        exceptionTextArea.setRows(5);
        exceptionPane.setViewportView(exceptionTextArea);

        tabbedPane.addTab("exception", exceptionPane);

        getContentPane().add(tabbedPane, java.awt.BorderLayout.CENTER);
        tabbedPane.getAccessibleContext().setAccessibleName("file context");

        jPanel1.setLayout(new java.awt.GridLayout(1, 0));

        editButton.setText("save edits and close");
        editButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editButtonActionPerformed(evt);
            }
        });
        jPanel1.add(editButton);

        okButton.setText("OK, close");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });
        jPanel1.add(okButton);

        getContentPane().add(jPanel1, java.awt.BorderLayout.SOUTH);

        setSize(new java.awt.Dimension(838, 471));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        setVisible(false);
    }//GEN-LAST:event_okButtonActionPerformed

    private void editButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_editButtonActionPerformed
    {//GEN-HEADEREND:event_editButtonActionPerformed
        try
        {
            if (fname != null && !fname.isEmpty() && lineNumber >= 0) {
                PrintWriter w = new PrintWriter(fname);
                w.print(badFilePanel.getText());
                w.close();
            }
            if (vnfName != null && !vnfName.isEmpty() && vnfLineNumber >= 0) {
                PrintWriter w = new PrintWriter(vnfName);
                w.print(vnfFilePanel.getText());
                w.close();
            }
        } catch (FileNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(FileErrorFrame.class.getName()).log(Level.SEVERE, fname + " or " + vnfName + "not found", ex);
        }
        this.setVisible(false);
    }//GEN-LAST:event_editButtonActionPerformed

/**
 * Creates and displays the frame with the given error message, offending file and exception
 * @param text error message text
 * @param fname offending file path
 * @param lineNumber number of offending line
 * @param e exception (may be null)
 */
    public static void display(String text, String fname, int lineNumber, Exception e)
    {
        new FileErrorFrame(text, fname, lineNumber, e);
    }

    /**
     * Creates and displays the frame describing a VNF parsing exception
     * @param e a VNF parsing exception thrown from the VisNow field reader
     */
    public static void display(VNIOException e)
    {
        new FileErrorFrame(e);
    }

    public static void main(String[] args)
    {
        display("xxxx<p>yyyyyyy", "/home/know/.tcshrc", 5, null);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.visnow.vn.gui.widgets.EditorPanel badFilePanel;
    private javax.swing.JButton editButton;
    private javax.swing.JLabel errorLabel;
    private javax.swing.JScrollPane exceptionPane;
    private javax.swing.JTextArea exceptionTextArea;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton okButton;
    private javax.swing.JTabbedPane tabbedPane;
    private org.visnow.vn.gui.widgets.EditorPanel vnfFilePanel;
    // End of variables declaration//GEN-END:variables
}
