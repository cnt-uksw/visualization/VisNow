//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
//</editor-fold>


package org.visnow.vn.gui.widgets;


import java.awt.Font;
import java.io.File;
import java.io.FileReader;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ListCellRenderer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Element;

/**
 * An extension to a standard JTextPane with line numbers and highlighting of a program selected line
 * @author know
 */
public class EditorPanel extends javax.swing.JPanel
{
    private int documentLength = -1;
    private int lineNumber = -1;
    class MyDocumentListener implements DocumentListener
    {
        @Override
        public void insertUpdate(DocumentEvent e)
        {
            updateLineNumbers(e.getOffset(), 1);
        }

        @Override
        public void removeUpdate(DocumentEvent e)
        {
            updateLineNumbers(e.getOffset(), -1);
        }

        @Override
        public void changedUpdate(DocumentEvent e)
        {
            updateLineNumbers(e.getOffset(), 0);
        }
    }


    /**
     * Creates new form EditorPanel
     */
    public EditorPanel()
    {
        initComponents();
        editPane.getDocument().addDocumentListener(new MyDocumentListener());
        linesList.setFixedCellHeight(editPane.getFontMetrics(editPane.getFont()).getHeight());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        scrollPane = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        linesList = new javax.swing.JList<>();
        editPane = new javax.swing.JTextPane();

        setLayout(new java.awt.BorderLayout());

        jPanel1.setLayout(new java.awt.GridBagLayout());

        linesList.setBackground(new java.awt.Color(200, 200, 200));
        linesList.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        linesList.setSelectionBackground(new java.awt.Color(255, 51, 51));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        jPanel1.add(linesList, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.6;
        jPanel1.add(editPane, gridBagConstraints);

        scrollPane.setViewportView(jPanel1);

        add(scrollPane, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void updateLineNumbers(int changePosition, int mod)
    {
        Element txt = editPane.getDocument().getDefaultRootElement();
        int len = txt.getElementCount();
        if (len == documentLength)
            return;
        DefaultListModel model = new DefaultListModel();
        for (int i = 0; i < len; i++)
            model.addElement(String.format("%6d ", i));
        linesList.setModel(model);
        if (lineNumber >= 0) {
            if (txt.getElementIndex(changePosition) < lineNumber)
                lineNumber += mod * (len - documentLength);
            linesList.setSelectedIndex(lineNumber);
            int y = (lineNumber - 6) * linesList.getFixedCellHeight();
            scrollPane.getViewport().setViewPosition(new java.awt.Point(0,Math.max(0, y)));
        }
        documentLength = len;
    }

/**
 * Fills the text pane with the multiline text, highlights selected line number
 * @param txt text to be displayed/edited
 * @param lineNumber highlighted line number
 */
    public void setData(String txt, int lineNumber)
    {
        this.lineNumber = lineNumber - 1;
        editPane.setText(txt);
        linesList.setSelectedIndex(lineNumber);
        if (lineNumber >= 0) {
            int y = (lineNumber - 6) * linesList.getFixedCellHeight();
            scrollPane.getViewport().setViewPosition(new java.awt.Point(0,Math.max(0, y)));
        }
    }

/**
 * Fills the text pane with the content of a text file, highlights selected line number
 * @param fname path to a text file to be edited
 * @param lineNumber  highlighted line number
 */
    public void setDataFromFile(String fname, int lineNumber)
    {
        if (fname != null) {
            try {
                File inFile = new File(fname);
                FileReader in = new FileReader(inFile);
                char[] buffer = new char[(int)inFile.length()];
                int n = in.read(buffer);
                in.close();
                setData(new String(buffer, 0, n), lineNumber);
            } catch (Exception e) {
            }
        }
    }

/**
 * getter to the text pane content
 * @return current text pane content (with edits)
 */
    public String getText()
    {
        return editPane.getText();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextPane editPane;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JList<String> linesList;
    private javax.swing.JScrollPane scrollPane;
    // End of variables declaration//GEN-END:variables
}
