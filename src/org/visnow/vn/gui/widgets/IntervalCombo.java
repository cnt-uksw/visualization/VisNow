/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.gui.widgets;

import java.awt.BorderLayout;
import java.util.ArrayList;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import org.visnow.vn.lib.utils.Range;
import static org.apache.commons.math3.util.FastMath.*;
import org.apache.log4j.Logger;
import org.visnow.vn.gui.swingwrappers.ComboBox;
import org.visnow.vn.gui.swingwrappers.UserActionListener;
import org.visnow.vn.gui.swingwrappers.UserEvent;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class IntervalCombo extends javax.swing.JPanel
{
    private static final Logger LOGGER = Logger.getLogger(IntervalCombo.class);
    private float selectedInterval;
    private int lastSelectedItem;
    private String[] intervalsFormattedStrings;
    private float[] intervalVals;

    /**
     * Creates new form IntervalCombo
     */
    public IntervalCombo()
    {
        initComponents();
    }

    /**
     * Prepares combo menu list as a series of nice values ranging from r/10^n to r
     * <p>
     * @param r       increment limit
     * @param n       range of increment values
     * @param initial initial selection is set at ~r/10^(initial + 1)
     */
    public void setRangeLength(float r, int n, int initial)
    {
//        LOGGER.debug(r + " " + n + " " + initial);
        initial = min(n - 1, initial);
        intervalsFormattedStrings = Range.intervalsFormattedStrings(r, n, 2);
        intervalSelectionCombo.setListData(intervalsFormattedStrings);
        lastSelectedItem = initial;
        if (initial == 0)
            lastSelectedItem = intervalsFormattedStrings.length / 2;
        intervalSelectionCombo.setSelectedIndex(lastSelectedItem);
        selectedInterval = Float.parseFloat(intervalsFormattedStrings[lastSelectedItem]);
    }

    /**
     * Convenience wrapper prepares combo menu list as a series of nice values ranging from r/10^n to r
     * <p>
     * @param r increment limit
     * @param n range of increment values
     *          initial selection is set at ~r/10
     */
    public void setRangeLength(float r, int n)
    {
        setRangeLength(r, n, 0);
    }

    /**
     * Convenience wrapper prepares combo menu list as a series of nice values ranging from r/100 to r
     * <p>
     * @param r increment limit
     */
    public void setRangeLength(float r)
    {
        setRangeLength(r, 2);
    }

    public void setIntervalValues(float[] intervalVals, String[] intervalsFormattedStrings, int initial)
    {
        this.intervalVals = intervalVals;
        this.intervalsFormattedStrings = intervalsFormattedStrings;
        intervalSelectionCombo.setListData(intervalsFormattedStrings);
        lastSelectedItem = initial;
        if (initial == 0)
            lastSelectedItem = intervalsFormattedStrings.length / 2;
        intervalSelectionCombo.setSelectedIndex(lastSelectedItem);
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT
     * modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        intervalSelectionCombo = new ComboBox();

        setLayout(new BorderLayout());

        intervalSelectionCombo.setEditable(true);
        intervalSelectionCombo.addUserActionListener(new UserActionListener()
        {
            public void userAction(UserEvent evt)
            {
            }
            public void userChangeAction(UserEvent evt)
            {
                intervalSelectionComboUserChangeAction(evt);
            }
        });
        add(intervalSelectionCombo, BorderLayout.CENTER);

        getAccessibleContext().setAccessibleName("");
    }// </editor-fold>//GEN-END:initComponents

    private void intervalSelectionComboUserChangeAction(UserEvent evt)//GEN-FIRST:event_intervalSelectionComboUserChangeAction
    {//GEN-HEADEREND:event_intervalSelectionComboUserChangeAction
        if (intervalSelectionCombo.getSelectedIndex() >= 0) {
            lastSelectedItem = intervalSelectionCombo.getSelectedIndex();
            selectedInterval = Float.parseFloat(intervalsFormattedStrings[lastSelectedItem]);
        } else {
            String s = (String) intervalSelectionCombo.getSelectedItem();
            try {
                float v = Float.parseFloat(s);
                if (v != 0) {
                    selectedInterval = v;
                    fireStateChanged();
                } else {
                    intervalSelectionCombo.setSelectedIndex(lastSelectedItem);
                    selectedInterval = Float.parseFloat(intervalsFormattedStrings[lastSelectedItem]);
                }
            } catch (NumberFormatException e) {
                intervalSelectionCombo.setSelectedIndex(lastSelectedItem);
                selectedInterval = Float.parseFloat(intervalsFormattedStrings[lastSelectedItem]);
            }
        }
        fireStateChanged();
    }//GEN-LAST:event_intervalSelectionComboUserChangeAction

    @Override
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled); //To change body of generated methods, choose Tools | Templates.
        intervalSelectionCombo.setEnabled(enabled);
    }
    
    /**
     * Utility field holding list of ChangeListeners.
     */
    private ArrayList<FloatValueModificationListener> changeListenerList
            = new ArrayList<FloatValueModificationListener>();

    /**
     * Registers ChangeListener to receive events.
     * <p>
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(FloatValueModificationListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(FloatValueModificationListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param object Parameter #1 of the <CODE>ChangeEvent<CODE> constructor.
     */
    private void fireStateChanged()
    {
        FloatValueModificationEvent e = new FloatValueModificationEvent(this, selectedInterval, false);
        for (FloatValueModificationListener listener : changeListenerList)
            listener.floatValueChanged(e);
    }

    public float getSelectedInterval()
    {
        return selectedInterval;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ComboBox intervalSelectionCombo;
    // End of variables declaration//GEN-END:variables
}
