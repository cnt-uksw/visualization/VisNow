/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.autohelp;

/**
 * fast, testing prototype of web browser to test java html rendering engine
 */
import javax.swing.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;

public class TestWebBrowser
{

    public static void main(String[] args)
    {

        // get the first URL
        String initialPage = "https://www.visnow.org/";
        if (args.length > 0) {
            initialPage = args[0];
        }

        // set up the editor pane
        final JEditorPane jep = new JEditorPane();
        jep.setEditable(false);

        try {
            jep.setPage(initialPage);
        } catch (IOException ex) {

        }

        // set up the window
        JScrollPane scrollPane = new JScrollPane(jep);
        JFrame f = new JFrame();
        JPanel p = new JPanel();
        p.setLayout(new BorderLayout());
        final JTextField t = new JTextField();
        t.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                try {
                    System.out.println("Loading : " + t.getText());
                    jep.setPage(t.getText());
                    System.out.println("Loading : " + t.getText());
                } catch (Exception ex) {
                }
                ;
            }
        });

        p.add(t, BorderLayout.NORTH);
        p.add(scrollPane, BorderLayout.CENTER);

        f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        f.setContentPane(p);
        f.setSize(512, 342);
        EventQueue.invokeLater(new FrameShower(f));

    }

    // Helps avoid a really obscure deadlock condition.
    // See http://java.sun.com/developer/JDCTechTips/2003/tt1208.html#1
    private static class FrameShower implements Runnable
    {

        private final Frame frame;

        FrameShower(Frame frame)
        {
            this.frame = frame;
        }

        public void run()
        {
            frame.setVisible(true);
        }

    }

}
