/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.datamaps;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class MultiArrayInt
{

    private int[] dims;
    private int[] dims2;
    private int[] data;

    public MultiArrayInt(int[] dims)
    {
        this.dims = dims;
        this.dims2 = new int[dims.length];
        int size = 1;
        for (int i = 0; i < dims.length; i++) {
            dims2[i] = size;
            size *= dims[i];
        }
        this.data = new int[size];
    }

    public MultiArrayInt(int[] dims, int[] data)
    {
        this.dims = dims;
        this.dims2 = new int[dims.length];
        int size = 1;
        for (int i = 0; i < dims.length; i++) {
            dims2[i] = size;
            size *= dims[i];
        }
        this.data = data;
    }

    public void set(int[] data)
    {
        this.data = data;
    }

    public void set(int value, int... indexes)
    {
        int p = 0;
        for (int i = 0; i < indexes.length; i++) {
            p += dims2[i] * indexes[i];
        }
        data[p] = value;
    }

    public int get(int... indexes)
    {
        int p = 0;
        for (int i = 0; i < indexes.length; i++) {
            p += dims2[i] * indexes[i];
        }
        return data[p];
    }

    public int getIndex(int... indexes)
    {
        int p = 0;
        for (int i = 0; i < indexes.length; i++) {
            p += dims2[i] * indexes[i];
        }
        return p;
    }

    public int getOffsets(int index, int... offsets)
    {
        int p = index;
        for (int i = 0; i < offsets.length; i++) {
            p += dims2[i] * offsets[i];
        }
        return data[p];
    }

    public void setOffsets(int value, int index, int... offsets)
    {
        int p = index;
        for (int i = 0; i < offsets.length; i++) {
            p += dims2[i] * offsets[i];
        }
        data[p] = value;
    }

    public int getNextIndex(int index, int dim)
    {
        return index + dims2[dim];
    }

    public int getPrevIndex(int index, int dim)
    {
        return index - dims2[dim];
    }

    public int getNext(int index, int dim)
    {
        return data[index + dims2[dim]];
    }

    public int getPrev(int index, int dim)
    {
        return data[index - dims2[dim]];
    }

    @Override
    public String toString()
    {
        String s = "";
        for (int i = 0; i < data.length; i++) {
            s = s + " " + data[i];
            for (int j = 0; j < dims.length; j++) {
                if ((i + 1) % dims2[j] == 0) {
                    s += "|";
                }
            }
        }
        return s;
    }

    public static void main(String argv[])
    {
        int[] dims = {2, 4};
        MultiArrayInt mai = new MultiArrayInt(dims);
        mai.set(9, 0, 1);
        mai.set(10, 0, 2);
        mai.set(11, 0, 3);
        mai.set(12, 1, 2);
        System.out.println(mai);
        int index = mai.getIndex(0, 2);
        System.out.println(mai.getNext(index, 0));
    }
}
