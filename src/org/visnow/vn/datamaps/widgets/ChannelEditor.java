/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.datamaps.widgets;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import org.visnow.vn.datamaps.colormap1d.ColorMap1D;
import org.visnow.vn.datamaps.colormap1d.RGBChannelColorMap1D;
import org.visnow.vn.datamaps.colormap1d.RGBChannelColorMap1D.Knot;
import org.visnow.vn.datamaps.utils.Orientation;
import static org.apache.commons.math3.util.FastMath.*;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ChannelEditor extends JComponent
{

    private final static Color[] channelColors = {new Color(255, 60, 115), new Color(127, 173, 50), new Color(1, 138, 253)};
    protected boolean[] channelVisiblity = {true, true, true};
    protected Knot selectedKnot = null;
    protected RGBChannelColorMap1D colorMap;
    protected Orientation orientation = Orientation.VERTICAL;
    protected Image btnImage;
    protected final PropertyChangeListener propertyChangeListener = new PropertyChangeListener()
    {

        public void propertyChange(PropertyChangeEvent evt)
        {
            repaint();
        }
    };

    public boolean[] getChannelVisiblity()
    {
        return channelVisiblity;
    }

    public void setChannelVisiblity(boolean[] channelVisiblity)
    {
        this.channelVisiblity = channelVisiblity;
    }

    public boolean isHorizontal()
    {
        return orientation == Orientation.HORIZONTAL;
    }

    private Point2D toModelSpace(Point p)
    {
        Insets insets = getInsets();
        Dimension dimension = new Dimension(getWidth() - insets.left - insets.right, getHeight() - insets.top - insets.bottom);
        if (isHorizontal()) {
            return new Point2D.Float((float) (p.x - insets.left) / dimension.width, 1 - (float) (p.y - insets.top) / dimension.height);
        } else {
            return new Point2D.Float((float) (p.y - insets.top) / dimension.height, (float) (p.x - insets.left) / dimension.width);
        }
    }

    public ChannelEditor()
    {
        try {
            btnImage = ImageIO.read(getClass().getResource("/org/visnow/vn/datamaps/widgets/resources/rgb_btn.png"));
        } catch (Exception e) {
        }
        addMouseListener(new MouseAdapter()
        {

            @Override
            public void mouseClicked(MouseEvent e)
            {
                if (colorMap != null) {
                    if ((e.getButton() == MouseEvent.BUTTON3)) {
                        float dist = Float.MAX_VALUE;
                        Knot closestKnot = null;
                        for (int c = 0; c < 3; c++) {
                            if (channelVisiblity[c]) {
                                Knot knot = colorMap.getClosestKnot(c, toModelSpace(e.getPoint()));
                                if (toModelSpace(e.getPoint()).distance(knot) < dist) {
                                    closestKnot = knot;
                                    dist = (float) toModelSpace(e.getPoint()).distance(knot);
                                }
                            }
                        }
                        if (closestKnot != null) {
                            colorMap.removeKnot(closestKnot);
                        }
                    } else if (e.getClickCount() >= 2) {

                        Point2D p = toModelSpace(e.getPoint());
                        float dist = Float.MAX_VALUE;
                        int channel = -1;
                        float[] components = colorMap.getComponents((float) p.getX());
                        for (int c = 0; c < 3; c++) {
                            if (channelVisiblity[c]) {
                                float newDist = (float) abs(components[c] - p.getY());
                                if (newDist < dist) {
                                    dist = newDist;
                                    channel = c;
                                }
                            }
                        }
                        if (channel >= 0) {
                            colorMap.addKnot(channel, new RGBChannelColorMap1D.Knot(p));
                        }
                    }
                }
            }

            @Override
            public void mousePressed(MouseEvent e)
            {
                if (colorMap != null) {
                    float dist = Float.MAX_VALUE;
                    for (int c = 0; c < 3; c++) {
                        if (channelVisiblity[c]) {
                            Knot knot = colorMap.getClosestKnot(c, toModelSpace(e.getPoint()));
                            if (toModelSpace(e.getPoint()).distance(knot) < dist) {
                                selectedKnot = knot;
                                dist = (float) toModelSpace(e.getPoint()).distance(knot);
                            }
                        }
                    }
                }
            }

            @Override
            public void mouseReleased(MouseEvent e)
            {
                selectedKnot = null;
            }
        });

        addMouseMotionListener(new MouseAdapter()
        {

            @Override
            public void mouseDragged(MouseEvent e)
            {
                if (colorMap != null) {
                    if (selectedKnot != null) {
                        colorMap.moveKnot(selectedKnot, toModelSpace(e.getPoint()));
                    }
                }
            }
        });
    }

    public Orientation getOrientation()
    {
        return orientation;
    }

    public void setOrientation(Orientation orientation)
    {
        this.orientation = orientation;
        repaint();
    }

    public RGBChannelColorMap1D getColorMap()
    {
        return colorMap;
    }

    public void setColorMap(ColorMap1D colorMap)
    {
        if (colorMap instanceof RGBChannelColorMap1D) {
            this.colorMap = (RGBChannelColorMap1D) colorMap;
            this.colorMap.addPropertyChangeListener(propertyChangeListener);
        }
        repaint();
    }

    @Override
    public void paint(Graphics g)
    {
        Graphics2D g2d = (Graphics2D) g;
        Insets insets = getInsets();
        Dimension dimension = new Dimension(getWidth() - insets.left - insets.right, getHeight() - insets.top - insets.bottom);

        g.setColor(getBackground());
        g.fillRect(insets.left, insets.top, dimension.width, dimension.height);

        if (colorMap != null) {
            // grid
            g.setColor(new Color(75, 75, 75, 10));
            int jump = (dimension.height - 1) / 16;
            for (int y = 0; y < dimension.height; y += jump) {
                g.drawLine(insets.left, insets.top + y, dimension.width - 1, insets.top + y);
            }
            g.drawLine(insets.left, dimension.height, dimension.width - 1, dimension.height);
            jump = (dimension.width - 1) / 16;
            for (int x = 0; x < dimension.width; x += jump) {
                g.drawLine(insets.left + x, insets.top, insets.left + x, dimension.height);
            }
            g.drawLine(dimension.width, insets.top, dimension.width, dimension.height);

            // channels
            List<RGBChannelColorMap1D.Knot>[] channelsKnots = colorMap.getChannelKnots();

            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                                 RenderingHints.VALUE_ANTIALIAS_ON);
            for (int c = 0; c < 3; c++) {
                if (!channelVisiblity[c]) {
                    g2d.setColor(new Color(50, 50, 50, 40));
                    g2d.setStroke(new BasicStroke(0.8f));
                    for (int i = 1; i < channelsKnots[c].size(); i++) {
                        RGBChannelColorMap1D.Knot knot0 = channelsKnots[c].get(i - 1);
                        RGBChannelColorMap1D.Knot knot1 = channelsKnots[c].get(i);

                        Point p0;
                        Point p1;
                        if (isHorizontal()) {
                            p0 = new Point((int) (insets.top + knot0.x * (dimension.width - 1)), (int) (insets.left + (1 - knot0.y) * (dimension.height - 1)));
                            p1 = new Point((int) (insets.top + knot1.x * (dimension.width - 1)), (int) (insets.left + (1 - knot1.y) * (dimension.height - 1)));
                        } else {
                            p0 = new Point((int) (insets.left + knot0.y * (dimension.width - 1)), (int) (insets.top + knot0.x * (dimension.height - 1)));
                            p1 = new Point((int) (insets.left + knot1.y * (dimension.width - 1)), (int) (insets.top + knot1.x * (dimension.height - 1)));
                        }

                        g2d.drawLine(p0.x, p0.y, p1.x, p1.y);
                    }
                    for (int i = 0; i < channelsKnots[c].size(); i++) {
                        RGBChannelColorMap1D.Knot knot0 = channelsKnots[c].get(i);
                        Point p0;
                        if (isHorizontal()) {
                            p0 = new Point((int) (insets.top + knot0.x * (dimension.width - 1)), (int) (insets.left + (1 - knot0.y) * (dimension.height - 1)));
                        } else {
                            p0 = new Point((int) (insets.left + knot0.y * (dimension.width - 1)), (int) (insets.top + knot0.x * (dimension.height - 1)));
                        }
                        g2d.drawImage(btnImage, p0.x - 3, p0.y - 3, p0.x + 4, p0.y + 4, 21, 0, 28, 7, null);
                    }
                }
            }
            for (int c = 0; c < 3; c++) {
                if (channelVisiblity[c]) {
                    g2d.setColor(channelColors[c]);
                    g2d.setStroke(new BasicStroke(0.6f));

                    for (int i = 1; i < channelsKnots[c].size(); i++) {
                        RGBChannelColorMap1D.Knot knot0 = channelsKnots[c].get(i - 1);
                        RGBChannelColorMap1D.Knot knot1 = channelsKnots[c].get(i);

                        Point p0;
                        Point p1;
                        if (isHorizontal()) {
                            p0 = new Point((int) (insets.top + knot0.x * (dimension.width - 1)), (int) (insets.left + (1 - knot0.y) * (dimension.height - 1)));
                            p1 = new Point((int) (insets.top + knot1.x * (dimension.width - 1)), (int) (insets.left + (1 - knot1.y) * (dimension.height - 1)));
                        } else {
                            p0 = new Point((int) (insets.left + knot0.y * (dimension.width - 1)), (int) (insets.top + knot0.x * (dimension.height - 1)));
                            p1 = new Point((int) (insets.left + knot1.y * (dimension.width - 1)), (int) (insets.top + knot1.x * (dimension.height - 1)));
                        }

                        g2d.drawLine(p0.x, p0.y, p1.x, p1.y);
                    }
                    for (int i = 0; i < channelsKnots[c].size(); i++) {
                        RGBChannelColorMap1D.Knot knot0 = channelsKnots[c].get(i);
                        Point p0;
                        if (isHorizontal()) {
                            p0 = new Point((int) (insets.top + knot0.x * (dimension.width - 1)), (int) (insets.left + (1 - knot0.y) * (dimension.height - 1)));
                        } else {
                            p0 = new Point((int) (insets.left + knot0.y * (dimension.width - 1)), (int) (insets.top + knot0.x * (dimension.height - 1)));
                        }
                        g2d.drawImage(btnImage, p0.x - 3, p0.y - 3, p0.x + 4, p0.y + 4, 7 * c, 0, 7 * (c + 1), 7, null);
                    }
                }
            }
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                                 RenderingHints.VALUE_ANTIALIAS_OFF);

        }
        super.paint(g);
    }
}
