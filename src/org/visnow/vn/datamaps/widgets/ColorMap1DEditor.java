/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.datamaps.widgets;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.visnow.vn.datamaps.colormap1d.ColorMap1D;
import org.visnow.vn.datamaps.utils.Orientation;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ColorMap1DEditor extends javax.swing.JPanel
{

    protected boolean[] channelVisiblity = {true, true, true};
    protected ColorMap1D colorMap;
    protected Orientation orientation = Orientation.VERTICAL;

    public ColorMap1D getColorMap()
    {
        return colorMap;
    }

    public void setColorMap(ColorMap1D colorMap)
    {
        this.colorMap = colorMap;
        channelEditor1.setColorMap(colorMap);
    }

    public Orientation getOrientation()
    {
        return orientation;
    }

    public void setOrientation(Orientation orientation)
    {
        this.orientation = orientation;
        channelEditor1.setOrientation(orientation);
        if (orientation == Orientation.HORIZONTAL) {
            channelEditor1.setPreferredSize(new Dimension(256, 90));
            channelEditor1.setSize(new Dimension(256, 90));
            channelEditor1.setMinimumSize(new Dimension(256, 90));
        } else {
            channelEditor1.setPreferredSize(new Dimension(100, 258));
            channelEditor1.setSize(new Dimension(100, 258));
            channelEditor1.setMinimumSize(new Dimension(100, 258));
        }
        repaint();
    }

    /**
     * Creates new form ChannelEditor
     */
    public ColorMap1DEditor()
    {
        initComponents();
        this.orientation = Orientation.VERTICAL;
        channelEditor1.setSize(getWidth(), getHeight());
        channelEditor1.setChannelVisiblity(channelVisiblity);

        try {
            //            tabbedButton1.setImage(ImageIO.read(getClass().getResource("/org/visnow/vn/datamaps/widgets/resources/tab.png")));
            //            tabbedButton1.addTab(new Rectangle(0, 0, 27, 13), new Rectangle(0, 39, 82, 13), new Rectangle(0, 0, 82, 13));
            //            tabbedButton1.addTab(new Rectangle(27, 0, 27, 13), new Rectangle(0, 52, 82, 13), new Rectangle(0, 13, 82, 13));
            //            tabbedButton1.addTab(new Rectangle(54, 0, 28, 13), new Rectangle(0, 65, 82, 13), new Rectangle(0, 26, 82, 13));
            Image img = ImageIO.read(getClass().getResource("/org/visnow/vn/datamaps/widgets/resources/rgb.png"));

            imageButton1.setImage(img);
            imageButton1.setNormalRect(new Rectangle(0, 0, 10, 26));
            imageButton1.setClickedRect(new Rectangle(30, 0, 10, 26));
            imageButton1.setSelectedRect(new Rectangle(20, 0, 10, 26));
            imageButton1.setSelectedClickedRect(new Rectangle(10, 0, 10, 26));

            imageButton2.setImage(img);
            imageButton2.setNormalRect(new Rectangle(0, 25, 10, 36));
            imageButton2.setClickedRect(new Rectangle(30, 25, 10, 36));
            imageButton2.setSelectedRect(new Rectangle(20, 25, 10, 36));
            imageButton2.setSelectedClickedRect(new Rectangle(10, 25, 10, 36));

            imageButton3.setImage(img);
            imageButton3.setNormalRect(new Rectangle(0, 60, 10, 30));
            imageButton3.setClickedRect(new Rectangle(30, 60, 10, 30));
            imageButton3.setSelectedRect(new Rectangle(20, 60, 10, 30));
            imageButton3.setSelectedClickedRect(new Rectangle(10, 60, 10, 30));
        } catch (Exception ex) {
            Logger.getLogger(ColorMap1DEditor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        imageButton1 = new org.visnow.vn.gui.widgets.ImageButton();
        imageButton2 = new org.visnow.vn.gui.widgets.ImageButton();
        imageButton3 = new org.visnow.vn.gui.widgets.ImageButton();
        channelEditor1 = new org.visnow.vn.datamaps.widgets.ChannelEditor();

        imageButton1.setPreferredSize(new java.awt.Dimension(10, 26));
        imageButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                setupChannelVisibility(evt);
            }
        });

        javax.swing.GroupLayout imageButton1Layout = new javax.swing.GroupLayout(imageButton1);
        imageButton1.setLayout(imageButton1Layout);
        imageButton1Layout.setHorizontalGroup(
                imageButton1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 10, Short.MAX_VALUE)
                );
        imageButton1Layout.setVerticalGroup(
                imageButton1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 26, Short.MAX_VALUE)
                );

        imageButton2.setPreferredSize(new java.awt.Dimension(10, 36));
        imageButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                setupChannelVisibility(evt);
            }
        });

        javax.swing.GroupLayout imageButton2Layout = new javax.swing.GroupLayout(imageButton2);
        imageButton2.setLayout(imageButton2Layout);
        imageButton2Layout.setHorizontalGroup(
                imageButton2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 10, Short.MAX_VALUE)
                );
        imageButton2Layout.setVerticalGroup(
                imageButton2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 36, Short.MAX_VALUE)
                );

        imageButton3.setPreferredSize(new java.awt.Dimension(10, 40));
        imageButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                setupChannelVisibility(evt);
            }
        });

        javax.swing.GroupLayout imageButton3Layout = new javax.swing.GroupLayout(imageButton3);
        imageButton3.setLayout(imageButton3Layout);
        imageButton3Layout.setHorizontalGroup(
                imageButton3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 10, Short.MAX_VALUE)
                );
        imageButton3Layout.setVerticalGroup(
                imageButton3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 40, Short.MAX_VALUE)
                );

        channelEditor1.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(java.awt.Color.gray), javax.swing.BorderFactory.createLineBorder(java.awt.Color.white)));

        javax.swing.GroupLayout channelEditor1Layout = new javax.swing.GroupLayout(channelEditor1);
        channelEditor1.setLayout(channelEditor1Layout);
        channelEditor1Layout.setHorizontalGroup(
                channelEditor1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 96, Short.MAX_VALUE)
                );
        channelEditor1Layout.setVerticalGroup(
                channelEditor1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 254, Short.MAX_VALUE)
                );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(channelEditor1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(imageButton3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(imageButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(imageButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(channelEditor1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(60, 60, 60)
                                                .addComponent(imageButton3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(imageButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(25, 25, 25)
                                                .addComponent(imageButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                );
    }// </editor-fold>//GEN-END:initComponents

    private void setupChannelVisibility(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_setupChannelVisibility

        channelVisiblity[0] = imageButton1.isSelected();
        channelVisiblity[1] = imageButton2.isSelected();
        channelVisiblity[2] = imageButton3.isSelected();
        channelEditor1.repaint();

    }//GEN-LAST:event_setupChannelVisibility
     // Variables declaration - do not modify//GEN-BEGIN:variables

    private org.visnow.vn.datamaps.widgets.ChannelEditor channelEditor1;
    private org.visnow.vn.gui.widgets.ImageButton imageButton1;
    private org.visnow.vn.gui.widgets.ImageButton imageButton2;
    private org.visnow.vn.gui.widgets.ImageButton imageButton3;
    // End of variables declaration//GEN-END:variables
}
