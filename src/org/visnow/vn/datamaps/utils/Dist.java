/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.datamaps.utils;

import java.awt.Point;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.util.Vector;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Dist
{

    public static Point getClosestPoint(Point p, Path2D path)
    {

        Vector<Point> pts = new Vector<Point>();
        PathIterator fpi = path.getPathIterator(null, 0.25f);

        double[] coords = new double[2];
        Point pp1 = new Point(0, 0), pp2 = new Point(0, 0);
        while (!fpi.isDone()) {
            switch (fpi.currentSegment(coords)) {
                case PathIterator.SEG_CLOSE:
                    break;
                case PathIterator.SEG_LINETO:
                    pp2 = new Point((int) coords[0], (int) coords[1]);
                    Point p4 = Dist.getClosestPoint(pp1, pp2, p);
                    pts.add(p4);
                    pp1 = pp2;
                    break;
                case PathIterator.SEG_MOVETO:
                    pp1 = new Point((int) coords[0], (int) coords[1]);
                    break;

                case PathIterator.SEG_CUBICTO:
                case PathIterator.SEG_QUADTO:
                default:

            }
            fpi.next();
        }

        double minDist = Double.MAX_VALUE;
        Point minP = new Point(0, 0);
        for (Point pp : pts) {
            if (p.distance(pp) < minDist) {
                minP = pp;
                minDist = p.distance(pp);
            }
        }

        return minP;
    }

    public static Point getClosestPoint(Point pt1, Point pt2, Point p)
    {
        double u = ((p.x - pt1.x) * (pt2.x - pt1.x) + (p.y - pt1.y) * (pt2.y - pt1.y)) / (sqr(pt2.x - pt1.x) + sqr(pt2.y - pt1.y));
        if (u > 1.0) {
            return (Point) pt2.clone();
        } else if (u <= 0.0) {
            return (Point) pt1.clone();
        } else {
            return new Point((int) (pt2.x * u + pt1.x * (1.0 - u) + 0.5), (int) (pt2.y * u + pt1.y * (1.0 - u) + 0.5));
        }
    }

    private static double sqr(double x)
    {
        return x * x;
    }

    private Dist()
    {
    }
}
