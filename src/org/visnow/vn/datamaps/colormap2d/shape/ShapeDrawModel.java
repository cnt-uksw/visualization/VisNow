/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.datamaps.colormap2d.shape;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.visnow.vn.lib.basic.utilities.ShapeColorMapEditor.PaintMode;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public abstract class ShapeDrawModel implements MouseMotionListener, MouseListener
{

    protected boolean showCP;
    protected boolean showOutline;
    protected boolean show;
    protected int selectedCP = -1;
    protected int pts[];
    public final PropertyChangeSupport propertyChangeSupport;

    public ShapeDrawModel()
    {
        this.propertyChangeSupport = new PropertyChangeSupport(this);
    }

    public boolean isShowCP()
    {
        return showCP;
    }

    public void setShowCP(boolean showCP)
    {
        this.showCP = showCP;
        propertyChangeSupport.firePropertyChange("showCP", null, showCP);
    }

    public boolean isShow()
    {
        return show;
    }

    public void setShow(boolean show)
    {
        this.show = show;
        propertyChangeSupport.firePropertyChange("show", null, show);
    }

    public boolean isShowOutline()
    {
        return showOutline;
    }

    public void setShowOutline(boolean showOutline)
    {
        this.showOutline = showOutline;
        propertyChangeSupport.firePropertyChange("showOutline", null, showOutline);
    }

    public abstract void paintShapeOutline(Graphics2D g2d);

    public abstract void paintShape(Graphics2D g2d, PaintMode paintMode);

    public void paintControlPoints(Graphics g)
    {
        if (showCP && show) {
            try {
                BufferedImage btnImage = ImageIO.read(getClass().getResource("/org/visnow/vn/datamaps/widgets/resources/rgb_btn.png"));
                Graphics2D g2d = (Graphics2D) g;

                int c = 3;
                for (int i = 0; i < pts.length / 2; i++) {
                    g2d.drawImage(btnImage, pts[2 * i] - 3, pts[2 * i + 1] - 3, pts[2 * i] + 4, pts[2 * i + 1] + 4, 7 * c, 0, 7 * (c + 1), 7, null);
                }

            } catch (IOException ex) {
            }
        }

    }

    public abstract void updatePts();

    public void mouseDragged(MouseEvent e)
    {
    }

    public void mouseMoved(MouseEvent e)
    {
    }

    public void mouseClicked(MouseEvent e)
    {
    }

    public void mousePressed(MouseEvent e)
    {
    }

    public void mouseReleased(MouseEvent e)
    {
    }

    public void mouseEntered(MouseEvent e)
    {
    }

    public void mouseExited(MouseEvent e)
    {
    }
}
